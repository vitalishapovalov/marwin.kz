/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	var parentJsonpFunction = window["webpackJsonp"];
/******/ 	window["webpackJsonp"] = function webpackJsonpCallback(chunkIds, moreModules) {
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, callbacks = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(installedChunks[chunkId])
/******/ 				callbacks.push.apply(callbacks, installedChunks[chunkId]);
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(chunkIds, moreModules);
/******/ 		while(callbacks.length)
/******/ 			callbacks.shift().call(null, __webpack_require__);
/******/
/******/ 	};
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// "0" means "already loaded"
/******/ 	// Array means "loading", array contains callbacks
/******/ 	var installedChunks = {
/******/ 		0:0
/******/ 	};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/ 	// This file contains only the entry chunk.
/******/ 	// The chunk loading function for additional chunks
/******/ 	__webpack_require__.e = function requireEnsure(chunkId, callback) {
/******/ 		// "0" is the signal for "already loaded"
/******/ 		if(installedChunks[chunkId] === 0)
/******/ 			return callback.call(null, __webpack_require__);
/******/
/******/ 		// an array means "currently loading".
/******/ 		if(installedChunks[chunkId] !== undefined) {
/******/ 			installedChunks[chunkId].push(callback);
/******/ 		} else {
/******/ 			// start chunk loading
/******/ 			installedChunks[chunkId] = [callback];
/******/ 			var head = document.getElementsByTagName('head')[0];
/******/ 			var script = document.createElement('script');
/******/ 			script.type = 'text/javascript';
/******/ 			script.charset = 'utf-8';
/******/ 			script.async = true;
/******/
/******/ 			script.src = __webpack_require__.p + window["webpackManifest"][chunkId];
/******/ 			head.appendChild(script);
/******/ 		}
/******/ 	};
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "./";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	/**
	 * App entry point.
	 *
	 * @module App
	 */
	
	/** Import commonly used libs (initialized by default) */
	
	__webpack_require__(1);
	
	__webpack_require__(3);
	
	var _Header = __webpack_require__(4);
	
	var _Header2 = _interopRequireDefault(_Header);
	
	var _Footer = __webpack_require__(8);
	
	var _Footer2 = _interopRequireDefault(_Footer);
	
	var _Menu = __webpack_require__(9);
	
	var _Menu2 = _interopRequireDefault(_Menu);
	
	var _Common = __webpack_require__(10);
	
	var _Common2 = _interopRequireDefault(_Common);
	
	var _ProductItem = __webpack_require__(13);
	
	var _ProductItem2 = _interopRequireDefault(_ProductItem);
	
	var _Main = __webpack_require__(14);
	
	var _Main2 = _interopRequireDefault(_Main);
	
	var _Catalog = __webpack_require__(17);
	
	var _Catalog2 = _interopRequireDefault(_Catalog);
	
	var _Filter = __webpack_require__(18);
	
	var _Filter2 = _interopRequireDefault(_Filter);
	
	var _Product = __webpack_require__(42);
	
	var _Product2 = _interopRequireDefault(_Product);
	
	var _Cart = __webpack_require__(43);
	
	var _Cart2 = _interopRequireDefault(_Cart);
	
	var _Registration = __webpack_require__(44);
	
	var _Registration2 = _interopRequireDefault(_Registration);
	
	var _Search = __webpack_require__(45);
	
	var _Search2 = _interopRequireDefault(_Search);
	
	var _News = __webpack_require__(46);
	
	var _News2 = _interopRequireDefault(_News);
	
	var _Calendar = __webpack_require__(47);
	
	var _Calendar2 = _interopRequireDefault(_Calendar);
	
	var _Shops = __webpack_require__(48);
	
	var _Shops2 = _interopRequireDefault(_Shops);
	
	var _Helpers = __webpack_require__(6);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	_Header2.default.init();
	
	/** Import common components (initialized by default) */
	
	_Footer2.default.init();
	
	_Menu2.default.init();
	
	_Common2.default.init();
	
	_ProductItem2.default.init();
	
	/** Import page controllers */
	
	
	/** Import utility functions */
	
	
	/**
	 * Detect current page and run appropriate scripts.
	 *
	 * @param {String} currentPage - current page detector.
	 **/
	switch (_Helpers.currentPage) {
	  /** Main page */
	  case 'main':
	    {
	      _Main2.default.init();
	      break;
	    }
	  /** Catalog page */
	  case 'catalog':
	    {
	      _Catalog2.default.init();
	      break;
	    }
	  /** Filter page */
	  case 'filter':
	    {
	      _Filter2.default.init();
	      break;
	    }
	  /** Product page */
	  case 'product':
	    {
	      _Product2.default.init();
	      break;
	    }
	  /** Cart page */
	  case 'cart':
	    {
	      _Cart2.default.init();
	      break;
	    }
	  /** Registration page */
	  case 'registration':
	    {
	      _Registration2.default.init();
	      break;
	    }
	  /** 'Search results' page */
	  case 'search':
	    {
	      _Search2.default.init();
	      break;
	    }
	  /** News & Sales pages */
	  case 'news':
	    {
	      _News2.default.init();
	      break;
	    }
	  /** Calendar page */
	  case 'calendar':
	    {
	      _Calendar2.default.init();
	      break;
	    }
	  /** Shops pages */
	  case 'shops':
	    {
	      _Shops2.default.init();
	      break;
	    }
	}

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(jQuery) {/**
	 * Owl Carousel v2.1.4
	 * Copyright 2013-2016 David Deutsch
	 * Licensed under MIT (https://github.com/OwlCarousel2/OwlCarousel2/blob/master/LICENSE)
	 */
	/**
	 * Owl carousel
	 * @version 2.1.0
	 * @author Bartosz Wojciechowski
	 * @author David Deutsch
	 * @license The MIT License (MIT)
	 * @todo Lazy Load Icon
	 * @todo prevent animationend bubling
	 * @todo itemsScaleUp
	 * @todo Test Zepto
	 * @todo stagePadding calculate wrong active classes
	 */
	;(function($, window, document) {
	
		/**
		 * Creates a carousel.
		 * @class The Owl Carousel.
		 * @public
		 * @param {HTMLElement|jQuery} element - The element to create the carousel for.
		 * @param {Object} [options] - The options
		 */
		function Owl(element, options) {
	
			/**
			 * Current settings for the carousel.
			 * @public
			 */
			this.settings = null;
	
			/**
			 * Current options set by the caller including defaults.
			 * @public
			 */
			this.options = $.extend({}, Owl.Defaults, options);
	
			/**
			 * Plugin element.
			 * @public
			 */
			this.$element = $(element);
	
			/**
			 * Proxied event handlers.
			 * @protected
			 */
			this._handlers = {};
	
			/**
			 * References to the running plugins of this carousel.
			 * @protected
			 */
			this._plugins = {};
	
			/**
			 * Currently suppressed events to prevent them from beeing retriggered.
			 * @protected
			 */
			this._supress = {};
	
			/**
			 * Absolute current position.
			 * @protected
			 */
			this._current = null;
	
			/**
			 * Animation speed in milliseconds.
			 * @protected
			 */
			this._speed = null;
	
			/**
			 * Coordinates of all items in pixel.
			 * @todo The name of this member is missleading.
			 * @protected
			 */
			this._coordinates = [];
	
			/**
			 * Current breakpoint.
			 * @todo Real media queries would be nice.
			 * @protected
			 */
			this._breakpoint = null;
	
			/**
			 * Current width of the plugin element.
			 */
			this._width = null;
	
			/**
			 * All real items.
			 * @protected
			 */
			this._items = [];
	
			/**
			 * All cloned items.
			 * @protected
			 */
			this._clones = [];
	
			/**
			 * Merge values of all items.
			 * @todo Maybe this could be part of a plugin.
			 * @protected
			 */
			this._mergers = [];
	
			/**
			 * Widths of all items.
			 */
			this._widths = [];
	
			/**
			 * Invalidated parts within the update process.
			 * @protected
			 */
			this._invalidated = {};
	
			/**
			 * Ordered list of workers for the update process.
			 * @protected
			 */
			this._pipe = [];
	
			/**
			 * Current state information for the drag operation.
			 * @todo #261
			 * @protected
			 */
			this._drag = {
				time: null,
				target: null,
				pointer: null,
				stage: {
					start: null,
					current: null
				},
				direction: null
			};
	
			/**
			 * Current state information and their tags.
			 * @type {Object}
			 * @protected
			 */
			this._states = {
				current: {},
				tags: {
					'initializing': [ 'busy' ],
					'animating': [ 'busy' ],
					'dragging': [ 'interacting' ]
				}
			};
	
			$.each([ 'onResize', 'onThrottledResize' ], $.proxy(function(i, handler) {
				this._handlers[handler] = $.proxy(this[handler], this);
			}, this));
	
			$.each(Owl.Plugins, $.proxy(function(key, plugin) {
				this._plugins[key.charAt(0).toLowerCase() + key.slice(1)]
					= new plugin(this);
			}, this));
	
			$.each(Owl.Workers, $.proxy(function(priority, worker) {
				this._pipe.push({
					'filter': worker.filter,
					'run': $.proxy(worker.run, this)
				});
			}, this));
	
			this.setup();
			this.initialize();
		}
	
		/**
		 * Default options for the carousel.
		 * @public
		 */
		Owl.Defaults = {
			items: 3,
			loop: false,
			center: false,
			rewind: false,
	
			mouseDrag: true,
			touchDrag: true,
			pullDrag: true,
			freeDrag: false,
	
			margin: 0,
			stagePadding: 0,
	
			merge: false,
			mergeFit: true,
			autoWidth: false,
	
			startPosition: 0,
			rtl: false,
	
			smartSpeed: 250,
			fluidSpeed: false,
			dragEndSpeed: false,
	
			responsive: {},
			responsiveRefreshRate: 200,
			responsiveBaseElement: window,
	
			fallbackEasing: 'swing',
	
			info: false,
	
			nestedItemSelector: false,
			itemElement: 'div',
			stageElement: 'div',
	
			refreshClass: 'owl-refresh',
			loadedClass: 'owl-loaded',
			loadingClass: 'owl-loading',
			rtlClass: 'owl-rtl',
			responsiveClass: 'owl-responsive',
			dragClass: 'owl-drag',
			itemClass: 'owl-item',
			stageClass: 'owl-stage',
			stageOuterClass: 'owl-stage-outer',
			grabClass: 'owl-grab'
		};
	
		/**
		 * Enumeration for width.
		 * @public
		 * @readonly
		 * @enum {String}
		 */
		Owl.Width = {
			Default: 'default',
			Inner: 'inner',
			Outer: 'outer'
		};
	
		/**
		 * Enumeration for types.
		 * @public
		 * @readonly
		 * @enum {String}
		 */
		Owl.Type = {
			Event: 'event',
			State: 'state'
		};
	
		/**
		 * Contains all registered plugins.
		 * @public
		 */
		Owl.Plugins = {};
	
		/**
		 * List of workers involved in the update process.
		 */
		Owl.Workers = [ {
			filter: [ 'width', 'settings' ],
			run: function() {
				this._width = this.$element.width();
			}
		}, {
			filter: [ 'width', 'items', 'settings' ],
			run: function(cache) {
				cache.current = this._items && this._items[this.relative(this._current)];
			}
		}, {
			filter: [ 'items', 'settings' ],
			run: function() {
				this.$stage.children('.cloned').remove();
			}
		}, {
			filter: [ 'width', 'items', 'settings' ],
			run: function(cache) {
				var margin = this.settings.margin || '',
					grid = !this.settings.autoWidth,
					rtl = this.settings.rtl,
					css = {
						'width': 'auto',
						'margin-left': rtl ? margin : '',
						'margin-right': rtl ? '' : margin
					};
	
				!grid && this.$stage.children().css(css);
	
				cache.css = css;
			}
		}, {
			filter: [ 'width', 'items', 'settings' ],
			run: function(cache) {
				var width = (this.width() / this.settings.items).toFixed(3) - this.settings.margin,
					merge = null,
					iterator = this._items.length,
					grid = !this.settings.autoWidth,
					widths = [];
	
				cache.items = {
					merge: false,
					width: width
				};
	
				while (iterator--) {
					merge = this._mergers[iterator];
					merge = this.settings.mergeFit && Math.min(merge, this.settings.items) || merge;
	
					cache.items.merge = merge > 1 || cache.items.merge;
	
					widths[iterator] = !grid ? this._items[iterator].width() : width * merge;
				}
	
				this._widths = widths;
			}
		}, {
			filter: [ 'items', 'settings' ],
			run: function() {
				var clones = [],
					items = this._items,
					settings = this.settings,
					view = Math.max(settings.items * 2, 4),
					size = Math.ceil(items.length / 2) * 2,
					repeat = settings.loop && items.length ? settings.rewind ? view : Math.max(view, size) : 0,
					append = '',
					prepend = '';
	
				repeat /= 2;
	
				while (repeat--) {
					clones.push(this.normalize(clones.length / 2, true));
					append = append + items[clones[clones.length - 1]][0].outerHTML;
					clones.push(this.normalize(items.length - 1 - (clones.length - 1) / 2, true));
					prepend = items[clones[clones.length - 1]][0].outerHTML + prepend;
				}
	
				this._clones = clones;
	
				$(append).addClass('cloned').appendTo(this.$stage);
				$(prepend).addClass('cloned').prependTo(this.$stage);
			}
		}, {
			filter: [ 'width', 'items', 'settings' ],
			run: function() {
				var rtl = this.settings.rtl ? 1 : -1,
					size = this._clones.length + this._items.length,
					iterator = -1,
					previous = 0,
					current = 0,
					coordinates = [];
	
				while (++iterator < size) {
					previous = coordinates[iterator - 1] || 0;
					current = this._widths[this.relative(iterator)] + this.settings.margin;
					coordinates.push(previous + current * rtl);
				}
	
				this._coordinates = coordinates;
			}
		}, {
			filter: [ 'width', 'items', 'settings' ],
			run: function() {
				var padding = this.settings.stagePadding,
					coordinates = this._coordinates,
					css = {
						'width': Math.ceil(Math.abs(coordinates[coordinates.length - 1])) + padding * 2,
						'padding-left': padding || '',
						'padding-right': padding || ''
					};
	
				this.$stage.css(css);
			}
		}, {
			filter: [ 'width', 'items', 'settings' ],
			run: function(cache) {
				var iterator = this._coordinates.length,
					grid = !this.settings.autoWidth,
					items = this.$stage.children();
	
				if (grid && cache.items.merge) {
					while (iterator--) {
						cache.css.width = this._widths[this.relative(iterator)];
						items.eq(iterator).css(cache.css);
					}
				} else if (grid) {
					cache.css.width = cache.items.width;
					items.css(cache.css);
				}
			}
		}, {
			filter: [ 'items' ],
			run: function() {
				this._coordinates.length < 1 && this.$stage.removeAttr('style');
			}
		}, {
			filter: [ 'width', 'items', 'settings' ],
			run: function(cache) {
				cache.current = cache.current ? this.$stage.children().index(cache.current) : 0;
				cache.current = Math.max(this.minimum(), Math.min(this.maximum(), cache.current));
				this.reset(cache.current);
			}
		}, {
			filter: [ 'position' ],
			run: function() {
				this.animate(this.coordinates(this._current));
			}
		}, {
			filter: [ 'width', 'position', 'items', 'settings' ],
			run: function() {
				var rtl = this.settings.rtl ? 1 : -1,
					padding = this.settings.stagePadding * 2,
					begin = this.coordinates(this.current()) + padding,
					end = begin + this.width() * rtl,
					inner, outer, matches = [], i, n;
	
				for (i = 0, n = this._coordinates.length; i < n; i++) {
					inner = this._coordinates[i - 1] || 0;
					outer = Math.abs(this._coordinates[i]) + padding * rtl;
	
					if ((this.op(inner, '<=', begin) && (this.op(inner, '>', end)))
						|| (this.op(outer, '<', begin) && this.op(outer, '>', end))) {
						matches.push(i);
					}
				}
	
				this.$stage.children('.active').removeClass('active');
				this.$stage.children(':eq(' + matches.join('), :eq(') + ')').addClass('active');
	
				if (this.settings.center) {
					this.$stage.children('.center').removeClass('center');
					this.$stage.children().eq(this.current()).addClass('center');
				}
			}
		} ];
	
		/**
		 * Initializes the carousel.
		 * @protected
		 */
		Owl.prototype.initialize = function() {
			this.enter('initializing');
			this.trigger('initialize');
	
			this.$element.toggleClass(this.settings.rtlClass, this.settings.rtl);
	
			if (this.settings.autoWidth && !this.is('pre-loading')) {
				var imgs, nestedSelector, width;
				imgs = this.$element.find('img');
				nestedSelector = this.settings.nestedItemSelector ? '.' + this.settings.nestedItemSelector : undefined;
				width = this.$element.children(nestedSelector).width();
	
				if (imgs.length && width <= 0) {
					this.preloadAutoWidthImages(imgs);
				}
			}
	
			this.$element.addClass(this.options.loadingClass);
	
			// create stage
			this.$stage = $('<' + this.settings.stageElement + ' class="' + this.settings.stageClass + '"/>')
				.wrap('<div class="' + this.settings.stageOuterClass + '"/>');
	
			// append stage
			this.$element.append(this.$stage.parent());
	
			// append content
			this.replace(this.$element.children().not(this.$stage.parent()));
	
			// check visibility
			if (this.$element.is(':visible')) {
				// update view
				this.refresh();
			} else {
				// invalidate width
				this.invalidate('width');
			}
	
			this.$element
				.removeClass(this.options.loadingClass)
				.addClass(this.options.loadedClass);
	
			// register event handlers
			this.registerEventHandlers();
	
			this.leave('initializing');
			this.trigger('initialized');
		};
	
		/**
		 * Setups the current settings.
		 * @todo Remove responsive classes. Why should adaptive designs be brought into IE8?
		 * @todo Support for media queries by using `matchMedia` would be nice.
		 * @public
		 */
		Owl.prototype.setup = function() {
			var viewport = this.viewport(),
				overwrites = this.options.responsive,
				match = -1,
				settings = null;
	
			if (!overwrites) {
				settings = $.extend({}, this.options);
			} else {
				$.each(overwrites, function(breakpoint) {
					if (breakpoint <= viewport && breakpoint > match) {
						match = Number(breakpoint);
					}
				});
	
				settings = $.extend({}, this.options, overwrites[match]);
				delete settings.responsive;
	
				// responsive class
				if (settings.responsiveClass) {
					this.$element.attr('class',
						this.$element.attr('class').replace(new RegExp('(' + this.options.responsiveClass + '-)\\S+\\s', 'g'), '$1' + match)
					);
				}
			}
	
			if (this.settings === null || this._breakpoint !== match) {
				this.trigger('change', { property: { name: 'settings', value: settings } });
				this._breakpoint = match;
				this.settings = settings;
				this.invalidate('settings');
				this.trigger('changed', { property: { name: 'settings', value: this.settings } });
			}
		};
	
		/**
		 * Updates option logic if necessery.
		 * @protected
		 */
		Owl.prototype.optionsLogic = function() {
			if (this.settings.autoWidth) {
				this.settings.stagePadding = false;
				this.settings.merge = false;
			}
		};
	
		/**
		 * Prepares an item before add.
		 * @todo Rename event parameter `content` to `item`.
		 * @protected
		 * @returns {jQuery|HTMLElement} - The item container.
		 */
		Owl.prototype.prepare = function(item) {
			var event = this.trigger('prepare', { content: item });
	
			if (!event.data) {
				event.data = $('<' + this.settings.itemElement + '/>')
					.addClass(this.options.itemClass).append(item)
			}
	
			this.trigger('prepared', { content: event.data });
	
			return event.data;
		};
	
		/**
		 * Updates the view.
		 * @public
		 */
		Owl.prototype.update = function() {
			var i = 0,
				n = this._pipe.length,
				filter = $.proxy(function(p) { return this[p] }, this._invalidated),
				cache = {};
	
			while (i < n) {
				if (this._invalidated.all || $.grep(this._pipe[i].filter, filter).length > 0) {
					this._pipe[i].run(cache);
				}
				i++;
			}
	
			this._invalidated = {};
	
			!this.is('valid') && this.enter('valid');
		};
	
		/**
		 * Gets the width of the view.
		 * @public
		 * @param {Owl.Width} [dimension=Owl.Width.Default] - The dimension to return.
		 * @returns {Number} - The width of the view in pixel.
		 */
		Owl.prototype.width = function(dimension) {
			dimension = dimension || Owl.Width.Default;
			switch (dimension) {
				case Owl.Width.Inner:
				case Owl.Width.Outer:
					return this._width;
				default:
					return this._width - this.settings.stagePadding * 2 + this.settings.margin;
			}
		};
	
		/**
		 * Refreshes the carousel primarily for adaptive purposes.
		 * @public
		 */
		Owl.prototype.refresh = function() {
			this.enter('refreshing');
			this.trigger('refresh');
	
			this.setup();
	
			this.optionsLogic();
	
			this.$element.addClass(this.options.refreshClass);
	
			this.update();
	
			this.$element.removeClass(this.options.refreshClass);
	
			this.leave('refreshing');
			this.trigger('refreshed');
		};
	
		/**
		 * Checks window `resize` event.
		 * @protected
		 */
		Owl.prototype.onThrottledResize = function() {
			window.clearTimeout(this.resizeTimer);
			this.resizeTimer = window.setTimeout(this._handlers.onResize, this.settings.responsiveRefreshRate);
		};
	
		/**
		 * Checks window `resize` event.
		 * @protected
		 */
		Owl.prototype.onResize = function() {
			if (!this._items.length) {
				return false;
			}
	
			if (this._width === this.$element.width()) {
				return false;
			}
	
			if (!this.$element.is(':visible')) {
				return false;
			}
	
			this.enter('resizing');
	
			if (this.trigger('resize').isDefaultPrevented()) {
				this.leave('resizing');
				return false;
			}
	
			this.invalidate('width');
	
			this.refresh();
	
			this.leave('resizing');
			this.trigger('resized');
		};
	
		/**
		 * Registers event handlers.
		 * @todo Check `msPointerEnabled`
		 * @todo #261
		 * @protected
		 */
		Owl.prototype.registerEventHandlers = function() {
			if ($.support.transition) {
				this.$stage.on($.support.transition.end + '.owl.core', $.proxy(this.onTransitionEnd, this));
			}
	
			if (this.settings.responsive !== false) {
				this.on(window, 'resize', this._handlers.onThrottledResize);
			}
	
			if (this.settings.mouseDrag) {
				this.$element.addClass(this.options.dragClass);
				this.$stage.on('mousedown.owl.core', $.proxy(this.onDragStart, this));
				this.$stage.on('dragstart.owl.core selectstart.owl.core', function() { return false });
			}
	
			if (this.settings.touchDrag){
				this.$stage.on('touchstart.owl.core', $.proxy(this.onDragStart, this));
				this.$stage.on('touchcancel.owl.core', $.proxy(this.onDragEnd, this));
			}
		};
	
		/**
		 * Handles `touchstart` and `mousedown` events.
		 * @todo Horizontal swipe threshold as option
		 * @todo #261
		 * @protected
		 * @param {Event} event - The event arguments.
		 */
		Owl.prototype.onDragStart = function(event) {
			var stage = null;
	
			if (event.which === 3) {
				return;
			}
	
			if ($.support.transform) {
				stage = this.$stage.css('transform').replace(/.*\(|\)| /g, '').split(',');
				stage = {
					x: stage[stage.length === 16 ? 12 : 4],
					y: stage[stage.length === 16 ? 13 : 5]
				};
			} else {
				stage = this.$stage.position();
				stage = {
					x: this.settings.rtl ?
						stage.left + this.$stage.width() - this.width() + this.settings.margin :
						stage.left,
					y: stage.top
				};
			}
	
			if (this.is('animating')) {
				$.support.transform ? this.animate(stage.x) : this.$stage.stop()
				this.invalidate('position');
			}
	
			this.$element.toggleClass(this.options.grabClass, event.type === 'mousedown');
	
			this.speed(0);
	
			this._drag.time = new Date().getTime();
			this._drag.target = $(event.target);
			this._drag.stage.start = stage;
			this._drag.stage.current = stage;
			this._drag.pointer = this.pointer(event);
	
			$(document).on('mouseup.owl.core touchend.owl.core', $.proxy(this.onDragEnd, this));
	
			$(document).one('mousemove.owl.core touchmove.owl.core', $.proxy(function(event) {
				var delta = this.difference(this._drag.pointer, this.pointer(event));
	
				$(document).on('mousemove.owl.core touchmove.owl.core', $.proxy(this.onDragMove, this));
	
				if (Math.abs(delta.x) < Math.abs(delta.y) && this.is('valid')) {
					return;
				}
	
				event.preventDefault();
	
				this.enter('dragging');
				this.trigger('drag');
			}, this));
		};
	
		/**
		 * Handles the `touchmove` and `mousemove` events.
		 * @todo #261
		 * @protected
		 * @param {Event} event - The event arguments.
		 */
		Owl.prototype.onDragMove = function(event) {
			var minimum = null,
				maximum = null,
				pull = null,
				delta = this.difference(this._drag.pointer, this.pointer(event)),
				stage = this.difference(this._drag.stage.start, delta);
	
			if (!this.is('dragging')) {
				return;
			}
	
			event.preventDefault();
	
			if (this.settings.loop) {
				minimum = this.coordinates(this.minimum());
				maximum = this.coordinates(this.maximum() + 1) - minimum;
				stage.x = (((stage.x - minimum) % maximum + maximum) % maximum) + minimum;
			} else {
				minimum = this.settings.rtl ? this.coordinates(this.maximum()) : this.coordinates(this.minimum());
				maximum = this.settings.rtl ? this.coordinates(this.minimum()) : this.coordinates(this.maximum());
				pull = this.settings.pullDrag ? -1 * delta.x / 5 : 0;
				stage.x = Math.max(Math.min(stage.x, minimum + pull), maximum + pull);
			}
	
			this._drag.stage.current = stage;
	
			this.animate(stage.x);
		};
	
		/**
		 * Handles the `touchend` and `mouseup` events.
		 * @todo #261
		 * @todo Threshold for click event
		 * @protected
		 * @param {Event} event - The event arguments.
		 */
		Owl.prototype.onDragEnd = function(event) {
			var delta = this.difference(this._drag.pointer, this.pointer(event)),
				stage = this._drag.stage.current,
				direction = delta.x > 0 ^ this.settings.rtl ? 'left' : 'right';
	
			$(document).off('.owl.core');
	
			this.$element.removeClass(this.options.grabClass);
	
			if (delta.x !== 0 && this.is('dragging') || !this.is('valid')) {
				this.speed(this.settings.dragEndSpeed || this.settings.smartSpeed);
				this.current(this.closest(stage.x, delta.x !== 0 ? direction : this._drag.direction));
				this.invalidate('position');
				this.update();
	
				this._drag.direction = direction;
	
				if (Math.abs(delta.x) > 3 || new Date().getTime() - this._drag.time > 300) {
					this._drag.target.one('click.owl.core', function() { return false; });
				}
			}
	
			if (!this.is('dragging')) {
				return;
			}
	
			this.leave('dragging');
			this.trigger('dragged');
		};
	
		/**
		 * Gets absolute position of the closest item for a coordinate.
		 * @todo Setting `freeDrag` makes `closest` not reusable. See #165.
		 * @protected
		 * @param {Number} coordinate - The coordinate in pixel.
		 * @param {String} direction - The direction to check for the closest item. Ether `left` or `right`.
		 * @return {Number} - The absolute position of the closest item.
		 */
		Owl.prototype.closest = function(coordinate, direction) {
			var position = -1,
				pull = 30,
				width = this.width(),
				coordinates = this.coordinates();
	
			if (!this.settings.freeDrag) {
				// check closest item
				$.each(coordinates, $.proxy(function(index, value) {
					// on a left pull, check on current index
					if (direction === 'left' && coordinate > value - pull && coordinate < value + pull) {
						position = index;
					// on a right pull, check on previous index
					// to do so, subtract width from value and set position = index + 1
					} else if (direction === 'right' && coordinate > value - width - pull && coordinate < value - width + pull) {
						position = index + 1;
					} else if (this.op(coordinate, '<', value)
						&& this.op(coordinate, '>', coordinates[index + 1] || value - width)) {
						position = direction === 'left' ? index + 1 : index;
					}
					return position === -1;
				}, this));
			}
	
			if (!this.settings.loop) {
				// non loop boundries
				if (this.op(coordinate, '>', coordinates[this.minimum()])) {
					position = coordinate = this.minimum();
				} else if (this.op(coordinate, '<', coordinates[this.maximum()])) {
					position = coordinate = this.maximum();
				}
			}
	
			return position;
		};
	
		/**
		 * Animates the stage.
		 * @todo #270
		 * @public
		 * @param {Number} coordinate - The coordinate in pixels.
		 */
		Owl.prototype.animate = function(coordinate) {
			var animate = this.speed() > 0;
	
			this.is('animating') && this.onTransitionEnd();
	
			if (animate) {
				this.enter('animating');
				this.trigger('translate');
			}
	
			if ($.support.transform3d && $.support.transition) {
				this.$stage.css({
					transform: 'translate3d(' + coordinate + 'px,0px,0px)',
					transition: (this.speed() / 1000) + 's'
				});
			} else if (animate) {
				this.$stage.animate({
					left: coordinate + 'px'
				}, this.speed(), this.settings.fallbackEasing, $.proxy(this.onTransitionEnd, this));
			} else {
				this.$stage.css({
					left: coordinate + 'px'
				});
			}
		};
	
		/**
		 * Checks whether the carousel is in a specific state or not.
		 * @param {String} state - The state to check.
		 * @returns {Boolean} - The flag which indicates if the carousel is busy.
		 */
		Owl.prototype.is = function(state) {
			return this._states.current[state] && this._states.current[state] > 0;
		};
	
		/**
		 * Sets the absolute position of the current item.
		 * @public
		 * @param {Number} [position] - The new absolute position or nothing to leave it unchanged.
		 * @returns {Number} - The absolute position of the current item.
		 */
		Owl.prototype.current = function(position) {
			if (position === undefined) {
				return this._current;
			}
	
			if (this._items.length === 0) {
				return undefined;
			}
	
			position = this.normalize(position);
	
			if (this._current !== position) {
				var event = this.trigger('change', { property: { name: 'position', value: position } });
	
				if (event.data !== undefined) {
					position = this.normalize(event.data);
				}
	
				this._current = position;
	
				this.invalidate('position');
	
				this.trigger('changed', { property: { name: 'position', value: this._current } });
			}
	
			return this._current;
		};
	
		/**
		 * Invalidates the given part of the update routine.
		 * @param {String} [part] - The part to invalidate.
		 * @returns {Array.<String>} - The invalidated parts.
		 */
		Owl.prototype.invalidate = function(part) {
			if ($.type(part) === 'string') {
				this._invalidated[part] = true;
				this.is('valid') && this.leave('valid');
			}
			return $.map(this._invalidated, function(v, i) { return i });
		};
	
		/**
		 * Resets the absolute position of the current item.
		 * @public
		 * @param {Number} position - The absolute position of the new item.
		 */
		Owl.prototype.reset = function(position) {
			position = this.normalize(position);
	
			if (position === undefined) {
				return;
			}
	
			this._speed = 0;
			this._current = position;
	
			this.suppress([ 'translate', 'translated' ]);
	
			this.animate(this.coordinates(position));
	
			this.release([ 'translate', 'translated' ]);
		};
	
		/**
		 * Normalizes an absolute or a relative position of an item.
		 * @public
		 * @param {Number} position - The absolute or relative position to normalize.
		 * @param {Boolean} [relative=false] - Whether the given position is relative or not.
		 * @returns {Number} - The normalized position.
		 */
		Owl.prototype.normalize = function(position, relative) {
			var n = this._items.length,
				m = relative ? 0 : this._clones.length;
	
			if (!this.isNumeric(position) || n < 1) {
				position = undefined;
			} else if (position < 0 || position >= n + m) {
				position = ((position - m / 2) % n + n) % n + m / 2;
			}
	
			return position;
		};
	
		/**
		 * Converts an absolute position of an item into a relative one.
		 * @public
		 * @param {Number} position - The absolute position to convert.
		 * @returns {Number} - The converted position.
		 */
		Owl.prototype.relative = function(position) {
			position -= this._clones.length / 2;
			return this.normalize(position, true);
		};
	
		/**
		 * Gets the maximum position for the current item.
		 * @public
		 * @param {Boolean} [relative=false] - Whether to return an absolute position or a relative position.
		 * @returns {Number}
		 */
		Owl.prototype.maximum = function(relative) {
			var settings = this.settings,
				maximum = this._coordinates.length,
				boundary = Math.abs(this._coordinates[maximum - 1]) - this._width,
				i = -1, j;
	
			if (settings.loop) {
				maximum = this._clones.length / 2 + this._items.length - 1;
			} else if (settings.autoWidth || settings.merge) {
				// binary search
				while (maximum - i > 1) {
					Math.abs(this._coordinates[j = maximum + i >> 1]) < boundary
						? i = j : maximum = j;
				}
			} else if (settings.center) {
				maximum = this._items.length - 1;
			} else {
				maximum = this._items.length - settings.items;
			}
	
			if (relative) {
				maximum -= this._clones.length / 2;
			}
	
			return Math.max(maximum, 0);
		};
	
		/**
		 * Gets the minimum position for the current item.
		 * @public
		 * @param {Boolean} [relative=false] - Whether to return an absolute position or a relative position.
		 * @returns {Number}
		 */
		Owl.prototype.minimum = function(relative) {
			return relative ? 0 : this._clones.length / 2;
		};
	
		/**
		 * Gets an item at the specified relative position.
		 * @public
		 * @param {Number} [position] - The relative position of the item.
		 * @return {jQuery|Array.<jQuery>} - The item at the given position or all items if no position was given.
		 */
		Owl.prototype.items = function(position) {
			if (position === undefined) {
				return this._items.slice();
			}
	
			position = this.normalize(position, true);
			return this._items[position];
		};
	
		/**
		 * Gets an item at the specified relative position.
		 * @public
		 * @param {Number} [position] - The relative position of the item.
		 * @return {jQuery|Array.<jQuery>} - The item at the given position or all items if no position was given.
		 */
		Owl.prototype.mergers = function(position) {
			if (position === undefined) {
				return this._mergers.slice();
			}
	
			position = this.normalize(position, true);
			return this._mergers[position];
		};
	
		/**
		 * Gets the absolute positions of clones for an item.
		 * @public
		 * @param {Number} [position] - The relative position of the item.
		 * @returns {Array.<Number>} - The absolute positions of clones for the item or all if no position was given.
		 */
		Owl.prototype.clones = function(position) {
			var odd = this._clones.length / 2,
				even = odd + this._items.length,
				map = function(index) { return index % 2 === 0 ? even + index / 2 : odd - (index + 1) / 2 };
	
			if (position === undefined) {
				return $.map(this._clones, function(v, i) { return map(i) });
			}
	
			return $.map(this._clones, function(v, i) { return v === position ? map(i) : null });
		};
	
		/**
		 * Sets the current animation speed.
		 * @public
		 * @param {Number} [speed] - The animation speed in milliseconds or nothing to leave it unchanged.
		 * @returns {Number} - The current animation speed in milliseconds.
		 */
		Owl.prototype.speed = function(speed) {
			if (speed !== undefined) {
				this._speed = speed;
			}
	
			return this._speed;
		};
	
		/**
		 * Gets the coordinate of an item.
		 * @todo The name of this method is missleanding.
		 * @public
		 * @param {Number} position - The absolute position of the item within `minimum()` and `maximum()`.
		 * @returns {Number|Array.<Number>} - The coordinate of the item in pixel or all coordinates.
		 */
		Owl.prototype.coordinates = function(position) {
			var multiplier = 1,
				newPosition = position - 1,
				coordinate;
	
			if (position === undefined) {
				return $.map(this._coordinates, $.proxy(function(coordinate, index) {
					return this.coordinates(index);
				}, this));
			}
	
			if (this.settings.center) {
				if (this.settings.rtl) {
					multiplier = -1;
					newPosition = position + 1;
				}
	
				coordinate = this._coordinates[position];
				coordinate += (this.width() - coordinate + (this._coordinates[newPosition] || 0)) / 2 * multiplier;
			} else {
				coordinate = this._coordinates[newPosition] || 0;
			}
	
			coordinate = Math.ceil(coordinate);
	
			return coordinate;
		};
	
		/**
		 * Calculates the speed for a translation.
		 * @protected
		 * @param {Number} from - The absolute position of the start item.
		 * @param {Number} to - The absolute position of the target item.
		 * @param {Number} [factor=undefined] - The time factor in milliseconds.
		 * @returns {Number} - The time in milliseconds for the translation.
		 */
		Owl.prototype.duration = function(from, to, factor) {
			if (factor === 0) {
				return 0;
			}
	
			return Math.min(Math.max(Math.abs(to - from), 1), 6) * Math.abs((factor || this.settings.smartSpeed));
		};
	
		/**
		 * Slides to the specified item.
		 * @public
		 * @param {Number} position - The position of the item.
		 * @param {Number} [speed] - The time in milliseconds for the transition.
		 */
		Owl.prototype.to = function(position, speed) {
			var current = this.current(),
				revert = null,
				distance = position - this.relative(current),
				direction = (distance > 0) - (distance < 0),
				items = this._items.length,
				minimum = this.minimum(),
				maximum = this.maximum();
	
			if (this.settings.loop) {
				if (!this.settings.rewind && Math.abs(distance) > items / 2) {
					distance += direction * -1 * items;
				}
	
				position = current + distance;
				revert = ((position - minimum) % items + items) % items + minimum;
	
				if (revert !== position && revert - distance <= maximum && revert - distance > 0) {
					current = revert - distance;
					position = revert;
					this.reset(current);
				}
			} else if (this.settings.rewind) {
				maximum += 1;
				position = (position % maximum + maximum) % maximum;
			} else {
				position = Math.max(minimum, Math.min(maximum, position));
			}
	
			this.speed(this.duration(current, position, speed));
			this.current(position);
	
			if (this.$element.is(':visible')) {
				this.update();
			}
		};
	
		/**
		 * Slides to the next item.
		 * @public
		 * @param {Number} [speed] - The time in milliseconds for the transition.
		 */
		Owl.prototype.next = function(speed) {
			speed = speed || false;
			this.to(this.relative(this.current()) + 1, speed);
		};
	
		/**
		 * Slides to the previous item.
		 * @public
		 * @param {Number} [speed] - The time in milliseconds for the transition.
		 */
		Owl.prototype.prev = function(speed) {
			speed = speed || false;
			this.to(this.relative(this.current()) - 1, speed);
		};
	
		/**
		 * Handles the end of an animation.
		 * @protected
		 * @param {Event} event - The event arguments.
		 */
		Owl.prototype.onTransitionEnd = function(event) {
	
			// if css2 animation then event object is undefined
			if (event !== undefined) {
				event.stopPropagation();
	
				// Catch only owl-stage transitionEnd event
				if ((event.target || event.srcElement || event.originalTarget) !== this.$stage.get(0)) {
					return false;
				}
			}
	
			this.leave('animating');
			this.trigger('translated');
		};
	
		/**
		 * Gets viewport width.
		 * @protected
		 * @return {Number} - The width in pixel.
		 */
		Owl.prototype.viewport = function() {
			var width;
			if (this.options.responsiveBaseElement !== window) {
				width = $(this.options.responsiveBaseElement).width();
			} else if (window.innerWidth) {
				width = window.innerWidth;
			} else if (document.documentElement && document.documentElement.clientWidth) {
				width = document.documentElement.clientWidth;
			} else {
				throw 'Can not detect viewport width.';
			}
			return width;
		};
	
		/**
		 * Replaces the current content.
		 * @public
		 * @param {HTMLElement|jQuery|String} content - The new content.
		 */
		Owl.prototype.replace = function(content) {
			this.$stage.empty();
			this._items = [];
	
			if (content) {
				content = (content instanceof jQuery) ? content : $(content);
			}
	
			if (this.settings.nestedItemSelector) {
				content = content.find('.' + this.settings.nestedItemSelector);
			}
	
			content.filter(function() {
				return this.nodeType === 1;
			}).each($.proxy(function(index, item) {
				item = this.prepare(item);
				this.$stage.append(item);
				this._items.push(item);
				this._mergers.push(item.find('[data-merge]').addBack('[data-merge]').attr('data-merge') * 1 || 1);
			}, this));
	
			this.reset(this.isNumeric(this.settings.startPosition) ? this.settings.startPosition : 0);
	
			this.invalidate('items');
		};
	
		/**
		 * Adds an item.
		 * @todo Use `item` instead of `content` for the event arguments.
		 * @public
		 * @param {HTMLElement|jQuery|String} content - The item content to add.
		 * @param {Number} [position] - The relative position at which to insert the item otherwise the item will be added to the end.
		 */
		Owl.prototype.add = function(content, position) {
			var current = this.relative(this._current);
	
			position = position === undefined ? this._items.length : this.normalize(position, true);
			content = content instanceof jQuery ? content : $(content);
	
			this.trigger('add', { content: content, position: position });
	
			content = this.prepare(content);
	
			if (this._items.length === 0 || position === this._items.length) {
				this._items.length === 0 && this.$stage.append(content);
				this._items.length !== 0 && this._items[position - 1].after(content);
				this._items.push(content);
				this._mergers.push(content.find('[data-merge]').andSelf('[data-merge]').attr('data-merge') * 1 || 1);
			} else {
				this._items[position].before(content);
				this._items.splice(position, 0, content);
				this._mergers.splice(position, 0, content.find('[data-merge]').andSelf('[data-merge]').attr('data-merge') * 1 || 1);
			}
	
			this._items[current] && this.reset(this._items[current].index());
	
			this.invalidate('items');
	
			this.trigger('added', { content: content, position: position });
		};
	
		/**
		 * Removes an item by its position.
		 * @todo Use `item` instead of `content` for the event arguments.
		 * @public
		 * @param {Number} position - The relative position of the item to remove.
		 */
		Owl.prototype.remove = function(position) {
			position = this.normalize(position, true);
	
			if (position === undefined) {
				return;
			}
	
			this.trigger('remove', { content: this._items[position], position: position });
	
			this._items[position].remove();
			this._items.splice(position, 1);
			this._mergers.splice(position, 1);
	
			this.invalidate('items');
	
			this.trigger('removed', { content: null, position: position });
		};
	
		/**
		 * Preloads images with auto width.
		 * @todo Replace by a more generic approach
		 * @protected
		 */
		Owl.prototype.preloadAutoWidthImages = function(images) {
			images.each($.proxy(function(i, element) {
				this.enter('pre-loading');
				element = $(element);
				$(new Image()).one('load', $.proxy(function(e) {
					element.attr('src', e.target.src);
					element.css('opacity', 1);
					this.leave('pre-loading');
					!this.is('pre-loading') && !this.is('initializing') && this.refresh();
				}, this)).attr('src', element.attr('src') || element.attr('data-src') || element.attr('data-src-retina'));
			}, this));
		};
	
		/**
		 * Destroys the carousel.
		 * @public
		 */
		Owl.prototype.destroy = function() {
	
			this.$element.off('.owl.core');
			this.$stage.off('.owl.core');
			$(document).off('.owl.core');
	
			if (this.settings.responsive !== false) {
				window.clearTimeout(this.resizeTimer);
				this.off(window, 'resize', this._handlers.onThrottledResize);
			}
	
			for (var i in this._plugins) {
				this._plugins[i].destroy();
			}
	
			this.$stage.children('.cloned').remove();
	
			this.$stage.unwrap();
			this.$stage.children().contents().unwrap();
			this.$stage.children().unwrap();
	
			this.$element
				.removeClass(this.options.refreshClass)
				.removeClass(this.options.loadingClass)
				.removeClass(this.options.loadedClass)
				.removeClass(this.options.rtlClass)
				.removeClass(this.options.dragClass)
				.removeClass(this.options.grabClass)
				.attr('class', this.$element.attr('class').replace(new RegExp(this.options.responsiveClass + '-\\S+\\s', 'g'), ''))
				.removeData('owl.carousel');
		};
	
		/**
		 * Operators to calculate right-to-left and left-to-right.
		 * @protected
		 * @param {Number} [a] - The left side operand.
		 * @param {String} [o] - The operator.
		 * @param {Number} [b] - The right side operand.
		 */
		Owl.prototype.op = function(a, o, b) {
			var rtl = this.settings.rtl;
			switch (o) {
				case '<':
					return rtl ? a > b : a < b;
				case '>':
					return rtl ? a < b : a > b;
				case '>=':
					return rtl ? a <= b : a >= b;
				case '<=':
					return rtl ? a >= b : a <= b;
				default:
					break;
			}
		};
	
		/**
		 * Attaches to an internal event.
		 * @protected
		 * @param {HTMLElement} element - The event source.
		 * @param {String} event - The event name.
		 * @param {Function} listener - The event handler to attach.
		 * @param {Boolean} capture - Wether the event should be handled at the capturing phase or not.
		 */
		Owl.prototype.on = function(element, event, listener, capture) {
			if (element.addEventListener) {
				element.addEventListener(event, listener, capture);
			} else if (element.attachEvent) {
				element.attachEvent('on' + event, listener);
			}
		};
	
		/**
		 * Detaches from an internal event.
		 * @protected
		 * @param {HTMLElement} element - The event source.
		 * @param {String} event - The event name.
		 * @param {Function} listener - The attached event handler to detach.
		 * @param {Boolean} capture - Wether the attached event handler was registered as a capturing listener or not.
		 */
		Owl.prototype.off = function(element, event, listener, capture) {
			if (element.removeEventListener) {
				element.removeEventListener(event, listener, capture);
			} else if (element.detachEvent) {
				element.detachEvent('on' + event, listener);
			}
		};
	
		/**
		 * Triggers a public event.
		 * @todo Remove `status`, `relatedTarget` should be used instead.
		 * @protected
		 * @param {String} name - The event name.
		 * @param {*} [data=null] - The event data.
		 * @param {String} [namespace=carousel] - The event namespace.
		 * @param {String} [state] - The state which is associated with the event.
		 * @param {Boolean} [enter=false] - Indicates if the call enters the specified state or not.
		 * @returns {Event} - The event arguments.
		 */
		Owl.prototype.trigger = function(name, data, namespace, state, enter) {
			var status = {
				item: { count: this._items.length, index: this.current() }
			}, handler = $.camelCase(
				$.grep([ 'on', name, namespace ], function(v) { return v })
					.join('-').toLowerCase()
			), event = $.Event(
				[ name, 'owl', namespace || 'carousel' ].join('.').toLowerCase(),
				$.extend({ relatedTarget: this }, status, data)
			);
	
			if (!this._supress[name]) {
				$.each(this._plugins, function(name, plugin) {
					if (plugin.onTrigger) {
						plugin.onTrigger(event);
					}
				});
	
				this.register({ type: Owl.Type.Event, name: name });
				this.$element.trigger(event);
	
				if (this.settings && typeof this.settings[handler] === 'function') {
					this.settings[handler].call(this, event);
				}
			}
	
			return event;
		};
	
		/**
		 * Enters a state.
		 * @param name - The state name.
		 */
		Owl.prototype.enter = function(name) {
			$.each([ name ].concat(this._states.tags[name] || []), $.proxy(function(i, name) {
				if (this._states.current[name] === undefined) {
					this._states.current[name] = 0;
				}
	
				this._states.current[name]++;
			}, this));
		};
	
		/**
		 * Leaves a state.
		 * @param name - The state name.
		 */
		Owl.prototype.leave = function(name) {
			$.each([ name ].concat(this._states.tags[name] || []), $.proxy(function(i, name) {
				this._states.current[name]--;
			}, this));
		};
	
		/**
		 * Registers an event or state.
		 * @public
		 * @param {Object} object - The event or state to register.
		 */
		Owl.prototype.register = function(object) {
			if (object.type === Owl.Type.Event) {
				if (!$.event.special[object.name]) {
					$.event.special[object.name] = {};
				}
	
				if (!$.event.special[object.name].owl) {
					var _default = $.event.special[object.name]._default;
					$.event.special[object.name]._default = function(e) {
						if (_default && _default.apply && (!e.namespace || e.namespace.indexOf('owl') === -1)) {
							return _default.apply(this, arguments);
						}
						return e.namespace && e.namespace.indexOf('owl') > -1;
					};
					$.event.special[object.name].owl = true;
				}
			} else if (object.type === Owl.Type.State) {
				if (!this._states.tags[object.name]) {
					this._states.tags[object.name] = object.tags;
				} else {
					this._states.tags[object.name] = this._states.tags[object.name].concat(object.tags);
				}
	
				this._states.tags[object.name] = $.grep(this._states.tags[object.name], $.proxy(function(tag, i) {
					return $.inArray(tag, this._states.tags[object.name]) === i;
				}, this));
			}
		};
	
		/**
		 * Suppresses events.
		 * @protected
		 * @param {Array.<String>} events - The events to suppress.
		 */
		Owl.prototype.suppress = function(events) {
			$.each(events, $.proxy(function(index, event) {
				this._supress[event] = true;
			}, this));
		};
	
		/**
		 * Releases suppressed events.
		 * @protected
		 * @param {Array.<String>} events - The events to release.
		 */
		Owl.prototype.release = function(events) {
			$.each(events, $.proxy(function(index, event) {
				delete this._supress[event];
			}, this));
		};
	
		/**
		 * Gets unified pointer coordinates from event.
		 * @todo #261
		 * @protected
		 * @param {Event} - The `mousedown` or `touchstart` event.
		 * @returns {Object} - Contains `x` and `y` coordinates of current pointer position.
		 */
		Owl.prototype.pointer = function(event) {
			var result = { x: null, y: null };
	
			event = event.originalEvent || event || window.event;
	
			event = event.touches && event.touches.length ?
				event.touches[0] : event.changedTouches && event.changedTouches.length ?
					event.changedTouches[0] : event;
	
			if (event.pageX) {
				result.x = event.pageX;
				result.y = event.pageY;
			} else {
				result.x = event.clientX;
				result.y = event.clientY;
			}
	
			return result;
		};
	
		/**
		 * Determines if the input is a Number or something that can be coerced to a Number
		 * @protected
		 * @param {Number|String|Object|Array|Boolean|RegExp|Function|Symbol} - The input to be tested
		 * @returns {Boolean} - An indication if the input is a Number or can be coerced to a Number
		 */
		Owl.prototype.isNumeric = function(number) {
			return !isNaN(parseFloat(number));
		};
	
		/**
		 * Gets the difference of two vectors.
		 * @todo #261
		 * @protected
		 * @param {Object} - The first vector.
		 * @param {Object} - The second vector.
		 * @returns {Object} - The difference.
		 */
		Owl.prototype.difference = function(first, second) {
			return {
				x: first.x - second.x,
				y: first.y - second.y
			};
		};
	
		/**
		 * The jQuery Plugin for the Owl Carousel
		 * @todo Navigation plugin `next` and `prev`
		 * @public
		 */
		$.fn.owlCarousel = function(option) {
			var args = Array.prototype.slice.call(arguments, 1);
	
			return this.each(function() {
				var $this = $(this),
					data = $this.data('owl.carousel');
	
				if (!data) {
					data = new Owl(this, typeof option == 'object' && option);
					$this.data('owl.carousel', data);
	
					$.each([
						'next', 'prev', 'to', 'destroy', 'refresh', 'replace', 'add', 'remove'
					], function(i, event) {
						data.register({ type: Owl.Type.Event, name: event });
						data.$element.on(event + '.owl.carousel.core', $.proxy(function(e) {
							if (e.namespace && e.relatedTarget !== this) {
								this.suppress([ event ]);
								data[event].apply(this, [].slice.call(arguments, 1));
								this.release([ event ]);
							}
						}, data));
					});
				}
	
				if (typeof option == 'string' && option.charAt(0) !== '_') {
					data[option].apply(data, args);
				}
			});
		};
	
		/**
		 * The constructor for the jQuery Plugin
		 * @public
		 */
		$.fn.owlCarousel.Constructor = Owl;
	
	})( jQuery, window, document);
	
	/**
	 * AutoRefresh Plugin
	 * @version 2.1.0
	 * @author Artus Kolanowski
	 * @author David Deutsch
	 * @license The MIT License (MIT)
	 */
	;(function($, window, document, undefined) {
	
		/**
		 * Creates the auto refresh plugin.
		 * @class The Auto Refresh Plugin
		 * @param {Owl} carousel - The Owl Carousel
		 */
		var AutoRefresh = function(carousel) {
			/**
			 * Reference to the core.
			 * @protected
			 * @type {Owl}
			 */
			this._core = carousel;
	
			/**
			 * Refresh interval.
			 * @protected
			 * @type {number}
			 */
			this._interval = null;
	
			/**
			 * Whether the element is currently visible or not.
			 * @protected
			 * @type {Boolean}
			 */
			this._visible = null;
	
			/**
			 * All event handlers.
			 * @protected
			 * @type {Object}
			 */
			this._handlers = {
				'initialized.owl.carousel': $.proxy(function(e) {
					if (e.namespace && this._core.settings.autoRefresh) {
						this.watch();
					}
				}, this)
			};
	
			// set default options
			this._core.options = $.extend({}, AutoRefresh.Defaults, this._core.options);
	
			// register event handlers
			this._core.$element.on(this._handlers);
		};
	
		/**
		 * Default options.
		 * @public
		 */
		AutoRefresh.Defaults = {
			autoRefresh: true,
			autoRefreshInterval: 500
		};
	
		/**
		 * Watches the element.
		 */
		AutoRefresh.prototype.watch = function() {
			if (this._interval) {
				return;
			}
	
			this._visible = this._core.$element.is(':visible');
			this._interval = window.setInterval($.proxy(this.refresh, this), this._core.settings.autoRefreshInterval);
		};
	
		/**
		 * Refreshes the element.
		 */
		AutoRefresh.prototype.refresh = function() {
			if (this._core.$element.is(':visible') === this._visible) {
				return;
			}
	
			this._visible = !this._visible;
	
			this._core.$element.toggleClass('owl-hidden', !this._visible);
	
			this._visible && (this._core.invalidate('width') && this._core.refresh());
		};
	
		/**
		 * Destroys the plugin.
		 */
		AutoRefresh.prototype.destroy = function() {
			var handler, property;
	
			window.clearInterval(this._interval);
	
			for (handler in this._handlers) {
				this._core.$element.off(handler, this._handlers[handler]);
			}
			for (property in Object.getOwnPropertyNames(this)) {
				typeof this[property] != 'function' && (this[property] = null);
			}
		};
	
		$.fn.owlCarousel.Constructor.Plugins.AutoRefresh = AutoRefresh;
	
	})( jQuery, window, document);
	
	/**
	 * Lazy Plugin
	 * @version 2.1.0
	 * @author Bartosz Wojciechowski
	 * @author David Deutsch
	 * @license The MIT License (MIT)
	 */
	;(function($, window, document, undefined) {
	
		/**
		 * Creates the lazy plugin.
		 * @class The Lazy Plugin
		 * @param {Owl} carousel - The Owl Carousel
		 */
		var Lazy = function(carousel) {
	
			/**
			 * Reference to the core.
			 * @protected
			 * @type {Owl}
			 */
			this._core = carousel;
	
			/**
			 * Already loaded items.
			 * @protected
			 * @type {Array.<jQuery>}
			 */
			this._loaded = [];
	
			/**
			 * Event handlers.
			 * @protected
			 * @type {Object}
			 */
			this._handlers = {
				'initialized.owl.carousel change.owl.carousel resized.owl.carousel': $.proxy(function(e) {
					if (!e.namespace) {
						return;
					}
	
					if (!this._core.settings || !this._core.settings.lazyLoad) {
						return;
					}
	
					if ((e.property && e.property.name == 'position') || e.type == 'initialized') {
						var settings = this._core.settings,
							n = (settings.center && Math.ceil(settings.items / 2) || settings.items),
							i = ((settings.center && n * -1) || 0),
							position = (e.property && e.property.value !== undefined ? e.property.value : this._core.current()) + i,
							clones = this._core.clones().length,
							load = $.proxy(function(i, v) { this.load(v) }, this);
	
						while (i++ < n) {
							this.load(clones / 2 + this._core.relative(position));
							clones && $.each(this._core.clones(this._core.relative(position)), load);
							position++;
						}
					}
				}, this)
			};
	
			// set the default options
			this._core.options = $.extend({}, Lazy.Defaults, this._core.options);
	
			// register event handler
			this._core.$element.on(this._handlers);
		};
	
		/**
		 * Default options.
		 * @public
		 */
		Lazy.Defaults = {
			lazyLoad: false
		};
	
		/**
		 * Loads all resources of an item at the specified position.
		 * @param {Number} position - The absolute position of the item.
		 * @protected
		 */
		Lazy.prototype.load = function(position) {
			var $item = this._core.$stage.children().eq(position),
				$elements = $item && $item.find('.owl-lazy');
	
			if (!$elements || $.inArray($item.get(0), this._loaded) > -1) {
				return;
			}
	
			$elements.each($.proxy(function(index, element) {
				var $element = $(element), image,
					url = (window.devicePixelRatio > 1 && $element.attr('data-src-retina')) || $element.attr('data-src');
	
				this._core.trigger('load', { element: $element, url: url }, 'lazy');
	
				if ($element.is('img')) {
					$element.one('load.owl.lazy', $.proxy(function() {
						$element.css('opacity', 1);
						this._core.trigger('loaded', { element: $element, url: url }, 'lazy');
					}, this)).attr('src', url);
				} else {
					image = new Image();
					image.onload = $.proxy(function() {
						$element.css({
							'background-image': 'url(' + url + ')',
							'opacity': '1'
						});
						this._core.trigger('loaded', { element: $element, url: url }, 'lazy');
					}, this);
					image.src = url;
				}
			}, this));
	
			this._loaded.push($item.get(0));
		};
	
		/**
		 * Destroys the plugin.
		 * @public
		 */
		Lazy.prototype.destroy = function() {
			var handler, property;
	
			for (handler in this.handlers) {
				this._core.$element.off(handler, this.handlers[handler]);
			}
			for (property in Object.getOwnPropertyNames(this)) {
				typeof this[property] != 'function' && (this[property] = null);
			}
		};
	
		$.fn.owlCarousel.Constructor.Plugins.Lazy = Lazy;
	
	})( jQuery, window, document);
	
	/**
	 * AutoHeight Plugin
	 * @version 2.1.0
	 * @author Bartosz Wojciechowski
	 * @author David Deutsch
	 * @license The MIT License (MIT)
	 */
	;(function($, window, document, undefined) {
	
		/**
		 * Creates the auto height plugin.
		 * @class The Auto Height Plugin
		 * @param {Owl} carousel - The Owl Carousel
		 */
		var AutoHeight = function(carousel) {
			/**
			 * Reference to the core.
			 * @protected
			 * @type {Owl}
			 */
			this._core = carousel;
	
			/**
			 * All event handlers.
			 * @protected
			 * @type {Object}
			 */
			this._handlers = {
				'initialized.owl.carousel refreshed.owl.carousel': $.proxy(function(e) {
					if (e.namespace && this._core.settings.autoHeight) {
						this.update();
					}
				}, this),
				'changed.owl.carousel': $.proxy(function(e) {
					if (e.namespace && this._core.settings.autoHeight && e.property.name == 'position'){
						this.update();
					}
				}, this),
				'loaded.owl.lazy': $.proxy(function(e) {
					if (e.namespace && this._core.settings.autoHeight
						&& e.element.closest('.' + this._core.settings.itemClass).index() === this._core.current()) {
						this.update();
					}
				}, this)
			};
	
			// set default options
			this._core.options = $.extend({}, AutoHeight.Defaults, this._core.options);
	
			// register event handlers
			this._core.$element.on(this._handlers);
		};
	
		/**
		 * Default options.
		 * @public
		 */
		AutoHeight.Defaults = {
			autoHeight: false,
			autoHeightClass: 'owl-height'
		};
	
		/**
		 * Updates the view.
		 */
		AutoHeight.prototype.update = function() {
			var start = this._core._current,
				end = start + this._core.settings.items,
				visible = this._core.$stage.children().toArray().slice(start, end),
				heights = [],
				maxheight = 0;
	
			$.each(visible, function(index, item) {
				heights.push($(item).height());
			});
	
			maxheight = Math.max.apply(null, heights);
	
			this._core.$stage.parent()
				.height(maxheight)
				.addClass(this._core.settings.autoHeightClass);
		};
	
		AutoHeight.prototype.destroy = function() {
			var handler, property;
	
			for (handler in this._handlers) {
				this._core.$element.off(handler, this._handlers[handler]);
			}
			for (property in Object.getOwnPropertyNames(this)) {
				typeof this[property] != 'function' && (this[property] = null);
			}
		};
	
		$.fn.owlCarousel.Constructor.Plugins.AutoHeight = AutoHeight;
	
	})( jQuery, window, document);
	
	/**
	 * Video Plugin
	 * @version 2.1.0
	 * @author Bartosz Wojciechowski
	 * @author David Deutsch
	 * @license The MIT License (MIT)
	 */
	;(function($, window, document, undefined) {
	
		/**
		 * Creates the video plugin.
		 * @class The Video Plugin
		 * @param {Owl} carousel - The Owl Carousel
		 */
		var Video = function(carousel) {
			/**
			 * Reference to the core.
			 * @protected
			 * @type {Owl}
			 */
			this._core = carousel;
	
			/**
			 * Cache all video URLs.
			 * @protected
			 * @type {Object}
			 */
			this._videos = {};
	
			/**
			 * Current playing item.
			 * @protected
			 * @type {jQuery}
			 */
			this._playing = null;
	
			/**
			 * All event handlers.
			 * @todo The cloned content removale is too late
			 * @protected
			 * @type {Object}
			 */
			this._handlers = {
				'initialized.owl.carousel': $.proxy(function(e) {
					if (e.namespace) {
						this._core.register({ type: 'state', name: 'playing', tags: [ 'interacting' ] });
					}
				}, this),
				'resize.owl.carousel': $.proxy(function(e) {
					if (e.namespace && this._core.settings.video && this.isInFullScreen()) {
						e.preventDefault();
					}
				}, this),
				'refreshed.owl.carousel': $.proxy(function(e) {
					if (e.namespace && this._core.is('resizing')) {
						this._core.$stage.find('.cloned .owl-video-frame').remove();
					}
				}, this),
				'changed.owl.carousel': $.proxy(function(e) {
					if (e.namespace && e.property.name === 'position' && this._playing) {
						this.stop();
					}
				}, this),
				'prepared.owl.carousel': $.proxy(function(e) {
					if (!e.namespace) {
						return;
					}
	
					var $element = $(e.content).find('.owl-video');
	
					if ($element.length) {
						$element.css('display', 'none');
						this.fetch($element, $(e.content));
					}
				}, this)
			};
	
			// set default options
			this._core.options = $.extend({}, Video.Defaults, this._core.options);
	
			// register event handlers
			this._core.$element.on(this._handlers);
	
			this._core.$element.on('click.owl.video', '.owl-video-play-icon', $.proxy(function(e) {
				this.play(e);
			}, this));
		};
	
		/**
		 * Default options.
		 * @public
		 */
		Video.Defaults = {
			video: false,
			videoHeight: false,
			videoWidth: false
		};
	
		/**
		 * Gets the video ID and the type (YouTube/Vimeo/vzaar only).
		 * @protected
		 * @param {jQuery} target - The target containing the video data.
		 * @param {jQuery} item - The item containing the video.
		 */
		Video.prototype.fetch = function(target, item) {
				var type = (function() {
						if (target.attr('data-vimeo-id')) {
							return 'vimeo';
						} else if (target.attr('data-vzaar-id')) {
							return 'vzaar'
						} else {
							return 'youtube';
						}
					})(),
					id = target.attr('data-vimeo-id') || target.attr('data-youtube-id') || target.attr('data-vzaar-id'),
					width = target.attr('data-width') || this._core.settings.videoWidth,
					height = target.attr('data-height') || this._core.settings.videoHeight,
					url = target.attr('href');
	
			if (url) {
	
				/*
						Parses the id's out of the following urls (and probably more):
						https://www.youtube.com/watch?v=:id
						https://youtu.be/:id
						https://vimeo.com/:id
						https://vimeo.com/channels/:channel/:id
						https://vimeo.com/groups/:group/videos/:id
						https://app.vzaar.com/videos/:id
	
						Visual example: https://regexper.com/#(http%3A%7Chttps%3A%7C)%5C%2F%5C%2F(player.%7Cwww.%7Capp.)%3F(vimeo%5C.com%7Cyoutu(be%5C.com%7C%5C.be%7Cbe%5C.googleapis%5C.com)%7Cvzaar%5C.com)%5C%2F(video%5C%2F%7Cvideos%5C%2F%7Cembed%5C%2F%7Cchannels%5C%2F.%2B%5C%2F%7Cgroups%5C%2F.%2B%5C%2F%7Cwatch%5C%3Fv%3D%7Cv%5C%2F)%3F(%5BA-Za-z0-9._%25-%5D*)(%5C%26%5CS%2B)%3F
				*/
	
				id = url.match(/(http:|https:|)\/\/(player.|www.|app.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com)|vzaar\.com)\/(video\/|videos\/|embed\/|channels\/.+\/|groups\/.+\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/);
	
				if (id[3].indexOf('youtu') > -1) {
					type = 'youtube';
				} else if (id[3].indexOf('vimeo') > -1) {
					type = 'vimeo';
				} else if (id[3].indexOf('vzaar') > -1) {
					type = 'vzaar';
				} else {
					throw new Error('Video URL not supported.');
				}
				id = id[6];
			} else {
				throw new Error('Missing video URL.');
			}
	
			this._videos[url] = {
				type: type,
				id: id,
				width: width,
				height: height
			};
	
			item.attr('data-video', url);
	
			this.thumbnail(target, this._videos[url]);
		};
	
		/**
		 * Creates video thumbnail.
		 * @protected
		 * @param {jQuery} target - The target containing the video data.
		 * @param {Object} info - The video info object.
		 * @see `fetch`
		 */
		Video.prototype.thumbnail = function(target, video) {
			var tnLink,
				icon,
				path,
				dimensions = video.width && video.height ? 'style="width:' + video.width + 'px;height:' + video.height + 'px;"' : '',
				customTn = target.find('img'),
				srcType = 'src',
				lazyClass = '',
				settings = this._core.settings,
				create = function(path) {
					icon = '<div class="owl-video-play-icon"></div>';
	
					if (settings.lazyLoad) {
						tnLink = '<div class="owl-video-tn ' + lazyClass + '" ' + srcType + '="' + path + '"></div>';
					} else {
						tnLink = '<div class="owl-video-tn" style="opacity:1;background-image:url(' + path + ')"></div>';
					}
					target.after(tnLink);
					target.after(icon);
				};
	
			// wrap video content into owl-video-wrapper div
			target.wrap('<div class="owl-video-wrapper"' + dimensions + '></div>');
	
			if (this._core.settings.lazyLoad) {
				srcType = 'data-src';
				lazyClass = 'owl-lazy';
			}
	
			// custom thumbnail
			if (customTn.length) {
				create(customTn.attr(srcType));
				customTn.remove();
				return false;
			}
	
			if (video.type === 'youtube') {
				path = "//img.youtube.com/vi/" + video.id + "/hqdefault.jpg";
				create(path);
			} else if (video.type === 'vimeo') {
				$.ajax({
					type: 'GET',
					url: '//vimeo.com/api/v2/video/' + video.id + '.json',
					jsonp: 'callback',
					dataType: 'jsonp',
					success: function(data) {
						path = data[0].thumbnail_large;
						create(path);
					}
				});
			} else if (video.type === 'vzaar') {
				$.ajax({
					type: 'GET',
					url: '//vzaar.com/api/videos/' + video.id + '.json',
					jsonp: 'callback',
					dataType: 'jsonp',
					success: function(data) {
						path = data.framegrab_url;
						create(path);
					}
				});
			}
		};
	
		/**
		 * Stops the current video.
		 * @public
		 */
		Video.prototype.stop = function() {
			this._core.trigger('stop', null, 'video');
			this._playing.find('.owl-video-frame').remove();
			this._playing.removeClass('owl-video-playing');
			this._playing = null;
			this._core.leave('playing');
			this._core.trigger('stopped', null, 'video');
		};
	
		/**
		 * Starts the current video.
		 * @public
		 * @param {Event} event - The event arguments.
		 */
		Video.prototype.play = function(event) {
			var target = $(event.target),
				item = target.closest('.' + this._core.settings.itemClass),
				video = this._videos[item.attr('data-video')],
				width = video.width || '100%',
				height = video.height || this._core.$stage.height(),
				html;
	
			if (this._playing) {
				return;
			}
	
			this._core.enter('playing');
			this._core.trigger('play', null, 'video');
	
			item = this._core.items(this._core.relative(item.index()));
	
			this._core.reset(item.index());
	
			if (video.type === 'youtube') {
				html = '<iframe width="' + width + '" height="' + height + '" src="//www.youtube.com/embed/' +
					video.id + '?autoplay=1&v=' + video.id + '" frameborder="0" allowfullscreen></iframe>';
			} else if (video.type === 'vimeo') {
				html = '<iframe src="//player.vimeo.com/video/' + video.id +
					'?autoplay=1" width="' + width + '" height="' + height +
					'" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
			} else if (video.type === 'vzaar') {
				html = '<iframe frameborder="0"' + 'height="' + height + '"' + 'width="' + width +
					'" allowfullscreen mozallowfullscreen webkitAllowFullScreen ' +
					'src="//view.vzaar.com/' + video.id + '/player?autoplay=true"></iframe>';
			}
	
			$('<div class="owl-video-frame">' + html + '</div>').insertAfter(item.find('.owl-video'));
	
			this._playing = item.addClass('owl-video-playing');
		};
	
		/**
		 * Checks whether an video is currently in full screen mode or not.
		 * @todo Bad style because looks like a readonly method but changes members.
		 * @protected
		 * @returns {Boolean}
		 */
		Video.prototype.isInFullScreen = function() {
			var element = document.fullscreenElement || document.mozFullScreenElement ||
					document.webkitFullscreenElement;
	
			return element && $(element).parent().hasClass('owl-video-frame');
		};
	
		/**
		 * Destroys the plugin.
		 */
		Video.prototype.destroy = function() {
			var handler, property;
	
			this._core.$element.off('click.owl.video');
	
			for (handler in this._handlers) {
				this._core.$element.off(handler, this._handlers[handler]);
			}
			for (property in Object.getOwnPropertyNames(this)) {
				typeof this[property] != 'function' && (this[property] = null);
			}
		};
	
		$.fn.owlCarousel.Constructor.Plugins.Video = Video;
	
	})( jQuery, window, document);
	
	/**
	 * Animate Plugin
	 * @version 2.1.0
	 * @author Bartosz Wojciechowski
	 * @author David Deutsch
	 * @license The MIT License (MIT)
	 */
	;(function($, window, document, undefined) {
	
		/**
		 * Creates the animate plugin.
		 * @class The Navigation Plugin
		 * @param {Owl} scope - The Owl Carousel
		 */
		var Animate = function(scope) {
			this.core = scope;
			this.core.options = $.extend({}, Animate.Defaults, this.core.options);
			this.swapping = true;
			this.previous = undefined;
			this.next = undefined;
	
			this.handlers = {
				'change.owl.carousel': $.proxy(function(e) {
					if (e.namespace && e.property.name == 'position') {
						this.previous = this.core.current();
						this.next = e.property.value;
					}
				}, this),
				'drag.owl.carousel dragged.owl.carousel translated.owl.carousel': $.proxy(function(e) {
					if (e.namespace) {
						this.swapping = e.type == 'translated';
					}
				}, this),
				'translate.owl.carousel': $.proxy(function(e) {
					if (e.namespace && this.swapping && (this.core.options.animateOut || this.core.options.animateIn)) {
						this.swap();
					}
				}, this)
			};
	
			this.core.$element.on(this.handlers);
		};
	
		/**
		 * Default options.
		 * @public
		 */
		Animate.Defaults = {
			animateOut: false,
			animateIn: false
		};
	
		/**
		 * Toggles the animation classes whenever an translations starts.
		 * @protected
		 * @returns {Boolean|undefined}
		 */
		Animate.prototype.swap = function() {
	
			if (this.core.settings.items !== 1) {
				return;
			}
	
			if (!$.support.animation || !$.support.transition) {
				return;
			}
	
			this.core.speed(0);
	
			var left,
				clear = $.proxy(this.clear, this),
				previous = this.core.$stage.children().eq(this.previous),
				next = this.core.$stage.children().eq(this.next),
				incoming = this.core.settings.animateIn,
				outgoing = this.core.settings.animateOut;
	
			if (this.core.current() === this.previous) {
				return;
			}
	
			if (outgoing) {
				left = this.core.coordinates(this.previous) - this.core.coordinates(this.next);
				previous.one($.support.animation.end, clear)
					.css( { 'left': left + 'px' } )
					.addClass('animated owl-animated-out')
					.addClass(outgoing);
			}
	
			if (incoming) {
				next.one($.support.animation.end, clear)
					.addClass('animated owl-animated-in')
					.addClass(incoming);
			}
		};
	
		Animate.prototype.clear = function(e) {
			$(e.target).css( { 'left': '' } )
				.removeClass('animated owl-animated-out owl-animated-in')
				.removeClass(this.core.settings.animateIn)
				.removeClass(this.core.settings.animateOut);
			this.core.onTransitionEnd();
		};
	
		/**
		 * Destroys the plugin.
		 * @public
		 */
		Animate.prototype.destroy = function() {
			var handler, property;
	
			for (handler in this.handlers) {
				this.core.$element.off(handler, this.handlers[handler]);
			}
			for (property in Object.getOwnPropertyNames(this)) {
				typeof this[property] != 'function' && (this[property] = null);
			}
		};
	
		$.fn.owlCarousel.Constructor.Plugins.Animate = Animate;
	
	})( jQuery, window, document);
	
	/**
	 * Autoplay Plugin
	 * @version 2.1.0
	 * @author Bartosz Wojciechowski
	 * @author Artus Kolanowski
	 * @author David Deutsch
	 * @license The MIT License (MIT)
	 */
	;(function($, window, document, undefined) {
	
		/**
		 * Creates the autoplay plugin.
		 * @class The Autoplay Plugin
		 * @param {Owl} scope - The Owl Carousel
		 */
		var Autoplay = function(carousel) {
			/**
			 * Reference to the core.
			 * @protected
			 * @type {Owl}
			 */
			this._core = carousel;
	
			/**
			 * The autoplay timeout.
			 * @type {Timeout}
			 */
			this._timeout = null;
	
			/**
			 * Indicates whenever the autoplay is paused.
			 * @type {Boolean}
			 */
			this._paused = false;
	
			/**
			 * All event handlers.
			 * @protected
			 * @type {Object}
			 */
			this._handlers = {
				'changed.owl.carousel': $.proxy(function(e) {
					if (e.namespace && e.property.name === 'settings') {
						if (this._core.settings.autoplay) {
							this.play();
						} else {
							this.stop();
						}
					} else if (e.namespace && e.property.name === 'position') {
						//console.log('play?', e);
						if (this._core.settings.autoplay) {
							this._setAutoPlayInterval();
						}
					}
				}, this),
				'initialized.owl.carousel': $.proxy(function(e) {
					if (e.namespace && this._core.settings.autoplay) {
						this.play();
					}
				}, this),
				'play.owl.autoplay': $.proxy(function(e, t, s) {
					if (e.namespace) {
						this.play(t, s);
					}
				}, this),
				'stop.owl.autoplay': $.proxy(function(e) {
					if (e.namespace) {
						this.stop();
					}
				}, this),
				'mouseover.owl.autoplay': $.proxy(function() {
					if (this._core.settings.autoplayHoverPause && this._core.is('rotating')) {
						this.pause();
					}
				}, this),
				'mouseleave.owl.autoplay': $.proxy(function() {
					if (this._core.settings.autoplayHoverPause && this._core.is('rotating')) {
						this.play();
					}
				}, this),
				'touchstart.owl.core': $.proxy(function() {
					if (this._core.settings.autoplayHoverPause && this._core.is('rotating')) {
						this.pause();
					}
				}, this),
				'touchend.owl.core': $.proxy(function() {
					if (this._core.settings.autoplayHoverPause) {
						this.play();
					}
				}, this)
			};
	
			// register event handlers
			this._core.$element.on(this._handlers);
	
			// set default options
			this._core.options = $.extend({}, Autoplay.Defaults, this._core.options);
		};
	
		/**
		 * Default options.
		 * @public
		 */
		Autoplay.Defaults = {
			autoplay: false,
			autoplayTimeout: 5000,
			autoplayHoverPause: false,
			autoplaySpeed: false
		};
	
		/**
		 * Starts the autoplay.
		 * @public
		 * @param {Number} [timeout] - The interval before the next animation starts.
		 * @param {Number} [speed] - The animation speed for the animations.
		 */
		Autoplay.prototype.play = function(timeout, speed) {
			this._paused = false;
	
			if (this._core.is('rotating')) {
				return;
			}
	
			this._core.enter('rotating');
	
			this._setAutoPlayInterval();
		};
	
		/**
		 * Gets a new timeout
		 * @private
		 * @param {Number} [timeout] - The interval before the next animation starts.
		 * @param {Number} [speed] - The animation speed for the animations.
		 * @return {Timeout}
		 */
		Autoplay.prototype._getNextTimeout = function(timeout, speed) {
			if ( this._timeout ) {
				window.clearTimeout(this._timeout);
			}
			return window.setTimeout($.proxy(function() {
				if (this._paused || this._core.is('busy') || this._core.is('interacting') || document.hidden) {
					return;
				}
				this._core.next(speed || this._core.settings.autoplaySpeed);
			}, this), timeout || this._core.settings.autoplayTimeout);
		};
	
		/**
		 * Sets autoplay in motion.
		 * @private
		 */
		Autoplay.prototype._setAutoPlayInterval = function() {
			this._timeout = this._getNextTimeout();
		};
	
		/**
		 * Stops the autoplay.
		 * @public
		 */
		Autoplay.prototype.stop = function() {
			if (!this._core.is('rotating')) {
				return;
			}
	
			window.clearTimeout(this._timeout);
			this._core.leave('rotating');
		};
	
		/**
		 * Stops the autoplay.
		 * @public
		 */
		Autoplay.prototype.pause = function() {
			if (!this._core.is('rotating')) {
				return;
			}
	
			this._paused = true;
		};
	
		/**
		 * Destroys the plugin.
		 */
		Autoplay.prototype.destroy = function() {
			var handler, property;
	
			this.stop();
	
			for (handler in this._handlers) {
				this._core.$element.off(handler, this._handlers[handler]);
			}
			for (property in Object.getOwnPropertyNames(this)) {
				typeof this[property] != 'function' && (this[property] = null);
			}
		};
	
		$.fn.owlCarousel.Constructor.Plugins.autoplay = Autoplay;
	
	})( jQuery, window, document);
	
	/**
	 * Navigation Plugin
	 * @version 2.1.0
	 * @author Artus Kolanowski
	 * @author David Deutsch
	 * @license The MIT License (MIT)
	 */
	;(function($, window, document, undefined) {
		'use strict';
	
		/**
		 * Creates the navigation plugin.
		 * @class The Navigation Plugin
		 * @param {Owl} carousel - The Owl Carousel.
		 */
		var Navigation = function(carousel) {
			/**
			 * Reference to the core.
			 * @protected
			 * @type {Owl}
			 */
			this._core = carousel;
	
			/**
			 * Indicates whether the plugin is initialized or not.
			 * @protected
			 * @type {Boolean}
			 */
			this._initialized = false;
	
			/**
			 * The current paging indexes.
			 * @protected
			 * @type {Array}
			 */
			this._pages = [];
	
			/**
			 * All DOM elements of the user interface.
			 * @protected
			 * @type {Object}
			 */
			this._controls = {};
	
			/**
			 * Markup for an indicator.
			 * @protected
			 * @type {Array.<String>}
			 */
			this._templates = [];
	
			/**
			 * The carousel element.
			 * @type {jQuery}
			 */
			this.$element = this._core.$element;
	
			/**
			 * Overridden methods of the carousel.
			 * @protected
			 * @type {Object}
			 */
			this._overrides = {
				next: this._core.next,
				prev: this._core.prev,
				to: this._core.to
			};
	
			/**
			 * All event handlers.
			 * @protected
			 * @type {Object}
			 */
			this._handlers = {
				'prepared.owl.carousel': $.proxy(function(e) {
					if (e.namespace && this._core.settings.dotsData) {
						this._templates.push('<div class="' + this._core.settings.dotClass + '">' +
							$(e.content).find('[data-dot]').addBack('[data-dot]').attr('data-dot') + '</div>');
					}
				}, this),
				'added.owl.carousel': $.proxy(function(e) {
					if (e.namespace && this._core.settings.dotsData) {
						this._templates.splice(e.position, 0, this._templates.pop());
					}
				}, this),
				'remove.owl.carousel': $.proxy(function(e) {
					if (e.namespace && this._core.settings.dotsData) {
						this._templates.splice(e.position, 1);
					}
				}, this),
				'changed.owl.carousel': $.proxy(function(e) {
					if (e.namespace && e.property.name == 'position') {
						this.draw();
					}
				}, this),
				'initialized.owl.carousel': $.proxy(function(e) {
					if (e.namespace && !this._initialized) {
						this._core.trigger('initialize', null, 'navigation');
						this.initialize();
						this.update();
						this.draw();
						this._initialized = true;
						this._core.trigger('initialized', null, 'navigation');
					}
				}, this),
				'refreshed.owl.carousel': $.proxy(function(e) {
					if (e.namespace && this._initialized) {
						this._core.trigger('refresh', null, 'navigation');
						this.update();
						this.draw();
						this._core.trigger('refreshed', null, 'navigation');
					}
				}, this)
			};
	
			// set default options
			this._core.options = $.extend({}, Navigation.Defaults, this._core.options);
	
			// register event handlers
			this.$element.on(this._handlers);
		};
	
		/**
		 * Default options.
		 * @public
		 * @todo Rename `slideBy` to `navBy`
		 */
		Navigation.Defaults = {
			nav: false,
			navText: [ 'prev', 'next' ],
			navSpeed: false,
			navElement: 'div',
			navContainer: false,
			navContainerClass: 'owl-nav',
			navClass: [ 'owl-prev', 'owl-next' ],
			slideBy: 1,
			dotClass: 'owl-dot',
			dotsClass: 'owl-dots',
			dots: true,
			dotsEach: false,
			dotsData: false,
			dotsSpeed: false,
			dotsContainer: false
		};
	
		/**
		 * Initializes the layout of the plugin and extends the carousel.
		 * @protected
		 */
		Navigation.prototype.initialize = function() {
			var override,
				settings = this._core.settings;
	
			// create DOM structure for relative navigation
			this._controls.$relative = (settings.navContainer ? $(settings.navContainer)
				: $('<div>').addClass(settings.navContainerClass).appendTo(this.$element)).addClass('disabled');
	
			this._controls.$previous = $('<' + settings.navElement + '>')
				.addClass(settings.navClass[0])
				.html(settings.navText[0])
				.prependTo(this._controls.$relative)
				.on('click', $.proxy(function(e) {
					this.prev(settings.navSpeed);
				}, this));
			this._controls.$next = $('<' + settings.navElement + '>')
				.addClass(settings.navClass[1])
				.html(settings.navText[1])
				.appendTo(this._controls.$relative)
				.on('click', $.proxy(function(e) {
					this.next(settings.navSpeed);
				}, this));
	
			// create DOM structure for absolute navigation
			if (!settings.dotsData) {
				this._templates = [ $('<div>')
					.addClass(settings.dotClass)
					.append($('<span>'))
					.prop('outerHTML') ];
			}
	
			this._controls.$absolute = (settings.dotsContainer ? $(settings.dotsContainer)
				: $('<div>').addClass(settings.dotsClass).appendTo(this.$element)).addClass('disabled');
	
			this._controls.$absolute.on('click', 'div', $.proxy(function(e) {
				var index = $(e.target).parent().is(this._controls.$absolute)
					? $(e.target).index() : $(e.target).parent().index();
	
				e.preventDefault();
	
				this.to(index, settings.dotsSpeed);
			}, this));
	
			// override public methods of the carousel
			for (override in this._overrides) {
				this._core[override] = $.proxy(this[override], this);
			}
		};
	
		/**
		 * Destroys the plugin.
		 * @protected
		 */
		Navigation.prototype.destroy = function() {
			var handler, control, property, override;
	
			for (handler in this._handlers) {
				this.$element.off(handler, this._handlers[handler]);
			}
			for (control in this._controls) {
				this._controls[control].remove();
			}
			for (override in this.overides) {
				this._core[override] = this._overrides[override];
			}
			for (property in Object.getOwnPropertyNames(this)) {
				typeof this[property] != 'function' && (this[property] = null);
			}
		};
	
		/**
		 * Updates the internal state.
		 * @protected
		 */
		Navigation.prototype.update = function() {
			var i, j, k,
				lower = this._core.clones().length / 2,
				upper = lower + this._core.items().length,
				maximum = this._core.maximum(true),
				settings = this._core.settings,
				size = settings.center || settings.autoWidth || settings.dotsData
					? 1 : settings.dotsEach || settings.items;
	
			if (settings.slideBy !== 'page') {
				settings.slideBy = Math.min(settings.slideBy, settings.items);
			}
	
			if (settings.dots || settings.slideBy == 'page') {
				this._pages = [];
	
				for (i = lower, j = 0, k = 0; i < upper; i++) {
					if (j >= size || j === 0) {
						this._pages.push({
							start: Math.min(maximum, i - lower),
							end: i - lower + size - 1
						});
						if (Math.min(maximum, i - lower) === maximum) {
							break;
						}
						j = 0, ++k;
					}
					j += this._core.mergers(this._core.relative(i));
				}
			}
		};
	
		/**
		 * Draws the user interface.
		 * @todo The option `dotsData` wont work.
		 * @protected
		 */
		Navigation.prototype.draw = function() {
			var difference,
				settings = this._core.settings,
				disabled = this._core.items().length <= settings.items,
				index = this._core.relative(this._core.current()),
				loop = settings.loop || settings.rewind;
	
			this._controls.$relative.toggleClass('disabled', !settings.nav || disabled);
	
			if (settings.nav) {
				this._controls.$previous.toggleClass('disabled', !loop && index <= this._core.minimum(true));
				this._controls.$next.toggleClass('disabled', !loop && index >= this._core.maximum(true));
			}
	
			this._controls.$absolute.toggleClass('disabled', !settings.dots || disabled);
	
			if (settings.dots) {
				difference = this._pages.length - this._controls.$absolute.children().length;
	
				if (settings.dotsData && difference !== 0) {
					this._controls.$absolute.html(this._templates.join(''));
				} else if (difference > 0) {
					this._controls.$absolute.append(new Array(difference + 1).join(this._templates[0]));
				} else if (difference < 0) {
					this._controls.$absolute.children().slice(difference).remove();
				}
	
				this._controls.$absolute.find('.active').removeClass('active');
				this._controls.$absolute.children().eq($.inArray(this.current(), this._pages)).addClass('active');
			}
		};
	
		/**
		 * Extends event data.
		 * @protected
		 * @param {Event} event - The event object which gets thrown.
		 */
		Navigation.prototype.onTrigger = function(event) {
			var settings = this._core.settings;
	
			event.page = {
				index: $.inArray(this.current(), this._pages),
				count: this._pages.length,
				size: settings && (settings.center || settings.autoWidth || settings.dotsData
					? 1 : settings.dotsEach || settings.items)
			};
		};
	
		/**
		 * Gets the current page position of the carousel.
		 * @protected
		 * @returns {Number}
		 */
		Navigation.prototype.current = function() {
			var current = this._core.relative(this._core.current());
			return $.grep(this._pages, $.proxy(function(page, index) {
				return page.start <= current && page.end >= current;
			}, this)).pop();
		};
	
		/**
		 * Gets the current succesor/predecessor position.
		 * @protected
		 * @returns {Number}
		 */
		Navigation.prototype.getPosition = function(successor) {
			var position, length,
				settings = this._core.settings;
	
			if (settings.slideBy == 'page') {
				position = $.inArray(this.current(), this._pages);
				length = this._pages.length;
				successor ? ++position : --position;
				position = this._pages[((position % length) + length) % length].start;
			} else {
				position = this._core.relative(this._core.current());
				length = this._core.items().length;
				successor ? position += settings.slideBy : position -= settings.slideBy;
			}
	
			return position;
		};
	
		/**
		 * Slides to the next item or page.
		 * @public
		 * @param {Number} [speed=false] - The time in milliseconds for the transition.
		 */
		Navigation.prototype.next = function(speed) {
			$.proxy(this._overrides.to, this._core)(this.getPosition(true), speed);
		};
	
		/**
		 * Slides to the previous item or page.
		 * @public
		 * @param {Number} [speed=false] - The time in milliseconds for the transition.
		 */
		Navigation.prototype.prev = function(speed) {
			$.proxy(this._overrides.to, this._core)(this.getPosition(false), speed);
		};
	
		/**
		 * Slides to the specified item or page.
		 * @public
		 * @param {Number} position - The position of the item or page.
		 * @param {Number} [speed] - The time in milliseconds for the transition.
		 * @param {Boolean} [standard=false] - Whether to use the standard behaviour or not.
		 */
		Navigation.prototype.to = function(position, speed, standard) {
			var length;
	
			if (!standard && this._pages.length) {
				length = this._pages.length;
				$.proxy(this._overrides.to, this._core)(this._pages[((position % length) + length) % length].start, speed);
			} else {
				$.proxy(this._overrides.to, this._core)(position, speed);
			}
		};
	
		$.fn.owlCarousel.Constructor.Plugins.Navigation = Navigation;
	
	})( jQuery, window, document);
	
	/**
	 * Hash Plugin
	 * @version 2.1.0
	 * @author Artus Kolanowski
	 * @author David Deutsch
	 * @license The MIT License (MIT)
	 */
	;(function($, window, document, undefined) {
		'use strict';
	
		/**
		 * Creates the hash plugin.
		 * @class The Hash Plugin
		 * @param {Owl} carousel - The Owl Carousel
		 */
		var Hash = function(carousel) {
			/**
			 * Reference to the core.
			 * @protected
			 * @type {Owl}
			 */
			this._core = carousel;
	
			/**
			 * Hash index for the items.
			 * @protected
			 * @type {Object}
			 */
			this._hashes = {};
	
			/**
			 * The carousel element.
			 * @type {jQuery}
			 */
			this.$element = this._core.$element;
	
			/**
			 * All event handlers.
			 * @protected
			 * @type {Object}
			 */
			this._handlers = {
				'initialized.owl.carousel': $.proxy(function(e) {
					if (e.namespace && this._core.settings.startPosition === 'URLHash') {
						$(window).trigger('hashchange.owl.navigation');
					}
				}, this),
				'prepared.owl.carousel': $.proxy(function(e) {
					if (e.namespace) {
						var hash = $(e.content).find('[data-hash]').addBack('[data-hash]').attr('data-hash');
	
						if (!hash) {
							return;
						}
	
						this._hashes[hash] = e.content;
					}
				}, this),
				'changed.owl.carousel': $.proxy(function(e) {
					if (e.namespace && e.property.name === 'position') {
						var current = this._core.items(this._core.relative(this._core.current())),
							hash = $.map(this._hashes, function(item, hash) {
								return item === current ? hash : null;
							}).join();
	
						if (!hash || window.location.hash.slice(1) === hash) {
							return;
						}
	
						window.location.hash = hash;
					}
				}, this)
			};
	
			// set default options
			this._core.options = $.extend({}, Hash.Defaults, this._core.options);
	
			// register the event handlers
			this.$element.on(this._handlers);
	
			// register event listener for hash navigation
			$(window).on('hashchange.owl.navigation', $.proxy(function(e) {
				var hash = window.location.hash.substring(1),
					items = this._core.$stage.children(),
					position = this._hashes[hash] && items.index(this._hashes[hash]);
	
				if (position === undefined || position === this._core.current()) {
					return;
				}
	
				this._core.to(this._core.relative(position), false, true);
			}, this));
		};
	
		/**
		 * Default options.
		 * @public
		 */
		Hash.Defaults = {
			URLhashListener: false
		};
	
		/**
		 * Destroys the plugin.
		 * @public
		 */
		Hash.prototype.destroy = function() {
			var handler, property;
	
			$(window).off('hashchange.owl.navigation');
	
			for (handler in this._handlers) {
				this._core.$element.off(handler, this._handlers[handler]);
			}
			for (property in Object.getOwnPropertyNames(this)) {
				typeof this[property] != 'function' && (this[property] = null);
			}
		};
	
		$.fn.owlCarousel.Constructor.Plugins.Hash = Hash;
	
	})( jQuery, window, document);
	
	/**
	 * Support Plugin
	 *
	 * @version 2.1.0
	 * @author Vivid Planet Software GmbH
	 * @author Artus Kolanowski
	 * @author David Deutsch
	 * @license The MIT License (MIT)
	 */
	;(function($, window, document, undefined) {
	
		var style = $('<support>').get(0).style,
			prefixes = 'Webkit Moz O ms'.split(' '),
			events = {
				transition: {
					end: {
						WebkitTransition: 'webkitTransitionEnd',
						MozTransition: 'transitionend',
						OTransition: 'oTransitionEnd',
						transition: 'transitionend'
					}
				},
				animation: {
					end: {
						WebkitAnimation: 'webkitAnimationEnd',
						MozAnimation: 'animationend',
						OAnimation: 'oAnimationEnd',
						animation: 'animationend'
					}
				}
			},
			tests = {
				csstransforms: function() {
					return !!test('transform');
				},
				csstransforms3d: function() {
					return !!test('perspective');
				},
				csstransitions: function() {
					return !!test('transition');
				},
				cssanimations: function() {
					return !!test('animation');
				}
			};
	
		function test(property, prefixed) {
			var result = false,
				upper = property.charAt(0).toUpperCase() + property.slice(1);
	
			$.each((property + ' ' + prefixes.join(upper + ' ') + upper).split(' '), function(i, property) {
				if (style[property] !== undefined) {
					result = prefixed ? property : true;
					return false;
				}
			});
	
			return result;
		}
	
		function prefixed(property) {
			return test(property, true);
		}
	
		if (tests.csstransitions()) {
			/* jshint -W053 */
			$.support.transition = new String(prefixed('transition'))
			$.support.transition.end = events.transition.end[ $.support.transition ];
		}
	
		if (tests.cssanimations()) {
			/* jshint -W053 */
			$.support.animation = new String(prefixed('animation'))
			$.support.animation.end = events.animation.end[ $.support.animation ];
		}
	
		if (tests.csstransforms()) {
			/* jshint -W053 */
			$.support.transform = new String(prefixed('transform'));
			$.support.transform3d = tests.csstransforms3d();
		}
	
	})( jQuery, window, document);
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*!
	 * jQuery JavaScript Library v3.2.1
	 * https://jquery.com/
	 *
	 * Includes Sizzle.js
	 * https://sizzlejs.com/
	 *
	 * Copyright JS Foundation and other contributors
	 * Released under the MIT license
	 * https://jquery.org/license
	 *
	 * Date: 2017-03-20T18:59Z
	 */
	( function( global, factory ) {
	
		"use strict";
	
		if ( typeof module === "object" && typeof module.exports === "object" ) {
	
			// For CommonJS and CommonJS-like environments where a proper `window`
			// is present, execute the factory and get jQuery.
			// For environments that do not have a `window` with a `document`
			// (such as Node.js), expose a factory as module.exports.
			// This accentuates the need for the creation of a real `window`.
			// e.g. var jQuery = require("jquery")(window);
			// See ticket #14549 for more info.
			module.exports = global.document ?
				factory( global, true ) :
				function( w ) {
					if ( !w.document ) {
						throw new Error( "jQuery requires a window with a document" );
					}
					return factory( w );
				};
		} else {
			factory( global );
		}
	
	// Pass this if window is not defined yet
	} )( typeof window !== "undefined" ? window : this, function( window, noGlobal ) {
	
	// Edge <= 12 - 13+, Firefox <=18 - 45+, IE 10 - 11, Safari 5.1 - 9+, iOS 6 - 9.1
	// throw exceptions when non-strict code (e.g., ASP.NET 4.5) accesses strict mode
	// arguments.callee.caller (trac-13335). But as of jQuery 3.0 (2016), strict mode should be common
	// enough that all such attempts are guarded in a try block.
	"use strict";
	
	var arr = [];
	
	var document = window.document;
	
	var getProto = Object.getPrototypeOf;
	
	var slice = arr.slice;
	
	var concat = arr.concat;
	
	var push = arr.push;
	
	var indexOf = arr.indexOf;
	
	var class2type = {};
	
	var toString = class2type.toString;
	
	var hasOwn = class2type.hasOwnProperty;
	
	var fnToString = hasOwn.toString;
	
	var ObjectFunctionString = fnToString.call( Object );
	
	var support = {};
	
	
	
		function DOMEval( code, doc ) {
			doc = doc || document;
	
			var script = doc.createElement( "script" );
	
			script.text = code;
			doc.head.appendChild( script ).parentNode.removeChild( script );
		}
	/* global Symbol */
	// Defining this global in .eslintrc.json would create a danger of using the global
	// unguarded in another place, it seems safer to define global only for this module
	
	
	
	var
		version = "3.2.1",
	
		// Define a local copy of jQuery
		jQuery = function( selector, context ) {
	
			// The jQuery object is actually just the init constructor 'enhanced'
			// Need init if jQuery is called (just allow error to be thrown if not included)
			return new jQuery.fn.init( selector, context );
		},
	
		// Support: Android <=4.0 only
		// Make sure we trim BOM and NBSP
		rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
	
		// Matches dashed string for camelizing
		rmsPrefix = /^-ms-/,
		rdashAlpha = /-([a-z])/g,
	
		// Used by jQuery.camelCase as callback to replace()
		fcamelCase = function( all, letter ) {
			return letter.toUpperCase();
		};
	
	jQuery.fn = jQuery.prototype = {
	
		// The current version of jQuery being used
		jquery: version,
	
		constructor: jQuery,
	
		// The default length of a jQuery object is 0
		length: 0,
	
		toArray: function() {
			return slice.call( this );
		},
	
		// Get the Nth element in the matched element set OR
		// Get the whole matched element set as a clean array
		get: function( num ) {
	
			// Return all the elements in a clean array
			if ( num == null ) {
				return slice.call( this );
			}
	
			// Return just the one element from the set
			return num < 0 ? this[ num + this.length ] : this[ num ];
		},
	
		// Take an array of elements and push it onto the stack
		// (returning the new matched element set)
		pushStack: function( elems ) {
	
			// Build a new jQuery matched element set
			var ret = jQuery.merge( this.constructor(), elems );
	
			// Add the old object onto the stack (as a reference)
			ret.prevObject = this;
	
			// Return the newly-formed element set
			return ret;
		},
	
		// Execute a callback for every element in the matched set.
		each: function( callback ) {
			return jQuery.each( this, callback );
		},
	
		map: function( callback ) {
			return this.pushStack( jQuery.map( this, function( elem, i ) {
				return callback.call( elem, i, elem );
			} ) );
		},
	
		slice: function() {
			return this.pushStack( slice.apply( this, arguments ) );
		},
	
		first: function() {
			return this.eq( 0 );
		},
	
		last: function() {
			return this.eq( -1 );
		},
	
		eq: function( i ) {
			var len = this.length,
				j = +i + ( i < 0 ? len : 0 );
			return this.pushStack( j >= 0 && j < len ? [ this[ j ] ] : [] );
		},
	
		end: function() {
			return this.prevObject || this.constructor();
		},
	
		// For internal use only.
		// Behaves like an Array's method, not like a jQuery method.
		push: push,
		sort: arr.sort,
		splice: arr.splice
	};
	
	jQuery.extend = jQuery.fn.extend = function() {
		var options, name, src, copy, copyIsArray, clone,
			target = arguments[ 0 ] || {},
			i = 1,
			length = arguments.length,
			deep = false;
	
		// Handle a deep copy situation
		if ( typeof target === "boolean" ) {
			deep = target;
	
			// Skip the boolean and the target
			target = arguments[ i ] || {};
			i++;
		}
	
		// Handle case when target is a string or something (possible in deep copy)
		if ( typeof target !== "object" && !jQuery.isFunction( target ) ) {
			target = {};
		}
	
		// Extend jQuery itself if only one argument is passed
		if ( i === length ) {
			target = this;
			i--;
		}
	
		for ( ; i < length; i++ ) {
	
			// Only deal with non-null/undefined values
			if ( ( options = arguments[ i ] ) != null ) {
	
				// Extend the base object
				for ( name in options ) {
					src = target[ name ];
					copy = options[ name ];
	
					// Prevent never-ending loop
					if ( target === copy ) {
						continue;
					}
	
					// Recurse if we're merging plain objects or arrays
					if ( deep && copy && ( jQuery.isPlainObject( copy ) ||
						( copyIsArray = Array.isArray( copy ) ) ) ) {
	
						if ( copyIsArray ) {
							copyIsArray = false;
							clone = src && Array.isArray( src ) ? src : [];
	
						} else {
							clone = src && jQuery.isPlainObject( src ) ? src : {};
						}
	
						// Never move original objects, clone them
						target[ name ] = jQuery.extend( deep, clone, copy );
	
					// Don't bring in undefined values
					} else if ( copy !== undefined ) {
						target[ name ] = copy;
					}
				}
			}
		}
	
		// Return the modified object
		return target;
	};
	
	jQuery.extend( {
	
		// Unique for each copy of jQuery on the page
		expando: "jQuery" + ( version + Math.random() ).replace( /\D/g, "" ),
	
		// Assume jQuery is ready without the ready module
		isReady: true,
	
		error: function( msg ) {
			throw new Error( msg );
		},
	
		noop: function() {},
	
		isFunction: function( obj ) {
			return jQuery.type( obj ) === "function";
		},
	
		isWindow: function( obj ) {
			return obj != null && obj === obj.window;
		},
	
		isNumeric: function( obj ) {
	
			// As of jQuery 3.0, isNumeric is limited to
			// strings and numbers (primitives or objects)
			// that can be coerced to finite numbers (gh-2662)
			var type = jQuery.type( obj );
			return ( type === "number" || type === "string" ) &&
	
				// parseFloat NaNs numeric-cast false positives ("")
				// ...but misinterprets leading-number strings, particularly hex literals ("0x...")
				// subtraction forces infinities to NaN
				!isNaN( obj - parseFloat( obj ) );
		},
	
		isPlainObject: function( obj ) {
			var proto, Ctor;
	
			// Detect obvious negatives
			// Use toString instead of jQuery.type to catch host objects
			if ( !obj || toString.call( obj ) !== "[object Object]" ) {
				return false;
			}
	
			proto = getProto( obj );
	
			// Objects with no prototype (e.g., `Object.create( null )`) are plain
			if ( !proto ) {
				return true;
			}
	
			// Objects with prototype are plain iff they were constructed by a global Object function
			Ctor = hasOwn.call( proto, "constructor" ) && proto.constructor;
			return typeof Ctor === "function" && fnToString.call( Ctor ) === ObjectFunctionString;
		},
	
		isEmptyObject: function( obj ) {
	
			/* eslint-disable no-unused-vars */
			// See https://github.com/eslint/eslint/issues/6125
			var name;
	
			for ( name in obj ) {
				return false;
			}
			return true;
		},
	
		type: function( obj ) {
			if ( obj == null ) {
				return obj + "";
			}
	
			// Support: Android <=2.3 only (functionish RegExp)
			return typeof obj === "object" || typeof obj === "function" ?
				class2type[ toString.call( obj ) ] || "object" :
				typeof obj;
		},
	
		// Evaluates a script in a global context
		globalEval: function( code ) {
			DOMEval( code );
		},
	
		// Convert dashed to camelCase; used by the css and data modules
		// Support: IE <=9 - 11, Edge 12 - 13
		// Microsoft forgot to hump their vendor prefix (#9572)
		camelCase: function( string ) {
			return string.replace( rmsPrefix, "ms-" ).replace( rdashAlpha, fcamelCase );
		},
	
		each: function( obj, callback ) {
			var length, i = 0;
	
			if ( isArrayLike( obj ) ) {
				length = obj.length;
				for ( ; i < length; i++ ) {
					if ( callback.call( obj[ i ], i, obj[ i ] ) === false ) {
						break;
					}
				}
			} else {
				for ( i in obj ) {
					if ( callback.call( obj[ i ], i, obj[ i ] ) === false ) {
						break;
					}
				}
			}
	
			return obj;
		},
	
		// Support: Android <=4.0 only
		trim: function( text ) {
			return text == null ?
				"" :
				( text + "" ).replace( rtrim, "" );
		},
	
		// results is for internal usage only
		makeArray: function( arr, results ) {
			var ret = results || [];
	
			if ( arr != null ) {
				if ( isArrayLike( Object( arr ) ) ) {
					jQuery.merge( ret,
						typeof arr === "string" ?
						[ arr ] : arr
					);
				} else {
					push.call( ret, arr );
				}
			}
	
			return ret;
		},
	
		inArray: function( elem, arr, i ) {
			return arr == null ? -1 : indexOf.call( arr, elem, i );
		},
	
		// Support: Android <=4.0 only, PhantomJS 1 only
		// push.apply(_, arraylike) throws on ancient WebKit
		merge: function( first, second ) {
			var len = +second.length,
				j = 0,
				i = first.length;
	
			for ( ; j < len; j++ ) {
				first[ i++ ] = second[ j ];
			}
	
			first.length = i;
	
			return first;
		},
	
		grep: function( elems, callback, invert ) {
			var callbackInverse,
				matches = [],
				i = 0,
				length = elems.length,
				callbackExpect = !invert;
	
			// Go through the array, only saving the items
			// that pass the validator function
			for ( ; i < length; i++ ) {
				callbackInverse = !callback( elems[ i ], i );
				if ( callbackInverse !== callbackExpect ) {
					matches.push( elems[ i ] );
				}
			}
	
			return matches;
		},
	
		// arg is for internal usage only
		map: function( elems, callback, arg ) {
			var length, value,
				i = 0,
				ret = [];
	
			// Go through the array, translating each of the items to their new values
			if ( isArrayLike( elems ) ) {
				length = elems.length;
				for ( ; i < length; i++ ) {
					value = callback( elems[ i ], i, arg );
	
					if ( value != null ) {
						ret.push( value );
					}
				}
	
			// Go through every key on the object,
			} else {
				for ( i in elems ) {
					value = callback( elems[ i ], i, arg );
	
					if ( value != null ) {
						ret.push( value );
					}
				}
			}
	
			// Flatten any nested arrays
			return concat.apply( [], ret );
		},
	
		// A global GUID counter for objects
		guid: 1,
	
		// Bind a function to a context, optionally partially applying any
		// arguments.
		proxy: function( fn, context ) {
			var tmp, args, proxy;
	
			if ( typeof context === "string" ) {
				tmp = fn[ context ];
				context = fn;
				fn = tmp;
			}
	
			// Quick check to determine if target is callable, in the spec
			// this throws a TypeError, but we will just return undefined.
			if ( !jQuery.isFunction( fn ) ) {
				return undefined;
			}
	
			// Simulated bind
			args = slice.call( arguments, 2 );
			proxy = function() {
				return fn.apply( context || this, args.concat( slice.call( arguments ) ) );
			};
	
			// Set the guid of unique handler to the same of original handler, so it can be removed
			proxy.guid = fn.guid = fn.guid || jQuery.guid++;
	
			return proxy;
		},
	
		now: Date.now,
	
		// jQuery.support is not used in Core but other projects attach their
		// properties to it so it needs to exist.
		support: support
	} );
	
	if ( typeof Symbol === "function" ) {
		jQuery.fn[ Symbol.iterator ] = arr[ Symbol.iterator ];
	}
	
	// Populate the class2type map
	jQuery.each( "Boolean Number String Function Array Date RegExp Object Error Symbol".split( " " ),
	function( i, name ) {
		class2type[ "[object " + name + "]" ] = name.toLowerCase();
	} );
	
	function isArrayLike( obj ) {
	
		// Support: real iOS 8.2 only (not reproducible in simulator)
		// `in` check used to prevent JIT error (gh-2145)
		// hasOwn isn't used here due to false negatives
		// regarding Nodelist length in IE
		var length = !!obj && "length" in obj && obj.length,
			type = jQuery.type( obj );
	
		if ( type === "function" || jQuery.isWindow( obj ) ) {
			return false;
		}
	
		return type === "array" || length === 0 ||
			typeof length === "number" && length > 0 && ( length - 1 ) in obj;
	}
	var Sizzle =
	/*!
	 * Sizzle CSS Selector Engine v2.3.3
	 * https://sizzlejs.com/
	 *
	 * Copyright jQuery Foundation and other contributors
	 * Released under the MIT license
	 * http://jquery.org/license
	 *
	 * Date: 2016-08-08
	 */
	(function( window ) {
	
	var i,
		support,
		Expr,
		getText,
		isXML,
		tokenize,
		compile,
		select,
		outermostContext,
		sortInput,
		hasDuplicate,
	
		// Local document vars
		setDocument,
		document,
		docElem,
		documentIsHTML,
		rbuggyQSA,
		rbuggyMatches,
		matches,
		contains,
	
		// Instance-specific data
		expando = "sizzle" + 1 * new Date(),
		preferredDoc = window.document,
		dirruns = 0,
		done = 0,
		classCache = createCache(),
		tokenCache = createCache(),
		compilerCache = createCache(),
		sortOrder = function( a, b ) {
			if ( a === b ) {
				hasDuplicate = true;
			}
			return 0;
		},
	
		// Instance methods
		hasOwn = ({}).hasOwnProperty,
		arr = [],
		pop = arr.pop,
		push_native = arr.push,
		push = arr.push,
		slice = arr.slice,
		// Use a stripped-down indexOf as it's faster than native
		// https://jsperf.com/thor-indexof-vs-for/5
		indexOf = function( list, elem ) {
			var i = 0,
				len = list.length;
			for ( ; i < len; i++ ) {
				if ( list[i] === elem ) {
					return i;
				}
			}
			return -1;
		},
	
		booleans = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
	
		// Regular expressions
	
		// http://www.w3.org/TR/css3-selectors/#whitespace
		whitespace = "[\\x20\\t\\r\\n\\f]",
	
		// http://www.w3.org/TR/CSS21/syndata.html#value-def-identifier
		identifier = "(?:\\\\.|[\\w-]|[^\0-\\xa0])+",
	
		// Attribute selectors: http://www.w3.org/TR/selectors/#attribute-selectors
		attributes = "\\[" + whitespace + "*(" + identifier + ")(?:" + whitespace +
			// Operator (capture 2)
			"*([*^$|!~]?=)" + whitespace +
			// "Attribute values must be CSS identifiers [capture 5] or strings [capture 3 or capture 4]"
			"*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + identifier + "))|)" + whitespace +
			"*\\]",
	
		pseudos = ":(" + identifier + ")(?:\\((" +
			// To reduce the number of selectors needing tokenize in the preFilter, prefer arguments:
			// 1. quoted (capture 3; capture 4 or capture 5)
			"('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|" +
			// 2. simple (capture 6)
			"((?:\\\\.|[^\\\\()[\\]]|" + attributes + ")*)|" +
			// 3. anything else (capture 2)
			".*" +
			")\\)|)",
	
		// Leading and non-escaped trailing whitespace, capturing some non-whitespace characters preceding the latter
		rwhitespace = new RegExp( whitespace + "+", "g" ),
		rtrim = new RegExp( "^" + whitespace + "+|((?:^|[^\\\\])(?:\\\\.)*)" + whitespace + "+$", "g" ),
	
		rcomma = new RegExp( "^" + whitespace + "*," + whitespace + "*" ),
		rcombinators = new RegExp( "^" + whitespace + "*([>+~]|" + whitespace + ")" + whitespace + "*" ),
	
		rattributeQuotes = new RegExp( "=" + whitespace + "*([^\\]'\"]*?)" + whitespace + "*\\]", "g" ),
	
		rpseudo = new RegExp( pseudos ),
		ridentifier = new RegExp( "^" + identifier + "$" ),
	
		matchExpr = {
			"ID": new RegExp( "^#(" + identifier + ")" ),
			"CLASS": new RegExp( "^\\.(" + identifier + ")" ),
			"TAG": new RegExp( "^(" + identifier + "|[*])" ),
			"ATTR": new RegExp( "^" + attributes ),
			"PSEUDO": new RegExp( "^" + pseudos ),
			"CHILD": new RegExp( "^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + whitespace +
				"*(even|odd|(([+-]|)(\\d*)n|)" + whitespace + "*(?:([+-]|)" + whitespace +
				"*(\\d+)|))" + whitespace + "*\\)|)", "i" ),
			"bool": new RegExp( "^(?:" + booleans + ")$", "i" ),
			// For use in libraries implementing .is()
			// We use this for POS matching in `select`
			"needsContext": new RegExp( "^" + whitespace + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" +
				whitespace + "*((?:-\\d)?\\d*)" + whitespace + "*\\)|)(?=[^-]|$)", "i" )
		},
	
		rinputs = /^(?:input|select|textarea|button)$/i,
		rheader = /^h\d$/i,
	
		rnative = /^[^{]+\{\s*\[native \w/,
	
		// Easily-parseable/retrievable ID or TAG or CLASS selectors
		rquickExpr = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
	
		rsibling = /[+~]/,
	
		// CSS escapes
		// http://www.w3.org/TR/CSS21/syndata.html#escaped-characters
		runescape = new RegExp( "\\\\([\\da-f]{1,6}" + whitespace + "?|(" + whitespace + ")|.)", "ig" ),
		funescape = function( _, escaped, escapedWhitespace ) {
			var high = "0x" + escaped - 0x10000;
			// NaN means non-codepoint
			// Support: Firefox<24
			// Workaround erroneous numeric interpretation of +"0x"
			return high !== high || escapedWhitespace ?
				escaped :
				high < 0 ?
					// BMP codepoint
					String.fromCharCode( high + 0x10000 ) :
					// Supplemental Plane codepoint (surrogate pair)
					String.fromCharCode( high >> 10 | 0xD800, high & 0x3FF | 0xDC00 );
		},
	
		// CSS string/identifier serialization
		// https://drafts.csswg.org/cssom/#common-serializing-idioms
		rcssescape = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
		fcssescape = function( ch, asCodePoint ) {
			if ( asCodePoint ) {
	
				// U+0000 NULL becomes U+FFFD REPLACEMENT CHARACTER
				if ( ch === "\0" ) {
					return "\uFFFD";
				}
	
				// Control characters and (dependent upon position) numbers get escaped as code points
				return ch.slice( 0, -1 ) + "\\" + ch.charCodeAt( ch.length - 1 ).toString( 16 ) + " ";
			}
	
			// Other potentially-special ASCII characters get backslash-escaped
			return "\\" + ch;
		},
	
		// Used for iframes
		// See setDocument()
		// Removing the function wrapper causes a "Permission Denied"
		// error in IE
		unloadHandler = function() {
			setDocument();
		},
	
		disabledAncestor = addCombinator(
			function( elem ) {
				return elem.disabled === true && ("form" in elem || "label" in elem);
			},
			{ dir: "parentNode", next: "legend" }
		);
	
	// Optimize for push.apply( _, NodeList )
	try {
		push.apply(
			(arr = slice.call( preferredDoc.childNodes )),
			preferredDoc.childNodes
		);
		// Support: Android<4.0
		// Detect silently failing push.apply
		arr[ preferredDoc.childNodes.length ].nodeType;
	} catch ( e ) {
		push = { apply: arr.length ?
	
			// Leverage slice if possible
			function( target, els ) {
				push_native.apply( target, slice.call(els) );
			} :
	
			// Support: IE<9
			// Otherwise append directly
			function( target, els ) {
				var j = target.length,
					i = 0;
				// Can't trust NodeList.length
				while ( (target[j++] = els[i++]) ) {}
				target.length = j - 1;
			}
		};
	}
	
	function Sizzle( selector, context, results, seed ) {
		var m, i, elem, nid, match, groups, newSelector,
			newContext = context && context.ownerDocument,
	
			// nodeType defaults to 9, since context defaults to document
			nodeType = context ? context.nodeType : 9;
	
		results = results || [];
	
		// Return early from calls with invalid selector or context
		if ( typeof selector !== "string" || !selector ||
			nodeType !== 1 && nodeType !== 9 && nodeType !== 11 ) {
	
			return results;
		}
	
		// Try to shortcut find operations (as opposed to filters) in HTML documents
		if ( !seed ) {
	
			if ( ( context ? context.ownerDocument || context : preferredDoc ) !== document ) {
				setDocument( context );
			}
			context = context || document;
	
			if ( documentIsHTML ) {
	
				// If the selector is sufficiently simple, try using a "get*By*" DOM method
				// (excepting DocumentFragment context, where the methods don't exist)
				if ( nodeType !== 11 && (match = rquickExpr.exec( selector )) ) {
	
					// ID selector
					if ( (m = match[1]) ) {
	
						// Document context
						if ( nodeType === 9 ) {
							if ( (elem = context.getElementById( m )) ) {
	
								// Support: IE, Opera, Webkit
								// TODO: identify versions
								// getElementById can match elements by name instead of ID
								if ( elem.id === m ) {
									results.push( elem );
									return results;
								}
							} else {
								return results;
							}
	
						// Element context
						} else {
	
							// Support: IE, Opera, Webkit
							// TODO: identify versions
							// getElementById can match elements by name instead of ID
							if ( newContext && (elem = newContext.getElementById( m )) &&
								contains( context, elem ) &&
								elem.id === m ) {
	
								results.push( elem );
								return results;
							}
						}
	
					// Type selector
					} else if ( match[2] ) {
						push.apply( results, context.getElementsByTagName( selector ) );
						return results;
	
					// Class selector
					} else if ( (m = match[3]) && support.getElementsByClassName &&
						context.getElementsByClassName ) {
	
						push.apply( results, context.getElementsByClassName( m ) );
						return results;
					}
				}
	
				// Take advantage of querySelectorAll
				if ( support.qsa &&
					!compilerCache[ selector + " " ] &&
					(!rbuggyQSA || !rbuggyQSA.test( selector )) ) {
	
					if ( nodeType !== 1 ) {
						newContext = context;
						newSelector = selector;
	
					// qSA looks outside Element context, which is not what we want
					// Thanks to Andrew Dupont for this workaround technique
					// Support: IE <=8
					// Exclude object elements
					} else if ( context.nodeName.toLowerCase() !== "object" ) {
	
						// Capture the context ID, setting it first if necessary
						if ( (nid = context.getAttribute( "id" )) ) {
							nid = nid.replace( rcssescape, fcssescape );
						} else {
							context.setAttribute( "id", (nid = expando) );
						}
	
						// Prefix every selector in the list
						groups = tokenize( selector );
						i = groups.length;
						while ( i-- ) {
							groups[i] = "#" + nid + " " + toSelector( groups[i] );
						}
						newSelector = groups.join( "," );
	
						// Expand context for sibling selectors
						newContext = rsibling.test( selector ) && testContext( context.parentNode ) ||
							context;
					}
	
					if ( newSelector ) {
						try {
							push.apply( results,
								newContext.querySelectorAll( newSelector )
							);
							return results;
						} catch ( qsaError ) {
						} finally {
							if ( nid === expando ) {
								context.removeAttribute( "id" );
							}
						}
					}
				}
			}
		}
	
		// All others
		return select( selector.replace( rtrim, "$1" ), context, results, seed );
	}
	
	/**
	 * Create key-value caches of limited size
	 * @returns {function(string, object)} Returns the Object data after storing it on itself with
	 *	property name the (space-suffixed) string and (if the cache is larger than Expr.cacheLength)
	 *	deleting the oldest entry
	 */
	function createCache() {
		var keys = [];
	
		function cache( key, value ) {
			// Use (key + " ") to avoid collision with native prototype properties (see Issue #157)
			if ( keys.push( key + " " ) > Expr.cacheLength ) {
				// Only keep the most recent entries
				delete cache[ keys.shift() ];
			}
			return (cache[ key + " " ] = value);
		}
		return cache;
	}
	
	/**
	 * Mark a function for special use by Sizzle
	 * @param {Function} fn The function to mark
	 */
	function markFunction( fn ) {
		fn[ expando ] = true;
		return fn;
	}
	
	/**
	 * Support testing using an element
	 * @param {Function} fn Passed the created element and returns a boolean result
	 */
	function assert( fn ) {
		var el = document.createElement("fieldset");
	
		try {
			return !!fn( el );
		} catch (e) {
			return false;
		} finally {
			// Remove from its parent by default
			if ( el.parentNode ) {
				el.parentNode.removeChild( el );
			}
			// release memory in IE
			el = null;
		}
	}
	
	/**
	 * Adds the same handler for all of the specified attrs
	 * @param {String} attrs Pipe-separated list of attributes
	 * @param {Function} handler The method that will be applied
	 */
	function addHandle( attrs, handler ) {
		var arr = attrs.split("|"),
			i = arr.length;
	
		while ( i-- ) {
			Expr.attrHandle[ arr[i] ] = handler;
		}
	}
	
	/**
	 * Checks document order of two siblings
	 * @param {Element} a
	 * @param {Element} b
	 * @returns {Number} Returns less than 0 if a precedes b, greater than 0 if a follows b
	 */
	function siblingCheck( a, b ) {
		var cur = b && a,
			diff = cur && a.nodeType === 1 && b.nodeType === 1 &&
				a.sourceIndex - b.sourceIndex;
	
		// Use IE sourceIndex if available on both nodes
		if ( diff ) {
			return diff;
		}
	
		// Check if b follows a
		if ( cur ) {
			while ( (cur = cur.nextSibling) ) {
				if ( cur === b ) {
					return -1;
				}
			}
		}
	
		return a ? 1 : -1;
	}
	
	/**
	 * Returns a function to use in pseudos for input types
	 * @param {String} type
	 */
	function createInputPseudo( type ) {
		return function( elem ) {
			var name = elem.nodeName.toLowerCase();
			return name === "input" && elem.type === type;
		};
	}
	
	/**
	 * Returns a function to use in pseudos for buttons
	 * @param {String} type
	 */
	function createButtonPseudo( type ) {
		return function( elem ) {
			var name = elem.nodeName.toLowerCase();
			return (name === "input" || name === "button") && elem.type === type;
		};
	}
	
	/**
	 * Returns a function to use in pseudos for :enabled/:disabled
	 * @param {Boolean} disabled true for :disabled; false for :enabled
	 */
	function createDisabledPseudo( disabled ) {
	
		// Known :disabled false positives: fieldset[disabled] > legend:nth-of-type(n+2) :can-disable
		return function( elem ) {
	
			// Only certain elements can match :enabled or :disabled
			// https://html.spec.whatwg.org/multipage/scripting.html#selector-enabled
			// https://html.spec.whatwg.org/multipage/scripting.html#selector-disabled
			if ( "form" in elem ) {
	
				// Check for inherited disabledness on relevant non-disabled elements:
				// * listed form-associated elements in a disabled fieldset
				//   https://html.spec.whatwg.org/multipage/forms.html#category-listed
				//   https://html.spec.whatwg.org/multipage/forms.html#concept-fe-disabled
				// * option elements in a disabled optgroup
				//   https://html.spec.whatwg.org/multipage/forms.html#concept-option-disabled
				// All such elements have a "form" property.
				if ( elem.parentNode && elem.disabled === false ) {
	
					// Option elements defer to a parent optgroup if present
					if ( "label" in elem ) {
						if ( "label" in elem.parentNode ) {
							return elem.parentNode.disabled === disabled;
						} else {
							return elem.disabled === disabled;
						}
					}
	
					// Support: IE 6 - 11
					// Use the isDisabled shortcut property to check for disabled fieldset ancestors
					return elem.isDisabled === disabled ||
	
						// Where there is no isDisabled, check manually
						/* jshint -W018 */
						elem.isDisabled !== !disabled &&
							disabledAncestor( elem ) === disabled;
				}
	
				return elem.disabled === disabled;
	
			// Try to winnow out elements that can't be disabled before trusting the disabled property.
			// Some victims get caught in our net (label, legend, menu, track), but it shouldn't
			// even exist on them, let alone have a boolean value.
			} else if ( "label" in elem ) {
				return elem.disabled === disabled;
			}
	
			// Remaining elements are neither :enabled nor :disabled
			return false;
		};
	}
	
	/**
	 * Returns a function to use in pseudos for positionals
	 * @param {Function} fn
	 */
	function createPositionalPseudo( fn ) {
		return markFunction(function( argument ) {
			argument = +argument;
			return markFunction(function( seed, matches ) {
				var j,
					matchIndexes = fn( [], seed.length, argument ),
					i = matchIndexes.length;
	
				// Match elements found at the specified indexes
				while ( i-- ) {
					if ( seed[ (j = matchIndexes[i]) ] ) {
						seed[j] = !(matches[j] = seed[j]);
					}
				}
			});
		});
	}
	
	/**
	 * Checks a node for validity as a Sizzle context
	 * @param {Element|Object=} context
	 * @returns {Element|Object|Boolean} The input node if acceptable, otherwise a falsy value
	 */
	function testContext( context ) {
		return context && typeof context.getElementsByTagName !== "undefined" && context;
	}
	
	// Expose support vars for convenience
	support = Sizzle.support = {};
	
	/**
	 * Detects XML nodes
	 * @param {Element|Object} elem An element or a document
	 * @returns {Boolean} True iff elem is a non-HTML XML node
	 */
	isXML = Sizzle.isXML = function( elem ) {
		// documentElement is verified for cases where it doesn't yet exist
		// (such as loading iframes in IE - #4833)
		var documentElement = elem && (elem.ownerDocument || elem).documentElement;
		return documentElement ? documentElement.nodeName !== "HTML" : false;
	};
	
	/**
	 * Sets document-related variables once based on the current document
	 * @param {Element|Object} [doc] An element or document object to use to set the document
	 * @returns {Object} Returns the current document
	 */
	setDocument = Sizzle.setDocument = function( node ) {
		var hasCompare, subWindow,
			doc = node ? node.ownerDocument || node : preferredDoc;
	
		// Return early if doc is invalid or already selected
		if ( doc === document || doc.nodeType !== 9 || !doc.documentElement ) {
			return document;
		}
	
		// Update global variables
		document = doc;
		docElem = document.documentElement;
		documentIsHTML = !isXML( document );
	
		// Support: IE 9-11, Edge
		// Accessing iframe documents after unload throws "permission denied" errors (jQuery #13936)
		if ( preferredDoc !== document &&
			(subWindow = document.defaultView) && subWindow.top !== subWindow ) {
	
			// Support: IE 11, Edge
			if ( subWindow.addEventListener ) {
				subWindow.addEventListener( "unload", unloadHandler, false );
	
			// Support: IE 9 - 10 only
			} else if ( subWindow.attachEvent ) {
				subWindow.attachEvent( "onunload", unloadHandler );
			}
		}
	
		/* Attributes
		---------------------------------------------------------------------- */
	
		// Support: IE<8
		// Verify that getAttribute really returns attributes and not properties
		// (excepting IE8 booleans)
		support.attributes = assert(function( el ) {
			el.className = "i";
			return !el.getAttribute("className");
		});
	
		/* getElement(s)By*
		---------------------------------------------------------------------- */
	
		// Check if getElementsByTagName("*") returns only elements
		support.getElementsByTagName = assert(function( el ) {
			el.appendChild( document.createComment("") );
			return !el.getElementsByTagName("*").length;
		});
	
		// Support: IE<9
		support.getElementsByClassName = rnative.test( document.getElementsByClassName );
	
		// Support: IE<10
		// Check if getElementById returns elements by name
		// The broken getElementById methods don't pick up programmatically-set names,
		// so use a roundabout getElementsByName test
		support.getById = assert(function( el ) {
			docElem.appendChild( el ).id = expando;
			return !document.getElementsByName || !document.getElementsByName( expando ).length;
		});
	
		// ID filter and find
		if ( support.getById ) {
			Expr.filter["ID"] = function( id ) {
				var attrId = id.replace( runescape, funescape );
				return function( elem ) {
					return elem.getAttribute("id") === attrId;
				};
			};
			Expr.find["ID"] = function( id, context ) {
				if ( typeof context.getElementById !== "undefined" && documentIsHTML ) {
					var elem = context.getElementById( id );
					return elem ? [ elem ] : [];
				}
			};
		} else {
			Expr.filter["ID"] =  function( id ) {
				var attrId = id.replace( runescape, funescape );
				return function( elem ) {
					var node = typeof elem.getAttributeNode !== "undefined" &&
						elem.getAttributeNode("id");
					return node && node.value === attrId;
				};
			};
	
			// Support: IE 6 - 7 only
			// getElementById is not reliable as a find shortcut
			Expr.find["ID"] = function( id, context ) {
				if ( typeof context.getElementById !== "undefined" && documentIsHTML ) {
					var node, i, elems,
						elem = context.getElementById( id );
	
					if ( elem ) {
	
						// Verify the id attribute
						node = elem.getAttributeNode("id");
						if ( node && node.value === id ) {
							return [ elem ];
						}
	
						// Fall back on getElementsByName
						elems = context.getElementsByName( id );
						i = 0;
						while ( (elem = elems[i++]) ) {
							node = elem.getAttributeNode("id");
							if ( node && node.value === id ) {
								return [ elem ];
							}
						}
					}
	
					return [];
				}
			};
		}
	
		// Tag
		Expr.find["TAG"] = support.getElementsByTagName ?
			function( tag, context ) {
				if ( typeof context.getElementsByTagName !== "undefined" ) {
					return context.getElementsByTagName( tag );
	
				// DocumentFragment nodes don't have gEBTN
				} else if ( support.qsa ) {
					return context.querySelectorAll( tag );
				}
			} :
	
			function( tag, context ) {
				var elem,
					tmp = [],
					i = 0,
					// By happy coincidence, a (broken) gEBTN appears on DocumentFragment nodes too
					results = context.getElementsByTagName( tag );
	
				// Filter out possible comments
				if ( tag === "*" ) {
					while ( (elem = results[i++]) ) {
						if ( elem.nodeType === 1 ) {
							tmp.push( elem );
						}
					}
	
					return tmp;
				}
				return results;
			};
	
		// Class
		Expr.find["CLASS"] = support.getElementsByClassName && function( className, context ) {
			if ( typeof context.getElementsByClassName !== "undefined" && documentIsHTML ) {
				return context.getElementsByClassName( className );
			}
		};
	
		/* QSA/matchesSelector
		---------------------------------------------------------------------- */
	
		// QSA and matchesSelector support
	
		// matchesSelector(:active) reports false when true (IE9/Opera 11.5)
		rbuggyMatches = [];
	
		// qSa(:focus) reports false when true (Chrome 21)
		// We allow this because of a bug in IE8/9 that throws an error
		// whenever `document.activeElement` is accessed on an iframe
		// So, we allow :focus to pass through QSA all the time to avoid the IE error
		// See https://bugs.jquery.com/ticket/13378
		rbuggyQSA = [];
	
		if ( (support.qsa = rnative.test( document.querySelectorAll )) ) {
			// Build QSA regex
			// Regex strategy adopted from Diego Perini
			assert(function( el ) {
				// Select is set to empty string on purpose
				// This is to test IE's treatment of not explicitly
				// setting a boolean content attribute,
				// since its presence should be enough
				// https://bugs.jquery.com/ticket/12359
				docElem.appendChild( el ).innerHTML = "<a id='" + expando + "'></a>" +
					"<select id='" + expando + "-\r\\' msallowcapture=''>" +
					"<option selected=''></option></select>";
	
				// Support: IE8, Opera 11-12.16
				// Nothing should be selected when empty strings follow ^= or $= or *=
				// The test attribute must be unknown in Opera but "safe" for WinRT
				// https://msdn.microsoft.com/en-us/library/ie/hh465388.aspx#attribute_section
				if ( el.querySelectorAll("[msallowcapture^='']").length ) {
					rbuggyQSA.push( "[*^$]=" + whitespace + "*(?:''|\"\")" );
				}
	
				// Support: IE8
				// Boolean attributes and "value" are not treated correctly
				if ( !el.querySelectorAll("[selected]").length ) {
					rbuggyQSA.push( "\\[" + whitespace + "*(?:value|" + booleans + ")" );
				}
	
				// Support: Chrome<29, Android<4.4, Safari<7.0+, iOS<7.0+, PhantomJS<1.9.8+
				if ( !el.querySelectorAll( "[id~=" + expando + "-]" ).length ) {
					rbuggyQSA.push("~=");
				}
	
				// Webkit/Opera - :checked should return selected option elements
				// http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
				// IE8 throws error here and will not see later tests
				if ( !el.querySelectorAll(":checked").length ) {
					rbuggyQSA.push(":checked");
				}
	
				// Support: Safari 8+, iOS 8+
				// https://bugs.webkit.org/show_bug.cgi?id=136851
				// In-page `selector#id sibling-combinator selector` fails
				if ( !el.querySelectorAll( "a#" + expando + "+*" ).length ) {
					rbuggyQSA.push(".#.+[+~]");
				}
			});
	
			assert(function( el ) {
				el.innerHTML = "<a href='' disabled='disabled'></a>" +
					"<select disabled='disabled'><option/></select>";
	
				// Support: Windows 8 Native Apps
				// The type and name attributes are restricted during .innerHTML assignment
				var input = document.createElement("input");
				input.setAttribute( "type", "hidden" );
				el.appendChild( input ).setAttribute( "name", "D" );
	
				// Support: IE8
				// Enforce case-sensitivity of name attribute
				if ( el.querySelectorAll("[name=d]").length ) {
					rbuggyQSA.push( "name" + whitespace + "*[*^$|!~]?=" );
				}
	
				// FF 3.5 - :enabled/:disabled and hidden elements (hidden elements are still enabled)
				// IE8 throws error here and will not see later tests
				if ( el.querySelectorAll(":enabled").length !== 2 ) {
					rbuggyQSA.push( ":enabled", ":disabled" );
				}
	
				// Support: IE9-11+
				// IE's :disabled selector does not pick up the children of disabled fieldsets
				docElem.appendChild( el ).disabled = true;
				if ( el.querySelectorAll(":disabled").length !== 2 ) {
					rbuggyQSA.push( ":enabled", ":disabled" );
				}
	
				// Opera 10-11 does not throw on post-comma invalid pseudos
				el.querySelectorAll("*,:x");
				rbuggyQSA.push(",.*:");
			});
		}
	
		if ( (support.matchesSelector = rnative.test( (matches = docElem.matches ||
			docElem.webkitMatchesSelector ||
			docElem.mozMatchesSelector ||
			docElem.oMatchesSelector ||
			docElem.msMatchesSelector) )) ) {
	
			assert(function( el ) {
				// Check to see if it's possible to do matchesSelector
				// on a disconnected node (IE 9)
				support.disconnectedMatch = matches.call( el, "*" );
	
				// This should fail with an exception
				// Gecko does not error, returns false instead
				matches.call( el, "[s!='']:x" );
				rbuggyMatches.push( "!=", pseudos );
			});
		}
	
		rbuggyQSA = rbuggyQSA.length && new RegExp( rbuggyQSA.join("|") );
		rbuggyMatches = rbuggyMatches.length && new RegExp( rbuggyMatches.join("|") );
	
		/* Contains
		---------------------------------------------------------------------- */
		hasCompare = rnative.test( docElem.compareDocumentPosition );
	
		// Element contains another
		// Purposefully self-exclusive
		// As in, an element does not contain itself
		contains = hasCompare || rnative.test( docElem.contains ) ?
			function( a, b ) {
				var adown = a.nodeType === 9 ? a.documentElement : a,
					bup = b && b.parentNode;
				return a === bup || !!( bup && bup.nodeType === 1 && (
					adown.contains ?
						adown.contains( bup ) :
						a.compareDocumentPosition && a.compareDocumentPosition( bup ) & 16
				));
			} :
			function( a, b ) {
				if ( b ) {
					while ( (b = b.parentNode) ) {
						if ( b === a ) {
							return true;
						}
					}
				}
				return false;
			};
	
		/* Sorting
		---------------------------------------------------------------------- */
	
		// Document order sorting
		sortOrder = hasCompare ?
		function( a, b ) {
	
			// Flag for duplicate removal
			if ( a === b ) {
				hasDuplicate = true;
				return 0;
			}
	
			// Sort on method existence if only one input has compareDocumentPosition
			var compare = !a.compareDocumentPosition - !b.compareDocumentPosition;
			if ( compare ) {
				return compare;
			}
	
			// Calculate position if both inputs belong to the same document
			compare = ( a.ownerDocument || a ) === ( b.ownerDocument || b ) ?
				a.compareDocumentPosition( b ) :
	
				// Otherwise we know they are disconnected
				1;
	
			// Disconnected nodes
			if ( compare & 1 ||
				(!support.sortDetached && b.compareDocumentPosition( a ) === compare) ) {
	
				// Choose the first element that is related to our preferred document
				if ( a === document || a.ownerDocument === preferredDoc && contains(preferredDoc, a) ) {
					return -1;
				}
				if ( b === document || b.ownerDocument === preferredDoc && contains(preferredDoc, b) ) {
					return 1;
				}
	
				// Maintain original order
				return sortInput ?
					( indexOf( sortInput, a ) - indexOf( sortInput, b ) ) :
					0;
			}
	
			return compare & 4 ? -1 : 1;
		} :
		function( a, b ) {
			// Exit early if the nodes are identical
			if ( a === b ) {
				hasDuplicate = true;
				return 0;
			}
	
			var cur,
				i = 0,
				aup = a.parentNode,
				bup = b.parentNode,
				ap = [ a ],
				bp = [ b ];
	
			// Parentless nodes are either documents or disconnected
			if ( !aup || !bup ) {
				return a === document ? -1 :
					b === document ? 1 :
					aup ? -1 :
					bup ? 1 :
					sortInput ?
					( indexOf( sortInput, a ) - indexOf( sortInput, b ) ) :
					0;
	
			// If the nodes are siblings, we can do a quick check
			} else if ( aup === bup ) {
				return siblingCheck( a, b );
			}
	
			// Otherwise we need full lists of their ancestors for comparison
			cur = a;
			while ( (cur = cur.parentNode) ) {
				ap.unshift( cur );
			}
			cur = b;
			while ( (cur = cur.parentNode) ) {
				bp.unshift( cur );
			}
	
			// Walk down the tree looking for a discrepancy
			while ( ap[i] === bp[i] ) {
				i++;
			}
	
			return i ?
				// Do a sibling check if the nodes have a common ancestor
				siblingCheck( ap[i], bp[i] ) :
	
				// Otherwise nodes in our document sort first
				ap[i] === preferredDoc ? -1 :
				bp[i] === preferredDoc ? 1 :
				0;
		};
	
		return document;
	};
	
	Sizzle.matches = function( expr, elements ) {
		return Sizzle( expr, null, null, elements );
	};
	
	Sizzle.matchesSelector = function( elem, expr ) {
		// Set document vars if needed
		if ( ( elem.ownerDocument || elem ) !== document ) {
			setDocument( elem );
		}
	
		// Make sure that attribute selectors are quoted
		expr = expr.replace( rattributeQuotes, "='$1']" );
	
		if ( support.matchesSelector && documentIsHTML &&
			!compilerCache[ expr + " " ] &&
			( !rbuggyMatches || !rbuggyMatches.test( expr ) ) &&
			( !rbuggyQSA     || !rbuggyQSA.test( expr ) ) ) {
	
			try {
				var ret = matches.call( elem, expr );
	
				// IE 9's matchesSelector returns false on disconnected nodes
				if ( ret || support.disconnectedMatch ||
						// As well, disconnected nodes are said to be in a document
						// fragment in IE 9
						elem.document && elem.document.nodeType !== 11 ) {
					return ret;
				}
			} catch (e) {}
		}
	
		return Sizzle( expr, document, null, [ elem ] ).length > 0;
	};
	
	Sizzle.contains = function( context, elem ) {
		// Set document vars if needed
		if ( ( context.ownerDocument || context ) !== document ) {
			setDocument( context );
		}
		return contains( context, elem );
	};
	
	Sizzle.attr = function( elem, name ) {
		// Set document vars if needed
		if ( ( elem.ownerDocument || elem ) !== document ) {
			setDocument( elem );
		}
	
		var fn = Expr.attrHandle[ name.toLowerCase() ],
			// Don't get fooled by Object.prototype properties (jQuery #13807)
			val = fn && hasOwn.call( Expr.attrHandle, name.toLowerCase() ) ?
				fn( elem, name, !documentIsHTML ) :
				undefined;
	
		return val !== undefined ?
			val :
			support.attributes || !documentIsHTML ?
				elem.getAttribute( name ) :
				(val = elem.getAttributeNode(name)) && val.specified ?
					val.value :
					null;
	};
	
	Sizzle.escape = function( sel ) {
		return (sel + "").replace( rcssescape, fcssescape );
	};
	
	Sizzle.error = function( msg ) {
		throw new Error( "Syntax error, unrecognized expression: " + msg );
	};
	
	/**
	 * Document sorting and removing duplicates
	 * @param {ArrayLike} results
	 */
	Sizzle.uniqueSort = function( results ) {
		var elem,
			duplicates = [],
			j = 0,
			i = 0;
	
		// Unless we *know* we can detect duplicates, assume their presence
		hasDuplicate = !support.detectDuplicates;
		sortInput = !support.sortStable && results.slice( 0 );
		results.sort( sortOrder );
	
		if ( hasDuplicate ) {
			while ( (elem = results[i++]) ) {
				if ( elem === results[ i ] ) {
					j = duplicates.push( i );
				}
			}
			while ( j-- ) {
				results.splice( duplicates[ j ], 1 );
			}
		}
	
		// Clear input after sorting to release objects
		// See https://github.com/jquery/sizzle/pull/225
		sortInput = null;
	
		return results;
	};
	
	/**
	 * Utility function for retrieving the text value of an array of DOM nodes
	 * @param {Array|Element} elem
	 */
	getText = Sizzle.getText = function( elem ) {
		var node,
			ret = "",
			i = 0,
			nodeType = elem.nodeType;
	
		if ( !nodeType ) {
			// If no nodeType, this is expected to be an array
			while ( (node = elem[i++]) ) {
				// Do not traverse comment nodes
				ret += getText( node );
			}
		} else if ( nodeType === 1 || nodeType === 9 || nodeType === 11 ) {
			// Use textContent for elements
			// innerText usage removed for consistency of new lines (jQuery #11153)
			if ( typeof elem.textContent === "string" ) {
				return elem.textContent;
			} else {
				// Traverse its children
				for ( elem = elem.firstChild; elem; elem = elem.nextSibling ) {
					ret += getText( elem );
				}
			}
		} else if ( nodeType === 3 || nodeType === 4 ) {
			return elem.nodeValue;
		}
		// Do not include comment or processing instruction nodes
	
		return ret;
	};
	
	Expr = Sizzle.selectors = {
	
		// Can be adjusted by the user
		cacheLength: 50,
	
		createPseudo: markFunction,
	
		match: matchExpr,
	
		attrHandle: {},
	
		find: {},
	
		relative: {
			">": { dir: "parentNode", first: true },
			" ": { dir: "parentNode" },
			"+": { dir: "previousSibling", first: true },
			"~": { dir: "previousSibling" }
		},
	
		preFilter: {
			"ATTR": function( match ) {
				match[1] = match[1].replace( runescape, funescape );
	
				// Move the given value to match[3] whether quoted or unquoted
				match[3] = ( match[3] || match[4] || match[5] || "" ).replace( runescape, funescape );
	
				if ( match[2] === "~=" ) {
					match[3] = " " + match[3] + " ";
				}
	
				return match.slice( 0, 4 );
			},
	
			"CHILD": function( match ) {
				/* matches from matchExpr["CHILD"]
					1 type (only|nth|...)
					2 what (child|of-type)
					3 argument (even|odd|\d*|\d*n([+-]\d+)?|...)
					4 xn-component of xn+y argument ([+-]?\d*n|)
					5 sign of xn-component
					6 x of xn-component
					7 sign of y-component
					8 y of y-component
				*/
				match[1] = match[1].toLowerCase();
	
				if ( match[1].slice( 0, 3 ) === "nth" ) {
					// nth-* requires argument
					if ( !match[3] ) {
						Sizzle.error( match[0] );
					}
	
					// numeric x and y parameters for Expr.filter.CHILD
					// remember that false/true cast respectively to 0/1
					match[4] = +( match[4] ? match[5] + (match[6] || 1) : 2 * ( match[3] === "even" || match[3] === "odd" ) );
					match[5] = +( ( match[7] + match[8] ) || match[3] === "odd" );
	
				// other types prohibit arguments
				} else if ( match[3] ) {
					Sizzle.error( match[0] );
				}
	
				return match;
			},
	
			"PSEUDO": function( match ) {
				var excess,
					unquoted = !match[6] && match[2];
	
				if ( matchExpr["CHILD"].test( match[0] ) ) {
					return null;
				}
	
				// Accept quoted arguments as-is
				if ( match[3] ) {
					match[2] = match[4] || match[5] || "";
	
				// Strip excess characters from unquoted arguments
				} else if ( unquoted && rpseudo.test( unquoted ) &&
					// Get excess from tokenize (recursively)
					(excess = tokenize( unquoted, true )) &&
					// advance to the next closing parenthesis
					(excess = unquoted.indexOf( ")", unquoted.length - excess ) - unquoted.length) ) {
	
					// excess is a negative index
					match[0] = match[0].slice( 0, excess );
					match[2] = unquoted.slice( 0, excess );
				}
	
				// Return only captures needed by the pseudo filter method (type and argument)
				return match.slice( 0, 3 );
			}
		},
	
		filter: {
	
			"TAG": function( nodeNameSelector ) {
				var nodeName = nodeNameSelector.replace( runescape, funescape ).toLowerCase();
				return nodeNameSelector === "*" ?
					function() { return true; } :
					function( elem ) {
						return elem.nodeName && elem.nodeName.toLowerCase() === nodeName;
					};
			},
	
			"CLASS": function( className ) {
				var pattern = classCache[ className + " " ];
	
				return pattern ||
					(pattern = new RegExp( "(^|" + whitespace + ")" + className + "(" + whitespace + "|$)" )) &&
					classCache( className, function( elem ) {
						return pattern.test( typeof elem.className === "string" && elem.className || typeof elem.getAttribute !== "undefined" && elem.getAttribute("class") || "" );
					});
			},
	
			"ATTR": function( name, operator, check ) {
				return function( elem ) {
					var result = Sizzle.attr( elem, name );
	
					if ( result == null ) {
						return operator === "!=";
					}
					if ( !operator ) {
						return true;
					}
	
					result += "";
	
					return operator === "=" ? result === check :
						operator === "!=" ? result !== check :
						operator === "^=" ? check && result.indexOf( check ) === 0 :
						operator === "*=" ? check && result.indexOf( check ) > -1 :
						operator === "$=" ? check && result.slice( -check.length ) === check :
						operator === "~=" ? ( " " + result.replace( rwhitespace, " " ) + " " ).indexOf( check ) > -1 :
						operator === "|=" ? result === check || result.slice( 0, check.length + 1 ) === check + "-" :
						false;
				};
			},
	
			"CHILD": function( type, what, argument, first, last ) {
				var simple = type.slice( 0, 3 ) !== "nth",
					forward = type.slice( -4 ) !== "last",
					ofType = what === "of-type";
	
				return first === 1 && last === 0 ?
	
					// Shortcut for :nth-*(n)
					function( elem ) {
						return !!elem.parentNode;
					} :
	
					function( elem, context, xml ) {
						var cache, uniqueCache, outerCache, node, nodeIndex, start,
							dir = simple !== forward ? "nextSibling" : "previousSibling",
							parent = elem.parentNode,
							name = ofType && elem.nodeName.toLowerCase(),
							useCache = !xml && !ofType,
							diff = false;
	
						if ( parent ) {
	
							// :(first|last|only)-(child|of-type)
							if ( simple ) {
								while ( dir ) {
									node = elem;
									while ( (node = node[ dir ]) ) {
										if ( ofType ?
											node.nodeName.toLowerCase() === name :
											node.nodeType === 1 ) {
	
											return false;
										}
									}
									// Reverse direction for :only-* (if we haven't yet done so)
									start = dir = type === "only" && !start && "nextSibling";
								}
								return true;
							}
	
							start = [ forward ? parent.firstChild : parent.lastChild ];
	
							// non-xml :nth-child(...) stores cache data on `parent`
							if ( forward && useCache ) {
	
								// Seek `elem` from a previously-cached index
	
								// ...in a gzip-friendly way
								node = parent;
								outerCache = node[ expando ] || (node[ expando ] = {});
	
								// Support: IE <9 only
								// Defend against cloned attroperties (jQuery gh-1709)
								uniqueCache = outerCache[ node.uniqueID ] ||
									(outerCache[ node.uniqueID ] = {});
	
								cache = uniqueCache[ type ] || [];
								nodeIndex = cache[ 0 ] === dirruns && cache[ 1 ];
								diff = nodeIndex && cache[ 2 ];
								node = nodeIndex && parent.childNodes[ nodeIndex ];
	
								while ( (node = ++nodeIndex && node && node[ dir ] ||
	
									// Fallback to seeking `elem` from the start
									(diff = nodeIndex = 0) || start.pop()) ) {
	
									// When found, cache indexes on `parent` and break
									if ( node.nodeType === 1 && ++diff && node === elem ) {
										uniqueCache[ type ] = [ dirruns, nodeIndex, diff ];
										break;
									}
								}
	
							} else {
								// Use previously-cached element index if available
								if ( useCache ) {
									// ...in a gzip-friendly way
									node = elem;
									outerCache = node[ expando ] || (node[ expando ] = {});
	
									// Support: IE <9 only
									// Defend against cloned attroperties (jQuery gh-1709)
									uniqueCache = outerCache[ node.uniqueID ] ||
										(outerCache[ node.uniqueID ] = {});
	
									cache = uniqueCache[ type ] || [];
									nodeIndex = cache[ 0 ] === dirruns && cache[ 1 ];
									diff = nodeIndex;
								}
	
								// xml :nth-child(...)
								// or :nth-last-child(...) or :nth(-last)?-of-type(...)
								if ( diff === false ) {
									// Use the same loop as above to seek `elem` from the start
									while ( (node = ++nodeIndex && node && node[ dir ] ||
										(diff = nodeIndex = 0) || start.pop()) ) {
	
										if ( ( ofType ?
											node.nodeName.toLowerCase() === name :
											node.nodeType === 1 ) &&
											++diff ) {
	
											// Cache the index of each encountered element
											if ( useCache ) {
												outerCache = node[ expando ] || (node[ expando ] = {});
	
												// Support: IE <9 only
												// Defend against cloned attroperties (jQuery gh-1709)
												uniqueCache = outerCache[ node.uniqueID ] ||
													(outerCache[ node.uniqueID ] = {});
	
												uniqueCache[ type ] = [ dirruns, diff ];
											}
	
											if ( node === elem ) {
												break;
											}
										}
									}
								}
							}
	
							// Incorporate the offset, then check against cycle size
							diff -= last;
							return diff === first || ( diff % first === 0 && diff / first >= 0 );
						}
					};
			},
	
			"PSEUDO": function( pseudo, argument ) {
				// pseudo-class names are case-insensitive
				// http://www.w3.org/TR/selectors/#pseudo-classes
				// Prioritize by case sensitivity in case custom pseudos are added with uppercase letters
				// Remember that setFilters inherits from pseudos
				var args,
					fn = Expr.pseudos[ pseudo ] || Expr.setFilters[ pseudo.toLowerCase() ] ||
						Sizzle.error( "unsupported pseudo: " + pseudo );
	
				// The user may use createPseudo to indicate that
				// arguments are needed to create the filter function
				// just as Sizzle does
				if ( fn[ expando ] ) {
					return fn( argument );
				}
	
				// But maintain support for old signatures
				if ( fn.length > 1 ) {
					args = [ pseudo, pseudo, "", argument ];
					return Expr.setFilters.hasOwnProperty( pseudo.toLowerCase() ) ?
						markFunction(function( seed, matches ) {
							var idx,
								matched = fn( seed, argument ),
								i = matched.length;
							while ( i-- ) {
								idx = indexOf( seed, matched[i] );
								seed[ idx ] = !( matches[ idx ] = matched[i] );
							}
						}) :
						function( elem ) {
							return fn( elem, 0, args );
						};
				}
	
				return fn;
			}
		},
	
		pseudos: {
			// Potentially complex pseudos
			"not": markFunction(function( selector ) {
				// Trim the selector passed to compile
				// to avoid treating leading and trailing
				// spaces as combinators
				var input = [],
					results = [],
					matcher = compile( selector.replace( rtrim, "$1" ) );
	
				return matcher[ expando ] ?
					markFunction(function( seed, matches, context, xml ) {
						var elem,
							unmatched = matcher( seed, null, xml, [] ),
							i = seed.length;
	
						// Match elements unmatched by `matcher`
						while ( i-- ) {
							if ( (elem = unmatched[i]) ) {
								seed[i] = !(matches[i] = elem);
							}
						}
					}) :
					function( elem, context, xml ) {
						input[0] = elem;
						matcher( input, null, xml, results );
						// Don't keep the element (issue #299)
						input[0] = null;
						return !results.pop();
					};
			}),
	
			"has": markFunction(function( selector ) {
				return function( elem ) {
					return Sizzle( selector, elem ).length > 0;
				};
			}),
	
			"contains": markFunction(function( text ) {
				text = text.replace( runescape, funescape );
				return function( elem ) {
					return ( elem.textContent || elem.innerText || getText( elem ) ).indexOf( text ) > -1;
				};
			}),
	
			// "Whether an element is represented by a :lang() selector
			// is based solely on the element's language value
			// being equal to the identifier C,
			// or beginning with the identifier C immediately followed by "-".
			// The matching of C against the element's language value is performed case-insensitively.
			// The identifier C does not have to be a valid language name."
			// http://www.w3.org/TR/selectors/#lang-pseudo
			"lang": markFunction( function( lang ) {
				// lang value must be a valid identifier
				if ( !ridentifier.test(lang || "") ) {
					Sizzle.error( "unsupported lang: " + lang );
				}
				lang = lang.replace( runescape, funescape ).toLowerCase();
				return function( elem ) {
					var elemLang;
					do {
						if ( (elemLang = documentIsHTML ?
							elem.lang :
							elem.getAttribute("xml:lang") || elem.getAttribute("lang")) ) {
	
							elemLang = elemLang.toLowerCase();
							return elemLang === lang || elemLang.indexOf( lang + "-" ) === 0;
						}
					} while ( (elem = elem.parentNode) && elem.nodeType === 1 );
					return false;
				};
			}),
	
			// Miscellaneous
			"target": function( elem ) {
				var hash = window.location && window.location.hash;
				return hash && hash.slice( 1 ) === elem.id;
			},
	
			"root": function( elem ) {
				return elem === docElem;
			},
	
			"focus": function( elem ) {
				return elem === document.activeElement && (!document.hasFocus || document.hasFocus()) && !!(elem.type || elem.href || ~elem.tabIndex);
			},
	
			// Boolean properties
			"enabled": createDisabledPseudo( false ),
			"disabled": createDisabledPseudo( true ),
	
			"checked": function( elem ) {
				// In CSS3, :checked should return both checked and selected elements
				// http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
				var nodeName = elem.nodeName.toLowerCase();
				return (nodeName === "input" && !!elem.checked) || (nodeName === "option" && !!elem.selected);
			},
	
			"selected": function( elem ) {
				// Accessing this property makes selected-by-default
				// options in Safari work properly
				if ( elem.parentNode ) {
					elem.parentNode.selectedIndex;
				}
	
				return elem.selected === true;
			},
	
			// Contents
			"empty": function( elem ) {
				// http://www.w3.org/TR/selectors/#empty-pseudo
				// :empty is negated by element (1) or content nodes (text: 3; cdata: 4; entity ref: 5),
				//   but not by others (comment: 8; processing instruction: 7; etc.)
				// nodeType < 6 works because attributes (2) do not appear as children
				for ( elem = elem.firstChild; elem; elem = elem.nextSibling ) {
					if ( elem.nodeType < 6 ) {
						return false;
					}
				}
				return true;
			},
	
			"parent": function( elem ) {
				return !Expr.pseudos["empty"]( elem );
			},
	
			// Element/input types
			"header": function( elem ) {
				return rheader.test( elem.nodeName );
			},
	
			"input": function( elem ) {
				return rinputs.test( elem.nodeName );
			},
	
			"button": function( elem ) {
				var name = elem.nodeName.toLowerCase();
				return name === "input" && elem.type === "button" || name === "button";
			},
	
			"text": function( elem ) {
				var attr;
				return elem.nodeName.toLowerCase() === "input" &&
					elem.type === "text" &&
	
					// Support: IE<8
					// New HTML5 attribute values (e.g., "search") appear with elem.type === "text"
					( (attr = elem.getAttribute("type")) == null || attr.toLowerCase() === "text" );
			},
	
			// Position-in-collection
			"first": createPositionalPseudo(function() {
				return [ 0 ];
			}),
	
			"last": createPositionalPseudo(function( matchIndexes, length ) {
				return [ length - 1 ];
			}),
	
			"eq": createPositionalPseudo(function( matchIndexes, length, argument ) {
				return [ argument < 0 ? argument + length : argument ];
			}),
	
			"even": createPositionalPseudo(function( matchIndexes, length ) {
				var i = 0;
				for ( ; i < length; i += 2 ) {
					matchIndexes.push( i );
				}
				return matchIndexes;
			}),
	
			"odd": createPositionalPseudo(function( matchIndexes, length ) {
				var i = 1;
				for ( ; i < length; i += 2 ) {
					matchIndexes.push( i );
				}
				return matchIndexes;
			}),
	
			"lt": createPositionalPseudo(function( matchIndexes, length, argument ) {
				var i = argument < 0 ? argument + length : argument;
				for ( ; --i >= 0; ) {
					matchIndexes.push( i );
				}
				return matchIndexes;
			}),
	
			"gt": createPositionalPseudo(function( matchIndexes, length, argument ) {
				var i = argument < 0 ? argument + length : argument;
				for ( ; ++i < length; ) {
					matchIndexes.push( i );
				}
				return matchIndexes;
			})
		}
	};
	
	Expr.pseudos["nth"] = Expr.pseudos["eq"];
	
	// Add button/input type pseudos
	for ( i in { radio: true, checkbox: true, file: true, password: true, image: true } ) {
		Expr.pseudos[ i ] = createInputPseudo( i );
	}
	for ( i in { submit: true, reset: true } ) {
		Expr.pseudos[ i ] = createButtonPseudo( i );
	}
	
	// Easy API for creating new setFilters
	function setFilters() {}
	setFilters.prototype = Expr.filters = Expr.pseudos;
	Expr.setFilters = new setFilters();
	
	tokenize = Sizzle.tokenize = function( selector, parseOnly ) {
		var matched, match, tokens, type,
			soFar, groups, preFilters,
			cached = tokenCache[ selector + " " ];
	
		if ( cached ) {
			return parseOnly ? 0 : cached.slice( 0 );
		}
	
		soFar = selector;
		groups = [];
		preFilters = Expr.preFilter;
	
		while ( soFar ) {
	
			// Comma and first run
			if ( !matched || (match = rcomma.exec( soFar )) ) {
				if ( match ) {
					// Don't consume trailing commas as valid
					soFar = soFar.slice( match[0].length ) || soFar;
				}
				groups.push( (tokens = []) );
			}
	
			matched = false;
	
			// Combinators
			if ( (match = rcombinators.exec( soFar )) ) {
				matched = match.shift();
				tokens.push({
					value: matched,
					// Cast descendant combinators to space
					type: match[0].replace( rtrim, " " )
				});
				soFar = soFar.slice( matched.length );
			}
	
			// Filters
			for ( type in Expr.filter ) {
				if ( (match = matchExpr[ type ].exec( soFar )) && (!preFilters[ type ] ||
					(match = preFilters[ type ]( match ))) ) {
					matched = match.shift();
					tokens.push({
						value: matched,
						type: type,
						matches: match
					});
					soFar = soFar.slice( matched.length );
				}
			}
	
			if ( !matched ) {
				break;
			}
		}
	
		// Return the length of the invalid excess
		// if we're just parsing
		// Otherwise, throw an error or return tokens
		return parseOnly ?
			soFar.length :
			soFar ?
				Sizzle.error( selector ) :
				// Cache the tokens
				tokenCache( selector, groups ).slice( 0 );
	};
	
	function toSelector( tokens ) {
		var i = 0,
			len = tokens.length,
			selector = "";
		for ( ; i < len; i++ ) {
			selector += tokens[i].value;
		}
		return selector;
	}
	
	function addCombinator( matcher, combinator, base ) {
		var dir = combinator.dir,
			skip = combinator.next,
			key = skip || dir,
			checkNonElements = base && key === "parentNode",
			doneName = done++;
	
		return combinator.first ?
			// Check against closest ancestor/preceding element
			function( elem, context, xml ) {
				while ( (elem = elem[ dir ]) ) {
					if ( elem.nodeType === 1 || checkNonElements ) {
						return matcher( elem, context, xml );
					}
				}
				return false;
			} :
	
			// Check against all ancestor/preceding elements
			function( elem, context, xml ) {
				var oldCache, uniqueCache, outerCache,
					newCache = [ dirruns, doneName ];
	
				// We can't set arbitrary data on XML nodes, so they don't benefit from combinator caching
				if ( xml ) {
					while ( (elem = elem[ dir ]) ) {
						if ( elem.nodeType === 1 || checkNonElements ) {
							if ( matcher( elem, context, xml ) ) {
								return true;
							}
						}
					}
				} else {
					while ( (elem = elem[ dir ]) ) {
						if ( elem.nodeType === 1 || checkNonElements ) {
							outerCache = elem[ expando ] || (elem[ expando ] = {});
	
							// Support: IE <9 only
							// Defend against cloned attroperties (jQuery gh-1709)
							uniqueCache = outerCache[ elem.uniqueID ] || (outerCache[ elem.uniqueID ] = {});
	
							if ( skip && skip === elem.nodeName.toLowerCase() ) {
								elem = elem[ dir ] || elem;
							} else if ( (oldCache = uniqueCache[ key ]) &&
								oldCache[ 0 ] === dirruns && oldCache[ 1 ] === doneName ) {
	
								// Assign to newCache so results back-propagate to previous elements
								return (newCache[ 2 ] = oldCache[ 2 ]);
							} else {
								// Reuse newcache so results back-propagate to previous elements
								uniqueCache[ key ] = newCache;
	
								// A match means we're done; a fail means we have to keep checking
								if ( (newCache[ 2 ] = matcher( elem, context, xml )) ) {
									return true;
								}
							}
						}
					}
				}
				return false;
			};
	}
	
	function elementMatcher( matchers ) {
		return matchers.length > 1 ?
			function( elem, context, xml ) {
				var i = matchers.length;
				while ( i-- ) {
					if ( !matchers[i]( elem, context, xml ) ) {
						return false;
					}
				}
				return true;
			} :
			matchers[0];
	}
	
	function multipleContexts( selector, contexts, results ) {
		var i = 0,
			len = contexts.length;
		for ( ; i < len; i++ ) {
			Sizzle( selector, contexts[i], results );
		}
		return results;
	}
	
	function condense( unmatched, map, filter, context, xml ) {
		var elem,
			newUnmatched = [],
			i = 0,
			len = unmatched.length,
			mapped = map != null;
	
		for ( ; i < len; i++ ) {
			if ( (elem = unmatched[i]) ) {
				if ( !filter || filter( elem, context, xml ) ) {
					newUnmatched.push( elem );
					if ( mapped ) {
						map.push( i );
					}
				}
			}
		}
	
		return newUnmatched;
	}
	
	function setMatcher( preFilter, selector, matcher, postFilter, postFinder, postSelector ) {
		if ( postFilter && !postFilter[ expando ] ) {
			postFilter = setMatcher( postFilter );
		}
		if ( postFinder && !postFinder[ expando ] ) {
			postFinder = setMatcher( postFinder, postSelector );
		}
		return markFunction(function( seed, results, context, xml ) {
			var temp, i, elem,
				preMap = [],
				postMap = [],
				preexisting = results.length,
	
				// Get initial elements from seed or context
				elems = seed || multipleContexts( selector || "*", context.nodeType ? [ context ] : context, [] ),
	
				// Prefilter to get matcher input, preserving a map for seed-results synchronization
				matcherIn = preFilter && ( seed || !selector ) ?
					condense( elems, preMap, preFilter, context, xml ) :
					elems,
	
				matcherOut = matcher ?
					// If we have a postFinder, or filtered seed, or non-seed postFilter or preexisting results,
					postFinder || ( seed ? preFilter : preexisting || postFilter ) ?
	
						// ...intermediate processing is necessary
						[] :
	
						// ...otherwise use results directly
						results :
					matcherIn;
	
			// Find primary matches
			if ( matcher ) {
				matcher( matcherIn, matcherOut, context, xml );
			}
	
			// Apply postFilter
			if ( postFilter ) {
				temp = condense( matcherOut, postMap );
				postFilter( temp, [], context, xml );
	
				// Un-match failing elements by moving them back to matcherIn
				i = temp.length;
				while ( i-- ) {
					if ( (elem = temp[i]) ) {
						matcherOut[ postMap[i] ] = !(matcherIn[ postMap[i] ] = elem);
					}
				}
			}
	
			if ( seed ) {
				if ( postFinder || preFilter ) {
					if ( postFinder ) {
						// Get the final matcherOut by condensing this intermediate into postFinder contexts
						temp = [];
						i = matcherOut.length;
						while ( i-- ) {
							if ( (elem = matcherOut[i]) ) {
								// Restore matcherIn since elem is not yet a final match
								temp.push( (matcherIn[i] = elem) );
							}
						}
						postFinder( null, (matcherOut = []), temp, xml );
					}
	
					// Move matched elements from seed to results to keep them synchronized
					i = matcherOut.length;
					while ( i-- ) {
						if ( (elem = matcherOut[i]) &&
							(temp = postFinder ? indexOf( seed, elem ) : preMap[i]) > -1 ) {
	
							seed[temp] = !(results[temp] = elem);
						}
					}
				}
	
			// Add elements to results, through postFinder if defined
			} else {
				matcherOut = condense(
					matcherOut === results ?
						matcherOut.splice( preexisting, matcherOut.length ) :
						matcherOut
				);
				if ( postFinder ) {
					postFinder( null, results, matcherOut, xml );
				} else {
					push.apply( results, matcherOut );
				}
			}
		});
	}
	
	function matcherFromTokens( tokens ) {
		var checkContext, matcher, j,
			len = tokens.length,
			leadingRelative = Expr.relative[ tokens[0].type ],
			implicitRelative = leadingRelative || Expr.relative[" "],
			i = leadingRelative ? 1 : 0,
	
			// The foundational matcher ensures that elements are reachable from top-level context(s)
			matchContext = addCombinator( function( elem ) {
				return elem === checkContext;
			}, implicitRelative, true ),
			matchAnyContext = addCombinator( function( elem ) {
				return indexOf( checkContext, elem ) > -1;
			}, implicitRelative, true ),
			matchers = [ function( elem, context, xml ) {
				var ret = ( !leadingRelative && ( xml || context !== outermostContext ) ) || (
					(checkContext = context).nodeType ?
						matchContext( elem, context, xml ) :
						matchAnyContext( elem, context, xml ) );
				// Avoid hanging onto element (issue #299)
				checkContext = null;
				return ret;
			} ];
	
		for ( ; i < len; i++ ) {
			if ( (matcher = Expr.relative[ tokens[i].type ]) ) {
				matchers = [ addCombinator(elementMatcher( matchers ), matcher) ];
			} else {
				matcher = Expr.filter[ tokens[i].type ].apply( null, tokens[i].matches );
	
				// Return special upon seeing a positional matcher
				if ( matcher[ expando ] ) {
					// Find the next relative operator (if any) for proper handling
					j = ++i;
					for ( ; j < len; j++ ) {
						if ( Expr.relative[ tokens[j].type ] ) {
							break;
						}
					}
					return setMatcher(
						i > 1 && elementMatcher( matchers ),
						i > 1 && toSelector(
							// If the preceding token was a descendant combinator, insert an implicit any-element `*`
							tokens.slice( 0, i - 1 ).concat({ value: tokens[ i - 2 ].type === " " ? "*" : "" })
						).replace( rtrim, "$1" ),
						matcher,
						i < j && matcherFromTokens( tokens.slice( i, j ) ),
						j < len && matcherFromTokens( (tokens = tokens.slice( j )) ),
						j < len && toSelector( tokens )
					);
				}
				matchers.push( matcher );
			}
		}
	
		return elementMatcher( matchers );
	}
	
	function matcherFromGroupMatchers( elementMatchers, setMatchers ) {
		var bySet = setMatchers.length > 0,
			byElement = elementMatchers.length > 0,
			superMatcher = function( seed, context, xml, results, outermost ) {
				var elem, j, matcher,
					matchedCount = 0,
					i = "0",
					unmatched = seed && [],
					setMatched = [],
					contextBackup = outermostContext,
					// We must always have either seed elements or outermost context
					elems = seed || byElement && Expr.find["TAG"]( "*", outermost ),
					// Use integer dirruns iff this is the outermost matcher
					dirrunsUnique = (dirruns += contextBackup == null ? 1 : Math.random() || 0.1),
					len = elems.length;
	
				if ( outermost ) {
					outermostContext = context === document || context || outermost;
				}
	
				// Add elements passing elementMatchers directly to results
				// Support: IE<9, Safari
				// Tolerate NodeList properties (IE: "length"; Safari: <number>) matching elements by id
				for ( ; i !== len && (elem = elems[i]) != null; i++ ) {
					if ( byElement && elem ) {
						j = 0;
						if ( !context && elem.ownerDocument !== document ) {
							setDocument( elem );
							xml = !documentIsHTML;
						}
						while ( (matcher = elementMatchers[j++]) ) {
							if ( matcher( elem, context || document, xml) ) {
								results.push( elem );
								break;
							}
						}
						if ( outermost ) {
							dirruns = dirrunsUnique;
						}
					}
	
					// Track unmatched elements for set filters
					if ( bySet ) {
						// They will have gone through all possible matchers
						if ( (elem = !matcher && elem) ) {
							matchedCount--;
						}
	
						// Lengthen the array for every element, matched or not
						if ( seed ) {
							unmatched.push( elem );
						}
					}
				}
	
				// `i` is now the count of elements visited above, and adding it to `matchedCount`
				// makes the latter nonnegative.
				matchedCount += i;
	
				// Apply set filters to unmatched elements
				// NOTE: This can be skipped if there are no unmatched elements (i.e., `matchedCount`
				// equals `i`), unless we didn't visit _any_ elements in the above loop because we have
				// no element matchers and no seed.
				// Incrementing an initially-string "0" `i` allows `i` to remain a string only in that
				// case, which will result in a "00" `matchedCount` that differs from `i` but is also
				// numerically zero.
				if ( bySet && i !== matchedCount ) {
					j = 0;
					while ( (matcher = setMatchers[j++]) ) {
						matcher( unmatched, setMatched, context, xml );
					}
	
					if ( seed ) {
						// Reintegrate element matches to eliminate the need for sorting
						if ( matchedCount > 0 ) {
							while ( i-- ) {
								if ( !(unmatched[i] || setMatched[i]) ) {
									setMatched[i] = pop.call( results );
								}
							}
						}
	
						// Discard index placeholder values to get only actual matches
						setMatched = condense( setMatched );
					}
	
					// Add matches to results
					push.apply( results, setMatched );
	
					// Seedless set matches succeeding multiple successful matchers stipulate sorting
					if ( outermost && !seed && setMatched.length > 0 &&
						( matchedCount + setMatchers.length ) > 1 ) {
	
						Sizzle.uniqueSort( results );
					}
				}
	
				// Override manipulation of globals by nested matchers
				if ( outermost ) {
					dirruns = dirrunsUnique;
					outermostContext = contextBackup;
				}
	
				return unmatched;
			};
	
		return bySet ?
			markFunction( superMatcher ) :
			superMatcher;
	}
	
	compile = Sizzle.compile = function( selector, match /* Internal Use Only */ ) {
		var i,
			setMatchers = [],
			elementMatchers = [],
			cached = compilerCache[ selector + " " ];
	
		if ( !cached ) {
			// Generate a function of recursive functions that can be used to check each element
			if ( !match ) {
				match = tokenize( selector );
			}
			i = match.length;
			while ( i-- ) {
				cached = matcherFromTokens( match[i] );
				if ( cached[ expando ] ) {
					setMatchers.push( cached );
				} else {
					elementMatchers.push( cached );
				}
			}
	
			// Cache the compiled function
			cached = compilerCache( selector, matcherFromGroupMatchers( elementMatchers, setMatchers ) );
	
			// Save selector and tokenization
			cached.selector = selector;
		}
		return cached;
	};
	
	/**
	 * A low-level selection function that works with Sizzle's compiled
	 *  selector functions
	 * @param {String|Function} selector A selector or a pre-compiled
	 *  selector function built with Sizzle.compile
	 * @param {Element} context
	 * @param {Array} [results]
	 * @param {Array} [seed] A set of elements to match against
	 */
	select = Sizzle.select = function( selector, context, results, seed ) {
		var i, tokens, token, type, find,
			compiled = typeof selector === "function" && selector,
			match = !seed && tokenize( (selector = compiled.selector || selector) );
	
		results = results || [];
	
		// Try to minimize operations if there is only one selector in the list and no seed
		// (the latter of which guarantees us context)
		if ( match.length === 1 ) {
	
			// Reduce context if the leading compound selector is an ID
			tokens = match[0] = match[0].slice( 0 );
			if ( tokens.length > 2 && (token = tokens[0]).type === "ID" &&
					context.nodeType === 9 && documentIsHTML && Expr.relative[ tokens[1].type ] ) {
	
				context = ( Expr.find["ID"]( token.matches[0].replace(runescape, funescape), context ) || [] )[0];
				if ( !context ) {
					return results;
	
				// Precompiled matchers will still verify ancestry, so step up a level
				} else if ( compiled ) {
					context = context.parentNode;
				}
	
				selector = selector.slice( tokens.shift().value.length );
			}
	
			// Fetch a seed set for right-to-left matching
			i = matchExpr["needsContext"].test( selector ) ? 0 : tokens.length;
			while ( i-- ) {
				token = tokens[i];
	
				// Abort if we hit a combinator
				if ( Expr.relative[ (type = token.type) ] ) {
					break;
				}
				if ( (find = Expr.find[ type ]) ) {
					// Search, expanding context for leading sibling combinators
					if ( (seed = find(
						token.matches[0].replace( runescape, funescape ),
						rsibling.test( tokens[0].type ) && testContext( context.parentNode ) || context
					)) ) {
	
						// If seed is empty or no tokens remain, we can return early
						tokens.splice( i, 1 );
						selector = seed.length && toSelector( tokens );
						if ( !selector ) {
							push.apply( results, seed );
							return results;
						}
	
						break;
					}
				}
			}
		}
	
		// Compile and execute a filtering function if one is not provided
		// Provide `match` to avoid retokenization if we modified the selector above
		( compiled || compile( selector, match ) )(
			seed,
			context,
			!documentIsHTML,
			results,
			!context || rsibling.test( selector ) && testContext( context.parentNode ) || context
		);
		return results;
	};
	
	// One-time assignments
	
	// Sort stability
	support.sortStable = expando.split("").sort( sortOrder ).join("") === expando;
	
	// Support: Chrome 14-35+
	// Always assume duplicates if they aren't passed to the comparison function
	support.detectDuplicates = !!hasDuplicate;
	
	// Initialize against the default document
	setDocument();
	
	// Support: Webkit<537.32 - Safari 6.0.3/Chrome 25 (fixed in Chrome 27)
	// Detached nodes confoundingly follow *each other*
	support.sortDetached = assert(function( el ) {
		// Should return 1, but returns 4 (following)
		return el.compareDocumentPosition( document.createElement("fieldset") ) & 1;
	});
	
	// Support: IE<8
	// Prevent attribute/property "interpolation"
	// https://msdn.microsoft.com/en-us/library/ms536429%28VS.85%29.aspx
	if ( !assert(function( el ) {
		el.innerHTML = "<a href='#'></a>";
		return el.firstChild.getAttribute("href") === "#" ;
	}) ) {
		addHandle( "type|href|height|width", function( elem, name, isXML ) {
			if ( !isXML ) {
				return elem.getAttribute( name, name.toLowerCase() === "type" ? 1 : 2 );
			}
		});
	}
	
	// Support: IE<9
	// Use defaultValue in place of getAttribute("value")
	if ( !support.attributes || !assert(function( el ) {
		el.innerHTML = "<input/>";
		el.firstChild.setAttribute( "value", "" );
		return el.firstChild.getAttribute( "value" ) === "";
	}) ) {
		addHandle( "value", function( elem, name, isXML ) {
			if ( !isXML && elem.nodeName.toLowerCase() === "input" ) {
				return elem.defaultValue;
			}
		});
	}
	
	// Support: IE<9
	// Use getAttributeNode to fetch booleans when getAttribute lies
	if ( !assert(function( el ) {
		return el.getAttribute("disabled") == null;
	}) ) {
		addHandle( booleans, function( elem, name, isXML ) {
			var val;
			if ( !isXML ) {
				return elem[ name ] === true ? name.toLowerCase() :
						(val = elem.getAttributeNode( name )) && val.specified ?
						val.value :
					null;
			}
		});
	}
	
	return Sizzle;
	
	})( window );
	
	
	
	jQuery.find = Sizzle;
	jQuery.expr = Sizzle.selectors;
	
	// Deprecated
	jQuery.expr[ ":" ] = jQuery.expr.pseudos;
	jQuery.uniqueSort = jQuery.unique = Sizzle.uniqueSort;
	jQuery.text = Sizzle.getText;
	jQuery.isXMLDoc = Sizzle.isXML;
	jQuery.contains = Sizzle.contains;
	jQuery.escapeSelector = Sizzle.escape;
	
	
	
	
	var dir = function( elem, dir, until ) {
		var matched = [],
			truncate = until !== undefined;
	
		while ( ( elem = elem[ dir ] ) && elem.nodeType !== 9 ) {
			if ( elem.nodeType === 1 ) {
				if ( truncate && jQuery( elem ).is( until ) ) {
					break;
				}
				matched.push( elem );
			}
		}
		return matched;
	};
	
	
	var siblings = function( n, elem ) {
		var matched = [];
	
		for ( ; n; n = n.nextSibling ) {
			if ( n.nodeType === 1 && n !== elem ) {
				matched.push( n );
			}
		}
	
		return matched;
	};
	
	
	var rneedsContext = jQuery.expr.match.needsContext;
	
	
	
	function nodeName( elem, name ) {
	
	  return elem.nodeName && elem.nodeName.toLowerCase() === name.toLowerCase();
	
	};
	var rsingleTag = ( /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i );
	
	
	
	var risSimple = /^.[^:#\[\.,]*$/;
	
	// Implement the identical functionality for filter and not
	function winnow( elements, qualifier, not ) {
		if ( jQuery.isFunction( qualifier ) ) {
			return jQuery.grep( elements, function( elem, i ) {
				return !!qualifier.call( elem, i, elem ) !== not;
			} );
		}
	
		// Single element
		if ( qualifier.nodeType ) {
			return jQuery.grep( elements, function( elem ) {
				return ( elem === qualifier ) !== not;
			} );
		}
	
		// Arraylike of elements (jQuery, arguments, Array)
		if ( typeof qualifier !== "string" ) {
			return jQuery.grep( elements, function( elem ) {
				return ( indexOf.call( qualifier, elem ) > -1 ) !== not;
			} );
		}
	
		// Simple selector that can be filtered directly, removing non-Elements
		if ( risSimple.test( qualifier ) ) {
			return jQuery.filter( qualifier, elements, not );
		}
	
		// Complex selector, compare the two sets, removing non-Elements
		qualifier = jQuery.filter( qualifier, elements );
		return jQuery.grep( elements, function( elem ) {
			return ( indexOf.call( qualifier, elem ) > -1 ) !== not && elem.nodeType === 1;
		} );
	}
	
	jQuery.filter = function( expr, elems, not ) {
		var elem = elems[ 0 ];
	
		if ( not ) {
			expr = ":not(" + expr + ")";
		}
	
		if ( elems.length === 1 && elem.nodeType === 1 ) {
			return jQuery.find.matchesSelector( elem, expr ) ? [ elem ] : [];
		}
	
		return jQuery.find.matches( expr, jQuery.grep( elems, function( elem ) {
			return elem.nodeType === 1;
		} ) );
	};
	
	jQuery.fn.extend( {
		find: function( selector ) {
			var i, ret,
				len = this.length,
				self = this;
	
			if ( typeof selector !== "string" ) {
				return this.pushStack( jQuery( selector ).filter( function() {
					for ( i = 0; i < len; i++ ) {
						if ( jQuery.contains( self[ i ], this ) ) {
							return true;
						}
					}
				} ) );
			}
	
			ret = this.pushStack( [] );
	
			for ( i = 0; i < len; i++ ) {
				jQuery.find( selector, self[ i ], ret );
			}
	
			return len > 1 ? jQuery.uniqueSort( ret ) : ret;
		},
		filter: function( selector ) {
			return this.pushStack( winnow( this, selector || [], false ) );
		},
		not: function( selector ) {
			return this.pushStack( winnow( this, selector || [], true ) );
		},
		is: function( selector ) {
			return !!winnow(
				this,
	
				// If this is a positional/relative selector, check membership in the returned set
				// so $("p:first").is("p:last") won't return true for a doc with two "p".
				typeof selector === "string" && rneedsContext.test( selector ) ?
					jQuery( selector ) :
					selector || [],
				false
			).length;
		}
	} );
	
	
	// Initialize a jQuery object
	
	
	// A central reference to the root jQuery(document)
	var rootjQuery,
	
		// A simple way to check for HTML strings
		// Prioritize #id over <tag> to avoid XSS via location.hash (#9521)
		// Strict HTML recognition (#11290: must start with <)
		// Shortcut simple #id case for speed
		rquickExpr = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/,
	
		init = jQuery.fn.init = function( selector, context, root ) {
			var match, elem;
	
			// HANDLE: $(""), $(null), $(undefined), $(false)
			if ( !selector ) {
				return this;
			}
	
			// Method init() accepts an alternate rootjQuery
			// so migrate can support jQuery.sub (gh-2101)
			root = root || rootjQuery;
	
			// Handle HTML strings
			if ( typeof selector === "string" ) {
				if ( selector[ 0 ] === "<" &&
					selector[ selector.length - 1 ] === ">" &&
					selector.length >= 3 ) {
	
					// Assume that strings that start and end with <> are HTML and skip the regex check
					match = [ null, selector, null ];
	
				} else {
					match = rquickExpr.exec( selector );
				}
	
				// Match html or make sure no context is specified for #id
				if ( match && ( match[ 1 ] || !context ) ) {
	
					// HANDLE: $(html) -> $(array)
					if ( match[ 1 ] ) {
						context = context instanceof jQuery ? context[ 0 ] : context;
	
						// Option to run scripts is true for back-compat
						// Intentionally let the error be thrown if parseHTML is not present
						jQuery.merge( this, jQuery.parseHTML(
							match[ 1 ],
							context && context.nodeType ? context.ownerDocument || context : document,
							true
						) );
	
						// HANDLE: $(html, props)
						if ( rsingleTag.test( match[ 1 ] ) && jQuery.isPlainObject( context ) ) {
							for ( match in context ) {
	
								// Properties of context are called as methods if possible
								if ( jQuery.isFunction( this[ match ] ) ) {
									this[ match ]( context[ match ] );
	
								// ...and otherwise set as attributes
								} else {
									this.attr( match, context[ match ] );
								}
							}
						}
	
						return this;
	
					// HANDLE: $(#id)
					} else {
						elem = document.getElementById( match[ 2 ] );
	
						if ( elem ) {
	
							// Inject the element directly into the jQuery object
							this[ 0 ] = elem;
							this.length = 1;
						}
						return this;
					}
	
				// HANDLE: $(expr, $(...))
				} else if ( !context || context.jquery ) {
					return ( context || root ).find( selector );
	
				// HANDLE: $(expr, context)
				// (which is just equivalent to: $(context).find(expr)
				} else {
					return this.constructor( context ).find( selector );
				}
	
			// HANDLE: $(DOMElement)
			} else if ( selector.nodeType ) {
				this[ 0 ] = selector;
				this.length = 1;
				return this;
	
			// HANDLE: $(function)
			// Shortcut for document ready
			} else if ( jQuery.isFunction( selector ) ) {
				return root.ready !== undefined ?
					root.ready( selector ) :
	
					// Execute immediately if ready is not present
					selector( jQuery );
			}
	
			return jQuery.makeArray( selector, this );
		};
	
	// Give the init function the jQuery prototype for later instantiation
	init.prototype = jQuery.fn;
	
	// Initialize central reference
	rootjQuery = jQuery( document );
	
	
	var rparentsprev = /^(?:parents|prev(?:Until|All))/,
	
		// Methods guaranteed to produce a unique set when starting from a unique set
		guaranteedUnique = {
			children: true,
			contents: true,
			next: true,
			prev: true
		};
	
	jQuery.fn.extend( {
		has: function( target ) {
			var targets = jQuery( target, this ),
				l = targets.length;
	
			return this.filter( function() {
				var i = 0;
				for ( ; i < l; i++ ) {
					if ( jQuery.contains( this, targets[ i ] ) ) {
						return true;
					}
				}
			} );
		},
	
		closest: function( selectors, context ) {
			var cur,
				i = 0,
				l = this.length,
				matched = [],
				targets = typeof selectors !== "string" && jQuery( selectors );
	
			// Positional selectors never match, since there's no _selection_ context
			if ( !rneedsContext.test( selectors ) ) {
				for ( ; i < l; i++ ) {
					for ( cur = this[ i ]; cur && cur !== context; cur = cur.parentNode ) {
	
						// Always skip document fragments
						if ( cur.nodeType < 11 && ( targets ?
							targets.index( cur ) > -1 :
	
							// Don't pass non-elements to Sizzle
							cur.nodeType === 1 &&
								jQuery.find.matchesSelector( cur, selectors ) ) ) {
	
							matched.push( cur );
							break;
						}
					}
				}
			}
	
			return this.pushStack( matched.length > 1 ? jQuery.uniqueSort( matched ) : matched );
		},
	
		// Determine the position of an element within the set
		index: function( elem ) {
	
			// No argument, return index in parent
			if ( !elem ) {
				return ( this[ 0 ] && this[ 0 ].parentNode ) ? this.first().prevAll().length : -1;
			}
	
			// Index in selector
			if ( typeof elem === "string" ) {
				return indexOf.call( jQuery( elem ), this[ 0 ] );
			}
	
			// Locate the position of the desired element
			return indexOf.call( this,
	
				// If it receives a jQuery object, the first element is used
				elem.jquery ? elem[ 0 ] : elem
			);
		},
	
		add: function( selector, context ) {
			return this.pushStack(
				jQuery.uniqueSort(
					jQuery.merge( this.get(), jQuery( selector, context ) )
				)
			);
		},
	
		addBack: function( selector ) {
			return this.add( selector == null ?
				this.prevObject : this.prevObject.filter( selector )
			);
		}
	} );
	
	function sibling( cur, dir ) {
		while ( ( cur = cur[ dir ] ) && cur.nodeType !== 1 ) {}
		return cur;
	}
	
	jQuery.each( {
		parent: function( elem ) {
			var parent = elem.parentNode;
			return parent && parent.nodeType !== 11 ? parent : null;
		},
		parents: function( elem ) {
			return dir( elem, "parentNode" );
		},
		parentsUntil: function( elem, i, until ) {
			return dir( elem, "parentNode", until );
		},
		next: function( elem ) {
			return sibling( elem, "nextSibling" );
		},
		prev: function( elem ) {
			return sibling( elem, "previousSibling" );
		},
		nextAll: function( elem ) {
			return dir( elem, "nextSibling" );
		},
		prevAll: function( elem ) {
			return dir( elem, "previousSibling" );
		},
		nextUntil: function( elem, i, until ) {
			return dir( elem, "nextSibling", until );
		},
		prevUntil: function( elem, i, until ) {
			return dir( elem, "previousSibling", until );
		},
		siblings: function( elem ) {
			return siblings( ( elem.parentNode || {} ).firstChild, elem );
		},
		children: function( elem ) {
			return siblings( elem.firstChild );
		},
		contents: function( elem ) {
	        if ( nodeName( elem, "iframe" ) ) {
	            return elem.contentDocument;
	        }
	
	        // Support: IE 9 - 11 only, iOS 7 only, Android Browser <=4.3 only
	        // Treat the template element as a regular one in browsers that
	        // don't support it.
	        if ( nodeName( elem, "template" ) ) {
	            elem = elem.content || elem;
	        }
	
	        return jQuery.merge( [], elem.childNodes );
		}
	}, function( name, fn ) {
		jQuery.fn[ name ] = function( until, selector ) {
			var matched = jQuery.map( this, fn, until );
	
			if ( name.slice( -5 ) !== "Until" ) {
				selector = until;
			}
	
			if ( selector && typeof selector === "string" ) {
				matched = jQuery.filter( selector, matched );
			}
	
			if ( this.length > 1 ) {
	
				// Remove duplicates
				if ( !guaranteedUnique[ name ] ) {
					jQuery.uniqueSort( matched );
				}
	
				// Reverse order for parents* and prev-derivatives
				if ( rparentsprev.test( name ) ) {
					matched.reverse();
				}
			}
	
			return this.pushStack( matched );
		};
	} );
	var rnothtmlwhite = ( /[^\x20\t\r\n\f]+/g );
	
	
	
	// Convert String-formatted options into Object-formatted ones
	function createOptions( options ) {
		var object = {};
		jQuery.each( options.match( rnothtmlwhite ) || [], function( _, flag ) {
			object[ flag ] = true;
		} );
		return object;
	}
	
	/*
	 * Create a callback list using the following parameters:
	 *
	 *	options: an optional list of space-separated options that will change how
	 *			the callback list behaves or a more traditional option object
	 *
	 * By default a callback list will act like an event callback list and can be
	 * "fired" multiple times.
	 *
	 * Possible options:
	 *
	 *	once:			will ensure the callback list can only be fired once (like a Deferred)
	 *
	 *	memory:			will keep track of previous values and will call any callback added
	 *					after the list has been fired right away with the latest "memorized"
	 *					values (like a Deferred)
	 *
	 *	unique:			will ensure a callback can only be added once (no duplicate in the list)
	 *
	 *	stopOnFalse:	interrupt callings when a callback returns false
	 *
	 */
	jQuery.Callbacks = function( options ) {
	
		// Convert options from String-formatted to Object-formatted if needed
		// (we check in cache first)
		options = typeof options === "string" ?
			createOptions( options ) :
			jQuery.extend( {}, options );
	
		var // Flag to know if list is currently firing
			firing,
	
			// Last fire value for non-forgettable lists
			memory,
	
			// Flag to know if list was already fired
			fired,
	
			// Flag to prevent firing
			locked,
	
			// Actual callback list
			list = [],
	
			// Queue of execution data for repeatable lists
			queue = [],
	
			// Index of currently firing callback (modified by add/remove as needed)
			firingIndex = -1,
	
			// Fire callbacks
			fire = function() {
	
				// Enforce single-firing
				locked = locked || options.once;
	
				// Execute callbacks for all pending executions,
				// respecting firingIndex overrides and runtime changes
				fired = firing = true;
				for ( ; queue.length; firingIndex = -1 ) {
					memory = queue.shift();
					while ( ++firingIndex < list.length ) {
	
						// Run callback and check for early termination
						if ( list[ firingIndex ].apply( memory[ 0 ], memory[ 1 ] ) === false &&
							options.stopOnFalse ) {
	
							// Jump to end and forget the data so .add doesn't re-fire
							firingIndex = list.length;
							memory = false;
						}
					}
				}
	
				// Forget the data if we're done with it
				if ( !options.memory ) {
					memory = false;
				}
	
				firing = false;
	
				// Clean up if we're done firing for good
				if ( locked ) {
	
					// Keep an empty list if we have data for future add calls
					if ( memory ) {
						list = [];
	
					// Otherwise, this object is spent
					} else {
						list = "";
					}
				}
			},
	
			// Actual Callbacks object
			self = {
	
				// Add a callback or a collection of callbacks to the list
				add: function() {
					if ( list ) {
	
						// If we have memory from a past run, we should fire after adding
						if ( memory && !firing ) {
							firingIndex = list.length - 1;
							queue.push( memory );
						}
	
						( function add( args ) {
							jQuery.each( args, function( _, arg ) {
								if ( jQuery.isFunction( arg ) ) {
									if ( !options.unique || !self.has( arg ) ) {
										list.push( arg );
									}
								} else if ( arg && arg.length && jQuery.type( arg ) !== "string" ) {
	
									// Inspect recursively
									add( arg );
								}
							} );
						} )( arguments );
	
						if ( memory && !firing ) {
							fire();
						}
					}
					return this;
				},
	
				// Remove a callback from the list
				remove: function() {
					jQuery.each( arguments, function( _, arg ) {
						var index;
						while ( ( index = jQuery.inArray( arg, list, index ) ) > -1 ) {
							list.splice( index, 1 );
	
							// Handle firing indexes
							if ( index <= firingIndex ) {
								firingIndex--;
							}
						}
					} );
					return this;
				},
	
				// Check if a given callback is in the list.
				// If no argument is given, return whether or not list has callbacks attached.
				has: function( fn ) {
					return fn ?
						jQuery.inArray( fn, list ) > -1 :
						list.length > 0;
				},
	
				// Remove all callbacks from the list
				empty: function() {
					if ( list ) {
						list = [];
					}
					return this;
				},
	
				// Disable .fire and .add
				// Abort any current/pending executions
				// Clear all callbacks and values
				disable: function() {
					locked = queue = [];
					list = memory = "";
					return this;
				},
				disabled: function() {
					return !list;
				},
	
				// Disable .fire
				// Also disable .add unless we have memory (since it would have no effect)
				// Abort any pending executions
				lock: function() {
					locked = queue = [];
					if ( !memory && !firing ) {
						list = memory = "";
					}
					return this;
				},
				locked: function() {
					return !!locked;
				},
	
				// Call all callbacks with the given context and arguments
				fireWith: function( context, args ) {
					if ( !locked ) {
						args = args || [];
						args = [ context, args.slice ? args.slice() : args ];
						queue.push( args );
						if ( !firing ) {
							fire();
						}
					}
					return this;
				},
	
				// Call all the callbacks with the given arguments
				fire: function() {
					self.fireWith( this, arguments );
					return this;
				},
	
				// To know if the callbacks have already been called at least once
				fired: function() {
					return !!fired;
				}
			};
	
		return self;
	};
	
	
	function Identity( v ) {
		return v;
	}
	function Thrower( ex ) {
		throw ex;
	}
	
	function adoptValue( value, resolve, reject, noValue ) {
		var method;
	
		try {
	
			// Check for promise aspect first to privilege synchronous behavior
			if ( value && jQuery.isFunction( ( method = value.promise ) ) ) {
				method.call( value ).done( resolve ).fail( reject );
	
			// Other thenables
			} else if ( value && jQuery.isFunction( ( method = value.then ) ) ) {
				method.call( value, resolve, reject );
	
			// Other non-thenables
			} else {
	
				// Control `resolve` arguments by letting Array#slice cast boolean `noValue` to integer:
				// * false: [ value ].slice( 0 ) => resolve( value )
				// * true: [ value ].slice( 1 ) => resolve()
				resolve.apply( undefined, [ value ].slice( noValue ) );
			}
	
		// For Promises/A+, convert exceptions into rejections
		// Since jQuery.when doesn't unwrap thenables, we can skip the extra checks appearing in
		// Deferred#then to conditionally suppress rejection.
		} catch ( value ) {
	
			// Support: Android 4.0 only
			// Strict mode functions invoked without .call/.apply get global-object context
			reject.apply( undefined, [ value ] );
		}
	}
	
	jQuery.extend( {
	
		Deferred: function( func ) {
			var tuples = [
	
					// action, add listener, callbacks,
					// ... .then handlers, argument index, [final state]
					[ "notify", "progress", jQuery.Callbacks( "memory" ),
						jQuery.Callbacks( "memory" ), 2 ],
					[ "resolve", "done", jQuery.Callbacks( "once memory" ),
						jQuery.Callbacks( "once memory" ), 0, "resolved" ],
					[ "reject", "fail", jQuery.Callbacks( "once memory" ),
						jQuery.Callbacks( "once memory" ), 1, "rejected" ]
				],
				state = "pending",
				promise = {
					state: function() {
						return state;
					},
					always: function() {
						deferred.done( arguments ).fail( arguments );
						return this;
					},
					"catch": function( fn ) {
						return promise.then( null, fn );
					},
	
					// Keep pipe for back-compat
					pipe: function( /* fnDone, fnFail, fnProgress */ ) {
						var fns = arguments;
	
						return jQuery.Deferred( function( newDefer ) {
							jQuery.each( tuples, function( i, tuple ) {
	
								// Map tuples (progress, done, fail) to arguments (done, fail, progress)
								var fn = jQuery.isFunction( fns[ tuple[ 4 ] ] ) && fns[ tuple[ 4 ] ];
	
								// deferred.progress(function() { bind to newDefer or newDefer.notify })
								// deferred.done(function() { bind to newDefer or newDefer.resolve })
								// deferred.fail(function() { bind to newDefer or newDefer.reject })
								deferred[ tuple[ 1 ] ]( function() {
									var returned = fn && fn.apply( this, arguments );
									if ( returned && jQuery.isFunction( returned.promise ) ) {
										returned.promise()
											.progress( newDefer.notify )
											.done( newDefer.resolve )
											.fail( newDefer.reject );
									} else {
										newDefer[ tuple[ 0 ] + "With" ](
											this,
											fn ? [ returned ] : arguments
										);
									}
								} );
							} );
							fns = null;
						} ).promise();
					},
					then: function( onFulfilled, onRejected, onProgress ) {
						var maxDepth = 0;
						function resolve( depth, deferred, handler, special ) {
							return function() {
								var that = this,
									args = arguments,
									mightThrow = function() {
										var returned, then;
	
										// Support: Promises/A+ section 2.3.3.3.3
										// https://promisesaplus.com/#point-59
										// Ignore double-resolution attempts
										if ( depth < maxDepth ) {
											return;
										}
	
										returned = handler.apply( that, args );
	
										// Support: Promises/A+ section 2.3.1
										// https://promisesaplus.com/#point-48
										if ( returned === deferred.promise() ) {
											throw new TypeError( "Thenable self-resolution" );
										}
	
										// Support: Promises/A+ sections 2.3.3.1, 3.5
										// https://promisesaplus.com/#point-54
										// https://promisesaplus.com/#point-75
										// Retrieve `then` only once
										then = returned &&
	
											// Support: Promises/A+ section 2.3.4
											// https://promisesaplus.com/#point-64
											// Only check objects and functions for thenability
											( typeof returned === "object" ||
												typeof returned === "function" ) &&
											returned.then;
	
										// Handle a returned thenable
										if ( jQuery.isFunction( then ) ) {
	
											// Special processors (notify) just wait for resolution
											if ( special ) {
												then.call(
													returned,
													resolve( maxDepth, deferred, Identity, special ),
													resolve( maxDepth, deferred, Thrower, special )
												);
	
											// Normal processors (resolve) also hook into progress
											} else {
	
												// ...and disregard older resolution values
												maxDepth++;
	
												then.call(
													returned,
													resolve( maxDepth, deferred, Identity, special ),
													resolve( maxDepth, deferred, Thrower, special ),
													resolve( maxDepth, deferred, Identity,
														deferred.notifyWith )
												);
											}
	
										// Handle all other returned values
										} else {
	
											// Only substitute handlers pass on context
											// and multiple values (non-spec behavior)
											if ( handler !== Identity ) {
												that = undefined;
												args = [ returned ];
											}
	
											// Process the value(s)
											// Default process is resolve
											( special || deferred.resolveWith )( that, args );
										}
									},
	
									// Only normal processors (resolve) catch and reject exceptions
									process = special ?
										mightThrow :
										function() {
											try {
												mightThrow();
											} catch ( e ) {
	
												if ( jQuery.Deferred.exceptionHook ) {
													jQuery.Deferred.exceptionHook( e,
														process.stackTrace );
												}
	
												// Support: Promises/A+ section 2.3.3.3.4.1
												// https://promisesaplus.com/#point-61
												// Ignore post-resolution exceptions
												if ( depth + 1 >= maxDepth ) {
	
													// Only substitute handlers pass on context
													// and multiple values (non-spec behavior)
													if ( handler !== Thrower ) {
														that = undefined;
														args = [ e ];
													}
	
													deferred.rejectWith( that, args );
												}
											}
										};
	
								// Support: Promises/A+ section 2.3.3.3.1
								// https://promisesaplus.com/#point-57
								// Re-resolve promises immediately to dodge false rejection from
								// subsequent errors
								if ( depth ) {
									process();
								} else {
	
									// Call an optional hook to record the stack, in case of exception
									// since it's otherwise lost when execution goes async
									if ( jQuery.Deferred.getStackHook ) {
										process.stackTrace = jQuery.Deferred.getStackHook();
									}
									window.setTimeout( process );
								}
							};
						}
	
						return jQuery.Deferred( function( newDefer ) {
	
							// progress_handlers.add( ... )
							tuples[ 0 ][ 3 ].add(
								resolve(
									0,
									newDefer,
									jQuery.isFunction( onProgress ) ?
										onProgress :
										Identity,
									newDefer.notifyWith
								)
							);
	
							// fulfilled_handlers.add( ... )
							tuples[ 1 ][ 3 ].add(
								resolve(
									0,
									newDefer,
									jQuery.isFunction( onFulfilled ) ?
										onFulfilled :
										Identity
								)
							);
	
							// rejected_handlers.add( ... )
							tuples[ 2 ][ 3 ].add(
								resolve(
									0,
									newDefer,
									jQuery.isFunction( onRejected ) ?
										onRejected :
										Thrower
								)
							);
						} ).promise();
					},
	
					// Get a promise for this deferred
					// If obj is provided, the promise aspect is added to the object
					promise: function( obj ) {
						return obj != null ? jQuery.extend( obj, promise ) : promise;
					}
				},
				deferred = {};
	
			// Add list-specific methods
			jQuery.each( tuples, function( i, tuple ) {
				var list = tuple[ 2 ],
					stateString = tuple[ 5 ];
	
				// promise.progress = list.add
				// promise.done = list.add
				// promise.fail = list.add
				promise[ tuple[ 1 ] ] = list.add;
	
				// Handle state
				if ( stateString ) {
					list.add(
						function() {
	
							// state = "resolved" (i.e., fulfilled)
							// state = "rejected"
							state = stateString;
						},
	
						// rejected_callbacks.disable
						// fulfilled_callbacks.disable
						tuples[ 3 - i ][ 2 ].disable,
	
						// progress_callbacks.lock
						tuples[ 0 ][ 2 ].lock
					);
				}
	
				// progress_handlers.fire
				// fulfilled_handlers.fire
				// rejected_handlers.fire
				list.add( tuple[ 3 ].fire );
	
				// deferred.notify = function() { deferred.notifyWith(...) }
				// deferred.resolve = function() { deferred.resolveWith(...) }
				// deferred.reject = function() { deferred.rejectWith(...) }
				deferred[ tuple[ 0 ] ] = function() {
					deferred[ tuple[ 0 ] + "With" ]( this === deferred ? undefined : this, arguments );
					return this;
				};
	
				// deferred.notifyWith = list.fireWith
				// deferred.resolveWith = list.fireWith
				// deferred.rejectWith = list.fireWith
				deferred[ tuple[ 0 ] + "With" ] = list.fireWith;
			} );
	
			// Make the deferred a promise
			promise.promise( deferred );
	
			// Call given func if any
			if ( func ) {
				func.call( deferred, deferred );
			}
	
			// All done!
			return deferred;
		},
	
		// Deferred helper
		when: function( singleValue ) {
			var
	
				// count of uncompleted subordinates
				remaining = arguments.length,
	
				// count of unprocessed arguments
				i = remaining,
	
				// subordinate fulfillment data
				resolveContexts = Array( i ),
				resolveValues = slice.call( arguments ),
	
				// the master Deferred
				master = jQuery.Deferred(),
	
				// subordinate callback factory
				updateFunc = function( i ) {
					return function( value ) {
						resolveContexts[ i ] = this;
						resolveValues[ i ] = arguments.length > 1 ? slice.call( arguments ) : value;
						if ( !( --remaining ) ) {
							master.resolveWith( resolveContexts, resolveValues );
						}
					};
				};
	
			// Single- and empty arguments are adopted like Promise.resolve
			if ( remaining <= 1 ) {
				adoptValue( singleValue, master.done( updateFunc( i ) ).resolve, master.reject,
					!remaining );
	
				// Use .then() to unwrap secondary thenables (cf. gh-3000)
				if ( master.state() === "pending" ||
					jQuery.isFunction( resolveValues[ i ] && resolveValues[ i ].then ) ) {
	
					return master.then();
				}
			}
	
			// Multiple arguments are aggregated like Promise.all array elements
			while ( i-- ) {
				adoptValue( resolveValues[ i ], updateFunc( i ), master.reject );
			}
	
			return master.promise();
		}
	} );
	
	
	// These usually indicate a programmer mistake during development,
	// warn about them ASAP rather than swallowing them by default.
	var rerrorNames = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
	
	jQuery.Deferred.exceptionHook = function( error, stack ) {
	
		// Support: IE 8 - 9 only
		// Console exists when dev tools are open, which can happen at any time
		if ( window.console && window.console.warn && error && rerrorNames.test( error.name ) ) {
			window.console.warn( "jQuery.Deferred exception: " + error.message, error.stack, stack );
		}
	};
	
	
	
	
	jQuery.readyException = function( error ) {
		window.setTimeout( function() {
			throw error;
		} );
	};
	
	
	
	
	// The deferred used on DOM ready
	var readyList = jQuery.Deferred();
	
	jQuery.fn.ready = function( fn ) {
	
		readyList
			.then( fn )
	
			// Wrap jQuery.readyException in a function so that the lookup
			// happens at the time of error handling instead of callback
			// registration.
			.catch( function( error ) {
				jQuery.readyException( error );
			} );
	
		return this;
	};
	
	jQuery.extend( {
	
		// Is the DOM ready to be used? Set to true once it occurs.
		isReady: false,
	
		// A counter to track how many items to wait for before
		// the ready event fires. See #6781
		readyWait: 1,
	
		// Handle when the DOM is ready
		ready: function( wait ) {
	
			// Abort if there are pending holds or we're already ready
			if ( wait === true ? --jQuery.readyWait : jQuery.isReady ) {
				return;
			}
	
			// Remember that the DOM is ready
			jQuery.isReady = true;
	
			// If a normal DOM Ready event fired, decrement, and wait if need be
			if ( wait !== true && --jQuery.readyWait > 0 ) {
				return;
			}
	
			// If there are functions bound, to execute
			readyList.resolveWith( document, [ jQuery ] );
		}
	} );
	
	jQuery.ready.then = readyList.then;
	
	// The ready event handler and self cleanup method
	function completed() {
		document.removeEventListener( "DOMContentLoaded", completed );
		window.removeEventListener( "load", completed );
		jQuery.ready();
	}
	
	// Catch cases where $(document).ready() is called
	// after the browser event has already occurred.
	// Support: IE <=9 - 10 only
	// Older IE sometimes signals "interactive" too soon
	if ( document.readyState === "complete" ||
		( document.readyState !== "loading" && !document.documentElement.doScroll ) ) {
	
		// Handle it asynchronously to allow scripts the opportunity to delay ready
		window.setTimeout( jQuery.ready );
	
	} else {
	
		// Use the handy event callback
		document.addEventListener( "DOMContentLoaded", completed );
	
		// A fallback to window.onload, that will always work
		window.addEventListener( "load", completed );
	}
	
	
	
	
	// Multifunctional method to get and set values of a collection
	// The value/s can optionally be executed if it's a function
	var access = function( elems, fn, key, value, chainable, emptyGet, raw ) {
		var i = 0,
			len = elems.length,
			bulk = key == null;
	
		// Sets many values
		if ( jQuery.type( key ) === "object" ) {
			chainable = true;
			for ( i in key ) {
				access( elems, fn, i, key[ i ], true, emptyGet, raw );
			}
	
		// Sets one value
		} else if ( value !== undefined ) {
			chainable = true;
	
			if ( !jQuery.isFunction( value ) ) {
				raw = true;
			}
	
			if ( bulk ) {
	
				// Bulk operations run against the entire set
				if ( raw ) {
					fn.call( elems, value );
					fn = null;
	
				// ...except when executing function values
				} else {
					bulk = fn;
					fn = function( elem, key, value ) {
						return bulk.call( jQuery( elem ), value );
					};
				}
			}
	
			if ( fn ) {
				for ( ; i < len; i++ ) {
					fn(
						elems[ i ], key, raw ?
						value :
						value.call( elems[ i ], i, fn( elems[ i ], key ) )
					);
				}
			}
		}
	
		if ( chainable ) {
			return elems;
		}
	
		// Gets
		if ( bulk ) {
			return fn.call( elems );
		}
	
		return len ? fn( elems[ 0 ], key ) : emptyGet;
	};
	var acceptData = function( owner ) {
	
		// Accepts only:
		//  - Node
		//    - Node.ELEMENT_NODE
		//    - Node.DOCUMENT_NODE
		//  - Object
		//    - Any
		return owner.nodeType === 1 || owner.nodeType === 9 || !( +owner.nodeType );
	};
	
	
	
	
	function Data() {
		this.expando = jQuery.expando + Data.uid++;
	}
	
	Data.uid = 1;
	
	Data.prototype = {
	
		cache: function( owner ) {
	
			// Check if the owner object already has a cache
			var value = owner[ this.expando ];
	
			// If not, create one
			if ( !value ) {
				value = {};
	
				// We can accept data for non-element nodes in modern browsers,
				// but we should not, see #8335.
				// Always return an empty object.
				if ( acceptData( owner ) ) {
	
					// If it is a node unlikely to be stringify-ed or looped over
					// use plain assignment
					if ( owner.nodeType ) {
						owner[ this.expando ] = value;
	
					// Otherwise secure it in a non-enumerable property
					// configurable must be true to allow the property to be
					// deleted when data is removed
					} else {
						Object.defineProperty( owner, this.expando, {
							value: value,
							configurable: true
						} );
					}
				}
			}
	
			return value;
		},
		set: function( owner, data, value ) {
			var prop,
				cache = this.cache( owner );
	
			// Handle: [ owner, key, value ] args
			// Always use camelCase key (gh-2257)
			if ( typeof data === "string" ) {
				cache[ jQuery.camelCase( data ) ] = value;
	
			// Handle: [ owner, { properties } ] args
			} else {
	
				// Copy the properties one-by-one to the cache object
				for ( prop in data ) {
					cache[ jQuery.camelCase( prop ) ] = data[ prop ];
				}
			}
			return cache;
		},
		get: function( owner, key ) {
			return key === undefined ?
				this.cache( owner ) :
	
				// Always use camelCase key (gh-2257)
				owner[ this.expando ] && owner[ this.expando ][ jQuery.camelCase( key ) ];
		},
		access: function( owner, key, value ) {
	
			// In cases where either:
			//
			//   1. No key was specified
			//   2. A string key was specified, but no value provided
			//
			// Take the "read" path and allow the get method to determine
			// which value to return, respectively either:
			//
			//   1. The entire cache object
			//   2. The data stored at the key
			//
			if ( key === undefined ||
					( ( key && typeof key === "string" ) && value === undefined ) ) {
	
				return this.get( owner, key );
			}
	
			// When the key is not a string, or both a key and value
			// are specified, set or extend (existing objects) with either:
			//
			//   1. An object of properties
			//   2. A key and value
			//
			this.set( owner, key, value );
	
			// Since the "set" path can have two possible entry points
			// return the expected data based on which path was taken[*]
			return value !== undefined ? value : key;
		},
		remove: function( owner, key ) {
			var i,
				cache = owner[ this.expando ];
	
			if ( cache === undefined ) {
				return;
			}
	
			if ( key !== undefined ) {
	
				// Support array or space separated string of keys
				if ( Array.isArray( key ) ) {
	
					// If key is an array of keys...
					// We always set camelCase keys, so remove that.
					key = key.map( jQuery.camelCase );
				} else {
					key = jQuery.camelCase( key );
	
					// If a key with the spaces exists, use it.
					// Otherwise, create an array by matching non-whitespace
					key = key in cache ?
						[ key ] :
						( key.match( rnothtmlwhite ) || [] );
				}
	
				i = key.length;
	
				while ( i-- ) {
					delete cache[ key[ i ] ];
				}
			}
	
			// Remove the expando if there's no more data
			if ( key === undefined || jQuery.isEmptyObject( cache ) ) {
	
				// Support: Chrome <=35 - 45
				// Webkit & Blink performance suffers when deleting properties
				// from DOM nodes, so set to undefined instead
				// https://bugs.chromium.org/p/chromium/issues/detail?id=378607 (bug restricted)
				if ( owner.nodeType ) {
					owner[ this.expando ] = undefined;
				} else {
					delete owner[ this.expando ];
				}
			}
		},
		hasData: function( owner ) {
			var cache = owner[ this.expando ];
			return cache !== undefined && !jQuery.isEmptyObject( cache );
		}
	};
	var dataPriv = new Data();
	
	var dataUser = new Data();
	
	
	
	//	Implementation Summary
	//
	//	1. Enforce API surface and semantic compatibility with 1.9.x branch
	//	2. Improve the module's maintainability by reducing the storage
	//		paths to a single mechanism.
	//	3. Use the same single mechanism to support "private" and "user" data.
	//	4. _Never_ expose "private" data to user code (TODO: Drop _data, _removeData)
	//	5. Avoid exposing implementation details on user objects (eg. expando properties)
	//	6. Provide a clear path for implementation upgrade to WeakMap in 2014
	
	var rbrace = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
		rmultiDash = /[A-Z]/g;
	
	function getData( data ) {
		if ( data === "true" ) {
			return true;
		}
	
		if ( data === "false" ) {
			return false;
		}
	
		if ( data === "null" ) {
			return null;
		}
	
		// Only convert to a number if it doesn't change the string
		if ( data === +data + "" ) {
			return +data;
		}
	
		if ( rbrace.test( data ) ) {
			return JSON.parse( data );
		}
	
		return data;
	}
	
	function dataAttr( elem, key, data ) {
		var name;
	
		// If nothing was found internally, try to fetch any
		// data from the HTML5 data-* attribute
		if ( data === undefined && elem.nodeType === 1 ) {
			name = "data-" + key.replace( rmultiDash, "-$&" ).toLowerCase();
			data = elem.getAttribute( name );
	
			if ( typeof data === "string" ) {
				try {
					data = getData( data );
				} catch ( e ) {}
	
				// Make sure we set the data so it isn't changed later
				dataUser.set( elem, key, data );
			} else {
				data = undefined;
			}
		}
		return data;
	}
	
	jQuery.extend( {
		hasData: function( elem ) {
			return dataUser.hasData( elem ) || dataPriv.hasData( elem );
		},
	
		data: function( elem, name, data ) {
			return dataUser.access( elem, name, data );
		},
	
		removeData: function( elem, name ) {
			dataUser.remove( elem, name );
		},
	
		// TODO: Now that all calls to _data and _removeData have been replaced
		// with direct calls to dataPriv methods, these can be deprecated.
		_data: function( elem, name, data ) {
			return dataPriv.access( elem, name, data );
		},
	
		_removeData: function( elem, name ) {
			dataPriv.remove( elem, name );
		}
	} );
	
	jQuery.fn.extend( {
		data: function( key, value ) {
			var i, name, data,
				elem = this[ 0 ],
				attrs = elem && elem.attributes;
	
			// Gets all values
			if ( key === undefined ) {
				if ( this.length ) {
					data = dataUser.get( elem );
	
					if ( elem.nodeType === 1 && !dataPriv.get( elem, "hasDataAttrs" ) ) {
						i = attrs.length;
						while ( i-- ) {
	
							// Support: IE 11 only
							// The attrs elements can be null (#14894)
							if ( attrs[ i ] ) {
								name = attrs[ i ].name;
								if ( name.indexOf( "data-" ) === 0 ) {
									name = jQuery.camelCase( name.slice( 5 ) );
									dataAttr( elem, name, data[ name ] );
								}
							}
						}
						dataPriv.set( elem, "hasDataAttrs", true );
					}
				}
	
				return data;
			}
	
			// Sets multiple values
			if ( typeof key === "object" ) {
				return this.each( function() {
					dataUser.set( this, key );
				} );
			}
	
			return access( this, function( value ) {
				var data;
	
				// The calling jQuery object (element matches) is not empty
				// (and therefore has an element appears at this[ 0 ]) and the
				// `value` parameter was not undefined. An empty jQuery object
				// will result in `undefined` for elem = this[ 0 ] which will
				// throw an exception if an attempt to read a data cache is made.
				if ( elem && value === undefined ) {
	
					// Attempt to get data from the cache
					// The key will always be camelCased in Data
					data = dataUser.get( elem, key );
					if ( data !== undefined ) {
						return data;
					}
	
					// Attempt to "discover" the data in
					// HTML5 custom data-* attrs
					data = dataAttr( elem, key );
					if ( data !== undefined ) {
						return data;
					}
	
					// We tried really hard, but the data doesn't exist.
					return;
				}
	
				// Set the data...
				this.each( function() {
	
					// We always store the camelCased key
					dataUser.set( this, key, value );
				} );
			}, null, value, arguments.length > 1, null, true );
		},
	
		removeData: function( key ) {
			return this.each( function() {
				dataUser.remove( this, key );
			} );
		}
	} );
	
	
	jQuery.extend( {
		queue: function( elem, type, data ) {
			var queue;
	
			if ( elem ) {
				type = ( type || "fx" ) + "queue";
				queue = dataPriv.get( elem, type );
	
				// Speed up dequeue by getting out quickly if this is just a lookup
				if ( data ) {
					if ( !queue || Array.isArray( data ) ) {
						queue = dataPriv.access( elem, type, jQuery.makeArray( data ) );
					} else {
						queue.push( data );
					}
				}
				return queue || [];
			}
		},
	
		dequeue: function( elem, type ) {
			type = type || "fx";
	
			var queue = jQuery.queue( elem, type ),
				startLength = queue.length,
				fn = queue.shift(),
				hooks = jQuery._queueHooks( elem, type ),
				next = function() {
					jQuery.dequeue( elem, type );
				};
	
			// If the fx queue is dequeued, always remove the progress sentinel
			if ( fn === "inprogress" ) {
				fn = queue.shift();
				startLength--;
			}
	
			if ( fn ) {
	
				// Add a progress sentinel to prevent the fx queue from being
				// automatically dequeued
				if ( type === "fx" ) {
					queue.unshift( "inprogress" );
				}
	
				// Clear up the last queue stop function
				delete hooks.stop;
				fn.call( elem, next, hooks );
			}
	
			if ( !startLength && hooks ) {
				hooks.empty.fire();
			}
		},
	
		// Not public - generate a queueHooks object, or return the current one
		_queueHooks: function( elem, type ) {
			var key = type + "queueHooks";
			return dataPriv.get( elem, key ) || dataPriv.access( elem, key, {
				empty: jQuery.Callbacks( "once memory" ).add( function() {
					dataPriv.remove( elem, [ type + "queue", key ] );
				} )
			} );
		}
	} );
	
	jQuery.fn.extend( {
		queue: function( type, data ) {
			var setter = 2;
	
			if ( typeof type !== "string" ) {
				data = type;
				type = "fx";
				setter--;
			}
	
			if ( arguments.length < setter ) {
				return jQuery.queue( this[ 0 ], type );
			}
	
			return data === undefined ?
				this :
				this.each( function() {
					var queue = jQuery.queue( this, type, data );
	
					// Ensure a hooks for this queue
					jQuery._queueHooks( this, type );
	
					if ( type === "fx" && queue[ 0 ] !== "inprogress" ) {
						jQuery.dequeue( this, type );
					}
				} );
		},
		dequeue: function( type ) {
			return this.each( function() {
				jQuery.dequeue( this, type );
			} );
		},
		clearQueue: function( type ) {
			return this.queue( type || "fx", [] );
		},
	
		// Get a promise resolved when queues of a certain type
		// are emptied (fx is the type by default)
		promise: function( type, obj ) {
			var tmp,
				count = 1,
				defer = jQuery.Deferred(),
				elements = this,
				i = this.length,
				resolve = function() {
					if ( !( --count ) ) {
						defer.resolveWith( elements, [ elements ] );
					}
				};
	
			if ( typeof type !== "string" ) {
				obj = type;
				type = undefined;
			}
			type = type || "fx";
	
			while ( i-- ) {
				tmp = dataPriv.get( elements[ i ], type + "queueHooks" );
				if ( tmp && tmp.empty ) {
					count++;
					tmp.empty.add( resolve );
				}
			}
			resolve();
			return defer.promise( obj );
		}
	} );
	var pnum = ( /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/ ).source;
	
	var rcssNum = new RegExp( "^(?:([+-])=|)(" + pnum + ")([a-z%]*)$", "i" );
	
	
	var cssExpand = [ "Top", "Right", "Bottom", "Left" ];
	
	var isHiddenWithinTree = function( elem, el ) {
	
			// isHiddenWithinTree might be called from jQuery#filter function;
			// in that case, element will be second argument
			elem = el || elem;
	
			// Inline style trumps all
			return elem.style.display === "none" ||
				elem.style.display === "" &&
	
				// Otherwise, check computed style
				// Support: Firefox <=43 - 45
				// Disconnected elements can have computed display: none, so first confirm that elem is
				// in the document.
				jQuery.contains( elem.ownerDocument, elem ) &&
	
				jQuery.css( elem, "display" ) === "none";
		};
	
	var swap = function( elem, options, callback, args ) {
		var ret, name,
			old = {};
	
		// Remember the old values, and insert the new ones
		for ( name in options ) {
			old[ name ] = elem.style[ name ];
			elem.style[ name ] = options[ name ];
		}
	
		ret = callback.apply( elem, args || [] );
	
		// Revert the old values
		for ( name in options ) {
			elem.style[ name ] = old[ name ];
		}
	
		return ret;
	};
	
	
	
	
	function adjustCSS( elem, prop, valueParts, tween ) {
		var adjusted,
			scale = 1,
			maxIterations = 20,
			currentValue = tween ?
				function() {
					return tween.cur();
				} :
				function() {
					return jQuery.css( elem, prop, "" );
				},
			initial = currentValue(),
			unit = valueParts && valueParts[ 3 ] || ( jQuery.cssNumber[ prop ] ? "" : "px" ),
	
			// Starting value computation is required for potential unit mismatches
			initialInUnit = ( jQuery.cssNumber[ prop ] || unit !== "px" && +initial ) &&
				rcssNum.exec( jQuery.css( elem, prop ) );
	
		if ( initialInUnit && initialInUnit[ 3 ] !== unit ) {
	
			// Trust units reported by jQuery.css
			unit = unit || initialInUnit[ 3 ];
	
			// Make sure we update the tween properties later on
			valueParts = valueParts || [];
	
			// Iteratively approximate from a nonzero starting point
			initialInUnit = +initial || 1;
	
			do {
	
				// If previous iteration zeroed out, double until we get *something*.
				// Use string for doubling so we don't accidentally see scale as unchanged below
				scale = scale || ".5";
	
				// Adjust and apply
				initialInUnit = initialInUnit / scale;
				jQuery.style( elem, prop, initialInUnit + unit );
	
			// Update scale, tolerating zero or NaN from tween.cur()
			// Break the loop if scale is unchanged or perfect, or if we've just had enough.
			} while (
				scale !== ( scale = currentValue() / initial ) && scale !== 1 && --maxIterations
			);
		}
	
		if ( valueParts ) {
			initialInUnit = +initialInUnit || +initial || 0;
	
			// Apply relative offset (+=/-=) if specified
			adjusted = valueParts[ 1 ] ?
				initialInUnit + ( valueParts[ 1 ] + 1 ) * valueParts[ 2 ] :
				+valueParts[ 2 ];
			if ( tween ) {
				tween.unit = unit;
				tween.start = initialInUnit;
				tween.end = adjusted;
			}
		}
		return adjusted;
	}
	
	
	var defaultDisplayMap = {};
	
	function getDefaultDisplay( elem ) {
		var temp,
			doc = elem.ownerDocument,
			nodeName = elem.nodeName,
			display = defaultDisplayMap[ nodeName ];
	
		if ( display ) {
			return display;
		}
	
		temp = doc.body.appendChild( doc.createElement( nodeName ) );
		display = jQuery.css( temp, "display" );
	
		temp.parentNode.removeChild( temp );
	
		if ( display === "none" ) {
			display = "block";
		}
		defaultDisplayMap[ nodeName ] = display;
	
		return display;
	}
	
	function showHide( elements, show ) {
		var display, elem,
			values = [],
			index = 0,
			length = elements.length;
	
		// Determine new display value for elements that need to change
		for ( ; index < length; index++ ) {
			elem = elements[ index ];
			if ( !elem.style ) {
				continue;
			}
	
			display = elem.style.display;
			if ( show ) {
	
				// Since we force visibility upon cascade-hidden elements, an immediate (and slow)
				// check is required in this first loop unless we have a nonempty display value (either
				// inline or about-to-be-restored)
				if ( display === "none" ) {
					values[ index ] = dataPriv.get( elem, "display" ) || null;
					if ( !values[ index ] ) {
						elem.style.display = "";
					}
				}
				if ( elem.style.display === "" && isHiddenWithinTree( elem ) ) {
					values[ index ] = getDefaultDisplay( elem );
				}
			} else {
				if ( display !== "none" ) {
					values[ index ] = "none";
	
					// Remember what we're overwriting
					dataPriv.set( elem, "display", display );
				}
			}
		}
	
		// Set the display of the elements in a second loop to avoid constant reflow
		for ( index = 0; index < length; index++ ) {
			if ( values[ index ] != null ) {
				elements[ index ].style.display = values[ index ];
			}
		}
	
		return elements;
	}
	
	jQuery.fn.extend( {
		show: function() {
			return showHide( this, true );
		},
		hide: function() {
			return showHide( this );
		},
		toggle: function( state ) {
			if ( typeof state === "boolean" ) {
				return state ? this.show() : this.hide();
			}
	
			return this.each( function() {
				if ( isHiddenWithinTree( this ) ) {
					jQuery( this ).show();
				} else {
					jQuery( this ).hide();
				}
			} );
		}
	} );
	var rcheckableType = ( /^(?:checkbox|radio)$/i );
	
	var rtagName = ( /<([a-z][^\/\0>\x20\t\r\n\f]+)/i );
	
	var rscriptType = ( /^$|\/(?:java|ecma)script/i );
	
	
	
	// We have to close these tags to support XHTML (#13200)
	var wrapMap = {
	
		// Support: IE <=9 only
		option: [ 1, "<select multiple='multiple'>", "</select>" ],
	
		// XHTML parsers do not magically insert elements in the
		// same way that tag soup parsers do. So we cannot shorten
		// this by omitting <tbody> or other required elements.
		thead: [ 1, "<table>", "</table>" ],
		col: [ 2, "<table><colgroup>", "</colgroup></table>" ],
		tr: [ 2, "<table><tbody>", "</tbody></table>" ],
		td: [ 3, "<table><tbody><tr>", "</tr></tbody></table>" ],
	
		_default: [ 0, "", "" ]
	};
	
	// Support: IE <=9 only
	wrapMap.optgroup = wrapMap.option;
	
	wrapMap.tbody = wrapMap.tfoot = wrapMap.colgroup = wrapMap.caption = wrapMap.thead;
	wrapMap.th = wrapMap.td;
	
	
	function getAll( context, tag ) {
	
		// Support: IE <=9 - 11 only
		// Use typeof to avoid zero-argument method invocation on host objects (#15151)
		var ret;
	
		if ( typeof context.getElementsByTagName !== "undefined" ) {
			ret = context.getElementsByTagName( tag || "*" );
	
		} else if ( typeof context.querySelectorAll !== "undefined" ) {
			ret = context.querySelectorAll( tag || "*" );
	
		} else {
			ret = [];
		}
	
		if ( tag === undefined || tag && nodeName( context, tag ) ) {
			return jQuery.merge( [ context ], ret );
		}
	
		return ret;
	}
	
	
	// Mark scripts as having already been evaluated
	function setGlobalEval( elems, refElements ) {
		var i = 0,
			l = elems.length;
	
		for ( ; i < l; i++ ) {
			dataPriv.set(
				elems[ i ],
				"globalEval",
				!refElements || dataPriv.get( refElements[ i ], "globalEval" )
			);
		}
	}
	
	
	var rhtml = /<|&#?\w+;/;
	
	function buildFragment( elems, context, scripts, selection, ignored ) {
		var elem, tmp, tag, wrap, contains, j,
			fragment = context.createDocumentFragment(),
			nodes = [],
			i = 0,
			l = elems.length;
	
		for ( ; i < l; i++ ) {
			elem = elems[ i ];
	
			if ( elem || elem === 0 ) {
	
				// Add nodes directly
				if ( jQuery.type( elem ) === "object" ) {
	
					// Support: Android <=4.0 only, PhantomJS 1 only
					// push.apply(_, arraylike) throws on ancient WebKit
					jQuery.merge( nodes, elem.nodeType ? [ elem ] : elem );
	
				// Convert non-html into a text node
				} else if ( !rhtml.test( elem ) ) {
					nodes.push( context.createTextNode( elem ) );
	
				// Convert html into DOM nodes
				} else {
					tmp = tmp || fragment.appendChild( context.createElement( "div" ) );
	
					// Deserialize a standard representation
					tag = ( rtagName.exec( elem ) || [ "", "" ] )[ 1 ].toLowerCase();
					wrap = wrapMap[ tag ] || wrapMap._default;
					tmp.innerHTML = wrap[ 1 ] + jQuery.htmlPrefilter( elem ) + wrap[ 2 ];
	
					// Descend through wrappers to the right content
					j = wrap[ 0 ];
					while ( j-- ) {
						tmp = tmp.lastChild;
					}
	
					// Support: Android <=4.0 only, PhantomJS 1 only
					// push.apply(_, arraylike) throws on ancient WebKit
					jQuery.merge( nodes, tmp.childNodes );
	
					// Remember the top-level container
					tmp = fragment.firstChild;
	
					// Ensure the created nodes are orphaned (#12392)
					tmp.textContent = "";
				}
			}
		}
	
		// Remove wrapper from fragment
		fragment.textContent = "";
	
		i = 0;
		while ( ( elem = nodes[ i++ ] ) ) {
	
			// Skip elements already in the context collection (trac-4087)
			if ( selection && jQuery.inArray( elem, selection ) > -1 ) {
				if ( ignored ) {
					ignored.push( elem );
				}
				continue;
			}
	
			contains = jQuery.contains( elem.ownerDocument, elem );
	
			// Append to fragment
			tmp = getAll( fragment.appendChild( elem ), "script" );
	
			// Preserve script evaluation history
			if ( contains ) {
				setGlobalEval( tmp );
			}
	
			// Capture executables
			if ( scripts ) {
				j = 0;
				while ( ( elem = tmp[ j++ ] ) ) {
					if ( rscriptType.test( elem.type || "" ) ) {
						scripts.push( elem );
					}
				}
			}
		}
	
		return fragment;
	}
	
	
	( function() {
		var fragment = document.createDocumentFragment(),
			div = fragment.appendChild( document.createElement( "div" ) ),
			input = document.createElement( "input" );
	
		// Support: Android 4.0 - 4.3 only
		// Check state lost if the name is set (#11217)
		// Support: Windows Web Apps (WWA)
		// `name` and `type` must use .setAttribute for WWA (#14901)
		input.setAttribute( "type", "radio" );
		input.setAttribute( "checked", "checked" );
		input.setAttribute( "name", "t" );
	
		div.appendChild( input );
	
		// Support: Android <=4.1 only
		// Older WebKit doesn't clone checked state correctly in fragments
		support.checkClone = div.cloneNode( true ).cloneNode( true ).lastChild.checked;
	
		// Support: IE <=11 only
		// Make sure textarea (and checkbox) defaultValue is properly cloned
		div.innerHTML = "<textarea>x</textarea>";
		support.noCloneChecked = !!div.cloneNode( true ).lastChild.defaultValue;
	} )();
	var documentElement = document.documentElement;
	
	
	
	var
		rkeyEvent = /^key/,
		rmouseEvent = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
		rtypenamespace = /^([^.]*)(?:\.(.+)|)/;
	
	function returnTrue() {
		return true;
	}
	
	function returnFalse() {
		return false;
	}
	
	// Support: IE <=9 only
	// See #13393 for more info
	function safeActiveElement() {
		try {
			return document.activeElement;
		} catch ( err ) { }
	}
	
	function on( elem, types, selector, data, fn, one ) {
		var origFn, type;
	
		// Types can be a map of types/handlers
		if ( typeof types === "object" ) {
	
			// ( types-Object, selector, data )
			if ( typeof selector !== "string" ) {
	
				// ( types-Object, data )
				data = data || selector;
				selector = undefined;
			}
			for ( type in types ) {
				on( elem, type, selector, data, types[ type ], one );
			}
			return elem;
		}
	
		if ( data == null && fn == null ) {
	
			// ( types, fn )
			fn = selector;
			data = selector = undefined;
		} else if ( fn == null ) {
			if ( typeof selector === "string" ) {
	
				// ( types, selector, fn )
				fn = data;
				data = undefined;
			} else {
	
				// ( types, data, fn )
				fn = data;
				data = selector;
				selector = undefined;
			}
		}
		if ( fn === false ) {
			fn = returnFalse;
		} else if ( !fn ) {
			return elem;
		}
	
		if ( one === 1 ) {
			origFn = fn;
			fn = function( event ) {
	
				// Can use an empty set, since event contains the info
				jQuery().off( event );
				return origFn.apply( this, arguments );
			};
	
			// Use same guid so caller can remove using origFn
			fn.guid = origFn.guid || ( origFn.guid = jQuery.guid++ );
		}
		return elem.each( function() {
			jQuery.event.add( this, types, fn, data, selector );
		} );
	}
	
	/*
	 * Helper functions for managing events -- not part of the public interface.
	 * Props to Dean Edwards' addEvent library for many of the ideas.
	 */
	jQuery.event = {
	
		global: {},
	
		add: function( elem, types, handler, data, selector ) {
	
			var handleObjIn, eventHandle, tmp,
				events, t, handleObj,
				special, handlers, type, namespaces, origType,
				elemData = dataPriv.get( elem );
	
			// Don't attach events to noData or text/comment nodes (but allow plain objects)
			if ( !elemData ) {
				return;
			}
	
			// Caller can pass in an object of custom data in lieu of the handler
			if ( handler.handler ) {
				handleObjIn = handler;
				handler = handleObjIn.handler;
				selector = handleObjIn.selector;
			}
	
			// Ensure that invalid selectors throw exceptions at attach time
			// Evaluate against documentElement in case elem is a non-element node (e.g., document)
			if ( selector ) {
				jQuery.find.matchesSelector( documentElement, selector );
			}
	
			// Make sure that the handler has a unique ID, used to find/remove it later
			if ( !handler.guid ) {
				handler.guid = jQuery.guid++;
			}
	
			// Init the element's event structure and main handler, if this is the first
			if ( !( events = elemData.events ) ) {
				events = elemData.events = {};
			}
			if ( !( eventHandle = elemData.handle ) ) {
				eventHandle = elemData.handle = function( e ) {
	
					// Discard the second event of a jQuery.event.trigger() and
					// when an event is called after a page has unloaded
					return typeof jQuery !== "undefined" && jQuery.event.triggered !== e.type ?
						jQuery.event.dispatch.apply( elem, arguments ) : undefined;
				};
			}
	
			// Handle multiple events separated by a space
			types = ( types || "" ).match( rnothtmlwhite ) || [ "" ];
			t = types.length;
			while ( t-- ) {
				tmp = rtypenamespace.exec( types[ t ] ) || [];
				type = origType = tmp[ 1 ];
				namespaces = ( tmp[ 2 ] || "" ).split( "." ).sort();
	
				// There *must* be a type, no attaching namespace-only handlers
				if ( !type ) {
					continue;
				}
	
				// If event changes its type, use the special event handlers for the changed type
				special = jQuery.event.special[ type ] || {};
	
				// If selector defined, determine special event api type, otherwise given type
				type = ( selector ? special.delegateType : special.bindType ) || type;
	
				// Update special based on newly reset type
				special = jQuery.event.special[ type ] || {};
	
				// handleObj is passed to all event handlers
				handleObj = jQuery.extend( {
					type: type,
					origType: origType,
					data: data,
					handler: handler,
					guid: handler.guid,
					selector: selector,
					needsContext: selector && jQuery.expr.match.needsContext.test( selector ),
					namespace: namespaces.join( "." )
				}, handleObjIn );
	
				// Init the event handler queue if we're the first
				if ( !( handlers = events[ type ] ) ) {
					handlers = events[ type ] = [];
					handlers.delegateCount = 0;
	
					// Only use addEventListener if the special events handler returns false
					if ( !special.setup ||
						special.setup.call( elem, data, namespaces, eventHandle ) === false ) {
	
						if ( elem.addEventListener ) {
							elem.addEventListener( type, eventHandle );
						}
					}
				}
	
				if ( special.add ) {
					special.add.call( elem, handleObj );
	
					if ( !handleObj.handler.guid ) {
						handleObj.handler.guid = handler.guid;
					}
				}
	
				// Add to the element's handler list, delegates in front
				if ( selector ) {
					handlers.splice( handlers.delegateCount++, 0, handleObj );
				} else {
					handlers.push( handleObj );
				}
	
				// Keep track of which events have ever been used, for event optimization
				jQuery.event.global[ type ] = true;
			}
	
		},
	
		// Detach an event or set of events from an element
		remove: function( elem, types, handler, selector, mappedTypes ) {
	
			var j, origCount, tmp,
				events, t, handleObj,
				special, handlers, type, namespaces, origType,
				elemData = dataPriv.hasData( elem ) && dataPriv.get( elem );
	
			if ( !elemData || !( events = elemData.events ) ) {
				return;
			}
	
			// Once for each type.namespace in types; type may be omitted
			types = ( types || "" ).match( rnothtmlwhite ) || [ "" ];
			t = types.length;
			while ( t-- ) {
				tmp = rtypenamespace.exec( types[ t ] ) || [];
				type = origType = tmp[ 1 ];
				namespaces = ( tmp[ 2 ] || "" ).split( "." ).sort();
	
				// Unbind all events (on this namespace, if provided) for the element
				if ( !type ) {
					for ( type in events ) {
						jQuery.event.remove( elem, type + types[ t ], handler, selector, true );
					}
					continue;
				}
	
				special = jQuery.event.special[ type ] || {};
				type = ( selector ? special.delegateType : special.bindType ) || type;
				handlers = events[ type ] || [];
				tmp = tmp[ 2 ] &&
					new RegExp( "(^|\\.)" + namespaces.join( "\\.(?:.*\\.|)" ) + "(\\.|$)" );
	
				// Remove matching events
				origCount = j = handlers.length;
				while ( j-- ) {
					handleObj = handlers[ j ];
	
					if ( ( mappedTypes || origType === handleObj.origType ) &&
						( !handler || handler.guid === handleObj.guid ) &&
						( !tmp || tmp.test( handleObj.namespace ) ) &&
						( !selector || selector === handleObj.selector ||
							selector === "**" && handleObj.selector ) ) {
						handlers.splice( j, 1 );
	
						if ( handleObj.selector ) {
							handlers.delegateCount--;
						}
						if ( special.remove ) {
							special.remove.call( elem, handleObj );
						}
					}
				}
	
				// Remove generic event handler if we removed something and no more handlers exist
				// (avoids potential for endless recursion during removal of special event handlers)
				if ( origCount && !handlers.length ) {
					if ( !special.teardown ||
						special.teardown.call( elem, namespaces, elemData.handle ) === false ) {
	
						jQuery.removeEvent( elem, type, elemData.handle );
					}
	
					delete events[ type ];
				}
			}
	
			// Remove data and the expando if it's no longer used
			if ( jQuery.isEmptyObject( events ) ) {
				dataPriv.remove( elem, "handle events" );
			}
		},
	
		dispatch: function( nativeEvent ) {
	
			// Make a writable jQuery.Event from the native event object
			var event = jQuery.event.fix( nativeEvent );
	
			var i, j, ret, matched, handleObj, handlerQueue,
				args = new Array( arguments.length ),
				handlers = ( dataPriv.get( this, "events" ) || {} )[ event.type ] || [],
				special = jQuery.event.special[ event.type ] || {};
	
			// Use the fix-ed jQuery.Event rather than the (read-only) native event
			args[ 0 ] = event;
	
			for ( i = 1; i < arguments.length; i++ ) {
				args[ i ] = arguments[ i ];
			}
	
			event.delegateTarget = this;
	
			// Call the preDispatch hook for the mapped type, and let it bail if desired
			if ( special.preDispatch && special.preDispatch.call( this, event ) === false ) {
				return;
			}
	
			// Determine handlers
			handlerQueue = jQuery.event.handlers.call( this, event, handlers );
	
			// Run delegates first; they may want to stop propagation beneath us
			i = 0;
			while ( ( matched = handlerQueue[ i++ ] ) && !event.isPropagationStopped() ) {
				event.currentTarget = matched.elem;
	
				j = 0;
				while ( ( handleObj = matched.handlers[ j++ ] ) &&
					!event.isImmediatePropagationStopped() ) {
	
					// Triggered event must either 1) have no namespace, or 2) have namespace(s)
					// a subset or equal to those in the bound event (both can have no namespace).
					if ( !event.rnamespace || event.rnamespace.test( handleObj.namespace ) ) {
	
						event.handleObj = handleObj;
						event.data = handleObj.data;
	
						ret = ( ( jQuery.event.special[ handleObj.origType ] || {} ).handle ||
							handleObj.handler ).apply( matched.elem, args );
	
						if ( ret !== undefined ) {
							if ( ( event.result = ret ) === false ) {
								event.preventDefault();
								event.stopPropagation();
							}
						}
					}
				}
			}
	
			// Call the postDispatch hook for the mapped type
			if ( special.postDispatch ) {
				special.postDispatch.call( this, event );
			}
	
			return event.result;
		},
	
		handlers: function( event, handlers ) {
			var i, handleObj, sel, matchedHandlers, matchedSelectors,
				handlerQueue = [],
				delegateCount = handlers.delegateCount,
				cur = event.target;
	
			// Find delegate handlers
			if ( delegateCount &&
	
				// Support: IE <=9
				// Black-hole SVG <use> instance trees (trac-13180)
				cur.nodeType &&
	
				// Support: Firefox <=42
				// Suppress spec-violating clicks indicating a non-primary pointer button (trac-3861)
				// https://www.w3.org/TR/DOM-Level-3-Events/#event-type-click
				// Support: IE 11 only
				// ...but not arrow key "clicks" of radio inputs, which can have `button` -1 (gh-2343)
				!( event.type === "click" && event.button >= 1 ) ) {
	
				for ( ; cur !== this; cur = cur.parentNode || this ) {
	
					// Don't check non-elements (#13208)
					// Don't process clicks on disabled elements (#6911, #8165, #11382, #11764)
					if ( cur.nodeType === 1 && !( event.type === "click" && cur.disabled === true ) ) {
						matchedHandlers = [];
						matchedSelectors = {};
						for ( i = 0; i < delegateCount; i++ ) {
							handleObj = handlers[ i ];
	
							// Don't conflict with Object.prototype properties (#13203)
							sel = handleObj.selector + " ";
	
							if ( matchedSelectors[ sel ] === undefined ) {
								matchedSelectors[ sel ] = handleObj.needsContext ?
									jQuery( sel, this ).index( cur ) > -1 :
									jQuery.find( sel, this, null, [ cur ] ).length;
							}
							if ( matchedSelectors[ sel ] ) {
								matchedHandlers.push( handleObj );
							}
						}
						if ( matchedHandlers.length ) {
							handlerQueue.push( { elem: cur, handlers: matchedHandlers } );
						}
					}
				}
			}
	
			// Add the remaining (directly-bound) handlers
			cur = this;
			if ( delegateCount < handlers.length ) {
				handlerQueue.push( { elem: cur, handlers: handlers.slice( delegateCount ) } );
			}
	
			return handlerQueue;
		},
	
		addProp: function( name, hook ) {
			Object.defineProperty( jQuery.Event.prototype, name, {
				enumerable: true,
				configurable: true,
	
				get: jQuery.isFunction( hook ) ?
					function() {
						if ( this.originalEvent ) {
								return hook( this.originalEvent );
						}
					} :
					function() {
						if ( this.originalEvent ) {
								return this.originalEvent[ name ];
						}
					},
	
				set: function( value ) {
					Object.defineProperty( this, name, {
						enumerable: true,
						configurable: true,
						writable: true,
						value: value
					} );
				}
			} );
		},
	
		fix: function( originalEvent ) {
			return originalEvent[ jQuery.expando ] ?
				originalEvent :
				new jQuery.Event( originalEvent );
		},
	
		special: {
			load: {
	
				// Prevent triggered image.load events from bubbling to window.load
				noBubble: true
			},
			focus: {
	
				// Fire native event if possible so blur/focus sequence is correct
				trigger: function() {
					if ( this !== safeActiveElement() && this.focus ) {
						this.focus();
						return false;
					}
				},
				delegateType: "focusin"
			},
			blur: {
				trigger: function() {
					if ( this === safeActiveElement() && this.blur ) {
						this.blur();
						return false;
					}
				},
				delegateType: "focusout"
			},
			click: {
	
				// For checkbox, fire native event so checked state will be right
				trigger: function() {
					if ( this.type === "checkbox" && this.click && nodeName( this, "input" ) ) {
						this.click();
						return false;
					}
				},
	
				// For cross-browser consistency, don't fire native .click() on links
				_default: function( event ) {
					return nodeName( event.target, "a" );
				}
			},
	
			beforeunload: {
				postDispatch: function( event ) {
	
					// Support: Firefox 20+
					// Firefox doesn't alert if the returnValue field is not set.
					if ( event.result !== undefined && event.originalEvent ) {
						event.originalEvent.returnValue = event.result;
					}
				}
			}
		}
	};
	
	jQuery.removeEvent = function( elem, type, handle ) {
	
		// This "if" is needed for plain objects
		if ( elem.removeEventListener ) {
			elem.removeEventListener( type, handle );
		}
	};
	
	jQuery.Event = function( src, props ) {
	
		// Allow instantiation without the 'new' keyword
		if ( !( this instanceof jQuery.Event ) ) {
			return new jQuery.Event( src, props );
		}
	
		// Event object
		if ( src && src.type ) {
			this.originalEvent = src;
			this.type = src.type;
	
			// Events bubbling up the document may have been marked as prevented
			// by a handler lower down the tree; reflect the correct value.
			this.isDefaultPrevented = src.defaultPrevented ||
					src.defaultPrevented === undefined &&
	
					// Support: Android <=2.3 only
					src.returnValue === false ?
				returnTrue :
				returnFalse;
	
			// Create target properties
			// Support: Safari <=6 - 7 only
			// Target should not be a text node (#504, #13143)
			this.target = ( src.target && src.target.nodeType === 3 ) ?
				src.target.parentNode :
				src.target;
	
			this.currentTarget = src.currentTarget;
			this.relatedTarget = src.relatedTarget;
	
		// Event type
		} else {
			this.type = src;
		}
	
		// Put explicitly provided properties onto the event object
		if ( props ) {
			jQuery.extend( this, props );
		}
	
		// Create a timestamp if incoming event doesn't have one
		this.timeStamp = src && src.timeStamp || jQuery.now();
	
		// Mark it as fixed
		this[ jQuery.expando ] = true;
	};
	
	// jQuery.Event is based on DOM3 Events as specified by the ECMAScript Language Binding
	// https://www.w3.org/TR/2003/WD-DOM-Level-3-Events-20030331/ecma-script-binding.html
	jQuery.Event.prototype = {
		constructor: jQuery.Event,
		isDefaultPrevented: returnFalse,
		isPropagationStopped: returnFalse,
		isImmediatePropagationStopped: returnFalse,
		isSimulated: false,
	
		preventDefault: function() {
			var e = this.originalEvent;
	
			this.isDefaultPrevented = returnTrue;
	
			if ( e && !this.isSimulated ) {
				e.preventDefault();
			}
		},
		stopPropagation: function() {
			var e = this.originalEvent;
	
			this.isPropagationStopped = returnTrue;
	
			if ( e && !this.isSimulated ) {
				e.stopPropagation();
			}
		},
		stopImmediatePropagation: function() {
			var e = this.originalEvent;
	
			this.isImmediatePropagationStopped = returnTrue;
	
			if ( e && !this.isSimulated ) {
				e.stopImmediatePropagation();
			}
	
			this.stopPropagation();
		}
	};
	
	// Includes all common event props including KeyEvent and MouseEvent specific props
	jQuery.each( {
		altKey: true,
		bubbles: true,
		cancelable: true,
		changedTouches: true,
		ctrlKey: true,
		detail: true,
		eventPhase: true,
		metaKey: true,
		pageX: true,
		pageY: true,
		shiftKey: true,
		view: true,
		"char": true,
		charCode: true,
		key: true,
		keyCode: true,
		button: true,
		buttons: true,
		clientX: true,
		clientY: true,
		offsetX: true,
		offsetY: true,
		pointerId: true,
		pointerType: true,
		screenX: true,
		screenY: true,
		targetTouches: true,
		toElement: true,
		touches: true,
	
		which: function( event ) {
			var button = event.button;
	
			// Add which for key events
			if ( event.which == null && rkeyEvent.test( event.type ) ) {
				return event.charCode != null ? event.charCode : event.keyCode;
			}
	
			// Add which for click: 1 === left; 2 === middle; 3 === right
			if ( !event.which && button !== undefined && rmouseEvent.test( event.type ) ) {
				if ( button & 1 ) {
					return 1;
				}
	
				if ( button & 2 ) {
					return 3;
				}
	
				if ( button & 4 ) {
					return 2;
				}
	
				return 0;
			}
	
			return event.which;
		}
	}, jQuery.event.addProp );
	
	// Create mouseenter/leave events using mouseover/out and event-time checks
	// so that event delegation works in jQuery.
	// Do the same for pointerenter/pointerleave and pointerover/pointerout
	//
	// Support: Safari 7 only
	// Safari sends mouseenter too often; see:
	// https://bugs.chromium.org/p/chromium/issues/detail?id=470258
	// for the description of the bug (it existed in older Chrome versions as well).
	jQuery.each( {
		mouseenter: "mouseover",
		mouseleave: "mouseout",
		pointerenter: "pointerover",
		pointerleave: "pointerout"
	}, function( orig, fix ) {
		jQuery.event.special[ orig ] = {
			delegateType: fix,
			bindType: fix,
	
			handle: function( event ) {
				var ret,
					target = this,
					related = event.relatedTarget,
					handleObj = event.handleObj;
	
				// For mouseenter/leave call the handler if related is outside the target.
				// NB: No relatedTarget if the mouse left/entered the browser window
				if ( !related || ( related !== target && !jQuery.contains( target, related ) ) ) {
					event.type = handleObj.origType;
					ret = handleObj.handler.apply( this, arguments );
					event.type = fix;
				}
				return ret;
			}
		};
	} );
	
	jQuery.fn.extend( {
	
		on: function( types, selector, data, fn ) {
			return on( this, types, selector, data, fn );
		},
		one: function( types, selector, data, fn ) {
			return on( this, types, selector, data, fn, 1 );
		},
		off: function( types, selector, fn ) {
			var handleObj, type;
			if ( types && types.preventDefault && types.handleObj ) {
	
				// ( event )  dispatched jQuery.Event
				handleObj = types.handleObj;
				jQuery( types.delegateTarget ).off(
					handleObj.namespace ?
						handleObj.origType + "." + handleObj.namespace :
						handleObj.origType,
					handleObj.selector,
					handleObj.handler
				);
				return this;
			}
			if ( typeof types === "object" ) {
	
				// ( types-object [, selector] )
				for ( type in types ) {
					this.off( type, selector, types[ type ] );
				}
				return this;
			}
			if ( selector === false || typeof selector === "function" ) {
	
				// ( types [, fn] )
				fn = selector;
				selector = undefined;
			}
			if ( fn === false ) {
				fn = returnFalse;
			}
			return this.each( function() {
				jQuery.event.remove( this, types, fn, selector );
			} );
		}
	} );
	
	
	var
	
		/* eslint-disable max-len */
	
		// See https://github.com/eslint/eslint/issues/3229
		rxhtmlTag = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,
	
		/* eslint-enable */
	
		// Support: IE <=10 - 11, Edge 12 - 13
		// In IE/Edge using regex groups here causes severe slowdowns.
		// See https://connect.microsoft.com/IE/feedback/details/1736512/
		rnoInnerhtml = /<script|<style|<link/i,
	
		// checked="checked" or checked
		rchecked = /checked\s*(?:[^=]|=\s*.checked.)/i,
		rscriptTypeMasked = /^true\/(.*)/,
		rcleanScript = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;
	
	// Prefer a tbody over its parent table for containing new rows
	function manipulationTarget( elem, content ) {
		if ( nodeName( elem, "table" ) &&
			nodeName( content.nodeType !== 11 ? content : content.firstChild, "tr" ) ) {
	
			return jQuery( ">tbody", elem )[ 0 ] || elem;
		}
	
		return elem;
	}
	
	// Replace/restore the type attribute of script elements for safe DOM manipulation
	function disableScript( elem ) {
		elem.type = ( elem.getAttribute( "type" ) !== null ) + "/" + elem.type;
		return elem;
	}
	function restoreScript( elem ) {
		var match = rscriptTypeMasked.exec( elem.type );
	
		if ( match ) {
			elem.type = match[ 1 ];
		} else {
			elem.removeAttribute( "type" );
		}
	
		return elem;
	}
	
	function cloneCopyEvent( src, dest ) {
		var i, l, type, pdataOld, pdataCur, udataOld, udataCur, events;
	
		if ( dest.nodeType !== 1 ) {
			return;
		}
	
		// 1. Copy private data: events, handlers, etc.
		if ( dataPriv.hasData( src ) ) {
			pdataOld = dataPriv.access( src );
			pdataCur = dataPriv.set( dest, pdataOld );
			events = pdataOld.events;
	
			if ( events ) {
				delete pdataCur.handle;
				pdataCur.events = {};
	
				for ( type in events ) {
					for ( i = 0, l = events[ type ].length; i < l; i++ ) {
						jQuery.event.add( dest, type, events[ type ][ i ] );
					}
				}
			}
		}
	
		// 2. Copy user data
		if ( dataUser.hasData( src ) ) {
			udataOld = dataUser.access( src );
			udataCur = jQuery.extend( {}, udataOld );
	
			dataUser.set( dest, udataCur );
		}
	}
	
	// Fix IE bugs, see support tests
	function fixInput( src, dest ) {
		var nodeName = dest.nodeName.toLowerCase();
	
		// Fails to persist the checked state of a cloned checkbox or radio button.
		if ( nodeName === "input" && rcheckableType.test( src.type ) ) {
			dest.checked = src.checked;
	
		// Fails to return the selected option to the default selected state when cloning options
		} else if ( nodeName === "input" || nodeName === "textarea" ) {
			dest.defaultValue = src.defaultValue;
		}
	}
	
	function domManip( collection, args, callback, ignored ) {
	
		// Flatten any nested arrays
		args = concat.apply( [], args );
	
		var fragment, first, scripts, hasScripts, node, doc,
			i = 0,
			l = collection.length,
			iNoClone = l - 1,
			value = args[ 0 ],
			isFunction = jQuery.isFunction( value );
	
		// We can't cloneNode fragments that contain checked, in WebKit
		if ( isFunction ||
				( l > 1 && typeof value === "string" &&
					!support.checkClone && rchecked.test( value ) ) ) {
			return collection.each( function( index ) {
				var self = collection.eq( index );
				if ( isFunction ) {
					args[ 0 ] = value.call( this, index, self.html() );
				}
				domManip( self, args, callback, ignored );
			} );
		}
	
		if ( l ) {
			fragment = buildFragment( args, collection[ 0 ].ownerDocument, false, collection, ignored );
			first = fragment.firstChild;
	
			if ( fragment.childNodes.length === 1 ) {
				fragment = first;
			}
	
			// Require either new content or an interest in ignored elements to invoke the callback
			if ( first || ignored ) {
				scripts = jQuery.map( getAll( fragment, "script" ), disableScript );
				hasScripts = scripts.length;
	
				// Use the original fragment for the last item
				// instead of the first because it can end up
				// being emptied incorrectly in certain situations (#8070).
				for ( ; i < l; i++ ) {
					node = fragment;
	
					if ( i !== iNoClone ) {
						node = jQuery.clone( node, true, true );
	
						// Keep references to cloned scripts for later restoration
						if ( hasScripts ) {
	
							// Support: Android <=4.0 only, PhantomJS 1 only
							// push.apply(_, arraylike) throws on ancient WebKit
							jQuery.merge( scripts, getAll( node, "script" ) );
						}
					}
	
					callback.call( collection[ i ], node, i );
				}
	
				if ( hasScripts ) {
					doc = scripts[ scripts.length - 1 ].ownerDocument;
	
					// Reenable scripts
					jQuery.map( scripts, restoreScript );
	
					// Evaluate executable scripts on first document insertion
					for ( i = 0; i < hasScripts; i++ ) {
						node = scripts[ i ];
						if ( rscriptType.test( node.type || "" ) &&
							!dataPriv.access( node, "globalEval" ) &&
							jQuery.contains( doc, node ) ) {
	
							if ( node.src ) {
	
								// Optional AJAX dependency, but won't run scripts if not present
								if ( jQuery._evalUrl ) {
									jQuery._evalUrl( node.src );
								}
							} else {
								DOMEval( node.textContent.replace( rcleanScript, "" ), doc );
							}
						}
					}
				}
			}
		}
	
		return collection;
	}
	
	function remove( elem, selector, keepData ) {
		var node,
			nodes = selector ? jQuery.filter( selector, elem ) : elem,
			i = 0;
	
		for ( ; ( node = nodes[ i ] ) != null; i++ ) {
			if ( !keepData && node.nodeType === 1 ) {
				jQuery.cleanData( getAll( node ) );
			}
	
			if ( node.parentNode ) {
				if ( keepData && jQuery.contains( node.ownerDocument, node ) ) {
					setGlobalEval( getAll( node, "script" ) );
				}
				node.parentNode.removeChild( node );
			}
		}
	
		return elem;
	}
	
	jQuery.extend( {
		htmlPrefilter: function( html ) {
			return html.replace( rxhtmlTag, "<$1></$2>" );
		},
	
		clone: function( elem, dataAndEvents, deepDataAndEvents ) {
			var i, l, srcElements, destElements,
				clone = elem.cloneNode( true ),
				inPage = jQuery.contains( elem.ownerDocument, elem );
	
			// Fix IE cloning issues
			if ( !support.noCloneChecked && ( elem.nodeType === 1 || elem.nodeType === 11 ) &&
					!jQuery.isXMLDoc( elem ) ) {
	
				// We eschew Sizzle here for performance reasons: https://jsperf.com/getall-vs-sizzle/2
				destElements = getAll( clone );
				srcElements = getAll( elem );
	
				for ( i = 0, l = srcElements.length; i < l; i++ ) {
					fixInput( srcElements[ i ], destElements[ i ] );
				}
			}
	
			// Copy the events from the original to the clone
			if ( dataAndEvents ) {
				if ( deepDataAndEvents ) {
					srcElements = srcElements || getAll( elem );
					destElements = destElements || getAll( clone );
	
					for ( i = 0, l = srcElements.length; i < l; i++ ) {
						cloneCopyEvent( srcElements[ i ], destElements[ i ] );
					}
				} else {
					cloneCopyEvent( elem, clone );
				}
			}
	
			// Preserve script evaluation history
			destElements = getAll( clone, "script" );
			if ( destElements.length > 0 ) {
				setGlobalEval( destElements, !inPage && getAll( elem, "script" ) );
			}
	
			// Return the cloned set
			return clone;
		},
	
		cleanData: function( elems ) {
			var data, elem, type,
				special = jQuery.event.special,
				i = 0;
	
			for ( ; ( elem = elems[ i ] ) !== undefined; i++ ) {
				if ( acceptData( elem ) ) {
					if ( ( data = elem[ dataPriv.expando ] ) ) {
						if ( data.events ) {
							for ( type in data.events ) {
								if ( special[ type ] ) {
									jQuery.event.remove( elem, type );
	
								// This is a shortcut to avoid jQuery.event.remove's overhead
								} else {
									jQuery.removeEvent( elem, type, data.handle );
								}
							}
						}
	
						// Support: Chrome <=35 - 45+
						// Assign undefined instead of using delete, see Data#remove
						elem[ dataPriv.expando ] = undefined;
					}
					if ( elem[ dataUser.expando ] ) {
	
						// Support: Chrome <=35 - 45+
						// Assign undefined instead of using delete, see Data#remove
						elem[ dataUser.expando ] = undefined;
					}
				}
			}
		}
	} );
	
	jQuery.fn.extend( {
		detach: function( selector ) {
			return remove( this, selector, true );
		},
	
		remove: function( selector ) {
			return remove( this, selector );
		},
	
		text: function( value ) {
			return access( this, function( value ) {
				return value === undefined ?
					jQuery.text( this ) :
					this.empty().each( function() {
						if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
							this.textContent = value;
						}
					} );
			}, null, value, arguments.length );
		},
	
		append: function() {
			return domManip( this, arguments, function( elem ) {
				if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
					var target = manipulationTarget( this, elem );
					target.appendChild( elem );
				}
			} );
		},
	
		prepend: function() {
			return domManip( this, arguments, function( elem ) {
				if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
					var target = manipulationTarget( this, elem );
					target.insertBefore( elem, target.firstChild );
				}
			} );
		},
	
		before: function() {
			return domManip( this, arguments, function( elem ) {
				if ( this.parentNode ) {
					this.parentNode.insertBefore( elem, this );
				}
			} );
		},
	
		after: function() {
			return domManip( this, arguments, function( elem ) {
				if ( this.parentNode ) {
					this.parentNode.insertBefore( elem, this.nextSibling );
				}
			} );
		},
	
		empty: function() {
			var elem,
				i = 0;
	
			for ( ; ( elem = this[ i ] ) != null; i++ ) {
				if ( elem.nodeType === 1 ) {
	
					// Prevent memory leaks
					jQuery.cleanData( getAll( elem, false ) );
	
					// Remove any remaining nodes
					elem.textContent = "";
				}
			}
	
			return this;
		},
	
		clone: function( dataAndEvents, deepDataAndEvents ) {
			dataAndEvents = dataAndEvents == null ? false : dataAndEvents;
			deepDataAndEvents = deepDataAndEvents == null ? dataAndEvents : deepDataAndEvents;
	
			return this.map( function() {
				return jQuery.clone( this, dataAndEvents, deepDataAndEvents );
			} );
		},
	
		html: function( value ) {
			return access( this, function( value ) {
				var elem = this[ 0 ] || {},
					i = 0,
					l = this.length;
	
				if ( value === undefined && elem.nodeType === 1 ) {
					return elem.innerHTML;
				}
	
				// See if we can take a shortcut and just use innerHTML
				if ( typeof value === "string" && !rnoInnerhtml.test( value ) &&
					!wrapMap[ ( rtagName.exec( value ) || [ "", "" ] )[ 1 ].toLowerCase() ] ) {
	
					value = jQuery.htmlPrefilter( value );
	
					try {
						for ( ; i < l; i++ ) {
							elem = this[ i ] || {};
	
							// Remove element nodes and prevent memory leaks
							if ( elem.nodeType === 1 ) {
								jQuery.cleanData( getAll( elem, false ) );
								elem.innerHTML = value;
							}
						}
	
						elem = 0;
	
					// If using innerHTML throws an exception, use the fallback method
					} catch ( e ) {}
				}
	
				if ( elem ) {
					this.empty().append( value );
				}
			}, null, value, arguments.length );
		},
	
		replaceWith: function() {
			var ignored = [];
	
			// Make the changes, replacing each non-ignored context element with the new content
			return domManip( this, arguments, function( elem ) {
				var parent = this.parentNode;
	
				if ( jQuery.inArray( this, ignored ) < 0 ) {
					jQuery.cleanData( getAll( this ) );
					if ( parent ) {
						parent.replaceChild( elem, this );
					}
				}
	
			// Force callback invocation
			}, ignored );
		}
	} );
	
	jQuery.each( {
		appendTo: "append",
		prependTo: "prepend",
		insertBefore: "before",
		insertAfter: "after",
		replaceAll: "replaceWith"
	}, function( name, original ) {
		jQuery.fn[ name ] = function( selector ) {
			var elems,
				ret = [],
				insert = jQuery( selector ),
				last = insert.length - 1,
				i = 0;
	
			for ( ; i <= last; i++ ) {
				elems = i === last ? this : this.clone( true );
				jQuery( insert[ i ] )[ original ]( elems );
	
				// Support: Android <=4.0 only, PhantomJS 1 only
				// .get() because push.apply(_, arraylike) throws on ancient WebKit
				push.apply( ret, elems.get() );
			}
	
			return this.pushStack( ret );
		};
	} );
	var rmargin = ( /^margin/ );
	
	var rnumnonpx = new RegExp( "^(" + pnum + ")(?!px)[a-z%]+$", "i" );
	
	var getStyles = function( elem ) {
	
			// Support: IE <=11 only, Firefox <=30 (#15098, #14150)
			// IE throws on elements created in popups
			// FF meanwhile throws on frame elements through "defaultView.getComputedStyle"
			var view = elem.ownerDocument.defaultView;
	
			if ( !view || !view.opener ) {
				view = window;
			}
	
			return view.getComputedStyle( elem );
		};
	
	
	
	( function() {
	
		// Executing both pixelPosition & boxSizingReliable tests require only one layout
		// so they're executed at the same time to save the second computation.
		function computeStyleTests() {
	
			// This is a singleton, we need to execute it only once
			if ( !div ) {
				return;
			}
	
			div.style.cssText =
				"box-sizing:border-box;" +
				"position:relative;display:block;" +
				"margin:auto;border:1px;padding:1px;" +
				"top:1%;width:50%";
			div.innerHTML = "";
			documentElement.appendChild( container );
	
			var divStyle = window.getComputedStyle( div );
			pixelPositionVal = divStyle.top !== "1%";
	
			// Support: Android 4.0 - 4.3 only, Firefox <=3 - 44
			reliableMarginLeftVal = divStyle.marginLeft === "2px";
			boxSizingReliableVal = divStyle.width === "4px";
	
			// Support: Android 4.0 - 4.3 only
			// Some styles come back with percentage values, even though they shouldn't
			div.style.marginRight = "50%";
			pixelMarginRightVal = divStyle.marginRight === "4px";
	
			documentElement.removeChild( container );
	
			// Nullify the div so it wouldn't be stored in the memory and
			// it will also be a sign that checks already performed
			div = null;
		}
	
		var pixelPositionVal, boxSizingReliableVal, pixelMarginRightVal, reliableMarginLeftVal,
			container = document.createElement( "div" ),
			div = document.createElement( "div" );
	
		// Finish early in limited (non-browser) environments
		if ( !div.style ) {
			return;
		}
	
		// Support: IE <=9 - 11 only
		// Style of cloned element affects source element cloned (#8908)
		div.style.backgroundClip = "content-box";
		div.cloneNode( true ).style.backgroundClip = "";
		support.clearCloneStyle = div.style.backgroundClip === "content-box";
	
		container.style.cssText = "border:0;width:8px;height:0;top:0;left:-9999px;" +
			"padding:0;margin-top:1px;position:absolute";
		container.appendChild( div );
	
		jQuery.extend( support, {
			pixelPosition: function() {
				computeStyleTests();
				return pixelPositionVal;
			},
			boxSizingReliable: function() {
				computeStyleTests();
				return boxSizingReliableVal;
			},
			pixelMarginRight: function() {
				computeStyleTests();
				return pixelMarginRightVal;
			},
			reliableMarginLeft: function() {
				computeStyleTests();
				return reliableMarginLeftVal;
			}
		} );
	} )();
	
	
	function curCSS( elem, name, computed ) {
		var width, minWidth, maxWidth, ret,
	
			// Support: Firefox 51+
			// Retrieving style before computed somehow
			// fixes an issue with getting wrong values
			// on detached elements
			style = elem.style;
	
		computed = computed || getStyles( elem );
	
		// getPropertyValue is needed for:
		//   .css('filter') (IE 9 only, #12537)
		//   .css('--customProperty) (#3144)
		if ( computed ) {
			ret = computed.getPropertyValue( name ) || computed[ name ];
	
			if ( ret === "" && !jQuery.contains( elem.ownerDocument, elem ) ) {
				ret = jQuery.style( elem, name );
			}
	
			// A tribute to the "awesome hack by Dean Edwards"
			// Android Browser returns percentage for some values,
			// but width seems to be reliably pixels.
			// This is against the CSSOM draft spec:
			// https://drafts.csswg.org/cssom/#resolved-values
			if ( !support.pixelMarginRight() && rnumnonpx.test( ret ) && rmargin.test( name ) ) {
	
				// Remember the original values
				width = style.width;
				minWidth = style.minWidth;
				maxWidth = style.maxWidth;
	
				// Put in the new values to get a computed value out
				style.minWidth = style.maxWidth = style.width = ret;
				ret = computed.width;
	
				// Revert the changed values
				style.width = width;
				style.minWidth = minWidth;
				style.maxWidth = maxWidth;
			}
		}
	
		return ret !== undefined ?
	
			// Support: IE <=9 - 11 only
			// IE returns zIndex value as an integer.
			ret + "" :
			ret;
	}
	
	
	function addGetHookIf( conditionFn, hookFn ) {
	
		// Define the hook, we'll check on the first run if it's really needed.
		return {
			get: function() {
				if ( conditionFn() ) {
	
					// Hook not needed (or it's not possible to use it due
					// to missing dependency), remove it.
					delete this.get;
					return;
				}
	
				// Hook needed; redefine it so that the support test is not executed again.
				return ( this.get = hookFn ).apply( this, arguments );
			}
		};
	}
	
	
	var
	
		// Swappable if display is none or starts with table
		// except "table", "table-cell", or "table-caption"
		// See here for display values: https://developer.mozilla.org/en-US/docs/CSS/display
		rdisplayswap = /^(none|table(?!-c[ea]).+)/,
		rcustomProp = /^--/,
		cssShow = { position: "absolute", visibility: "hidden", display: "block" },
		cssNormalTransform = {
			letterSpacing: "0",
			fontWeight: "400"
		},
	
		cssPrefixes = [ "Webkit", "Moz", "ms" ],
		emptyStyle = document.createElement( "div" ).style;
	
	// Return a css property mapped to a potentially vendor prefixed property
	function vendorPropName( name ) {
	
		// Shortcut for names that are not vendor prefixed
		if ( name in emptyStyle ) {
			return name;
		}
	
		// Check for vendor prefixed names
		var capName = name[ 0 ].toUpperCase() + name.slice( 1 ),
			i = cssPrefixes.length;
	
		while ( i-- ) {
			name = cssPrefixes[ i ] + capName;
			if ( name in emptyStyle ) {
				return name;
			}
		}
	}
	
	// Return a property mapped along what jQuery.cssProps suggests or to
	// a vendor prefixed property.
	function finalPropName( name ) {
		var ret = jQuery.cssProps[ name ];
		if ( !ret ) {
			ret = jQuery.cssProps[ name ] = vendorPropName( name ) || name;
		}
		return ret;
	}
	
	function setPositiveNumber( elem, value, subtract ) {
	
		// Any relative (+/-) values have already been
		// normalized at this point
		var matches = rcssNum.exec( value );
		return matches ?
	
			// Guard against undefined "subtract", e.g., when used as in cssHooks
			Math.max( 0, matches[ 2 ] - ( subtract || 0 ) ) + ( matches[ 3 ] || "px" ) :
			value;
	}
	
	function augmentWidthOrHeight( elem, name, extra, isBorderBox, styles ) {
		var i,
			val = 0;
	
		// If we already have the right measurement, avoid augmentation
		if ( extra === ( isBorderBox ? "border" : "content" ) ) {
			i = 4;
	
		// Otherwise initialize for horizontal or vertical properties
		} else {
			i = name === "width" ? 1 : 0;
		}
	
		for ( ; i < 4; i += 2 ) {
	
			// Both box models exclude margin, so add it if we want it
			if ( extra === "margin" ) {
				val += jQuery.css( elem, extra + cssExpand[ i ], true, styles );
			}
	
			if ( isBorderBox ) {
	
				// border-box includes padding, so remove it if we want content
				if ( extra === "content" ) {
					val -= jQuery.css( elem, "padding" + cssExpand[ i ], true, styles );
				}
	
				// At this point, extra isn't border nor margin, so remove border
				if ( extra !== "margin" ) {
					val -= jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );
				}
			} else {
	
				// At this point, extra isn't content, so add padding
				val += jQuery.css( elem, "padding" + cssExpand[ i ], true, styles );
	
				// At this point, extra isn't content nor padding, so add border
				if ( extra !== "padding" ) {
					val += jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );
				}
			}
		}
	
		return val;
	}
	
	function getWidthOrHeight( elem, name, extra ) {
	
		// Start with computed style
		var valueIsBorderBox,
			styles = getStyles( elem ),
			val = curCSS( elem, name, styles ),
			isBorderBox = jQuery.css( elem, "boxSizing", false, styles ) === "border-box";
	
		// Computed unit is not pixels. Stop here and return.
		if ( rnumnonpx.test( val ) ) {
			return val;
		}
	
		// Check for style in case a browser which returns unreliable values
		// for getComputedStyle silently falls back to the reliable elem.style
		valueIsBorderBox = isBorderBox &&
			( support.boxSizingReliable() || val === elem.style[ name ] );
	
		// Fall back to offsetWidth/Height when value is "auto"
		// This happens for inline elements with no explicit setting (gh-3571)
		if ( val === "auto" ) {
			val = elem[ "offset" + name[ 0 ].toUpperCase() + name.slice( 1 ) ];
		}
	
		// Normalize "", auto, and prepare for extra
		val = parseFloat( val ) || 0;
	
		// Use the active box-sizing model to add/subtract irrelevant styles
		return ( val +
			augmentWidthOrHeight(
				elem,
				name,
				extra || ( isBorderBox ? "border" : "content" ),
				valueIsBorderBox,
				styles
			)
		) + "px";
	}
	
	jQuery.extend( {
	
		// Add in style property hooks for overriding the default
		// behavior of getting and setting a style property
		cssHooks: {
			opacity: {
				get: function( elem, computed ) {
					if ( computed ) {
	
						// We should always get a number back from opacity
						var ret = curCSS( elem, "opacity" );
						return ret === "" ? "1" : ret;
					}
				}
			}
		},
	
		// Don't automatically add "px" to these possibly-unitless properties
		cssNumber: {
			"animationIterationCount": true,
			"columnCount": true,
			"fillOpacity": true,
			"flexGrow": true,
			"flexShrink": true,
			"fontWeight": true,
			"lineHeight": true,
			"opacity": true,
			"order": true,
			"orphans": true,
			"widows": true,
			"zIndex": true,
			"zoom": true
		},
	
		// Add in properties whose names you wish to fix before
		// setting or getting the value
		cssProps: {
			"float": "cssFloat"
		},
	
		// Get and set the style property on a DOM Node
		style: function( elem, name, value, extra ) {
	
			// Don't set styles on text and comment nodes
			if ( !elem || elem.nodeType === 3 || elem.nodeType === 8 || !elem.style ) {
				return;
			}
	
			// Make sure that we're working with the right name
			var ret, type, hooks,
				origName = jQuery.camelCase( name ),
				isCustomProp = rcustomProp.test( name ),
				style = elem.style;
	
			// Make sure that we're working with the right name. We don't
			// want to query the value if it is a CSS custom property
			// since they are user-defined.
			if ( !isCustomProp ) {
				name = finalPropName( origName );
			}
	
			// Gets hook for the prefixed version, then unprefixed version
			hooks = jQuery.cssHooks[ name ] || jQuery.cssHooks[ origName ];
	
			// Check if we're setting a value
			if ( value !== undefined ) {
				type = typeof value;
	
				// Convert "+=" or "-=" to relative numbers (#7345)
				if ( type === "string" && ( ret = rcssNum.exec( value ) ) && ret[ 1 ] ) {
					value = adjustCSS( elem, name, ret );
	
					// Fixes bug #9237
					type = "number";
				}
	
				// Make sure that null and NaN values aren't set (#7116)
				if ( value == null || value !== value ) {
					return;
				}
	
				// If a number was passed in, add the unit (except for certain CSS properties)
				if ( type === "number" ) {
					value += ret && ret[ 3 ] || ( jQuery.cssNumber[ origName ] ? "" : "px" );
				}
	
				// background-* props affect original clone's values
				if ( !support.clearCloneStyle && value === "" && name.indexOf( "background" ) === 0 ) {
					style[ name ] = "inherit";
				}
	
				// If a hook was provided, use that value, otherwise just set the specified value
				if ( !hooks || !( "set" in hooks ) ||
					( value = hooks.set( elem, value, extra ) ) !== undefined ) {
	
					if ( isCustomProp ) {
						style.setProperty( name, value );
					} else {
						style[ name ] = value;
					}
				}
	
			} else {
	
				// If a hook was provided get the non-computed value from there
				if ( hooks && "get" in hooks &&
					( ret = hooks.get( elem, false, extra ) ) !== undefined ) {
	
					return ret;
				}
	
				// Otherwise just get the value from the style object
				return style[ name ];
			}
		},
	
		css: function( elem, name, extra, styles ) {
			var val, num, hooks,
				origName = jQuery.camelCase( name ),
				isCustomProp = rcustomProp.test( name );
	
			// Make sure that we're working with the right name. We don't
			// want to modify the value if it is a CSS custom property
			// since they are user-defined.
			if ( !isCustomProp ) {
				name = finalPropName( origName );
			}
	
			// Try prefixed name followed by the unprefixed name
			hooks = jQuery.cssHooks[ name ] || jQuery.cssHooks[ origName ];
	
			// If a hook was provided get the computed value from there
			if ( hooks && "get" in hooks ) {
				val = hooks.get( elem, true, extra );
			}
	
			// Otherwise, if a way to get the computed value exists, use that
			if ( val === undefined ) {
				val = curCSS( elem, name, styles );
			}
	
			// Convert "normal" to computed value
			if ( val === "normal" && name in cssNormalTransform ) {
				val = cssNormalTransform[ name ];
			}
	
			// Make numeric if forced or a qualifier was provided and val looks numeric
			if ( extra === "" || extra ) {
				num = parseFloat( val );
				return extra === true || isFinite( num ) ? num || 0 : val;
			}
	
			return val;
		}
	} );
	
	jQuery.each( [ "height", "width" ], function( i, name ) {
		jQuery.cssHooks[ name ] = {
			get: function( elem, computed, extra ) {
				if ( computed ) {
	
					// Certain elements can have dimension info if we invisibly show them
					// but it must have a current display style that would benefit
					return rdisplayswap.test( jQuery.css( elem, "display" ) ) &&
	
						// Support: Safari 8+
						// Table columns in Safari have non-zero offsetWidth & zero
						// getBoundingClientRect().width unless display is changed.
						// Support: IE <=11 only
						// Running getBoundingClientRect on a disconnected node
						// in IE throws an error.
						( !elem.getClientRects().length || !elem.getBoundingClientRect().width ) ?
							swap( elem, cssShow, function() {
								return getWidthOrHeight( elem, name, extra );
							} ) :
							getWidthOrHeight( elem, name, extra );
				}
			},
	
			set: function( elem, value, extra ) {
				var matches,
					styles = extra && getStyles( elem ),
					subtract = extra && augmentWidthOrHeight(
						elem,
						name,
						extra,
						jQuery.css( elem, "boxSizing", false, styles ) === "border-box",
						styles
					);
	
				// Convert to pixels if value adjustment is needed
				if ( subtract && ( matches = rcssNum.exec( value ) ) &&
					( matches[ 3 ] || "px" ) !== "px" ) {
	
					elem.style[ name ] = value;
					value = jQuery.css( elem, name );
				}
	
				return setPositiveNumber( elem, value, subtract );
			}
		};
	} );
	
	jQuery.cssHooks.marginLeft = addGetHookIf( support.reliableMarginLeft,
		function( elem, computed ) {
			if ( computed ) {
				return ( parseFloat( curCSS( elem, "marginLeft" ) ) ||
					elem.getBoundingClientRect().left -
						swap( elem, { marginLeft: 0 }, function() {
							return elem.getBoundingClientRect().left;
						} )
					) + "px";
			}
		}
	);
	
	// These hooks are used by animate to expand properties
	jQuery.each( {
		margin: "",
		padding: "",
		border: "Width"
	}, function( prefix, suffix ) {
		jQuery.cssHooks[ prefix + suffix ] = {
			expand: function( value ) {
				var i = 0,
					expanded = {},
	
					// Assumes a single number if not a string
					parts = typeof value === "string" ? value.split( " " ) : [ value ];
	
				for ( ; i < 4; i++ ) {
					expanded[ prefix + cssExpand[ i ] + suffix ] =
						parts[ i ] || parts[ i - 2 ] || parts[ 0 ];
				}
	
				return expanded;
			}
		};
	
		if ( !rmargin.test( prefix ) ) {
			jQuery.cssHooks[ prefix + suffix ].set = setPositiveNumber;
		}
	} );
	
	jQuery.fn.extend( {
		css: function( name, value ) {
			return access( this, function( elem, name, value ) {
				var styles, len,
					map = {},
					i = 0;
	
				if ( Array.isArray( name ) ) {
					styles = getStyles( elem );
					len = name.length;
	
					for ( ; i < len; i++ ) {
						map[ name[ i ] ] = jQuery.css( elem, name[ i ], false, styles );
					}
	
					return map;
				}
	
				return value !== undefined ?
					jQuery.style( elem, name, value ) :
					jQuery.css( elem, name );
			}, name, value, arguments.length > 1 );
		}
	} );
	
	
	function Tween( elem, options, prop, end, easing ) {
		return new Tween.prototype.init( elem, options, prop, end, easing );
	}
	jQuery.Tween = Tween;
	
	Tween.prototype = {
		constructor: Tween,
		init: function( elem, options, prop, end, easing, unit ) {
			this.elem = elem;
			this.prop = prop;
			this.easing = easing || jQuery.easing._default;
			this.options = options;
			this.start = this.now = this.cur();
			this.end = end;
			this.unit = unit || ( jQuery.cssNumber[ prop ] ? "" : "px" );
		},
		cur: function() {
			var hooks = Tween.propHooks[ this.prop ];
	
			return hooks && hooks.get ?
				hooks.get( this ) :
				Tween.propHooks._default.get( this );
		},
		run: function( percent ) {
			var eased,
				hooks = Tween.propHooks[ this.prop ];
	
			if ( this.options.duration ) {
				this.pos = eased = jQuery.easing[ this.easing ](
					percent, this.options.duration * percent, 0, 1, this.options.duration
				);
			} else {
				this.pos = eased = percent;
			}
			this.now = ( this.end - this.start ) * eased + this.start;
	
			if ( this.options.step ) {
				this.options.step.call( this.elem, this.now, this );
			}
	
			if ( hooks && hooks.set ) {
				hooks.set( this );
			} else {
				Tween.propHooks._default.set( this );
			}
			return this;
		}
	};
	
	Tween.prototype.init.prototype = Tween.prototype;
	
	Tween.propHooks = {
		_default: {
			get: function( tween ) {
				var result;
	
				// Use a property on the element directly when it is not a DOM element,
				// or when there is no matching style property that exists.
				if ( tween.elem.nodeType !== 1 ||
					tween.elem[ tween.prop ] != null && tween.elem.style[ tween.prop ] == null ) {
					return tween.elem[ tween.prop ];
				}
	
				// Passing an empty string as a 3rd parameter to .css will automatically
				// attempt a parseFloat and fallback to a string if the parse fails.
				// Simple values such as "10px" are parsed to Float;
				// complex values such as "rotate(1rad)" are returned as-is.
				result = jQuery.css( tween.elem, tween.prop, "" );
	
				// Empty strings, null, undefined and "auto" are converted to 0.
				return !result || result === "auto" ? 0 : result;
			},
			set: function( tween ) {
	
				// Use step hook for back compat.
				// Use cssHook if its there.
				// Use .style if available and use plain properties where available.
				if ( jQuery.fx.step[ tween.prop ] ) {
					jQuery.fx.step[ tween.prop ]( tween );
				} else if ( tween.elem.nodeType === 1 &&
					( tween.elem.style[ jQuery.cssProps[ tween.prop ] ] != null ||
						jQuery.cssHooks[ tween.prop ] ) ) {
					jQuery.style( tween.elem, tween.prop, tween.now + tween.unit );
				} else {
					tween.elem[ tween.prop ] = tween.now;
				}
			}
		}
	};
	
	// Support: IE <=9 only
	// Panic based approach to setting things on disconnected nodes
	Tween.propHooks.scrollTop = Tween.propHooks.scrollLeft = {
		set: function( tween ) {
			if ( tween.elem.nodeType && tween.elem.parentNode ) {
				tween.elem[ tween.prop ] = tween.now;
			}
		}
	};
	
	jQuery.easing = {
		linear: function( p ) {
			return p;
		},
		swing: function( p ) {
			return 0.5 - Math.cos( p * Math.PI ) / 2;
		},
		_default: "swing"
	};
	
	jQuery.fx = Tween.prototype.init;
	
	// Back compat <1.8 extension point
	jQuery.fx.step = {};
	
	
	
	
	var
		fxNow, inProgress,
		rfxtypes = /^(?:toggle|show|hide)$/,
		rrun = /queueHooks$/;
	
	function schedule() {
		if ( inProgress ) {
			if ( document.hidden === false && window.requestAnimationFrame ) {
				window.requestAnimationFrame( schedule );
			} else {
				window.setTimeout( schedule, jQuery.fx.interval );
			}
	
			jQuery.fx.tick();
		}
	}
	
	// Animations created synchronously will run synchronously
	function createFxNow() {
		window.setTimeout( function() {
			fxNow = undefined;
		} );
		return ( fxNow = jQuery.now() );
	}
	
	// Generate parameters to create a standard animation
	function genFx( type, includeWidth ) {
		var which,
			i = 0,
			attrs = { height: type };
	
		// If we include width, step value is 1 to do all cssExpand values,
		// otherwise step value is 2 to skip over Left and Right
		includeWidth = includeWidth ? 1 : 0;
		for ( ; i < 4; i += 2 - includeWidth ) {
			which = cssExpand[ i ];
			attrs[ "margin" + which ] = attrs[ "padding" + which ] = type;
		}
	
		if ( includeWidth ) {
			attrs.opacity = attrs.width = type;
		}
	
		return attrs;
	}
	
	function createTween( value, prop, animation ) {
		var tween,
			collection = ( Animation.tweeners[ prop ] || [] ).concat( Animation.tweeners[ "*" ] ),
			index = 0,
			length = collection.length;
		for ( ; index < length; index++ ) {
			if ( ( tween = collection[ index ].call( animation, prop, value ) ) ) {
	
				// We're done with this property
				return tween;
			}
		}
	}
	
	function defaultPrefilter( elem, props, opts ) {
		var prop, value, toggle, hooks, oldfire, propTween, restoreDisplay, display,
			isBox = "width" in props || "height" in props,
			anim = this,
			orig = {},
			style = elem.style,
			hidden = elem.nodeType && isHiddenWithinTree( elem ),
			dataShow = dataPriv.get( elem, "fxshow" );
	
		// Queue-skipping animations hijack the fx hooks
		if ( !opts.queue ) {
			hooks = jQuery._queueHooks( elem, "fx" );
			if ( hooks.unqueued == null ) {
				hooks.unqueued = 0;
				oldfire = hooks.empty.fire;
				hooks.empty.fire = function() {
					if ( !hooks.unqueued ) {
						oldfire();
					}
				};
			}
			hooks.unqueued++;
	
			anim.always( function() {
	
				// Ensure the complete handler is called before this completes
				anim.always( function() {
					hooks.unqueued--;
					if ( !jQuery.queue( elem, "fx" ).length ) {
						hooks.empty.fire();
					}
				} );
			} );
		}
	
		// Detect show/hide animations
		for ( prop in props ) {
			value = props[ prop ];
			if ( rfxtypes.test( value ) ) {
				delete props[ prop ];
				toggle = toggle || value === "toggle";
				if ( value === ( hidden ? "hide" : "show" ) ) {
	
					// Pretend to be hidden if this is a "show" and
					// there is still data from a stopped show/hide
					if ( value === "show" && dataShow && dataShow[ prop ] !== undefined ) {
						hidden = true;
	
					// Ignore all other no-op show/hide data
					} else {
						continue;
					}
				}
				orig[ prop ] = dataShow && dataShow[ prop ] || jQuery.style( elem, prop );
			}
		}
	
		// Bail out if this is a no-op like .hide().hide()
		propTween = !jQuery.isEmptyObject( props );
		if ( !propTween && jQuery.isEmptyObject( orig ) ) {
			return;
		}
	
		// Restrict "overflow" and "display" styles during box animations
		if ( isBox && elem.nodeType === 1 ) {
	
			// Support: IE <=9 - 11, Edge 12 - 13
			// Record all 3 overflow attributes because IE does not infer the shorthand
			// from identically-valued overflowX and overflowY
			opts.overflow = [ style.overflow, style.overflowX, style.overflowY ];
	
			// Identify a display type, preferring old show/hide data over the CSS cascade
			restoreDisplay = dataShow && dataShow.display;
			if ( restoreDisplay == null ) {
				restoreDisplay = dataPriv.get( elem, "display" );
			}
			display = jQuery.css( elem, "display" );
			if ( display === "none" ) {
				if ( restoreDisplay ) {
					display = restoreDisplay;
				} else {
	
					// Get nonempty value(s) by temporarily forcing visibility
					showHide( [ elem ], true );
					restoreDisplay = elem.style.display || restoreDisplay;
					display = jQuery.css( elem, "display" );
					showHide( [ elem ] );
				}
			}
	
			// Animate inline elements as inline-block
			if ( display === "inline" || display === "inline-block" && restoreDisplay != null ) {
				if ( jQuery.css( elem, "float" ) === "none" ) {
	
					// Restore the original display value at the end of pure show/hide animations
					if ( !propTween ) {
						anim.done( function() {
							style.display = restoreDisplay;
						} );
						if ( restoreDisplay == null ) {
							display = style.display;
							restoreDisplay = display === "none" ? "" : display;
						}
					}
					style.display = "inline-block";
				}
			}
		}
	
		if ( opts.overflow ) {
			style.overflow = "hidden";
			anim.always( function() {
				style.overflow = opts.overflow[ 0 ];
				style.overflowX = opts.overflow[ 1 ];
				style.overflowY = opts.overflow[ 2 ];
			} );
		}
	
		// Implement show/hide animations
		propTween = false;
		for ( prop in orig ) {
	
			// General show/hide setup for this element animation
			if ( !propTween ) {
				if ( dataShow ) {
					if ( "hidden" in dataShow ) {
						hidden = dataShow.hidden;
					}
				} else {
					dataShow = dataPriv.access( elem, "fxshow", { display: restoreDisplay } );
				}
	
				// Store hidden/visible for toggle so `.stop().toggle()` "reverses"
				if ( toggle ) {
					dataShow.hidden = !hidden;
				}
	
				// Show elements before animating them
				if ( hidden ) {
					showHide( [ elem ], true );
				}
	
				/* eslint-disable no-loop-func */
	
				anim.done( function() {
	
				/* eslint-enable no-loop-func */
	
					// The final step of a "hide" animation is actually hiding the element
					if ( !hidden ) {
						showHide( [ elem ] );
					}
					dataPriv.remove( elem, "fxshow" );
					for ( prop in orig ) {
						jQuery.style( elem, prop, orig[ prop ] );
					}
				} );
			}
	
			// Per-property setup
			propTween = createTween( hidden ? dataShow[ prop ] : 0, prop, anim );
			if ( !( prop in dataShow ) ) {
				dataShow[ prop ] = propTween.start;
				if ( hidden ) {
					propTween.end = propTween.start;
					propTween.start = 0;
				}
			}
		}
	}
	
	function propFilter( props, specialEasing ) {
		var index, name, easing, value, hooks;
	
		// camelCase, specialEasing and expand cssHook pass
		for ( index in props ) {
			name = jQuery.camelCase( index );
			easing = specialEasing[ name ];
			value = props[ index ];
			if ( Array.isArray( value ) ) {
				easing = value[ 1 ];
				value = props[ index ] = value[ 0 ];
			}
	
			if ( index !== name ) {
				props[ name ] = value;
				delete props[ index ];
			}
	
			hooks = jQuery.cssHooks[ name ];
			if ( hooks && "expand" in hooks ) {
				value = hooks.expand( value );
				delete props[ name ];
	
				// Not quite $.extend, this won't overwrite existing keys.
				// Reusing 'index' because we have the correct "name"
				for ( index in value ) {
					if ( !( index in props ) ) {
						props[ index ] = value[ index ];
						specialEasing[ index ] = easing;
					}
				}
			} else {
				specialEasing[ name ] = easing;
			}
		}
	}
	
	function Animation( elem, properties, options ) {
		var result,
			stopped,
			index = 0,
			length = Animation.prefilters.length,
			deferred = jQuery.Deferred().always( function() {
	
				// Don't match elem in the :animated selector
				delete tick.elem;
			} ),
			tick = function() {
				if ( stopped ) {
					return false;
				}
				var currentTime = fxNow || createFxNow(),
					remaining = Math.max( 0, animation.startTime + animation.duration - currentTime ),
	
					// Support: Android 2.3 only
					// Archaic crash bug won't allow us to use `1 - ( 0.5 || 0 )` (#12497)
					temp = remaining / animation.duration || 0,
					percent = 1 - temp,
					index = 0,
					length = animation.tweens.length;
	
				for ( ; index < length; index++ ) {
					animation.tweens[ index ].run( percent );
				}
	
				deferred.notifyWith( elem, [ animation, percent, remaining ] );
	
				// If there's more to do, yield
				if ( percent < 1 && length ) {
					return remaining;
				}
	
				// If this was an empty animation, synthesize a final progress notification
				if ( !length ) {
					deferred.notifyWith( elem, [ animation, 1, 0 ] );
				}
	
				// Resolve the animation and report its conclusion
				deferred.resolveWith( elem, [ animation ] );
				return false;
			},
			animation = deferred.promise( {
				elem: elem,
				props: jQuery.extend( {}, properties ),
				opts: jQuery.extend( true, {
					specialEasing: {},
					easing: jQuery.easing._default
				}, options ),
				originalProperties: properties,
				originalOptions: options,
				startTime: fxNow || createFxNow(),
				duration: options.duration,
				tweens: [],
				createTween: function( prop, end ) {
					var tween = jQuery.Tween( elem, animation.opts, prop, end,
							animation.opts.specialEasing[ prop ] || animation.opts.easing );
					animation.tweens.push( tween );
					return tween;
				},
				stop: function( gotoEnd ) {
					var index = 0,
	
						// If we are going to the end, we want to run all the tweens
						// otherwise we skip this part
						length = gotoEnd ? animation.tweens.length : 0;
					if ( stopped ) {
						return this;
					}
					stopped = true;
					for ( ; index < length; index++ ) {
						animation.tweens[ index ].run( 1 );
					}
	
					// Resolve when we played the last frame; otherwise, reject
					if ( gotoEnd ) {
						deferred.notifyWith( elem, [ animation, 1, 0 ] );
						deferred.resolveWith( elem, [ animation, gotoEnd ] );
					} else {
						deferred.rejectWith( elem, [ animation, gotoEnd ] );
					}
					return this;
				}
			} ),
			props = animation.props;
	
		propFilter( props, animation.opts.specialEasing );
	
		for ( ; index < length; index++ ) {
			result = Animation.prefilters[ index ].call( animation, elem, props, animation.opts );
			if ( result ) {
				if ( jQuery.isFunction( result.stop ) ) {
					jQuery._queueHooks( animation.elem, animation.opts.queue ).stop =
						jQuery.proxy( result.stop, result );
				}
				return result;
			}
		}
	
		jQuery.map( props, createTween, animation );
	
		if ( jQuery.isFunction( animation.opts.start ) ) {
			animation.opts.start.call( elem, animation );
		}
	
		// Attach callbacks from options
		animation
			.progress( animation.opts.progress )
			.done( animation.opts.done, animation.opts.complete )
			.fail( animation.opts.fail )
			.always( animation.opts.always );
	
		jQuery.fx.timer(
			jQuery.extend( tick, {
				elem: elem,
				anim: animation,
				queue: animation.opts.queue
			} )
		);
	
		return animation;
	}
	
	jQuery.Animation = jQuery.extend( Animation, {
	
		tweeners: {
			"*": [ function( prop, value ) {
				var tween = this.createTween( prop, value );
				adjustCSS( tween.elem, prop, rcssNum.exec( value ), tween );
				return tween;
			} ]
		},
	
		tweener: function( props, callback ) {
			if ( jQuery.isFunction( props ) ) {
				callback = props;
				props = [ "*" ];
			} else {
				props = props.match( rnothtmlwhite );
			}
	
			var prop,
				index = 0,
				length = props.length;
	
			for ( ; index < length; index++ ) {
				prop = props[ index ];
				Animation.tweeners[ prop ] = Animation.tweeners[ prop ] || [];
				Animation.tweeners[ prop ].unshift( callback );
			}
		},
	
		prefilters: [ defaultPrefilter ],
	
		prefilter: function( callback, prepend ) {
			if ( prepend ) {
				Animation.prefilters.unshift( callback );
			} else {
				Animation.prefilters.push( callback );
			}
		}
	} );
	
	jQuery.speed = function( speed, easing, fn ) {
		var opt = speed && typeof speed === "object" ? jQuery.extend( {}, speed ) : {
			complete: fn || !fn && easing ||
				jQuery.isFunction( speed ) && speed,
			duration: speed,
			easing: fn && easing || easing && !jQuery.isFunction( easing ) && easing
		};
	
		// Go to the end state if fx are off
		if ( jQuery.fx.off ) {
			opt.duration = 0;
	
		} else {
			if ( typeof opt.duration !== "number" ) {
				if ( opt.duration in jQuery.fx.speeds ) {
					opt.duration = jQuery.fx.speeds[ opt.duration ];
	
				} else {
					opt.duration = jQuery.fx.speeds._default;
				}
			}
		}
	
		// Normalize opt.queue - true/undefined/null -> "fx"
		if ( opt.queue == null || opt.queue === true ) {
			opt.queue = "fx";
		}
	
		// Queueing
		opt.old = opt.complete;
	
		opt.complete = function() {
			if ( jQuery.isFunction( opt.old ) ) {
				opt.old.call( this );
			}
	
			if ( opt.queue ) {
				jQuery.dequeue( this, opt.queue );
			}
		};
	
		return opt;
	};
	
	jQuery.fn.extend( {
		fadeTo: function( speed, to, easing, callback ) {
	
			// Show any hidden elements after setting opacity to 0
			return this.filter( isHiddenWithinTree ).css( "opacity", 0 ).show()
	
				// Animate to the value specified
				.end().animate( { opacity: to }, speed, easing, callback );
		},
		animate: function( prop, speed, easing, callback ) {
			var empty = jQuery.isEmptyObject( prop ),
				optall = jQuery.speed( speed, easing, callback ),
				doAnimation = function() {
	
					// Operate on a copy of prop so per-property easing won't be lost
					var anim = Animation( this, jQuery.extend( {}, prop ), optall );
	
					// Empty animations, or finishing resolves immediately
					if ( empty || dataPriv.get( this, "finish" ) ) {
						anim.stop( true );
					}
				};
				doAnimation.finish = doAnimation;
	
			return empty || optall.queue === false ?
				this.each( doAnimation ) :
				this.queue( optall.queue, doAnimation );
		},
		stop: function( type, clearQueue, gotoEnd ) {
			var stopQueue = function( hooks ) {
				var stop = hooks.stop;
				delete hooks.stop;
				stop( gotoEnd );
			};
	
			if ( typeof type !== "string" ) {
				gotoEnd = clearQueue;
				clearQueue = type;
				type = undefined;
			}
			if ( clearQueue && type !== false ) {
				this.queue( type || "fx", [] );
			}
	
			return this.each( function() {
				var dequeue = true,
					index = type != null && type + "queueHooks",
					timers = jQuery.timers,
					data = dataPriv.get( this );
	
				if ( index ) {
					if ( data[ index ] && data[ index ].stop ) {
						stopQueue( data[ index ] );
					}
				} else {
					for ( index in data ) {
						if ( data[ index ] && data[ index ].stop && rrun.test( index ) ) {
							stopQueue( data[ index ] );
						}
					}
				}
	
				for ( index = timers.length; index--; ) {
					if ( timers[ index ].elem === this &&
						( type == null || timers[ index ].queue === type ) ) {
	
						timers[ index ].anim.stop( gotoEnd );
						dequeue = false;
						timers.splice( index, 1 );
					}
				}
	
				// Start the next in the queue if the last step wasn't forced.
				// Timers currently will call their complete callbacks, which
				// will dequeue but only if they were gotoEnd.
				if ( dequeue || !gotoEnd ) {
					jQuery.dequeue( this, type );
				}
			} );
		},
		finish: function( type ) {
			if ( type !== false ) {
				type = type || "fx";
			}
			return this.each( function() {
				var index,
					data = dataPriv.get( this ),
					queue = data[ type + "queue" ],
					hooks = data[ type + "queueHooks" ],
					timers = jQuery.timers,
					length = queue ? queue.length : 0;
	
				// Enable finishing flag on private data
				data.finish = true;
	
				// Empty the queue first
				jQuery.queue( this, type, [] );
	
				if ( hooks && hooks.stop ) {
					hooks.stop.call( this, true );
				}
	
				// Look for any active animations, and finish them
				for ( index = timers.length; index--; ) {
					if ( timers[ index ].elem === this && timers[ index ].queue === type ) {
						timers[ index ].anim.stop( true );
						timers.splice( index, 1 );
					}
				}
	
				// Look for any animations in the old queue and finish them
				for ( index = 0; index < length; index++ ) {
					if ( queue[ index ] && queue[ index ].finish ) {
						queue[ index ].finish.call( this );
					}
				}
	
				// Turn off finishing flag
				delete data.finish;
			} );
		}
	} );
	
	jQuery.each( [ "toggle", "show", "hide" ], function( i, name ) {
		var cssFn = jQuery.fn[ name ];
		jQuery.fn[ name ] = function( speed, easing, callback ) {
			return speed == null || typeof speed === "boolean" ?
				cssFn.apply( this, arguments ) :
				this.animate( genFx( name, true ), speed, easing, callback );
		};
	} );
	
	// Generate shortcuts for custom animations
	jQuery.each( {
		slideDown: genFx( "show" ),
		slideUp: genFx( "hide" ),
		slideToggle: genFx( "toggle" ),
		fadeIn: { opacity: "show" },
		fadeOut: { opacity: "hide" },
		fadeToggle: { opacity: "toggle" }
	}, function( name, props ) {
		jQuery.fn[ name ] = function( speed, easing, callback ) {
			return this.animate( props, speed, easing, callback );
		};
	} );
	
	jQuery.timers = [];
	jQuery.fx.tick = function() {
		var timer,
			i = 0,
			timers = jQuery.timers;
	
		fxNow = jQuery.now();
	
		for ( ; i < timers.length; i++ ) {
			timer = timers[ i ];
	
			// Run the timer and safely remove it when done (allowing for external removal)
			if ( !timer() && timers[ i ] === timer ) {
				timers.splice( i--, 1 );
			}
		}
	
		if ( !timers.length ) {
			jQuery.fx.stop();
		}
		fxNow = undefined;
	};
	
	jQuery.fx.timer = function( timer ) {
		jQuery.timers.push( timer );
		jQuery.fx.start();
	};
	
	jQuery.fx.interval = 13;
	jQuery.fx.start = function() {
		if ( inProgress ) {
			return;
		}
	
		inProgress = true;
		schedule();
	};
	
	jQuery.fx.stop = function() {
		inProgress = null;
	};
	
	jQuery.fx.speeds = {
		slow: 600,
		fast: 200,
	
		// Default speed
		_default: 400
	};
	
	
	// Based off of the plugin by Clint Helfers, with permission.
	// https://web.archive.org/web/20100324014747/http://blindsignals.com/index.php/2009/07/jquery-delay/
	jQuery.fn.delay = function( time, type ) {
		time = jQuery.fx ? jQuery.fx.speeds[ time ] || time : time;
		type = type || "fx";
	
		return this.queue( type, function( next, hooks ) {
			var timeout = window.setTimeout( next, time );
			hooks.stop = function() {
				window.clearTimeout( timeout );
			};
		} );
	};
	
	
	( function() {
		var input = document.createElement( "input" ),
			select = document.createElement( "select" ),
			opt = select.appendChild( document.createElement( "option" ) );
	
		input.type = "checkbox";
	
		// Support: Android <=4.3 only
		// Default value for a checkbox should be "on"
		support.checkOn = input.value !== "";
	
		// Support: IE <=11 only
		// Must access selectedIndex to make default options select
		support.optSelected = opt.selected;
	
		// Support: IE <=11 only
		// An input loses its value after becoming a radio
		input = document.createElement( "input" );
		input.value = "t";
		input.type = "radio";
		support.radioValue = input.value === "t";
	} )();
	
	
	var boolHook,
		attrHandle = jQuery.expr.attrHandle;
	
	jQuery.fn.extend( {
		attr: function( name, value ) {
			return access( this, jQuery.attr, name, value, arguments.length > 1 );
		},
	
		removeAttr: function( name ) {
			return this.each( function() {
				jQuery.removeAttr( this, name );
			} );
		}
	} );
	
	jQuery.extend( {
		attr: function( elem, name, value ) {
			var ret, hooks,
				nType = elem.nodeType;
	
			// Don't get/set attributes on text, comment and attribute nodes
			if ( nType === 3 || nType === 8 || nType === 2 ) {
				return;
			}
	
			// Fallback to prop when attributes are not supported
			if ( typeof elem.getAttribute === "undefined" ) {
				return jQuery.prop( elem, name, value );
			}
	
			// Attribute hooks are determined by the lowercase version
			// Grab necessary hook if one is defined
			if ( nType !== 1 || !jQuery.isXMLDoc( elem ) ) {
				hooks = jQuery.attrHooks[ name.toLowerCase() ] ||
					( jQuery.expr.match.bool.test( name ) ? boolHook : undefined );
			}
	
			if ( value !== undefined ) {
				if ( value === null ) {
					jQuery.removeAttr( elem, name );
					return;
				}
	
				if ( hooks && "set" in hooks &&
					( ret = hooks.set( elem, value, name ) ) !== undefined ) {
					return ret;
				}
	
				elem.setAttribute( name, value + "" );
				return value;
			}
	
			if ( hooks && "get" in hooks && ( ret = hooks.get( elem, name ) ) !== null ) {
				return ret;
			}
	
			ret = jQuery.find.attr( elem, name );
	
			// Non-existent attributes return null, we normalize to undefined
			return ret == null ? undefined : ret;
		},
	
		attrHooks: {
			type: {
				set: function( elem, value ) {
					if ( !support.radioValue && value === "radio" &&
						nodeName( elem, "input" ) ) {
						var val = elem.value;
						elem.setAttribute( "type", value );
						if ( val ) {
							elem.value = val;
						}
						return value;
					}
				}
			}
		},
	
		removeAttr: function( elem, value ) {
			var name,
				i = 0,
	
				// Attribute names can contain non-HTML whitespace characters
				// https://html.spec.whatwg.org/multipage/syntax.html#attributes-2
				attrNames = value && value.match( rnothtmlwhite );
	
			if ( attrNames && elem.nodeType === 1 ) {
				while ( ( name = attrNames[ i++ ] ) ) {
					elem.removeAttribute( name );
				}
			}
		}
	} );
	
	// Hooks for boolean attributes
	boolHook = {
		set: function( elem, value, name ) {
			if ( value === false ) {
	
				// Remove boolean attributes when set to false
				jQuery.removeAttr( elem, name );
			} else {
				elem.setAttribute( name, name );
			}
			return name;
		}
	};
	
	jQuery.each( jQuery.expr.match.bool.source.match( /\w+/g ), function( i, name ) {
		var getter = attrHandle[ name ] || jQuery.find.attr;
	
		attrHandle[ name ] = function( elem, name, isXML ) {
			var ret, handle,
				lowercaseName = name.toLowerCase();
	
			if ( !isXML ) {
	
				// Avoid an infinite loop by temporarily removing this function from the getter
				handle = attrHandle[ lowercaseName ];
				attrHandle[ lowercaseName ] = ret;
				ret = getter( elem, name, isXML ) != null ?
					lowercaseName :
					null;
				attrHandle[ lowercaseName ] = handle;
			}
			return ret;
		};
	} );
	
	
	
	
	var rfocusable = /^(?:input|select|textarea|button)$/i,
		rclickable = /^(?:a|area)$/i;
	
	jQuery.fn.extend( {
		prop: function( name, value ) {
			return access( this, jQuery.prop, name, value, arguments.length > 1 );
		},
	
		removeProp: function( name ) {
			return this.each( function() {
				delete this[ jQuery.propFix[ name ] || name ];
			} );
		}
	} );
	
	jQuery.extend( {
		prop: function( elem, name, value ) {
			var ret, hooks,
				nType = elem.nodeType;
	
			// Don't get/set properties on text, comment and attribute nodes
			if ( nType === 3 || nType === 8 || nType === 2 ) {
				return;
			}
	
			if ( nType !== 1 || !jQuery.isXMLDoc( elem ) ) {
	
				// Fix name and attach hooks
				name = jQuery.propFix[ name ] || name;
				hooks = jQuery.propHooks[ name ];
			}
	
			if ( value !== undefined ) {
				if ( hooks && "set" in hooks &&
					( ret = hooks.set( elem, value, name ) ) !== undefined ) {
					return ret;
				}
	
				return ( elem[ name ] = value );
			}
	
			if ( hooks && "get" in hooks && ( ret = hooks.get( elem, name ) ) !== null ) {
				return ret;
			}
	
			return elem[ name ];
		},
	
		propHooks: {
			tabIndex: {
				get: function( elem ) {
	
					// Support: IE <=9 - 11 only
					// elem.tabIndex doesn't always return the
					// correct value when it hasn't been explicitly set
					// https://web.archive.org/web/20141116233347/http://fluidproject.org/blog/2008/01/09/getting-setting-and-removing-tabindex-values-with-javascript/
					// Use proper attribute retrieval(#12072)
					var tabindex = jQuery.find.attr( elem, "tabindex" );
	
					if ( tabindex ) {
						return parseInt( tabindex, 10 );
					}
	
					if (
						rfocusable.test( elem.nodeName ) ||
						rclickable.test( elem.nodeName ) &&
						elem.href
					) {
						return 0;
					}
	
					return -1;
				}
			}
		},
	
		propFix: {
			"for": "htmlFor",
			"class": "className"
		}
	} );
	
	// Support: IE <=11 only
	// Accessing the selectedIndex property
	// forces the browser to respect setting selected
	// on the option
	// The getter ensures a default option is selected
	// when in an optgroup
	// eslint rule "no-unused-expressions" is disabled for this code
	// since it considers such accessions noop
	if ( !support.optSelected ) {
		jQuery.propHooks.selected = {
			get: function( elem ) {
	
				/* eslint no-unused-expressions: "off" */
	
				var parent = elem.parentNode;
				if ( parent && parent.parentNode ) {
					parent.parentNode.selectedIndex;
				}
				return null;
			},
			set: function( elem ) {
	
				/* eslint no-unused-expressions: "off" */
	
				var parent = elem.parentNode;
				if ( parent ) {
					parent.selectedIndex;
	
					if ( parent.parentNode ) {
						parent.parentNode.selectedIndex;
					}
				}
			}
		};
	}
	
	jQuery.each( [
		"tabIndex",
		"readOnly",
		"maxLength",
		"cellSpacing",
		"cellPadding",
		"rowSpan",
		"colSpan",
		"useMap",
		"frameBorder",
		"contentEditable"
	], function() {
		jQuery.propFix[ this.toLowerCase() ] = this;
	} );
	
	
	
	
		// Strip and collapse whitespace according to HTML spec
		// https://html.spec.whatwg.org/multipage/infrastructure.html#strip-and-collapse-whitespace
		function stripAndCollapse( value ) {
			var tokens = value.match( rnothtmlwhite ) || [];
			return tokens.join( " " );
		}
	
	
	function getClass( elem ) {
		return elem.getAttribute && elem.getAttribute( "class" ) || "";
	}
	
	jQuery.fn.extend( {
		addClass: function( value ) {
			var classes, elem, cur, curValue, clazz, j, finalValue,
				i = 0;
	
			if ( jQuery.isFunction( value ) ) {
				return this.each( function( j ) {
					jQuery( this ).addClass( value.call( this, j, getClass( this ) ) );
				} );
			}
	
			if ( typeof value === "string" && value ) {
				classes = value.match( rnothtmlwhite ) || [];
	
				while ( ( elem = this[ i++ ] ) ) {
					curValue = getClass( elem );
					cur = elem.nodeType === 1 && ( " " + stripAndCollapse( curValue ) + " " );
	
					if ( cur ) {
						j = 0;
						while ( ( clazz = classes[ j++ ] ) ) {
							if ( cur.indexOf( " " + clazz + " " ) < 0 ) {
								cur += clazz + " ";
							}
						}
	
						// Only assign if different to avoid unneeded rendering.
						finalValue = stripAndCollapse( cur );
						if ( curValue !== finalValue ) {
							elem.setAttribute( "class", finalValue );
						}
					}
				}
			}
	
			return this;
		},
	
		removeClass: function( value ) {
			var classes, elem, cur, curValue, clazz, j, finalValue,
				i = 0;
	
			if ( jQuery.isFunction( value ) ) {
				return this.each( function( j ) {
					jQuery( this ).removeClass( value.call( this, j, getClass( this ) ) );
				} );
			}
	
			if ( !arguments.length ) {
				return this.attr( "class", "" );
			}
	
			if ( typeof value === "string" && value ) {
				classes = value.match( rnothtmlwhite ) || [];
	
				while ( ( elem = this[ i++ ] ) ) {
					curValue = getClass( elem );
	
					// This expression is here for better compressibility (see addClass)
					cur = elem.nodeType === 1 && ( " " + stripAndCollapse( curValue ) + " " );
	
					if ( cur ) {
						j = 0;
						while ( ( clazz = classes[ j++ ] ) ) {
	
							// Remove *all* instances
							while ( cur.indexOf( " " + clazz + " " ) > -1 ) {
								cur = cur.replace( " " + clazz + " ", " " );
							}
						}
	
						// Only assign if different to avoid unneeded rendering.
						finalValue = stripAndCollapse( cur );
						if ( curValue !== finalValue ) {
							elem.setAttribute( "class", finalValue );
						}
					}
				}
			}
	
			return this;
		},
	
		toggleClass: function( value, stateVal ) {
			var type = typeof value;
	
			if ( typeof stateVal === "boolean" && type === "string" ) {
				return stateVal ? this.addClass( value ) : this.removeClass( value );
			}
	
			if ( jQuery.isFunction( value ) ) {
				return this.each( function( i ) {
					jQuery( this ).toggleClass(
						value.call( this, i, getClass( this ), stateVal ),
						stateVal
					);
				} );
			}
	
			return this.each( function() {
				var className, i, self, classNames;
	
				if ( type === "string" ) {
	
					// Toggle individual class names
					i = 0;
					self = jQuery( this );
					classNames = value.match( rnothtmlwhite ) || [];
	
					while ( ( className = classNames[ i++ ] ) ) {
	
						// Check each className given, space separated list
						if ( self.hasClass( className ) ) {
							self.removeClass( className );
						} else {
							self.addClass( className );
						}
					}
	
				// Toggle whole class name
				} else if ( value === undefined || type === "boolean" ) {
					className = getClass( this );
					if ( className ) {
	
						// Store className if set
						dataPriv.set( this, "__className__", className );
					}
	
					// If the element has a class name or if we're passed `false`,
					// then remove the whole classname (if there was one, the above saved it).
					// Otherwise bring back whatever was previously saved (if anything),
					// falling back to the empty string if nothing was stored.
					if ( this.setAttribute ) {
						this.setAttribute( "class",
							className || value === false ?
							"" :
							dataPriv.get( this, "__className__" ) || ""
						);
					}
				}
			} );
		},
	
		hasClass: function( selector ) {
			var className, elem,
				i = 0;
	
			className = " " + selector + " ";
			while ( ( elem = this[ i++ ] ) ) {
				if ( elem.nodeType === 1 &&
					( " " + stripAndCollapse( getClass( elem ) ) + " " ).indexOf( className ) > -1 ) {
						return true;
				}
			}
	
			return false;
		}
	} );
	
	
	
	
	var rreturn = /\r/g;
	
	jQuery.fn.extend( {
		val: function( value ) {
			var hooks, ret, isFunction,
				elem = this[ 0 ];
	
			if ( !arguments.length ) {
				if ( elem ) {
					hooks = jQuery.valHooks[ elem.type ] ||
						jQuery.valHooks[ elem.nodeName.toLowerCase() ];
	
					if ( hooks &&
						"get" in hooks &&
						( ret = hooks.get( elem, "value" ) ) !== undefined
					) {
						return ret;
					}
	
					ret = elem.value;
	
					// Handle most common string cases
					if ( typeof ret === "string" ) {
						return ret.replace( rreturn, "" );
					}
	
					// Handle cases where value is null/undef or number
					return ret == null ? "" : ret;
				}
	
				return;
			}
	
			isFunction = jQuery.isFunction( value );
	
			return this.each( function( i ) {
				var val;
	
				if ( this.nodeType !== 1 ) {
					return;
				}
	
				if ( isFunction ) {
					val = value.call( this, i, jQuery( this ).val() );
				} else {
					val = value;
				}
	
				// Treat null/undefined as ""; convert numbers to string
				if ( val == null ) {
					val = "";
	
				} else if ( typeof val === "number" ) {
					val += "";
	
				} else if ( Array.isArray( val ) ) {
					val = jQuery.map( val, function( value ) {
						return value == null ? "" : value + "";
					} );
				}
	
				hooks = jQuery.valHooks[ this.type ] || jQuery.valHooks[ this.nodeName.toLowerCase() ];
	
				// If set returns undefined, fall back to normal setting
				if ( !hooks || !( "set" in hooks ) || hooks.set( this, val, "value" ) === undefined ) {
					this.value = val;
				}
			} );
		}
	} );
	
	jQuery.extend( {
		valHooks: {
			option: {
				get: function( elem ) {
	
					var val = jQuery.find.attr( elem, "value" );
					return val != null ?
						val :
	
						// Support: IE <=10 - 11 only
						// option.text throws exceptions (#14686, #14858)
						// Strip and collapse whitespace
						// https://html.spec.whatwg.org/#strip-and-collapse-whitespace
						stripAndCollapse( jQuery.text( elem ) );
				}
			},
			select: {
				get: function( elem ) {
					var value, option, i,
						options = elem.options,
						index = elem.selectedIndex,
						one = elem.type === "select-one",
						values = one ? null : [],
						max = one ? index + 1 : options.length;
	
					if ( index < 0 ) {
						i = max;
	
					} else {
						i = one ? index : 0;
					}
	
					// Loop through all the selected options
					for ( ; i < max; i++ ) {
						option = options[ i ];
	
						// Support: IE <=9 only
						// IE8-9 doesn't update selected after form reset (#2551)
						if ( ( option.selected || i === index ) &&
	
								// Don't return options that are disabled or in a disabled optgroup
								!option.disabled &&
								( !option.parentNode.disabled ||
									!nodeName( option.parentNode, "optgroup" ) ) ) {
	
							// Get the specific value for the option
							value = jQuery( option ).val();
	
							// We don't need an array for one selects
							if ( one ) {
								return value;
							}
	
							// Multi-Selects return an array
							values.push( value );
						}
					}
	
					return values;
				},
	
				set: function( elem, value ) {
					var optionSet, option,
						options = elem.options,
						values = jQuery.makeArray( value ),
						i = options.length;
	
					while ( i-- ) {
						option = options[ i ];
	
						/* eslint-disable no-cond-assign */
	
						if ( option.selected =
							jQuery.inArray( jQuery.valHooks.option.get( option ), values ) > -1
						) {
							optionSet = true;
						}
	
						/* eslint-enable no-cond-assign */
					}
	
					// Force browsers to behave consistently when non-matching value is set
					if ( !optionSet ) {
						elem.selectedIndex = -1;
					}
					return values;
				}
			}
		}
	} );
	
	// Radios and checkboxes getter/setter
	jQuery.each( [ "radio", "checkbox" ], function() {
		jQuery.valHooks[ this ] = {
			set: function( elem, value ) {
				if ( Array.isArray( value ) ) {
					return ( elem.checked = jQuery.inArray( jQuery( elem ).val(), value ) > -1 );
				}
			}
		};
		if ( !support.checkOn ) {
			jQuery.valHooks[ this ].get = function( elem ) {
				return elem.getAttribute( "value" ) === null ? "on" : elem.value;
			};
		}
	} );
	
	
	
	
	// Return jQuery for attributes-only inclusion
	
	
	var rfocusMorph = /^(?:focusinfocus|focusoutblur)$/;
	
	jQuery.extend( jQuery.event, {
	
		trigger: function( event, data, elem, onlyHandlers ) {
	
			var i, cur, tmp, bubbleType, ontype, handle, special,
				eventPath = [ elem || document ],
				type = hasOwn.call( event, "type" ) ? event.type : event,
				namespaces = hasOwn.call( event, "namespace" ) ? event.namespace.split( "." ) : [];
	
			cur = tmp = elem = elem || document;
	
			// Don't do events on text and comment nodes
			if ( elem.nodeType === 3 || elem.nodeType === 8 ) {
				return;
			}
	
			// focus/blur morphs to focusin/out; ensure we're not firing them right now
			if ( rfocusMorph.test( type + jQuery.event.triggered ) ) {
				return;
			}
	
			if ( type.indexOf( "." ) > -1 ) {
	
				// Namespaced trigger; create a regexp to match event type in handle()
				namespaces = type.split( "." );
				type = namespaces.shift();
				namespaces.sort();
			}
			ontype = type.indexOf( ":" ) < 0 && "on" + type;
	
			// Caller can pass in a jQuery.Event object, Object, or just an event type string
			event = event[ jQuery.expando ] ?
				event :
				new jQuery.Event( type, typeof event === "object" && event );
	
			// Trigger bitmask: & 1 for native handlers; & 2 for jQuery (always true)
			event.isTrigger = onlyHandlers ? 2 : 3;
			event.namespace = namespaces.join( "." );
			event.rnamespace = event.namespace ?
				new RegExp( "(^|\\.)" + namespaces.join( "\\.(?:.*\\.|)" ) + "(\\.|$)" ) :
				null;
	
			// Clean up the event in case it is being reused
			event.result = undefined;
			if ( !event.target ) {
				event.target = elem;
			}
	
			// Clone any incoming data and prepend the event, creating the handler arg list
			data = data == null ?
				[ event ] :
				jQuery.makeArray( data, [ event ] );
	
			// Allow special events to draw outside the lines
			special = jQuery.event.special[ type ] || {};
			if ( !onlyHandlers && special.trigger && special.trigger.apply( elem, data ) === false ) {
				return;
			}
	
			// Determine event propagation path in advance, per W3C events spec (#9951)
			// Bubble up to document, then to window; watch for a global ownerDocument var (#9724)
			if ( !onlyHandlers && !special.noBubble && !jQuery.isWindow( elem ) ) {
	
				bubbleType = special.delegateType || type;
				if ( !rfocusMorph.test( bubbleType + type ) ) {
					cur = cur.parentNode;
				}
				for ( ; cur; cur = cur.parentNode ) {
					eventPath.push( cur );
					tmp = cur;
				}
	
				// Only add window if we got to document (e.g., not plain obj or detached DOM)
				if ( tmp === ( elem.ownerDocument || document ) ) {
					eventPath.push( tmp.defaultView || tmp.parentWindow || window );
				}
			}
	
			// Fire handlers on the event path
			i = 0;
			while ( ( cur = eventPath[ i++ ] ) && !event.isPropagationStopped() ) {
	
				event.type = i > 1 ?
					bubbleType :
					special.bindType || type;
	
				// jQuery handler
				handle = ( dataPriv.get( cur, "events" ) || {} )[ event.type ] &&
					dataPriv.get( cur, "handle" );
				if ( handle ) {
					handle.apply( cur, data );
				}
	
				// Native handler
				handle = ontype && cur[ ontype ];
				if ( handle && handle.apply && acceptData( cur ) ) {
					event.result = handle.apply( cur, data );
					if ( event.result === false ) {
						event.preventDefault();
					}
				}
			}
			event.type = type;
	
			// If nobody prevented the default action, do it now
			if ( !onlyHandlers && !event.isDefaultPrevented() ) {
	
				if ( ( !special._default ||
					special._default.apply( eventPath.pop(), data ) === false ) &&
					acceptData( elem ) ) {
	
					// Call a native DOM method on the target with the same name as the event.
					// Don't do default actions on window, that's where global variables be (#6170)
					if ( ontype && jQuery.isFunction( elem[ type ] ) && !jQuery.isWindow( elem ) ) {
	
						// Don't re-trigger an onFOO event when we call its FOO() method
						tmp = elem[ ontype ];
	
						if ( tmp ) {
							elem[ ontype ] = null;
						}
	
						// Prevent re-triggering of the same event, since we already bubbled it above
						jQuery.event.triggered = type;
						elem[ type ]();
						jQuery.event.triggered = undefined;
	
						if ( tmp ) {
							elem[ ontype ] = tmp;
						}
					}
				}
			}
	
			return event.result;
		},
	
		// Piggyback on a donor event to simulate a different one
		// Used only for `focus(in | out)` events
		simulate: function( type, elem, event ) {
			var e = jQuery.extend(
				new jQuery.Event(),
				event,
				{
					type: type,
					isSimulated: true
				}
			);
	
			jQuery.event.trigger( e, null, elem );
		}
	
	} );
	
	jQuery.fn.extend( {
	
		trigger: function( type, data ) {
			return this.each( function() {
				jQuery.event.trigger( type, data, this );
			} );
		},
		triggerHandler: function( type, data ) {
			var elem = this[ 0 ];
			if ( elem ) {
				return jQuery.event.trigger( type, data, elem, true );
			}
		}
	} );
	
	
	jQuery.each( ( "blur focus focusin focusout resize scroll click dblclick " +
		"mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave " +
		"change select submit keydown keypress keyup contextmenu" ).split( " " ),
		function( i, name ) {
	
		// Handle event binding
		jQuery.fn[ name ] = function( data, fn ) {
			return arguments.length > 0 ?
				this.on( name, null, data, fn ) :
				this.trigger( name );
		};
	} );
	
	jQuery.fn.extend( {
		hover: function( fnOver, fnOut ) {
			return this.mouseenter( fnOver ).mouseleave( fnOut || fnOver );
		}
	} );
	
	
	
	
	support.focusin = "onfocusin" in window;
	
	
	// Support: Firefox <=44
	// Firefox doesn't have focus(in | out) events
	// Related ticket - https://bugzilla.mozilla.org/show_bug.cgi?id=687787
	//
	// Support: Chrome <=48 - 49, Safari <=9.0 - 9.1
	// focus(in | out) events fire after focus & blur events,
	// which is spec violation - http://www.w3.org/TR/DOM-Level-3-Events/#events-focusevent-event-order
	// Related ticket - https://bugs.chromium.org/p/chromium/issues/detail?id=449857
	if ( !support.focusin ) {
		jQuery.each( { focus: "focusin", blur: "focusout" }, function( orig, fix ) {
	
			// Attach a single capturing handler on the document while someone wants focusin/focusout
			var handler = function( event ) {
				jQuery.event.simulate( fix, event.target, jQuery.event.fix( event ) );
			};
	
			jQuery.event.special[ fix ] = {
				setup: function() {
					var doc = this.ownerDocument || this,
						attaches = dataPriv.access( doc, fix );
	
					if ( !attaches ) {
						doc.addEventListener( orig, handler, true );
					}
					dataPriv.access( doc, fix, ( attaches || 0 ) + 1 );
				},
				teardown: function() {
					var doc = this.ownerDocument || this,
						attaches = dataPriv.access( doc, fix ) - 1;
	
					if ( !attaches ) {
						doc.removeEventListener( orig, handler, true );
						dataPriv.remove( doc, fix );
	
					} else {
						dataPriv.access( doc, fix, attaches );
					}
				}
			};
		} );
	}
	var location = window.location;
	
	var nonce = jQuery.now();
	
	var rquery = ( /\?/ );
	
	
	
	// Cross-browser xml parsing
	jQuery.parseXML = function( data ) {
		var xml;
		if ( !data || typeof data !== "string" ) {
			return null;
		}
	
		// Support: IE 9 - 11 only
		// IE throws on parseFromString with invalid input.
		try {
			xml = ( new window.DOMParser() ).parseFromString( data, "text/xml" );
		} catch ( e ) {
			xml = undefined;
		}
	
		if ( !xml || xml.getElementsByTagName( "parsererror" ).length ) {
			jQuery.error( "Invalid XML: " + data );
		}
		return xml;
	};
	
	
	var
		rbracket = /\[\]$/,
		rCRLF = /\r?\n/g,
		rsubmitterTypes = /^(?:submit|button|image|reset|file)$/i,
		rsubmittable = /^(?:input|select|textarea|keygen)/i;
	
	function buildParams( prefix, obj, traditional, add ) {
		var name;
	
		if ( Array.isArray( obj ) ) {
	
			// Serialize array item.
			jQuery.each( obj, function( i, v ) {
				if ( traditional || rbracket.test( prefix ) ) {
	
					// Treat each array item as a scalar.
					add( prefix, v );
	
				} else {
	
					// Item is non-scalar (array or object), encode its numeric index.
					buildParams(
						prefix + "[" + ( typeof v === "object" && v != null ? i : "" ) + "]",
						v,
						traditional,
						add
					);
				}
			} );
	
		} else if ( !traditional && jQuery.type( obj ) === "object" ) {
	
			// Serialize object item.
			for ( name in obj ) {
				buildParams( prefix + "[" + name + "]", obj[ name ], traditional, add );
			}
	
		} else {
	
			// Serialize scalar item.
			add( prefix, obj );
		}
	}
	
	// Serialize an array of form elements or a set of
	// key/values into a query string
	jQuery.param = function( a, traditional ) {
		var prefix,
			s = [],
			add = function( key, valueOrFunction ) {
	
				// If value is a function, invoke it and use its return value
				var value = jQuery.isFunction( valueOrFunction ) ?
					valueOrFunction() :
					valueOrFunction;
	
				s[ s.length ] = encodeURIComponent( key ) + "=" +
					encodeURIComponent( value == null ? "" : value );
			};
	
		// If an array was passed in, assume that it is an array of form elements.
		if ( Array.isArray( a ) || ( a.jquery && !jQuery.isPlainObject( a ) ) ) {
	
			// Serialize the form elements
			jQuery.each( a, function() {
				add( this.name, this.value );
			} );
	
		} else {
	
			// If traditional, encode the "old" way (the way 1.3.2 or older
			// did it), otherwise encode params recursively.
			for ( prefix in a ) {
				buildParams( prefix, a[ prefix ], traditional, add );
			}
		}
	
		// Return the resulting serialization
		return s.join( "&" );
	};
	
	jQuery.fn.extend( {
		serialize: function() {
			return jQuery.param( this.serializeArray() );
		},
		serializeArray: function() {
			return this.map( function() {
	
				// Can add propHook for "elements" to filter or add form elements
				var elements = jQuery.prop( this, "elements" );
				return elements ? jQuery.makeArray( elements ) : this;
			} )
			.filter( function() {
				var type = this.type;
	
				// Use .is( ":disabled" ) so that fieldset[disabled] works
				return this.name && !jQuery( this ).is( ":disabled" ) &&
					rsubmittable.test( this.nodeName ) && !rsubmitterTypes.test( type ) &&
					( this.checked || !rcheckableType.test( type ) );
			} )
			.map( function( i, elem ) {
				var val = jQuery( this ).val();
	
				if ( val == null ) {
					return null;
				}
	
				if ( Array.isArray( val ) ) {
					return jQuery.map( val, function( val ) {
						return { name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
					} );
				}
	
				return { name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
			} ).get();
		}
	} );
	
	
	var
		r20 = /%20/g,
		rhash = /#.*$/,
		rantiCache = /([?&])_=[^&]*/,
		rheaders = /^(.*?):[ \t]*([^\r\n]*)$/mg,
	
		// #7653, #8125, #8152: local protocol detection
		rlocalProtocol = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
		rnoContent = /^(?:GET|HEAD)$/,
		rprotocol = /^\/\//,
	
		/* Prefilters
		 * 1) They are useful to introduce custom dataTypes (see ajax/jsonp.js for an example)
		 * 2) These are called:
		 *    - BEFORE asking for a transport
		 *    - AFTER param serialization (s.data is a string if s.processData is true)
		 * 3) key is the dataType
		 * 4) the catchall symbol "*" can be used
		 * 5) execution will start with transport dataType and THEN continue down to "*" if needed
		 */
		prefilters = {},
	
		/* Transports bindings
		 * 1) key is the dataType
		 * 2) the catchall symbol "*" can be used
		 * 3) selection will start with transport dataType and THEN go to "*" if needed
		 */
		transports = {},
	
		// Avoid comment-prolog char sequence (#10098); must appease lint and evade compression
		allTypes = "*/".concat( "*" ),
	
		// Anchor tag for parsing the document origin
		originAnchor = document.createElement( "a" );
		originAnchor.href = location.href;
	
	// Base "constructor" for jQuery.ajaxPrefilter and jQuery.ajaxTransport
	function addToPrefiltersOrTransports( structure ) {
	
		// dataTypeExpression is optional and defaults to "*"
		return function( dataTypeExpression, func ) {
	
			if ( typeof dataTypeExpression !== "string" ) {
				func = dataTypeExpression;
				dataTypeExpression = "*";
			}
	
			var dataType,
				i = 0,
				dataTypes = dataTypeExpression.toLowerCase().match( rnothtmlwhite ) || [];
	
			if ( jQuery.isFunction( func ) ) {
	
				// For each dataType in the dataTypeExpression
				while ( ( dataType = dataTypes[ i++ ] ) ) {
	
					// Prepend if requested
					if ( dataType[ 0 ] === "+" ) {
						dataType = dataType.slice( 1 ) || "*";
						( structure[ dataType ] = structure[ dataType ] || [] ).unshift( func );
	
					// Otherwise append
					} else {
						( structure[ dataType ] = structure[ dataType ] || [] ).push( func );
					}
				}
			}
		};
	}
	
	// Base inspection function for prefilters and transports
	function inspectPrefiltersOrTransports( structure, options, originalOptions, jqXHR ) {
	
		var inspected = {},
			seekingTransport = ( structure === transports );
	
		function inspect( dataType ) {
			var selected;
			inspected[ dataType ] = true;
			jQuery.each( structure[ dataType ] || [], function( _, prefilterOrFactory ) {
				var dataTypeOrTransport = prefilterOrFactory( options, originalOptions, jqXHR );
				if ( typeof dataTypeOrTransport === "string" &&
					!seekingTransport && !inspected[ dataTypeOrTransport ] ) {
	
					options.dataTypes.unshift( dataTypeOrTransport );
					inspect( dataTypeOrTransport );
					return false;
				} else if ( seekingTransport ) {
					return !( selected = dataTypeOrTransport );
				}
			} );
			return selected;
		}
	
		return inspect( options.dataTypes[ 0 ] ) || !inspected[ "*" ] && inspect( "*" );
	}
	
	// A special extend for ajax options
	// that takes "flat" options (not to be deep extended)
	// Fixes #9887
	function ajaxExtend( target, src ) {
		var key, deep,
			flatOptions = jQuery.ajaxSettings.flatOptions || {};
	
		for ( key in src ) {
			if ( src[ key ] !== undefined ) {
				( flatOptions[ key ] ? target : ( deep || ( deep = {} ) ) )[ key ] = src[ key ];
			}
		}
		if ( deep ) {
			jQuery.extend( true, target, deep );
		}
	
		return target;
	}
	
	/* Handles responses to an ajax request:
	 * - finds the right dataType (mediates between content-type and expected dataType)
	 * - returns the corresponding response
	 */
	function ajaxHandleResponses( s, jqXHR, responses ) {
	
		var ct, type, finalDataType, firstDataType,
			contents = s.contents,
			dataTypes = s.dataTypes;
	
		// Remove auto dataType and get content-type in the process
		while ( dataTypes[ 0 ] === "*" ) {
			dataTypes.shift();
			if ( ct === undefined ) {
				ct = s.mimeType || jqXHR.getResponseHeader( "Content-Type" );
			}
		}
	
		// Check if we're dealing with a known content-type
		if ( ct ) {
			for ( type in contents ) {
				if ( contents[ type ] && contents[ type ].test( ct ) ) {
					dataTypes.unshift( type );
					break;
				}
			}
		}
	
		// Check to see if we have a response for the expected dataType
		if ( dataTypes[ 0 ] in responses ) {
			finalDataType = dataTypes[ 0 ];
		} else {
	
			// Try convertible dataTypes
			for ( type in responses ) {
				if ( !dataTypes[ 0 ] || s.converters[ type + " " + dataTypes[ 0 ] ] ) {
					finalDataType = type;
					break;
				}
				if ( !firstDataType ) {
					firstDataType = type;
				}
			}
	
			// Or just use first one
			finalDataType = finalDataType || firstDataType;
		}
	
		// If we found a dataType
		// We add the dataType to the list if needed
		// and return the corresponding response
		if ( finalDataType ) {
			if ( finalDataType !== dataTypes[ 0 ] ) {
				dataTypes.unshift( finalDataType );
			}
			return responses[ finalDataType ];
		}
	}
	
	/* Chain conversions given the request and the original response
	 * Also sets the responseXXX fields on the jqXHR instance
	 */
	function ajaxConvert( s, response, jqXHR, isSuccess ) {
		var conv2, current, conv, tmp, prev,
			converters = {},
	
			// Work with a copy of dataTypes in case we need to modify it for conversion
			dataTypes = s.dataTypes.slice();
	
		// Create converters map with lowercased keys
		if ( dataTypes[ 1 ] ) {
			for ( conv in s.converters ) {
				converters[ conv.toLowerCase() ] = s.converters[ conv ];
			}
		}
	
		current = dataTypes.shift();
	
		// Convert to each sequential dataType
		while ( current ) {
	
			if ( s.responseFields[ current ] ) {
				jqXHR[ s.responseFields[ current ] ] = response;
			}
	
			// Apply the dataFilter if provided
			if ( !prev && isSuccess && s.dataFilter ) {
				response = s.dataFilter( response, s.dataType );
			}
	
			prev = current;
			current = dataTypes.shift();
	
			if ( current ) {
	
				// There's only work to do if current dataType is non-auto
				if ( current === "*" ) {
	
					current = prev;
	
				// Convert response if prev dataType is non-auto and differs from current
				} else if ( prev !== "*" && prev !== current ) {
	
					// Seek a direct converter
					conv = converters[ prev + " " + current ] || converters[ "* " + current ];
	
					// If none found, seek a pair
					if ( !conv ) {
						for ( conv2 in converters ) {
	
							// If conv2 outputs current
							tmp = conv2.split( " " );
							if ( tmp[ 1 ] === current ) {
	
								// If prev can be converted to accepted input
								conv = converters[ prev + " " + tmp[ 0 ] ] ||
									converters[ "* " + tmp[ 0 ] ];
								if ( conv ) {
	
									// Condense equivalence converters
									if ( conv === true ) {
										conv = converters[ conv2 ];
	
									// Otherwise, insert the intermediate dataType
									} else if ( converters[ conv2 ] !== true ) {
										current = tmp[ 0 ];
										dataTypes.unshift( tmp[ 1 ] );
									}
									break;
								}
							}
						}
					}
	
					// Apply converter (if not an equivalence)
					if ( conv !== true ) {
	
						// Unless errors are allowed to bubble, catch and return them
						if ( conv && s.throws ) {
							response = conv( response );
						} else {
							try {
								response = conv( response );
							} catch ( e ) {
								return {
									state: "parsererror",
									error: conv ? e : "No conversion from " + prev + " to " + current
								};
							}
						}
					}
				}
			}
		}
	
		return { state: "success", data: response };
	}
	
	jQuery.extend( {
	
		// Counter for holding the number of active queries
		active: 0,
	
		// Last-Modified header cache for next request
		lastModified: {},
		etag: {},
	
		ajaxSettings: {
			url: location.href,
			type: "GET",
			isLocal: rlocalProtocol.test( location.protocol ),
			global: true,
			processData: true,
			async: true,
			contentType: "application/x-www-form-urlencoded; charset=UTF-8",
	
			/*
			timeout: 0,
			data: null,
			dataType: null,
			username: null,
			password: null,
			cache: null,
			throws: false,
			traditional: false,
			headers: {},
			*/
	
			accepts: {
				"*": allTypes,
				text: "text/plain",
				html: "text/html",
				xml: "application/xml, text/xml",
				json: "application/json, text/javascript"
			},
	
			contents: {
				xml: /\bxml\b/,
				html: /\bhtml/,
				json: /\bjson\b/
			},
	
			responseFields: {
				xml: "responseXML",
				text: "responseText",
				json: "responseJSON"
			},
	
			// Data converters
			// Keys separate source (or catchall "*") and destination types with a single space
			converters: {
	
				// Convert anything to text
				"* text": String,
	
				// Text to html (true = no transformation)
				"text html": true,
	
				// Evaluate text as a json expression
				"text json": JSON.parse,
	
				// Parse text as xml
				"text xml": jQuery.parseXML
			},
	
			// For options that shouldn't be deep extended:
			// you can add your own custom options here if
			// and when you create one that shouldn't be
			// deep extended (see ajaxExtend)
			flatOptions: {
				url: true,
				context: true
			}
		},
	
		// Creates a full fledged settings object into target
		// with both ajaxSettings and settings fields.
		// If target is omitted, writes into ajaxSettings.
		ajaxSetup: function( target, settings ) {
			return settings ?
	
				// Building a settings object
				ajaxExtend( ajaxExtend( target, jQuery.ajaxSettings ), settings ) :
	
				// Extending ajaxSettings
				ajaxExtend( jQuery.ajaxSettings, target );
		},
	
		ajaxPrefilter: addToPrefiltersOrTransports( prefilters ),
		ajaxTransport: addToPrefiltersOrTransports( transports ),
	
		// Main method
		ajax: function( url, options ) {
	
			// If url is an object, simulate pre-1.5 signature
			if ( typeof url === "object" ) {
				options = url;
				url = undefined;
			}
	
			// Force options to be an object
			options = options || {};
	
			var transport,
	
				// URL without anti-cache param
				cacheURL,
	
				// Response headers
				responseHeadersString,
				responseHeaders,
	
				// timeout handle
				timeoutTimer,
	
				// Url cleanup var
				urlAnchor,
	
				// Request state (becomes false upon send and true upon completion)
				completed,
	
				// To know if global events are to be dispatched
				fireGlobals,
	
				// Loop variable
				i,
	
				// uncached part of the url
				uncached,
	
				// Create the final options object
				s = jQuery.ajaxSetup( {}, options ),
	
				// Callbacks context
				callbackContext = s.context || s,
	
				// Context for global events is callbackContext if it is a DOM node or jQuery collection
				globalEventContext = s.context &&
					( callbackContext.nodeType || callbackContext.jquery ) ?
						jQuery( callbackContext ) :
						jQuery.event,
	
				// Deferreds
				deferred = jQuery.Deferred(),
				completeDeferred = jQuery.Callbacks( "once memory" ),
	
				// Status-dependent callbacks
				statusCode = s.statusCode || {},
	
				// Headers (they are sent all at once)
				requestHeaders = {},
				requestHeadersNames = {},
	
				// Default abort message
				strAbort = "canceled",
	
				// Fake xhr
				jqXHR = {
					readyState: 0,
	
					// Builds headers hashtable if needed
					getResponseHeader: function( key ) {
						var match;
						if ( completed ) {
							if ( !responseHeaders ) {
								responseHeaders = {};
								while ( ( match = rheaders.exec( responseHeadersString ) ) ) {
									responseHeaders[ match[ 1 ].toLowerCase() ] = match[ 2 ];
								}
							}
							match = responseHeaders[ key.toLowerCase() ];
						}
						return match == null ? null : match;
					},
	
					// Raw string
					getAllResponseHeaders: function() {
						return completed ? responseHeadersString : null;
					},
	
					// Caches the header
					setRequestHeader: function( name, value ) {
						if ( completed == null ) {
							name = requestHeadersNames[ name.toLowerCase() ] =
								requestHeadersNames[ name.toLowerCase() ] || name;
							requestHeaders[ name ] = value;
						}
						return this;
					},
	
					// Overrides response content-type header
					overrideMimeType: function( type ) {
						if ( completed == null ) {
							s.mimeType = type;
						}
						return this;
					},
	
					// Status-dependent callbacks
					statusCode: function( map ) {
						var code;
						if ( map ) {
							if ( completed ) {
	
								// Execute the appropriate callbacks
								jqXHR.always( map[ jqXHR.status ] );
							} else {
	
								// Lazy-add the new callbacks in a way that preserves old ones
								for ( code in map ) {
									statusCode[ code ] = [ statusCode[ code ], map[ code ] ];
								}
							}
						}
						return this;
					},
	
					// Cancel the request
					abort: function( statusText ) {
						var finalText = statusText || strAbort;
						if ( transport ) {
							transport.abort( finalText );
						}
						done( 0, finalText );
						return this;
					}
				};
	
			// Attach deferreds
			deferred.promise( jqXHR );
	
			// Add protocol if not provided (prefilters might expect it)
			// Handle falsy url in the settings object (#10093: consistency with old signature)
			// We also use the url parameter if available
			s.url = ( ( url || s.url || location.href ) + "" )
				.replace( rprotocol, location.protocol + "//" );
	
			// Alias method option to type as per ticket #12004
			s.type = options.method || options.type || s.method || s.type;
	
			// Extract dataTypes list
			s.dataTypes = ( s.dataType || "*" ).toLowerCase().match( rnothtmlwhite ) || [ "" ];
	
			// A cross-domain request is in order when the origin doesn't match the current origin.
			if ( s.crossDomain == null ) {
				urlAnchor = document.createElement( "a" );
	
				// Support: IE <=8 - 11, Edge 12 - 13
				// IE throws exception on accessing the href property if url is malformed,
				// e.g. http://example.com:80x/
				try {
					urlAnchor.href = s.url;
	
					// Support: IE <=8 - 11 only
					// Anchor's host property isn't correctly set when s.url is relative
					urlAnchor.href = urlAnchor.href;
					s.crossDomain = originAnchor.protocol + "//" + originAnchor.host !==
						urlAnchor.protocol + "//" + urlAnchor.host;
				} catch ( e ) {
	
					// If there is an error parsing the URL, assume it is crossDomain,
					// it can be rejected by the transport if it is invalid
					s.crossDomain = true;
				}
			}
	
			// Convert data if not already a string
			if ( s.data && s.processData && typeof s.data !== "string" ) {
				s.data = jQuery.param( s.data, s.traditional );
			}
	
			// Apply prefilters
			inspectPrefiltersOrTransports( prefilters, s, options, jqXHR );
	
			// If request was aborted inside a prefilter, stop there
			if ( completed ) {
				return jqXHR;
			}
	
			// We can fire global events as of now if asked to
			// Don't fire events if jQuery.event is undefined in an AMD-usage scenario (#15118)
			fireGlobals = jQuery.event && s.global;
	
			// Watch for a new set of requests
			if ( fireGlobals && jQuery.active++ === 0 ) {
				jQuery.event.trigger( "ajaxStart" );
			}
	
			// Uppercase the type
			s.type = s.type.toUpperCase();
	
			// Determine if request has content
			s.hasContent = !rnoContent.test( s.type );
	
			// Save the URL in case we're toying with the If-Modified-Since
			// and/or If-None-Match header later on
			// Remove hash to simplify url manipulation
			cacheURL = s.url.replace( rhash, "" );
	
			// More options handling for requests with no content
			if ( !s.hasContent ) {
	
				// Remember the hash so we can put it back
				uncached = s.url.slice( cacheURL.length );
	
				// If data is available, append data to url
				if ( s.data ) {
					cacheURL += ( rquery.test( cacheURL ) ? "&" : "?" ) + s.data;
	
					// #9682: remove data so that it's not used in an eventual retry
					delete s.data;
				}
	
				// Add or update anti-cache param if needed
				if ( s.cache === false ) {
					cacheURL = cacheURL.replace( rantiCache, "$1" );
					uncached = ( rquery.test( cacheURL ) ? "&" : "?" ) + "_=" + ( nonce++ ) + uncached;
				}
	
				// Put hash and anti-cache on the URL that will be requested (gh-1732)
				s.url = cacheURL + uncached;
	
			// Change '%20' to '+' if this is encoded form body content (gh-2658)
			} else if ( s.data && s.processData &&
				( s.contentType || "" ).indexOf( "application/x-www-form-urlencoded" ) === 0 ) {
				s.data = s.data.replace( r20, "+" );
			}
	
			// Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
			if ( s.ifModified ) {
				if ( jQuery.lastModified[ cacheURL ] ) {
					jqXHR.setRequestHeader( "If-Modified-Since", jQuery.lastModified[ cacheURL ] );
				}
				if ( jQuery.etag[ cacheURL ] ) {
					jqXHR.setRequestHeader( "If-None-Match", jQuery.etag[ cacheURL ] );
				}
			}
	
			// Set the correct header, if data is being sent
			if ( s.data && s.hasContent && s.contentType !== false || options.contentType ) {
				jqXHR.setRequestHeader( "Content-Type", s.contentType );
			}
	
			// Set the Accepts header for the server, depending on the dataType
			jqXHR.setRequestHeader(
				"Accept",
				s.dataTypes[ 0 ] && s.accepts[ s.dataTypes[ 0 ] ] ?
					s.accepts[ s.dataTypes[ 0 ] ] +
						( s.dataTypes[ 0 ] !== "*" ? ", " + allTypes + "; q=0.01" : "" ) :
					s.accepts[ "*" ]
			);
	
			// Check for headers option
			for ( i in s.headers ) {
				jqXHR.setRequestHeader( i, s.headers[ i ] );
			}
	
			// Allow custom headers/mimetypes and early abort
			if ( s.beforeSend &&
				( s.beforeSend.call( callbackContext, jqXHR, s ) === false || completed ) ) {
	
				// Abort if not done already and return
				return jqXHR.abort();
			}
	
			// Aborting is no longer a cancellation
			strAbort = "abort";
	
			// Install callbacks on deferreds
			completeDeferred.add( s.complete );
			jqXHR.done( s.success );
			jqXHR.fail( s.error );
	
			// Get transport
			transport = inspectPrefiltersOrTransports( transports, s, options, jqXHR );
	
			// If no transport, we auto-abort
			if ( !transport ) {
				done( -1, "No Transport" );
			} else {
				jqXHR.readyState = 1;
	
				// Send global event
				if ( fireGlobals ) {
					globalEventContext.trigger( "ajaxSend", [ jqXHR, s ] );
				}
	
				// If request was aborted inside ajaxSend, stop there
				if ( completed ) {
					return jqXHR;
				}
	
				// Timeout
				if ( s.async && s.timeout > 0 ) {
					timeoutTimer = window.setTimeout( function() {
						jqXHR.abort( "timeout" );
					}, s.timeout );
				}
	
				try {
					completed = false;
					transport.send( requestHeaders, done );
				} catch ( e ) {
	
					// Rethrow post-completion exceptions
					if ( completed ) {
						throw e;
					}
	
					// Propagate others as results
					done( -1, e );
				}
			}
	
			// Callback for when everything is done
			function done( status, nativeStatusText, responses, headers ) {
				var isSuccess, success, error, response, modified,
					statusText = nativeStatusText;
	
				// Ignore repeat invocations
				if ( completed ) {
					return;
				}
	
				completed = true;
	
				// Clear timeout if it exists
				if ( timeoutTimer ) {
					window.clearTimeout( timeoutTimer );
				}
	
				// Dereference transport for early garbage collection
				// (no matter how long the jqXHR object will be used)
				transport = undefined;
	
				// Cache response headers
				responseHeadersString = headers || "";
	
				// Set readyState
				jqXHR.readyState = status > 0 ? 4 : 0;
	
				// Determine if successful
				isSuccess = status >= 200 && status < 300 || status === 304;
	
				// Get response data
				if ( responses ) {
					response = ajaxHandleResponses( s, jqXHR, responses );
				}
	
				// Convert no matter what (that way responseXXX fields are always set)
				response = ajaxConvert( s, response, jqXHR, isSuccess );
	
				// If successful, handle type chaining
				if ( isSuccess ) {
	
					// Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
					if ( s.ifModified ) {
						modified = jqXHR.getResponseHeader( "Last-Modified" );
						if ( modified ) {
							jQuery.lastModified[ cacheURL ] = modified;
						}
						modified = jqXHR.getResponseHeader( "etag" );
						if ( modified ) {
							jQuery.etag[ cacheURL ] = modified;
						}
					}
	
					// if no content
					if ( status === 204 || s.type === "HEAD" ) {
						statusText = "nocontent";
	
					// if not modified
					} else if ( status === 304 ) {
						statusText = "notmodified";
	
					// If we have data, let's convert it
					} else {
						statusText = response.state;
						success = response.data;
						error = response.error;
						isSuccess = !error;
					}
				} else {
	
					// Extract error from statusText and normalize for non-aborts
					error = statusText;
					if ( status || !statusText ) {
						statusText = "error";
						if ( status < 0 ) {
							status = 0;
						}
					}
				}
	
				// Set data for the fake xhr object
				jqXHR.status = status;
				jqXHR.statusText = ( nativeStatusText || statusText ) + "";
	
				// Success/Error
				if ( isSuccess ) {
					deferred.resolveWith( callbackContext, [ success, statusText, jqXHR ] );
				} else {
					deferred.rejectWith( callbackContext, [ jqXHR, statusText, error ] );
				}
	
				// Status-dependent callbacks
				jqXHR.statusCode( statusCode );
				statusCode = undefined;
	
				if ( fireGlobals ) {
					globalEventContext.trigger( isSuccess ? "ajaxSuccess" : "ajaxError",
						[ jqXHR, s, isSuccess ? success : error ] );
				}
	
				// Complete
				completeDeferred.fireWith( callbackContext, [ jqXHR, statusText ] );
	
				if ( fireGlobals ) {
					globalEventContext.trigger( "ajaxComplete", [ jqXHR, s ] );
	
					// Handle the global AJAX counter
					if ( !( --jQuery.active ) ) {
						jQuery.event.trigger( "ajaxStop" );
					}
				}
			}
	
			return jqXHR;
		},
	
		getJSON: function( url, data, callback ) {
			return jQuery.get( url, data, callback, "json" );
		},
	
		getScript: function( url, callback ) {
			return jQuery.get( url, undefined, callback, "script" );
		}
	} );
	
	jQuery.each( [ "get", "post" ], function( i, method ) {
		jQuery[ method ] = function( url, data, callback, type ) {
	
			// Shift arguments if data argument was omitted
			if ( jQuery.isFunction( data ) ) {
				type = type || callback;
				callback = data;
				data = undefined;
			}
	
			// The url can be an options object (which then must have .url)
			return jQuery.ajax( jQuery.extend( {
				url: url,
				type: method,
				dataType: type,
				data: data,
				success: callback
			}, jQuery.isPlainObject( url ) && url ) );
		};
	} );
	
	
	jQuery._evalUrl = function( url ) {
		return jQuery.ajax( {
			url: url,
	
			// Make this explicit, since user can override this through ajaxSetup (#11264)
			type: "GET",
			dataType: "script",
			cache: true,
			async: false,
			global: false,
			"throws": true
		} );
	};
	
	
	jQuery.fn.extend( {
		wrapAll: function( html ) {
			var wrap;
	
			if ( this[ 0 ] ) {
				if ( jQuery.isFunction( html ) ) {
					html = html.call( this[ 0 ] );
				}
	
				// The elements to wrap the target around
				wrap = jQuery( html, this[ 0 ].ownerDocument ).eq( 0 ).clone( true );
	
				if ( this[ 0 ].parentNode ) {
					wrap.insertBefore( this[ 0 ] );
				}
	
				wrap.map( function() {
					var elem = this;
	
					while ( elem.firstElementChild ) {
						elem = elem.firstElementChild;
					}
	
					return elem;
				} ).append( this );
			}
	
			return this;
		},
	
		wrapInner: function( html ) {
			if ( jQuery.isFunction( html ) ) {
				return this.each( function( i ) {
					jQuery( this ).wrapInner( html.call( this, i ) );
				} );
			}
	
			return this.each( function() {
				var self = jQuery( this ),
					contents = self.contents();
	
				if ( contents.length ) {
					contents.wrapAll( html );
	
				} else {
					self.append( html );
				}
			} );
		},
	
		wrap: function( html ) {
			var isFunction = jQuery.isFunction( html );
	
			return this.each( function( i ) {
				jQuery( this ).wrapAll( isFunction ? html.call( this, i ) : html );
			} );
		},
	
		unwrap: function( selector ) {
			this.parent( selector ).not( "body" ).each( function() {
				jQuery( this ).replaceWith( this.childNodes );
			} );
			return this;
		}
	} );
	
	
	jQuery.expr.pseudos.hidden = function( elem ) {
		return !jQuery.expr.pseudos.visible( elem );
	};
	jQuery.expr.pseudos.visible = function( elem ) {
		return !!( elem.offsetWidth || elem.offsetHeight || elem.getClientRects().length );
	};
	
	
	
	
	jQuery.ajaxSettings.xhr = function() {
		try {
			return new window.XMLHttpRequest();
		} catch ( e ) {}
	};
	
	var xhrSuccessStatus = {
	
			// File protocol always yields status code 0, assume 200
			0: 200,
	
			// Support: IE <=9 only
			// #1450: sometimes IE returns 1223 when it should be 204
			1223: 204
		},
		xhrSupported = jQuery.ajaxSettings.xhr();
	
	support.cors = !!xhrSupported && ( "withCredentials" in xhrSupported );
	support.ajax = xhrSupported = !!xhrSupported;
	
	jQuery.ajaxTransport( function( options ) {
		var callback, errorCallback;
	
		// Cross domain only allowed if supported through XMLHttpRequest
		if ( support.cors || xhrSupported && !options.crossDomain ) {
			return {
				send: function( headers, complete ) {
					var i,
						xhr = options.xhr();
	
					xhr.open(
						options.type,
						options.url,
						options.async,
						options.username,
						options.password
					);
	
					// Apply custom fields if provided
					if ( options.xhrFields ) {
						for ( i in options.xhrFields ) {
							xhr[ i ] = options.xhrFields[ i ];
						}
					}
	
					// Override mime type if needed
					if ( options.mimeType && xhr.overrideMimeType ) {
						xhr.overrideMimeType( options.mimeType );
					}
	
					// X-Requested-With header
					// For cross-domain requests, seeing as conditions for a preflight are
					// akin to a jigsaw puzzle, we simply never set it to be sure.
					// (it can always be set on a per-request basis or even using ajaxSetup)
					// For same-domain requests, won't change header if already provided.
					if ( !options.crossDomain && !headers[ "X-Requested-With" ] ) {
						headers[ "X-Requested-With" ] = "XMLHttpRequest";
					}
	
					// Set headers
					for ( i in headers ) {
						xhr.setRequestHeader( i, headers[ i ] );
					}
	
					// Callback
					callback = function( type ) {
						return function() {
							if ( callback ) {
								callback = errorCallback = xhr.onload =
									xhr.onerror = xhr.onabort = xhr.onreadystatechange = null;
	
								if ( type === "abort" ) {
									xhr.abort();
								} else if ( type === "error" ) {
	
									// Support: IE <=9 only
									// On a manual native abort, IE9 throws
									// errors on any property access that is not readyState
									if ( typeof xhr.status !== "number" ) {
										complete( 0, "error" );
									} else {
										complete(
	
											// File: protocol always yields status 0; see #8605, #14207
											xhr.status,
											xhr.statusText
										);
									}
								} else {
									complete(
										xhrSuccessStatus[ xhr.status ] || xhr.status,
										xhr.statusText,
	
										// Support: IE <=9 only
										// IE9 has no XHR2 but throws on binary (trac-11426)
										// For XHR2 non-text, let the caller handle it (gh-2498)
										( xhr.responseType || "text" ) !== "text"  ||
										typeof xhr.responseText !== "string" ?
											{ binary: xhr.response } :
											{ text: xhr.responseText },
										xhr.getAllResponseHeaders()
									);
								}
							}
						};
					};
	
					// Listen to events
					xhr.onload = callback();
					errorCallback = xhr.onerror = callback( "error" );
	
					// Support: IE 9 only
					// Use onreadystatechange to replace onabort
					// to handle uncaught aborts
					if ( xhr.onabort !== undefined ) {
						xhr.onabort = errorCallback;
					} else {
						xhr.onreadystatechange = function() {
	
							// Check readyState before timeout as it changes
							if ( xhr.readyState === 4 ) {
	
								// Allow onerror to be called first,
								// but that will not handle a native abort
								// Also, save errorCallback to a variable
								// as xhr.onerror cannot be accessed
								window.setTimeout( function() {
									if ( callback ) {
										errorCallback();
									}
								} );
							}
						};
					}
	
					// Create the abort callback
					callback = callback( "abort" );
	
					try {
	
						// Do send the request (this may raise an exception)
						xhr.send( options.hasContent && options.data || null );
					} catch ( e ) {
	
						// #14683: Only rethrow if this hasn't been notified as an error yet
						if ( callback ) {
							throw e;
						}
					}
				},
	
				abort: function() {
					if ( callback ) {
						callback();
					}
				}
			};
		}
	} );
	
	
	
	
	// Prevent auto-execution of scripts when no explicit dataType was provided (See gh-2432)
	jQuery.ajaxPrefilter( function( s ) {
		if ( s.crossDomain ) {
			s.contents.script = false;
		}
	} );
	
	// Install script dataType
	jQuery.ajaxSetup( {
		accepts: {
			script: "text/javascript, application/javascript, " +
				"application/ecmascript, application/x-ecmascript"
		},
		contents: {
			script: /\b(?:java|ecma)script\b/
		},
		converters: {
			"text script": function( text ) {
				jQuery.globalEval( text );
				return text;
			}
		}
	} );
	
	// Handle cache's special case and crossDomain
	jQuery.ajaxPrefilter( "script", function( s ) {
		if ( s.cache === undefined ) {
			s.cache = false;
		}
		if ( s.crossDomain ) {
			s.type = "GET";
		}
	} );
	
	// Bind script tag hack transport
	jQuery.ajaxTransport( "script", function( s ) {
	
		// This transport only deals with cross domain requests
		if ( s.crossDomain ) {
			var script, callback;
			return {
				send: function( _, complete ) {
					script = jQuery( "<script>" ).prop( {
						charset: s.scriptCharset,
						src: s.url
					} ).on(
						"load error",
						callback = function( evt ) {
							script.remove();
							callback = null;
							if ( evt ) {
								complete( evt.type === "error" ? 404 : 200, evt.type );
							}
						}
					);
	
					// Use native DOM manipulation to avoid our domManip AJAX trickery
					document.head.appendChild( script[ 0 ] );
				},
				abort: function() {
					if ( callback ) {
						callback();
					}
				}
			};
		}
	} );
	
	
	
	
	var oldCallbacks = [],
		rjsonp = /(=)\?(?=&|$)|\?\?/;
	
	// Default jsonp settings
	jQuery.ajaxSetup( {
		jsonp: "callback",
		jsonpCallback: function() {
			var callback = oldCallbacks.pop() || ( jQuery.expando + "_" + ( nonce++ ) );
			this[ callback ] = true;
			return callback;
		}
	} );
	
	// Detect, normalize options and install callbacks for jsonp requests
	jQuery.ajaxPrefilter( "json jsonp", function( s, originalSettings, jqXHR ) {
	
		var callbackName, overwritten, responseContainer,
			jsonProp = s.jsonp !== false && ( rjsonp.test( s.url ) ?
				"url" :
				typeof s.data === "string" &&
					( s.contentType || "" )
						.indexOf( "application/x-www-form-urlencoded" ) === 0 &&
					rjsonp.test( s.data ) && "data"
			);
	
		// Handle iff the expected data type is "jsonp" or we have a parameter to set
		if ( jsonProp || s.dataTypes[ 0 ] === "jsonp" ) {
	
			// Get callback name, remembering preexisting value associated with it
			callbackName = s.jsonpCallback = jQuery.isFunction( s.jsonpCallback ) ?
				s.jsonpCallback() :
				s.jsonpCallback;
	
			// Insert callback into url or form data
			if ( jsonProp ) {
				s[ jsonProp ] = s[ jsonProp ].replace( rjsonp, "$1" + callbackName );
			} else if ( s.jsonp !== false ) {
				s.url += ( rquery.test( s.url ) ? "&" : "?" ) + s.jsonp + "=" + callbackName;
			}
	
			// Use data converter to retrieve json after script execution
			s.converters[ "script json" ] = function() {
				if ( !responseContainer ) {
					jQuery.error( callbackName + " was not called" );
				}
				return responseContainer[ 0 ];
			};
	
			// Force json dataType
			s.dataTypes[ 0 ] = "json";
	
			// Install callback
			overwritten = window[ callbackName ];
			window[ callbackName ] = function() {
				responseContainer = arguments;
			};
	
			// Clean-up function (fires after converters)
			jqXHR.always( function() {
	
				// If previous value didn't exist - remove it
				if ( overwritten === undefined ) {
					jQuery( window ).removeProp( callbackName );
	
				// Otherwise restore preexisting value
				} else {
					window[ callbackName ] = overwritten;
				}
	
				// Save back as free
				if ( s[ callbackName ] ) {
	
					// Make sure that re-using the options doesn't screw things around
					s.jsonpCallback = originalSettings.jsonpCallback;
	
					// Save the callback name for future use
					oldCallbacks.push( callbackName );
				}
	
				// Call if it was a function and we have a response
				if ( responseContainer && jQuery.isFunction( overwritten ) ) {
					overwritten( responseContainer[ 0 ] );
				}
	
				responseContainer = overwritten = undefined;
			} );
	
			// Delegate to script
			return "script";
		}
	} );
	
	
	
	
	// Support: Safari 8 only
	// In Safari 8 documents created via document.implementation.createHTMLDocument
	// collapse sibling forms: the second one becomes a child of the first one.
	// Because of that, this security measure has to be disabled in Safari 8.
	// https://bugs.webkit.org/show_bug.cgi?id=137337
	support.createHTMLDocument = ( function() {
		var body = document.implementation.createHTMLDocument( "" ).body;
		body.innerHTML = "<form></form><form></form>";
		return body.childNodes.length === 2;
	} )();
	
	
	// Argument "data" should be string of html
	// context (optional): If specified, the fragment will be created in this context,
	// defaults to document
	// keepScripts (optional): If true, will include scripts passed in the html string
	jQuery.parseHTML = function( data, context, keepScripts ) {
		if ( typeof data !== "string" ) {
			return [];
		}
		if ( typeof context === "boolean" ) {
			keepScripts = context;
			context = false;
		}
	
		var base, parsed, scripts;
	
		if ( !context ) {
	
			// Stop scripts or inline event handlers from being executed immediately
			// by using document.implementation
			if ( support.createHTMLDocument ) {
				context = document.implementation.createHTMLDocument( "" );
	
				// Set the base href for the created document
				// so any parsed elements with URLs
				// are based on the document's URL (gh-2965)
				base = context.createElement( "base" );
				base.href = document.location.href;
				context.head.appendChild( base );
			} else {
				context = document;
			}
		}
	
		parsed = rsingleTag.exec( data );
		scripts = !keepScripts && [];
	
		// Single tag
		if ( parsed ) {
			return [ context.createElement( parsed[ 1 ] ) ];
		}
	
		parsed = buildFragment( [ data ], context, scripts );
	
		if ( scripts && scripts.length ) {
			jQuery( scripts ).remove();
		}
	
		return jQuery.merge( [], parsed.childNodes );
	};
	
	
	/**
	 * Load a url into a page
	 */
	jQuery.fn.load = function( url, params, callback ) {
		var selector, type, response,
			self = this,
			off = url.indexOf( " " );
	
		if ( off > -1 ) {
			selector = stripAndCollapse( url.slice( off ) );
			url = url.slice( 0, off );
		}
	
		// If it's a function
		if ( jQuery.isFunction( params ) ) {
	
			// We assume that it's the callback
			callback = params;
			params = undefined;
	
		// Otherwise, build a param string
		} else if ( params && typeof params === "object" ) {
			type = "POST";
		}
	
		// If we have elements to modify, make the request
		if ( self.length > 0 ) {
			jQuery.ajax( {
				url: url,
	
				// If "type" variable is undefined, then "GET" method will be used.
				// Make value of this field explicit since
				// user can override it through ajaxSetup method
				type: type || "GET",
				dataType: "html",
				data: params
			} ).done( function( responseText ) {
	
				// Save response for use in complete callback
				response = arguments;
	
				self.html( selector ?
	
					// If a selector was specified, locate the right elements in a dummy div
					// Exclude scripts to avoid IE 'Permission Denied' errors
					jQuery( "<div>" ).append( jQuery.parseHTML( responseText ) ).find( selector ) :
	
					// Otherwise use the full result
					responseText );
	
			// If the request succeeds, this function gets "data", "status", "jqXHR"
			// but they are ignored because response was set above.
			// If it fails, this function gets "jqXHR", "status", "error"
			} ).always( callback && function( jqXHR, status ) {
				self.each( function() {
					callback.apply( this, response || [ jqXHR.responseText, status, jqXHR ] );
				} );
			} );
		}
	
		return this;
	};
	
	
	
	
	// Attach a bunch of functions for handling common AJAX events
	jQuery.each( [
		"ajaxStart",
		"ajaxStop",
		"ajaxComplete",
		"ajaxError",
		"ajaxSuccess",
		"ajaxSend"
	], function( i, type ) {
		jQuery.fn[ type ] = function( fn ) {
			return this.on( type, fn );
		};
	} );
	
	
	
	
	jQuery.expr.pseudos.animated = function( elem ) {
		return jQuery.grep( jQuery.timers, function( fn ) {
			return elem === fn.elem;
		} ).length;
	};
	
	
	
	
	jQuery.offset = {
		setOffset: function( elem, options, i ) {
			var curPosition, curLeft, curCSSTop, curTop, curOffset, curCSSLeft, calculatePosition,
				position = jQuery.css( elem, "position" ),
				curElem = jQuery( elem ),
				props = {};
	
			// Set position first, in-case top/left are set even on static elem
			if ( position === "static" ) {
				elem.style.position = "relative";
			}
	
			curOffset = curElem.offset();
			curCSSTop = jQuery.css( elem, "top" );
			curCSSLeft = jQuery.css( elem, "left" );
			calculatePosition = ( position === "absolute" || position === "fixed" ) &&
				( curCSSTop + curCSSLeft ).indexOf( "auto" ) > -1;
	
			// Need to be able to calculate position if either
			// top or left is auto and position is either absolute or fixed
			if ( calculatePosition ) {
				curPosition = curElem.position();
				curTop = curPosition.top;
				curLeft = curPosition.left;
	
			} else {
				curTop = parseFloat( curCSSTop ) || 0;
				curLeft = parseFloat( curCSSLeft ) || 0;
			}
	
			if ( jQuery.isFunction( options ) ) {
	
				// Use jQuery.extend here to allow modification of coordinates argument (gh-1848)
				options = options.call( elem, i, jQuery.extend( {}, curOffset ) );
			}
	
			if ( options.top != null ) {
				props.top = ( options.top - curOffset.top ) + curTop;
			}
			if ( options.left != null ) {
				props.left = ( options.left - curOffset.left ) + curLeft;
			}
	
			if ( "using" in options ) {
				options.using.call( elem, props );
	
			} else {
				curElem.css( props );
			}
		}
	};
	
	jQuery.fn.extend( {
		offset: function( options ) {
	
			// Preserve chaining for setter
			if ( arguments.length ) {
				return options === undefined ?
					this :
					this.each( function( i ) {
						jQuery.offset.setOffset( this, options, i );
					} );
			}
	
			var doc, docElem, rect, win,
				elem = this[ 0 ];
	
			if ( !elem ) {
				return;
			}
	
			// Return zeros for disconnected and hidden (display: none) elements (gh-2310)
			// Support: IE <=11 only
			// Running getBoundingClientRect on a
			// disconnected node in IE throws an error
			if ( !elem.getClientRects().length ) {
				return { top: 0, left: 0 };
			}
	
			rect = elem.getBoundingClientRect();
	
			doc = elem.ownerDocument;
			docElem = doc.documentElement;
			win = doc.defaultView;
	
			return {
				top: rect.top + win.pageYOffset - docElem.clientTop,
				left: rect.left + win.pageXOffset - docElem.clientLeft
			};
		},
	
		position: function() {
			if ( !this[ 0 ] ) {
				return;
			}
	
			var offsetParent, offset,
				elem = this[ 0 ],
				parentOffset = { top: 0, left: 0 };
	
			// Fixed elements are offset from window (parentOffset = {top:0, left: 0},
			// because it is its only offset parent
			if ( jQuery.css( elem, "position" ) === "fixed" ) {
	
				// Assume getBoundingClientRect is there when computed position is fixed
				offset = elem.getBoundingClientRect();
	
			} else {
	
				// Get *real* offsetParent
				offsetParent = this.offsetParent();
	
				// Get correct offsets
				offset = this.offset();
				if ( !nodeName( offsetParent[ 0 ], "html" ) ) {
					parentOffset = offsetParent.offset();
				}
	
				// Add offsetParent borders
				parentOffset = {
					top: parentOffset.top + jQuery.css( offsetParent[ 0 ], "borderTopWidth", true ),
					left: parentOffset.left + jQuery.css( offsetParent[ 0 ], "borderLeftWidth", true )
				};
			}
	
			// Subtract parent offsets and element margins
			return {
				top: offset.top - parentOffset.top - jQuery.css( elem, "marginTop", true ),
				left: offset.left - parentOffset.left - jQuery.css( elem, "marginLeft", true )
			};
		},
	
		// This method will return documentElement in the following cases:
		// 1) For the element inside the iframe without offsetParent, this method will return
		//    documentElement of the parent window
		// 2) For the hidden or detached element
		// 3) For body or html element, i.e. in case of the html node - it will return itself
		//
		// but those exceptions were never presented as a real life use-cases
		// and might be considered as more preferable results.
		//
		// This logic, however, is not guaranteed and can change at any point in the future
		offsetParent: function() {
			return this.map( function() {
				var offsetParent = this.offsetParent;
	
				while ( offsetParent && jQuery.css( offsetParent, "position" ) === "static" ) {
					offsetParent = offsetParent.offsetParent;
				}
	
				return offsetParent || documentElement;
			} );
		}
	} );
	
	// Create scrollLeft and scrollTop methods
	jQuery.each( { scrollLeft: "pageXOffset", scrollTop: "pageYOffset" }, function( method, prop ) {
		var top = "pageYOffset" === prop;
	
		jQuery.fn[ method ] = function( val ) {
			return access( this, function( elem, method, val ) {
	
				// Coalesce documents and windows
				var win;
				if ( jQuery.isWindow( elem ) ) {
					win = elem;
				} else if ( elem.nodeType === 9 ) {
					win = elem.defaultView;
				}
	
				if ( val === undefined ) {
					return win ? win[ prop ] : elem[ method ];
				}
	
				if ( win ) {
					win.scrollTo(
						!top ? val : win.pageXOffset,
						top ? val : win.pageYOffset
					);
	
				} else {
					elem[ method ] = val;
				}
			}, method, val, arguments.length );
		};
	} );
	
	// Support: Safari <=7 - 9.1, Chrome <=37 - 49
	// Add the top/left cssHooks using jQuery.fn.position
	// Webkit bug: https://bugs.webkit.org/show_bug.cgi?id=29084
	// Blink bug: https://bugs.chromium.org/p/chromium/issues/detail?id=589347
	// getComputedStyle returns percent when specified for top/left/bottom/right;
	// rather than make the css module depend on the offset module, just check for it here
	jQuery.each( [ "top", "left" ], function( i, prop ) {
		jQuery.cssHooks[ prop ] = addGetHookIf( support.pixelPosition,
			function( elem, computed ) {
				if ( computed ) {
					computed = curCSS( elem, prop );
	
					// If curCSS returns percentage, fallback to offset
					return rnumnonpx.test( computed ) ?
						jQuery( elem ).position()[ prop ] + "px" :
						computed;
				}
			}
		);
	} );
	
	
	// Create innerHeight, innerWidth, height, width, outerHeight and outerWidth methods
	jQuery.each( { Height: "height", Width: "width" }, function( name, type ) {
		jQuery.each( { padding: "inner" + name, content: type, "": "outer" + name },
			function( defaultExtra, funcName ) {
	
			// Margin is only for outerHeight, outerWidth
			jQuery.fn[ funcName ] = function( margin, value ) {
				var chainable = arguments.length && ( defaultExtra || typeof margin !== "boolean" ),
					extra = defaultExtra || ( margin === true || value === true ? "margin" : "border" );
	
				return access( this, function( elem, type, value ) {
					var doc;
	
					if ( jQuery.isWindow( elem ) ) {
	
						// $( window ).outerWidth/Height return w/h including scrollbars (gh-1729)
						return funcName.indexOf( "outer" ) === 0 ?
							elem[ "inner" + name ] :
							elem.document.documentElement[ "client" + name ];
					}
	
					// Get document width or height
					if ( elem.nodeType === 9 ) {
						doc = elem.documentElement;
	
						// Either scroll[Width/Height] or offset[Width/Height] or client[Width/Height],
						// whichever is greatest
						return Math.max(
							elem.body[ "scroll" + name ], doc[ "scroll" + name ],
							elem.body[ "offset" + name ], doc[ "offset" + name ],
							doc[ "client" + name ]
						);
					}
	
					return value === undefined ?
	
						// Get width or height on the element, requesting but not forcing parseFloat
						jQuery.css( elem, type, extra ) :
	
						// Set width or height on the element
						jQuery.style( elem, type, value, extra );
				}, type, chainable ? margin : undefined, chainable );
			};
		} );
	} );
	
	
	jQuery.fn.extend( {
	
		bind: function( types, data, fn ) {
			return this.on( types, null, data, fn );
		},
		unbind: function( types, fn ) {
			return this.off( types, null, fn );
		},
	
		delegate: function( selector, types, data, fn ) {
			return this.on( types, selector, data, fn );
		},
		undelegate: function( selector, types, fn ) {
	
			// ( namespace ) or ( selector, types [, fn] )
			return arguments.length === 1 ?
				this.off( selector, "**" ) :
				this.off( types, selector || "**", fn );
		}
	} );
	
	jQuery.holdReady = function( hold ) {
		if ( hold ) {
			jQuery.readyWait++;
		} else {
			jQuery.ready( true );
		}
	};
	jQuery.isArray = Array.isArray;
	jQuery.parseJSON = JSON.parse;
	jQuery.nodeName = nodeName;
	
	
	
	
	// Register as a named AMD module, since jQuery can be concatenated with other
	// files that may use define, but not via a proper concatenation script that
	// understands anonymous AMD modules. A named AMD is safest and most robust
	// way to register. Lowercase jquery is used because AMD module names are
	// derived from file names, and jQuery is normally delivered in a lowercase
	// file name. Do this after creating the global so that if an AMD module wants
	// to call noConflict to hide this version of jQuery, it will work.
	
	// Note that for maximum portability, libraries that are not jQuery should
	// declare themselves as anonymous modules, and avoid setting a global if an
	// AMD loader is present. jQuery is a special case. For more information, see
	// https://github.com/jrburke/requirejs/wiki/Updating-existing-libraries#wiki-anon
	
	if ( true ) {
		!(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_RESULT__ = function() {
			return jQuery;
		}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	}
	
	
	
	
	var
	
		// Map over jQuery in case of overwrite
		_jQuery = window.jQuery,
	
		// Map over the $ in case of overwrite
		_$ = window.$;
	
	jQuery.noConflict = function( deep ) {
		if ( window.$ === jQuery ) {
			window.$ = _$;
		}
	
		if ( deep && window.jQuery === jQuery ) {
			window.jQuery = _jQuery;
		}
	
		return jQuery;
	};
	
	// Expose jQuery and $ identifiers, even in AMD
	// (#7102#comment:10, https://github.com/jquery/jquery/pull/557)
	// and CommonJS for browser emulators (#13566)
	if ( !noGlobal ) {
		window.jQuery = window.$ = jQuery;
	}
	
	
	
	
	return jQuery;
	} );


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(jQuery) {/*!
	 * Bootstrap v3.3.7 (http://getbootstrap.com)
	 * Copyright 2011-2016 Twitter, Inc.
	 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
	 */
	
	/*!
	 * Generated using the Bootstrap Customizer (http://getbootstrap.com/customize/?id=557e8f07d5733edcc93dd35f3cf5cac9)
	 * Config saved to config.json and https://gist.github.com/557e8f07d5733edcc93dd35f3cf5cac9
	 */
	if (typeof jQuery === 'undefined') {
	  throw new Error('Bootstrap\'s JavaScript requires jQuery')
	}
	+function ($) {
	  'use strict';
	  var version = $.fn.jquery.split(' ')[0].split('.')
	  if ((version[0] < 2 && version[1] < 9) || (version[0] == 1 && version[1] == 9 && version[2] < 1) || (version[0] > 3)) {
	    throw new Error('Bootstrap\'s JavaScript requires jQuery version 1.9.1 or higher, but lower than version 4')
	  }
	}(jQuery);
	
	/* ========================================================================
	 * Bootstrap: modal.js v3.3.7
	 * http://getbootstrap.com/javascript/#modals
	 * ========================================================================
	 * Copyright 2011-2016 Twitter, Inc.
	 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
	 * ======================================================================== */
	
	
	+function ($) {
	  'use strict';
	
	  // MODAL CLASS DEFINITION
	  // ======================
	
	  var Modal = function (element, options) {
	    this.options             = options
	    this.$body               = $(document.body)
	    this.$element            = $(element)
	    this.$dialog             = this.$element.find('.modal-dialog')
	    this.$backdrop           = null
	    this.isShown             = null
	    this.originalBodyPad     = null
	    this.scrollbarWidth      = 0
	    this.ignoreBackdropClick = false
	
	    if (this.options.remote) {
	      this.$element
	        .find('.modal-content')
	        .load(this.options.remote, $.proxy(function () {
	          this.$element.trigger('loaded.bs.modal')
	        }, this))
	    }
	  }
	
	  Modal.VERSION  = '3.3.7'
	
	  Modal.TRANSITION_DURATION = 300
	  Modal.BACKDROP_TRANSITION_DURATION = 150
	
	  Modal.DEFAULTS = {
	    backdrop: true,
	    keyboard: true,
	    show: true
	  }
	
	  Modal.prototype.toggle = function (_relatedTarget) {
	    return this.isShown ? this.hide() : this.show(_relatedTarget)
	  }
	
	  Modal.prototype.show = function (_relatedTarget) {
	    var that = this
	    var e    = $.Event('show.bs.modal', { relatedTarget: _relatedTarget })
	
	    this.$element.trigger(e)
	
	    if (this.isShown || e.isDefaultPrevented()) return
	
	    this.isShown = true
	
	    this.checkScrollbar()
	    this.setScrollbar()
	    this.$body.addClass('modal-open')
	
	    this.escape()
	    this.resize()
	
	    this.$element.on('click.dismiss.bs.modal', '[data-dismiss="modal"]', $.proxy(this.hide, this))
	
	    this.$dialog.on('mousedown.dismiss.bs.modal', function () {
	      that.$element.one('mouseup.dismiss.bs.modal', function (e) {
	        if ($(e.target).is(that.$element)) that.ignoreBackdropClick = true
	      })
	    })
	
	    this.backdrop(function () {
	      var transition = $.support.transition && that.$element.hasClass('fade')
	
	      if (!that.$element.parent().length) {
	        that.$element.appendTo(that.$body) // don't move modals dom position
	      }
	
	      that.$element
	        .show()
	        .scrollTop(0)
	
	      that.adjustDialog()
	
	      if (transition) {
	        that.$element[0].offsetWidth // force reflow
	      }
	
	      that.$element.addClass('in')
	
	      that.enforceFocus()
	
	      var e = $.Event('shown.bs.modal', { relatedTarget: _relatedTarget })
	
	      transition ?
	        that.$dialog // wait for modal to slide in
	          .one('bsTransitionEnd', function () {
	            that.$element.trigger('focus').trigger(e)
	          })
	          .emulateTransitionEnd(Modal.TRANSITION_DURATION) :
	        that.$element.trigger('focus').trigger(e)
	    })
	  }
	
	  Modal.prototype.hide = function (e) {
	    if (e) e.preventDefault()
	
	    e = $.Event('hide.bs.modal')
	
	    this.$element.trigger(e)
	
	    if (!this.isShown || e.isDefaultPrevented()) return
	
	    this.isShown = false
	
	    this.escape()
	    this.resize()
	
	    $(document).off('focusin.bs.modal')
	
	    this.$element
	      .removeClass('in')
	      .off('click.dismiss.bs.modal')
	      .off('mouseup.dismiss.bs.modal')
	
	    this.$dialog.off('mousedown.dismiss.bs.modal')
	
	    $.support.transition && this.$element.hasClass('fade') ?
	      this.$element
	        .one('bsTransitionEnd', $.proxy(this.hideModal, this))
	        .emulateTransitionEnd(Modal.TRANSITION_DURATION) :
	      this.hideModal()
	  }
	
	  Modal.prototype.enforceFocus = function () {
	    $(document)
	      .off('focusin.bs.modal') // guard against infinite focus loop
	      .on('focusin.bs.modal', $.proxy(function (e) {
	        if (document !== e.target &&
	            this.$element[0] !== e.target &&
	            !this.$element.has(e.target).length) {
	          this.$element.trigger('focus')
	        }
	      }, this))
	  }
	
	  Modal.prototype.escape = function () {
	    if (this.isShown && this.options.keyboard) {
	      this.$element.on('keydown.dismiss.bs.modal', $.proxy(function (e) {
	        e.which == 27 && this.hide()
	      }, this))
	    } else if (!this.isShown) {
	      this.$element.off('keydown.dismiss.bs.modal')
	    }
	  }
	
	  Modal.prototype.resize = function () {
	    if (this.isShown) {
	      $(window).on('resize.bs.modal', $.proxy(this.handleUpdate, this))
	    } else {
	      $(window).off('resize.bs.modal')
	    }
	  }
	
	  Modal.prototype.hideModal = function () {
	    var that = this
	    this.$element.hide()
	    this.backdrop(function () {
	      that.$body.removeClass('modal-open')
	      that.resetAdjustments()
	      that.resetScrollbar()
	      that.$element.trigger('hidden.bs.modal')
	    })
	  }
	
	  Modal.prototype.removeBackdrop = function () {
	    this.$backdrop && this.$backdrop.remove()
	    this.$backdrop = null
	  }
	
	  Modal.prototype.backdrop = function (callback) {
	    var that = this
	    var animate = this.$element.hasClass('fade') ? 'fade' : ''
	
	    if (this.isShown && this.options.backdrop) {
	      var doAnimate = $.support.transition && animate
	
	      this.$backdrop = $(document.createElement('div'))
	        .addClass('modal-backdrop ' + animate)
	        .appendTo(this.$body)
	
	      this.$element.on('click.dismiss.bs.modal', $.proxy(function (e) {
	        if (this.ignoreBackdropClick) {
	          this.ignoreBackdropClick = false
	          return
	        }
	        if (e.target !== e.currentTarget) return
	        this.options.backdrop == 'static'
	          ? this.$element[0].focus()
	          : this.hide()
	      }, this))
	
	      if (doAnimate) this.$backdrop[0].offsetWidth // force reflow
	
	      this.$backdrop.addClass('in')
	
	      if (!callback) return
	
	      doAnimate ?
	        this.$backdrop
	          .one('bsTransitionEnd', callback)
	          .emulateTransitionEnd(Modal.BACKDROP_TRANSITION_DURATION) :
	        callback()
	
	    } else if (!this.isShown && this.$backdrop) {
	      this.$backdrop.removeClass('in')
	
	      var callbackRemove = function () {
	        that.removeBackdrop()
	        callback && callback()
	      }
	      $.support.transition && this.$element.hasClass('fade') ?
	        this.$backdrop
	          .one('bsTransitionEnd', callbackRemove)
	          .emulateTransitionEnd(Modal.BACKDROP_TRANSITION_DURATION) :
	        callbackRemove()
	
	    } else if (callback) {
	      callback()
	    }
	  }
	
	  // these following methods are used to handle overflowing modals
	
	  Modal.prototype.handleUpdate = function () {
	    this.adjustDialog()
	  }
	
	  Modal.prototype.adjustDialog = function () {
	    var modalIsOverflowing = this.$element[0].scrollHeight > document.documentElement.clientHeight
	
	    this.$element.css({
	      paddingLeft:  !this.bodyIsOverflowing && modalIsOverflowing ? this.scrollbarWidth : '',
	      paddingRight: this.bodyIsOverflowing && !modalIsOverflowing ? this.scrollbarWidth : ''
	    })
	  }
	
	  Modal.prototype.resetAdjustments = function () {
	    this.$element.css({
	      paddingLeft: '',
	      paddingRight: ''
	    })
	  }
	
	  Modal.prototype.checkScrollbar = function () {
	    var fullWindowWidth = window.innerWidth
	    if (!fullWindowWidth) { // workaround for missing window.innerWidth in IE8
	      var documentElementRect = document.documentElement.getBoundingClientRect()
	      fullWindowWidth = documentElementRect.right - Math.abs(documentElementRect.left)
	    }
	    this.bodyIsOverflowing = document.body.clientWidth < fullWindowWidth
	    this.scrollbarWidth = this.measureScrollbar()
	  }
	
	  Modal.prototype.setScrollbar = function () {
	    var bodyPad = parseInt((this.$body.css('padding-right') || 0), 10)
	    this.originalBodyPad = document.body.style.paddingRight || ''
	    if (this.bodyIsOverflowing) this.$body.css('padding-right', bodyPad + this.scrollbarWidth)
	  }
	
	  Modal.prototype.resetScrollbar = function () {
	    this.$body.css('padding-right', this.originalBodyPad)
	  }
	
	  Modal.prototype.measureScrollbar = function () { // thx walsh
	    var scrollDiv = document.createElement('div')
	    scrollDiv.className = 'modal-scrollbar-measure'
	    this.$body.append(scrollDiv)
	    var scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth
	    this.$body[0].removeChild(scrollDiv)
	    return scrollbarWidth
	  }
	
	
	  // MODAL PLUGIN DEFINITION
	  // =======================
	
	  function Plugin(option, _relatedTarget) {
	    return this.each(function () {
	      var $this   = $(this)
	      var data    = $this.data('bs.modal')
	      var options = $.extend({}, Modal.DEFAULTS, $this.data(), typeof option == 'object' && option)
	
	      if (!data) $this.data('bs.modal', (data = new Modal(this, options)))
	      if (typeof option == 'string') data[option](_relatedTarget)
	      else if (options.show) data.show(_relatedTarget)
	    })
	  }
	
	  var old = $.fn.modal
	
	  $.fn.modal             = Plugin
	  $.fn.modal.Constructor = Modal
	
	
	  // MODAL NO CONFLICT
	  // =================
	
	  $.fn.modal.noConflict = function () {
	    $.fn.modal = old
	    return this
	  }
	
	
	  // MODAL DATA-API
	  // ==============
	
	  $(document).on('click.bs.modal.data-api', '[data-toggle="modal"]', function (e) {
	    var $this   = $(this)
	    var href    = $this.attr('href')
	    var $target = $($this.attr('data-target') || (href && href.replace(/.*(?=#[^\s]+$)/, ''))) // strip for ie7
	    var option  = $target.data('bs.modal') ? 'toggle' : $.extend({ remote: !/#/.test(href) && href }, $target.data(), $this.data())
	
	    if ($this.is('a')) e.preventDefault()
	
	    $target.one('show.bs.modal', function (showEvent) {
	      if (showEvent.isDefaultPrevented()) return // only register focus restorer if modal will actually get shown
	      $target.one('hidden.bs.modal', function () {
	        $this.is(':visible') && $this.trigger('focus')
	      })
	    })
	    Plugin.call($target, option, this)
	  })
	
	}(jQuery);
	
	/* ========================================================================
	 * Bootstrap: transition.js v3.3.7
	 * http://getbootstrap.com/javascript/#transitions
	 * ========================================================================
	 * Copyright 2011-2016 Twitter, Inc.
	 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
	 * ======================================================================== */
	
	
	+function ($) {
	  'use strict';
	
	  // CSS TRANSITION SUPPORT (Shoutout: http://www.modernizr.com/)
	  // ============================================================
	
	  function transitionEnd() {
	    var el = document.createElement('bootstrap')
	
	    var transEndEventNames = {
	      WebkitTransition : 'webkitTransitionEnd',
	      MozTransition    : 'transitionend',
	      OTransition      : 'oTransitionEnd otransitionend',
	      transition       : 'transitionend'
	    }
	
	    for (var name in transEndEventNames) {
	      if (el.style[name] !== undefined) {
	        return { end: transEndEventNames[name] }
	      }
	    }
	
	    return false // explicit for ie8 (  ._.)
	  }
	
	  // http://blog.alexmaccaw.com/css-transitions
	  $.fn.emulateTransitionEnd = function (duration) {
	    var called = false
	    var $el = this
	    $(this).one('bsTransitionEnd', function () { called = true })
	    var callback = function () { if (!called) $($el).trigger($.support.transition.end) }
	    setTimeout(callback, duration)
	    return this
	  }
	
	  $(function () {
	    $.support.transition = transitionEnd()
	
	    if (!$.support.transition) return
	
	    $.event.special.bsTransitionEnd = {
	      bindType: $.support.transition.end,
	      delegateType: $.support.transition.end,
	      handle: function (e) {
	        if ($(e.target).is(this)) return e.handleObj.handler.apply(this, arguments)
	      }
	    }
	  })
	
	}(jQuery);
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($) {'use strict';
	
	/**
	 * Header's elements scripts.
	 *
	 * @module Header
	 */
	
	/** Import easy-autocomplete plugin */
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	/** Import utility functions */
	
	
	/** Import Validator class */
	
	
	__webpack_require__(5);
	
	var _Helpers = __webpack_require__(6);
	
	var _Validator = __webpack_require__(7);
	
	var _Validator2 = _interopRequireDefault(_Validator);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	/** Class representing a website's header functions. Initialized by default. */
	var Header = function () {
	
	  /**
	   * Cache header's DOM elements for instant access.
	   */
	  function Header() {
	    _classCallCheck(this, Header);
	
	    /** Search */
	    this.search_input = $('.header-search__search-field input');
	    this.search_trigger = $('.header-search-trigger');
	    this.search_button = $('.header-search__search-button');
	
	    /** Header links */
	    this.headerLinks = $('.header-links__links');
	    this.headerLinks_toggle = $('.header-links-toggle');
	
	    /** Categories */
	    this.allCategories_toggle = $('.header-search__categories');
	    this.allCategories = $('.all-categories-container');
	    this.category = $('.all-categories-container .category');
	    this.currentCategory = $('.header-search__categories > span');
	
	    /** City */
	    this.city_choice = $('#city_choice');
	    this.city_list = $('.header-links__city-list');
	    this.city_item = $('.header-links__city-item');
	    this.city_selected = $('.header-links__city-selected');
	
	    /** Modal's components */
	    this.modalRegisterTrigger = $('#call_register_modal');
	    this.modalRegister = $('.modal.modal-register');
	    this.modalLogin = $('.modal.modal-login');
	
	    /** Forms */
	    this.loginForm = $('.modal .form-login');
	    this.registrationForm = $('.modal .form-register');
	
	    /** AJAX URLs */
	    this.searchUrl = './ajax/search.json';
	    this.cityUrl = './ajax/city.json';
	    this.loginUrl = './ajax/success.json';
	    this.registrationUrl = './ajax/success.json';
	
	    /** AJAX event names */
	    this.searchWithKeypressEventName = 'searchWithKeypress';
	    this.searchWithEnterOrButtonEventName = 'searchWithEnterOrClick';
	    this.loginEventName = 'userLogin';
	    this.registrationEventName = 'userRegister';
	
	    /** Personal cabinet dropdown */
	    this.loggedUserDropdown = $('.header-user-dropdown');
	    this.loggedUserDropdown_toggle = $('.header-user-toggle');
	  }
	
	  /**
	   * Returns strings representing element's state.
	   *
	   * @return {String} class name representing element's state.
	   */
	
	
	  _createClass(Header, [{
	    key: 'headerSizeToggleInit',
	
	
	    /**
	     * Toggle header size on devices.
	     *
	     * @return {Header}
	     */
	    value: function headerSizeToggleInit() {
	      var smallHeader = this.states.smallHeader;
	
	      $(window).on('scroll', function () {
	        if ((0, _Helpers.screenWidthDynamically)() < 600) {
	          var scrollTop = $(window).scrollTop();
	
	          if (scrollTop > 10) {
	            _Helpers.body.addClass(smallHeader);
	          } else {
	            _Helpers.body.removeClass(smallHeader);
	          }
	        }
	      });
	
	      return this;
	    }
	
	    /**
	     * Initialize dropDown components.
	     *
	     * @return {Header}
	     */
	
	  }, {
	    key: 'dropdownListsInit',
	    value: function dropdownListsInit() {
	      // cities
	      (0, _Helpers.dropdownItemInit)(this.city_choice, this.city_list);
	      // categories
	      (0, _Helpers.dropdownItemInit)(this.allCategories_toggle, this.allCategories);
	      // header links ( only on mobile devices )
	      if (_Helpers.screenWidth <= _Helpers.tabletLandscapeBreakpoint) {
	        (0, _Helpers.dropdownItemInit)(this.headerLinks_toggle, this.headerLinks);
	      }
	
	      return this;
	    }
	
	    /**
	     * Initialize one item from list selection elements.
	     *
	     * @return {Header}
	     */
	
	  }, {
	    key: 'selectItemFromListInit',
	    value: function selectItemFromListInit() {
	      // city from cities
	      (0, _Helpers.selectOneFromListInit)(this.city_item, this.city_selected, this.city_list);
	      // category from categories
	      (0, _Helpers.selectOneFromListInit)(this.category, this.currentCategory);
	
	      return this;
	    }
	
	    /**
	     * Get current city and perform AJAX request for every city change.
	     *
	     * @return {Header}
	     */
	
	  }, {
	    key: 'dynamicCityChangeInit',
	    value: function dynamicCityChangeInit() {
	      var url = this.cityUrl;
	      var cityItem = this.city_item;
	
	      cityItem.on('click tap', function () {
	        var cityId = $(this).attr('data-city-id');
	        var data = JSON.stringify({
	          event: 'cityChange',
	          id: cityId
	        });
	
	        $.ajax({
	          url: url,
	          method: 'POST',
	          dataType: 'json',
	          data: data,
	          success: function success(res) {
	            console.log('Data: ' + data + '\nresponse: ' + res);
	          }
	        });
	      });
	
	      return this;
	    }
	
	    /**
	     * Perform AJAX request for searched phrase.
	     *
	     * @param {String} url - string with url.
	     */
	
	  }, {
	    key: 'ajaxSearch',
	    value: function ajaxSearch(url) {
	      var categories = this.allCategories;
	      var searchInput = this.search_input;
	      var activeEventName = this.searchWithEnterOrButtonEventName;
	      var category = categories.find('.active').attr('data-category-id');
	      var phrase = searchInput.val();
	
	      // prevent empty string search
	      if (!phrase) return;
	
	      var data = JSON.stringify({
	        phrase: {
	          event: activeEventName,
	          text: phrase,
	          category: category
	        }
	      });
	
	      $.ajax({
	        url: url,
	        method: 'POST',
	        cache: false,
	        dataType: 'json',
	        data: data,
	        success: function success(res) {
	          console.log('Data: ' + data + '\nresponse: ' + res);
	        }
	      });
	    }
	
	    /**
	     * Bind 'Enter' key and 'Search' button to perform search ajax request.
	     *
	     * @return {Header}
	     */
	
	  }, {
	    key: 'bindSearchKeys',
	    value: function bindSearchKeys() {
	      var _this2 = this;
	
	      var url = this.searchUrl;
	
	      this.search_button.on('click tap', function () {
	        return _this2.ajaxSearch(url);
	      });
	
	      this.search_input.on('keypress', function (e) {
	        if (e.which == 13) {
	          e.preventDefault();
	
	          _this2.ajaxSearch(url);
	        }
	      });
	
	      return this;
	    }
	
	    /**
	     * Generate options for easy-autocomplete plug-in.
	     *
	     * @static
	     * @return {Object} options for easy-autocomplete.
	     */
	
	  }, {
	    key: 'autofillInit',
	
	
	    /**
	     * Initialize easy-autocomplete plug-in on search bar's input.
	     *
	     * @return {Header}
	     */
	    value: function autofillInit() {
	      this.search_input.easyAutocomplete(Header.autofillOptions);
	
	      return this;
	    }
	
	    /**
	     * Initialize search bar's active state on focus.
	     *
	     * @return {Header}
	     */
	
	  }, {
	    key: 'searchActiveStateOnFocusInit',
	    value: function searchActiveStateOnFocusInit() {
	      var searchActiveState = this.states.activeSearchInput;
	
	      this.search_input.focus(function () {
	        _Helpers.body.addClass(searchActiveState);
	      }).blur(function () {
	        _Helpers.body.removeClass(searchActiveState);
	      });
	
	      return this;
	    }
	
	    /**
	     * Initialize search bar's show/hide toggle on click.
	     *
	     * @return {Header}
	     */
	
	  }, {
	    key: 'toggleSearchBarOnClickInit',
	    value: function toggleSearchBarOnClickInit() {
	      (0, _Helpers.activeBodyClassItemInit)(this.search_trigger, this.states.searchBarVisible);
	
	      return this;
	    }
	
	    /**
	     * Initialize all of search bar scripts.
	     *
	     * @return {Header}
	     */
	
	  }, {
	    key: 'searchBarInit',
	    value: function searchBarInit() {
	      this.autofillInit().searchActiveStateOnFocusInit().toggleSearchBarOnClickInit().bindSearchKeys();
	
	      return this;
	    }
	
	    /**
	     * Issue with opening register modal from login modal fix.
	     *
	     * @return {Header}
	     */
	
	  }, {
	    key: 'registerModalFromLoginModal',
	    value: function registerModalFromLoginModal() {
	      var _this = this;
	
	      _this.modalRegisterTrigger.on('click tap', function (e) {
	        e.preventDefault();
	
	        _this.modalLogin.modal('hide').on('hidden.bs.modal', function () {
	          _this.modalRegister.modal('show');
	
	          $(this).off('hidden.bs.modal');
	        });
	      });
	
	      return this;
	    }
	
	    /**
	     * Initialize personal cabinet menu dropdown.
	     *
	     * @return {Header}
	     */
	
	  }, {
	    key: 'userCabinetMenuInit',
	    value: function userCabinetMenuInit() {
	      var container = this.loggedUserDropdown;
	      var trigger = this.loggedUserDropdown_toggle;
	
	      (0, _Helpers.dropdownItemInit)(trigger, container);
	
	      return this;
	    }
	
	    /**
	     * Unbind login and register modal triggers.
	     *
	     * @return {Header}
	     */
	
	  }, {
	    key: 'disableLoginModal',
	    value: function disableLoginModal() {
	      $('[data-modal-action="login"]').removeAttr('data-target');
	      $('[data-modal-action="register"]').removeAttr('data-target');
	
	      return this;
	    }
	
	    /**
	     * Initialize scripts for logged-in users.
	     *
	     * @return {Header}
	     */
	
	  }, {
	    key: 'loggedUserActionsInit',
	    value: function loggedUserActionsInit() {
	      this.disableLoginModal().userCabinetMenuInit();
	
	      return this;
	    }
	
	    /**
	     * Initialize Login form validation and data send.
	     *
	     * @return {Header}
	     */
	
	  }, {
	    key: 'initLoginValidation',
	    value: function initLoginValidation() {
	      var url = this.loginUrl;
	      var eventName = this.loginEventName;
	      var options = {
	        nestedInModal: true
	      };
	
	      var validateLogin = new _Validator2.default(this.loginForm, function (context) {
	        var data = {
	          event: eventName,
	          data: context.serializedFormData()
	        };
	
	        return {
	          url: url,
	          method: 'GET',
	          cache: false,
	          dataType: 'json',
	          data: data,
	          success: function success(res) {
	            console.log('Your callback here');
	            console.log('DATA: ' + JSON.stringify(data));
	            if (res.response === 'success') {
	              console.log('login successful');
	              context.unbindOnClick();
	            } else {
	              console.log('login failed');
	            }
	          }
	        };
	      }, options);
	
	      return this;
	    }
	
	    /**
	     * Initialize Login form validation and data send.
	     *
	     * @return {Header}
	     */
	
	  }, {
	    key: 'initRegistrationValidation',
	    value: function initRegistrationValidation() {
	      var url = this.registrationUrl;
	      var eventName = this.registrationEventName;
	      var options = {
	        nestedInModal: true
	      };
	
	      var validateRegistration = new _Validator2.default(this.registrationForm, function (context) {
	        var data = {
	          event: eventName,
	          data: context.serializedFormData()
	        };
	
	        return {
	          url: url,
	          method: 'GET',
	          cache: false,
	          dataType: 'json',
	          data: data,
	          success: function success(res) {
	            console.log('Your callback here');
	            console.log('DATA: ' + JSON.stringify(data));
	            if (res.response === 'success') {
	              console.log('registration successful');
	              context.unbindOnClick();
	            } else {
	              console.log('registration failed');
	            }
	          }
	        };
	      }, options);
	
	      return this;
	    }
	
	    /**
	     * Initialize scripts for not logged-in users.
	     *
	     * @return {Header}
	     */
	
	  }, {
	    key: 'notLoggedUserActionsInit',
	    value: function notLoggedUserActionsInit() {
	      this.initLoginValidation().initRegistrationValidation();
	
	      return this;
	    }
	
	    /**
	     * Initialize Header scripts.
	     *
	     * @static
	     */
	
	  }, {
	    key: 'states',
	    get: function get() {
	      return {
	        activeSearchInput: 'input-active',
	        searchBarVisible: 'search-active',
	        smallHeader: 'header-small'
	      };
	    }
	  }], [{
	    key: 'init',
	    value: function init() {
	      var obj = new this();
	
	      obj.dynamicCityChangeInit().headerSizeToggleInit().dropdownListsInit().selectItemFromListInit().registerModalFromLoginModal().searchBarInit();
	
	      if (_Helpers.userLoggedIn) {
	        obj.loggedUserActionsInit();
	      } else {
	        obj.notLoggedUserActionsInit();
	      }
	    }
	  }, {
	    key: 'autofillOptions',
	    get: function get() {
	      var obj = new this();
	      var categories = obj.allCategories;
	      var url = obj.searchUrl;
	      var keypressEventName = this.searchWithKeypressEventName;
	
	      return {
	        getValue: "text",
	
	        url: url,
	
	        ajaxSettings: {
	          dataType: "json",
	          method: "POST"
	        },
	
	        preparePostData: function preparePostData(data, phrase) {
	          var category = categories.find('.active').attr('data-category-id');
	
	          data = JSON.stringify({
	            phrase: {
	              event: keypressEventName,
	              text: phrase,
	              category: category
	            }
	          });
	
	          return data;
	        },
	
	        template: {
	          type: "links",
	          fields: {
	            link: "website-link"
	          }
	        },
	
	        list: {
	          showAnimation: {
	            type: "fade",
	            time: 150
	          },
	          hideAnimation: {
	            type: "fade",
	            time: 150
	          },
	          maxNumberOfElements: 6,
	          match: {
	            // @TODO: set 'enabled' to 'false' on production
	            enabled: true
	          }
	        }
	      };
	    }
	  }]);
	
	  return Header;
	}();
	
	exports.default = Header;
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($, jQuery) {/*
	 * easy-autocomplete
	 * jQuery plugin for autocompletion
	 * 
	 * @author Łukasz Pawełczak (http://github.com/pawelczak)
	 * @version 1.3.5
	 * Copyright  License: 
	 */
	
	/*
	 * EasyAutocomplete - Configuration 
	 */
	var EasyAutocomplete = (function(scope){
	
		scope.Configuration = function Configuration(options) {
			var defaults = {
				data: "list-required",
				url: "list-required",
				dataType: "json",
	
				listLocation: function(data) {
					return data;
				},
	
				xmlElementName: "",
	
				getValue: function(element) {
					return element;
				},
	
				autocompleteOff: true,
	
				placeholder: false,
	
				ajaxCallback: function() {},
	
				matchResponseProperty: false,
	
				list: {
					sort: {
						enabled: false,
						method: function(a, b) {
							a = defaults.getValue(a);
							b = defaults.getValue(b);
							if (a < b) {
								return -1;
							}
							if (a > b) {
								return 1;
							}
							return 0;
						}
					},
	
					maxNumberOfElements: 6,
	
					hideOnEmptyPhrase: true,
	
					match: {
						enabled: false,
						caseSensitive: false,
						method: function(element, phrase) {
	
							if (element.search(phrase) > -1) {
								return true;
							} else {
								return false;
							}
						}
					},
	
					showAnimation: {
						type: "normal", //normal|slide|fade
						time: 400,
						callback: function() {}
					},
	
					hideAnimation: {
						type: "normal",
						time: 400,
						callback: function() {}
					},
	
					/* Events */
					onClickEvent: function() {},
					onSelectItemEvent: function() {},
					onLoadEvent: function() {},
					onChooseEvent: function() {},
					onKeyEnterEvent: function() {},
					onMouseOverEvent: function() {},
					onMouseOutEvent: function() {},	
					onShowListEvent: function() {},
					onHideListEvent: function() {}
				},
	
				highlightPhrase: true,
	
				theme: "",
	
				cssClasses: "",
	
				minCharNumber: 0,
	
				requestDelay: 0,
	
				adjustWidth: true,
	
				ajaxSettings: {},
	
				preparePostData: function(data, inputPhrase) {return data;},
	
				loggerEnabled: true,
	
				template: "",
	
				categoriesAssigned: false,
	
				categories: [{
					maxNumberOfElements: 4
				}]
	
			};
			
			var externalObjects = ["ajaxSettings", "template"];
	
			this.get = function(propertyName) {
				return defaults[propertyName];
			};
	
			this.equals = function(name, value) {
				if (isAssigned(name)) {
					if (defaults[name] === value) {
						return true;
					}
				} 
				
				return false;
			};
	
			this.checkDataUrlProperties = function() {
				if (defaults.url === "list-required" && defaults.data === "list-required") {
					return false;
				}
				return true;
			};
			this.checkRequiredProperties = function() {
				for (var propertyName in defaults) {
					if (defaults[propertyName] === "required") {
						logger.error("Option " + propertyName + " must be defined");
						return false;
					}
				}
				return true;
			};
	
			this.printPropertiesThatDoesntExist = function(consol, optionsToCheck) {
				printPropertiesThatDoesntExist(consol, optionsToCheck);
			};
	
	
			prepareDefaults();
	
			mergeOptions();
	
			if (defaults.loggerEnabled === true) {
				printPropertiesThatDoesntExist(console, options);	
			}
	
			addAjaxSettings();
	
			processAfterMerge();
			function prepareDefaults() {
	
				if (options.dataType === "xml") {
					
					if (!options.getValue) {
	
						options.getValue = function(element) {
							return $(element).text();
						};
					}
	
					
					if (!options.list) {
	
						options.list = {};
					} 
	
					if (!options.list.sort) {
						options.list.sort = {};
					}
	
	
					options.list.sort.method = function(a, b) {
						a = options.getValue(a);
						b = options.getValue(b);
						if (a < b) {
							return -1;
						}
						if (a > b) {
							return 1;
						}
						return 0;
					};
	
					if (!options.list.match) {
						options.list.match = {};
					}
	
					options.list.match.method = function(element, phrase) {
	
						if (element.search(phrase) > -1) {
							return true;
						} else {
							return false;
						}
					};
	
				}
				if (options.categories !== undefined && options.categories instanceof Array) {
	
					var categories = [];
	
					for (var i = 0, length = options.categories.length; i < length; i += 1) { 
	
						var category = options.categories[i];
	
						for (var property in defaults.categories[0]) {
	
							if (category[property] === undefined) {
								category[property] = defaults.categories[0][property];
							}
						}
	
						categories.push(category);
					}
	
					options.categories = categories;
				}
			}
	
			function mergeOptions() {
	
				defaults = mergeObjects(defaults, options);
	
				function mergeObjects(source, target) {
					var mergedObject = source || {};
	
					for (var propertyName in source) {
						if (target[propertyName] !== undefined && target[propertyName] !== null) {
	
							if (typeof target[propertyName] !== "object" || 
									target[propertyName] instanceof Array) {
								mergedObject[propertyName] = target[propertyName];
							} else {
								mergeObjects(source[propertyName], target[propertyName]);
							}
						}
					}
				
					/* If data is an object */
					if (target.data !== undefined && target.data !== null && typeof target.data === "object") {
						mergedObject.data = target.data;
					}
	
					return mergedObject;
				}
			}	
	
	
			function processAfterMerge() {
				
				if (defaults.url !== "list-required" && typeof defaults.url !== "function") {
					var defaultUrl = defaults.url;
					defaults.url = function() {
						return defaultUrl;
					};
				}
	
				if (defaults.ajaxSettings.url !== undefined && typeof defaults.ajaxSettings.url !== "function") {
					var defaultUrl = defaults.ajaxSettings.url;
					defaults.ajaxSettings.url = function() {
						return defaultUrl;
					};
				}
	
				if (typeof defaults.listLocation === "string") {
					var defaultlistLocation = defaults.listLocation;
	
					if (defaults.dataType.toUpperCase() === "XML") {
						defaults.listLocation = function(data) {
							return $(data).find(defaultlistLocation);
						};
					} else {
						defaults.listLocation = function(data) {
							return data[defaultlistLocation];
						};	
					}
				}
	
				if (typeof defaults.getValue === "string") {
					var defaultsGetValue = defaults.getValue;
					defaults.getValue = function(element) {
						return element[defaultsGetValue];
					};
				}
	
				if (options.categories !== undefined) {
					defaults.categoriesAssigned = true;
				}
	
			}
	
			function addAjaxSettings() {
	
				if (options.ajaxSettings !== undefined && typeof options.ajaxSettings === "object") {
					defaults.ajaxSettings = options.ajaxSettings;
				} else {
					defaults.ajaxSettings = {};	
				}
				
			}
	
			function isAssigned(name) {
				if (defaults[name] !== undefined && defaults[name] !== null) {
					return true;
				} else {
					return false;
				}
			}
			function printPropertiesThatDoesntExist(consol, optionsToCheck) {
				
				checkPropertiesIfExist(defaults, optionsToCheck);
	
				function checkPropertiesIfExist(source, target) {
					for(var property in target) {
						if (source[property] === undefined) {
							consol.log("Property '" + property + "' does not exist in EasyAutocomplete options API.");		
						}
	
						if (typeof source[property] === "object" && $.inArray(property, externalObjects) === -1) {
							checkPropertiesIfExist(source[property], target[property]);
						}
					}	
				}
			}
		};
	
		return scope;
	
	})(EasyAutocomplete || {});
	
	
	/*
	 * EasyAutocomplete - Logger 
	 */
	var EasyAutocomplete = (function(scope){
		
		scope.Logger = function Logger() {
	
			this.error = function(message) {
				console.log("ERROR: " + message);
			};
	
			this.warning = function(message) {
				console.log("WARNING: " + message);
			};
		};
	
		return scope;
	
	})(EasyAutocomplete || {});
		
	
	/*
	 * EasyAutocomplete - Constans
	 */
	var EasyAutocomplete = (function(scope){	
		
		scope.Constans = function Constans() {
			var constants = {
				CONTAINER_CLASS: "easy-autocomplete-container",
				CONTAINER_ID: "eac-container-",
	
				WRAPPER_CSS_CLASS: "easy-autocomplete"
			};
	
			this.getValue = function(propertyName) {
				return constants[propertyName];
			};
	
		};
	
		return scope;
	
	})(EasyAutocomplete || {});
	
	/*
	 * EasyAutocomplete - ListBuilderService 
	 *
	 * @author Łukasz Pawełczak 
	 *
	 */
	var EasyAutocomplete = (function(scope) {
	
		scope.ListBuilderService = function ListBuilderService(configuration, proccessResponseData) {
	
	
			this.init = function(data) {
				var listBuilder = [],
					builder = {};
	
				builder.data = configuration.get("listLocation")(data);
				builder.getValue = configuration.get("getValue");
				builder.maxListSize = configuration.get("list").maxNumberOfElements;
	
					
				listBuilder.push(builder);
	
				return listBuilder;
			};
	
			this.updateCategories = function(listBuilder, data) {
				
				if (configuration.get("categoriesAssigned")) {
	
					listBuilder = [];
	
					for(var i = 0; i < configuration.get("categories").length; i += 1) {
	
						var builder = convertToListBuilder(configuration.get("categories")[i], data);
	
						listBuilder.push(builder);
					}
	
				} 
	
				return listBuilder;
			};
	
			this.convertXml = function(listBuilder) {
				if(configuration.get("dataType").toUpperCase() === "XML") {
	
					for(var i = 0; i < listBuilder.length; i += 1) {
						listBuilder[i].data = convertXmlToList(listBuilder[i]);
					}
				}
	
				return listBuilder;
			};
	
			this.processData = function(listBuilder, inputPhrase) {
	
				for(var i = 0, length = listBuilder.length; i < length; i+=1) {
					listBuilder[i].data = proccessResponseData(configuration, listBuilder[i], inputPhrase);
				}
	
				return listBuilder;
			};
	
			this.checkIfDataExists = function(listBuilders) {
	
				for(var i = 0, length = listBuilders.length; i < length; i += 1) {
	
					if (listBuilders[i].data !== undefined && listBuilders[i].data instanceof Array) {
						if (listBuilders[i].data.length > 0) {
							return true;
						}
					} 
				}
	
				return false;
			};
	
	
			function convertToListBuilder(category, data) {
	
				var builder = {};
	
				if(configuration.get("dataType").toUpperCase() === "XML") {
	
					builder = convertXmlToListBuilder();
				} else {
	
					builder = convertDataToListBuilder();
				}
				
	
				if (category.header !== undefined) {
					builder.header = category.header;
				}
	
				if (category.maxNumberOfElements !== undefined) {
					builder.maxNumberOfElements = category.maxNumberOfElements;
				}
	
				if (configuration.get("list").maxNumberOfElements !== undefined) {
	
					builder.maxListSize = configuration.get("list").maxNumberOfElements;
				}
	
				if (category.getValue !== undefined) {
	
					if (typeof category.getValue === "string") {
						var defaultsGetValue = category.getValue;
						builder.getValue = function(element) {
							return element[defaultsGetValue];
						};
					} else if (typeof category.getValue === "function") {
						builder.getValue = category.getValue;
					}
	
				} else {
					builder.getValue = configuration.get("getValue");	
				}
				
	
				return builder;
	
	
				function convertXmlToListBuilder() {
	
					var builder = {},
						listLocation;
	
					if (category.xmlElementName !== undefined) {
						builder.xmlElementName = category.xmlElementName;
					}
	
					if (category.listLocation !== undefined) {
	
						listLocation = category.listLocation;
					} else if (configuration.get("listLocation") !== undefined) {
	
						listLocation = configuration.get("listLocation");
					}
	
					if (listLocation !== undefined) {
						if (typeof listLocation === "string") {
							builder.data = $(data).find(listLocation);
						} else if (typeof listLocation === "function") {
	
							builder.data = listLocation(data);
						}
					} else {
	
						builder.data = data;
					}
	
					return builder;
				}
	
	
				function convertDataToListBuilder() {
	
					var builder = {};
	
					if (category.listLocation !== undefined) {
	
						if (typeof category.listLocation === "string") {
							builder.data = data[category.listLocation];
						} else if (typeof category.listLocation === "function") {
							builder.data = category.listLocation(data);
						}
					} else {
						builder.data = data;
					}
	
					return builder;
				}
			}
	
			function convertXmlToList(builder) {
				var simpleList = [];
	
				if (builder.xmlElementName === undefined) {
					builder.xmlElementName = configuration.get("xmlElementName");
				}
	
	
				$(builder.data).find(builder.xmlElementName).each(function() {
					simpleList.push(this);
				});
	
				return simpleList;
			}
	
		};
	
		return scope;
	
	})(EasyAutocomplete || {});
	
	
	/*
	 * EasyAutocomplete - Data proccess module
	 *
	 * Process list to display:
	 * - sort 
	 * - decrease number to specific number
	 * - show only matching list
	 *
	 */
	var EasyAutocomplete = (function(scope) {
	
		scope.proccess = function proccessData(config, listBuilder, phrase) {
	
			scope.proccess.match = match;
	
			var list = listBuilder.data,
				inputPhrase = phrase;//TODO REFACTOR
	
			list = findMatch(list, inputPhrase);
			list = reduceElementsInList(list);
			list = sort(list);
	
			return list;
	
	
			function findMatch(list, phrase) {
				var preparedList = [],
					value = "";
	
				if (config.get("list").match.enabled) {
	
					for(var i = 0, length = list.length; i < length; i += 1) {
	
						value = config.get("getValue")(list[i]);
						
						if (match(value, phrase)) {
							preparedList.push(list[i]);	
						}
						
					}
	
				} else {
					preparedList = list;
				}
	
				return preparedList;
			}
	
			function match(value, phrase) {
	
				if (!config.get("list").match.caseSensitive) {
	
					if (typeof value === "string") {
						value = value.toLowerCase();	
					}
					
					phrase = phrase.toLowerCase();
				}
				if (config.get("list").match.method(value, phrase)) {
					return true;
				} else {
					return false;
				}
			}
	
			function reduceElementsInList(list) {
				if (listBuilder.maxNumberOfElements !== undefined && list.length > listBuilder.maxNumberOfElements) {
					list = list.slice(0, listBuilder.maxNumberOfElements);
				}
	
				return list;
			}
	
			function sort(list) {
				if (config.get("list").sort.enabled) {
					list.sort(config.get("list").sort.method);
				}
	
				return list;
			}
			
		};
	
	
		return scope;
	
	
	})(EasyAutocomplete || {});
	
	
	/*
	 * EasyAutocomplete - Template 
	 *
	 * 
	 *
	 */
	var EasyAutocomplete = (function(scope){
	
		scope.Template = function Template(options) {
	
	
			var genericTemplates = {
				basic: {
					type: "basic",
					method: function(element) { return element; },
					cssClass: ""
				},
				description: {
					type: "description",
					fields: {
						description: "description"
					},
					method: function(element) {	return element + " - description"; },
					cssClass: "eac-description"
				},
				iconLeft: {
					type: "iconLeft",
					fields: {
						icon: ""
					},
					method: function(element) {
						return element;
					},
					cssClass: "eac-icon-left"
				},
				iconRight: {
					type: "iconRight",
					fields: {
						iconSrc: ""
					},
					method: function(element) {
						return element;
					},
					cssClass: "eac-icon-right"
				},
				links: {
					type: "links",
					fields: {
						link: ""
					},
					method: function(element) {
						return element;
					},
					cssClass: ""
				},
				custom: {
					type: "custom",
					method: function() {},
					cssClass: ""
				}
			},
	
	
	
			/*
			 * Converts method with {{text}} to function
			 */
			convertTemplateToMethod = function(template) {
	
	
				var _fields = template.fields,
					buildMethod;
	
				if (template.type === "description") {
	
					buildMethod = genericTemplates.description.method; 
	
					if (typeof _fields.description === "string") {
						buildMethod = function(elementValue, element) {
							return elementValue + " - <span>" + element[_fields.description] + "</span>";
						};					
					} else if (typeof _fields.description === "function") {
						buildMethod = function(elementValue, element) {
							return elementValue + " - <span>" + _fields.description(element) + "</span>";
						};	
					}
	
					return buildMethod;
				}
	
				if (template.type === "iconRight") {
	
					if (typeof _fields.iconSrc === "string") {
						buildMethod = function(elementValue, element) {
							return elementValue + "<img class='eac-icon' src='" + element[_fields.iconSrc] + "' />" ;
						};					
					} else if (typeof _fields.iconSrc === "function") {
						buildMethod = function(elementValue, element) {
							return elementValue + "<img class='eac-icon' src='" + _fields.iconSrc(element) + "' />" ;
						};
					}
	
					return buildMethod;
				}
	
	
				if (template.type === "iconLeft") {
	
					if (typeof _fields.iconSrc === "string") {
						buildMethod = function(elementValue, element) {
							return "<img class='eac-icon' src='" + element[_fields.iconSrc] + "' />" + elementValue;
						};					
					} else if (typeof _fields.iconSrc === "function") {
						buildMethod = function(elementValue, element) {
							return "<img class='eac-icon' src='" + _fields.iconSrc(element) + "' />" + elementValue;
						};
					}
	
					return buildMethod;
				}
	
				if(template.type === "links") {
	
					if (typeof _fields.link === "string") {
						buildMethod = function(elementValue, element) {
							return "<a href='" + element[_fields.link] + "' >" + elementValue + "</a>";
						};					
					} else if (typeof _fields.link === "function") {
						buildMethod = function(elementValue, element) {
							return "<a href='" + _fields.link(element) + "' >" + elementValue + "</a>";
						};
					}
	
					return buildMethod;
				}
	
	
				if (template.type === "custom") {
	
					return template.method;
				}
	
				return genericTemplates.basic.method;
	
			},
	
	
			prepareBuildMethod = function(options) {
				if (!options || !options.type) {
	
					return genericTemplates.basic.method;
				}
	
				if (options.type && genericTemplates[options.type]) {
	
					return convertTemplateToMethod(options);
				} else {
	
					return genericTemplates.basic.method;
				}
	
			},
	
			templateClass = function(options) {
				var emptyStringFunction = function() {return "";};
	
				if (!options || !options.type) {
	
					return emptyStringFunction;
				}
	
				if (options.type && genericTemplates[options.type]) {
					return (function () { 
						var _cssClass = genericTemplates[options.type].cssClass;
						return function() { return _cssClass;};
					})();
				} else {
					return emptyStringFunction;
				}
			};
	
	
			this.getTemplateClass = templateClass(options);
	
			this.build = prepareBuildMethod(options);
	
	
		};
	
		return scope;
	
	})(EasyAutocomplete || {});
	
	
	/*
	 * EasyAutocomplete - jQuery plugin for autocompletion
	 *
	 */
	var EasyAutocomplete = (function(scope) {
	
		
		scope.main = function Core($input, options) {
					
			var module = {
					name: "EasyAutocomplete",
					shortcut: "eac"
				};
	
			var consts = new scope.Constans(),
				config = new scope.Configuration(options),
				logger = new scope.Logger(),
				template = new scope.Template(options.template),
				listBuilderService = new scope.ListBuilderService(config, scope.proccess),
				checkParam = config.equals,
	
				$field = $input, 
				$container = "",
				elementsList = [],
				selectedElement = -1,
				requestDelayTimeoutId;
	
			scope.consts = consts;
	
			this.getConstants = function() {
				return consts;
			};
	
			this.getConfiguration = function() {
				return config;
			};
	
			this.getContainer = function() {
				return $container;
			};
	
			this.getSelectedItemIndex = function() {
				return selectedElement;
			};
	
			this.getItems = function () {
				return elementsList;
			};
	
			this.getItemData = function(index) {
	
				if (elementsList.length < index || elementsList[index] === undefined) {
					return -1;
				} else {
					return elementsList[index];
				}
			};
	
			this.getSelectedItemData = function() {
				return this.getItemData(selectedElement);
			};
	
			this.build = function() {
				prepareField();
			};
	
			this.init = function() {
				init();
			};
			function init() {
	
				if ($field.length === 0) {
					logger.error("Input field doesn't exist.");
					return;
				}
	
				if (!config.checkDataUrlProperties()) {
					logger.error("One of options variables 'data' or 'url' must be defined.");
					return;
				}
	
				if (!config.checkRequiredProperties()) {
					logger.error("Will not work without mentioned properties.");
					return;
				}
	
	
				prepareField();
				bindEvents();	
	
			}
			function prepareField() {
	
					
				if ($field.parent().hasClass(consts.getValue("WRAPPER_CSS_CLASS"))) {
					removeContainer();
					removeWrapper();
				} 
				
				createWrapper();
				createContainer();	
	
				$container = $("#" + getContainerId());
				if (config.get("placeholder")) {
					$field.attr("placeholder", config.get("placeholder"));
				}
	
	
				function createWrapper() {
					var $wrapper = $("<div>"),
						classes = consts.getValue("WRAPPER_CSS_CLASS");
	
				
					if (config.get("theme") && config.get("theme") !== "") {
						classes += " eac-" + config.get("theme");
					}
	
					if (config.get("cssClasses") && config.get("cssClasses") !== "") {
						classes += " " + config.get("cssClasses");
					}
	
					if (template.getTemplateClass() !== "") {
						classes += " " + template.getTemplateClass();
					}
					
	
					$wrapper
						.addClass(classes);
					$field.wrap($wrapper);
	
	
					if (config.get("adjustWidth") === true) {
						adjustWrapperWidth();	
					}
					
	
				}
	
				function adjustWrapperWidth() {
					var fieldWidth = $field.outerWidth();
	
					$field.parent().css("width", fieldWidth);				
				}
	
				function removeWrapper() {
					$field.unwrap();
				}
	
				function createContainer() {
					var $elements_container = $("<div>").addClass(consts.getValue("CONTAINER_CLASS"));
	
					$elements_container
							.attr("id", getContainerId())
							.prepend($("<ul>"));
	
	
					(function() {
	
						$elements_container
							/* List show animation */
							.on("show.eac", function() {
	
								switch(config.get("list").showAnimation.type) {
	
									case "slide":
										var animationTime = config.get("list").showAnimation.time,
											callback = config.get("list").showAnimation.callback;
	
										$elements_container.find("ul").slideDown(animationTime, callback);
									break;
	
									case "fade":
										var animationTime = config.get("list").showAnimation.time,
											callback = config.get("list").showAnimation.callback;
	
										$elements_container.find("ul").fadeIn(animationTime), callback;
									break;
	
									default:
										$elements_container.find("ul").show();
									break;
								}
	
								config.get("list").onShowListEvent();
								
							})
							/* List hide animation */
							.on("hide.eac", function() {
	
								switch(config.get("list").hideAnimation.type) {
	
									case "slide":
										var animationTime = config.get("list").hideAnimation.time,
											callback = config.get("list").hideAnimation.callback;
	
										$elements_container.find("ul").slideUp(animationTime, callback);
									break;
	
									case "fade":
										var animationTime = config.get("list").hideAnimation.time,
											callback = config.get("list").hideAnimation.callback;
	
										$elements_container.find("ul").fadeOut(animationTime, callback);
									break;
	
									default:
										$elements_container.find("ul").hide();
									break;
								}
	
								config.get("list").onHideListEvent();
	
							})
							.on("selectElement.eac", function() {
								$elements_container.find("ul li").removeClass("selected");
								$elements_container.find("ul li").eq(selectedElement).addClass("selected");
	
								config.get("list").onSelectItemEvent();
							})
							.on("loadElements.eac", function(event, listBuilders, phrase) {
				
	
								var $item = "",
									$listContainer = $elements_container.find("ul");
	
								$listContainer
									.empty()
									.detach();
	
								elementsList = [];
								var counter = 0;
								for(var builderIndex = 0, listBuildersLength = listBuilders.length; builderIndex < listBuildersLength; builderIndex += 1) {
	
									var listData = listBuilders[builderIndex].data;
	
									if (listData.length === 0) {
										continue;
									}
	
									if (listBuilders[builderIndex].header !== undefined && listBuilders[builderIndex].header.length > 0) {
										$listContainer.append("<div class='eac-category' >" + listBuilders[builderIndex].header + "</div>");
									}
	
									for(var i = 0, listDataLength = listData.length; i < listDataLength && counter < listBuilders[builderIndex].maxListSize; i += 1) {
										$item = $("<li><div class='eac-item'></div></li>");
										
	
										(function() {
											var j = i,
												itemCounter = counter,
												elementsValue = listBuilders[builderIndex].getValue(listData[j]);
	
											$item.find(" > div")
												.on("click", function() {
	
													$field.val(elementsValue).trigger("change");
	
													selectedElement = itemCounter;
													selectElement(itemCounter);
	
													config.get("list").onClickEvent();
													config.get("list").onChooseEvent();
												})
												.mouseover(function() {
	
													selectedElement = itemCounter;
													selectElement(itemCounter);	
	
													config.get("list").onMouseOverEvent();
												})
												.mouseout(function() {
													config.get("list").onMouseOutEvent();
												})
												.html(template.build(highlight(elementsValue, phrase), listData[j]));
										})();
	
										$listContainer.append($item);
										elementsList.push(listData[i]);
										counter += 1;
									}
								}
	
								$elements_container.append($listContainer);
	
								config.get("list").onLoadEvent();
							});
	
					})();
	
					$field.after($elements_container);
				}
	
				function removeContainer() {
					$field.next("." + consts.getValue("CONTAINER_CLASS")).remove();
				}
	
				function highlight(string, phrase) {
	
					if(config.get("highlightPhrase") && phrase !== "") {
						return highlightPhrase(string, phrase);	
					} else {
						return string;
					}
						
				}
	
				function escapeRegExp(str) {
					return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
	 			}
	
				function highlightPhrase(string, phrase) {
					var escapedPhrase = escapeRegExp(phrase);
					return (string + "").replace(new RegExp("(" + escapedPhrase + ")", "gi") , "<b>$1</b>");
				}
	
	
	
			}
			function getContainerId() {
				
				var elementId = $field.attr("id");
	
				elementId = consts.getValue("CONTAINER_ID") + elementId;
	
				return elementId;
			}
			function bindEvents() {
	
				bindAllEvents();
				
	
				function bindAllEvents() {
					if (checkParam("autocompleteOff", true)) {
						removeAutocomplete();
					}
	
					bindFocusOut();
					bindKeyup();
					bindKeydown();
					bindKeypress();
					bindFocus();
					bindBlur();
				}
	
				function bindFocusOut() {
					$field.focusout(function () {
	
						var fieldValue = $field.val(),
							phrase;
	
						if (!config.get("list").match.caseSensitive) {
							fieldValue = fieldValue.toLowerCase();
						}
	
						for (var i = 0, length = elementsList.length; i < length; i += 1) {
	
							phrase = config.get("getValue")(elementsList[i]);
							if (!config.get("list").match.caseSensitive) {
								phrase = phrase.toLowerCase();
							}
	
							if (phrase === fieldValue) {
								selectedElement = i;
								selectElement(selectedElement);
								return;
							}
						}
					});
				}
	
				function bindKeyup() {
					$field
					.off("keyup")
					.keyup(function(event) {
	
						switch(event.keyCode) {
	
							case 27:
	
								hideContainer();
								loseFieldFocus();
							break;
	
							case 38:
	
								event.preventDefault();
	
								if(elementsList.length > 0 && selectedElement > 0) {
	
									selectedElement -= 1;
	
									$field.val(config.get("getValue")(elementsList[selectedElement]));
	
									selectElement(selectedElement);
	
								}						
							break;
	
							case 40:
	
								event.preventDefault();
	
								if(elementsList.length > 0 && selectedElement < elementsList.length - 1) {
	
									selectedElement += 1;
	
									$field.val(config.get("getValue")(elementsList[selectedElement]));
	
									selectElement(selectedElement);
									
								}
	
							break;
	
							default:
	
								if (event.keyCode > 40 || event.keyCode === 8) {
	
									var inputPhrase = $field.val();
	
									if (!(config.get("list").hideOnEmptyPhrase === true && event.keyCode === 8 && inputPhrase === "")) {
	
										if (config.get("requestDelay") > 0) {
											if (requestDelayTimeoutId !== undefined) {
												clearTimeout(requestDelayTimeoutId);
											}
	
											requestDelayTimeoutId = setTimeout(function () { loadData(inputPhrase);}, config.get("requestDelay"));
										} else {
											loadData(inputPhrase);
										}
	
									} else {
										hideContainer();
									}
									
								}
	
	
							break;
						}
					
	
						function loadData(inputPhrase) {
	
	
							if (inputPhrase.length < config.get("minCharNumber")) {
								return;
							}
	
	
							if (config.get("data") !== "list-required") {
	
								var data = config.get("data");
	
								var listBuilders = listBuilderService.init(data);
	
								listBuilders = listBuilderService.updateCategories(listBuilders, data);
								
								listBuilders = listBuilderService.processData(listBuilders, inputPhrase);
	
								loadElements(listBuilders, inputPhrase);
	
								if ($field.parent().find("li").length > 0) {
									showContainer();	
								} else {
									hideContainer();
								}
	
							}
	
							var settings = createAjaxSettings();
	
							if (settings.url === undefined || settings.url === "") {
								settings.url = config.get("url");
							}
	
							if (settings.dataType === undefined || settings.dataType === "") {
								settings.dataType = config.get("dataType");
							}
	
	
							if (settings.url !== undefined && settings.url !== "list-required") {
	
								settings.url = settings.url(inputPhrase);
	
								settings.data = config.get("preparePostData")(settings.data, inputPhrase);
	
								$.ajax(settings) 
									.done(function(data) {
	
										var listBuilders = listBuilderService.init(data);
	
										listBuilders = listBuilderService.updateCategories(listBuilders, data);
										
										listBuilders = listBuilderService.convertXml(listBuilders);
										if (checkInputPhraseMatchResponse(inputPhrase, data)) {
	
											listBuilders = listBuilderService.processData(listBuilders, inputPhrase);
	
											loadElements(listBuilders, inputPhrase);	
																					
										}
	
										if (listBuilderService.checkIfDataExists(listBuilders) && $field.parent().find("li").length > 0) {
											showContainer();	
										} else {
											hideContainer();
										}
	
										config.get("ajaxCallback")();
	
									})
									.fail(function() {
										logger.warning("Fail to load response data");
									})
									.always(function() {
	
									});
							}
	
							
	
							function createAjaxSettings() {
	
								var settings = {},
									ajaxSettings = config.get("ajaxSettings") || {};
	
								for (var set in ajaxSettings) {
									settings[set] = ajaxSettings[set];
								}
	
								return settings;
							}
	
							function checkInputPhraseMatchResponse(inputPhrase, data) {
	
								if (config.get("matchResponseProperty") !== false) {
									if (typeof config.get("matchResponseProperty") === "string") {
										return (data[config.get("matchResponseProperty")] === inputPhrase);
									}
	
									if (typeof config.get("matchResponseProperty") === "function") {
										return (config.get("matchResponseProperty")(data) === inputPhrase);
									}
	
									return true;
								} else {
									return true;
								}
	
							}
	
						}
	
	
					});
				}
	
				function bindKeydown() {
					$field
						.on("keydown", function(evt) {
		        		    evt = evt || window.event;
		        		    var keyCode = evt.keyCode;
		        		    if (keyCode === 38) {
		        		        suppressKeypress = true; 
		        		        return false;
		        		    }
			        	})
						.keydown(function(event) {
	
							if (event.keyCode === 13 && selectedElement > -1) {
	
								$field.val(config.get("getValue")(elementsList[selectedElement]));
	
								config.get("list").onKeyEnterEvent();
								config.get("list").onChooseEvent();
	
								selectedElement = -1;
								hideContainer();
	
								event.preventDefault();
							}
						});
				}
	
				function bindKeypress() {
					$field
					.off("keypress");
				}
	
				function bindFocus() {
					$field.focus(function() {
	
						if ($field.val() !== "" && elementsList.length > 0) {
							
							selectedElement = -1;
							showContainer();	
						}
										
					});
				}
	
				function bindBlur() {
					$field.blur(function() {
						setTimeout(function() { 
							
							selectedElement = -1;
							hideContainer();
						}, 250);
					});
				}
	
				function removeAutocomplete() {
					$field.attr("autocomplete","off");
				}
	
			}
	
			function showContainer() {
				$container.trigger("show.eac");
			}
	
			function hideContainer() {
				$container.trigger("hide.eac");
			}
	
			function selectElement(index) {
				
				$container.trigger("selectElement.eac", index);
			}
	
			function loadElements(list, phrase) {
				$container.trigger("loadElements.eac", [list, phrase]);
			}
	
			function loseFieldFocus() {
				$field.trigger("blur");
			}
	
	
		};
		scope.eacHandles = [];
	
		scope.getHandle = function(id) {
			return scope.eacHandles[id];
		};
	
		scope.inputHasId = function(input) {
	
			if($(input).attr("id") !== undefined && $(input).attr("id").length > 0) {
				return true;
			} else {
				return false;
			}
	
		};
	
		scope.assignRandomId = function(input) {
	
			var fieldId = "";
	
			do {
				fieldId = "eac-" + Math.floor(Math.random() * 10000);		
			} while ($("#" + fieldId).length !== 0);
			
			elementId = scope.consts.getValue("CONTAINER_ID") + fieldId;
	
			$(input).attr("id", fieldId);
	 
		};
	
		scope.setHandle = function(handle, id) {
			scope.eacHandles[id] = handle;
		};
	
	
		return scope;
	
	})(EasyAutocomplete || {});
	
	(function($) {
	
		$.fn.easyAutocomplete = function(options) {
	
			return this.each(function() {
				var $this = $(this),
					eacHandle = new EasyAutocomplete.main($this, options);
	
				if (!EasyAutocomplete.inputHasId($this)) {
					EasyAutocomplete.assignRandomId($this);
				}
	
				eacHandle.init();
	
				EasyAutocomplete.setHandle(eacHandle, $this.attr("id"));
	
			});
		};
	
		$.fn.getSelectedItemIndex = function() {
	
			var inputId = $(this).attr("id");
	
			if (inputId !== undefined) {
				return EasyAutocomplete.getHandle(inputId).getSelectedItemIndex();
			}
	
			return -1;
		};
	
		$.fn.getItems = function () {
	
			var inputId = $(this).attr("id");
	
			if (inputId !== undefined) {
				return EasyAutocomplete.getHandle(inputId).getItems();
			}
	
			return -1;
		};
	
		$.fn.getItemData = function(index) {
	
			var inputId = $(this).attr("id");
	
			if (inputId !== undefined && index > -1) {
				return EasyAutocomplete.getHandle(inputId).getItemData(index);
			}
	
			return -1;
		};
	
		$.fn.getSelectedItemData = function() {
	
			var inputId = $(this).attr("id");
	
			if (inputId !== undefined) {
				return EasyAutocomplete.getHandle(inputId).getSelectedItemData();
			}
	
			return -1;
		};
	
	})(jQuery);
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2), __webpack_require__(2)))

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($) {'use strict';
	
	/**
	 * Commonly used constants and functions.
	 *
	 * @module Helpers
	 */
	
	/**
	 * CSS class name to indicate most elements state.
	 *
	 * @constant
	 * @type {String}
	 */
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };
	
	var active = exports.active = 'active';
	
	/**
	 * CSS class name to indicate form's incorrect state.
	 *
	 * @constant
	 * @type {String}
	 */
	var formIsIncorrect = exports.formIsIncorrect = 'incorrect';
	
	/**
	 * CSS class name to indicate form's correct state.
	 *
	 * @constant
	 * @type {String}
	 */
	var formIsValidated = exports.formIsValidated = 'validated';
	
	/**
	 * Path to SVG-sprite file.
	 *
	 * @constant
	 * @type {String}
	 */
	var svgPath = exports.svgPath = './img/svg-default.svg';
	
	/**
	 * Symbol for params separation.
	 *
	 * @constant
	 * @type {String}
	 */
	var separator = exports.separator = '?';
	
	/**
	 * Cache body DOM element.
	 *
	 * @constant
	 * @type {jQuery}
	 */
	var body = exports.body = $('body');
	
	/**
	 * Detect current page.
	 *
	 * @constant
	 * @type {String}
	 */
	var currentPage = exports.currentPage = body.find('main').data('page');
	
	/**
	 * Detect personal cabinet's current page.
	 *
	 * @constant
	 * @type {String}
	 */
	var currentPcPage = exports.currentPcPage = body.find('main').data('pc-page');
	
	/**
	 * Menu should be hidden on all resolutions?
	 *
	 * @constant
	 * @type {Boolean}
	 */
	var menuIsAlwaysHidden = exports.menuIsAlwaysHidden = currentPage !== 'main';
	
	/**
	 * Detect window width once.
	 *
	 * @constant
	 * @type {Number}
	 */
	var screenWidth = exports.screenWidth = $(window).width();
	
	/**
	 * Detect whether user logged in or not and return true/false flag.
	 *
	 * @constant
	 * @type {Boolean}
	 */
	var userLoggedIn = exports.userLoggedIn = body.hasClass('logged');
	
	/**
	 * Breakpoint for landscape tablets.
	 *
	 * @constant
	 * @type {Number}
	 */
	var tabletLandscapeBreakpoint = exports.tabletLandscapeBreakpoint = 1280;
	
	/**
	 * Breakpoint for portrait tablets.
	 *
	 * @constant
	 * @type {Number}
	 */
	var tabletPortraitBreakpoint = exports.tabletPortraitBreakpoint = 600;
	
	/**
	 * Cache has own property method.
	 *
	 * @constant
	 * @type {Function}
	 */
	var has = exports.has = Object.prototype.hasOwnProperty;
	
	/**
	 * Custom HTML template for owlCarousel 'Prev' button.
	 *
	 * @constant
	 * @type {String}
	 */
	var buttonLeftTemplate = exports.buttonLeftTemplate = '<svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="' + svgPath + '#icon-item-buttonL"></use></svg>';
	
	/**
	 * Custom HTML template for owlCarousel 'Next' button.
	 *
	 * @constant
	 * @type {String}
	 */
	var buttonRightTemplate = exports.buttonRightTemplate = '<svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="' + svgPath + '#icon-item-buttonR"></use></svg>';
	
	/**
	 * Common options for owlCarousel sliders.
	 *
	 * @type {Object}
	 */
	var owlCommonOptions = exports.owlCommonOptions = {
	  loop: true,
	  dots: false,
	  navText: [buttonLeftTemplate, buttonRightTemplate],
	  margin: 10,
	  responsive: {
	    0: {
	      nav: true,
	      items: 1
	    },
	    600: {
	      nav: true,
	      items: 2
	    },
	    1024: {
	      nav: true,
	      items: 4
	    },
	    1670: {
	      nav: true,
	      items: 6
	    }
	  }
	};
	
	/**
	 * Initialize owl carousel on each element from array.
	 *
	 * @param {jQuery} elements - array of elements to init owlCarousel on.
	 * @param {Object} [options] - options for owlCarousel.
	 */
	var initOwlCarousel = exports.initOwlCarousel = function initOwlCarousel(elements) {
	  var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : owlCommonOptions;
	
	  elements.each(function (index, el) {
	    $(el).owlCarousel(options);
	  });
	};
	
	/**
	 * Check for the DOM element is visible in the current viewport.
	 *
	 * @param {jQuery} el - element to check.
	 * @param {Number} range - additional range to check, 200 by default.
	 */
	var isElementInViewport = exports.isElementInViewport = function isElementInViewport(el) {
	  var range = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 200;
	
	  el = el[0];
	  var elCords = el.getBoundingClientRect();
	
	  return elCords.top >= -range && elCords.left >= 0 && elCords.bottom <= $(window).height() + range && elCords.right <= $(window).width();
	};
	
	/**
	 * Crop text in selected element, if text's length more then specified number.
	 *
	 * @param {jQuery} elements - Elements to crop.
	 * @param {Number} symbolsToShow - Number of symbols, that should be shown.
	 */
	var cropText = exports.cropText = function cropText(elements, symbolsToShow) {
	  elements.each(function () {
	    var element = $(this);
	    var text = element.text();
	
	    if (text.length > symbolsToShow) {
	      var croppedText = text.substr(0, symbolsToShow) + '...';
	
	      element.text(croppedText);
	    }
	  });
	};
	
	/**
	 * Detect user logged in or not.
	 *
	 * @returns {Number} - Current window width.
	 */
	var screenWidthDynamically = exports.screenWidthDynamically = function screenWidthDynamically() {
	  return $(window).width();
	};
	
	/**
	 * Check specified item to be target of event.
	 *
	 * @param {Object} e - Event object.
	 * @param {jQuery} item - Item to compare with.
	 * @returns {Boolean} - Indicate whether clicked target is the specified item or no.
	 */
	var checkClosest = exports.checkClosest = function checkClosest(e, item) {
	  return $(e.target).closest(item).length > 0;
	};
	
	/**
	 * Toggles specified class name on body.
	 *
	 * @param {jQuery} elem - Trigger element.
	 * @param {String} className - Class name to apply.
	 */
	var activeBodyClassItemInit = exports.activeBodyClassItemInit = function activeBodyClassItemInit(elem, className) {
	  elem.on('click tap', function () {
	    body.toggleClass(className);
	  });
	};
	
	/**
	 * Toggle dropdown on click.
	 *
	 * @param {jQuery} activeItem - Click event handler.
	 * @param {jQuery} toggleItem - Toggling object.
	 */
	var toggleDropdown = exports.toggleDropdown = function toggleDropdown(activeItem, toggleItem) {
	  activeItem.on('click tap', function () {
	    toggleItem.toggleClass(active);
	  });
	};
	
	/**
	 * Remove active class if click outside the element.
	 *
	 * @param {jQuery} activeItem - Default click event handler to active item.
	 * @param {jQuery} toggleItem - Toggling object.
	 * @param {String} className
	 */
	var hideOnBodyClick = exports.hideOnBodyClick = function hideOnBodyClick(activeItem, toggleItem, className) {
	  var classNameToRemove = '';
	
	  typeof className === 'string' ? classNameToRemove = className : classNameToRemove = active;
	
	  body.on('click tap', function (e) {
	    if (!checkClosest(e, toggleItem) && toggleItem.hasClass(classNameToRemove) && !checkClosest(e, activeItem)) {
	      toggleItem.removeClass(classNameToRemove);
	    }
	  });
	};
	
	/**
	 * Remove specified class name if clicked anywhere outside the specified container.
	 *
	 * @param {jQuery} element - Container.
	 * @param {jQuery} elementToRemoveClass - Remove class from this element.
	 * @param {String} className - Class name to remove.
	 */
	var removeClassOnBodyClick = exports.removeClassOnBodyClick = function removeClassOnBodyClick(element) {
	  var elementToRemoveClass = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : element;
	  var className = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'incorrect';
	
	  body.on('click tap', function (e) {
	    var clickIsOutsideTheElement = !$(e.target).closest(element).length > 0;
	
	    if (clickIsOutsideTheElement) elementToRemoveClass.removeClass(className);
	  });
	};
	
	/**
	 * Makes default dropdown object with event handlers.
	 *
	 * @param {jQuery} activeItem - Default click event handler to active item.
	 * @param {jQuery} toggleItem - Toggling object.
	 */
	var dropdownItemInit = exports.dropdownItemInit = function dropdownItemInit(activeItem, toggleItem) {
	  toggleDropdown(activeItem, toggleItem);
	  hideOnBodyClick(activeItem, toggleItem);
	};
	
	/**
	 * Select item from list: make it active ( while disabling others ), change selected item text.
	 *
	 * @param {jQuery} item - Clicked item.
	 * @param {jQuery} selected - Selected item's text keeper.
	 * @param {jQuery} list - List of items.
	 * @param {String} customEventName - listen for this event
	 */
	var selectOneFromListInit = exports.selectOneFromListInit = function selectOneFromListInit(item, selected, list) {
	  var customEventName = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : '';
	
	  item.on('click tap ' + customEventName, function () {
	    var $this = $(this);
	
	    selected.text($this.text());
	
	    item.removeClass(active);
	    $this.addClass(active);
	
	    (typeof list === 'undefined' ? 'undefined' : _typeof(list)) === 'object' ? list.removeClass(active) : null;
	  });
	};
	
	/**
	 * Reduce the set of checkbox elements to checked ones.
	 *
	 * @param {jQuery} checkboxes - list of items.
	 */
	var getChecked = exports.getChecked = function getChecked(checkboxes) {
	  return $.map(checkboxes, function (checkbox) {
	    if ($(checkbox).prop('checked')) return checkbox;
	  });
	};
	
	/**
	 * Get length of checked elements from list.
	 *
	 * @param {jQuery} checkboxes - list of items.
	 */
	var getCheckedLength = exports.getCheckedLength = function getCheckedLength(checkboxes) {
	  return getChecked(checkboxes).length;
	};
	
	/**
	 * Send AJAX with specified callback.
	 *
	 * @param {String} url
	 * @param {Object|Array|String} data
	 * @param {Function} callback
	 * @param {Object} [context=null] - callback is called within this context
	 * @param {String} [method=GET]
	 */
	var sendJsonAjaxWithCallback = exports.sendJsonAjaxWithCallback = function sendJsonAjaxWithCallback(url, data, callback) {
	  var context = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
	  var method = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : 'GET';
	
	  $.ajax({
	    url: url,
	    method: method,
	    dataType: 'json',
	    cache: false,
	    data: data,
	    success: function success(res) {
	      console.log('DATA: ' + JSON.stringify(data) + ' \n Response: ' + JSON.stringify(res));
	      callback.call(context, res);
	    }
	  });
	};
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($) {'use strict';
	
	/**
	 * @name Validator.js
	 * @version 0.1
	 * @author Vitali Shapovalov
	 *
	 * @fileoverview
	 * This modules is used for forms validation.
	 * Text fields, emails, phones, checkobxes etc.
	 */
	
	/*
	 * Licensed under the Apache License, Version 2.0 (the "License");
	 * you may not use this file except in compliance with the License.
	 * You may obtain a copy of the License at
	 *
	 *     http://www.apache.org/licenses/LICENSE-2.0
	 *
	 * Unless required by applicable law or agreed to in writing, software
	 * distributed under the License is distributed on an "AS IS" BASIS,
	 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	 * See the License for the specific language governing permissions and
	 * limitations under the License.
	 */
	
	/**
	 * Validator.js module
	 *
	 * @constructor
	 *
	 * @param {jQuery} form - form to validate
	 * @param {Function} ajaxOptions - function that should return AJAX request options
	 * @param {Object} [options] - user specified options
	 * @param {Boolean} [options.nestedInModal=false] - when true, remove fields incorrect state on modal hide
	 * @param {String} [options.fieldsSelector='.form-input'] - form's field selector string
	 */
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var Validator = function () {
	  function Validator() {
	    var form = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : $();
	
	    var _this = this;
	
	    var ajaxOptions = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
	    var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
	
	    _classCallCheck(this, Validator);
	
	    /** Form to validate */
	    this.form = form;
	
	    /** User-specified AJAX options */
	    this.ajaxOptions = ajaxOptions;
	
	    /** User-specified options */
	    this.options = {
	      modal: options.nestedInModal || false,
	      fieldsSelector: options.fieldsSelector || '.form-input'
	    };
	
	    /** Default options */
	    this.default = {
	      // SELECTORS
	      incorrectFields: '.incorrect',
	      error: '.error',
	
	      // CLASS NAMES
	      incorrectClass: 'incorrect',
	      formIsValidClass: 'validated',
	
	      // ERROR MESSAGES TEXT
	      fieldEmptyText: 'Заполните поле',
	      incorrectPhoneText: 'Введите корректный номер',
	      incorrectEmailText: 'Введите корректный Email',
	
	      // PARAM NAME FOR SETTING CUSTOM ERROR MESSAGES
	      textDataName: 'validation-text',
	
	      // 'data-validation-*' TYPES
	      dataCondition: 'validation-condition',
	      dataType: 'validation-type',
	
	      // FIELD VALIDATION TYPES
	      textType: 'text',
	      phoneType: 'phone',
	      emailType: 'email',
	      checkboxType: 'checkbox',
	      radioType: 'radio',
	
	      // 'data-validation-condition' VALUE TYPES
	      minLengthDataName: 'min-length',
	      lengthDataName: 'length',
	
	      // 'data-validation' VALUE TYPES
	      requiredToValidate: 'required'
	    };
	
	    /** DOM elements */
	    this.elements = {
	      // DYNAMIC ELEMENTS SELECTORS
	      inputs: function inputs() {
	        return _this.form.find('input, textarea, select');
	      },
	      fields: function fields() {
	        return _this.form.find(_this.options.fieldsSelector);
	      },
	
	      // STATIC ELEMENTS SELECTORS
	      modal: this.form.parents('.modal'),
	      button: this.form.find('.validate-form-button')
	    };
	
	    /** Buffer object */
	    this.buffer = {};
	
	    /** Initialize */
	    this.init();
	  }
	
	  /**
	   * Check form for validness.
	   *
	   * @return {Boolean}
	   */
	
	
	  _createClass(Validator, [{
	    key: 'formIsValid',
	    value: function formIsValid() {
	      var incorrectFields = this.form.find(this.default.incorrectFields);
	
	      return incorrectFields.length === 0 && this.form.hasClass(this.default.formIsValidClass);
	    }
	
	    /**
	     * Validates an email.
	     *
	     * @static
	     *
	     * @param {String} email
	     * @return {Boolean}
	     */
	
	  }, {
	    key: 'removeIncorrectState',
	
	
	    /**
	     * Remove incorrect state from all fields.
	     *
	     * @return {Validator}
	     */
	    value: function removeIncorrectState() {
	      this.form.find(this.options.fieldsSelector).removeClass(this.default.incorrectClass);
	
	      return this;
	    }
	
	    /**
	     * Reset form to default state.
	     *
	     * @return {Validator}
	     */
	
	  }, {
	    key: 'resetForm',
	    value: function resetForm() {
	      this.form[0].reset();
	
	      return this;
	    }
	
	    /**
	     * Remove incorrect state from all fields when modal is closed.
	     *
	     * @return {Validator}
	     */
	
	  }, {
	    key: 'removeIncorrectStateOnModalClose',
	    value: function removeIncorrectStateOnModalClose() {
	      var _this2 = this;
	
	      this.elements.modal.on('hidden.bs.modal', function () {
	        return _this2.removeIncorrectState();
	      });
	
	      return this;
	    }
	
	    /**
	     * Set incorrect state on field.
	     *
	     * @param {jQuery} field
	     * @param {String} errorText - displayed error text
	     */
	
	  }, {
	    key: 'throwError',
	    value: function throwError(field, errorText) {
	      field.addClass(this.default.incorrectClass).find(this.default.error).text(errorText);
	    }
	
	    /**
	     * Check field for validness and set valid/incorrect state.
	     *
	     * @param {jQuery} field
	     * @param {Number} valueLength
	     * @param {String} errorText
	     * @param {Boolean} condition - condition to set valid state
	     */
	
	  }, {
	    key: 'checkFieldValidness',
	    value: function checkFieldValidness(field, condition, errorText, valueLength) {
	      var dataText = field.data(this.default.textDataName);
	
	      if (dataText && dataText.length) errorText = dataText;
	
	      if (!valueLength) {
	        this.throwError(field, this.default.fieldEmptyText);
	      } else if (!condition) {
	        this.throwError(field, errorText);
	      } else {
	        this.form.addClass(this.default.formIsValidClass);
	      }
	    }
	
	    /**
	     * Validates field.
	     *
	     * @param {jQuery} field
	     */
	
	  }, {
	    key: 'validateField',
	    value: function validateField(field) {
	      var type = field.data(this.default.dataType);
	      var fieldParams = {
	        condition: true,
	        errorText: this.default.fieldEmptyText,
	        length: 1
	      };
	
	      switch (type) {
	        case this.default.textType:
	          {
	            fieldParams = this.validateTextField(field);
	            break;
	          }
	        case this.default.phoneType:
	          {
	            fieldParams = this.validateTextField(field);
	            fieldParams.errorText = this.default.incorrectPhoneText;
	            break;
	          }
	        case this.default.emailType:
	          {
	            fieldParams = this.validateEmailField(field);
	            fieldParams.errorText = this.default.incorrectEmailText;
	            break;
	          }
	        case this.default.radioType:
	          {
	            fieldParams.condition = this.validateRadioField(field);
	            break;
	          }
	      }
	
	      this.checkFieldValidness(field, fieldParams.condition, fieldParams.errorText, fieldParams.length);
	    }
	
	    /**
	     * Validate 'Text' field.
	     *
	     * @param {jQuery} field
	     * @return {Object}
	     */
	
	  }, {
	    key: 'validateTextField',
	    value: function validateTextField(field) {
	      var input = field.find('input').length ? field.find('input') : field.find('textarea');
	      var value = input.val();
	      var valueLength = value.length;
	
	      var conditionType = input.data(this.default.dataCondition);
	      var condition = void 0,
	          errorText = void 0;
	
	      switch (conditionType) {
	        case this.default.minLengthDataName:
	          {
	            var minLength = input.data(this.default.minLengthDataName);
	
	            condition = valueLength >= parseInt(minLength, 10);
	            errorText = '\u041C\u0438\u043D\u0438\u043C\u0430\u043B\u044C\u043D\u0430\u044F \u0434\u043B\u0438\u043D\u043D\u0430 \u043F\u043E\u043B\u044F - ' + minLength + ' \u0441\u0438\u043C\u0432\u043E\u043B\u0430';
	
	            break;
	          }
	        case this.default.lengthDataName:
	          {
	            var neededLength = input.data(this.default.lengthDataName);
	
	            condition = valueLength === parseInt(neededLength, 10);
	            errorText = '\u041D\u0435\u043E\u0431\u0445\u043E\u0434\u0438\u043C\u0430\u044F \u0434\u043B\u0438\u043D\u043D\u0430 \u043F\u043E\u043B\u044F - ' + neededLength + ' \u0441\u0438\u043C\u0432\u043E\u043B\u043E\u0432';
	
	            break;
	          }
	      }
	
	      return {
	        condition: condition,
	        errorText: errorText,
	        length: valueLength
	      };
	    }
	
	    /**
	     * Validate 'Email' field.
	     *
	     * @param {jQuery} field
	     */
	
	  }, {
	    key: 'validateEmailField',
	    value: function validateEmailField(field) {
	      var value = field.find('input').val();
	
	      return {
	        condition: Validator.validateEmail(value),
	        length: value.length
	      };
	    }
	
	    /**
	     * Validate 'Radio' field.
	     *
	     * @param {jQuery} field
	     */
	
	  }, {
	    key: 'validateRadioField',
	    value: function validateRadioField(field) {
	      var checkedVisibleRadio = field.find('input[type="radio"]:checked:visible');
	
	      return checkedVisibleRadio.length >= 1;
	    }
	
	    /**
	     * Serialize form.
	     *
	     * @return {Array} serialized form data
	     */
	
	  }, {
	    key: 'serializedFormData',
	    value: function serializedFormData() {
	      return this.form.serialize();
	    }
	
	    /**
	     * Send data if form is valid.
	     *
	     * @param {Object|Function} options - ajax options
	     * @return {Validator}
	     */
	
	  }, {
	    key: 'sendIfValidated',
	    value: function sendIfValidated() {
	      var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.ajaxOptions;
	
	      if (this.formIsValid()) {
	        $.ajax(options.call(null, this));
	      }
	
	      return this;
	    }
	
	    /**
	     * Validate form fields.
	     *
	     * @return {Validator}
	     */
	
	  }, {
	    key: 'validateAllFields',
	    value: function validateAllFields() {
	      var _this3 = this;
	
	      this.elements.fields().each(function (index, field) {
	        var $field = $(field);
	        var requiredToValidate = $field.data('validation') === _this3.default.requiredToValidate;
	
	        if (requiredToValidate) _this3.validateField($field);
	      });
	
	      return this;
	    }
	
	    /**
	     * Validate form.
	     *
	     * @return {Validator}
	     */
	
	  }, {
	    key: 'runFormValidation',
	    value: function runFormValidation() {
	      this.removeIncorrectState().validateAllFields().sendIfValidated();
	
	      return this;
	    }
	
	    /**
	     * Initialize on-click validation.
	     *
	     * @return {Validator}
	     */
	
	  }, {
	    key: 'bindOnClickValidation',
	    value: function bindOnClickValidation() {
	      var _this4 = this;
	
	      this.elements.button.on('click.validation tap.validation', function (e) {
	        e.preventDefault();
	
	        _this4.runFormValidation();
	      });
	
	      return this;
	    }
	
	    /**
	     * Unbind on-click event.
	     *
	     * @return {Validator}
	     */
	
	  }, {
	    key: 'unbindOnClick',
	    value: function unbindOnClick() {
	      this.elements.button.unbind('click.validation tap.validation');
	
	      return this;
	    }
	
	    /**
	     * Initialize all validation scripts.
	     */
	
	  }, {
	    key: 'init',
	    value: function init() {
	      this.bindOnClickValidation();
	
	      if (this.options.modal) this.removeIncorrectStateOnModalClose();
	    }
	  }], [{
	    key: 'validateEmail',
	    value: function validateEmail(email) {
	      var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
	
	      return pattern.test(email);
	    }
	  }]);
	
	  return Validator;
	}();
	
	exports.default = Validator;
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($) {'use strict';
	
	/**
	 * Footer's elements scripts.
	 *
	 * @module Footer
	 */
	
	/** Import utility functions */
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	/** Import Validator class */
	
	
	var _Helpers = __webpack_require__(6);
	
	var _Validator2 = __webpack_require__(7);
	
	var _Validator3 = _interopRequireDefault(_Validator2);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	/** Class representing a website's footer functions. Initialized by default. */
	var Footer = function () {
	
	  /**
	   * Cache footer's DOM elements for instant access and generate options for owlCarousel.
	   */
	  function Footer() {
	    _classCallCheck(this, Footer);
	
	    /** AJAX URLs */
	    this.subscribeUrl = './ajax/subscribe-footer.json';
	    this.issueRequestUrl = './ajax/success.json';
	
	    /** AJAX event names */
	    this.subscribeEventName = 'subscribeFooter';
	    this.issueRequestEventName = 'issueRequest';
	
	    /** Other */
	    this.footerInfo = $('.footer-info');
	    this.footer_toggling_item = $('.footer-links > h5:not(.app-links-header)');
	    this.footer_up_button = $('.footer-up-button');
	    this.issueRequestForm = $('.modal .form-issue');
	
	    /** Subscribe */
	    this.subscribeButton = $('#footer_subscribe');
	    this.subscribeInput = $('#footer_subscribe_input');
	    this.subscribeFormContainer = $('.footer-subscribe__form');
	
	    /** Slider */
	    this.sliderOptions = {
	      margin: 0,
	      navText: [_Helpers.buttonLeftTemplate, _Helpers.buttonRightTemplate],
	      responsive: {
	        0: {
	          loop: true,
	          nav: true,
	          items: 1
	        },
	        600: {
	          loop: false,
	          nav: true,
	          items: 2
	        },
	        1024: {
	          items: 3,
	          nav: true
	        }
	      }
	    };
	  }
	
	  /**
	   * Initialize footer's links dropDown trigger (on mobile devices only).
	   *
	   * @return {Footer}
	   */
	
	
	  _createClass(Footer, [{
	    key: 'dropdownLinksInit',
	    value: function dropdownLinksInit() {
	      this.footer_toggling_item.on('click tap', function () {
	        var $this = $(this);
	        var container = $this.parent();
	
	        container.toggleClass(_Helpers.active);
	      });
	
	      return this;
	    }
	
	    /**
	     * Initialize 'Up' button.
	     *
	     * @return {Footer}
	     */
	
	  }, {
	    key: 'upButtonInit',
	    value: function upButtonInit() {
	      this.footer_up_button.on('click tap', function () {
	        $('body, html').animate({ scrollTop: 0 }, 800);
	      });
	
	      return this;
	    }
	
	    /**
	     * Initialize owlCarousel.
	     *
	     * @return {Footer}
	     */
	
	  }, {
	    key: 'sliderInit',
	    value: function sliderInit() {
	      this.footerInfo.owlCarousel(this.sliderOptions);
	      return this;
	    }
	
	    /**
	     * Initialize subscribers email validation and data send.
	     *
	     * @return {Footer}
	     */
	
	  }, {
	    key: 'validateSubscribeEmail',
	    value: function validateSubscribeEmail() {
	      var url = this.subscribeUrl;
	      var eventName = this.subscribeEventName;
	
	      var validateSubscribe = new _Validator3.default(this.subscribeFormContainer, function (context) {
	        var data = {
	          event: eventName,
	          email: context.elements.inputs().val()
	        };
	
	        return {
	          url: url,
	          method: 'GET',
	          cache: false,
	          dataType: 'json',
	          data: data,
	          success: function success(res) {
	            console.log('DATA: ' + JSON.stringify(data));
	            if (res.response === 'success') {
	              var textToRemove = $('.hide-on-subscribe-success');
	
	              textToRemove.animate({ opacity: 0 }, 500, function () {
	                $(this).remove();
	              });
	
	              context.form.animate({ opacity: 0 }, 500, function () {
	                $(this).html(res.message).animate({ opacity: 1 }, 500);
	              });
	            } else {
	              console.log('subscribe failed');
	            }
	          }
	        };
	      });
	
	      // remove incorrect style if clicked anywhere outside the container
	      (0, _Helpers.removeClassOnBodyClick)(validateSubscribe.form, validateSubscribe.elements.fields());
	
	      return this;
	    }
	
	    /**
	     * Validate and send data from Issue request from.
	     *
	     * @return {Footer}
	     */
	
	  }, {
	    key: 'validateIssueForm',
	    value: function validateIssueForm() {
	      var url = this.issueRequestUrl;
	      var eventName = this.issueRequestEventName;
	      var options = {
	        nestedInModal: true
	      };
	
	      // Custom class for issue form validation
	
	      var ValidateIssue = function (_Validator) {
	        _inherits(ValidateIssue, _Validator);
	
	        function ValidateIssue() {
	          _classCallCheck(this, ValidateIssue);
	
	          return _possibleConstructorReturn(this, (ValidateIssue.__proto__ || Object.getPrototypeOf(ValidateIssue)).apply(this, arguments));
	        }
	
	        _createClass(ValidateIssue, [{
	          key: 'unbindOnClick',
	          value: function unbindOnClick() {
	            var _this2 = this;
	
	            _get(ValidateIssue.prototype.__proto__ || Object.getPrototypeOf(ValidateIssue.prototype), 'unbindOnClick', this).call(this);
	
	            var button = this.elements.button;
	            var modal = this.elements.modal;
	
	            // set action button to close modal
	            button.on('click tap', function (e) {
	              e.preventDefault();
	
	              modal.modal('hide');
	            });
	
	            // return modal to default state when closed
	            modal.on('hidden.bs.modal', function () {
	              var title = modal.find('.modal-title');
	              var subtitle = modal.find('.modal-subtitle');
	
	              _this2.resetForm();
	
	              title.text(_this2.buffer.titleText);
	              subtitle.text(_this2.buffer.subtitleText);
	              button.text(_this2.buffer.buttonText);
	
	              modal.removeClass(_this2.default.formIsValidClass);
	
	              button.unbind('click tap');
	
	              _this2.bindOnClickValidation();
	            });
	
	            return this;
	          }
	        }]);
	
	        return ValidateIssue;
	      }(_Validator3.default);
	
	      var validateIssue = new ValidateIssue(this.issueRequestForm, function (context) {
	        var data = {
	          data: context.serializedFormData(),
	          event: eventName
	        };
	
	        return {
	          url: url,
	          method: 'POST',
	          cache: false,
	          dataType: 'json',
	          data: data,
	          success: function success(res) {
	            console.log('DATA: ' + JSON.stringify(data));
	            if (res.response === 'success') {
	              var modal = context.elements.modal;
	              var button = context.elements.button;
	
	              var title = modal.find('.modal-title');
	              var subtitle = modal.find('.modal-subtitle');
	
	              // save data in buffer obj
	              context.buffer = {
	                titleText: title.text(),
	                subtitleText: subtitle.text(),
	                buttonText: button.text()
	              };
	
	              // change text
	              title.text('Ваше сообщено отправлено');
	              subtitle.text('Вскоре с Вами свяжется наш сотрудник. Спасибо, что помогаете нам стать лучше.');
	              button.text('Закрыть');
	
	              // change state
	              modal.addClass(context.default.formIsValidClass);
	
	              // unbind action button
	              context.unbindOnClick();
	            } else {
	              console.log('issue request failed');
	            }
	          }
	        };
	      }, options);
	
	      return this;
	    }
	
	    /**
	     * Initialize Footer scripts.
	     *
	     * @static
	     */
	
	  }], [{
	    key: 'init',
	    value: function init() {
	      var obj = new this();
	
	      obj.upButtonInit().validateSubscribeEmail().validateIssueForm();
	
	      // only on mobile devices
	      if (_Helpers.screenWidth <= _Helpers.tabletLandscapeBreakpoint) {
	        obj.dropdownLinksInit().sliderInit();
	      }
	    }
	  }]);
	
	  return Footer;
	}();
	
	exports.default = Footer;
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($) {'use strict';
	
	/**
	 * Menu scripts.
	 *
	 * @module Menu
	 */
	
	/** Import utility functions */
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _Helpers = __webpack_require__(6);
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	/** Class representing a website's main menu functions. Initialized by default. */
	var Menu = function () {
	
	  /**
	   * Cache main menu's DOM elements for instant access.
	   */
	  function Menu() {
	    _classCallCheck(this, Menu);
	
	    this.menu = $('#main-menu');
	    this.menu_toggle = $('#header__menu-toggle');
	    this.menu_close = $('.category-thirdLevel-title > svg');
	    this.menu_bg = $('.menu-backface');
	    this.menu_back_cat = $('.category-secondLevel-back');
	    this.menu_firstLevel = $('#main-menu .category');
	    this.menu_secondLevel = $('.category-secondLevel');
	    this.menu_thirdLevelCont = $('.category-thirdLevel-title');
	    this.thirdLevelTrigger = $('.category-secondLevel > span');
	  }
	
	  /**
	   * Get the state.
	   *
	   * @static
	   * @return {Object} Contains class names representing element's state.
	   */
	
	
	  _createClass(Menu, [{
	    key: 'secondLevelHrefInit',
	
	
	    /**
	     * Relocate to second category on click init. (desktop)
	     *
	     * @return {Menu}
	     */
	    value: function secondLevelHrefInit() {
	      // polyfill
	      if (!location.origin) location.origin = location.protocol + "//" + location.host;
	
	      this.menu_secondLevel.on('click tap', function () {
	        if ((0, _Helpers.screenWidthDynamically)() > 1280) {
	          var link = $(this).attr('data-href');
	          location.href = location.origin + '/' + link;
	        }
	      });
	
	      return this;
	    }
	
	    /**
	     * Close menu and all of sub menus.
	     *
	     * @return {Menu}
	     */
	
	  }, {
	    key: 'closeMenu',
	    value: function closeMenu() {
	      _Helpers.body.removeClass(Menu.states.menuOpened);
	      Menu.closeSubMenu.all();
	
	      return this;
	    }
	
	    /**
	     * Hide menu when clicked outside main menu.
	     *
	     * @return {Menu}
	     */
	
	  }, {
	    key: 'closeMenuOnBodyClick',
	    value: function closeMenuOnBodyClick() {
	      var _this2 = this;
	
	      _Helpers.body.on('click tap', function (e) {
	        if (!(0, _Helpers.checkClosest)(e, _this2.menu) && !(0, _Helpers.checkClosest)(e, _this2.menu_toggle)) {
	          _this2.closeMenu();
	        }
	      });
	
	      return this;
	    }
	
	    /**
	     * Close menu when clicked on black overlay.
	     *
	     * @return {Menu}
	     */
	
	  }, {
	    key: 'blackOverlayClickHandler',
	    value: function blackOverlayClickHandler() {
	      var _this3 = this;
	
	      this.menu_bg.on('click tap', function () {
	        _this3.closeMenu();
	      });
	
	      return this;
	    }
	
	    /**
	     * Initialize open/close menu trigger.
	     *
	     * @return {Menu}
	     */
	
	  }, {
	    key: 'toggleMenuOnClick',
	    value: function toggleMenuOnClick() {
	      var menuOpened = Menu.states.menuOpened;
	      var menuTrigger = this.menu_toggle;
	
	      // current state
	      var bound = false;
	
	      function bindMenu() {
	        if (!bound) {
	          menuTrigger.on('click.trig tap.trig', function () {
	            if (_Helpers.body.hasClass(menuOpened)) {
	              Menu.closeSubMenu.all();
	            }
	            _Helpers.body.toggleClass(menuOpened);
	          });
	
	          bound = true;
	        }
	      }
	
	      function unbindMenu() {
	        if (bound) {
	          menuTrigger.off('click.trig tap.trig');
	
	          bound = false;
	        }
	      }
	
	      if (_Helpers.menuIsAlwaysHidden) {
	        bindMenu();
	      } else {
	        var checkAndChangeHandler = function checkAndChangeHandler() {
	          var widthToActivate = (0, _Helpers.screenWidthDynamically)() < _Helpers.tabletLandscapeBreakpoint;
	
	          if (widthToActivate) {
	            bindMenu();
	          } else {
	            unbindMenu();
	          }
	        };
	        checkAndChangeHandler();
	
	        $(window).on('resize orientationchange', checkAndChangeHandler);
	      }
	
	      return this;
	    }
	
	    /**
	     * Initialize 'Return' and 'Close' buttons in sub menus.
	     *
	     * @return {Menu}
	     */
	
	  }, {
	    key: 'bindBackButtons',
	    value: function bindBackButtons() {
	      this.menu_back_cat.on('click tap', function () {
	        Menu.closeSubMenu.first();
	      });
	
	      this.menu_close.on('click tap', function () {
	        Menu.closeSubMenu.second();
	      });
	
	      return this;
	    }
	
	    /**
	     * Open sub menu.
	     *
	     * @static
	     * @return {Object} open specified sub menu;
	     */
	
	  }, {
	    key: 'openSubCatMenuInit',
	
	
	    /**
	     * Open sub menu when clicked on sub menu trigger.
	     *
	     * @return {Menu}
	     */
	    value: function openSubCatMenuInit() {
	      var _this = this;
	      var subCatMenuTrigger = _this.menu_firstLevel;
	
	      subCatMenuTrigger.on('click tap', function (e) {
	        var $this = $(this);
	        var notBackToCat = !(0, _Helpers.checkClosest)(e, _this.menu_back_cat);
	        var notSubSubCat = !(0, _Helpers.checkClosest)(e, _this.menu_secondLevel);
	
	        if (notSubSubCat && notBackToCat) {
	          if ($this.hasClass(_Helpers.active)) {
	            Menu.closeSubMenu.all();
	          } else {
	            Menu.closeSubMenu.all();
	            Menu.openSubMenu.first($this);
	          }
	        }
	      });
	
	      return this;
	    }
	
	    /**
	     * Open sub sub menu when clicked on sub sub menu trigger.
	     *
	     * @return {Menu}
	     */
	
	  }, {
	    key: 'openSubSubCatMenuInit',
	    value: function openSubSubCatMenuInit() {
	      var _this = this;
	      var subSubCatMenuTrigger = _this.menu_secondLevel;
	
	      subSubCatMenuTrigger.on('click tap', function (e) {
	        var $this = $(this);
	        var notSubSubCat = !(0, _Helpers.checkClosest)(e, _this.menu_thirdLevelCont);
	
	        if (notSubSubCat) {
	          if ($this.hasClass(_Helpers.active)) {
	            Menu.closeSubMenu.second();
	          } else {
	            Menu.closeSubMenu.second();
	            Menu.openSubMenu.second($this);
	          }
	        }
	      });
	
	      return this;
	    }
	
	    /**
	     * Show sub sub menu when hovered on sub sub menu trigger ( > 1280px screens ).
	     *
	     * @return {Menu}
	     */
	
	  }, {
	    key: 'subSubMenuOnHoverInit',
	    value: function subSubMenuOnHoverInit() {
	      this.thirdLevelTrigger.on('mouseover', function () {
	        var $this = $(this);
	        var thisSecondLevelMenu = $this.parent();
	
	        Menu.closeSubMenu.second();
	        Menu.openSubMenu.second(thisSecondLevelMenu);
	      });
	
	      return this;
	    }
	
	    /**
	     * Initialize Menu scripts.
	     *
	     * @static
	     */
	
	  }], [{
	    key: 'init',
	    value: function init() {
	      /** Make overflow: visible when menu opened. */
	      if (_Helpers.menuIsAlwaysHidden) _Helpers.body.addClass('visible');
	
	      var obj = new this();
	
	      obj.openSubCatMenuInit().toggleMenuOnClick().secondLevelHrefInit();
	
	      if (_Helpers.screenWidth > _Helpers.tabletLandscapeBreakpoint) {
	        obj.subSubMenuOnHoverInit();
	      }
	
	      if (_Helpers.screenWidth <= _Helpers.tabletLandscapeBreakpoint || _Helpers.menuIsAlwaysHidden) {
	        obj.openSubSubCatMenuInit().bindBackButtons().blackOverlayClickHandler();
	      }
	
	      if (_Helpers.screenWidth > 600) {
	        obj.closeMenuOnBodyClick();
	      }
	    }
	  }, {
	    key: 'states',
	    get: function get() {
	      return {
	        menuOpened: 'menu-opened',
	        subCatOpened: 'menu-2',
	        subSubCatOpened: 'menu-3'
	      };
	    }
	
	    /**
	     * Get main menu's sub menu object.
	     *
	     * @static
	     * @return {Object} Contains jQuery objects - sub menu containers.
	     */
	
	  }, {
	    key: 'subMenu',
	    get: function get() {
	      var obj = new this();
	
	      return {
	        first: obj.menu_firstLevel,
	        second: obj.menu_secondLevel
	      };
	    }
	  }, {
	    key: 'openSubMenu',
	    get: function get() {
	      return {
	        first: function first(subCategory) {
	          subCategory.addClass(_Helpers.active);
	          _Helpers.body.addClass(Menu.states.subCatOpened);
	        },
	        second: function second(subSubCategory) {
	          subSubCategory.addClass(_Helpers.active);
	          _Helpers.body.addClass(Menu.states.subSubCatOpened);
	        }
	      };
	    }
	
	    /**
	     * Close sub menu.
	     *
	     * @static
	     * @return {Object} close specified sub menu;
	     */
	
	  }, {
	    key: 'closeSubMenu',
	    get: function get() {
	      var subCatOpened = this.states.subCatOpened;
	      var subSubCatOpened = this.states.subSubCatOpened;
	      var subMenu = this.subMenu.first;
	      var subSubMenu = this.subMenu.second;
	
	      return {
	        first: function first() {
	          _Helpers.body.removeClass(subCatOpened);
	          subMenu.removeClass(_Helpers.active);
	        },
	        second: function second() {
	          _Helpers.body.removeClass(subSubCatOpened);
	          subSubMenu.removeClass(_Helpers.active);
	        },
	        all: function all() {
	          _Helpers.body.removeClass(subCatOpened + ' ' + subSubCatOpened);
	          subMenu.removeClass(_Helpers.active);
	          subSubMenu.removeClass(_Helpers.active);
	        }
	      };
	    }
	  }]);
	
	  return Menu;
	}();
	
	exports.default = Menu;
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($) {'use strict';
	
	/**
	 * Breadcrumbs, issue fixing, input mask, get current products in cart.
	 *
	 * @module Common
	 */
	
	/** Import utility functions */
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	/** Import Cart controller. */
	
	
	/** Import masked input plugin. */
	
	
	var _Helpers = __webpack_require__(6);
	
	var _CartController = __webpack_require__(11);
	
	var _CartController2 = _interopRequireDefault(_CartController);
	
	__webpack_require__(12);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	/** Class representing common functions. Initialized by default. */
	var Common = function () {
	
	  /**
	   * Cache DOM elements.
	   */
	  function Common() {
	    _classCallCheck(this, Common);
	
	    this.breadcrumbItems = $('.breadcrumb-item');
	
	    this.phoneInputs = $('input[type="tel"]');
	
	    /** Class-indicators names */
	    this.iphoneFixingClassName = 'iphone4fix';
	  }
	
	  /**
	   * Breadcrumbs dropdown.
	   *
	   * @return {Common}
	   */
	
	
	  _createClass(Common, [{
	    key: 'breadcrumbsDropdownsInit',
	    value: function breadcrumbsDropdownsInit() {
	      this.breadcrumbItems.each(function (index, element) {
	        var breadcrumb = $(element);
	        var breadcrumbToggle = breadcrumb.find('.breadcrumb-title');
	
	        (0, _Helpers.dropdownItemInit)(breadcrumbToggle, breadcrumb);
	      });
	
	      return this;
	    }
	
	    /**
	     * Mask phone inputs.
	     *
	     * @return {Common}
	     */
	
	  }, {
	    key: 'initPhoneMasks',
	    value: function initPhoneMasks() {
	      this.phoneInputs.mask("+7 (999) 999-99-99");
	
	      return this;
	    }
	
	    /**
	     * iPhone 4 scroll issue (main menu) fix.
	     *
	     * @return {Common}
	     */
	
	  }, {
	    key: 'initIphone4IssueFix',
	    value: function initIphone4IssueFix() {
	      var checkHeightAndFix = function () {
	        var isIphone4 = window.screen.height == 480;
	
	        if (isIphone4) _Helpers.body.addClass(this.iphoneFixingClassName);
	      }.bind(this);
	
	      $(window).on('resize orientationchange load', checkHeightAndFix);
	
	      return this;
	    }
	
	    /**
	     * Cart update on each page reload.
	     *
	     * @return {Common}
	     */
	
	  }, {
	    key: 'initCart',
	    value: function initCart() {
	      var CartInstance = new _CartController2.default();
	      CartInstance.getCurrentProducts();
	
	      return this;
	    }
	
	    /**
	     * Initialize common scripts.
	     *
	     * @static
	     */
	
	  }], [{
	    key: 'init',
	    value: function init() {
	      var obj = new this();
	
	      obj.initCart().initPhoneMasks().breadcrumbsDropdownsInit().initIphone4IssueFix();
	    }
	  }]);
	
	  return Common;
	}();
	
	exports.default = Common;
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($) {'use strict';
	
	/**
	 * Cart functions (add to cart etc.)
	 *
	 * @module CartController
	 */
	
	/** Import utility functions */
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.CartItem = undefined;
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _Helpers = __webpack_require__(6);
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	/**
	 * @class CartController
	 * @classdesc Class representing a website's cart actions.
	 */
	var CartController = function () {
	  function CartController() {
	    _classCallCheck(this, CartController);
	
	    /** Cart */
	    this.cartCount = $('.header-cart .cart');
	    this.cartPrice = $('.header-cart .subtitle');
	    this.cartIsEmptyText = 'Ваша корзина пуста';
	
	    /** AJAX URLs */
	    this.cartUrl = './ajax/success-cart.json';
	    this.getItemsInCartUrl = './ajax/cart.json';
	
	    /** Class-indicators names */
	    this.purchasedClassName = 'added';
	    this.cartIsActiveClass = 'cart-active';
	
	    /** AJAX event names */
	    this.getItemsInCartEventName = 'getCart';
	
	    /** Other */
	    this.buttonStates = {
	      pending: 'pending',
	      completed: 'completed'
	    };
	
	    return this;
	  }
	
	  /**
	   * Get current count.
	   *
	   * @static
	   */
	
	
	  _createClass(CartController, [{
	    key: 'setButtonState',
	
	
	    /**
	     * Set button state (request pending / completed)
	     *
	     * @param {jQuery} product
	     * @param {String} state - state to set.
	     * @return {CartController}
	     */
	    value: function setButtonState(product, state) {
	      for (var _state in this.buttonStates) {
	        if (_Helpers.has.call(this.buttonStates, _state)) product.removeClass(_state);
	      }
	
	      product.addClass(state);
	    }
	
	    /**
	     * Set cart data.
	     *
	     * @param {Number} newAmount - elements in cart.
	     * @param {Number} newPrice - total price.
	     * @return {CartController}
	     */
	
	  }, {
	    key: 'setCartData',
	    value: function setCartData(newAmount, newPrice) {
	      CartController.currentCount = newAmount;
	      CartController.totalPrice = newPrice;
	
	      return this;
	    }
	
	    /**
	     * Change cart displayed data.
	     *
	     * @return {CartController}
	     */
	
	  }, {
	    key: 'refreshDisplayedCartData',
	    value: function refreshDisplayedCartData() {
	      this.cartCount.text(CartController.getCurrentCount);
	      this.cartPrice.text(CartController.getTotalPrice + ' \u0442');
	
	      var className = this.cartIsActiveClass;
	      if (+CartController.getCurrentCount > 0) {
	        !_Helpers.body.hasClass(className) ? _Helpers.body.addClass(className) : null;
	      } else {
	        _Helpers.body.removeClass(className);
	
	        this.cartPrice.text(this.cartIsEmptyText);
	      }
	
	      return this;
	    }
	
	    /**
	     * Set and change displayed cart data.
	     *
	     * @param {Number} newAmount - elements in cart.
	     * @param {Number} newPrice - total price.
	     * @return {CartController}
	     */
	
	  }, {
	    key: 'updateCartData',
	    value: function updateCartData(newPrice) {
	      var newAmount = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
	
	      var currentTotalPrice = +CartController.getTotalPrice;
	      var currentTotalCount = +CartController.getCurrentCount;
	
	      newAmount = currentTotalCount + newAmount;
	      newPrice = currentTotalPrice + newPrice;
	
	      this.setCartData(newAmount, newPrice).refreshDisplayedCartData();
	
	      return this;
	    }
	
	    /**
	     * Get current products in cart.
	     */
	
	  }, {
	    key: 'getCurrentProducts',
	    value: function getCurrentProducts() {
	      var _this2 = this;
	
	      var data = {
	        event: this.getItemsInCartEventName
	      };
	
	      $.ajax({
	        url: this.getItemsInCartUrl,
	        dataType: 'json',
	        method: 'GET',
	        data: data,
	        success: function success(res) {
	          var totalAmount = +res.totalAmount;
	          var totalPrice = +res.totalPrice;
	
	          if (totalAmount && totalPrice) {
	            _this2.setCartData(totalAmount, totalPrice).refreshDisplayedCartData();
	          }
	        }
	      });
	    }
	  }], [{
	    key: 'getCurrentCount',
	    get: function get() {
	      return localStorage.getItem('userProductsInCartCount');
	    }
	
	    /**
	     * Get total price.
	     *
	     * @static
	     */
	
	  }, {
	    key: 'getTotalPrice',
	    get: function get() {
	      return localStorage.getItem('userProductsInCartTotalPrice');
	    }
	
	    /**
	     * Set current count.
	     *
	     * @static
	     */
	
	  }, {
	    key: 'currentCount',
	    set: function set(newCurrentCount) {
	      localStorage.setItem('userProductsInCartCount', newCurrentCount);
	    }
	
	    /**
	     * Set total price.
	     *
	     * @static
	     */
	
	  }, {
	    key: 'totalPrice',
	    set: function set(newPrice) {
	      localStorage.setItem('userProductsInCartTotalPrice', newPrice);
	    }
	  }]);
	
	  return CartController;
	}();
	
	/**
	 * Cache cart's DOM elements and bind 'Buy' button.
	 *
	 * @class CartItem
	 * @classdesc Class used for adding products to cart.
	 * @extends CartController
	 *
	 * @param {String} addButton - this product's 'buy' button selector
	 * @param {String} removeButton - this product's 'remove' button selector
	 * @param {String} dataObject - object with product data (id, price etc.) selector
	 */
	
	
	exports.default = CartController;
	
	var CartItem = exports.CartItem = function (_CartController) {
	  _inherits(CartItem, _CartController);
	
	  function CartItem() {
	    var addButton = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
	    var removeButton = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
	    var dataObject = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '';
	
	    _classCallCheck(this, CartItem);
	
	    var _this3 = _possibleConstructorReturn(this, (CartItem.__proto__ || Object.getPrototypeOf(CartItem)).call(this));
	
	    _this3.buyButton = addButton;
	    _this3.dataObject = dataObject;
	    _this3.removeButton = removeButton;
	
	    _this3.buyEventName = 'buyCart';
	    _this3.removeEventName = 'removeCart';
	    return _this3;
	  }
	
	  /**
	   * Change product quantity (add/remove).
	   *
	   * @param {String} product - product object
	   * @param {String} event - Event name ('add' or 'remove').
	   * @return {CartItem}
	   */
	
	
	  _createClass(CartItem, [{
	    key: 'changeCartQuantity',
	    value: function changeCartQuantity(product, event) {
	      var _this4 = this;
	
	      product = $(product);
	      var id = product.data('id');
	      var price = product.data('price');
	      var purchasedClassName = this.purchasedClassName;
	      var data = {
	        event: event,
	        id: id,
	        price: price
	      };
	
	      var checkEvent = function (buyCB, removeCB) {
	        switch (event) {
	          case this.buyEventName:
	            {
	              buyCB.call(this);
	              product.addClass(purchasedClassName);
	              break;
	            }
	          case this.removeEventName:
	            {
	              removeCB.call(this);
	              product.removeClass(purchasedClassName);
	              break;
	            }
	        }
	      }.bind(this);
	
	      $.ajax({
	        url: this.cartUrl,
	        dataType: 'json',
	        method: 'POST',
	        cache: false,
	        data: data,
	        beforeSend: function beforeSend() {
	          _this4.setButtonState(product, _this4.buttonStates.pending);
	        },
	        success: function success(res) {
	          console.log('DATA:\n' + JSON.stringify(data));
	          var response = res.response;
	
	          if (response === 'success') {
	            checkEvent(function () {
	              _this4.updateCartData(+price);
	            }, function () {
	              _this4.updateCartData(-+price, -1);
	            });
	          } else if (response === 'change') {
	            _this4.setCartData(res.basket.amount, res.basket.price).refreshDisplayedCartData();
	          }
	        },
	        complete: function complete() {
	          _this4.setButtonState(product, _this4.buttonStates.completed);
	        }
	      });
	
	      return this;
	    }
	
	    /**
	     * Bind action buttons.
	     */
	
	  }, {
	    key: 'bind',
	    value: function bind() {
	      var _this = this;
	
	      function bindButton(type, button) {
	        _Helpers.body.on('click tap', button, function () {
	          var dataObject = void 0;
	
	          if (!_this.dataObject) {
	            dataObject = $(this).parents('.default-item');
	          } else {
	            dataObject = _this.dataObject;
	          }
	
	          _this.changeCartQuantity(dataObject, type);
	        });
	      }
	
	      bindButton(this.buyEventName, this.buyButton);
	      bindButton(this.removeEventName, this.removeButton);
	    }
	  }]);
	
	  return CartItem;
	}(CartController);
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;(function (factory) {
	    if (true) {
	        // AMD. Register as an anonymous module.
	        !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(2)], __WEBPACK_AMD_DEFINE_FACTORY__ = (factory), __WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ? (__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	    } else if (typeof exports === 'object') {
	        // Node/CommonJS
	        factory(require('jquery'));
	    } else {
	        // Browser globals
	        factory(jQuery);
	    }
	}(function ($) {
	
	var ua = navigator.userAgent,
		iPhone = /iphone/i.test(ua),
		chrome = /chrome/i.test(ua),
		android = /android/i.test(ua),
		caretTimeoutId;
	
	$.mask = {
		//Predefined character definitions
		definitions: {
			'9': "[0-9]",
			'a': "[A-Za-z]",
			'*': "[A-Za-z0-9]"
		},
		autoclear: true,
		dataName: "rawMaskFn",
		placeholder: '_'
	};
	
	$.fn.extend({
		//Helper Function for Caret positioning
		caret: function(begin, end) {
			var range;
	
			if (this.length === 0 || this.is(":hidden") || this.get(0) !== document.activeElement) {
				return;
			}
	
			if (typeof begin == 'number') {
				end = (typeof end === 'number') ? end : begin;
				return this.each(function() {
					if (this.setSelectionRange) {
						this.setSelectionRange(begin, end);
					} else if (this.createTextRange) {
						range = this.createTextRange();
						range.collapse(true);
						range.moveEnd('character', end);
						range.moveStart('character', begin);
						range.select();
					}
				});
			} else {
				if (this[0].setSelectionRange) {
					begin = this[0].selectionStart;
					end = this[0].selectionEnd;
				} else if (document.selection && document.selection.createRange) {
					range = document.selection.createRange();
					begin = 0 - range.duplicate().moveStart('character', -100000);
					end = begin + range.text.length;
				}
				return { begin: begin, end: end };
			}
		},
		unmask: function() {
			return this.trigger("unmask");
		},
		mask: function(mask, settings) {
			var input,
				defs,
				tests,
				partialPosition,
				firstNonMaskPos,
	            lastRequiredNonMaskPos,
	            len,
	            oldVal;
	
			if (!mask && this.length > 0) {
				input = $(this[0]);
	            var fn = input.data($.mask.dataName)
				return fn?fn():undefined;
			}
	
			settings = $.extend({
				autoclear: $.mask.autoclear,
				placeholder: $.mask.placeholder, // Load default placeholder
				completed: null
			}, settings);
	
	
			defs = $.mask.definitions;
			tests = [];
			partialPosition = len = mask.length;
			firstNonMaskPos = null;
	
			mask = String(mask);
	
			$.each(mask.split(""), function(i, c) {
				if (c == '?') {
					len--;
					partialPosition = i;
				} else if (defs[c]) {
					tests.push(new RegExp(defs[c]));
					if (firstNonMaskPos === null) {
						firstNonMaskPos = tests.length - 1;
					}
	                if(i < partialPosition){
	                    lastRequiredNonMaskPos = tests.length - 1;
	                }
				} else {
					tests.push(null);
				}
			});
	
			return this.trigger("unmask").each(function() {
				var input = $(this),
					buffer = $.map(
	    				mask.split(""),
	    				function(c, i) {
	    					if (c != '?') {
	    						return defs[c] ? getPlaceholder(i) : c;
	    					}
	    				}),
					defaultBuffer = buffer.join(''),
					focusText = input.val();
	
	            function tryFireCompleted(){
	                if (!settings.completed) {
	                    return;
	                }
	
	                for (var i = firstNonMaskPos; i <= lastRequiredNonMaskPos; i++) {
	                    if (tests[i] && buffer[i] === getPlaceholder(i)) {
	                        return;
	                    }
	                }
	                settings.completed.call(input);
	            }
	
	            function getPlaceholder(i){
	                if(i < settings.placeholder.length)
	                    return settings.placeholder.charAt(i);
	                return settings.placeholder.charAt(0);
	            }
	
				function seekNext(pos) {
					while (++pos < len && !tests[pos]);
					return pos;
				}
	
				function seekPrev(pos) {
					while (--pos >= 0 && !tests[pos]);
					return pos;
				}
	
				function shiftL(begin,end) {
					var i,
						j;
	
					if (begin<0) {
						return;
					}
	
					for (i = begin, j = seekNext(end); i < len; i++) {
						if (tests[i]) {
							if (j < len && tests[i].test(buffer[j])) {
								buffer[i] = buffer[j];
								buffer[j] = getPlaceholder(j);
							} else {
								break;
							}
	
							j = seekNext(j);
						}
					}
					writeBuffer();
					input.caret(Math.max(firstNonMaskPos, begin));
				}
	
				function shiftR(pos) {
					var i,
						c,
						j,
						t;
	
					for (i = pos, c = getPlaceholder(pos); i < len; i++) {
						if (tests[i]) {
							j = seekNext(i);
							t = buffer[i];
							buffer[i] = c;
							if (j < len && tests[j].test(t)) {
								c = t;
							} else {
								break;
							}
						}
					}
				}
	
				function androidInputEvent(e) {
					var curVal = input.val();
					var pos = input.caret();
					if (oldVal && oldVal.length && oldVal.length > curVal.length ) {
						// a deletion or backspace happened
						checkVal(true);
						while (pos.begin > 0 && !tests[pos.begin-1])
							pos.begin--;
						if (pos.begin === 0)
						{
							while (pos.begin < firstNonMaskPos && !tests[pos.begin])
								pos.begin++;
						}
						input.caret(pos.begin,pos.begin);
					} else {
						var pos2 = checkVal(true);
						var lastEnteredValue = curVal.charAt(pos.begin);
						if (pos.begin < len){
							if(!tests[pos.begin]){
								pos.begin++;
								if(tests[pos.begin].test(lastEnteredValue)){
									pos.begin++;
								}
							}else{
								if(tests[pos.begin].test(lastEnteredValue)){
									pos.begin++;
								}
							}
						}
						input.caret(pos.begin,pos.begin);
					}
					tryFireCompleted();
				}
	
	
				function blurEvent(e) {
	                checkVal();
	
	                if (input.val() != focusText)
	                    input.change();
	            }
	
				function keydownEvent(e) {
	                if (input.prop("readonly")){
	                    return;
	                }
	
					var k = e.which || e.keyCode,
						pos,
						begin,
						end;
	                    oldVal = input.val();
					//backspace, delete, and escape get special treatment
					if (k === 8 || k === 46 || (iPhone && k === 127)) {
						pos = input.caret();
						begin = pos.begin;
						end = pos.end;
	
						if (end - begin === 0) {
							begin=k!==46?seekPrev(begin):(end=seekNext(begin-1));
							end=k===46?seekNext(end):end;
						}
						clearBuffer(begin, end);
						shiftL(begin, end - 1);
	
						e.preventDefault();
					} else if( k === 13 ) { // enter
						blurEvent.call(this, e);
					} else if (k === 27) { // escape
						input.val(focusText);
						input.caret(0, checkVal());
						e.preventDefault();
					}
				}
	
				function keypressEvent(e) {
	                if (input.prop("readonly")){
	                    return;
	                }
	
					var k = e.which || e.keyCode,
						pos = input.caret(),
						p,
						c,
						next;
	
					if (e.ctrlKey || e.altKey || e.metaKey || k < 32) {//Ignore
						return;
					} else if ( k && k !== 13 ) {
						if (pos.end - pos.begin !== 0){
							clearBuffer(pos.begin, pos.end);
							shiftL(pos.begin, pos.end-1);
						}
	
						p = seekNext(pos.begin - 1);
						if (p < len) {
							c = String.fromCharCode(k);
							if (tests[p].test(c)) {
								shiftR(p);
	
								buffer[p] = c;
								writeBuffer();
								next = seekNext(p);
	
								if(android){
									//Path for CSP Violation on FireFox OS 1.1
									var proxy = function() {
										$.proxy($.fn.caret,input,next)();
									};
	
									setTimeout(proxy,0);
								}else{
									input.caret(next);
								}
	                            if(pos.begin <= lastRequiredNonMaskPos){
			                         tryFireCompleted();
	                             }
							}
						}
						e.preventDefault();
					}
				}
	
				function clearBuffer(start, end) {
					var i;
					for (i = start; i < end && i < len; i++) {
						if (tests[i]) {
							buffer[i] = getPlaceholder(i);
						}
					}
				}
	
				function writeBuffer() { input.val(buffer.join('')); }
	
				function checkVal(allow) {
					//try to place characters where they belong
					var test = input.val(),
						lastMatch = -1,
						i,
						c,
						pos;
	
					for (i = 0, pos = 0; i < len; i++) {
						if (tests[i]) {
							buffer[i] = getPlaceholder(i);
							while (pos++ < test.length) {
								c = test.charAt(pos - 1);
								if (tests[i].test(c)) {
									buffer[i] = c;
									lastMatch = i;
									break;
								}
							}
							if (pos > test.length) {
								clearBuffer(i + 1, len);
								break;
							}
						} else {
	                        if (buffer[i] === test.charAt(pos)) {
	                            pos++;
	                        }
	                        if( i < partialPosition){
	                            lastMatch = i;
	                        }
						}
					}
					if (allow) {
						writeBuffer();
					} else if (lastMatch + 1 < partialPosition) {
						if (settings.autoclear || buffer.join('') === defaultBuffer) {
							// Invalid value. Remove it and replace it with the
							// mask, which is the default behavior.
							if(input.val()) input.val("");
							clearBuffer(0, len);
						} else {
							// Invalid value, but we opt to show the value to the
							// user and allow them to correct their mistake.
							writeBuffer();
						}
					} else {
						writeBuffer();
						input.val(input.val().substring(0, lastMatch + 1));
					}
					return (partialPosition ? i : firstNonMaskPos);
				}
	
				input.data($.mask.dataName,function(){
					return $.map(buffer, function(c, i) {
						return tests[i]&&c!=getPlaceholder(i) ? c : null;
					}).join('');
				});
	
	
				input
					.one("unmask", function() {
						input
							.off(".mask")
							.removeData($.mask.dataName);
					})
					.on("focus.mask", function() {
	                    if (input.prop("readonly")){
	                        return;
	                    }
	
						clearTimeout(caretTimeoutId);
						var pos;
	
						focusText = input.val();
	
						pos = checkVal();
	
						caretTimeoutId = setTimeout(function(){
	                        if(input.get(0) !== document.activeElement){
	                            return;
	                        }
							writeBuffer();
							if (pos == mask.replace("?","").length) {
								input.caret(0, pos);
							} else {
								input.caret(pos);
							}
						}, 10);
					})
					.on("blur.mask", blurEvent)
					.on("keydown.mask", keydownEvent)
					.on("keypress.mask", keypressEvent)
					.on("input.mask paste.mask", function() {
	                    if (input.prop("readonly")){
	                        return;
	                    }
	
						setTimeout(function() {
							var pos=checkVal(true);
							input.caret(pos);
	                        tryFireCompleted();
						}, 0);
					});
	                if (chrome && android)
	                {
	                    input
	                        .off('input.mask')
	                        .on('input.mask', androidInputEvent);
	                }
					checkVal(); //Perform initial check for existing values
			});
		}
	});
	}));


/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($) {'use strict';
	
	/**
	 * Add/remove from cart, title crop, lazy-load.
	 *
	 * @module Product item
	 */
	
	/** Import utility functions */
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	/** Import CartItem class. */
	
	
	var _Helpers = __webpack_require__(6);
	
	var _CartController = __webpack_require__(11);
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	/** Class representing a website's product actions. Initialized by default. */
	var ProductItem = function () {
	  /**
	   * @constructor
	   */
	  function ProductItem() {
	    _classCallCheck(this, ProductItem);
	
	    // for dynamically added elements
	    this.items = function () {
	      return $('.default-item');
	    };
	
	    this.title = $('.default-item-title');
	
	    this.buyButton = '.default-item-buy';
	    this.removeButton = '.default-item-purchased';
	
	    this.maxTitleLength = 62;
	
	    /** Class-indicators names */
	    this.itemIsLoadedClass = 'loaded';
	  }
	
	  /**
	   * Crop title up to maximal allowed title length.
	   *
	   * @return {ProductItem}
	   **/
	
	
	  _createClass(ProductItem, [{
	    key: 'cropTitle',
	    value: function cropTitle() {
	      (0, _Helpers.cropText)(this.title, this.maxTitleLength);
	
	      return this;
	    }
	
	    /**
	     * Product item image lazy-load.
	     *
	     * @return {ProductItem}
	     */
	
	  }, {
	    key: 'imageLazyloadInit',
	    value: function imageLazyloadInit() {
	      var _this = this;
	
	      var checkAndLoad = function checkAndLoad() {
	        _this.items().not('.' + _this.itemIsLoadedClass).each(function (index, el) {
	          var $el = $(el);
	
	          if ((0, _Helpers.isElementInViewport)($el, 350)) {
	            var img = $el.find('.default-item-preview img');
	            var src = img.attr('data-src');
	
	            $el.addClass(_this.itemIsLoadedClass);
	
	            img.attr('src', src);
	          }
	        });
	      };
	      checkAndLoad();
	
	      $(window).on('load scroll resize orientationchange', checkAndLoad);
	
	      $('.owl-carousel').on('change.owl.carousel', function (e) {
	        setTimeout(function () {
	          var element = $(e.currentTarget).find('.owl-item.active').find('.default-item');
	          element.each(function (index, el) {
	            var $el = $(el);
	            var img = $el.find('.default-item-preview img');
	            var src = img.attr('data-src');
	
	            $el.addClass(_this.itemIsLoadedClass);
	
	            img.attr('src', src);
	          });
	        }, 300);
	      });
	
	      return this;
	    }
	
	    /**
	     * Initialize 'Add to cart' and 'Remove from cart' buttons.
	     *
	     * @return {ProductItem}
	     */
	
	  }, {
	    key: 'initBuyButton',
	    value: function initBuyButton() {
	      var CartItemInstance = new _CartController.CartItem(this.buyButton, this.removeButton);
	
	      CartItemInstance.bind();
	
	      return this;
	    }
	
	    /**
	     * Initialize Product item scripts.
	     *
	     * @static
	     */
	
	  }], [{
	    key: 'init',
	    value: function init() {
	      var obj = new this();
	
	      obj.initBuyButton().cropTitle().imageLazyloadInit();
	    }
	  }]);
	
	  return ProductItem;
	}();
	
	exports.default = ProductItem;
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($) {'use strict';
	
	/**
	 * Main page scripts.
	 *
	 * @module Main
	 */
	
	/** Import utility functions */
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	/** Import Calendar */
	
	
	var _Helpers = __webpack_require__(6);
	
	var _Calendar = __webpack_require__(15);
	
	var _Calendar2 = _interopRequireDefault(_Calendar);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	/** Class representing a website's Main page functions. Initialized only on web site's main page. */
	var Main = function () {
	  /**
	   * Cache main page's DOM elements for instant access and generate options for owlCarousel.
	   */
	  function Main() {
	    _classCallCheck(this, Main);
	
	    this.infoContainer = $('#info .info-container');
	    this.videoSection = $('section#video');
	
	    /** Common options for owlCarousel sliders */
	    this.commonItemSliderOptions = {
	      loop: true,
	      dots: false,
	      navText: [_Helpers.buttonLeftTemplate, _Helpers.buttonRightTemplate],
	      margin: 10,
	      responsive: {
	        0: {
	          items: 1
	        },
	        600: {
	          items: 2
	        },
	        1024: {
	          items: 4
	        }
	      }
	    };
	
	    /** Slider objects and their options */
	    this.sliders = {
	      /** Big banners carousel */
	      bigBanners: {
	        container: $('.banners-container'),
	        options: _extends({}, this.commonItemSliderOptions, {
	          lazyLoad: true,
	          dots: true,
	          responsive: {
	            0: {
	              items: 1
	            },
	            600: {
	              items: 2
	            },
	            1024: {
	              items: 3,
	              autoplay: true,
	              autoplayTimeout: 15000
	            }
	          }
	        }),
	        condition: true
	      },
	      /** 'Super price' products */
	      superPrice: {
	        container: $('#super-price .items-container'),
	        options: _extends({}, this.commonItemSliderOptions, {
	          responsive: {
	            0: {
	              nav: true,
	              items: 1
	            },
	            600: {
	              nav: true,
	              items: 2
	            },
	            1024: {
	              nav: true
	            }
	          }
	        }),
	        condition: _Helpers.screenWidth <= _Helpers.tabletLandscapeBreakpoint
	      },
	      /** 'Sales' section */
	      sales: {
	        container: $('.sales-item-container'),
	        options: _extends({}, this.commonItemSliderOptions, {
	          loop: true,
	          responsive: {
	            0: {
	              dots: true,
	              nav: false,
	              items: 1
	            },
	            600: {
	              dots: false,
	              nav: true,
	              items: 1
	            },
	            1024: {
	              nav: true,
	              items: 2
	            }
	          }
	        }),
	        condition: _Helpers.screenWidth <= 1669
	      },
	      /** 'New products' section */
	      newItems: {
	        container: $('.new-items-container'),
	        options: _extends({}, this.commonItemSliderOptions, {
	          responsive: {
	            0: {
	              dots: true,
	              nav: false,
	              items: 1
	            },
	            600: {
	              dots: false,
	              nav: true,
	              items: 1
	            },
	            1024: {
	              items: 2,
	              nav: true
	            }
	          }
	        }),
	        condition: _Helpers.screenWidth <= 1669
	      },
	      /** 'Products of the day' section */
	      prodOfTheDay: {
	        container: $('#best-of-the-day .items-container'),
	        options: _extends({}, this.commonItemSliderOptions, {
	          responsive: {
	            0: {
	              nav: true,
	              items: 1
	            },
	            600: {
	              nav: true,
	              items: 2
	            },
	            1024: {
	              nav: true,
	              items: 4
	            },
	            1670: {
	              nav: true,
	              items: 6
	            }
	          }
	        }),
	        condition: _Helpers.screenWidth < 1903
	      },
	      /** 'Video' section */
	      video: {
	        container: $('.video-items-container'),
	        options: _extends({}, this.commonItemSliderOptions, {
	          responsive: {
	            0: {
	              dots: true,
	              items: 1
	            },
	            600: {
	              dots: false,
	              nav: true,
	              video: true,
	              lazyLoad: true,
	              items: 1
	            },
	            1024: {
	              video: false,
	              lazyLoad: false,
	              items: 2,
	              nav: true
	            }
	          }
	        }),
	        condition: _Helpers.screenWidth <= 1669
	      }
	    };
	  }
	
	  /**
	   * Initialize owl carousel on selected element.
	   *
	   * @param {jQuery} element - Element to activate slider on.
	   * @param {Object} options - Carousel options.
	   * @param {Boolean} condition - Condition to init slider.
	   */
	
	
	  _createClass(Main, [{
	    key: 'initSliders',
	
	
	    /**
	     * Init sliders for all objects specified in settings.
	     *
	     * @param {Object} settings - objects with their options.
	     * @return {Main}
	     */
	    value: function initSliders(settings) {
	      for (var section in settings) {
	        if (_Helpers.has.call(settings, section)) {
	          var element = settings[section];
	          Main.initSlider(element.container, element.options, element.condition);
	        }
	      }
	
	      return this;
	    }
	
	    /**
	     * Toggle additional info block's ( on the bottom of the page ) size on click.
	     *
	     * @return {Main}
	     */
	
	  }, {
	    key: 'initTogglingInfo',
	    value: function initTogglingInfo() {
	      var _this = this;
	
	      if (_Helpers.screenWidth < 1024) {
	        this.infoContainer.click(function () {
	          _this.infoContainer.toggleClass(_Helpers.active);
	        });
	      }
	
	      return this;
	    }
	
	    /**
	     * Video lazy-load.
	     *
	     * @return {Main}
	     */
	
	  }, {
	    key: 'videoLazyloadInit',
	    value: function videoLazyloadInit() {
	      var _this2 = this;
	
	      var checkAndLoadVideo = function checkAndLoadVideo() {
	        var videoItems = _this2.videoSection.find('.video-container iframe');
	        var bodyScrollTop = _Helpers.body.scrollTop() + $(window).height();
	        var videoOffsetTop = _this2.videoSection.offset().top;
	
	        if (bodyScrollTop >= videoOffsetTop) {
	          videoItems.each(function (index, el) {
	            var $el = $(el);
	            var hasSrc = $el.attr('src');
	
	            if (!hasSrc) {
	              var src = $el.data('src');
	
	              $el.attr('src', src);
	            }
	          });
	        }
	      };
	      checkAndLoadVideo();
	
	      $(window).on('scroll resize orientationchange', checkAndLoadVideo);
	
	      return this;
	    }
	
	    /**
	     * Crop specified element's length.
	     *
	     * @return {Main}
	     */
	
	  }, {
	    key: 'initCrop',
	    value: function initCrop() {
	      /** 'Sales' items title */
	      if (_Helpers.screenWidth < 400) {
	        var $elementsToCrop = $('.cropped');
	
	        $elementsToCrop.each(function () {
	          var $this = $(this);
	          var symbolsToShow = $this.data('crop-number');
	
	          (0, _Helpers.cropText)($this, symbolsToShow);
	        });
	      }
	
	      return this;
	    }
	
	    /**
	     * Load and initialize Calendar.svg module
	     *
	     * @return {Main}
	     */
	
	  }, {
	    key: 'initCalendar',
	    value: function initCalendar() {
	      // create calendar instance
	      var CalendarInstance = new _Calendar2.default();
	
	      return this;
	    }
	
	    /**
	     * Initialize Main page scripts.
	     *
	     * @static
	     */
	
	  }], [{
	    key: 'initSlider',
	    value: function initSlider(element, options, condition) {
	      if (condition) element.owlCarousel(options);
	    }
	  }, {
	    key: 'init',
	    value: function init() {
	      var obj = new this();
	
	      obj.videoLazyloadInit().initSliders(obj.sliders).initTogglingInfo().initCrop().initCalendar();
	    }
	  }]);
	
	  return Main;
	}();
	
	exports.default = Main;
	;
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($) {'use strict';
	
	/**
	 * Animated svg calendar using Snap.svg
	 *
	 * @module Calendar.svg
	 */
	
	/** Import utility functions */
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _Helpers = __webpack_require__(6);
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var Calendar = function () {
	  /**
	   * @param {jQuery|HTMLElement|String} container - container to append svg
	   */
	  function Calendar() {
	    var _this = this;
	
	    var container = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '.calendar-svg-container';
	
	    _classCallCheck(this, Calendar);
	
	    this.$svgContainer = $(container);
	
	    this.currentMonth = new Date().getMonth() + 1;
	    this.currentYear = new Date().getFullYear();
	
	    this.realMonth = new Date().getMonth() + 1;
	
	    this.$svgLayout = $('#calendar svg');
	    this.$calendarLayout = this.$svgLayout.find('.calendar__layout');
	
	    // buttons
	    this.$prevButton = this.$svgLayout.find('.calendar__prev-month');
	    this.$nextButton = this.$svgLayout.find('.calendar__next-month');
	
	    // days
	    this.$daysContainer = this.$svgLayout.find('.calendar__days-container');
	    this.$days = function () {
	      return _this.$svgLayout.find('.calendar__day');
	    };
	    this.daysInitialTranslateX = '0.318';
	    this.daysInitialTranslateY = '0.218';
	    this.daysOfWeek = ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'];
	
	    /**
	     * Load GSAP and run Calendar.svg
	     */
	    __webpack_require__.e/* nsure */(1, function () {
	      var gsap = __webpack_require__(16);
	
	      _this.runCalendarScripts();
	    });
	  }
	
	  _createClass(Calendar, [{
	    key: 'changeCalendarsMonth',
	    value: function changeCalendarsMonth() {
	      var _this2 = this;
	
	      TweenMax.to(this.$daysContainer, 1, {
	        css: { x: '-10px', opacity: '0' },
	        ease: Power2.easeInOut
	      }, function () {
	        return _this2.$days().remove();
	      });
	    }
	  }, {
	    key: 'bindMonthsChangeButtons',
	    value: function bindMonthsChangeButtons() {
	      var _this3 = this;
	
	      /*// previous month
	      body.on('click tap', this.$prevButton, () => {
	        if (this.currentMonth === 1) {
	          this.currentYear--;
	          this.currentMonth = 12;
	        } else {
	          this.currentMonth--;
	        }
	          this.changeCalendarsMonth();
	      });*/
	
	      // next month
	      _Helpers.body.on('click tap', this.$nextButton, function () {
	        if (_this3.currentMonth === 12) {
	          _this3.currentYear++;
	          _this3.currentMonth = 1;
	        } else {
	          _this3.currentMonth++;
	        }
	
	        _this3.changeCalendarsMonth();
	      });
	    }
	
	    /**
	     * Initialize Calendar.svg event handlers
	     */
	
	  }, {
	    key: 'runCalendarScripts',
	    value: function runCalendarScripts() {
	      // bind next/prev month buttons
	      this.bindMonthsChangeButtons();
	    }
	  }], [{
	    key: 'getDaysInMonth',
	    value: function getDaysInMonth(month, year) {
	      return new Date(year, month, 0).getDate();
	    }
	  }, {
	    key: 'getFirstDayInMonth',
	    value: function getFirstDayInMonth(month, year) {
	      var day = new Date(year + '-' + month + '-01').getDay();
	      return day === 0 ? 7 : day;
	    }
	  }]);
	
	  return Calendar;
	}();
	
	exports.default = Calendar;
	;
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ }),
/* 16 */,
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($) {'use strict';
	
	/**
	 * Catalog page scripts.
	 *
	 * @module Catalog
	 */
	
	/** Import utility functions */
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _Helpers = __webpack_require__(6);
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	/** Class representing a website's Catalog page functions. Initialized only on web site's catalog pages. */
	var Catalog = function () {
	  /**
	   * Cache catalog page DOM elements for instant access and generate options for owlCarousel.
	   */
	  function Catalog() {
	    _classCallCheck(this, Catalog);
	
	    /** Collect all item containers to initialize owlCarousel on. */
	    this.sliders = $('section .items-container');
	
	    /** Collect all items to initialize dropdowns on. */
	    this.dropdowns = $('.dropdown-box');
	
	    this.resolutionToInitSliders = _Helpers.screenWidth < 1903;
	  }
	
	  /**
	   * Initialize owl carousel on all item containers.
	   *
	   * @return {Catalog}
	   */
	
	
	  _createClass(Catalog, [{
	    key: 'initSliders',
	    value: function initSliders() {
	      (0, _Helpers.initOwlCarousel)(this.sliders);
	
	      return this;
	    }
	
	    /**
	     * Initialize dropdown items.
	     *
	     * @return {Catalog}
	     */
	
	  }, {
	    key: 'initDropdowns',
	    value: function initDropdowns() {
	      this.dropdowns.each(function (index, el) {
	        var element = $(el);
	        var trigger = element.find('.dropdown-box-title');
	
	        (0, _Helpers.toggleDropdown)(trigger, element);
	
	        if ((0, _Helpers.screenWidthDynamically)() > 600) {
	          (0, _Helpers.hideOnBodyClick)(trigger, element);
	        }
	      });
	
	      return this;
	    }
	
	    /**
	     * Initialize Catalog page scripts.
	     *
	     * @static
	     */
	
	  }], [{
	    key: 'init',
	    value: function init() {
	      var obj = new this();
	
	      obj.initDropdowns();
	
	      if (obj.resolutionToInitSliders) {
	        obj.initSliders();
	      }
	    }
	  }]);
	
	  return Catalog;
	}();
	
	exports.default = Catalog;
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($) {'use strict';
	
	/**
	 * Filter page scripts.
	 *
	 * @module Filter
	 */
	
	/** Import utility functions */
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _Helpers = __webpack_require__(6);
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	/** Class representing a website's Filter page functions. Initialized only on web site's filter pages. */
	var Filter = function () {
	  /**
	   * Cache filter page DOM elements for instant access.
	   */
	  function Filter() {
	    _classCallCheck(this, Filter);
	
	    this.sortDropdowns = $('.dropdown-box.sort');
	    this.filterDropdowns = $('form.filters.mobile .dropdown-box');
	
	    this.priceSliders = $('.filters .price-slider');
	
	    this.filterScrollContainer = $('.filter-block-items-container');
	
	    this.psOptions = {
	      suppressScrollX: true,
	      swipePropagation: false
	    };
	
	    this.mobileFilters = $('form.filters.mobile');
	
	    this.filtersTrigger = $('.filters-trigger');
	
	    this.applyFiltersButton = $('.filter-button');
	    this.clearFiltersButton = $('.filters-clear-button');
	  }
	
	  /**
	   * Sort dropdowns (sort by type, show the number of products).
	   * Dropdowns, select one from list, redirect page.
	   *
	   * @return {Filter}
	   */
	
	
	  _createClass(Filter, [{
	    key: 'initSortFilter',
	    value: function initSortFilter() {
	      this.sortDropdowns.each(function (index, el) {
	        var element = $(el);
	        var trigger = element.find('.dropdown-box-title');
	        (0, _Helpers.dropdownItemInit)(trigger, element);
	
	        var sortItemSelected = element.find('.dropdown-box-title > span');
	        var sortItem = element.find('.dropdown-box-option');
	        (0, _Helpers.selectOneFromListInit)(sortItem, sortItemSelected, element);
	
	        sortItem.on('click tap', function () {});
	      });
	
	      return this;
	    }
	
	    /**
	     * Initialize filter dropdowns.
	     *
	     * @return {Filter}
	     */
	
	  }, {
	    key: 'initFilterDropdowns',
	    value: function initFilterDropdowns() {
	      var _this = this;
	
	      this.filterDropdowns.each(function (index, el) {
	        var element = $(el);
	        var trigger = element.find('.dropdown-box-title');
	        (0, _Helpers.toggleDropdown)(trigger, element);
	
	        _Helpers.body.on('click tap', function (e) {
	          if ((0, _Helpers.screenWidthDynamically)() > 600) {
	            if (!(0, _Helpers.checkClosest)(e, element) && element.hasClass(_Helpers.active) && !(0, _Helpers.checkClosest)(e, trigger)) {
	              element.removeClass(_Helpers.active);
	            }
	          }
	        });
	      });
	
	      // close all selects on orientation change
	      $(window).on('orientationchange', function () {
	        return _this.filterDropdowns.removeClass(_Helpers.active);
	      });
	
	      // Price section dropdown
	      var priceFilter = this.mobileFilters.find('.filter-block.input-container');
	      var trigger = priceFilter.find('.filter-block-title');
	      (0, _Helpers.toggleDropdown)(trigger, priceFilter);
	
	      return this;
	    }
	
	    /**
	     * Initialize filters container toggle button.
	     *
	     * @return {Filter}
	     */
	
	  }, {
	    key: 'initFiltersToggle',
	    value: function initFiltersToggle() {
	      var trigger = this.filtersTrigger;
	      var form = trigger.parents('form');
	
	      (0, _Helpers.toggleDropdown)(trigger, form);
	
	      return this;
	    }
	
	    /**
	     * Initialize perfect scrollbar.
	     *
	     * @return {Filter}
	     */
	
	  }, {
	    key: 'initPerfectScrollbar',
	    value: function initPerfectScrollbar() {
	      var _this2 = this;
	
	      this.filterScrollContainer.each(function (index, el) {
	        var elHeight = el.scrollHeight;
	
	        if (elHeight > 150) {
	          $(el).perfectScrollbar(_this2.psOptions);
	        }
	
	        $(window).on('resize orientationchange', function () {
	          var elHeight = el.scrollHeight;
	
	          if (elHeight > 150) {
	            $(el).perfectScrollbar(_this2.psOptions);
	          }
	        });
	      });
	
	      return this;
	    }
	
	    /**
	     * Generate options for ionRangeSlider.
	     *
	     * @param {jQuery} priceFrom - input with min price.
	     * @param {jQuery} priceTo - input with max price.
	     * @return {Object} object with options for IRS plugin.
	     */
	
	  }, {
	    key: 'irsOptions',
	    value: function irsOptions(priceFrom, priceTo) {
	      return {
	        type: "double",
	        min: priceFrom.attr('data-from'),
	        max: priceTo.attr('data-to'),
	        from: priceFrom.val(),
	        to: priceTo.val(),
	        hide_min_max: true,
	        grid: false,
	        onChange: function onChange(data) {
	          var newFrom = data.from;
	          var newTo = data.to;
	          priceFrom.val(newFrom);
	          priceTo.val(newTo);
	        }
	      };
	    }
	  }, {
	    key: 'initIRS',
	
	
	    /**
	     * Initialize IonRangeSlider.
	     *
	     * @return {Filter}
	     */
	    value: function initIRS() {
	      var _this3 = this;
	
	      var sliders = this.priceSliders;
	
	      sliders.each(function (index, slider) {
	        var $slider = $(slider);
	        var form = $slider.parents('form');
	        var priceFrom = form.find('.price-from');
	        var priceTo = form.find('.price-to');
	
	        $slider.ionRangeSlider(_this3.irsOptions(priceFrom, priceTo));
	
	        var sliderInstance = $slider.data('ionRangeSlider');
	        var elementsToBind = [priceFrom, priceTo];
	
	        elementsToBind.forEach(function (element) {
	          element.on('input', function () {
	            sliderInstance.update({
	              from: priceFrom.val(),
	              to: priceTo.val()
	            });
	          });
	        });
	      });
	
	      return this;
	    }
	
	    /**
	     * Bind 'Apply filters' button.
	     *
	     * @return {Filter}
	     */
	
	  }, {
	    key: 'bindApplyButton',
	    value: function bindApplyButton() {
	      this.applyFiltersButton.on('click tap', function () {
	        var form = $(this).parents('form.filters');
	        var serialized = form.serialize();
	
	        console.log(serialized.replace(/&/gi, '/'));
	        console.log('Redirect or function here');
	      });
	
	      return this;
	    }
	
	    /**
	     * Bind 'Clear filters' button.
	     *
	     * @return {Filter}
	     */
	
	  }, {
	    key: 'bindClearButton',
	    value: function bindClearButton() {
	      this.clearFiltersButton.on('click tap', function () {
	        console.log('Redirect or function here');
	      });
	
	      return this;
	    }
	
	    /**
	     * Initialize ITS and PS plugins.
	     *
	     * @return {Filter}
	     */
	
	  }, {
	    key: 'initPlugins',
	    value: function initPlugins() {
	      var _this4 = this;
	
	      __webpack_require__.e/* nsure */(2, function () {
	        var ionRangeSlider = __webpack_require__(19);
	        var perfectScrollbar = __webpack_require__(20);
	
	        // init Perfect Scrollbar and IRS
	        _this4.initPerfectScrollbar().initIRS();
	      });
	
	      return this;
	    }
	
	    /**
	     * Initialize Filter page scripts.
	     *
	     * @static
	     */
	
	  }], [{
	    key: 'init',
	    value: function init() {
	      var obj = new this();
	
	      obj.initPlugins().initFilterDropdowns().initFiltersToggle().initSortFilter().bindApplyButton().bindClearButton();
	    }
	  }]);
	
	  return Filter;
	}();
	
	exports.default = Filter;
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ }),
/* 19 */,
/* 20 */,
/* 21 */,
/* 22 */,
/* 23 */,
/* 24 */,
/* 25 */,
/* 26 */,
/* 27 */,
/* 28 */,
/* 29 */,
/* 30 */,
/* 31 */,
/* 32 */,
/* 33 */,
/* 34 */,
/* 35 */,
/* 36 */,
/* 37 */,
/* 38 */,
/* 39 */,
/* 40 */,
/* 41 */,
/* 42 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($) {'use strict';
	
	/**
	 * Product page scripts.
	 *
	 * @module Product
	 */
	
	/** Import utility functions */
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	/** Import validator for review form validation. */
	
	
	/** Import cart class. */
	
	
	var _Helpers = __webpack_require__(6);
	
	var _Validator = __webpack_require__(7);
	
	var _Validator2 = _interopRequireDefault(_Validator);
	
	var _CartController = __webpack_require__(11);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	/** Class representing a website's Product page functions. Initialized only on web site's product pages. */
	var Product = function () {
	  /**
	   * Cache product page DOM elements for instant access.
	   */
	  function Product() {
	    _classCallCheck(this, Product);
	
	    /** 'Product preview' elements */
	    this.previewPhotosContainer = $('.product-preview-items');
	    this.previewPhotos = $('.product-preview-item');
	    this.previewMainPhoto = $('.product-preview-mainPicture');
	
	    /** 'Gallery' elements */
	    this.galleryMainPhoto = $('.gallery-main-preview');
	    this.galleryPreviewPhotos = $('.gallery-photo');
	    this.galleryModal = $('.modal-gallery');
	
	    /** Slider elements */
	    this.mobileSlider = $('.product-preview-carousel');
	    this.sliders = $('section .items-container.owl-carousel');
	    this.resolutionToInitSliders = _Helpers.screenWidth < 1903;
	
	    /** Buy / Buy immediately / Remove buttons */
	    this.buyButton = $('.button.buy-now');
	    this.addToCartButton = '.button.add-to-cart';
	    this.removeFromCartButton = '.button.remove-from-cart';
	
	    /** 'Comments' elements */
	    this.moreCommentsButton = $('.reviews-toggle');
	    this.commentsContainer = $('.reviews-container');
	    this.totalReviewsCount = $('.totalReviewsCount');
	    this.reviewsContainer = $('.product-preview-reviews');
	
	    /** 'Create new review' elements */
	    this.newReviewFormToggle = $('.toggle-button.logged');
	    this.newReviewForm = $('.review-form');
	    this.newReviewFormClose = $('.review-form-close');
	
	    /** Forms */
	    this.priceNotifyForm = $('.price-notify-form');
	    this.availableNotifyForm = $('.available-notify-form');
	    this.fastOrderForm = $('.form-fastOrder');
	
	    /** AJAX URLs */
	    this.likesUrl = './ajax/vote.json';
	    this.getMoreCommentsUrl = './ajax/comments.json';
	    this.priceNotifyUrl = './ajax/success.json';
	    this.notifyAvailableUrl = './ajax/success.json';
	    this.favouriteUrl = './ajax/success.json';
	    this.fastOrderFormUrl = './ajax/success.json';
	    this.newReviewUrl = './ajax/addComment.json';
	
	    /** AJAX event names */
	    this.loadMoteCommentsEventName = 'getMoreComments';
	    this.priceNotifyEventName = 'notifyPrice';
	    this.notifyAvailableEventName = 'notifyAvailable';
	    this.addToFavouriteEventName = 'addToFavourite';
	    this.removeFromFavouriteEventName = 'removeFromFavourite';
	    this.fastOrderEventName = 'fastOrder';
	    this.newReviewEventName = 'addReview';
	
	    /** Class-indicators names */
	    this.allCharacteristicsActive = 'all-characteristics-opened';
	    this.isInFavourite = 'in-favourite';
	    this.loading = 'loading';
	
	    /** Other */
	    this.votesSelectorString = '.review-item .votes .vote';
	    this.favourite = $('#favourite');
	    this.productDataContainer = '.product-preview-tabloid';
	    this.productId = $(this.productDataContainer).data('id');
	    this.moreInfotrigger = $('.product-preview-moreInfo-trigger');
	    this.perfectScrollBarOptions = {
	      swipePropagation: false
	    };
	  }
	
	  /**
	   * Init owl carousel (mobile devices only).
	   *
	   * @return {Product}
	   */
	
	
	  _createClass(Product, [{
	    key: 'initMobileSlider',
	    value: function initMobileSlider() {
	      var slider = this.mobileSlider;
	      var options = {};
	      var owlInitialized = false;
	
	      $.extend(options, _Helpers.owlCommonOptions);
	      delete options.responsive;
	
	      options.nav = true;
	      options.loop = true;
	      options.items = 1;
	      options.lazyLoad = true;
	      options.autoHeight = true;
	
	      var checkAndInitOwl = function checkAndInitOwl() {
	        if ((0, _Helpers.screenWidthDynamically)() < 600) {
	          if (!owlInitialized) {
	            slider.owlCarousel(options);
	
	            owlInitialized = true;
	          }
	        }
	      };
	      checkAndInitOwl();
	
	      $(window).on('resize orientationchange', checkAndInitOwl);
	
	      return this;
	    }
	
	    /**
	     * Generate comment template with data.
	     *
	     * @param {Object} review - object with data to insert.
	     * @return {String} - comment template.
	     */
	
	  }, {
	    key: 'generateCommentTemplate',
	    value: function generateCommentTemplate(review) {
	      return '<div class="review-item" data-review-id="' + review.id + '">\n                            <div class="review-user-data">\n                                <span class="nickname">' + review.userName + '</span>\n                                <span class="stars inline-bottom">\n                                    <span class="stars-cover" style="width: ' + review.rating * 10 + '%"></span>\n                                </span>\n                                <span class="date">' + review.date + '</span>\n                            </div>\n                            <div class="review-user-message">\n                                <span class="text">' + review.text + '</span>\n                                <div class="votes">\n                                    <span class="likes vote" data-event-type="like">\n                                        <svg>g\n                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="./img/svg-default.svg#icon-like"></use>\n                                        </svg>\n                                        <span class="counter">' + review.likes + '</span>\n                                    </span>\n                                    <span class="dislikes vote" data-event-type="dislike">\n                                        <svg>\n                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="./img/svg-default.svg#icon-dislike"></use>\n                                        </svg>\n                                        <span class="counter">' + review.dislikes + '</span></span></div>\n                            </div>\n                        </div>';
	    }
	
	    /**
	     * 'Load more comments' button.
	     *
	     * @return {Product}
	     */
	
	  }, {
	    key: 'initLoadMoreCommentsButton',
	    value: function initLoadMoreCommentsButton() {
	      var _this = this;
	
	      var loadMoreComments = function loadMoreComments() {
	        var data = {
	          event: _this.loadMoteCommentsEventName,
	          id: _this.productId
	        };
	
	        $.ajax({
	          url: _this.getMoreCommentsUrl,
	          method: 'GET',
	          dataType: 'json',
	          data: data,
	          success: function success(res) {
	            var template = '';
	
	            res.forEach(function (review) {
	              template += _this.generateCommentTemplate(review);
	            });
	
	            _this.commentsContainer.append(template);
	
	            _this.moreCommentsButton.remove();
	          },
	          beforeSend: function beforeSend() {
	            return _this.reviewsContainer.addClass(_this.loading);
	          },
	          complete: function complete() {
	            return _this.reviewsContainer.removeClass(_this.loading);
	          }
	        });
	      };
	
	      this.moreCommentsButton.on('click tap', loadMoreComments);
	
	      return this;
	    }
	
	    /**
	     * Like/dislike buttons.
	     *
	     * @return {Product}
	     */
	
	  }, {
	    key: 'initCommentLikes',
	    value: function initCommentLikes() {
	      var _this2 = this;
	
	      var votesSelector = this.votesSelectorString;
	      var vote = function vote(id, eventType, callback) {
	        var data = {
	          id: id,
	          eventType: eventType
	        };
	
	        $.ajax({
	          url: _this2.likesUrl,
	          method: 'POST',
	          dataType: 'json',
	          data: data,
	          success: function success(res) {
	            if (res.response == 'success') {
	              callback.call(null, res.likes, res.dislikes);
	            }
	          }
	        });
	      };
	
	      _Helpers.body.on('click tap', votesSelector, function () {
	        var $this = $(this);
	        var comment = $this.parents('.review-item');
	        var id = comment.attr('id');
	        var eventType = $this.data('event-type');
	        var likesCounter = comment.find('.likes .counter');
	        var dislikesCounter = comment.find('.dislikes .counter');
	
	        var onSuccess = function onSuccess(likes, dislikes) {
	          likesCounter.text(likes);
	          dislikesCounter.text(dislikes);
	        };
	
	        vote(id, eventType, onSuccess);
	      });
	
	      return this;
	    }
	
	    /**
	     * Toggle review form button and close review init.
	     *
	     * @return {Product}
	     */
	
	  }, {
	    key: 'initReviewFormToggleAndClose',
	    value: function initReviewFormToggleAndClose() {
	      var _this3 = this;
	
	      var form = this.newReviewForm;
	      var toggleElements = [this.newReviewFormToggle, this.newReviewFormClose];
	
	      var toggleForm = function toggleForm() {
	        form.toggle();
	
	        _this3.newReviewFormToggle.toggle();
	      };
	
	      toggleElements.forEach(function (element) {
	        element.on('click tap', toggleForm);
	      });
	
	      return this;
	    }
	
	    /**
	     * Bind 'Send review' button (form validation, add new review).
	     *
	     * @return {Product}
	     */
	
	  }, {
	    key: 'initSendReviewButton',
	    value: function initSendReviewButton() {
	      var generateCommentTemplate = this.generateCommentTemplate;
	      var commentsContainer = this.commentsContainer;
	      var totalReviewsCount = this.totalReviewsCount;
	      var elementsToRemove = [this.newReviewFormToggle, this.newReviewForm];
	
	      var url = this.newReviewUrl;
	      var eventName = this.newReviewEventName;
	      var onSuccessCallback = function onSuccessCallback(review) {
	        var template = generateCommentTemplate(review);
	
	        elementsToRemove.forEach(function (element) {
	          element.remove();
	        });
	
	        commentsContainer.prepend(template);
	
	        totalReviewsCount.each(function (index, element) {
	          var $element = $(element);
	
	          $element.text(+$element.text() + 1);
	        });
	      };
	
	      var validateReview = new _Validator2.default(this.newReviewForm, function (context) {
	        var data = {
	          event: eventName,
	          rating: context.form.find('.new-review-rating input:checked').attr('id'),
	          message: context.form.find('.message textarea').val()
	        };
	
	        return {
	          url: url,
	          method: 'POST',
	          cache: false,
	          dataType: 'json',
	          data: data,
	          success: function success(res) {
	            console.log('DATA: ' + JSON.stringify(data));
	            if (res.response === 'success') {
	              console.log('review successful');
	              onSuccessCallback(res);
	            } else {
	              console.log('review failed');
	            }
	          }
	        };
	      });
	
	      return this;
	    }
	
	    /**
	     * Bind 'Fast order' send button.
	     *
	     * @return {Product}
	     */
	
	  }, {
	    key: 'initFastOrderSendButton',
	    value: function initFastOrderSendButton() {
	      var url = this.fastOrderFormUrl;
	      var eventName = this.fastOrderEventName;
	      var options = {
	        nestedInModal: true
	      };
	
	      var validateFastOrder = new _Validator2.default(this.fastOrderForm, function (context) {
	        var content = context.form.parents('.modal-content');
	
	        return {
	          url: url,
	          method: 'POST',
	          cache: false,
	          dataType: 'json',
	          data: {
	            data: context.serializedFormData(),
	            event: eventName
	          },
	          success: function success(res) {
	            console.log('DATA: ' + context.serializedFormData());
	            if (res.response === 'success') {
	              console.log('fastOrder successful');
	
	              content.addClass(context.default.formIsValidClass);
	
	              context.elements.modal.one('hidden.bs.modal', function () {
	                content.removeClass(context.default.formIsValidClass);
	                context.resetForm();
	              });
	            } else {
	              console.log('fastOrder failed');
	            }
	          }
	        };
	      }, options);
	
	      return this;
	    }
	
	    /**
	     * Init 'Show all characteristics' button.
	     *
	     * @return {Product}
	     */
	
	  }, {
	    key: 'initAllCharacteristicsToggle',
	    value: function initAllCharacteristicsToggle() {
	      (0, _Helpers.activeBodyClassItemInit)(this.moreInfotrigger, this.allCharacteristicsActive);
	
	      return this;
	    }
	
	    /**
	     * Change main preview picture on hover.
	     *
	     * @return {Product}
	     */
	
	  }, {
	    key: 'initChangeMainPreviewPhotoOnHover',
	    value: function initChangeMainPreviewPhotoOnHover() {
	      var mainPreviewPhoto = this.previewMainPhoto;
	
	      this.previewPhotos.on('mouseover', function () {
	        var $this = $(this);
	        var href = $this.attr('data-href');
	
	        mainPreviewPhoto.attr('data-href', href).find('img').attr('src', href);
	      });
	
	      return this;
	    }
	
	    /**
	     * Bind 'Add to cart' button.
	     *
	     * @return {Product}
	     */
	
	  }, {
	    key: 'bindBuyButton',
	    value: function bindBuyButton() {
	      var cartController = new _CartController.CartItem(this.addToCartButton, this.removeFromCartButton, this.productDataContainer);
	
	      cartController.bind();
	
	      return this;
	    }
	
	    /**
	     * Product pictures gallery scripts.
	     *
	     * @return {Product}
	     */
	
	  }, {
	    key: 'initGallery',
	    value: function initGallery() {
	      var galleryMainPhoto = this.galleryMainPhoto;
	      var galleryPreviewPhotos = this.galleryPreviewPhotos;
	
	      function setActivePreviewItem(item) {
	        item = $(item);
	
	        galleryPreviewPhotos.removeClass(_Helpers.active);
	
	        item.addClass(_Helpers.active);
	      }
	
	      function setGalleryMainPreviewPhoto(src) {
	        galleryMainPhoto.find('img').attr('src', src);
	      }
	
	      function getDataAndSet(item) {
	        var $item = $(item);
	        var src = $item.attr('data-href');
	
	        setGalleryMainPreviewPhoto(src);
	      }
	
	      // open gallery with selected photo
	      this.galleryModal.on('show.bs.modal', function (e) {
	        var target = $(e.relatedTarget);
	        var gallery = $(this);
	        var items = gallery.find(galleryPreviewPhotos);
	        var filter = '[data-href="' + target.attr('data-href') + '"]';
	        var activeItem = items.filter(filter);
	
	        setActivePreviewItem(activeItem);
	
	        getDataAndSet(target[0]);
	      });
	
	      // click on the gallery preview photo
	      this.galleryPreviewPhotos.on('click tap', function () {
	        setActivePreviewItem(this);
	
	        getDataAndSet(this);
	      });
	
	      return this;
	    }
	
	    /**
	     * Init perfect scrollBar.
	     *
	     * @return {Product}
	     */
	
	  }, {
	    key: 'initPerfectScrollBar',
	    value: function initPerfectScrollBar() {
	      var _this4 = this;
	
	      __webpack_require__.e/* nsure */(3, function () {/* WEBPACK VAR INJECTION */(function($) {
	        var perfectScrollbar = __webpack_require__(20);
	
	        var scrollbarEnabled = false;
	        var container = _this4.previewPhotosContainer;
	
	        var disableScrollbar = function disableScrollbar() {
	          container.perfectScrollbar('destroy');
	          scrollbarEnabled = false;
	        };
	        var enableScrollbar = function enableScrollbar() {
	          container.perfectScrollbar(_this4.perfectScrollBarOptions);
	          scrollbarEnabled = true;
	        };
	        var updateScrollbar = function updateScrollbar() {
	          return container.perfectScrollbar('update');
	        };
	
	        var checkAndUpdatePS = function checkAndUpdatePS() {
	          if ((0, _Helpers.screenWidthDynamically)() < 600) {
	            if (scrollbarEnabled) {
	              disableScrollbar();
	            }
	          } else {
	            if (scrollbarEnabled) {
	              updateScrollbar();
	            } else {
	              enableScrollbar();
	            }
	          }
	        };
	        checkAndUpdatePS();
	
	        $(window).on('resize orientationchange', checkAndUpdatePS);
	      
	/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(2)))});
	
	      return this;
	    }
	
	    /**
	     * Initialize owl carousel on all item containers.
	     *
	     * @return {Product}
	     */
	
	  }, {
	    key: 'initSliders',
	    value: function initSliders() {
	      if (this.resolutionToInitSliders) (0, _Helpers.initOwlCarousel)(this.sliders);
	
	      return this;
	    }
	
	    /**
	     * Validate price-notify form.
	     *
	     * @return {Product}
	     */
	
	  }, {
	    key: 'initValidatePriceNotify',
	    value: function initValidatePriceNotify() {
	      var url = this.priceNotifyUrl;
	      var eventName = this.priceNotifyEventName;
	      var productId = this.productId;
	
	      var validatePriceNotify = new _Validator2.default(this.priceNotifyForm, function (context) {
	        var data = {
	          id: productId,
	          event: eventName,
	          email: context.elements.inputs().val()
	        };
	
	        return {
	          url: url,
	          method: 'GET',
	          cache: false,
	          dataType: 'json',
	          data: data,
	          success: function success(res) {
	            console.log('DATA: ' + JSON.stringify(data));
	            if (res.response === 'success') {
	              context.elements.fields().remove();
	
	              context.form.append('<span class="success">\n                    <b>\u0421\u043F\u0430\u0441\u0438\u0431\u043E!</b> <br />\n                    \u041C\u044B \u043E\u0442\u043F\u0440\u0430\u0432\u0438\u043C \u043F\u0438\u0441\u044C\u043C\u043E \u043D\u0430 \u0412\u0430\u0448 E-mail, \n                    \u0435\u0441\u043B\u0438 \u0446\u0435\u043D\u0430 \u043D\u0430 \u044D\u0442\u043E\u0442 \u0442\u043E\u0432\u0430\u0440 \u0441\u0442\u0430\u043D\u0435\u0442 \u043D\u0438\u0436\u0435.\n                </span>');
	            } else {
	              console.log('price notify failed');
	            }
	          }
	        };
	      });
	
	      // remove incorrect style if clicked anywhere outside the container
	      (0, _Helpers.removeClassOnBodyClick)(validatePriceNotify.form, validatePriceNotify.elements.fields());
	
	      return this;
	    }
	
	    /**
	     * Validate price-notify form.
	     *
	     * @return {Product}
	     */
	
	  }, {
	    key: 'initValidateAvailableNotify',
	    value: function initValidateAvailableNotify() {
	      var url = this.notifyAvailableUrl;
	      var eventName = this.notifyAvailableEventName;
	      var productId = this.productId;
	
	      var validateAvailableNotify = new _Validator2.default(this.availableNotifyForm, function (context) {
	        var data = {
	          id: productId,
	          event: eventName,
	          email: context.elements.inputs().val()
	        };
	
	        return {
	          url: url,
	          method: 'GET',
	          cache: false,
	          dataType: 'json',
	          data: data,
	          success: function success(res) {
	            console.log('DATA: ' + JSON.stringify(data));
	            if (res.response === 'success') {
	              context.elements.fields().remove();
	              context.elements.button.remove();
	
	              context.form.append('<span class="success">\n                    <b>\u0421\u043F\u0430\u0441\u0438\u0431\u043E!</b> <br />\n                    \u041C\u044B \u043E\u0442\u043F\u0440\u0430\u0432\u0438\u043C \u043F\u0438\u0441\u044C\u043C\u043E \u043D\u0430 \u0412\u0430\u0448 E-mail, \n                    \u043A\u043E\u0433\u0434\u0430 \u0434\u0430\u043D\u043D\u044B\u0439 \u0442\u043E\u0432\u0430\u0440 \u043F\u043E\u044F\u0432\u0438\u0442\u0441\u044F \u0432 \u043D\u0430\u043B\u0438\u0447\u0438\u0438.\n                </span>');
	            } else {
	              console.log('available notify failed');
	            }
	          }
	        };
	      });
	
	      // remove incorrect style if clicked anywhere outside the container
	      (0, _Helpers.removeClassOnBodyClick)(validateAvailableNotify.form, validateAvailableNotify.elements.fields());
	
	      return this;
	    }
	
	    /**
	     * Add/remove item from favourites.
	     *
	     * @return {Product}
	     */
	
	  }, {
	    key: 'initFavourite',
	    value: function initFavourite() {
	      var _this5 = this;
	
	      var generateDataAndSend = function (eventName, callback) {
	        var data = {
	          event: eventName,
	          id: this.productId
	        };
	
	        $.ajax({
	          url: this.favouriteUrl,
	          method: 'POST',
	          cache: false,
	          dataType: 'json',
	          data: data,
	          success: function success(res) {
	            console.log('DATA:\n' + JSON.stringify(data) + '\nRESPONSE:\n' + JSON.stringify(res));
	            if (res.response == 'success') {
	              callback.call(null);
	            }
	          }
	        });
	      }.bind(this);
	
	      var container = this.favourite;
	      var button = container.find('a');
	
	      button.on('click tap', function (e) {
	        e.preventDefault();
	
	        var cb = void 0,
	            eventName = void 0;
	
	        if (button.hasClass(_this5.isInFavourite)) {
	          cb = function cb() {
	            return button.text('добавить в избранное').removeClass(_this5.isInFavourite);
	          };
	          eventName = _this5.removeFromFavouriteEventName;
	        } else {
	          cb = function cb() {
	            return button.text('убрать из избранного').addClass(_this5.isInFavourite);
	          };
	          eventName = _this5.addToFavouriteEventName;
	        }
	
	        generateDataAndSend(eventName, cb);
	      });
	
	      return this;
	    }
	
	    /**
	     * Initialize Product page scripts.
	     *
	     * @static
	     */
	
	  }], [{
	    key: 'init',
	    value: function init() {
	      var obj = new this();
	
	      obj.initPerfectScrollBar().initMobileSlider().initFavourite().initFastOrderSendButton().initAllCharacteristicsToggle().initChangeMainPreviewPhotoOnHover().initGallery().initValidatePriceNotify().initValidateAvailableNotify().initSliders().bindBuyButton().initLoadMoreCommentsButton();
	
	      if (_Helpers.userLoggedIn) {
	        obj.initReviewFormToggleAndClose().initSendReviewButton().initCommentLikes();
	      }
	    }
	  }]);
	
	  return Product;
	}();
	
	exports.default = Product;
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ }),
/* 43 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($) {// jscs:disable
	'use strict';
	
	/**
	 * Cart page scripts.
	 *
	 * @module Cart
	 */
	
	/** Import utility functions */
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	/** Import Validator class */
	
	
	var _Helpers = __webpack_require__(6);
	
	var _Validator = __webpack_require__(7);
	
	var _Validator2 = _interopRequireDefault(_Validator);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	/** Class representing a website's Cart page functions. Initialized only on web site's cart page. */
	var Cart = function () {
	  /**
	   * Cache cart page DOM elements for instant access.
	   */
	  function Cart() {
	    _classCallCheck(this, Cart);
	
	    /** Bonus fields elements */
	    this.sendBonusButton = $('.validate-form-button.bonus');
	    this.sendPromoButton = $('.validate-form-button.promo');
	    this.bonusField = $('.form-input.cart-bonus');
	    this.promoField = $('.form-input.cart-promo');
	    this.changeValueButtons = $('.inner-validated .change-value-button');
	
	    /** Checkboxes and their amount output */
	    this.mainCheckbox = $('label.checkbox.selectAll input');
	    this.itemCheckBoxes = function () {
	      return $('.cart-item .checkbox input');
	    };
	    this.displayContainer = $('.checkbox.selectAll span');
	    this.currentPageItems = $('.cart-links a.active span');
	    this.deferredItemsContainer = $('.cart-links a.deferred span');
	    this.readyToBuyItemsContainer = $('.cart-links a.readyToBuy span');
	
	    /** Items count */
	    this.countIncrement = $('.cart-item .count.plus');
	    this.countDecrement = $('.cart-item .count.minus');
	    this.countInput = $('.cart-item .count.current');
	
	    /** 'Presents' section */
	    this.presentsDropdownTrigger = $('.cart-presents .trigger');
	    this.presentsContainer = $('.cart-presents');
	
	    /** Other */
	    this.sliders = $('section .items-container.owl-carousel');
	    this.cartActionButtons = $('.cart-main-actions .action');
	    this.cartMainContainer = $('.cart-main-container');
	    this.promoModal = $('.modal-promo.modal');
	
	    /** Class-indicators names */
	    this.cartIsEmptyClassName = 'empty';
	    this.validatedConfirm = 'validated-confirm';
	    this.validatedError = 'validated-error';
	    this.buttonIsDisabled = 'disabled';
	
	    /** AJAX URLs */
	    this.changeItemQuantityUrl = './ajax/cart-changeCount.json';
	    this.cartItemsActionsUrl = './ajax/cart-action.json';
	    this.bonusUrl = './ajax/bonus.json';
	    this.promoUrl = './ajax/promo.json';
	    this.changeBonusOrPromoUrl = './ajax/success.json';
	
	    /** AJAX event names */
	    this.bonusEventName = 'setBonus';
	    this.promoEventName = 'setPromo';
	  }
	
	  /**
	   * Initialize present block dropdown.
	   *
	   * @return {Cart}
	   */
	
	
	  _createClass(Cart, [{
	    key: 'initPresentsDropdown',
	    value: function initPresentsDropdown() {
	      (0, _Helpers.toggleDropdown)(this.presentsDropdownTrigger, this.presentsContainer);
	
	      return this;
	    }
	
	    /**
	     * Validate and send Bonus/Promo information.
	     *
	     * @return {Cart}
	     */
	
	  }, {
	    key: 'initBonusAndPromo',
	    value: function initBonusAndPromo() {
	      var _this2 = this;
	
	      var ValidatorInstance = new _Validator2.default();
	
	      var validateField = function (field, url, eventType, callback) {
	        var input = field.find('input');
	        var value = input.val();
	        var data = {
	          eventType: eventType,
	          value: value
	        };
	
	        if (!value.length) {
	          ValidatorInstance.throwError(field, 'Заполните поле');
	        } else {
	          field.removeClass(_Helpers.formIsIncorrect);
	
	          (0, _Helpers.sendJsonAjaxWithCallback)(url, data, callback, this);
	        }
	      }.bind(this);
	
	      /** Bonus field */
	      this.sendBonusButton.on('click tap', function (e) {
	        e.preventDefault();
	
	        function onAjaxSuccess(res) {
	          var response = res.response;
	          var value = res.value;
	
	          if (response == 'success') {
	            var valueField = this.bonusField.find('.message .value');
	
	            valueField.text(value);
	            this.bonusField.addClass(_Helpers.formIsValidated);
	          } else {
	            ValidatorInstance.throwError(this.bonusField, res.text);
	          }
	        }
	
	        validateField(_this2.bonusField, _this2.bonusUrl, _this2.bonusEventName, onAjaxSuccess);
	      });
	
	      /** Promo field */
	      this.sendPromoButton.on('click tap', function (e) {
	        e.preventDefault();
	
	        function onAjaxSuccess(res) {
	          var resMessage = res.message;
	          var modalIsActive = res.modal;
	          var resClass = res.class;
	
	          var valueField = this.promoField.find('.message span');
	
	          valueField.text(resMessage);
	          this.promoField.addClass(_Helpers.formIsValidated + ' ' + resClass);
	
	          if (modalIsActive) {
	            var message = res.modalMessage;
	            var messageBox = this.promoModal.find('.message');
	
	            messageBox.text(message);
	            this.promoModal.modal('show');
	          }
	        }
	
	        validateField(_this2.promoField, _this2.promoUrl, _this2.promoEventName, onAjaxSuccess);
	      });
	
	      // remove incorrect style if clicked anywhere outside the container
	      (0, _Helpers.removeClassOnBodyClick)(this.bonusField);
	      (0, _Helpers.removeClassOnBodyClick)(this.promoField);
	
	      return this;
	    }
	
	    /**
	     * Change Bonus/Promo information.
	     *
	     * @return {Cart}
	     */
	
	  }, {
	    key: 'initBonusAndPromoChange',
	    value: function initBonusAndPromoChange() {
	
	      var changeValueAjax = function (eventType, field) {
	        var data = {
	          event: eventType
	        };
	
	        function callback(res) {
	          if (res.response == 'success') {
	            field.removeClass(_Helpers.formIsValidated + ' ' + this.validatedConfirm + ' ' + this.validatedError);
	          }
	        }
	
	        (0, _Helpers.sendJsonAjaxWithCallback)(this.changeBonusOrPromoUrl, data, callback, this);
	      }.bind(this);
	
	      this.changeValueButtons.on('click tap', function () {
	        var $this = $(this);
	        var eventType = $this.data('event-name');
	        var field = $this.parents('.form-input');
	
	        changeValueAjax(eventType, field);
	      });
	
	      return this;
	    }
	
	    /**
	     * Display number of currently selected products.
	     *
	     * @param {Number} number - number of selected products.
	     * @return {Cart}
	     */
	
	  }, {
	    key: 'displaySelectedProducts',
	    value: function displaySelectedProducts(number) {
	      var display = this.displayContainer;
	      var dividedNumber = number % 10;
	
	      function set(string) {
	        display.text(string);
	      }
	
	      if (!number) {
	        set('Товар');
	      } else if (number === 1) {
	        set('\u0412\u044B\u0431\u0440\u0430\u043D ' + number + ' \u0442\u043E\u0432\u0430\u0440');
	      } else if (dividedNumber === 2 || dividedNumber === 3 || dividedNumber === 4) {
	        set('\u0412\u044B\u0431\u0440\u0430\u043D\u043E ' + number + ' \u0442\u043E\u0432\u0430\u0440\u0430');
	      } else {
	        set('\u0412\u044B\u0431\u0440\u0430\u043D\u043E ' + number + ' \u0442\u043E\u0432\u0430\u0440\u043E\u0432');
	      }
	
	      return this;
	    }
	
	    /**
	     * Change displayed cart total sum.
	     *
	     * @param {Number} price
	     * @param {Number} discount
	     * @param {Number} toPay
	     * @return {Cart}
	     */
	
	  }, {
	    key: 'initProductCountChangeListener',
	
	
	    /**
	     * Change products count.
	     *
	     * @return {Cart}
	     */
	    value: function initProductCountChangeListener() {
	      var plus = this.countIncrement;
	      var minus = this.countDecrement;
	      var input = this.countInput;
	      var url = this.changeItemQuantityUrl;
	      var disabled = this.buttonIsDisabled;
	
	      function generateDOM(context) {
	        var item = $(context).parents('.cart-item');
	        var input = item.find('.count.current');
	        var totalPrice = item.find('.total-price');
	        var totalDiscount = item.find('.total-discount');
	        var minusButton = item.find('.count.minus');
	
	        return {
	          item: item,
	          input: input,
	          minusButton: minusButton,
	          totalPrice: totalPrice,
	          totalDiscount: totalDiscount
	        };
	      }
	
	      function changeItemsCount(newValue, el) {
	        $.ajax({
	          url: url,
	          method: 'GET',
	          cache: false,
	          dataType: 'json',
	          data: {
	            itemId: el.item.data('id'),
	            quantity: newValue
	          },
	          success: function success(res) {
	            console.log('DATA\nID: ' + el.item.data('id') + '\nNEW VALUE: ' + newValue);
	
	            // change cart's 'total price' data
	            var basket = res.basket;
	            Cart.changeDisplayedCartInfo(basket.totalPrice_txt, basket.totalDiscount_txt, basket.totalPriceToPay_txt);
	
	            // change item's 'total price' data
	            var item = res.item;
	            el.item.attr('data-quantity', item.quantity);
	            el.input.val(item.quantity);
	            el.totalPrice.text(item.totalPrice_txt);
	            el.totalDiscount.text(item.totalDiscount_txt);
	
	            // block/unblock 'minus' button
	            if (item.quantity == 1) {
	              el.minusButton.addClass(disabled);
	            } else {
	              el.minusButton.removeClass(disabled);
	            }
	          }
	        });
	      }
	
	      function changeOnClick(context, inc) {
	        if ($(context).hasClass(disabled)) {
	          return false;
	        }
	
	        var el = generateDOM(context);
	
	        var currentQuantity = el.item.attr('data-quantity');
	        var newQuantity = void 0;
	
	        if (inc == 'plus') {
	          newQuantity = ++currentQuantity;
	        } else if (inc == 'minus') {
	          newQuantity = --currentQuantity;
	        }
	
	        el.input.val(newQuantity);
	
	        changeItemsCount(newQuantity, el);
	      }
	
	      plus.on('click tap', function () {
	        changeOnClick(this, 'plus');
	      });
	
	      minus.on('click tap', function () {
	        changeOnClick(this, 'minus');
	      });
	
	      input.on('input', function () {
	        var el = generateDOM(this);
	
	        var newValue = $(this).val();
	
	        changeItemsCount(newValue, el);
	      });
	
	      return this;
	    }
	
	    /**
	     * Initialize actions with cart items (delay, delete, restore).
	     *
	     * @return {Cart}
	     */
	
	  }, {
	    key: 'initCartActions',
	    value: function initCartActions() {
	      var _this = this;
	
	      this.cartActionButtons.on('click tap', function () {
	        var eventName = $(this).data('eventName');
	        var selectedItems = (0, _Helpers.getChecked)(_this.itemCheckBoxes());
	        var selectedItemsLength = selectedItems.length;
	
	        // no items were selected or the cart is empty
	        if (!selectedItems.length || _this.cartMainContainer.hasClass('empty')) {
	          return;
	        }
	
	        // data for ajax
	        var data = {
	          eventName: eventName,
	          items: []
	        };
	
	        // push selected items to the data container
	        selectedItems.forEach(function (input) {
	          var item = $(input).parents('.cart-item');
	          var id = item.data('id');
	          var quantity = item.data('quantity');
	
	          data.items.push({ id: id, quantity: quantity });
	        });
	
	        $.ajax({
	          url: _this.cartItemsActionsUrl,
	          method: 'POST',
	          dataType: 'json',
	          cache: false,
	          data: data,
	          success: function success(res) {
	            console.log('DATA: ' + JSON.stringify(data) + '\nRESPONSE: ' + JSON.stringify(res));
	
	            if (res.response === 'success') {
	              console.log(eventName + ' success');
	
	              // remove items
	              selectedItems.forEach(function (el) {
	                return $(el).parents('.cart-item').remove();
	              });
	
	              // count remaining items
	              var itemCheckboxes = _this.itemCheckBoxes();
	              _this.displaySelectedProducts((0, _Helpers.getCheckedLength)(itemCheckboxes));
	
	              // change total price cart data
	              var basket = res.basket;
	              var newTotalPrice = basket.totalPrice_txt;
	              var newTotalDiscount = basket.totalDiscount_txt;
	              var newTotalPriceToPay = basket.totalPriceToPay_txt;
	              Cart.changeDisplayedCartInfo(newTotalPrice, newTotalDiscount, newTotalPriceToPay);
	
	              // if no items left
	              if (!itemCheckboxes.length) {
	                _this.cartMainContainer.addClass(_this.cartIsEmptyClassName);
	              }
	
	              // display items count
	              var readyToBuy = _this.readyToBuyItemsContainer;
	              var deferred = _this.deferredItemsContainer;
	              var current = _this.currentPageItems;
	
	              current.text(itemCheckboxes.length);
	
	              switch (eventName) {
	                case 'cart-add-deferred':
	                  {
	                    deferred.text(parseInt(deferred.text(), 10) + selectedItemsLength);
	                    break;
	                  }
	                case 'cart-restore':
	                  {
	                    readyToBuy.text(parseInt(readyToBuy.text(), 10) + selectedItemsLength);
	                    break;
	                  }
	              }
	            } else {
	              console.log(eventName + ' failed');
	            }
	          }
	        });
	      });
	
	      return this;
	    }
	
	    /**
	     * Init checkboxes. (select all, unselect all etc.)
	     *
	     * @return {Cart}
	     */
	
	  }, {
	    key: 'initCheckboxes',
	    value: function initCheckboxes() {
	      var _this3 = this;
	
	      var mainCheckbox = this.mainCheckbox;
	
	      function set(state) {
	        for (var _len = arguments.length, items = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
	          items[_key - 1] = arguments[_key];
	        }
	
	        items.forEach(function (item) {
	          $(item).prop('checked', state);
	        });
	      }
	
	      mainCheckbox.on('change', function () {
	        var itemCheckboxes = _this3.itemCheckBoxes();
	
	        if ((0, _Helpers.getCheckedLength)(itemCheckboxes) < itemCheckboxes.length) {
	          set(true, itemCheckboxes);
	        } else {
	          set(false, itemCheckboxes);
	        }
	
	        _this3.displaySelectedProducts((0, _Helpers.getCheckedLength)(itemCheckboxes));
	      });
	
	      this.itemCheckBoxes().on('change', function () {
	        var itemCheckboxes = _this3.itemCheckBoxes();
	
	        if ((0, _Helpers.getCheckedLength)(itemCheckboxes) < itemCheckboxes.length) {
	          set(false, mainCheckbox);
	        } else {
	          set(true, mainCheckbox);
	        }
	
	        _this3.displaySelectedProducts((0, _Helpers.getCheckedLength)(itemCheckboxes));
	      });
	
	      return this;
	    }
	
	    /**
	     * Initialize owl carousel on all item containers.
	     *
	     * @return {Cart}
	     */
	
	  }, {
	    key: 'initSliders',
	    value: function initSliders() {
	      (0, _Helpers.initOwlCarousel)(this.sliders);
	
	      return this;
	    }
	
	    /**
	     * Initialize Cart page scripts.
	     *
	     * @static
	     */
	
	  }], [{
	    key: 'changeDisplayedCartInfo',
	    value: function changeDisplayedCartInfo(price, discount, toPay) {
	      var totalPrice = $('.cart-price-total .price-field.total .value span:first-of-type');
	      var totalDiscount = $('.cart-price-total .discount .value span:first-of-type');
	      var totalPriceToPay = $('.cart-price-total .price-field.to-pay .value span:first-of-type');
	
	      totalPrice.text(price);
	      totalDiscount.text(discount);
	      totalPriceToPay.text(toPay);
	    }
	  }, {
	    key: 'init',
	    value: function init() {
	      var obj = new this();
	
	      obj.initPresentsDropdown().initBonusAndPromo().initBonusAndPromoChange().initCartActions().initProductCountChangeListener().initCheckboxes();
	
	      if (_Helpers.screenWidth < 1903) {
	        obj.initSliders();
	      }
	    }
	  }]);
	
	  return Cart;
	}();
	
	exports.default = Cart;
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ }),
/* 44 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($) {'use strict';
	
	/**
	 * Registration page scripts.
	 *
	 * @module Registration
	 */
	
	/** Import utility functions */
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	/** Import Validator. */
	
	
	/** Import Footer class for subscribe form validation method */
	
	
	var _Helpers = __webpack_require__(6);
	
	var _Validator = __webpack_require__(7);
	
	var _Validator2 = _interopRequireDefault(_Validator);
	
	var _Footer = __webpack_require__(8);
	
	var _Footer2 = _interopRequireDefault(_Footer);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	/** Class representing a website's Registration page functions. Initialized only on web site's registration page. */
	var Registration = function () {
	  /**
	   * Cache registration page DOM elements for instant access.
	   */
	  function Registration() {
	    _classCallCheck(this, Registration);
	
	    this.cartContainer = $('.cart-main-container');
	    this.cartTrigger = $('.cart-toggle');
	
	    this.orderForm = $('.form.form-order');
	
	    this.printButton = $('.print-order-button');
	
	    this.order = $('.cart-main');
	
	    /** Subscribe form elements */
	    this.subscribeButton = $('#registration_subscribe');
	    this.subscribeInput = $('#registration-subscribe-input');
	    this.subscribeFormContainer = $('form.registration-subscribe-form');
	
	    /** AJAX URLs */
	    this.subscribeUrl = './ajax/subscribe-other.json';
	    this.orderFormUrl = './ajax/success.json';
	
	    /** AJAX event names */
	    this.orderFormEventName = 'validateOrder';
	    this.subscribeEventName = 'subscribeForm';
	
	    /** 'Delivery method state change' components */
	    this.changeStateSelect = this.orderForm.find('label.userDelivery select');
	  }
	
	  /**
	   * Init cart dropdown.
	   *
	   * @return {Registration}
	   */
	
	
	  _createClass(Registration, [{
	    key: 'initCartDropdown',
	    value: function initCartDropdown() {
	      (0, _Helpers.toggleDropdown)(this.cartTrigger, this.cartContainer);
	
	      return this;
	    }
	
	    /**
	     * Print on click.
	     *
	     * @return {Registration}
	     */
	
	  }, {
	    key: 'initPrint',
	    value: function initPrint() {
	      this.printButton.on('click tap', function () {
	        return window.print();
	      });
	
	      return this;
	    }
	
	    /**
	     * Init order form validation & data send.
	     *
	     * @return {Registration}
	     */
	
	  }, {
	    key: 'initOrderForm',
	    value: function initOrderForm() {
	      var formUrl = this.orderFormUrl;
	      var eventName = this.orderFormEventName;
	
	      var validateOrderForm = new _Validator2.default(this.orderForm, function (context) {
	        return {
	          url: formUrl,
	          method: 'POST',
	          cache: false,
	          dataType: 'json',
	          data: {
	            data: context.serializedFormData(),
	            event: eventName
	          },
	          success: function success(res) {
	            console.log('DATA: ' + context.serializedFormData());
	            if (res.response === 'success') {
	              console.log('order successful');
	            } else {
	              console.log('order failed');
	            }
	          }
	        };
	      });
	
	      return this;
	    }
	
	    /**
	     * Init subscribe form validation & data send.
	     *
	     * @return {Registration}
	     */
	
	  }, {
	    key: 'initSubscribeForm',
	    value: function initSubscribeForm() {
	      new _Footer2.default().validateSubscribeEmail.call(this);
	
	      return this;
	    }
	
	    /**
	     * Init form's state change on delivery method change.
	     *
	     * @return {Registration}
	     */
	
	  }, {
	    key: 'initChangeFormStateOnDeliveryChange',
	    value: function initChangeFormStateOnDeliveryChange() {
	      var _this = this;
	
	      this.changeStateSelect.on('change', function () {
	        _this.orderForm.attr('data-method', _this.changeStateSelect.val());
	      });
	
	      return this;
	    }
	
	    /**
	     * Initialize Registration page scripts.
	     *
	     * @static
	     */
	
	  }], [{
	    key: 'init',
	    value: function init() {
	      var obj = new this();
	
	      obj.initCartDropdown().initOrderForm().initSubscribeForm().initChangeFormStateOnDeliveryChange().initPrint();
	    }
	  }]);
	
	  return Registration;
	}();
	
	exports.default = Registration;
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ }),
/* 45 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($) {'use strict';
	
	/**
	 * 'Search results' page scripts.
	 *
	 * @module Search
	 */
	
	/** Import utility functions */
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	/** Import Validator. */
	
	
	var _Helpers = __webpack_require__(6);
	
	var _Validator = __webpack_require__(7);
	
	var _Validator2 = _interopRequireDefault(_Validator);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	/** Class representing a website's 'Search results' page functions. Initialized only on web site's 'Search results' pages. */
	var Search = function () {
	  /**
	   * Cache 'Search results' page DOM elements for instant access.
	   */
	  function Search() {
	    _classCallCheck(this, Search);
	
	    this.filterCheckboxes = $('label.filter-block-item input');
	    this.filterCheckboxesForm = $('form.filters');
	    this.filterCheckboxesFormTrigger = this.filterCheckboxesForm.find('.filters-mobile-title');
	
	    this.requestForm = $('form.product-request-form');
	
	    /** AJAX URLs */
	    this.requestFormUrl = './ajax/search-form.json';
	
	    /** AJAX event names */
	    this.requestFormEventName = 'productRequest';
	  }
	
	  /**
	   * Register filter checkboxes on 'change(check)' event.
	   *
	   * @return {Search}
	   */
	
	
	  _createClass(Search, [{
	    key: 'initFiltersAction',
	    value: function initFiltersAction() {
	      var _this = this;
	
	      this.filterCheckboxes.on('change', function () {
	        var thisCheckboxValue = $(this).serialize();
	        var allCheckboxesValue = _this.filterCheckboxesForm.serialize().replace(/&/g, '/');
	
	        console.log('your function here');
	        console.log('Checked checkbox data:\n' + thisCheckboxValue);
	        console.log('All checkboxes data:\n' + allCheckboxesValue);
	      });
	
	      return this;
	    }
	
	    /**
	     * Validate and send request form's data.
	     *
	     * @return {Search}
	     */
	
	  }, {
	    key: 'initRequestFormValidation',
	    value: function initRequestFormValidation() {
	      var form = this.requestForm;
	      var formUrl = this.requestFormUrl;
	      var eventName = this.requestFormEventName;
	
	      var validateRequestForm = new _Validator2.default(this.requestForm, function (context) {
	        return {
	          url: formUrl,
	          method: 'POST',
	          cache: false,
	          dataType: 'json',
	          data: {
	            data: context.serializedFormData(),
	            event: eventName
	          },
	          success: function success(res) {
	            console.log('DATA: ' + context.serializedFormData());
	            if (res.response === 'success') {
	              form.html('<div class="form-title">' + res.message_title + '</div>\n               <div class="form-text">' + res.message_text + '</div>');
	              console.log('product request successful');
	            } else {
	              console.log('product request failed');
	            }
	          }
	        };
	      });
	
	      return this;
	    }
	
	    /**
	     * Init dropDowns on mobile.
	     *
	     * @return {Search}
	     */
	
	  }, {
	    key: 'initFilterDropdownsMobile',
	    value: function initFilterDropdownsMobile() {
	      var state = 1;
	
	      var initDropdowns = function () {
	        if ((0, _Helpers.screenWidthDynamically)() < 768 && state === 1) {
	          (0, _Helpers.toggleDropdown)(this.filterCheckboxesFormTrigger, this.filterCheckboxesForm);
	          state = 2;
	        }
	      }.bind(this);
	
	      initDropdowns();
	
	      $(window).on('resize orientationchange', initDropdowns);
	
	      return this;
	    }
	
	    /**
	     * Initialize 'Search results' page scripts.
	     *
	     * @static
	     */
	
	  }], [{
	    key: 'init',
	    value: function init() {
	      var obj = new this();
	
	      obj.initFiltersAction().initFilterDropdownsMobile().initRequestFormValidation();
	    }
	  }]);
	
	  return Search;
	}();
	
	exports.default = Search;
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ }),
/* 46 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($) {'use strict';
	
	/**
	 * News & Sales pages scripts.
	 *
	 * @module News
	 */
	
	/** Import utility functions */
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _Helpers = __webpack_require__(6);
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	/** Class representing a website's News & Sales pages functions. Initialized only on web site's News & Sales pages. */
	var News = function () {
	  /**
	   * Cache News & Sales pages page DOM elements for instant access.
	   */
	  function News() {
	    _classCallCheck(this, News);
	
	    this.dropdownBox = $('.menu.category-menu');
	    this.dropdownTrigger = this.dropdownBox.find('.category-separator');
	  }
	
	  /**
	   * Init dropDowns on mobile.
	   *
	   * @return {News}
	   */
	
	
	  _createClass(News, [{
	    key: 'initDropdownsMobile',
	    value: function initDropdownsMobile() {
	      var state = 1;
	
	      var initDropdowns = function () {
	        if ((0, _Helpers.screenWidthDynamically)() < 768 && state === 1) {
	          (0, _Helpers.toggleDropdown)(this.dropdownTrigger, this.dropdownBox);
	          state = 2;
	        }
	      }.bind(this);
	
	      initDropdowns();
	
	      $(window).on('resize orientationchange', initDropdowns);
	
	      return this;
	    }
	
	    /**
	     * Initialize News & Sales pages scripts.
	     *
	     * @static
	     */
	
	  }], [{
	    key: 'init',
	    value: function init() {
	      var obj = new this();
	
	      obj.initDropdownsMobile();
	    }
	  }]);
	
	  return News;
	}();
	
	exports.default = News;
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ }),
/* 47 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($) {'use strict';
	
	/**
	 * Calendar page scripts.
	 *
	 * @module Calendar
	 */
	
	/** Import utility functions */
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _Helpers = __webpack_require__(6);
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	/** Class representing a website's Calendar page functions. Initialized only on web site's Calendar page. */
	var Calendar = function () {
	  /**
	   * Cache Calendar page page DOM elements for instant access.
	   */
	  function Calendar() {
	    _classCallCheck(this, Calendar);
	
	    this.slider = $('.calendar-events.mobile');
	    this.sliderOptions = {
	      loop: true,
	      nav: false,
	      dots: true,
	      navText: [_Helpers.buttonLeftTemplate, _Helpers.buttonRightTemplate],
	      margin: 10,
	      responsive: {
	        0: {
	          items: 1
	        },
	        600: {
	          items: 2
	        }
	      }
	    };
	  }
	
	  /**
	   * Init slider on mobile.
	   *
	   * @return {Calendar}
	   */
	
	
	  _createClass(Calendar, [{
	    key: 'initSliderMobile',
	    value: function initSliderMobile() {
	      var state = 1;
	      var initSliders = function () {
	        if ((0, _Helpers.screenWidthDynamically)() < 768 && state === 1) {
	          (0, _Helpers.initOwlCarousel)(this.slider, this.sliderOptions);
	          state = 2;
	        }
	      }.bind(this);
	      initSliders();
	      $(window).on('resize orientationchange', initSliders);
	
	      return this;
	    }
	
	    /**
	     * Initialize Calendar page scripts.
	     *
	     * @static
	     */
	
	  }], [{
	    key: 'init',
	    value: function init() {
	      var obj = new this();
	
	      obj.initSliderMobile();
	    }
	  }]);
	
	  return Calendar;
	}();
	
	exports.default = Calendar;
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ }),
/* 48 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($) {'use strict';
	
	/**
	 * Shops page scripts.
	 *
	 * @module Shops
	 */
	
	/** Import utility functions */
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	/** Import google maps class */
	
	
	var _Helpers = __webpack_require__(6);
	
	var _GoogleMaps = __webpack_require__(49);
	
	var _GoogleMaps2 = _interopRequireDefault(_GoogleMaps);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	/** Class representing a website's Shops page functions. Initialized only on web site's Shops pages. */
	var Shops = function () {
	  /**
	   * Cache Shops page DOM elements for instant access.
	   */
	  function Shops() {
	    _classCallCheck(this, Shops);
	
	    /** Desktop dropdown lists */
	    this.desktopDropDownContainers = $('.map-links-container .map-link');
	    this.desktopDropDownMaplinks = this.desktopDropDownContainers.find('.map-link-address');
	
	    /** Mobile select dropdowns */
	    this.selectDropdowns = $('.dropdown-box-container .dropdown-box');
	    this.cityDropdown = $('.dropdown-box-container .dropdown-box.city');
	    this.shopDropdown = $('.dropdown-box-container .dropdown-box.shop');
	    this.dropdownScrollContainer = $('.dropdown-box-options > .relative');
	
	    /** Map links */
	    this.mapLinks = $('.map-link-address');
	
	    /** Class names */
	    this.disabledSelectClass = 'disabled';
	
	    /** Other */
	    this.psOptions = {
	      suppressScrollX: true,
	      swipePropagation: false
	    };
	  }
	
	  /**
	   * Initialize desktop dropdown lists.
	   *
	   * @return {Shops}
	   */
	
	
	  _createClass(Shops, [{
	    key: 'initDesktopDropdowns',
	    value: function initDesktopDropdowns() {
	      this.desktopDropDownContainers.each(function (index, container) {
	        var $container = $(container);
	        var trigger = $container.find('.title');
	
	        (0, _Helpers.toggleDropdown)(trigger, $container);
	      });
	
	      return this;
	    }
	
	    /**
	     * Sort dropdowns (city, shop).
	     * Dropdowns, select one from list.
	     *
	     * @return {Shops}
	     */
	
	  }, {
	    key: 'initDropdownSelects',
	    value: function initDropdownSelects() {
	      this.selectDropdowns.each(function (index, el) {
	        var element = $(el);
	        var trigger = element.find('.dropdown-box-title');
	        (0, _Helpers.dropdownItemInit)(trigger, element);
	
	        var sortItemSelected = element.find('.dropdown-box-title > span');
	        var sortItem = element.find('.dropdown-box-option');
	        (0, _Helpers.selectOneFromListInit)(sortItem, sortItemSelected, element, 'shop.select');
	
	        sortItem.on('click tap', function () {});
	      });
	
	      return this;
	    }
	
	    /**
	     * Fire 'city change' event.
	     *
	     * @param {Number|String} cityId
	     * @param {Object} anotherParams
	     * @return {Shops}
	     */
	
	  }, {
	    key: 'fireCityChangeEvent',
	    value: function fireCityChangeEvent(cityId) {
	      var anotherParams = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
	
	      var shopDropdown = this.shopDropdown;
	      var disabledSelectClass = this.disabledSelectClass;
	
	      if (shopDropdown.hasClass(disabledSelectClass)) {
	        shopDropdown.removeClass(disabledSelectClass);
	      }
	
	      // Fire change event
	      shopDropdown.attr('data-city-id', cityId).trigger('change.city.id');
	    }
	
	    /**
	     * Select and set city ID.
	     *
	     * @return {Shops}
	     */
	
	  }, {
	    key: 'initCitySelect',
	    value: function initCitySelect() {
	      var _this = this;
	
	      var cityDropdownOptions = this.cityDropdown.find('.dropdown-box-option');
	
	      cityDropdownOptions.each(function (index, option) {
	        var $option = $(option);
	        var value = $option.data('value');
	
	        // Fire change event
	        $option.on('click tap', function () {
	          return _this.fireCityChangeEvent(value);
	        });
	      });
	
	      return this;
	    }
	
	    /**
	     * Init city select from desktop dropdown list.
	     *
	     * @retirn {Shops}
	     */
	
	  }, {
	    key: 'initCitySelectFromDesktop',
	    value: function initCitySelectFromDesktop() {
	      var _this2 = this;
	
	      this.desktopDropDownMaplinks.each(function (index, link) {
	        var $link = $(link);
	        var value = $link.parents('.map-link').data('city-id');
	        var maplinkId = $link.data('maplink-id');
	
	        var imitateSelect = function () {
	          // Select city
	          this.cityDropdown.find('.dropdown-box-option[data-value="' + value + '"]').trigger('shop.select');
	          // Select maplink
	          this.shopDropdown.find('.dropdown-box-option[data-maplink-id="' + maplinkId + '"]').trigger('shop.select');
	        }.bind(_this2);
	
	        // Fire change event
	        $link.on('click tap', function () {
	          imitateSelect();
	
	          _this2.fireCityChangeEvent(value);
	        });
	      });
	
	      return this;
	    }
	
	    /**
	     * Filter 'shops' dropdown on city select.
	     *
	     * @return {Shops}
	     */
	
	  }, {
	    key: 'initShopSortSelect',
	    value: function initShopSortSelect() {
	      this.shopDropdown.on('change.city.id', function () {
	        var $this = $(this);
	        var value = this.dataset.cityId;
	        var allOptions = $this.find('.dropdown-box-option');
	        var showThisOptions = $this.find('.dropdown-box-option[data-city-id="' + value + '"]');
	
	        // Switch cities
	        allOptions.hide();
	        showThisOptions.show();
	
	        // update PerfectScrollbar
	        var psContainer = $this.find('.ps-container');
	        psContainer.perfectScrollbar('update');
	      });
	
	      return this;
	    }
	
	    /**
	     * Init google map.
	     *
	     * @return {Shops}
	     */
	
	  }, {
	    key: 'initGoogleMap',
	    value: function initGoogleMap() {
	      var GoogleMapsInstance = new _GoogleMaps2.default({}, {}, this.mapLinks);
	      GoogleMapsInstance.init();
	
	      return this;
	    }
	
	    /**
	     * Initialize perfect scrollbar.
	     *
	     * @return {Shops}
	     */
	
	  }, {
	    key: 'initPerfectScrollbar',
	    value: function initPerfectScrollbar() {
	      var _this3 = this;
	
	      __webpack_require__.e/* nsure */(3/* duplicate */, function () {/* WEBPACK VAR INJECTION */(function($) {
	        var perfectScrollbar = __webpack_require__(20);
	
	        // init Perfect Scrollbar
	        _this3.dropdownScrollContainer.each(function (index, el) {
	          $(el).perfectScrollbar(_this3.psOptions);
	          $(window).on('resize orientationchange', function () {
	            $(el).perfectScrollbar('update');
	          });
	        });
	      
	/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(2)))});
	
	      return this;
	    }
	
	    /**
	     * Initialize Shops page scripts.
	     *
	     * @static
	     */
	
	  }], [{
	    key: 'init',
	    value: function init() {
	      var obj = new this();
	
	      obj.initDesktopDropdowns().initDropdownSelects().initCitySelect().initCitySelectFromDesktop().initShopSortSelect().initPerfectScrollbar().initGoogleMap();
	    }
	  }]);
	
	  return Shops;
	}();
	
	exports.default = Shops;
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ }),
/* 49 */
/***/ (function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($) {'use strict';
	
	/**
	 * @class GoogleMaps
	 * @classdesc Class used to initialize google maps, load markes, bind onClick event.
	 */
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var GoogleMaps = function () {
	  /**
	   * Generate and set-up default settings.
	   *
	   * @constructor
	   *
	   * @param {Object} [options]
	   * @param {Object} [markers]
	   * @param {jQuery} [clickableElements]
	   *
	   * @param {Node} [options.map] - map container node (default: document.getElementById('map'))
	   * @param {String} [options.callbackName] - function inited on ggl maps script load name (default: "initMapId1")
	   * @param {String} [options.scriptUrl] - ggl maps api script url (default: "https://maps.googleapis.com/maps/api/js")
	   * @param {String} [options.scriptKey] - ggl maps key id (default: "key=AIzaSyCrFjHOCFQbsMvKEGL92MTAffwFyHgogZI")
	   * @param {String} [options.scriptOptions] - ggl maps options added to script's url (default: `&language=ru&callback=${this.callbackName}`)
	   * @param {Object} [options.mapInitOptions] - ggl maps on create options (default: {center: {lat: 48,lng: 67.5}, zoom: 5, styles: [Array]})
	   *
	   * @param {String} [markers.image] - url to image. image will be used as marker on ggl map (default: './img/map-marker.png')
	   * @param {String} [markers.url] - url for AJAX (get markers array) (default: './ajax/googleMapMarkers.json')
	   * @param {String} [markers.eventName] - event name. used in AJAX request (default: 'markersEventName')
	   */
	  function GoogleMaps() {
	    var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
	    var markers = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
	    var clickableElements = arguments[2];
	
	    _classCallCheck(this, GoogleMaps);
	
	    this.container = options.map || document.getElementById('map');
	    this.callbackName = options.callbackName || "initMapId1";
	    this.scriptUrl = options.scriptUrl || "https://maps.googleapis.com/maps/api/js";
	    this.scriptKey = options.scriptKey || "key=AIzaSyCrFjHOCFQbsMvKEGL92MTAffwFyHgogZI";
	    this.scriptOtions = options.scriptOptions || '&language=ru&callback=' + this.callbackName;
	    this.optionsZoom = 5;
	    this.optionsCenterCords = {
	      lat: 48,
	      lng: 67.5
	    };
	    this.optionsStyle = [{
	      "featureType": "road.highway",
	      "stylers": [{ "hue": "#FFC200" }, { "saturation": -61.8 }, { "lightness": 45.599999999999994 }, { "gamma": 1 }]
	    }, {
	      "featureType": "road.arterial",
	      "stylers": [{ "hue": "#FF0300" }, { "saturation": -100 }, { "lightness": 51.19999999999999 }, { "gamma": 1 }]
	    }, {
	      "featureType": "road.local",
	      "stylers": [{ "hue": "#FF0300" }, { "saturation": -100 }, { "lightness": 52 }, { "gamma": 1 }]
	    }, {
	      "featureType": "water",
	      "stylers": [{ "hue": "#0078FF" }, { "saturation": -13.200000000000003 }, { "lightness": 2.4000000000000057 }, { "gamma": 1 }]
	    }, {
	      "featureType": "poi",
	      "stylers": [{ "hue": "#00FF6A" }, { "saturation": -1.0989010989011234 }, { "lightness": 11.200000000000017 }, { "gamma": 1 }]
	    }];
	    this.mapInitOptions = options.mapInitOptions || {
	      center: this.optionsCenterCords,
	      zoom: this.optionsZoom,
	      styles: this.optionsStyle
	    };
	
	    this.markerImage = markers.image || './img/map-marker.png';
	    this.markersUrl = markers.url || './ajax/googleMapMarkers.json';
	    this.markersEventName = markers.eventName || 'getGoogleMapsShopMarkers';
	
	    this.clickableElements = clickableElements || null;
	
	    this.markers = [];
	  }
	
	  /**
	   * Describe callback which will be called on google maps scripts load.
	   *
	   * @return {GoogleMaps}
	   */
	
	
	  _createClass(GoogleMaps, [{
	    key: 'setGoogleMapCallback',
	    value: function setGoogleMapCallback() {
	      var _this2 = this;
	
	      var _this = this;
	
	      // callback on ggl map load
	      window[this.callbackName] = function () {
	
	        //create map
	        var MapInstance = new google.maps.Map(_this2.container, _this2.mapInitOptions);
	
	        // get and set markers
	        var getAndSetMarkers = new Promise(function (resolve, reject) {
	          $.ajax({
	            url: _this2.markersUrl,
	            method: 'GET',
	            dataType: 'json',
	            data: {
	              eventName: _this2.markersEventName
	            },
	            success: function success(res) {
	              resolve(res);
	            },
	            error: function error(xhr) {
	              reject(xhr);
	            }
	          });
	        }).then(function (res) {
	          res.forEach(function (element) {
	            // marker options
	            var markerOptions = {
	              map: MapInstance,
	              position: element.position,
	              icon: _this2.markerImage,
	              title: '\u0410\u0434\u0440\u0435\u0441: ' + element.address
	            };
	
	            // create marker
	            var marker = new google.maps.Marker(markerOptions);
	
	            // Marker info window content template
	            var templateString = '<div id="mapLinkContainer">\n                                    <img id="mapLinkImage" src="' + element.image + '">\n                                    <div id="mapLinkTextContainer">\n                                      <div id="mapLinkAddress">\u0410\u0434\u0440\u0435\u0441: ' + element.address + '</div>\n                                      <div id="mapLinkPhone">\u0422\u0435\u043B\u0435\u0444\u043E\u043D: ' + element.phone + '</div>\n                                      <div id="mapLinkWorkTime">\u0420\u0435\u0436\u0438\u043C \u0440\u0430\u0431\u043E\u0442\u044B: ' + element.workTime + '</div>\n                                    </div>\n                                  </div>';
	
	            // create info window
	            marker.infoWindow = new InfoBubble({
	              content: templateString,
	              shadowStyle: 0,
	              borderColor: "transparent",
	              backgroundClassName: 'transparent',
	              padding: "10px",
	              backgroundColor: "transparent",
	              arrowSize: 0,
	              borderRadius: 0,
	              minWidth: "225px",
	              minHeight: "230px",
	              closeSrc: "./img/map-close-button.png",
	              disableAnimation: true
	            });
	
	            // Stole the ref to infowindows in array
	            _this2.markers.push(marker);
	
	            // function-handler for click/tap events on markers
	            function markerClickHandler() {
	              if (marker.infoWindow.isOpen_) {
	                marker.infoWindow.close();
	                return;
	              }
	
	              _this.markers.forEach(function (element) {
	                element.infoWindow.close();
	              });
	
	              // set map's zoom
	              MapInstance.setZoom(12);
	
	              marker.infoWindow.open(MapInstance, marker);
	            }
	
	            // add click listener to the MARKER (open infowindow, close on double-click)
	            marker.addListener('click', function () {
	              markerClickHandler();
	            });
	
	            // add click listener to the MAPLINK (open infowindow, close on double-click)
	            var thisMaplinkElements = _this2.clickableElements.filter('[data-maplink-id="' + element.mapLinkId + '"]');
	            thisMaplinkElements.on('click tap', function () {
	              markerClickHandler();
	            });
	          });
	        }).catch(function (xhr) {
	          console.log('Error is thrown. Error text:\n' + xhr.text);
	        });
	      };
	    }
	
	    /**
	     * Load google maps api v3 scripts and call callback.
	     */
	
	  }, {
	    key: 'init',
	    value: function init() {
	      var _this3 = this;
	
	      __webpack_require__.e/* nsure */(4, function () {/* WEBPACK VAR INJECTION */(function($) {
	        var InfoBubbleInstance = __webpack_require__(50);
	
	        // set callback
	        _this3.setGoogleMapCallback();
	        //append google maps api v3 script
	        $('body').append('<script async src="' + _this3.scriptUrl + '?' + _this3.scriptKey + _this3.scriptOtions + '" charset="UTF-8"></script>');
	      
	/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(2)))});
	    }
	  }]);
	
	  return GoogleMaps;
	}();
	
	exports.default = GoogleMaps;
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ })
/******/ ]);
//# sourceMappingURL=index.bundle.js.map