body.logged = user is logged in
body.not-logged = user is logged out

body.cart-active = user have items in his cart
body.user-discount-active = user have discount

//--- Страница "Корзина" ---//
'disabled' - класс для кнопок "+/- товар". если он висит на кнопке - кнопка не сработает