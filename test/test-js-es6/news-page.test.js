/** NEWS PAGE TESTS */

'use strict';

/** Tested components */
const News = require('../../src/js/pages/News.js');

/** Their module dependencies */
const bs = require('../../src/js/modules/dep/bootstrap-custom');

describe('Страница "Новости / Акции"', function () {

  const NewsPageInstance = new News.default();

  it('Работа выпадающего списка (<768px)', function () {
    const $dropdownBox = $(NewsPageInstance.dropdownBox.get(1));
    const $dropdownTrigger = $(NewsPageInstance.dropdownTrigger.get(1));

    // Инициализируем
    NewsPageInstance.initDropdownsMobile();

    if (screenWidth >= 768) this.skip();

    // Проверяем
    checkDropbox($dropdownTrigger, $dropdownBox, false);
  });

  after(() => {
    scrollElements.animate({ scrollTop: 0 }, 1);
    $body.css('overflow', 'hidden');
  });
});

