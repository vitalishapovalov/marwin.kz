/** MAIN PAGE TESTS */

'use strict';

/** Tested components */
const Main = require('../../src/js/pages/Main.js');

/** Their module dependencies */
const owl = require('../../src/js/modules/dep/owl.carousel-custom');
const bs = require('../../src/js/modules/dep/bootstrap-custom');

describe('Главная страница', function(){

  const MainPageInstance = new Main.default();

  describe('Слайдеры', function(){

    const slidersOptions = MainPageInstance.sliders;

    for (const slider in slidersOptions) {

      const sliderObject = slidersOptions[slider];
      const sliderDomElement = sliderObject.container;

      it(`Слайдер ${sliderDomElement.parents('section').attr('id')} инициализировался корректно`, function(done){
        // Инициализируем слайдер
        const sliderInitOptions = sliderObject.options;
        const sliderInitCondition = sliderObject.condition;
        Main.default.initSlider(sliderDomElement, sliderInitOptions, sliderInitCondition);

        // Индикатор инициализации слайдера
        var initedIndicator = sliderDomElement.hasClass(owlLoadedClass);

        deferredTest(() => {
          if (sliderInitCondition) {
            expect(initedIndicator, 'Слайдер не инициализировался').to.be.true;
          } else {
            expect(initedIndicator, 'Слайдер не должен был инициализироваться на этом разрешении').to.be.false;
          }

          done();
        }, 100);
      });

    }
  });

  describe('Остальное', function(){

    it('Развернуть/Свернуть контейнер с текстом - общая информация внизу страницы (<1024px)', function(){

      if (screenWidth >= 1024) {
        this.skip()
      }

      // Инициализируем разворачивание/сворачивание контейнера
      MainPageInstance.initTogglingInfo();

      // Сам контейнер
      const container = MainPageInstance.infoContainer;

      // Разворачиваем контейнер
      container.click();
      expect(container.hasClass(defaultActiveClass), 'Контейнер не раскрылся').to.be.true;

      // Сворачиваем контейнер
      container.click();
      expect(container.hasClass(defaultActiveClass), 'Контейнер не закрылся').to.be.false;
    });

    it('Ленивая подгрузка видео', function(done){

      // Инициализируем ленивую загрузку
      MainPageInstance.videoLazyloadInit();
      const videoScrollOffset = MainPageInstance.videoSection.offset().top;
      const testedElements = MainPageInstance.videoSection.find('.video-container iframe');

      // Скроллим к видео
      scrollElements.animate({scrollTop: videoScrollOffset}, 10);

      deferredTest(() => {
        testedElements.each((index, el) => {
          const elementIsLoaded = $(el).attr('src');

          expect(elementIsLoaded, 'Элемент загрузился').to.be.a('string').and.to.have.length.above(0);
        });

        done();
      }, 100);
    });
  });

  after(() => {
    scrollElements.animate({scrollTop: 0}, 1);
    $body.css('overflow', 'hidden');
  })
});