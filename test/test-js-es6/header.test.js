/** HEADER COMPONENT TESTS */

'use strict';

/** Tested components */
const Header = require('../../src/js/components/Header.js');

/** Their module dependencies */
const owl = require('../../src/js/modules/dep/owl.carousel-custom');
const bs = require('../../src/js/modules/dep/bootstrap-custom');

describe('Header', function () {

  const HeaderInstance = new Header.default();

  /** Дефолтные действия хедера (Выпадающие списки и тд). */

  describe('Действия по-умолчанию (Выпадающие списки и тд)', function() {

    function checkOneItemFromListSelect_Header(trigger, container, items, textContainer) {
      function afterOpen() {
        // Выбрать первый пункт из меню.
        const $clickedItem = $(items[0]);
        const clickedItemText = $clickedItem.text();

        // Первое нажатие
        $clickedItem.click();

        // Добавился ли класс к выбранному элементу.
        expect($clickedItem.hasClass(defaultActiveClass),
          `После выбора к выбранному элементу добавился класс ${defaultActiveClass}`).to.be.true;

        // Вывелся ли текст в превью-контейнер.
        expect($(textContainer[0]).text(),
        `После выбора, текст в превью-контейнере совпадет с текстом выбранного элемента`).to.equal(clickedItemText);

        // Закрылся ли выпадающий список после выбора.
        expect(container.hasClass(defaultActiveClass),
        `После выбора на контейнере не осталось ${defaultActiveClass}`).to.be.false;

        // Активным должен оставаться только 1 элемент.
        const activeItems = items.map((index, el) => {
          if ($(el).hasClass(defaultActiveClass)) return el
        });
        assert.lengthOf(activeItems, 1, 'После выбора только 1 элемент остался активными');
      }

      checkDropdown(trigger, container, afterOpen)
    }

    // Активируем выпадающие списки
    HeaderInstance.dropdownListsInit();

    const headerLinks = HeaderInstance.headerLinks;
    const headerLinks_toggle = HeaderInstance.headerLinks_toggle;

    it('Выпадающий список "Дополнительная информация" (только для устройств с экранами <1280px)', function () {

      if (screenWidth > 1279) {
        this.skip()
      }

      // Проверить работоспособность выпадающего списка (открыть, закрыть, закрыть при нажатии в любое место сайта).
      checkDropdown(headerLinks_toggle, headerLinks)
    });

    const cityList = HeaderInstance.city_list;
    const cityList_toggle = HeaderInstance.city_choice;

    it('Выпадающий список "Ваш город"', function() {
      // Проверить работоспособность выпадающего списка (открыть, закрыть, закрыть при нажатии в любое место сайта).
      checkDropdown(cityList_toggle, cityList)
    });

    const allCategories_toggle = HeaderInstance.allCategories_toggle;
    const allCategories = HeaderInstance.allCategories;

    it('Выпадающий список "Категория"', function() {
      // Проверить работоспособность выпадающего списка (открыть, закрыть, закрыть при нажатии в любое место сайта).
      checkDropdown(allCategories_toggle, allCategories)
    });

    // Для выпадающего списка "Личный кабинет" - отдельная функция
    HeaderInstance.userCabinetMenuInit();

    const loggedUserDropdown = HeaderInstance.loggedUserDropdown;
    const loggedUserDropdown_toggle = HeaderInstance.loggedUserDropdown_toggle;

    it('Выпадающий список "Личный кабинет"', function(){
      // Проверить работоспособность выпадающего списка (открыть, закрыть, закрыть при нажатии в любое место сайта).
      checkDropdown(loggedUserDropdown_toggle, loggedUserDropdown)
    });

    // Активируем выбор 1 элемента из списка
    HeaderInstance.selectItemFromListInit();

    const city_item = HeaderInstance.city_item;
    const city_selected = HeaderInstance.city_selected;

    it('Выбор одного элемента из списка "Ваш город"', function() {
      // Только выбранный элемент остался с активным классом, текст отобразился в превью-контейнере, меню закрылось.
      checkOneItemFromListSelect_Header(cityList_toggle, cityList, city_item, city_selected)
    });

    const category = HeaderInstance.category;
    const currentCategory = HeaderInstance.currentCategory;

    it('Выбор одного элемента из списка "Категория"', function() {
      // Только выбранный элемент остался с активным классом, текст отобразился в превью-контейнере, меню закрылось.
      checkOneItemFromListSelect_Header(allCategories_toggle, allCategories, category, currentCategory)
    });
  });

  /** Тесты поля поиска */

  describe('Поле поиска (у используемого плагина EasyAutocomplete имеются свои unit-тесты)', function(){

    let catcher;

    beforeEach(() => {
      catcher = sinon.useFakeXMLHttpRequest();
    });

    afterEach(() => {
      catcher.restore();
    });

    const search_input = HeaderInstance.search_input;
    const search_button = HeaderInstance.search_button;
    const testEvent = HeaderInstance.searchWithEnterOrButtonEventName;

    // Активируем Search Bar
    HeaderInstance.autofillInit().searchActiveStateOnFocusInit().bindSearchKeys();

    it('Поиск - кнопка поиска', function(done) {

      const testPhrase = 'new';

      catcher.onCreate = (xhr) => {
        deferredTest(() => {
          const request = JSON.parse(xhr.requestBody).phrase;

          // Проверяем фразу.
          expect(request.text, 'Искомая фраза сошлась с ожидаемой').to.equal(testPhrase);

          // Проверяем название ивента
          expect(request.event, 'Название ивента сошлось с ожидаемым').to.equal(testEvent);

          // Имитируем ответ сервера.
          xhr.respond(200, { "Content-Type": "application/json" },
            '{"text": "Sony", "website-link": "http://chis.kiev.ua/meloman/filter.html"}' );

          done();
        }, 1);
      };

      // Имитируем ввод символов
      search_input.val(testPhrase);

      // Проводим поиск
      search_button.click()
    });

    it('Поиск - кнопка поиска (по выбранной категории)', function(done) {

      const categories = HeaderInstance.category;
      const testPhrase = 'Playstation';
      const testNumber = getRandomInt(0, categories.length);

      catcher.onCreate = (xhr) => {
        deferredTest(() => {
          const request = JSON.parse(xhr.requestBody).phrase;

          // Проверяем фразу
          expect(request.text, 'Искомая фраза сошлась с ожидаемой').to.equal(testPhrase);

          // Проверяем название ивента
          expect(request.event, 'Название ивента сошлось с ожидаемым').to.equal(testEvent);

          // Проверяем категорию, по которой проводится поиск
          expect(toInt(request.category), 'ID категории соответствует ожидаемому').to.equal(testNumber + 1);

          // Имитируем ответ сервера.
          xhr.respond(200, { "Content-Type": "application/json" },
            '{"text": "Sony", "website-link": "http://chis.kiev.ua/meloman/filter.html"}' );

          done();
        }, 1);
      };

      // Имитируем ввод символов
      search_input.val(testPhrase);

      // Выбираем категорию
      categories.eq(testNumber).click();

      // Проводим поиск
      search_button.click()
    });
  });

  /** Тесты для функционала для мобильных устройств. */

  describe('Тесты функционала для мобильных устройств', function() {

    const search_trigger = HeaderInstance.search_trigger;

    it('Показать/Скрыть "Поиск" (только для устройств с экранами <1024px)', function() {

      // Активируем триггер
      HeaderInstance.toggleSearchBarOnClickInit();

      if (screenWidth > 1023) {
        this.skip()
      }

      // Нажатие на триггер.
      search_trigger.click();

      // Проверяем состояние.
      expect($body.hasClass(searchBarVisibleClass),
        `После нажатия к <body> добавился класс ${searchBarVisibleClass}`).to.be.true;

      // Повторное нажатие на триггер.
      search_trigger.click();

      // Проверяем состояние.
      expect($body.hasClass(searchBarVisibleClass),
        `После повторного нажатия у <body> не осталось класса ${searchBarVisibleClass}`).to.be.false;
    });

    it('Свернутое/Нормальное состояние хедера при скролле (только для устройств с экранами <600px)', function(done) {

      // Активируем функцию
      HeaderInstance.headerSizeToggleInit();

      if (screenWidth > 599) {
        this.skip()
      }

      setTimeout(() => {
        // Перематываем вниз.
        scrollElements.animate({scrollTop: 20}, 100);

        // После перематываня.
        deferredTest(() => {
          // Проверим состояние (хедер свернулся).
          expect($body.hasClass(smallHeaderClass),
            `После скролла к <body> добавился класс ${smallHeaderClass}`).to.be.true;

          // Перематываем "Вверх"
          scrollElements.animate({scrollTop: 0}, 100);

          // Откладываем событие.
          deferredTest(() => {
            // Проверим состояние (хедер развернулся).
            expect($body.hasClass(smallHeaderClass),
              `После скролла у <body> не осталось класса ${smallHeaderClass}`).to.be.false;

            done();
          }, 150);

        }, 150);

      }, 400)
    });
  });

  after(() => {
    scrollElements.animate({scrollTop: 0}, 1);
    $body.css('overflow', 'hidden');
  })
});