/** ORDER REGISTRATION PAGE TESTS */

'use strict';

/** Tested components */
const Registration = require('../../src/js/pages/Registration.js');

/** Their module dependencies */
const bs = require('../../src/js/modules/dep/bootstrap-custom');

describe('Страница "Оформление заказа"', function () {

  const RegistrationPageInstance = new Registration.default();

  describe('Формы', function () {
    it('Форма "Информация о доставке"', function () {
      // Смотри "Компоненты -> Validator"
    });

    it('Форма "Подпишись"', function () {
      // Смотри "Компоненты -> Validator"
    });
  });

  describe('Остальное', function () {
    it('Зависимые выпадающие списки в форме "Информация о доставке"', function() {
      const changeStateSelect = RegistrationPageInstance.changeStateSelect;
      const orderForm = RegistrationPageInstance.orderForm;
      const testedValue = 'courier';

      // Проверяем
      changeStateSelect.val(testedValue).change();

      expect(orderForm.data('method'),
        'Изменения произошли корректно').to.be.equal(testedValue);
    });

    it('Выпадающий список "Список товаров"', function () {
      const cartTrigger = RegistrationPageInstance.cartTrigger;
      const cartContainer = RegistrationPageInstance.cartContainer;

      // Инициализируем
      RegistrationPageInstance.initCartDropdown();

      // Проверяем
      checkDropbox(cartTrigger, cartContainer, false);
    });
  });

  after(() => {
    scrollElements.animate({ scrollTop: 0 }, 1);
    $body.css('overflow', 'hidden');
  });
});

