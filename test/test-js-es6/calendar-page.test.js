/** CALENDAR PAGE TESTS */

'use strict';

/** Tested components */
const Calendar = require('../../src/js/pages/Calendar.js');

/** Their module dependencies */
const owl = require('../../src/js/modules/dep/owl.carousel-custom');
const bs = require('../../src/js/modules/dep/bootstrap-custom');

describe('Страница "Календарь событий"', function () {

  const CalendarPageInstance = new Calendar.default();

  it('Инициализация слайдера', function () {
    const slider = CalendarPageInstance.slider;

    // Инициализируем
    CalendarPageInstance.initSliderMobile();

    // Проверяем
    const initedIndicator = slider.hasClass(owlLoadedClass);
    if (screenWidth < 768) {
      expect(initedIndicator, 'Слайдер инициализировался корректно').to.be.true;
    } else {
      expect(initedIndicator, 'Слайдер инициализировался корректно').to.be.false;
    }
  });

  after(() => {
    scrollElements.animate({ scrollTop: 0 }, 1);
    $body.css('overflow', 'hidden');
  });
});

