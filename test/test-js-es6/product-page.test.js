/** PRODUCT PAGE TESTS */

'use strict';

/** Tested components */
const Product = require('../../src/js/pages/Product.js');

/** Their module dependencies */
const owl = require('../../src/js/modules/dep/owl.carousel-custom');
const bs = require('../../src/js/modules/dep/bootstrap-custom');

describe('Страница единицы товара', function(){

  const ProductPageInstance = new Product.default();
  const testedProductId = ProductPageInstance.productId;

  describe('Галерея', function(){

    const mainPreviewPhoto = ProductPageInstance.previewMainPhoto;
    const previewPhotos = ProductPageInstance.previewPhotos;

    it('При наведении на мини-превью-фото установить его как основное', function(){
      // Активируем
      ProductPageInstance.initChangeMainPreviewPhotoOnHover();

      // Имитируем хавер
      const testedItem = previewPhotos.eq(2);
      testedItem.trigger('mouseover');

      // Проверяем
      const testedItemHref = testedItem.attr('data-href');

      const mainPhotoHref = mainPreviewPhoto.data('href');
      expect(mainPhotoHref, "Ссылка установилась").to.be.equal(testedItemHref);

      const mainPhotoImageHref = mainPreviewPhoto.find('img').attr('src');
      expect(mainPhotoImageHref, "src картинки установился").to.be.equal(testedItemHref);
    });

    // Активируем галерею
    ProductPageInstance.initGallery();

    it('При нажатии на превью-фото в галерее установить его как основное (в галерее)', function(){
      const galleryPreviewPhotos = ProductPageInstance.galleryPreviewPhotos;
      const galleryMainPhoto = ProductPageInstance.galleryMainPhoto;

      const testedItem = galleryPreviewPhotos.eq(2);
      const testedItemPhoto = testedItem.data('href');

      // Проверяем
      testedItem.click();
      const newPhoto = galleryMainPhoto.find('img').attr('src');

      expect(newPhoto, "Нажатая картинка установилась как основная").to.be.equal(testedItemPhoto)
    });
  });

  describe('Комментарии', function(){

    it('"Показать еще комментарии"', function(done) {
      const moreCommentsButton = ProductPageInstance.moreCommentsButton;
      const commentsContainer = ProductPageInstance.commentsContainer;
      let initialCommentsCount = commentsContainer.children().length;

      // Активируем кнопку
      ProductPageInstance.initLoadMoreCommentsButton();

      const catcher = sinon.useFakeXMLHttpRequest();
      catcher.onCreate = (xhr) => {
        deferredTest(() => {
          // Имитируем ответ сервера.
          xhr.respond(200, { "Content-Type": "application/json" },
            `[{
            "id": 5,
            "userName": "Андрей Витальевич",
            "date": "9 августа 2015",
            "rating": 3.4,
            "text": "Честно говоря, попользовался недельку и понял - дичь полнейшая. Хотя достаточно интересно посмотреть что будет дальше.",
            "likes": 1,
            "dislikes": 4 
            }]`
          );

          // Проверяем работу коллбека
          const checkButton = $body.find(moreCommentsButton);
          expect(checkButton.length, "После подгрузки комментариев кнопка была удалена").to.equal(0);
          const newCommentsCount = commentsContainer.children().length;
          expect(newCommentsCount, "Общее кол-во комментариев изменилось").to.be.equal(++initialCommentsCount);

          catcher.restore();
          done();
        }, 100);
      };

      // Нажимаем на кнопку
      moreCommentsButton.click();
    });

    it('"Лайки" на комментариях', function(done){
      const firstCommentVote = $(ProductPageInstance.votesSelectorString).first();
      const firstComment = firstCommentVote.parents('.review-item');
      const likes = firstComment.find('.likes .counter');
      const dislikes = firstComment.find('.dislikes .counter');

      // Активируем лайки
      ProductPageInstance.initCommentLikes();

      const catcher = sinon.useFakeXMLHttpRequest();
      catcher.onCreate = (xhr) => {
        deferredTest(() => {
          const passedLikesCount = 2;
          const passedDislikesCount = 3;

          // Имитируем ответ сервера.
          xhr.respond(200, { "Content-Type": "application/json" },
            `{
              "response":"success",
              "likes": ${passedLikesCount},
              "dislikes": ${passedDislikesCount}
            }`
          );

          // Проверяем работу коллбека
          expect(+likes.text(), "Кол-во лайков верное").to.be.equal(passedLikesCount);
          expect(+dislikes.text(), "Кол-во дизлайков верное").to.be.equal(passedDislikesCount);

          catcher.restore();
          done();
        }, 100);
      };

      // Нажимаем на кнопку
      firstCommentVote.click();
    });

    it('Форма "Оставить комментарий"', function() {
      const testedForm = ProductPageInstance.newReviewForm;
      const formToggle = ProductPageInstance.newReviewFormToggle;
      const formClose = ProductPageInstance.newReviewFormClose;

      // Активируем открытие/закрытие формы
      ProductPageInstance.initReviewFormToggleAndClose();

      // Проверяем открытие формы
      formToggle.click();
      expect(testedForm.is(':visible'), "Форма открылась").to.be.true;

      // Проверяем закрытие формы
      formClose.click();
      expect(testedForm.is(':visible'), "Форма закрылась").to.be.false;

      // Тестирование валидации формы: "Компоненты -> Validator"
    });
  });

  describe('Остальное', function(){
    const resolutionToInitSliders = ProductPageInstance.resolutionToInitSliders;
    const sliders = ProductPageInstance.sliders;

    // Активируем слайдеры
    ProductPageInstance.initSliders();

    // Проверяем все слайдеры
    sliders.each((index, element) => {
      const $element = $(element);

      it(`Слайдер "${$element.prev().text()}" инициализировался корректно`, function(){
        const initedIndicator = $element.hasClass('owl-loaded');

        if (resolutionToInitSliders) {
          expect(initedIndicator, 'Слайдер инициализировался').to.be.true;
        } else {
          expect(initedIndicator, 'Слайдер не инициализироваться на этом разрешении').to.be.false;
        }
      });
    });

    it('"Избранное" - добавить/убрать', function(done) {
      const favContainer = ProductPageInstance.favourite;
      const addToFavEventName = ProductPageInstance.addToFavouriteEventName;
      const rmFromFavEventName = ProductPageInstance.removeFromFavouriteEventName;
      const button = favContainer.find('a');

      // Активируем избранное
      ProductPageInstance.initFavourite();

      // Шпион за аяксом
      const ajaxSpy = sinon.spy($, 'ajax');

      const testWithPromise = new Promise((resolve, reject) => {
        // Имитируем сервер
        const catcher = sinon.useFakeXMLHttpRequest();
        catcher.onCreate = (xhr) => {
          deferredTest(() => {
            const request = xhr.requestBody;
            const reqEventData = request.split('&')[0];
            const reqEventName = reqEventData.split('=')[1];

            expect(reqEventName, "Добавлен в избранное").to.be.equal(addToFavEventName);

            xhr.respond(200, { "Content-Type": "application/json" },
              '{ "response":"success" }'
            );

            catcher.restore();
            resolve()
          }, 100)
        };

        // Добавляем в избранное
        button.click();
      });

      testWithPromise.then(() => {
        // Имитируем сервер
        const catcher = sinon.useFakeXMLHttpRequest();
        catcher.onCreate = (xhr) => {
          deferredTest(() => {
            const request = xhr.requestBody;
            const reqEventData = request.split('&')[0];
            const reqEventName = reqEventData.split('=')[1];

            expect(reqEventName, "Убран из избранного").to.be.equal(rmFromFavEventName);

            xhr.respond(200, { "Content-Type": "application/json" },
              '{ "response":"success" }'
            );

            expect(ajaxSpy.calledTwice, "AJAX был вызван 2 раза").to.be.true;

            catcher.restore();
            done();
          }, 100);
        };

        // Убираем из избранного
        button.click();
      });
    });

    it('PerfectScrollbar иницилизировался (у плагина имеются свои unit-тесты)', function(){
      const psContainer = ProductPageInstance.previewPhotosContainer;

      // Активируем PS
      ProductPageInstance.initPerfectScrollBar();

      // Проверяем наличие PerfectScrollbar ID у элемента
      psContainer.promise().then(() => {
        const psID = psContainer.data('ps-id');
        expect(psID, "PS нициализировался").to.not.be.undefined;
      });
    });

    it('Слайдер для превью-фото инициализировался корректно', function(){
      const slider = ProductPageInstance.mobileSlider;
      const resolutionToInitSlider = screenWidth < 600;

      // Activate slider
      ProductPageInstance.initMobileSlider();

      const initedIndicator = slider.hasClass('owl-loaded');

      if (resolutionToInitSlider) {
        expect(initedIndicator, 'Слайдер инициализировался').to.be.true;
      } else {
        expect(initedIndicator, 'Слайдер не инициализироваться на этом разрешении').to.be.false;
      }
    });

    it('Показать/Скрыть все характеристики', function(){
      const triggger = ProductPageInstance.moreInfotrigger;
      const classNameToCheck = ProductPageInstance.allCharacteristicsActive;

      // Активируем
      ProductPageInstance.initAllCharacteristicsToggle();

      // Нажатие на клавишу
      triggger.click();
      expect($body.hasClass(classNameToCheck), "Активный класс добавлен").to.be.true;

      // Повторное нажатие
      triggger.click();
      expect($body.hasClass(classNameToCheck), "Активный класс убран").to.be.false;
    });

    it('Добавить/Удалить из корзины', function(){
      // Смотри "Компоненты -> Common -> Единица товара -> Добавить/Удалить из корзины"
    });

    it('Валидация форм: оповещение о наличии, быстрая покупка, оповещение о наявности', function(){
      // Смотри "Компоненты -> Validator"
    });
  });

  after(() => {
    scrollElements.animate({scrollTop: 0}, 1);
    $body.css('overflow', 'hidden');
  })
});