/** VALIDATOR MODULE TESTS. */

/** Tested module */
const Validator = require('../../src/js/modules/dev/Validator.js');

describe('Validator', function () {
  const $formMail = $('.form-mail-test');
  const $formMinLength = $('.form-minlength-test');
  const $formLength = $('.form-length-test');
  const $formRadio = $('.form-radio-test');

  const validFormClass = 'validated';
  const incorrectFieldClass = 'incorrect';

  describe('Поле "Емейл"', function () {
    const testedValues = ['123', 'asd@', 'sd@agag', 'mail@mymail.com'];

    // инициализируем
    const validateEmail = new Validator.default($formMail, () => {});
    const button = validateEmail.elements.button;
    const $input = $(validateEmail.elements.inputs()[0]);

    testedValues.forEach((value, index) => {
      it(`Ввод и проверка значения ${value}`, function () {
        $input.val(value);
        button.click();
        if (index === 3) {
          expect(validateEmail.form.hasClass(validFormClass), 'Введено корректное значение').to.be.true;
        } else {
          expect(validateEmail.elements.fields().hasClass(incorrectFieldClass), 'Введено некорректное значение').to.be.true;
        }
      });
    });
  });

  describe('Поле "С минимальной длинной вводимого значения" Мин. длинна - 2 (пример)', function () {
    const testedValues = ['', 'a', 'sd'];

    // инициализируем
    const validateMinLength = new Validator.default($formMinLength, () => {});
    const button = validateMinLength.elements.button;
    const $input = $(validateMinLength.elements.inputs()[0]);

    testedValues.forEach((value, index) => {
      it(`Ввод и проверка значения ${value}`, function () {
        $input.val(value);
        button.click();
        if (index === 2) {
          expect(validateMinLength.form.hasClass(validFormClass), 'Введено корректное значение').to.be.true;
        } else {
          expect(validateMinLength.elements.fields().hasClass(incorrectFieldClass), 'Введено некорректное значение').to.be.true;
        }
      });
    });
  });

  describe('Поле "С указанной длинной вводимого значения" Длинна - 18 (пример)', function () {
    const testedValues = ['', '123123', '123 12   3', '0123456789abcdefgh'];

    // инициализируем
    const validateLength = new Validator.default($formLength, () => {});
    const button = validateLength.elements.button;
    const $input = $(validateLength.elements.inputs()[0]);

    testedValues.forEach((value, index) => {
      it(`Ввод и проверка значения ${value}`, function () {
        $input.val(value);
        button.click();
        if (index === 3) {
          expect(validateLength.form.hasClass(validFormClass), 'Введено корректное значение').to.be.true;
        } else {
          expect(validateLength.elements.fields().hasClass(incorrectFieldClass), 'Введено некорректное значение').to.be.true;
        }
      });
    });
  });

  describe('Поле с чекбоксами/радио', function () {
    // инициализируем
    const validateRadio = new Validator.default($formRadio, () => {});
    const radioButtons = validateRadio.elements.inputs();
    const button = validateRadio.elements.button;

    it('Ошибка при "0" выбранных вариантах', function () {
      // проверяем
      radioButtons.prop('checked', false);
      button.click();
      expect(validateRadio.elements.fields().hasClass(incorrectFieldClass), 'Введено некорректное значение').to.be.true;
    });

    it('Валидация успешна при "1" выбранном варианте', function () {
      // проверяем
      radioButtons.eq(0).prop('checked', true);
      button.click();
      expect(validateRadio.form.hasClass(validFormClass), 'Введено корректное значение').to.be.true;
    });
  });

  after(() => {
    scrollElements.animate({ scrollTop: 0 }, 1);
    $body.css('overflow', 'hidden');
  });
});
