/** SEARCH RESULTS PAGE TESTS */

'use strict';

/** Tested components */
const Search = require('../../src/js/pages/Search.js');

/** Their module dependencies */
const bs = require('../../src/js/modules/dep/bootstrap-custom');

describe('Страница "Результаты поиска"', function () {

  const SearchPageInstance = new Search.default();

  it('Выпадающий список "Фильтры"(<768px)', function () {
    const filterCheckboxesForm = SearchPageInstance.filterCheckboxesForm;
    const filterCheckboxesFormTrigger = SearchPageInstance.filterCheckboxesFormTrigger;

    // Инициализируем
    SearchPageInstance.initFilterDropdownsMobile();

    if (screenWidth >= 768) this.skip();

    // Проверяем
    checkDropbox(filterCheckboxesFormTrigger, filterCheckboxesForm, false);
  });

  it('Действия при выборе чекбокса', function () {
    const filterCheckboxes = SearchPageInstance.filterCheckboxes;

    // шпионим за методом 'log' объекта 'console' (на время написания теста это единственный вызваный метод)
    const logSpy = sinon.spy(console, 'log');

    // Инициализируем
    SearchPageInstance.initFiltersAction();

    // Проверяем
    filterCheckboxes.eq(0).prop('checked', true).change();
    expect(logSpy.calledThrice, 'Функция-обработчик сработала').to.be.true;
  });

  it('Форма "Товаров не найдено"', function () {
    // Смотри "Компоненты -> Validator"
  });

  after(() => {
    scrollElements.animate({ scrollTop: 0 }, 1);
    $body.css('overflow', 'hidden');
  });
});

