/** FILTER PAGE TESTS */

'use strict';

/** Tested components */
const Filter = require('../../src/js/pages/Filter.js');

/** Their module dependencies */
const owl = require('../../src/js/modules/dep/owl.carousel-custom');
const bs = require('../../src/js/modules/dep/bootstrap-custom');

describe('Страница фильтров', function(){

  const FilterPageInstance = new Filter.default();

  describe('Выпадающие списки сортировки', function(){

    // Активируем выпадающие списки
    FilterPageInstance.initSortFilter();

    FilterPageInstance.sortDropdowns.each((index, element) => {
      const $element = $(element);
      const trigger = $element.find('.dropdown-box-title');

      it(`Работа выпадающего списка "${$element.prev().text()}"`, function(){
        // Проверить работоспособность выпадающего списка (открыть, закрыть, закрыть при нажатии в любое место сайта).
        checkDropbox(trigger, $element, true)
      });

      const sortItemSelected = $element.find('.dropdown-box-title > span');
      const sortItem = $element.find('.dropdown-box-option');

      it(`Выбор одного элемента из списка "${$element.prev().text()}"`, function(){
        // Только выбранный элемент остался с активным классом, текст отобразился в превью-контейнере, меню закрылось.
        checkOneItemFromListSelect(trigger, sortItem, $element, sortItemSelected, 1)
      })
    });
  });

  describe('Выпадающие списки фильтрации', function(){

    // Активируем выпадающие списки
    FilterPageInstance.initFilterDropdowns();

    it('Работа выпадающего списка "Цена"', function(){
      const priceFilter = FilterPageInstance.mobileFilters.find('.filter-block.input-container');
      const trigger = priceFilter.find('.filter-block-title');

      // Проверить работоспособность выпадающего списка (открыть, закрыть).
      checkDropbox(trigger, priceFilter, false)
    });

    FilterPageInstance.filterDropdowns.each((index, element) => {
      const $element = $(element);
      const trigger = $element.find('.dropdown-box-title');
      const condition = screenWidth > 600;

      it(`Работа выпадающего списка "${trigger.find('span').text()}"`, function(){
        // Проверить работоспособность выпадающего списка (открыть, закрыть, закрыть при нажатии в любое место сайта).
        checkDropbox(trigger, $element, condition)
      });
    });
  });

  describe('Применить/сбросить фильтры', function(){

    // Активируем кнопки
    FilterPageInstance.bindApplyButton().bindClearButton();

    let spy;

    beforeEach(() => {
      spy = sinon.spy(console, 'log');
    });

    afterEach(() => {
      spy.restore()
    });

    it('Кнопка "Применить фильтры". Будет необходимо переписать тест после изменения функционала кнопки', function(){

      const testedButton = FilterPageInstance.applyFiltersButton[0];
      const testedButtonForm = $(testedButton).parents('form.filters');

      // Предварительно отметим пару рандомных чекбоксов (4, 7, 9)
      const checkboxes = testedButtonForm.find('input[type=checkbox]');
      [checkboxes[4], checkboxes[7], checkboxes[9]].forEach((element) => {
        element.checked = true;
      });

      // Нажатие на клавишу (первую в списке)
      testedButton.click();

      // console.log должен был вызваться 2 раза
      expect(spy.calledTwice).to.be.true;

      // первый вызов должен был произойти со следующими аргументами
      const truthyArguments = testedButtonForm.serialize().replace(/&/gi, '/');
      const [firstArgument] = spy.firstCall.args;
      expect(firstArgument).to.equal(truthyArguments);
    });

    it('Кнопка "Сбросить фильтры". Будет необходимо переписать тест после изменения функционала кнопки', function(){
      const button = FilterPageInstance.clearFiltersButton;

      // Нажатие на клавишу
      button.click();

      // console.log должен был вызваться 1 раз
      expect(spy.calledOnce).to.be.true;
    });
  });

  describe('Подгрузка и инициализация модулей', function(){

    // Активируем подгрузку модулей
    FilterPageInstance.initPlugins();

    it('PerfectScrollbar иницилизировался (у плагина имеются свои unit-тесты)', function(){
      // Проверяем каждый элемент
      FilterPageInstance.filterScrollContainer.each((index, element) => {
        const elHeight = element.scrollHeight;

        // На этом элемента PS должен был инициализироваться
        if (elHeight > 150) {
          const psID = $(element).data('ps-id');

          // Проверяем наличие PerfectScrollbar ID у элемента
          expect(psID).to.not.be.undefined;
        }
      })
    });

    it('IonRangeSlider иницилизировался (у плагина имеются свои unit-тесты)', function(){
      // Проверяем каждый элемент
      FilterPageInstance.priceSliders.each((index, slider) => {
        const $slider = $(slider);
        const sliderInstance = $slider.data('ionRangeSlider');

        // Проверяем наличие IRS
        expect(sliderInstance).to.be.an('object');
      });
    });
  });

  describe('Остальное', function(){

    it('Работа выпадающего списка с фильтрами (для устройств)', function(){
      // Активируем работу выпадающего списка
      FilterPageInstance.initFiltersToggle();

      const trigger = FilterPageInstance.filtersTrigger;
      const form = trigger.parents('form');

      // Проверить работоспособность выпадающего списка (открыть, закрыть).
      checkDropbox(trigger, form, false)
    });
  });

  after(() => {
    scrollElements.animate({scrollTop: 0}, 1);
    $body.css('overflow', 'hidden');
  })
});