/** FOOTER COMPONENT TESTS. */

/** Tested components */
const Footer = require('../../src/js/components/Footer.js');

/** Their module dependencies */
const owl = require('../../src/js/modules/dep/owl.carousel-custom');
const bs = require('../../src/js/modules/dep/bootstrap-custom');

describe('Footer', function () {

  const FooterPageInstance = new Footer.default();

  const footerUpButton = FooterPageInstance.footer_up_button;
  const footerDropDowns = FooterPageInstance.footer_toggling_item;
  const footerSlider = FooterPageInstance.footerInfo;

  /** Дефолтные действия футера (кнопка "Вверх" и тд). */

  describe('Действия по-умолчанию (кнопка "Вверх" и тд)', function(){

    it('Нажатие на кнопку "Вверх" перематывает к началу страницы.', function(done){

      // Активируем кнопку "Вверх"
      FooterPageInstance.upButtonInit();

      // Переметывает вниз
      body.scrollTop = 346;

      // Жмем кнопку "Вверх"
      footerUpButton.click();

      // Проверяем через 800 мс (время работы анимации)
      deferredTest(function(){
        expect(body.scrollTop, `Страница не промотана вверх. Отклонение на ${body.scrollTop} пикселей`).to.be.equal(0);

        done();
      }, 900);
    });

    it('Работа выпадающих списков меню (только на экранах <1280px).', function(){

      if (screenWidth > 1280) {
        this.skip()
      }

      // Активируем выпадающие списки
      FooterPageInstance.dropdownLinksInit();

      // Проверяем на работе первого элемента из списка.
      const $clickedDropdown = $(footerDropDowns[0]);

      // Первое нажатие.
      $clickedDropdown.click();

      // На контейнере должен быть указанный класс.
      expect($clickedDropdown.parent().hasClass(defaultActiveClass),
        `К контейнеру не добавился класс ${defaultActiveClass}`).to.be.true;

      // Повторное нажатие.
      $clickedDropdown.click();

      // На контейнере не должно быть указанного класса.
      expect($clickedDropdown.parent().hasClass(defaultActiveClass),
        `На контейнере остался класс ${defaultActiveClass} после закрытия`).to.be.false;
    });

    it('Слайдер инициализировался (только на экранах <1280px).', function(){
      // Тест только для устройств.
      if (screenWidth > 1280) {
        this.skip()
      }

      // Активируем слайдер
      FooterPageInstance.sliderInit();

      // Проверяем.
      expect(footerSlider.hasClass(owlLoadedClass),
      `Слайдер не инициализировался (ключевой класс ${owlLoadedClass} не был добавлен)`).to.be.true;
    });
  });

  /** Действия с формой подписки. */

  const footerSubscribeButton = FooterPageInstance.subscribeButton;
  const footerSubscribeInput = FooterPageInstance.subscribeInput;
  const footerLabel = footerSubscribeInput.parent();

  const mistakes = ['asfasag', 'email@mymail', '21231@dfsdf.asdasdasd'];
  const correctEmail = 'correctEmail@gmail.com';

  describe('Форма subscribe', function(){

    function subscribeFormDefaultState() {
      // Обнуляем значение в инпуте.
      footerSubscribeInput.val('');

      // Убираем класс - индикатор ошибки.
      footerLabel.removeClass(formErrorClass);
    }

    var xhrCatcher;

    beforeEach(() => {
      // Обновляем форму.
      subscribeFormDefaultState();
      // Фейк-сервер.
      xhrCatcher = sinon.useFakeXMLHttpRequest();
    });

    afterEach(() => {
      xhrCatcher.restore();
    });

    // Активируем валидацию подписки
    FooterPageInstance.validateSubscribeEmail();

    mistakes.forEach(function(element){
      it(`Ошибка при вводе - ${element}`, function(){
        // Записываем значение в инпут.
        footerSubscribeInput.val(element);

        // Проверяем форму.
        footerSubscribeButton.click();

        // Проверка инпута должна привести к ошибке.
        expect(footerLabel.hasClass(formErrorClass),
          `Проверка должна привести к ошибке. ${formErrorClass}`).to.be.true;
      });
    });

    it(`Введен корректный e-mail - ${correctEmail}. После отправки показывается соответствующее сообщение.`, function(done){
      // Фейк-сервер.
      xhrCatcher.onCreate = (xhr) => {
        deferredTest(() => {
          // Имитируем ответ сервера.
          xhr.respond(200, { "Content-Type": "application/json" }, '{ "response": "success" }');

          // На примере элемента, который удаляется после выполнения запроса.
          deferredTest(() => {
            var shouldntExist = document.querySelector('.hide-on-subscribe-success');
            should.not.exist(shouldntExist, 'Элемент должен был быть удален');

            done();
          }, 700);
        }, 1);
      };

      // Записываем значение в инпут.
      footerSubscribeInput.val(correctEmail);

      // Проверяем форму.
      footerSubscribeButton.click();

      // Проверка не должна привести к ошибке.
      expect(footerLabel.hasClass(formErrorClass),
        `У формы нет индикатора некорректности - ${formErrorClass}`).to.be.false;
    });
  });

  after(() => {
    scrollElements.animate({scrollTop: 0}, 1);
    $body.css('overflow', 'hidden');
  })
});