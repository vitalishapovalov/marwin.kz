/** SHOPS PAGE TESTS */

'use strict';

/** Tested components */
const Shops = require('../../src/js/pages/Shops.js');

/** Their module dependencies */
const bs = require('../../src/js/modules/dep/bootstrap-custom');

describe('Страница "Магазины"', function () {

  const ShopsPageInstance = new Shops.default();

  describe('Выпадающие списки', function () {
    it(`Работа выпадающих списков (desktop)`, function () {
      const dropdownsToTest = ShopsPageInstance.desktopDropDownContainers;

      // инициализируем
      ShopsPageInstance.initDesktopDropdowns();

      dropdownsToTest.each((index, element) => {
        const $element = $(element);
        const trigger = $element.find('.title');

        checkDropbox(trigger, $element, false);
      });
    });

    // Активируем выпадающие списки
    ShopsPageInstance.initDropdownSelects();

    ShopsPageInstance.selectDropdowns.each((index, element) => {
      const $element = $(element);
      const trigger = $element.find('.dropdown-box-title');

      it(`Работа ${index + 1} выпадающего списка "Выберите город и магазин" (devices)`, function () {
        // Проверить работоспособность выпадающего списка (открыть, закрыть, закрыть при нажатии в любое место сайта).
        checkDropbox(trigger, $element, true);
      });

      const sortItemSelected = $element.find('.dropdown-box-title > span');
      const sortItem = $element.find('.dropdown-box-option');

      it(`Выбор одного элемента из ${index + 1} списка "Выберите город и магазин" (devices)`, function () {
        // Только выбранный элемент остался с активным классом, текст отобразился в превью-контейнере, меню закрылось.
        checkOneItemFromListSelect(trigger, sortItem, $element, sortItemSelected, 1);
      });
    });

    it('"Выбор города"', function () {
      const cityDropdownOptions = ShopsPageInstance.cityDropdown.find('.dropdown-box-option');

      // инициализируем
      ShopsPageInstance.initCitySelect();

      const fireEventSpy = sinon.spy(ShopsPageInstance, 'fireCityChangeEvent');
      fireEventSpy.withArgs(cityDropdownOptions.eq(0).data('value'));

      // проверяем
      cityDropdownOptions.eq(0).click();

      expect(fireEventSpy.called, 'Вызвано событие смены города').to.be.true;
      fireEventSpy.restore();
    });

    it('"Выбор города" (desktop)', function () {
      const desktopDropDownMaplinks = ShopsPageInstance.desktopDropDownMaplinks;

      // инициализируем
      ShopsPageInstance.initCitySelectFromDesktop();

      const fireEventSpy = sinon.spy(ShopsPageInstance, 'fireCityChangeEvent');
      fireEventSpy.withArgs(desktopDropDownMaplinks.eq(0).data('value'));

      // проверяем
      desktopDropDownMaplinks.eq(0).click();

      expect(fireEventSpy.called, 'Вызвано событие смены города').to.be.true;
      fireEventSpy.restore();
    });
  });

  describe('Остальное', function () {
    it('PerfectScrollbar иницилизировался (у плагина имеются свои unit-тесты)', function () {
      const psContainer = ShopsPageInstance.dropdownScrollContainer;

      // Активируем PS
      ShopsPageInstance.initPerfectScrollbar();

      // Проверяем наличие PerfectScrollbar ID у элемента
      psContainer.promise().then(() => {
        const psID = psContainer.data('ps-id');
        expect(psID, 'PS нициализировался').to.not.be.undefined;
      });
    });

    it('Google maps', function () {
      const $map = $('#map');

      // инициализируем
      ShopsPageInstance.initGoogleMap();

      deferredTest(() => {
        expect($map.children().length, 'Карта была добавлена').to.be.equal(1);
      }, 1000);
    });
  });

  after(() => {
    scrollElements.animate({ scrollTop: 0 }, 1);
    $body.css('overflow', 'hidden');
  });
});

