/** CATALOG PAGE TESTS */

'use strict';

/** Tested components */
const Catalog = require('../../src/js/pages/Catalog.js');

/** Their module dependencies */
const owl = require('../../src/js/modules/dep/owl.carousel-custom');
const bs = require('../../src/js/modules/dep/bootstrap-custom');

describe('Страница каталога', function () {

  Catalog.default.init();

  const CatalogPageInstance = new Catalog.default();

  describe('Слайдеры', function () {

    const sliders = CatalogPageInstance.sliders;
    const resolutionToInitSliders = CatalogPageInstance.resolutionToInitSliders;

    sliders.each((index, element) => {

      const $element = $(element);

      it(`Слайдер ${$element.prev().text()} инициализировался корректно`, () => {
        const initedIndicator = $element.hasClass(owlLoadedClass);

        if (resolutionToInitSliders) {
          expect(initedIndicator, 'Слайдер инициализировался').to.be.true;
        } else {
          expect(initedIndicator, 'Слайдер не инициализироваться на этом разрешении').to.be.false;
        }
      });
    });
  });

  describe('Остальное', function () {

    const dropdowns = CatalogPageInstance.dropdowns;

    dropdowns.each((index, element) => {

      const $element = $(element);
      const trigger = $element.find('.dropdown-box-title');
      const title = trigger.find('span').text();
      const condition = screenWidth > 600;

      it(`Работа выпадающего списка - ${title}`, function () {
        // Проверить работоспособность выпадающего списка (открыть, закрыть, закрыть при нажатии в любое место сайта).
        checkDropbox(trigger, $element, condition);
      });
    });
  });

  after(() => {
    scrollElements.animate({ scrollTop: 0 }, 1);
    $body.css('overflow', 'hidden');
  });
});
