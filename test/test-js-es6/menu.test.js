/** MENU COMPONENT TESTS. */

/** Tested components */
const Menu = require('../../src/js/components/Menu.js');

/** Their module dependencies */
const owl = require('../../src/js/modules/dep/owl.carousel-custom');
const bs = require('../../src/js/modules/dep/bootstrap-custom');


describe('Menu', function () {

  const MenuInstance = new Menu.default();

  function menuIsClosed() {
    return !$(body).hasClass(menuIsOpenedClass)
  }

  function closeMenuIfOpened() {
    if (!menuIsClosed()) $(body).removeClass(menuIsOpenedClass)
  }

  function openMenu() {
    if (menuIsClosed()) $(body).addClass(menuIsOpenedClass)
  }

  /** Дефолтные действия меню (открыть/закрыть и тд). */

  describe('Действия по-умолчанию (открыть/закрыть и тд)', function() {

    const blackOverlay = MenuInstance.menu_bg;
    const menuToggle = MenuInstance.menu_toggle;

    // Активируем кнопку открытия/закрытия меню
    MenuInstance.toggleMenuOnClick();

    it('После загрузки страницы меню находится в закрытом состоянии.', function () {
      expect(menuIsClosed(), 'Меню закрыто при загрузке страницы').to.be.true;
    });

    it('Меню открывается при нажатии на кнопку "Каталог товаров" (меню).', function () {
      // Закрыть меню, если открыто
      closeMenuIfOpened();

      // Нажать на триггер меню
      menuToggle.click();

      expect(menuIsClosed(), 'Меню открылось').to.be.false;
    });

    it('Меню закрывается при повторном нажатии на кнопку "Каталог товаров" (меню).', function () {
      // Закрыть меню, если открыто
      closeMenuIfOpened();

      // Открыть меню
      openMenu();

      // Нажать на триггер меню
      menuToggle.click();

      expect(menuIsClosed(), 'Меню закрылось').to.be.true;
    });

    it('Меню закрывается по клику на черный оверлей.', function(){
      // Активируем функцию закрытия по нажатию на черный оверлей
      MenuInstance.blackOverlayClickHandler();

      // Закрыть меню, если открыто
      closeMenuIfOpened();

      // Открыть меню
      openMenu();

      // Нажать на черный оверлей
      blackOverlay.click();

      expect(menuIsClosed(), 'Меню закрылось').to.be.true;
    });

    it('Меню закрывается при клике в любой части сайта, кроме самого меню (>600px)', function(){

      function test() {
        // Закрыть меню, если открыто
        closeMenuIfOpened();

        // Открыть меню
        openMenu();

        // нажать на хедер (случайно выбранная цель)
        header.click();
      }

      if (screenWidth > 600) {
        // Активируем функцию закрытия по нажатию в любую точку экрана
        MenuInstance.closeMenuOnBodyClick();

        test();

        // Должно закрываться на экранах > 600px
        expect(menuIsClosed(), 'Меню закрылось').to.be.true;
      } else {
        test();

        // Не должно закрываться на экранах < 600px
        expect(menuIsClosed(), 'Меню не закрылось').to.be.false;
      }

    });

  });

  /** Действия с подкатегорями. */

  const categoryButtons = MenuInstance.menu_firstLevel;
  const subCategoryButtons = MenuInstance.menu_secondLevel;

  describe('Действия подкатегорий', function() {

    function checkOpenedSubMenu(subMenuClass, clickedCategory, checkOpened, event) {

      if (checkOpened) {
        if (!$body.hasClass(subMenuClass)) {
          throw new Error('К <body> не добавился класс ' + subMenuClass + '\nEvent: ' + event)
        }

        if (!$(clickedCategory).hasClass(defaultActiveClass)) {
          throw new Error('К нажатой категории не добавился класс ' + defaultActiveClass + '\nEvent: ' + event)
        }
      } else {
        if ($body.hasClass(subMenuClass)) {
          throw new Error('После закрытия меню, у <body> остался класс ' + subMenuClass + '\nEvent: ' + event)
        }

        if ($(clickedCategory).hasClass(defaultActiveClass)) {
          throw new Error('У нажатой категории остался класс ' + defaultActiveClass + '\nEvent: ' + event)
        }
      }
    }

    function closeAllSubmenus() {
      $body.removeClass(`${subMenuIsOpenedClass} ${subSubMenuIsOpenedClass}`);

      [subCategoryButtons, categoryButtons].forEach((buttons) => {
        $(buttons).each((index, button) => {
          $(button).removeClass(defaultActiveClass)
        });
      });
    }

    function closeAll() {
      closeAllSubmenus();
      closeMenuIfOpened();
    }

    function openSubMenu(button, subMenuClass, event, buttonToCheck) {
      if (typeof buttonToCheck === 'undefined') {
        buttonToCheck = button
      }

      if (event == 'click') {
        $(button).click()
      } else {
        button.dispatchEvent(new Event(event))
      }

      checkOpenedSubMenu(subMenuClass, buttonToCheck, true, event);
    }

    function openSubCategoryMenu(index) {
      // Открыть меню, если закрыто
      openMenu();

      // Нажать на первый элемент в списке подкатегорий
      openSubMenu(categoryButtons[index], subMenuIsOpenedClass, 'click');
    }

    function openSubSubCategoryMenu(index) {
      const subMenuButton = categoryButtons[index].querySelectorAll('.category-secondLevel');
      const subMenuHoverTarget = subMenuButton[index].querySelector('span');

      if (screenWidth <= 1280) {
        // Для любых устройств, кроме ПК и ноутбуков - on click
        openSubMenu(subMenuButton[index], subSubMenuIsOpenedClass, 'click');
      } else {
        // Для ПК и ноутбуков - on mouseover
        openSubMenu(subMenuHoverTarget, subSubMenuIsOpenedClass, 'mouseover', subMenuButton[index]);
      }

      return subMenuButton[index]
    }

    function checkAndClose() {
      // Проверить, не закрылось ли меню
      if (menuIsClosed()) throw new Error('Меню закрылось (а не должно было)');

      // Закрыть все
      closeAll();
    }

    // Активируем функции ответственные за подкатегории
    MenuInstance.openSubCatMenuInit().openSubSubCatMenuInit().subSubMenuOnHoverInit().bindBackButtons();

    it('Список подкатегорий открывается по клику на категорию (1-й подуровень меню)', function(){
      // Открыть меню и первую категорию в списке категорий.
      openSubCategoryMenu(0);

      // Проверить, не закрылось ли меню и закрыть его.
      checkAndClose();
    });

    it('Кнопка "Назад" в 1-м подуровне меню закрывает 1-й подуровень меню.', function(){
      // Открыть меню и первую категорию в списке категорий.
      openSubCategoryMenu(0);

      // Нажать на кнопку "Назад" в этой же категории
      const backButton = categoryButtons[0].querySelector('.category-secondLevel-back');
      $(backButton).click();

      // Проверить, закрылось ли подменю
      checkOpenedSubMenu(subMenuIsOpenedClass, categoryButtons[0], false, 'click');

      // Проверить, не закрылось ли меню и закрыть его.
      checkAndClose();
    });

    it('Повторное нажатие на категорию закрывает ее.', function(){
      // Открыть меню и первую категорию в списке категорий.
      openSubCategoryMenu(0);

      // Повторное нажатие
      $(categoryButtons[0]).click();

      // Проверить, закрылось ли подменю
      checkOpenedSubMenu(subMenuIsOpenedClass, categoryButtons[0], false, 'click');

      // Проверить, не закрылось ли меню и закрыть его.
      checkAndClose();
    });

    it('Список под-подкатегорий открывается по клику (для >1280px - по хаверу) на под-категорию (2-й подуровень меню)', function(){
      // Открыть меню и первую категорию в списке категорий.
      openSubCategoryMenu(0);

      // Открыть первую подкатегорию в списке под-подкатегорию.
      openSubSubCategoryMenu(0);

      // Проверить, не закрылось ли меню и закрыть его.
      checkAndClose();
    });

    it('Кнопка "Закрыть" во 2-м подуровне меню закрывает 2-й подуровень меню (только для устройств с экранами <1281px)', function(){

      if (screenWidth > 1280) {
        this.skip()
      }

      // Открыть меню и первую категорию в списке категорий.
      openSubCategoryMenu(0);

      // Открыть первую подкатегорию в списке под-подкатегорию.
      var subSubMenuButton = openSubSubCategoryMenu(0);

      // Нажать на кнопку "Закрыть"
      var closeButton = $(subSubMenuButton).find('.category-thirdLevel-title > svg');
      closeButton[0].dispatchEvent(new Event('click'));

      // Убедиться что под-подменю закрылось, а подменю - нет
      checkOpenedSubMenu(subSubMenuIsOpenedClass, subSubMenuButton[0], false);
      checkOpenedSubMenu(subMenuIsOpenedClass, categoryButtons[0], true);

      // Проверить, не закрылось ли меню и закрыть его.
      checkAndClose();
    });

    it('Повторное нажатие на под-категорию закрывает 2-й подуровень меню (тест для <1280px экранов).', function(){

      if (screenWidth > 1280) {
        this.skip()
      }

      // Открыть меню и первую категорию в списке категорий.
      openSubCategoryMenu(0);

      var subMenuButton = categoryButtons[0].querySelectorAll('.category-secondLevel');

      // Нажать на под-категорию
      openSubMenu(subMenuButton[0], subSubMenuIsOpenedClass, 'click');

      // Повторное нажатие
      subMenuButton[0].dispatchEvent(new Event('click'));

      // Убедиться что под-подменю закрылось, а подменю - нет
      checkOpenedSubMenu(subSubMenuIsOpenedClass, subMenuButton[0], false);
      checkOpenedSubMenu(subMenuIsOpenedClass, categoryButtons[0], true);

      // Проверить, не закрылось ли меню и закрыть его.
      checkAndClose();
    });

    it('После закрытия меню закрываются все подменю.', function(){
      // Открыть меню и первую категорию в списке категорий.
      openSubCategoryMenu(0);

      // Открыть первую подкатегорию в списке под-подкатегорию.
      openSubSubCategoryMenu(0);

      var subMenuButton = categoryButtons[0].querySelectorAll('.category-secondLevel');

      // Закрыть меню.
      MenuInstance.menu_toggle.click();

      // Убедиться, что все закрылось.
      checkOpenedSubMenu(subMenuIsOpenedClass, categoryButtons[0], false);
      checkOpenedSubMenu(subSubMenuIsOpenedClass, subMenuButton[0], false);
    });
  });

  after(() => {
    scrollElements.animate({scrollTop: 0}, 1);
    $body.css('overflow', 'hidden');
  })
});