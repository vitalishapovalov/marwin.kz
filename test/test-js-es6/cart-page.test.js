/** CART PAGE TESTS */

'use strict';

/** Tested components */
const Cart = require('../../src/js/pages/Cart.js');

/** Their module dependencies */
const owl = require('../../src/js/modules/dep/owl.carousel-custom');
const bs = require('../../src/js/modules/dep/bootstrap-custom');
const getCheckedLength = require('../../src/js/modules/dev/Helpers').getCheckedLength;

describe('Страница "Корзина"', function () {

  const CartPageInstance = new Cart.default();

  describe('Смена кол-ва единицы товара', function () {
    // Инициализируем
    CartPageInstance.initProductCountChangeListener();

    let catcher;

    beforeEach(() => {
      catcher = sinon.useFakeXMLHttpRequest();
    });

    afterEach(() => {
      catcher.restore();
    });

    let testedItemIndex = 0;

    it('Увеличить на 1', function (done) {
      const testedPlus = CartPageInstance.countIncrement.eq(testedItemIndex);
      const testedInput = CartPageInstance.countInput.eq(testedItemIndex);
      const currentValue = testedInput.val();
      const responseValue = '2';

      const ajaxSpy = sinon.spy($, 'ajax');

      catcher.onCreate = (xhr) => {
        deferredTest(() => {
          // Имитируем ответ от сервера
          xhr.respond(200, { 'Content-Type': 'application/json' },
            `{
              "item": {
                "quantity": ${responseValue},
                "totalPrice_txt": "13 200 т",
                "totalDiscount_txt": "Скидка 1000 т"
              },
              "basket": {
                "totalPrice_txt": "26 000",
                "totalDiscount_txt": "1 500",
                "totalPriceToPay_txt": "24 500"
              }
            }`);

          expect(testedInput.val(),
              'Установленно корректное текущее кол-во единицы товара').to.be.equal(responseValue);

          ajaxSpy.restore();
          done();
        }, 100);
      };

      // Проверяем
      testedPlus.click();

      const requestData = ajaxSpy.lastCall.args[0].data;
      expect(requestData.quantity,
        'Корректное новое кол-во').to.be.equal(+currentValue + 1);
      expect(requestData.itemId,
        'Изменения произошли в корректной единице товара').to.be.equal(testedItemIndex);
    });

    it('Уменьшить на 1', function (done) {
      const testedMinus = CartPageInstance.countDecrement.eq(testedItemIndex);
      const testedInput = CartPageInstance.countInput.eq(testedItemIndex);
      const disabledClass = CartPageInstance.buttonIsDisabled;
      const currentValue = testedInput.val();
      const responseValue = '1';

      const ajaxSpy = sinon.spy($, 'ajax');

      catcher.onCreate = (xhr) => {
        deferredTest(() => {
          // Имитируем ответ от сервера
          xhr.respond(200, { 'Content-Type': 'application/json' },
            `{
              "item": {
                "quantity": ${responseValue},
                "totalPrice_txt": "13 200 т",
                "totalDiscount_txt": "Скидка 1000 т"
              },
              "basket": {
                "totalPrice_txt": "26 000",
                "totalDiscount_txt": "1 500",
                "totalPriceToPay_txt": "24 500"
              }
            }`);

          expect(testedInput.val(),
            'Установленно корректное текущее кол-во единицы товара').to.be.equal(responseValue);
          expect(testedMinus.hasClass(disabledClass),
            'Кнопка "-" недоступна').to.be.true;

          ajaxSpy.restore();
          done();
        }, 100);
      };

      // Проверяем
      testedMinus.click();

      const requestData = ajaxSpy.lastCall.args[0].data;
      expect(requestData.quantity,
        'Корректное новое кол-во').to.be.equal(+currentValue - 1);
      expect(requestData.itemId,
        'Изменения произошли в корректной единице товара').to.be.equal(testedItemIndex);
    });

    testedItemIndex = 1;

    it('Ввод произвольного значения', function (done) {
      const testedInput = CartPageInstance.countInput.eq(testedItemIndex);
      const testedValue = '20';

      const ajaxSpy = sinon.spy($, 'ajax');

      catcher.onCreate = (xhr) => {
        deferredTest(() => {
          // Имитируем ответ от сервера
          xhr.respond(200, { 'Content-Type': 'application/json' },
            `{
              "item": {
                "quantity": ${testedValue},
                "totalPrice_txt": "13 200 т",
                "totalDiscount_txt": "Скидка 1000 т"
              },
              "basket": {
                "totalPrice_txt": "26 000",
                "totalDiscount_txt": "1 500",
                "totalPriceToPay_txt": "24 500"
              }
            }`);

          expect(testedInput.val(),
            'Установленно корректное текущее кол-во единицы товара').to.be.equal(testedValue);

          ajaxSpy.restore();
          done();
        }, 100);
      };

      // Проверяем
      testedInput.val(testedValue).trigger('input');

      const requestData = ajaxSpy.lastCall.args[0].data;
      expect(requestData.quantity,
        'Корректное новое кол-во').to.be.equal(testedValue);
      expect(requestData.itemId,
        'Изменения произошли в корректной единице товара').to.be.equal(testedItemIndex);
    });
  });

  describe('Выбор / Снятие выбора с продуктов в корзине', function () {
    // Инициализируем
    CartPageInstance.initCheckboxes();

    const mainCheckbox = CartPageInstance.mainCheckbox;
    const checkboxes = CartPageInstance.itemCheckBoxes;

    it('Выбор всех чекбоксов по клику на главный чекбокс', function () {
      const displayChangeSpy = sinon.spy(CartPageInstance, 'displaySelectedProducts');

      // Проверяем
      mainCheckbox.change();
      expect(getCheckedLength(checkboxes()), 'Выбрать всё').to.be.equal(checkboxes().length);
      expect(displayChangeSpy.calledOnce,
        'Функция подсчета и отображения выделенных элементов была вызвана').to.be.true;
      expect(displayChangeSpy.getCall(0).args[0],
        'Функция подсчета и отображения выделенных элементов была вызвана с корректными аргументами').to.be.equal(checkboxes().length);

      displayChangeSpy.restore();
    });

    it('Снятие выбора со всех чекбоксов по клику на главный чекбокс', function () {
      const displayChangeSpy = sinon.spy(CartPageInstance, 'displaySelectedProducts');

      // Проверяем
      mainCheckbox.change();
      expect(getCheckedLength(checkboxes()), 'Снять всё').to.be.equal(0);
      expect(displayChangeSpy.calledOnce,
        'Функция подсчета и отображения выделенных элементов была вызвана').to.be.true;
      expect(displayChangeSpy.getCall(0).args[0],
        'Функция подсчета и отображения выделенных элементов была вызвана с корректными аргументами').to.be.equal(0);

      displayChangeSpy.restore();
    });

    it('Выбор всех чекбоксов по очереди', function () {
      const displayChangeSpy = sinon.spy(CartPageInstance, 'displaySelectedProducts');

      checkboxes().each((index, element) => {
        $(element).prop('checked', true).change();

        expect(displayChangeSpy.callCount,
          'Функция подсчета и отображения выделенных элементов была вызвана корректное количество раз').to.be.equal(index + 1);

        if (!(checkboxes().length === index + 1)) {
          expect(mainCheckbox.prop('checked'),
            `Основной чекбокс не был выбран`).to.be.false;
        }
      });

      expect(mainCheckbox.prop('checked'),
        'Основной чекбокс был выбран после выбора ВСЕХ чекбоксов').to.be.true;

      displayChangeSpy.restore();

      // Снять выбор со всех чекбоксов
      mainCheckbox.change();
    });
  });

  describe('Удалить / В отложенные / Добавить', function () {
    // Инициализируем
    CartPageInstance.initCartActions();

    let catcher;

    beforeEach(() => {
      catcher = sinon.useFakeXMLHttpRequest();
    });

    afterEach(() => {
      catcher.restore();
    });

    const actionButtons = CartPageInstance.cartActionButtons;
    const checkboxes = CartPageInstance.itemCheckBoxes;
    const primeItemsCount = checkboxes().length;

    actionButtons.each((index, button) => {
      const $button = $(button);

      it(`${$button.find('span').text()}`, function (done) {
        const eventName = $button.data('eventName');

        // Выберем 1-й чекбокс из списка
        const checkbox = checkboxes().first();
        checkbox.prop('checked', true);

        const ajaxSpy = sinon.spy($, 'ajax');

        catcher.onCreate = (xhr) => {
          deferredTest(() => {
            xhr.respond(200, { 'Content-Type': 'application/json' },
              `{
                "response":"success",
                "basket": {
                  "totalPrice_txt": "26 000",
                  "totalDiscount_txt": "1 500",
                  "totalPriceToPay_txt": "24 500"
                }
              }`);
 
            const currentCount = checkboxes().length;
            expect(currentCount,
              'Кол-во продуктов в корзине изменилось корректно').to.be.equal(primeItemsCount - (index + 1));

            ajaxSpy.restore();
            done();
          }, 100);
        };

        // Проверяем
        $button.click();
        expect(ajaxSpy.lastCall.args[0].data.eventName,
          'Название события совпадает с ожидаемым').to.be.equal(eventName);
      });
    });
  });

  describe('Бонус / Промо', function () {
    beforeEach(() => {
      this.catcher = sinon.useFakeXMLHttpRequest();
    });

    afterEach(() => {
      this.catcher.restore();
    });

    function checkField(button, field, value) {
      const input = field.find('input');

      const ajaxSpy = sinon.spy($, 'ajax');

      // Проверяем (пустое поле)
      button.click();
      expect(field.hasClass(formErrorClass), 'Ошибка при вводе пустого значения').to.be.true;

      // Проверяем (вводим значение)
      input.val(value);
      button.click();
      expect(field.hasClass(formErrorClass), 'Отсутствие ошибки при вводе корректного значения').to.be.false;
      expect(ajaxSpy.calledOnce, 'За время теста AJAX был вызван 1 раз').to.be.true;

      ajaxSpy.restore();
    }

    // Инициализируем
    CartPageInstance.initBonusAndPromo();

    it('Поле "Бонус"', function () {
      const bonusButton = CartPageInstance.sendBonusButton;
      const bonusField = CartPageInstance.bonusField;

      checkField(bonusButton, bonusField, 222);
    });

    it('Поле "Промо"', function () {
      const promoButton = CartPageInstance.sendPromoButton;
      const promoField = CartPageInstance.promoField;

      checkField(promoButton, promoField, 'xgf97');
    });

    it('Смена значений после их указания (кнопка "Изменить")', function () {
      const changeValueButtons = CartPageInstance.changeValueButtons;

      // Инициализируем
      CartPageInstance.initBonusAndPromoChange();

      const ajaxSpy = sinon.spy($, 'ajax');

      changeValueButtons.each((index, element) => {
        const $element = $(element);

        $element.click();

        const thisCallEventExpectedName = $element.data('event-name');
        const thisCallEventName = ajaxSpy.lastCall.args[0].data.event;
        expect(thisCallEventName, 'AJAX вызван с корректными аргументами').to.be.equal(thisCallEventExpectedName);
      });

      expect(ajaxSpy.calledTwice, 'AJAX был вызван 2 раза').to.be.true;
    });
  });

  describe('Остальное', function () {
    it('Выпадающий список "Подарок к заказу"', function () {
      const presentsDropdownTrigger = CartPageInstance.presentsDropdownTrigger;
      const presentsContainer = CartPageInstance.presentsContainer;

      // Инициализируем
      CartPageInstance.initPresentsDropdown();

      // Проверяем
      checkDropbox(presentsDropdownTrigger, presentsContainer, false);
    });

    it('Слайдеры инициализировались корректно', function () {
      const sliders = CartPageInstance.sliders;

      // Инициализируем
      CartPageInstance.initSliders();

      // Проверяем
      const sliderInitialized = sliders.hasClass(owlLoadedClass);
      const expectMessage = 'Слайдер инициализировался корректно';

      if (screenWidth < 1903) {
        expect(sliderInitialized, expectMessage).to.be.true;
      } else {
        expect(sliderInitialized, expectMessage).to.be.false;
      }
    });
  });

  after(() => {
    scrollElements.animate({ scrollTop: 0 }, 1);
    $body.css('overflow', 'hidden');
  });
});

