/** COMMON COMPONENTS TESTS */

'use strict';

/** Tested components */
const Common = require('../../src/js/components/Common.js');
const ProductItem = require('../../src/js/components/ProductItem.js');

/** Their module dependencies */
const owl = require('../../src/js/modules/dep/owl.carousel-custom');
const bs = require('../../src/js/modules/dep/bootstrap-custom');
const Main = require('../../src/js/pages/Main.js');
const MainPageInstance = new Main.default();

describe('Действия (компонентов) по умолчанию', function() {

  const CommonInstance = new Common.default();

  describe('Корзина', function() {
    it('Загружаются текущие товары в корзине (если они есть))', function(done) {
      const catcher = sinon.useFakeXMLHttpRequest();

      catcher.onCreate = (xhr) => {
        deferredTest(() => {
          // Имитируем ответ сервера.
          xhr.respond(200, { "Content-Type": "application/json" }, '{ "totalAmount": "5", "totalPrice": "23657" }');

          done();
        }, 1)
      };

      // Активируем функцию загрузки
      CommonInstance.initCart();
    });
  });

  describe('Остальное', function() {
    it('Накладывается маска на все поля вводе номера телефона', function() {

      // Активируем накладывание масок
      CommonInstance.initPhoneMasks();

      CommonInstance.phoneInputs.each((index, element) => {
        const mask = $(element).data('rawMaskFn');

        expect(mask, 'На элемент наложилась маска').to.be.a('function');
      });
    });

    it('Выпадающий список хлебных крошек', function() {

      // Активируем хлебные крошки
      CommonInstance.breadcrumbsDropdownsInit();

      const firstBreadCrumb = $('.breadcrumb-item').first();
      const breadcrumbsTrigger = firstBreadCrumb.find('.breadcrumb-title');

      checkDropdown(breadcrumbsTrigger, firstBreadCrumb)
    });
  });
});

describe('Единица товара', function() {

  const ProductItemInstance = new ProductItem.default();

  describe('Ленивая загрузка картинок продуктов', function() {

    // Активируем ленивую подгрузку
    ProductItemInstance.imageLazyloadInit();

    it('Картинка (<img />) продукта начинает загружаться только после того как продукт попадает на превью слайдера (<=1280px)', function(done) {

      if (screenWidth > 1280) {
        this.skip()
      }

      // Инициализируем слайдеры
      const sliders = MainPageInstance.sliders;
      MainPageInstance.initSliders(sliders);

      // Убедиться, что слайдер инициализировался.
      const slider = sliders.superPrice.container;
      expect(slider.hasClass(owlLoadedClass), 'Слайдер инициализировался').to.be.true;

      const testedProduct = slider.find('.owl-item.active').last().next();
      const testedItem = testedProduct.find('.default-item');
      const testedProductImage = testedProduct.find('.default-item-preview > img');

      // Убедиться, что картинка еще не загружена
      expect(testedProductImage.prop('src'),
        `Картинка у продукта с индексом - 
        ${testedProduct.index()}
         - загрузилась раньше времени. Возможно, Вам следует проскроллить страницу к началу и обновить ее`).to.have.length(0);

      // Перемотать слайдер к продукту.
      const sliderNextButton = slider.find('.owl-next');
      sliderNextButton.click();

      // Откладываем проверку.
      deferredTest(() => {
        // Наличие аттрибута 'src'
        expect(testedProductImage.prop('src'),
          `К картинке продукта с индексом -
          ${testedProduct.index()}
          - не добавился аттрибут "src"`).to.exist.and.to.have.length.above(0);

        // Наличие класса 'loaded'
        expect(testedItem.hasClass(imageIsLoadedClass),
          `К продукту с индексом -
          ${testedProduct.index()} 
          - не добавился класс ${imageIsLoadedClass}`).to.be.true;

        done();
      }, 100);
    });

    it('Картинка (<img />) продукта начинает загружаться только после того, как до продукта доскроллили (>1280px)', function(done) {

      if (screenWidth <= 1280) {
        this.skip()
      }

      // Тестируем на 10-м продукте из списка.
      const testedProductIndex = 9;
      const testedProduct = ProductItemInstance.items().eq(testedProductIndex);
      const testedProductImage = testedProduct.find('.default-item-preview > img');

      // Убедиться, что картинка еще не загружена
      expect(testedProductImage.prop('src'),
        `Картинка у продукта с индексом - 
        ${testedProductIndex}
         - загрузилась раньше времени. Возможно, Вам следует проскроллить страницу к началу и обновить ее`).to.have.length(0);

      // Скроллим к продукту.
      scrollElements.animate({scrollTop: testedProduct.offset().top + 200}, 50);

      // Откладываем проверку.
      deferredTest(() => {
        // Наличие аттрибута 'src'
        expect(testedProductImage.prop('src'),
          `К картинке продукта с индексом -
          ${testedProductIndex}
          - не добавился аттрибут "src"`).to.exist.and.to.have.length.above(0);

        // Наличие класса 'loaded'
        expect(testedProduct.hasClass(imageIsLoadedClass),
          `К продукту с индексом -
          ${testedProductIndex} 
          - не добавился класс ${imageIsLoadedClass}`).to.be.true;

        done();
      }, 200);
    });

  });

  describe('Добавить/Удалить из корзины', function() {

    const randomNumber = getRandomInt(0, commonItems().length);

    // Тестируем на случайном товаре.
    const testedProduct = $(commonItems().get(randomNumber));
    const buyButton = testedProduct.find('.default-item-buy');
    const removeButton = testedProduct.find('.default-item-purchased');

    // Данные тестируемого объекта.
    const testedProductId = testedProduct.data('id');
    const testedProductPrice = testedProduct.data('price');

    // Отпарсить запрос и проверить на ошибки.
    function beforeResponse(xhr, testedEvent) {
      const params = xhr.requestBody.split('&');

      let paramsObj = {};

      params.forEach((el) => {
        const parts = el.split('=');

        paramsObj[parts[0]] = parts[1];
      });

      // корректный ли 'event'
      if (testedEvent != paramsObj.event) {
        throw new Error(`В запросе неверный параметр "event". Ожидался - ${testedEvent}. Был получен - ${paramsObj.event}`)
      }

      // корректный ли 'id'
      if (testedProductId != paramsObj.id) {
        throw new Error(`В запросе неверный параметр "id". Ожидался - ${testedProductId}. Был получен - ${paramsObj.id}`)
      }

      // корректный ли 'price'
      if (testedProductPrice != paramsObj.price) {
        throw new Error(`В запросе неверный параметр "id". Ожидался - ${testedProductPrice}. Был получен - ${paramsObj.price}`)
      }

      // Переменные для проверки корректности цены/кол-ва
      const cartContainer = $('.header-cart.header-action-item');
      const currentCount = cartContainer.find('.badge.cart').text();
      const currentPrice = cartContainer.find('.subtitle').text();

      let totalPriceShouldBe, totalAmountShouldBe;

      switch (testedEvent) {
        case buyProductEventName: {
          totalPriceShouldBe = toInt(currentPrice) + toInt(testedProductPrice);
          totalAmountShouldBe = toInt(currentCount) + 1;

          break
        }
        case removeProductEventName: {
          totalPriceShouldBe = toInt(currentPrice) - toInt(testedProductPrice);
          totalAmountShouldBe = toInt(currentCount) - 1;

          break
        }
      }

      return {
        container: cartContainer,
        priceShouldBe: totalPriceShouldBe,
        amountShouldBe: totalAmountShouldBe
      }
    }

    // Проверить соответствие цены/кол-ва товаров.
    function afterResponse(cart) {
      // Только что установленные цена/кол-во.
      const newAmount = cart.container.find('.badge.cart').text();
      const newPrice = cart.container.find('.subtitle').text();

      // Проверяем, корректно ли изменилась цена.
      if (cart.priceShouldBe != toInt(newPrice)) {
        throw new Error(`Общая цена товаров в корзине не сошлась. Ожидалась - ${cart.priceShouldBe}. Была получена - ${newPrice}`)
      }

      // Проверяем, корректно ли изменилост кол-во.
      if (cart.amountShouldBe != toInt(newAmount)) {
        throw new Error(`Общее кол-во товаров в корзине не сошлось. Ожидалось - ${cart.amountShouldBe}. Было получено - ${newAmount}`)
      }
    }

    // Проверить наличие/отсутствие тех или иных классов.
    function checkCompletePendingAdded(complete, pending, added) {
      const completedClass = testedProduct.hasClass(productCompletedClass);
      const pendingClass = testedProduct.hasClass(productPendingClass);
      const addedClass = testedProduct.hasClass(productAddedClass);

      complete = complete ? !completedClass : completedClass;
      pending = pending ? !pendingClass : pendingClass;

      if (complete || pending) {
        throw new Error('Классы на товаре изменились некорректно')
      }

      if (typeof added != 'string') {
        added = added ? !addedClass : addedClass;

        if (added) {
          throw new Error('Классы на товаре изменились некорректно')
        }
      }
    }

    // Активируем кнопки
    ProductItemInstance.initBuyButton();

    it('Добавить товар в корзину по нажатию на кнопку "Купить"', function(done) {
      let xhrCatcher = sinon.useFakeXMLHttpRequest();

      xhrCatcher.onCreate = (xhr) => {
        deferredTest(() => {
          // Проверяем на ошибки запрос.
          var cart = beforeResponse(xhr, buyProductEventName);

          // Проверить корретно ли изменились классы.
          checkCompletePendingAdded(false, true, 'notImportant');

          // Имитируем ответ сервера.
          xhr.respond(200, { "Content-Type": "application/json" }, '{ "response": "success" }');

          // Проверяем состояние элемента после ответа сервера (и, соответственно, коллбэка)
          afterResponse(cart);

          // Проверить корретно ли изменились классы.
          checkCompletePendingAdded(true, false, true);

          xhrCatcher.restore();

          done();
        }, 1)
      };

      // Нажать на кнопку "Покупки"
      buyButton.click();
    });

    it('Убрать товар из корзины по нажатию на кнопку "Убрать"', function(done) {
      let xhrCatcher = sinon.useFakeXMLHttpRequest();

      xhrCatcher.onCreate = (xhr) => {
        deferredTest(() => {
          // Проверяем на ошибки запрос.
          var cart = beforeResponse(xhr, removeProductEventName);

          // Проверить корретно ли изменились классы.
          checkCompletePendingAdded(false, true, 'notImportant');

          // Имитируем ответ сервера.
          xhr.respond(200, { "Content-Type": "application/json" }, '{ "response": "success" }');

          // Проверяем состояние элемента после ответа сервера (и, соответственно, коллбэка)
          afterResponse(cart);

          // Проверить корретно ли изменились классы.
          checkCompletePendingAdded(true, false, false);

          xhrCatcher.restore();

          done();
        }, 1)
      };

      // Нажать на кнопку "Покупки"
      removeButton.click();
    });
  });

  after(() => {
    scrollElements.animate({scrollTop: 0}, 1);
    $body.css('overflow', 'hidden');
  })
});