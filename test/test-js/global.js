window.webpackManifest = {
  "0": "calendar-page.test.js",
  "1": "cart-page.test.js",
  "2": "catalog-page.test.js",
  "3": "common.test.js",
  "4": "chunks/chunk-4-385f8e01d31f5c4f4792.js",
  "5": "filter-page.test.js",
  "6": "chunks/chunk-6-7a1814cdda834ac9d27b.js",
  "7": "footer.test.js",
  "8": "header.test.js",
  "9": "main-page.test.js",
  "10": "menu.test.js",
  "11": "news-page.test.js",
  "12": "product-page.test.js",
  "13": "chunks/chunk-13-03b29cfe291f3db3dced.js",
  "14": "registration-page.test.js",
  "15": "search-page.test.js",
  "16": "shops-page.test.js",
  "17": "chunks/chunk-17-f88525c7d37c6e153726.js",
  "18": "validator.test.js"
};

var body = document.body;
var main = body.querySelector('main');
var header = body.querySelector('header');
var footer = body.querySelector('footer');

var defaultActiveClass = 'active';
var formErrorClass = 'incorrect';
var owlLoadedClass = 'owl-loaded';
var buyProductEventName = 'buyCart';
var removeProductEventName = 'removeCart';
var imageIsLoadedClass = 'loaded';
var productPendingClass = 'pending';
var productAddedClass = 'added';
var productCompletedClass = 'completed';
var menuIsOpenedClass = 'menu-opened';
var subMenuIsOpenedClass = 'menu-2';
var subSubMenuIsOpenedClass = 'menu-3';
var activeSearchInputClass = 'input-active';
var searchBarVisibleClass =  'search-active';
var smallHeaderClass =  'header-small';

var screenWidth = window.innerWidth;
var assert = chai.assert;
var expect = chai.expect;
var should = chai.should();

var $body = $(body);
var $header = $(header);
var scrollElements = $('body, html');

var deferredTest = function(onDone, timeout) {
  var dfd = $.Deferred();

  dfd.done(onDone);

  setTimeout(function(){
    dfd.resolve()
  }, timeout)
};

var afterAllTests = function() {
  $body.css('overflow', 'hidden')
};

var commonItems = function() {
  return $('.default-item')
};

var toInt = function(value) {
  return parseInt(value, 10)
};

var getRandomInt = function (min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
};

var checkDropdown = function(trigger, container, cb1) {
  // Открыть выпадающее меню.
  trigger.click();
  // Открылся ли контейнер.
  if (!container.hasClass(defaultActiveClass)) {
    throw new Error('После нажатия на триггер контейнеру не добавился класс ' + defaultActiveClass)
  } else if (cb1 !== undefined) {
    cb1.call(null);
    return
  }
  // Закрыть выпадающее меню повторным нажатием.
  trigger.click();
  // Закрылся ли контейнер.
  if (container.hasClass(defaultActiveClass)) {
    throw new Error('После нажатия триггера на контейнере остался класс ' + defaultActiveClass)
  }
  // Закрытие выпадающего списка при нажатии в любую точку сайта, кроме самого меню.
  trigger.click();
  $header.click();
  // Закрылся ли контейнер.
  if (container.hasClass(defaultActiveClass)) {
    throw new Error('После нажатия на header на контейнере остался класс ' + defaultActiveClass)
  }
};

var checkDropbox = function(trigger, container, condition) {
  // Открыть выпадающее меню.
  trigger.click();
  // Открылся ли контейнер.
  if (!container.hasClass(defaultActiveClass)) {
    throw new Error('После нажатия на триггер контейнеру не добавился класс ' + defaultActiveClass)
  }
  // Закрыть выпадающее меню повторным нажатием.
  trigger.click();
  // Закрылся ли контейнер.
  if (container.hasClass(defaultActiveClass)) {
    throw new Error('После нажатия триггера на контейнере остался класс ' + defaultActiveClass)
  }

  if (condition) {
    // Закрытие выпадающего списка при нажатии в любую точку сайта, кроме самого меню.
    trigger.click();
    $header.click();
    // Закрылся ли контейнер.
    if (container.hasClass(defaultActiveClass)) {
      throw new Error('После нажатия на header на контейнере остался класс ' + defaultActiveClass)
    }
  } else {
    // Закрытие выпадающего списка при нажатии в любую точку сайта, кроме самого меню.
    trigger.click();
    $header.click();
    // Закрылся ли контейнер.
    if (!container.hasClass(defaultActiveClass)) {
      throw new Error('После нажатия на header на контейнере не осталось класса ' + defaultActiveClass)
    }
  }
};

var checkOneItemFromListSelect = function(trigger, sortItem, container, textContainer, index) {
  // открыть список
  trigger.click();

  // Выбрать первый пункт из меню.
  const testedItem = $(sortItem[index]);
  const testedItemText = testedItem.text();
  testedItem.click();

  // Добавился ли класс к выбранному элементу.
  if (!testedItem.hasClass(defaultActiveClass)) {
    throw new Error('После выбора к выбранному элементу добавился класс ' + defaultActiveClass)
  }

  // Вывелся ли текст в превью-контейнер.
  if (textContainer.text() != testedItemText) {
    throw new Error('После выбора, текст в превью-контейнере совпадет с текстом выбранного элемента')
  }

  // Закрылся ли выпадающий список после выбора.
  if (container.hasClass(defaultActiveClass)) {
    throw new Error('После выбора контейнер не закрылся')
  }
};