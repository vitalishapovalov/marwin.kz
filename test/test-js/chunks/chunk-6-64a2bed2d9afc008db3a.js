webpackJsonp([6,13],Array(33).concat([
/* 33 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;// Ion.RangeSlider
	// version 2.1.4 Build: 355
	// © Denis Ineshin, 2016
	// https://github.com/IonDen
	//
	// Project page:    http://ionden.com/a/plugins/ion.rangeSlider/en.html
	// GitHub page:     https://github.com/IonDen/ion.rangeSlider
	//
	// Released under MIT licence:
	// http://ionden.com/a/plugins/licence-en.html
	// =====================================================================================================================
	
	(function (factory) {
	    if (true) {
	        !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(11)], __WEBPACK_AMD_DEFINE_RESULT__ = function ($) {
	            factory($, document, window, navigator);
	        }.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	    } else {
	        factory(jQuery, document, window, navigator);
	    }
	} (function ($, document, window, navigator, undefined) {
	    "use strict";
	
	    // =================================================================================================================
	    // Service
	
	    var plugin_count = 0;
	
	    // IE8 fix
	    var is_old_ie = (function () {
	        var n = navigator.userAgent,
	            r = /msie\s\d+/i,
	            v;
	        if (n.search(r) > 0) {
	            v = r.exec(n).toString();
	            v = v.split(" ")[1];
	            if (v < 9) {
	                $("html").addClass("lt-ie9");
	                return true;
	            }
	        }
	        return false;
	    } ());
	    if (!Function.prototype.bind) {
	        Function.prototype.bind = function bind(that) {
	
	            var target = this;
	            var slice = [].slice;
	
	            if (typeof target != "function") {
	                throw new TypeError();
	            }
	
	            var args = slice.call(arguments, 1),
	                bound = function () {
	
	                    if (this instanceof bound) {
	
	                        var F = function(){};
	                        F.prototype = target.prototype;
	                        var self = new F();
	
	                        var result = target.apply(
	                            self,
	                            args.concat(slice.call(arguments))
	                        );
	                        if (Object(result) === result) {
	                            return result;
	                        }
	                        return self;
	
	                    } else {
	
	                        return target.apply(
	                            that,
	                            args.concat(slice.call(arguments))
	                        );
	
	                    }
	
	                };
	
	            return bound;
	        };
	    }
	    if (!Array.prototype.indexOf) {
	        Array.prototype.indexOf = function(searchElement, fromIndex) {
	            var k;
	            if (this == null) {
	                throw new TypeError('"this" is null or not defined');
	            }
	            var O = Object(this);
	            var len = O.length >>> 0;
	            if (len === 0) {
	                return -1;
	            }
	            var n = +fromIndex || 0;
	            if (Math.abs(n) === Infinity) {
	                n = 0;
	            }
	            if (n >= len) {
	                return -1;
	            }
	            k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);
	            while (k < len) {
	                if (k in O && O[k] === searchElement) {
	                    return k;
	                }
	                k++;
	            }
	            return -1;
	        };
	    }
	
	
	
	    // =================================================================================================================
	    // Template
	
	    var base_html =
	        '<span class="irs">' +
	        '<span class="irs-line" tabindex="-1"><span class="irs-line-left"></span><span class="irs-line-mid"></span><span class="irs-line-right"></span></span>' +
	        '<span class="irs-min">0</span><span class="irs-max">1</span>' +
	        '<span class="irs-from">0</span><span class="irs-to">0</span><span class="irs-single">0</span>' +
	        '</span>' +
	        '<span class="irs-grid"></span>' +
	        '<span class="irs-bar"></span>';
	
	    var single_html =
	        '<span class="irs-bar-edge"></span>' +
	        '<span class="irs-shadow shadow-single"></span>' +
	        '<span class="irs-slider single"></span>';
	
	    var double_html =
	        '<span class="irs-shadow shadow-from"></span>' +
	        '<span class="irs-shadow shadow-to"></span>' +
	        '<span class="irs-slider from"><span></span></span>' +
	        '<span class="irs-slider to"><span></span></span>';
	
	    var disable_html =
	        '<span class="irs-disable-mask"></span>';
	
	
	
	    // =================================================================================================================
	    // Core
	
	    /**
	     * Main plugin constructor
	     *
	     * @param input {Object} link to base input element
	     * @param options {Object} slider config
	     * @param plugin_count {Number}
	     * @constructor
	     */
	    var IonRangeSlider = function (input, options, plugin_count) {
	        this.VERSION = "2.1.4";
	        this.input = input;
	        this.plugin_count = plugin_count;
	        this.current_plugin = 0;
	        this.calc_count = 0;
	        this.update_tm = 0;
	        this.old_from = 0;
	        this.old_to = 0;
	        this.old_min_interval = null;
	        this.raf_id = null;
	        this.dragging = false;
	        this.force_redraw = false;
	        this.no_diapason = false;
	        this.is_key = false;
	        this.is_update = false;
	        this.is_start = true;
	        this.is_finish = false;
	        this.is_active = false;
	        this.is_resize = false;
	        this.is_click = false;
	
	        // cache for links to all DOM elements
	        this.$cache = {
	            win: $(window),
	            body: $(document.body),
	            input: $(input),
	            cont: null,
	            rs: null,
	            min: null,
	            max: null,
	            from: null,
	            to: null,
	            single: null,
	            bar: null,
	            line: null,
	            s_single: null,
	            s_from: null,
	            s_to: null,
	            shad_single: null,
	            shad_from: null,
	            shad_to: null,
	            edge: null,
	            grid: null,
	            grid_labels: []
	        };
	
	        // storage for measure variables
	        this.coords = {
	            // left
	            x_gap: 0,
	            x_pointer: 0,
	
	            // width
	            w_rs: 0,
	            w_rs_old: 0,
	            w_handle: 0,
	
	            // percents
	            p_gap: 0,
	            p_gap_left: 0,
	            p_gap_right: 0,
	            p_step: 0,
	            p_pointer: 0,
	            p_handle: 0,
	            p_single_fake: 0,
	            p_single_real: 0,
	            p_from_fake: 0,
	            p_from_real: 0,
	            p_to_fake: 0,
	            p_to_real: 0,
	            p_bar_x: 0,
	            p_bar_w: 0,
	
	            // grid
	            grid_gap: 0,
	            big_num: 0,
	            big: [],
	            big_w: [],
	            big_p: [],
	            big_x: []
	        };
	
	        // storage for labels measure variables
	        this.labels = {
	            // width
	            w_min: 0,
	            w_max: 0,
	            w_from: 0,
	            w_to: 0,
	            w_single: 0,
	
	            // percents
	            p_min: 0,
	            p_max: 0,
	            p_from_fake: 0,
	            p_from_left: 0,
	            p_to_fake: 0,
	            p_to_left: 0,
	            p_single_fake: 0,
	            p_single_left: 0
	        };
	
	
	
	        /**
	         * get and validate config
	         */
	        var $inp = this.$cache.input,
	            val = $inp.prop("value"),
	            config, config_from_data, prop;
	
	        // default config
	        config = {
	            type: "single",
	
	            min: 10,
	            max: 100,
	            from: null,
	            to: null,
	            step: 1,
	
	            min_interval: 0,
	            max_interval: 0,
	            drag_interval: false,
	
	            values: [],
	            p_values: [],
	
	            from_fixed: false,
	            from_min: null,
	            from_max: null,
	            from_shadow: false,
	
	            to_fixed: false,
	            to_min: null,
	            to_max: null,
	            to_shadow: false,
	
	            prettify_enabled: true,
	            prettify_separator: " ",
	            prettify: null,
	
	            force_edges: false,
	
	            keyboard: false,
	            keyboard_step: 5,
	
	            grid: false,
	            grid_margin: true,
	            grid_num: 4,
	            grid_snap: false,
	
	            hide_min_max: false,
	            hide_from_to: false,
	
	            prefix: "",
	            postfix: "",
	            max_postfix: "",
	            decorate_both: true,
	            values_separator: " — ",
	
	            input_values_separator: ";",
	
	            disable: false,
	
	            onStart: null,
	            onChange: null,
	            onFinish: null,
	            onUpdate: null
	        };
	
	
	
	        // config from data-attributes extends js config
	        config_from_data = {
	            type: $inp.data("type"),
	
	            min: $inp.data("min"),
	            max: $inp.data("max"),
	            from: $inp.data("from"),
	            to: $inp.data("to"),
	            step: $inp.data("step"),
	
	            min_interval: $inp.data("minInterval"),
	            max_interval: $inp.data("maxInterval"),
	            drag_interval: $inp.data("dragInterval"),
	
	            values: $inp.data("values"),
	
	            from_fixed: $inp.data("fromFixed"),
	            from_min: $inp.data("fromMin"),
	            from_max: $inp.data("fromMax"),
	            from_shadow: $inp.data("fromShadow"),
	
	            to_fixed: $inp.data("toFixed"),
	            to_min: $inp.data("toMin"),
	            to_max: $inp.data("toMax"),
	            to_shadow: $inp.data("toShadow"),
	
	            prettify_enabled: $inp.data("prettifyEnabled"),
	            prettify_separator: $inp.data("prettifySeparator"),
	
	            force_edges: $inp.data("forceEdges"),
	
	            keyboard: $inp.data("keyboard"),
	            keyboard_step: $inp.data("keyboardStep"),
	
	            grid: $inp.data("grid"),
	            grid_margin: $inp.data("gridMargin"),
	            grid_num: $inp.data("gridNum"),
	            grid_snap: $inp.data("gridSnap"),
	
	            hide_min_max: $inp.data("hideMinMax"),
	            hide_from_to: $inp.data("hideFromTo"),
	
	            prefix: $inp.data("prefix"),
	            postfix: $inp.data("postfix"),
	            max_postfix: $inp.data("maxPostfix"),
	            decorate_both: $inp.data("decorateBoth"),
	            values_separator: $inp.data("valuesSeparator"),
	
	            input_values_separator: $inp.data("inputValuesSeparator"),
	
	            disable: $inp.data("disable")
	        };
	        config_from_data.values = config_from_data.values && config_from_data.values.split(",");
	
	        for (prop in config_from_data) {
	            if (config_from_data.hasOwnProperty(prop)) {
	                if (!config_from_data[prop] && config_from_data[prop] !== 0) {
	                    delete config_from_data[prop];
	                }
	            }
	        }
	
	
	
	        // input value extends default config
	        if (val) {
	            val = val.split(config_from_data.input_values_separator || options.input_values_separator || ";");
	
	            if (val[0] && val[0] == +val[0]) {
	                val[0] = +val[0];
	            }
	            if (val[1] && val[1] == +val[1]) {
	                val[1] = +val[1];
	            }
	
	            if (options && options.values && options.values.length) {
	                config.from = val[0] && options.values.indexOf(val[0]);
	                config.to = val[1] && options.values.indexOf(val[1]);
	            } else {
	                config.from = val[0] && +val[0];
	                config.to = val[1] && +val[1];
	            }
	        }
	
	
	
	        // js config extends default config
	        $.extend(config, options);
	
	
	        // data config extends config
	        $.extend(config, config_from_data);
	        this.options = config;
	
	
	
	        // validate config, to be sure that all data types are correct
	        this.validate();
	
	
	
	        // default result object, returned to callbacks
	        this.result = {
	            input: this.$cache.input,
	            slider: null,
	
	            min: this.options.min,
	            max: this.options.max,
	
	            from: this.options.from,
	            from_percent: 0,
	            from_value: null,
	
	            to: this.options.to,
	            to_percent: 0,
	            to_value: null
	        };
	
	
	
	        this.init();
	    };
	
	    IonRangeSlider.prototype = {
	
	        /**
	         * Starts or updates the plugin instance
	         *
	         * @param is_update {boolean}
	         */
	        init: function (is_update) {
	            this.no_diapason = false;
	            this.coords.p_step = this.convertToPercent(this.options.step, true);
	
	            this.target = "base";
	
	            this.toggleInput();
	            this.append();
	            this.setMinMax();
	
	            if (is_update) {
	                this.force_redraw = true;
	                this.calc(true);
	
	                // callbacks called
	                this.callOnUpdate();
	            } else {
	                this.force_redraw = true;
	                this.calc(true);
	
	                // callbacks called
	                this.callOnStart();
	            }
	
	            this.updateScene();
	        },
	
	        /**
	         * Appends slider template to a DOM
	         */
	        append: function () {
	            var container_html = '<span class="irs js-irs-' + this.plugin_count + '"></span>';
	            this.$cache.input.before(container_html);
	            this.$cache.input.prop("readonly", true);
	            this.$cache.cont = this.$cache.input.prev();
	            this.result.slider = this.$cache.cont;
	
	            this.$cache.cont.html(base_html);
	            this.$cache.rs = this.$cache.cont.find(".irs");
	            this.$cache.min = this.$cache.cont.find(".irs-min");
	            this.$cache.max = this.$cache.cont.find(".irs-max");
	            this.$cache.from = this.$cache.cont.find(".irs-from");
	            this.$cache.to = this.$cache.cont.find(".irs-to");
	            this.$cache.single = this.$cache.cont.find(".irs-single");
	            this.$cache.bar = this.$cache.cont.find(".irs-bar");
	            this.$cache.line = this.$cache.cont.find(".irs-line");
	            this.$cache.grid = this.$cache.cont.find(".irs-grid");
	
	            if (this.options.type === "single") {
	                this.$cache.cont.append(single_html);
	                this.$cache.edge = this.$cache.cont.find(".irs-bar-edge");
	                this.$cache.s_single = this.$cache.cont.find(".single");
	                this.$cache.from[0].style.visibility = "hidden";
	                this.$cache.to[0].style.visibility = "hidden";
	                this.$cache.shad_single = this.$cache.cont.find(".shadow-single");
	            } else {
	                this.$cache.cont.append(double_html);
	                this.$cache.s_from = this.$cache.cont.find(".from");
	                this.$cache.s_to = this.$cache.cont.find(".to");
	                this.$cache.shad_from = this.$cache.cont.find(".shadow-from");
	                this.$cache.shad_to = this.$cache.cont.find(".shadow-to");
	
	                this.setTopHandler();
	            }
	
	            if (this.options.hide_from_to) {
	                this.$cache.from[0].style.display = "none";
	                this.$cache.to[0].style.display = "none";
	                this.$cache.single[0].style.display = "none";
	            }
	
	            this.appendGrid();
	
	            if (this.options.disable) {
	                this.appendDisableMask();
	                this.$cache.input[0].disabled = true;
	            } else {
	                this.$cache.cont.removeClass("irs-disabled");
	                this.$cache.input[0].disabled = false;
	                this.bindEvents();
	            }
	
	            if (this.options.drag_interval) {
	                this.$cache.bar[0].style.cursor = "ew-resize";
	            }
	        },
	
	        /**
	         * Determine which handler has a priority
	         * works only for double slider type
	         */
	        setTopHandler: function () {
	            var min = this.options.min,
	                max = this.options.max,
	                from = this.options.from,
	                to = this.options.to;
	
	            if (from > min && to === max) {
	                this.$cache.s_from.addClass("type_last");
	            } else if (to < max) {
	                this.$cache.s_to.addClass("type_last");
	            }
	        },
	
	        /**
	         * Determine which handles was clicked last
	         * and which handler should have hover effect
	         *
	         * @param target {String}
	         */
	        changeLevel: function (target) {
	            switch (target) {
	                case "single":
	                    this.coords.p_gap = this.toFixed(this.coords.p_pointer - this.coords.p_single_fake);
	                    break;
	                case "from":
	                    this.coords.p_gap = this.toFixed(this.coords.p_pointer - this.coords.p_from_fake);
	                    this.$cache.s_from.addClass("state_hover");
	                    this.$cache.s_from.addClass("type_last");
	                    this.$cache.s_to.removeClass("type_last");
	                    break;
	                case "to":
	                    this.coords.p_gap = this.toFixed(this.coords.p_pointer - this.coords.p_to_fake);
	                    this.$cache.s_to.addClass("state_hover");
	                    this.$cache.s_to.addClass("type_last");
	                    this.$cache.s_from.removeClass("type_last");
	                    break;
	                case "both":
	                    this.coords.p_gap_left = this.toFixed(this.coords.p_pointer - this.coords.p_from_fake);
	                    this.coords.p_gap_right = this.toFixed(this.coords.p_to_fake - this.coords.p_pointer);
	                    this.$cache.s_to.removeClass("type_last");
	                    this.$cache.s_from.removeClass("type_last");
	                    break;
	            }
	        },
	
	        /**
	         * Then slider is disabled
	         * appends extra layer with opacity
	         */
	        appendDisableMask: function () {
	            this.$cache.cont.append(disable_html);
	            this.$cache.cont.addClass("irs-disabled");
	        },
	
	        /**
	         * Remove slider instance
	         * and ubind all events
	         */
	        remove: function () {
	            this.$cache.cont.remove();
	            this.$cache.cont = null;
	
	            this.$cache.line.off("keydown.irs_" + this.plugin_count);
	
	            this.$cache.body.off("touchmove.irs_" + this.plugin_count);
	            this.$cache.body.off("mousemove.irs_" + this.plugin_count);
	
	            this.$cache.win.off("touchend.irs_" + this.plugin_count);
	            this.$cache.win.off("mouseup.irs_" + this.plugin_count);
	
	            if (is_old_ie) {
	                this.$cache.body.off("mouseup.irs_" + this.plugin_count);
	                this.$cache.body.off("mouseleave.irs_" + this.plugin_count);
	            }
	
	            this.$cache.grid_labels = [];
	            this.coords.big = [];
	            this.coords.big_w = [];
	            this.coords.big_p = [];
	            this.coords.big_x = [];
	
	            cancelAnimationFrame(this.raf_id);
	        },
	
	        /**
	         * bind all slider events
	         */
	        bindEvents: function () {
	            if (this.no_diapason) {
	                return;
	            }
	
	            this.$cache.body.on("touchmove.irs_" + this.plugin_count, this.pointerMove.bind(this));
	            this.$cache.body.on("mousemove.irs_" + this.plugin_count, this.pointerMove.bind(this));
	
	            this.$cache.win.on("touchend.irs_" + this.plugin_count, this.pointerUp.bind(this));
	            this.$cache.win.on("mouseup.irs_" + this.plugin_count, this.pointerUp.bind(this));
	
	            this.$cache.line.on("touchstart.irs_" + this.plugin_count, this.pointerClick.bind(this, "click"));
	            this.$cache.line.on("mousedown.irs_" + this.plugin_count, this.pointerClick.bind(this, "click"));
	
	            if (this.options.drag_interval && this.options.type === "double") {
	                this.$cache.bar.on("touchstart.irs_" + this.plugin_count, this.pointerDown.bind(this, "both"));
	                this.$cache.bar.on("mousedown.irs_" + this.plugin_count, this.pointerDown.bind(this, "both"));
	            } else {
	                this.$cache.bar.on("touchstart.irs_" + this.plugin_count, this.pointerClick.bind(this, "click"));
	                this.$cache.bar.on("mousedown.irs_" + this.plugin_count, this.pointerClick.bind(this, "click"));
	            }
	
	            if (this.options.type === "single") {
	                this.$cache.single.on("touchstart.irs_" + this.plugin_count, this.pointerDown.bind(this, "single"));
	                this.$cache.s_single.on("touchstart.irs_" + this.plugin_count, this.pointerDown.bind(this, "single"));
	                this.$cache.shad_single.on("touchstart.irs_" + this.plugin_count, this.pointerClick.bind(this, "click"));
	
	                this.$cache.single.on("mousedown.irs_" + this.plugin_count, this.pointerDown.bind(this, "single"));
	                this.$cache.s_single.on("mousedown.irs_" + this.plugin_count, this.pointerDown.bind(this, "single"));
	                this.$cache.edge.on("mousedown.irs_" + this.plugin_count, this.pointerClick.bind(this, "click"));
	                this.$cache.shad_single.on("mousedown.irs_" + this.plugin_count, this.pointerClick.bind(this, "click"));
	            } else {
	                this.$cache.single.on("touchstart.irs_" + this.plugin_count, this.pointerDown.bind(this, null));
	                this.$cache.single.on("mousedown.irs_" + this.plugin_count, this.pointerDown.bind(this, null));
	
	                this.$cache.from.on("touchstart.irs_" + this.plugin_count, this.pointerDown.bind(this, "from"));
	                this.$cache.s_from.on("touchstart.irs_" + this.plugin_count, this.pointerDown.bind(this, "from"));
	                this.$cache.to.on("touchstart.irs_" + this.plugin_count, this.pointerDown.bind(this, "to"));
	                this.$cache.s_to.on("touchstart.irs_" + this.plugin_count, this.pointerDown.bind(this, "to"));
	                this.$cache.shad_from.on("touchstart.irs_" + this.plugin_count, this.pointerClick.bind(this, "click"));
	                this.$cache.shad_to.on("touchstart.irs_" + this.plugin_count, this.pointerClick.bind(this, "click"));
	
	                this.$cache.from.on("mousedown.irs_" + this.plugin_count, this.pointerDown.bind(this, "from"));
	                this.$cache.s_from.on("mousedown.irs_" + this.plugin_count, this.pointerDown.bind(this, "from"));
	                this.$cache.to.on("mousedown.irs_" + this.plugin_count, this.pointerDown.bind(this, "to"));
	                this.$cache.s_to.on("mousedown.irs_" + this.plugin_count, this.pointerDown.bind(this, "to"));
	                this.$cache.shad_from.on("mousedown.irs_" + this.plugin_count, this.pointerClick.bind(this, "click"));
	                this.$cache.shad_to.on("mousedown.irs_" + this.plugin_count, this.pointerClick.bind(this, "click"));
	            }
	
	            if (this.options.keyboard) {
	                this.$cache.line.on("keydown.irs_" + this.plugin_count, this.key.bind(this, "keyboard"));
	            }
	
	            if (is_old_ie) {
	                this.$cache.body.on("mouseup.irs_" + this.plugin_count, this.pointerUp.bind(this));
	                this.$cache.body.on("mouseleave.irs_" + this.plugin_count, this.pointerUp.bind(this));
	            }
	        },
	
	        /**
	         * Mousemove or touchmove
	         * only for handlers
	         *
	         * @param e {Object} event object
	         */
	        pointerMove: function (e) {
	            if (!this.dragging) {
	                return;
	            }
	
	            var x = e.pageX || e.originalEvent.touches && e.originalEvent.touches[0].pageX;
	            this.coords.x_pointer = x - this.coords.x_gap;
	
	            this.calc();
	        },
	
	        /**
	         * Mouseup or touchend
	         * only for handlers
	         *
	         * @param e {Object} event object
	         */
	        pointerUp: function (e) {
	            if (this.current_plugin !== this.plugin_count) {
	                return;
	            }
	
	            if (this.is_active) {
	                this.is_active = false;
	            } else {
	                return;
	            }
	
	            this.$cache.cont.find(".state_hover").removeClass("state_hover");
	
	            this.force_redraw = true;
	
	            if (is_old_ie) {
	                $("*").prop("unselectable", false);
	            }
	
	            this.updateScene();
	            this.restoreOriginalMinInterval();
	
	            // callbacks call
	            if ($.contains(this.$cache.cont[0], e.target) || this.dragging) {
	                this.is_finish = true;
	                this.callOnFinish();
	            }
	            
	            this.dragging = false;
	        },
	
	        /**
	         * Mousedown or touchstart
	         * only for handlers
	         *
	         * @param target {String|null}
	         * @param e {Object} event object
	         */
	        pointerDown: function (target, e) {
	            e.preventDefault();
	            var x = e.pageX || e.originalEvent.touches && e.originalEvent.touches[0].pageX;
	            if (e.button === 2) {
	                return;
	            }
	
	            if (target === "both") {
	                this.setTempMinInterval();
	            }
	
	            if (!target) {
	                target = this.target;
	            }
	
	            this.current_plugin = this.plugin_count;
	            this.target = target;
	
	            this.is_active = true;
	            this.dragging = true;
	
	            this.coords.x_gap = this.$cache.rs.offset().left;
	            this.coords.x_pointer = x - this.coords.x_gap;
	
	            this.calcPointerPercent();
	            this.changeLevel(target);
	
	            if (is_old_ie) {
	                $("*").prop("unselectable", true);
	            }
	
	            this.$cache.line.trigger("focus");
	
	            this.updateScene();
	        },
	
	        /**
	         * Mousedown or touchstart
	         * for other slider elements, like diapason line
	         *
	         * @param target {String}
	         * @param e {Object} event object
	         */
	        pointerClick: function (target, e) {
	            e.preventDefault();
	            var x = e.pageX || e.originalEvent.touches && e.originalEvent.touches[0].pageX;
	            if (e.button === 2) {
	                return;
	            }
	
	            this.current_plugin = this.plugin_count;
	            this.target = target;
	
	            this.is_click = true;
	            this.coords.x_gap = this.$cache.rs.offset().left;
	            this.coords.x_pointer = +(x - this.coords.x_gap).toFixed();
	
	            this.force_redraw = true;
	            this.calc();
	
	            this.$cache.line.trigger("focus");
	        },
	
	        /**
	         * Keyborard controls for focused slider
	         *
	         * @param target {String}
	         * @param e {Object} event object
	         * @returns {boolean|undefined}
	         */
	        key: function (target, e) {
	            if (this.current_plugin !== this.plugin_count || e.altKey || e.ctrlKey || e.shiftKey || e.metaKey) {
	                return;
	            }
	
	            switch (e.which) {
	                case 83: // W
	                case 65: // A
	                case 40: // DOWN
	                case 37: // LEFT
	                    e.preventDefault();
	                    this.moveByKey(false);
	                    break;
	
	                case 87: // S
	                case 68: // D
	                case 38: // UP
	                case 39: // RIGHT
	                    e.preventDefault();
	                    this.moveByKey(true);
	                    break;
	            }
	
	            return true;
	        },
	
	        /**
	         * Move by key. Beta
	         * @todo refactor than have plenty of time
	         *
	         * @param right {boolean} direction to move
	         */
	        moveByKey: function (right) {
	            var p = this.coords.p_pointer;
	
	            if (right) {
	                p += this.options.keyboard_step;
	            } else {
	                p -= this.options.keyboard_step;
	            }
	
	            this.coords.x_pointer = this.toFixed(this.coords.w_rs / 100 * p);
	            this.is_key = true;
	            this.calc();
	        },
	
	        /**
	         * Set visibility and content
	         * of Min and Max labels
	         */
	        setMinMax: function () {
	            if (!this.options) {
	                return;
	            }
	
	            if (this.options.hide_min_max) {
	                this.$cache.min[0].style.display = "none";
	                this.$cache.max[0].style.display = "none";
	                return;
	            }
	
	            if (this.options.values.length) {
	                this.$cache.min.html(this.decorate(this.options.p_values[this.options.min]));
	                this.$cache.max.html(this.decorate(this.options.p_values[this.options.max]));
	            } else {
	                this.$cache.min.html(this.decorate(this._prettify(this.options.min), this.options.min));
	                this.$cache.max.html(this.decorate(this._prettify(this.options.max), this.options.max));
	            }
	
	            this.labels.w_min = this.$cache.min.outerWidth(false);
	            this.labels.w_max = this.$cache.max.outerWidth(false);
	        },
	
	        /**
	         * Then dragging interval, prevent interval collapsing
	         * using min_interval option
	         */
	        setTempMinInterval: function () {
	            var interval = this.result.to - this.result.from;
	
	            if (this.old_min_interval === null) {
	                this.old_min_interval = this.options.min_interval;
	            }
	
	            this.options.min_interval = interval;
	        },
	
	        /**
	         * Restore min_interval option to original
	         */
	        restoreOriginalMinInterval: function () {
	            if (this.old_min_interval !== null) {
	                this.options.min_interval = this.old_min_interval;
	                this.old_min_interval = null;
	            }
	        },
	
	
	
	        // =============================================================================================================
	        // Calculations
	
	        /**
	         * All calculations and measures start here
	         *
	         * @param update {boolean=}
	         */
	        calc: function (update) {
	            if (!this.options) {
	                return;
	            }
	
	            this.calc_count++;
	
	            if (this.calc_count === 10 || update) {
	                this.calc_count = 0;
	                this.coords.w_rs = this.$cache.rs.outerWidth(false);
	
	                this.calcHandlePercent();
	            }
	
	            if (!this.coords.w_rs) {
	                return;
	            }
	
	            this.calcPointerPercent();
	            var handle_x = this.getHandleX();
	
	            if (this.target === "click") {
	                this.coords.p_gap = this.coords.p_handle / 2;
	                handle_x = this.getHandleX();
	
	                if (this.options.drag_interval) {
	                    this.target = "both_one";
	                } else {
	                    this.target = this.chooseHandle(handle_x);
	                }
	            }
	
	            switch (this.target) {
	                case "base":
	                    var w = (this.options.max - this.options.min) / 100,
	                        f = (this.result.from - this.options.min) / w,
	                        t = (this.result.to - this.options.min) / w;
	
	                    this.coords.p_single_real = this.toFixed(f);
	                    this.coords.p_from_real = this.toFixed(f);
	                    this.coords.p_to_real = this.toFixed(t);
	
	                    this.coords.p_single_real = this.checkDiapason(this.coords.p_single_real, this.options.from_min, this.options.from_max);
	                    this.coords.p_from_real = this.checkDiapason(this.coords.p_from_real, this.options.from_min, this.options.from_max);
	                    this.coords.p_to_real = this.checkDiapason(this.coords.p_to_real, this.options.to_min, this.options.to_max);
	
	                    this.coords.p_single_fake = this.convertToFakePercent(this.coords.p_single_real);
	                    this.coords.p_from_fake = this.convertToFakePercent(this.coords.p_from_real);
	                    this.coords.p_to_fake = this.convertToFakePercent(this.coords.p_to_real);
	
	                    this.target = null;
	
	                    break;
	
	                case "single":
	                    if (this.options.from_fixed) {
	                        break;
	                    }
	
	                    this.coords.p_single_real = this.convertToRealPercent(handle_x);
	                    this.coords.p_single_real = this.calcWithStep(this.coords.p_single_real);
	                    this.coords.p_single_real = this.checkDiapason(this.coords.p_single_real, this.options.from_min, this.options.from_max);
	
	                    this.coords.p_single_fake = this.convertToFakePercent(this.coords.p_single_real);
	
	                    break;
	
	                case "from":
	                    if (this.options.from_fixed) {
	                        break;
	                    }
	
	                    this.coords.p_from_real = this.convertToRealPercent(handle_x);
	                    this.coords.p_from_real = this.calcWithStep(this.coords.p_from_real);
	                    if (this.coords.p_from_real > this.coords.p_to_real) {
	                        this.coords.p_from_real = this.coords.p_to_real;
	                    }
	                    this.coords.p_from_real = this.checkDiapason(this.coords.p_from_real, this.options.from_min, this.options.from_max);
	                    this.coords.p_from_real = this.checkMinInterval(this.coords.p_from_real, this.coords.p_to_real, "from");
	                    this.coords.p_from_real = this.checkMaxInterval(this.coords.p_from_real, this.coords.p_to_real, "from");
	
	                    this.coords.p_from_fake = this.convertToFakePercent(this.coords.p_from_real);
	
	                    break;
	
	                case "to":
	                    if (this.options.to_fixed) {
	                        break;
	                    }
	
	                    this.coords.p_to_real = this.convertToRealPercent(handle_x);
	                    this.coords.p_to_real = this.calcWithStep(this.coords.p_to_real);
	                    if (this.coords.p_to_real < this.coords.p_from_real) {
	                        this.coords.p_to_real = this.coords.p_from_real;
	                    }
	                    this.coords.p_to_real = this.checkDiapason(this.coords.p_to_real, this.options.to_min, this.options.to_max);
	                    this.coords.p_to_real = this.checkMinInterval(this.coords.p_to_real, this.coords.p_from_real, "to");
	                    this.coords.p_to_real = this.checkMaxInterval(this.coords.p_to_real, this.coords.p_from_real, "to");
	
	                    this.coords.p_to_fake = this.convertToFakePercent(this.coords.p_to_real);
	
	                    break;
	
	                case "both":
	                    if (this.options.from_fixed || this.options.to_fixed) {
	                        break;
	                    }
	
	                    handle_x = this.toFixed(handle_x + (this.coords.p_handle * 0.1));
	
	                    this.coords.p_from_real = this.convertToRealPercent(handle_x) - this.coords.p_gap_left;
	                    this.coords.p_from_real = this.calcWithStep(this.coords.p_from_real);
	                    this.coords.p_from_real = this.checkDiapason(this.coords.p_from_real, this.options.from_min, this.options.from_max);
	                    this.coords.p_from_real = this.checkMinInterval(this.coords.p_from_real, this.coords.p_to_real, "from");
	                    this.coords.p_from_fake = this.convertToFakePercent(this.coords.p_from_real);
	
	                    this.coords.p_to_real = this.convertToRealPercent(handle_x) + this.coords.p_gap_right;
	                    this.coords.p_to_real = this.calcWithStep(this.coords.p_to_real);
	                    this.coords.p_to_real = this.checkDiapason(this.coords.p_to_real, this.options.to_min, this.options.to_max);
	                    this.coords.p_to_real = this.checkMinInterval(this.coords.p_to_real, this.coords.p_from_real, "to");
	                    this.coords.p_to_fake = this.convertToFakePercent(this.coords.p_to_real);
	
	                    break;
	
	                case "both_one":
	                    if (this.options.from_fixed || this.options.to_fixed) {
	                        break;
	                    }
	
	                    var real_x = this.convertToRealPercent(handle_x),
	                        from = this.result.from_percent,
	                        to = this.result.to_percent,
	                        full = to - from,
	                        half = full / 2,
	                        new_from = real_x - half,
	                        new_to = real_x + half;
	
	                    if (new_from < 0) {
	                        new_from = 0;
	                        new_to = new_from + full;
	                    }
	
	                    if (new_to > 100) {
	                        new_to = 100;
	                        new_from = new_to - full;
	                    }
	
	                    this.coords.p_from_real = this.calcWithStep(new_from);
	                    this.coords.p_from_real = this.checkDiapason(this.coords.p_from_real, this.options.from_min, this.options.from_max);
	                    this.coords.p_from_fake = this.convertToFakePercent(this.coords.p_from_real);
	
	                    this.coords.p_to_real = this.calcWithStep(new_to);
	                    this.coords.p_to_real = this.checkDiapason(this.coords.p_to_real, this.options.to_min, this.options.to_max);
	                    this.coords.p_to_fake = this.convertToFakePercent(this.coords.p_to_real);
	
	                    break;
	            }
	
	            if (this.options.type === "single") {
	                this.coords.p_bar_x = (this.coords.p_handle / 2);
	                this.coords.p_bar_w = this.coords.p_single_fake;
	
	                this.result.from_percent = this.coords.p_single_real;
	                this.result.from = this.convertToValue(this.coords.p_single_real);
	
	                if (this.options.values.length) {
	                    this.result.from_value = this.options.values[this.result.from];
	                }
	            } else {
	                this.coords.p_bar_x = this.toFixed(this.coords.p_from_fake + (this.coords.p_handle / 2));
	                this.coords.p_bar_w = this.toFixed(this.coords.p_to_fake - this.coords.p_from_fake);
	
	                this.result.from_percent = this.coords.p_from_real;
	                this.result.from = this.convertToValue(this.coords.p_from_real);
	                this.result.to_percent = this.coords.p_to_real;
	                this.result.to = this.convertToValue(this.coords.p_to_real);
	
	                if (this.options.values.length) {
	                    this.result.from_value = this.options.values[this.result.from];
	                    this.result.to_value = this.options.values[this.result.to];
	                }
	            }
	
	            this.calcMinMax();
	            this.calcLabels();
	        },
	
	
	        /**
	         * calculates pointer X in percent
	         */
	        calcPointerPercent: function () {
	            if (!this.coords.w_rs) {
	                this.coords.p_pointer = 0;
	                return;
	            }
	
	            if (this.coords.x_pointer < 0 || isNaN(this.coords.x_pointer)  ) {
	                this.coords.x_pointer = 0;
	            } else if (this.coords.x_pointer > this.coords.w_rs) {
	                this.coords.x_pointer = this.coords.w_rs;
	            }
	
	            this.coords.p_pointer = this.toFixed(this.coords.x_pointer / this.coords.w_rs * 100);
	        },
	
	        convertToRealPercent: function (fake) {
	            var full = 100 - this.coords.p_handle;
	            return fake / full * 100;
	        },
	
	        convertToFakePercent: function (real) {
	            var full = 100 - this.coords.p_handle;
	            return real / 100 * full;
	        },
	
	        getHandleX: function () {
	            var max = 100 - this.coords.p_handle,
	                x = this.toFixed(this.coords.p_pointer - this.coords.p_gap);
	
	            if (x < 0) {
	                x = 0;
	            } else if (x > max) {
	                x = max;
	            }
	
	            return x;
	        },
	
	        calcHandlePercent: function () {
	            if (this.options.type === "single") {
	                this.coords.w_handle = this.$cache.s_single.outerWidth(false);
	            } else {
	                this.coords.w_handle = this.$cache.s_from.outerWidth(false);
	            }
	
	            this.coords.p_handle = this.toFixed(this.coords.w_handle / this.coords.w_rs * 100);
	        },
	
	        /**
	         * Find closest handle to pointer click
	         *
	         * @param real_x {Number}
	         * @returns {String}
	         */
	        chooseHandle: function (real_x) {
	            if (this.options.type === "single") {
	                return "single";
	            } else {
	                var m_point = this.coords.p_from_real + ((this.coords.p_to_real - this.coords.p_from_real) / 2);
	                if (real_x >= m_point) {
	                    return this.options.to_fixed ? "from" : "to";
	                } else {
	                    return this.options.from_fixed ? "to" : "from";
	                }
	            }
	        },
	
	        /**
	         * Measure Min and Max labels width in percent
	         */
	        calcMinMax: function () {
	            if (!this.coords.w_rs) {
	                return;
	            }
	
	            this.labels.p_min = this.labels.w_min / this.coords.w_rs * 100;
	            this.labels.p_max = this.labels.w_max / this.coords.w_rs * 100;
	        },
	
	        /**
	         * Measure labels width and X in percent
	         */
	        calcLabels: function () {
	            if (!this.coords.w_rs || this.options.hide_from_to) {
	                return;
	            }
	
	            if (this.options.type === "single") {
	
	                this.labels.w_single = this.$cache.single.outerWidth(false);
	                this.labels.p_single_fake = this.labels.w_single / this.coords.w_rs * 100;
	                this.labels.p_single_left = this.coords.p_single_fake + (this.coords.p_handle / 2) - (this.labels.p_single_fake / 2);
	                this.labels.p_single_left = this.checkEdges(this.labels.p_single_left, this.labels.p_single_fake);
	
	            } else {
	
	                this.labels.w_from = this.$cache.from.outerWidth(false);
	                this.labels.p_from_fake = this.labels.w_from / this.coords.w_rs * 100;
	                this.labels.p_from_left = this.coords.p_from_fake + (this.coords.p_handle / 2) - (this.labels.p_from_fake / 2);
	                this.labels.p_from_left = this.toFixed(this.labels.p_from_left);
	                this.labels.p_from_left = this.checkEdges(this.labels.p_from_left, this.labels.p_from_fake);
	
	                this.labels.w_to = this.$cache.to.outerWidth(false);
	                this.labels.p_to_fake = this.labels.w_to / this.coords.w_rs * 100;
	                this.labels.p_to_left = this.coords.p_to_fake + (this.coords.p_handle / 2) - (this.labels.p_to_fake / 2);
	                this.labels.p_to_left = this.toFixed(this.labels.p_to_left);
	                this.labels.p_to_left = this.checkEdges(this.labels.p_to_left, this.labels.p_to_fake);
	
	                this.labels.w_single = this.$cache.single.outerWidth(false);
	                this.labels.p_single_fake = this.labels.w_single / this.coords.w_rs * 100;
	                this.labels.p_single_left = ((this.labels.p_from_left + this.labels.p_to_left + this.labels.p_to_fake) / 2) - (this.labels.p_single_fake / 2);
	                this.labels.p_single_left = this.toFixed(this.labels.p_single_left);
	                this.labels.p_single_left = this.checkEdges(this.labels.p_single_left, this.labels.p_single_fake);
	
	            }
	        },
	
	
	
	        // =============================================================================================================
	        // Drawings
	
	        /**
	         * Main function called in request animation frame
	         * to update everything
	         */
	        updateScene: function () {
	            if (this.raf_id) {
	                cancelAnimationFrame(this.raf_id);
	                this.raf_id = null;
	            }
	
	            clearTimeout(this.update_tm);
	            this.update_tm = null;
	
	            if (!this.options) {
	                return;
	            }
	
	            this.drawHandles();
	
	            if (this.is_active) {
	                this.raf_id = requestAnimationFrame(this.updateScene.bind(this));
	            } else {
	                this.update_tm = setTimeout(this.updateScene.bind(this), 300);
	            }
	        },
	
	        /**
	         * Draw handles
	         */
	        drawHandles: function () {
	            this.coords.w_rs = this.$cache.rs.outerWidth(false);
	
	            if (!this.coords.w_rs) {
	                return;
	            }
	
	            if (this.coords.w_rs !== this.coords.w_rs_old) {
	                this.target = "base";
	                this.is_resize = true;
	            }
	
	            if (this.coords.w_rs !== this.coords.w_rs_old || this.force_redraw) {
	                this.setMinMax();
	                this.calc(true);
	                this.drawLabels();
	                if (this.options.grid) {
	                    this.calcGridMargin();
	                    this.calcGridLabels();
	                }
	                this.force_redraw = true;
	                this.coords.w_rs_old = this.coords.w_rs;
	                this.drawShadow();
	            }
	
	            if (!this.coords.w_rs) {
	                return;
	            }
	
	            if (!this.dragging && !this.force_redraw && !this.is_key) {
	                return;
	            }
	
	            if (this.old_from !== this.result.from || this.old_to !== this.result.to || this.force_redraw || this.is_key) {
	
	                this.drawLabels();
	
	                this.$cache.bar[0].style.left = this.coords.p_bar_x + "%";
	                this.$cache.bar[0].style.width = this.coords.p_bar_w + "%";
	
	                if (this.options.type === "single") {
	                    this.$cache.s_single[0].style.left = this.coords.p_single_fake + "%";
	
	                    this.$cache.single[0].style.left = this.labels.p_single_left + "%";
	
	                    if (this.options.values.length) {
	                        this.$cache.input.prop("value", this.result.from_value);
	                    } else {
	                        this.$cache.input.prop("value", this.result.from);
	                    }
	                    this.$cache.input.data("from", this.result.from);
	                } else {
	                    this.$cache.s_from[0].style.left = this.coords.p_from_fake + "%";
	                    this.$cache.s_to[0].style.left = this.coords.p_to_fake + "%";
	
	                    if (this.old_from !== this.result.from || this.force_redraw) {
	                        this.$cache.from[0].style.left = this.labels.p_from_left + "%";
	                    }
	                    if (this.old_to !== this.result.to || this.force_redraw) {
	                        this.$cache.to[0].style.left = this.labels.p_to_left + "%";
	                    }
	
	                    this.$cache.single[0].style.left = this.labels.p_single_left + "%";
	
	                    if (this.options.values.length) {
	                        this.$cache.input.prop("value", this.result.from_value + this.options.input_values_separator + this.result.to_value);
	                    } else {
	                        this.$cache.input.prop("value", this.result.from + this.options.input_values_separator + this.result.to);
	                    }
	                    this.$cache.input.data("from", this.result.from);
	                    this.$cache.input.data("to", this.result.to);
	                }
	
	                if ((this.old_from !== this.result.from || this.old_to !== this.result.to) && !this.is_start) {
	                    this.$cache.input.trigger("change");
	                }
	
	                this.old_from = this.result.from;
	                this.old_to = this.result.to;
	
	                // callbacks call
	                if (!this.is_resize && !this.is_update && !this.is_start && !this.is_finish) {
	                    this.callOnChange();
	                }
	                if (this.is_key || this.is_click) {
	                    this.is_key = false;
	                    this.is_click = false;
	                    this.callOnFinish();
	                }
	
	                this.is_update = false;
	                this.is_resize = false;
	                this.is_finish = false;
	            }
	
	            this.is_start = false;
	            this.is_key = false;
	            this.is_click = false;
	            this.force_redraw = false;
	        },
	
	        /**
	         * Draw labels
	         * measure labels collisions
	         * collapse close labels
	         */
	        drawLabels: function () {
	            if (!this.options) {
	                return;
	            }
	
	            var values_num = this.options.values.length,
	                p_values = this.options.p_values,
	                text_single,
	                text_from,
	                text_to;
	
	            if (this.options.hide_from_to) {
	                return;
	            }
	
	            if (this.options.type === "single") {
	
	                if (values_num) {
	                    text_single = this.decorate(p_values[this.result.from]);
	                    this.$cache.single.html(text_single);
	                } else {
	                    text_single = this.decorate(this._prettify(this.result.from), this.result.from);
	                    this.$cache.single.html(text_single);
	                }
	
	                this.calcLabels();
	
	                if (this.labels.p_single_left < this.labels.p_min + 1) {
	                    this.$cache.min[0].style.visibility = "hidden";
	                } else {
	                    this.$cache.min[0].style.visibility = "visible";
	                }
	
	                if (this.labels.p_single_left + this.labels.p_single_fake > 100 - this.labels.p_max - 1) {
	                    this.$cache.max[0].style.visibility = "hidden";
	                } else {
	                    this.$cache.max[0].style.visibility = "visible";
	                }
	
	            } else {
	
	                if (values_num) {
	
	                    if (this.options.decorate_both) {
	                        text_single = this.decorate(p_values[this.result.from]);
	                        text_single += this.options.values_separator;
	                        text_single += this.decorate(p_values[this.result.to]);
	                    } else {
	                        text_single = this.decorate(p_values[this.result.from] + this.options.values_separator + p_values[this.result.to]);
	                    }
	                    text_from = this.decorate(p_values[this.result.from]);
	                    text_to = this.decorate(p_values[this.result.to]);
	
	                    this.$cache.single.html(text_single);
	                    this.$cache.from.html(text_from);
	                    this.$cache.to.html(text_to);
	
	                } else {
	
	                    if (this.options.decorate_both) {
	                        text_single = this.decorate(this._prettify(this.result.from), this.result.from);
	                        text_single += this.options.values_separator;
	                        text_single += this.decorate(this._prettify(this.result.to), this.result.to);
	                    } else {
	                        text_single = this.decorate(this._prettify(this.result.from) + this.options.values_separator + this._prettify(this.result.to), this.result.to);
	                    }
	                    text_from = this.decorate(this._prettify(this.result.from), this.result.from);
	                    text_to = this.decorate(this._prettify(this.result.to), this.result.to);
	
	                    this.$cache.single.html(text_single);
	                    this.$cache.from.html(text_from);
	                    this.$cache.to.html(text_to);
	
	                }
	
	                this.calcLabels();
	
	                var min = Math.min(this.labels.p_single_left, this.labels.p_from_left),
	                    single_left = this.labels.p_single_left + this.labels.p_single_fake,
	                    to_left = this.labels.p_to_left + this.labels.p_to_fake,
	                    max = Math.max(single_left, to_left);
	
	                if (this.labels.p_from_left + this.labels.p_from_fake >= this.labels.p_to_left) {
	                    this.$cache.from[0].style.visibility = "hidden";
	                    this.$cache.to[0].style.visibility = "hidden";
	                    this.$cache.single[0].style.visibility = "visible";
	
	                    if (this.result.from === this.result.to) {
	                        if (this.target === "from") {
	                            this.$cache.from[0].style.visibility = "visible";
	                        } else if (this.target === "to") {
	                            this.$cache.to[0].style.visibility = "visible";
	                        } else if (!this.target) {
	                            this.$cache.from[0].style.visibility = "visible";
	                        }
	                        this.$cache.single[0].style.visibility = "hidden";
	                        max = to_left;
	                    } else {
	                        this.$cache.from[0].style.visibility = "hidden";
	                        this.$cache.to[0].style.visibility = "hidden";
	                        this.$cache.single[0].style.visibility = "visible";
	                        max = Math.max(single_left, to_left);
	                    }
	                } else {
	                    this.$cache.from[0].style.visibility = "visible";
	                    this.$cache.to[0].style.visibility = "visible";
	                    this.$cache.single[0].style.visibility = "hidden";
	                }
	
	                if (min < this.labels.p_min + 1) {
	                    this.$cache.min[0].style.visibility = "hidden";
	                } else {
	                    this.$cache.min[0].style.visibility = "visible";
	                }
	
	                if (max > 100 - this.labels.p_max - 1) {
	                    this.$cache.max[0].style.visibility = "hidden";
	                } else {
	                    this.$cache.max[0].style.visibility = "visible";
	                }
	
	            }
	        },
	
	        /**
	         * Draw shadow intervals
	         */
	        drawShadow: function () {
	            var o = this.options,
	                c = this.$cache,
	
	                is_from_min = typeof o.from_min === "number" && !isNaN(o.from_min),
	                is_from_max = typeof o.from_max === "number" && !isNaN(o.from_max),
	                is_to_min = typeof o.to_min === "number" && !isNaN(o.to_min),
	                is_to_max = typeof o.to_max === "number" && !isNaN(o.to_max),
	
	                from_min,
	                from_max,
	                to_min,
	                to_max;
	
	            if (o.type === "single") {
	                if (o.from_shadow && (is_from_min || is_from_max)) {
	                    from_min = this.convertToPercent(is_from_min ? o.from_min : o.min);
	                    from_max = this.convertToPercent(is_from_max ? o.from_max : o.max) - from_min;
	                    from_min = this.toFixed(from_min - (this.coords.p_handle / 100 * from_min));
	                    from_max = this.toFixed(from_max - (this.coords.p_handle / 100 * from_max));
	                    from_min = from_min + (this.coords.p_handle / 2);
	
	                    c.shad_single[0].style.display = "block";
	                    c.shad_single[0].style.left = from_min + "%";
	                    c.shad_single[0].style.width = from_max + "%";
	                } else {
	                    c.shad_single[0].style.display = "none";
	                }
	            } else {
	                if (o.from_shadow && (is_from_min || is_from_max)) {
	                    from_min = this.convertToPercent(is_from_min ? o.from_min : o.min);
	                    from_max = this.convertToPercent(is_from_max ? o.from_max : o.max) - from_min;
	                    from_min = this.toFixed(from_min - (this.coords.p_handle / 100 * from_min));
	                    from_max = this.toFixed(from_max - (this.coords.p_handle / 100 * from_max));
	                    from_min = from_min + (this.coords.p_handle / 2);
	
	                    c.shad_from[0].style.display = "block";
	                    c.shad_from[0].style.left = from_min + "%";
	                    c.shad_from[0].style.width = from_max + "%";
	                } else {
	                    c.shad_from[0].style.display = "none";
	                }
	
	                if (o.to_shadow && (is_to_min || is_to_max)) {
	                    to_min = this.convertToPercent(is_to_min ? o.to_min : o.min);
	                    to_max = this.convertToPercent(is_to_max ? o.to_max : o.max) - to_min;
	                    to_min = this.toFixed(to_min - (this.coords.p_handle / 100 * to_min));
	                    to_max = this.toFixed(to_max - (this.coords.p_handle / 100 * to_max));
	                    to_min = to_min + (this.coords.p_handle / 2);
	
	                    c.shad_to[0].style.display = "block";
	                    c.shad_to[0].style.left = to_min + "%";
	                    c.shad_to[0].style.width = to_max + "%";
	                } else {
	                    c.shad_to[0].style.display = "none";
	                }
	            }
	        },
	
	
	
	        // =============================================================================================================
	        // Callbacks
	
	        callOnStart: function () {
	            if (this.options.onStart && typeof this.options.onStart === "function") {
	                this.options.onStart(this.result);
	            }
	        },
	        callOnChange: function () {
	            if (this.options.onChange && typeof this.options.onChange === "function") {
	                this.options.onChange(this.result);
	            }
	        },
	        callOnFinish: function () {
	            if (this.options.onFinish && typeof this.options.onFinish === "function") {
	                this.options.onFinish(this.result);
	            }
	        },
	        callOnUpdate: function () {
	            if (this.options.onUpdate && typeof this.options.onUpdate === "function") {
	                this.options.onUpdate(this.result);
	            }
	        },
	
	
	
	        // =============================================================================================================
	        // Service methods
	
	        toggleInput: function () {
	            this.$cache.input.toggleClass("irs-hidden-input");
	        },
	
	        /**
	         * Convert real value to percent
	         *
	         * @param value {Number} X in real
	         * @param no_min {boolean=} don't use min value
	         * @returns {Number} X in percent
	         */
	        convertToPercent: function (value, no_min) {
	            var diapason = this.options.max - this.options.min,
	                one_percent = diapason / 100,
	                val, percent;
	
	            if (!diapason) {
	                this.no_diapason = true;
	                return 0;
	            }
	
	            if (no_min) {
	                val = value;
	            } else {
	                val = value - this.options.min;
	            }
	
	            percent = val / one_percent;
	
	            return this.toFixed(percent);
	        },
	
	        /**
	         * Convert percent to real values
	         *
	         * @param percent {Number} X in percent
	         * @returns {Number} X in real
	         */
	        convertToValue: function (percent) {
	            var min = this.options.min,
	                max = this.options.max,
	                min_decimals = min.toString().split(".")[1],
	                max_decimals = max.toString().split(".")[1],
	                min_length, max_length,
	                avg_decimals = 0,
	                abs = 0;
	
	            if (percent === 0) {
	                return this.options.min;
	            }
	            if (percent === 100) {
	                return this.options.max;
	            }
	
	
	            if (min_decimals) {
	                min_length = min_decimals.length;
	                avg_decimals = min_length;
	            }
	            if (max_decimals) {
	                max_length = max_decimals.length;
	                avg_decimals = max_length;
	            }
	            if (min_length && max_length) {
	                avg_decimals = (min_length >= max_length) ? min_length : max_length;
	            }
	
	            if (min < 0) {
	                abs = Math.abs(min);
	                min = +(min + abs).toFixed(avg_decimals);
	                max = +(max + abs).toFixed(avg_decimals);
	            }
	
	            var number = ((max - min) / 100 * percent) + min,
	                string = this.options.step.toString().split(".")[1],
	                result;
	
	            if (string) {
	                number = +number.toFixed(string.length);
	            } else {
	                number = number / this.options.step;
	                number = number * this.options.step;
	
	                number = +number.toFixed(0);
	            }
	
	            if (abs) {
	                number -= abs;
	            }
	
	            if (string) {
	                result = +number.toFixed(string.length);
	            } else {
	                result = this.toFixed(number);
	            }
	
	            if (result < this.options.min) {
	                result = this.options.min;
	            } else if (result > this.options.max) {
	                result = this.options.max;
	            }
	
	            return result;
	        },
	
	        /**
	         * Round percent value with step
	         *
	         * @param percent {Number}
	         * @returns percent {Number} rounded
	         */
	        calcWithStep: function (percent) {
	            var rounded = Math.round(percent / this.coords.p_step) * this.coords.p_step;
	
	            if (rounded > 100) {
	                rounded = 100;
	            }
	            if (percent === 100) {
	                rounded = 100;
	            }
	
	            return this.toFixed(rounded);
	        },
	
	        checkMinInterval: function (p_current, p_next, type) {
	            var o = this.options,
	                current,
	                next;
	
	            if (!o.min_interval) {
	                return p_current;
	            }
	
	            current = this.convertToValue(p_current);
	            next = this.convertToValue(p_next);
	
	            if (type === "from") {
	
	                if (next - current < o.min_interval) {
	                    current = next - o.min_interval;
	                }
	
	            } else {
	
	                if (current - next < o.min_interval) {
	                    current = next + o.min_interval;
	                }
	
	            }
	
	            return this.convertToPercent(current);
	        },
	
	        checkMaxInterval: function (p_current, p_next, type) {
	            var o = this.options,
	                current,
	                next;
	
	            if (!o.max_interval) {
	                return p_current;
	            }
	
	            current = this.convertToValue(p_current);
	            next = this.convertToValue(p_next);
	
	            if (type === "from") {
	
	                if (next - current > o.max_interval) {
	                    current = next - o.max_interval;
	                }
	
	            } else {
	
	                if (current - next > o.max_interval) {
	                    current = next + o.max_interval;
	                }
	
	            }
	
	            return this.convertToPercent(current);
	        },
	
	        checkDiapason: function (p_num, min, max) {
	            var num = this.convertToValue(p_num),
	                o = this.options;
	
	            if (typeof min !== "number") {
	                min = o.min;
	            }
	
	            if (typeof max !== "number") {
	                max = o.max;
	            }
	
	            if (num < min) {
	                num = min;
	            }
	
	            if (num > max) {
	                num = max;
	            }
	
	            return this.convertToPercent(num);
	        },
	
	        toFixed: function (num) {
	            num = num.toFixed(9);
	            return +num;
	        },
	
	        _prettify: function (num) {
	            if (!this.options.prettify_enabled) {
	                return num;
	            }
	
	            if (this.options.prettify && typeof this.options.prettify === "function") {
	                return this.options.prettify(num);
	            } else {
	                return this.prettify(num);
	            }
	        },
	
	        prettify: function (num) {
	            var n = num.toString();
	            return n.replace(/(\d{1,3}(?=(?:\d\d\d)+(?!\d)))/g, "$1" + this.options.prettify_separator);
	        },
	
	        checkEdges: function (left, width) {
	            if (!this.options.force_edges) {
	                return this.toFixed(left);
	            }
	
	            if (left < 0) {
	                left = 0;
	            } else if (left > 100 - width) {
	                left = 100 - width;
	            }
	
	            return this.toFixed(left);
	        },
	
	        validate: function () {
	            var o = this.options,
	                r = this.result,
	                v = o.values,
	                vl = v.length,
	                value,
	                i;
	
	            if (typeof o.min === "string") o.min = +o.min;
	            if (typeof o.max === "string") o.max = +o.max;
	            if (typeof o.from === "string") o.from = +o.from;
	            if (typeof o.to === "string") o.to = +o.to;
	            if (typeof o.step === "string") o.step = +o.step;
	
	            if (typeof o.from_min === "string") o.from_min = +o.from_min;
	            if (typeof o.from_max === "string") o.from_max = +o.from_max;
	            if (typeof o.to_min === "string") o.to_min = +o.to_min;
	            if (typeof o.to_max === "string") o.to_max = +o.to_max;
	
	            if (typeof o.keyboard_step === "string") o.keyboard_step = +o.keyboard_step;
	            if (typeof o.grid_num === "string") o.grid_num = +o.grid_num;
	
	            if (o.max < o.min) {
	                o.max = o.min;
	            }
	
	            if (vl) {
	                o.p_values = [];
	                o.min = 0;
	                o.max = vl - 1;
	                o.step = 1;
	                o.grid_num = o.max;
	                o.grid_snap = true;
	
	
	                for (i = 0; i < vl; i++) {
	                    value = +v[i];
	
	                    if (!isNaN(value)) {
	                        v[i] = value;
	                        value = this._prettify(value);
	                    } else {
	                        value = v[i];
	                    }
	
	                    o.p_values.push(value);
	                }
	            }
	
	            if (typeof o.from !== "number" || isNaN(o.from)) {
	                o.from = o.min;
	            }
	
	            if (typeof o.to !== "number" || isNaN(o.from)) {
	                o.to = o.max;
	            }
	
	            if (o.type === "single") {
	
	                if (o.from < o.min) {
	                    o.from = o.min;
	                }
	
	                if (o.from > o.max) {
	                    o.from = o.max;
	                }
	
	            } else {
	
	                if (o.from < o.min || o.from > o.max) {
	                    o.from = o.min;
	                }
	                if (o.to > o.max || o.to < o.min) {
	                    o.to = o.max;
	                }
	                if (o.from > o.to) {
	                    o.from = o.to;
	                }
	
	            }
	
	            if (typeof o.step !== "number" || isNaN(o.step) || !o.step || o.step < 0) {
	                o.step = 1;
	            }
	
	            if (typeof o.keyboard_step !== "number" || isNaN(o.keyboard_step) || !o.keyboard_step || o.keyboard_step < 0) {
	                o.keyboard_step = 5;
	            }
	
	            if (typeof o.from_min === "number" && o.from < o.from_min) {
	                o.from = o.from_min;
	            }
	
	            if (typeof o.from_max === "number" && o.from > o.from_max) {
	                o.from = o.from_max;
	            }
	
	            if (typeof o.to_min === "number" && o.to < o.to_min) {
	                o.to = o.to_min;
	            }
	
	            if (typeof o.to_max === "number" && o.from > o.to_max) {
	                o.to = o.to_max;
	            }
	
	            if (r) {
	                if (r.min !== o.min) {
	                    r.min = o.min;
	                }
	
	                if (r.max !== o.max) {
	                    r.max = o.max;
	                }
	
	                if (r.from < r.min || r.from > r.max) {
	                    r.from = o.from;
	                }
	
	                if (r.to < r.min || r.to > r.max) {
	                    r.to = o.to;
	                }
	            }
	
	            if (typeof o.min_interval !== "number" || isNaN(o.min_interval) || !o.min_interval || o.min_interval < 0) {
	                o.min_interval = 0;
	            }
	
	            if (typeof o.max_interval !== "number" || isNaN(o.max_interval) || !o.max_interval || o.max_interval < 0) {
	                o.max_interval = 0;
	            }
	
	            if (o.min_interval && o.min_interval > o.max - o.min) {
	                o.min_interval = o.max - o.min;
	            }
	
	            if (o.max_interval && o.max_interval > o.max - o.min) {
	                o.max_interval = o.max - o.min;
	            }
	        },
	
	        decorate: function (num, original) {
	            var decorated = "",
	                o = this.options;
	
	            if (o.prefix) {
	                decorated += o.prefix;
	            }
	
	            decorated += num;
	
	            if (o.max_postfix) {
	                if (o.values.length && num === o.p_values[o.max]) {
	                    decorated += o.max_postfix;
	                    if (o.postfix) {
	                        decorated += " ";
	                    }
	                } else if (original === o.max) {
	                    decorated += o.max_postfix;
	                    if (o.postfix) {
	                        decorated += " ";
	                    }
	                }
	            }
	
	            if (o.postfix) {
	                decorated += o.postfix;
	            }
	
	            return decorated;
	        },
	
	        updateFrom: function () {
	            this.result.from = this.options.from;
	            this.result.from_percent = this.convertToPercent(this.result.from);
	            if (this.options.values) {
	                this.result.from_value = this.options.values[this.result.from];
	            }
	        },
	
	        updateTo: function () {
	            this.result.to = this.options.to;
	            this.result.to_percent = this.convertToPercent(this.result.to);
	            if (this.options.values) {
	                this.result.to_value = this.options.values[this.result.to];
	            }
	        },
	
	        updateResult: function () {
	            this.result.min = this.options.min;
	            this.result.max = this.options.max;
	            this.updateFrom();
	            this.updateTo();
	        },
	
	
	        // =============================================================================================================
	        // Grid
	
	        appendGrid: function () {
	            if (!this.options.grid) {
	                return;
	            }
	
	            var o = this.options,
	                i, z,
	
	                total = o.max - o.min,
	                big_num = o.grid_num,
	                big_p = 0,
	                big_w = 0,
	
	                small_max = 4,
	                local_small_max,
	                small_p,
	                small_w = 0,
	
	                result,
	                html = '';
	
	
	
	            this.calcGridMargin();
	
	            if (o.grid_snap) {
	                big_num = total / o.step;
	                big_p = this.toFixed(o.step / (total / 100));
	            } else {
	                big_p = this.toFixed(100 / big_num);
	            }
	
	            if (big_num > 4) {
	                small_max = 3;
	            }
	            if (big_num > 7) {
	                small_max = 2;
	            }
	            if (big_num > 14) {
	                small_max = 1;
	            }
	            if (big_num > 28) {
	                small_max = 0;
	            }
	
	            for (i = 0; i < big_num + 1; i++) {
	                local_small_max = small_max;
	
	                big_w = this.toFixed(big_p * i);
	
	                if (big_w > 100) {
	                    big_w = 100;
	
	                    local_small_max -= 2;
	                    if (local_small_max < 0) {
	                        local_small_max = 0;
	                    }
	                }
	                this.coords.big[i] = big_w;
	
	                small_p = (big_w - (big_p * (i - 1))) / (local_small_max + 1);
	
	                for (z = 1; z <= local_small_max; z++) {
	                    if (big_w === 0) {
	                        break;
	                    }
	
	                    small_w = this.toFixed(big_w - (small_p * z));
	
	                    html += '<span class="irs-grid-pol small" style="left: ' + small_w + '%"></span>';
	                }
	
	                html += '<span class="irs-grid-pol" style="left: ' + big_w + '%"></span>';
	
	                result = this.convertToValue(big_w);
	                if (o.values.length) {
	                    result = o.p_values[result];
	                } else {
	                    result = this._prettify(result);
	                }
	
	                html += '<span class="irs-grid-text js-grid-text-' + i + '" style="left: ' + big_w + '%">' + result + '</span>';
	            }
	            this.coords.big_num = Math.ceil(big_num + 1);
	
	
	
	            this.$cache.cont.addClass("irs-with-grid");
	            this.$cache.grid.html(html);
	            this.cacheGridLabels();
	        },
	
	        cacheGridLabels: function () {
	            var $label, i,
	                num = this.coords.big_num;
	
	            for (i = 0; i < num; i++) {
	                $label = this.$cache.grid.find(".js-grid-text-" + i);
	                this.$cache.grid_labels.push($label);
	            }
	
	            this.calcGridLabels();
	        },
	
	        calcGridLabels: function () {
	            var i, label, start = [], finish = [],
	                num = this.coords.big_num;
	
	            for (i = 0; i < num; i++) {
	                this.coords.big_w[i] = this.$cache.grid_labels[i].outerWidth(false);
	                this.coords.big_p[i] = this.toFixed(this.coords.big_w[i] / this.coords.w_rs * 100);
	                this.coords.big_x[i] = this.toFixed(this.coords.big_p[i] / 2);
	
	                start[i] = this.toFixed(this.coords.big[i] - this.coords.big_x[i]);
	                finish[i] = this.toFixed(start[i] + this.coords.big_p[i]);
	            }
	
	            if (this.options.force_edges) {
	                if (start[0] < -this.coords.grid_gap) {
	                    start[0] = -this.coords.grid_gap;
	                    finish[0] = this.toFixed(start[0] + this.coords.big_p[0]);
	
	                    this.coords.big_x[0] = this.coords.grid_gap;
	                }
	
	                if (finish[num - 1] > 100 + this.coords.grid_gap) {
	                    finish[num - 1] = 100 + this.coords.grid_gap;
	                    start[num - 1] = this.toFixed(finish[num - 1] - this.coords.big_p[num - 1]);
	
	                    this.coords.big_x[num - 1] = this.toFixed(this.coords.big_p[num - 1] - this.coords.grid_gap);
	                }
	            }
	
	            this.calcGridCollision(2, start, finish);
	            this.calcGridCollision(4, start, finish);
	
	            for (i = 0; i < num; i++) {
	                label = this.$cache.grid_labels[i][0];
	                label.style.marginLeft = -this.coords.big_x[i] + "%";
	            }
	        },
	
	        // Collisions Calc Beta
	        // TODO: Refactor then have plenty of time
	        calcGridCollision: function (step, start, finish) {
	            var i, next_i, label,
	                num = this.coords.big_num;
	
	            for (i = 0; i < num; i += step) {
	                next_i = i + (step / 2);
	                if (next_i >= num) {
	                    break;
	                }
	
	                label = this.$cache.grid_labels[next_i][0];
	
	                if (finish[i] <= start[next_i]) {
	                    label.style.visibility = "visible";
	                } else {
	                    label.style.visibility = "hidden";
	                }
	            }
	        },
	
	        calcGridMargin: function () {
	            if (!this.options.grid_margin) {
	                return;
	            }
	
	            this.coords.w_rs = this.$cache.rs.outerWidth(false);
	            if (!this.coords.w_rs) {
	                return;
	            }
	
	            if (this.options.type === "single") {
	                this.coords.w_handle = this.$cache.s_single.outerWidth(false);
	            } else {
	                this.coords.w_handle = this.$cache.s_from.outerWidth(false);
	            }
	            this.coords.p_handle = this.toFixed(this.coords.w_handle  / this.coords.w_rs * 100);
	            this.coords.grid_gap = this.toFixed((this.coords.p_handle / 2) - 0.1);
	
	            this.$cache.grid[0].style.width = this.toFixed(100 - this.coords.p_handle) + "%";
	            this.$cache.grid[0].style.left = this.coords.grid_gap + "%";
	        },
	
	
	
	        // =============================================================================================================
	        // Public methods
	
	        update: function (options) {
	            if (!this.input) {
	                return;
	            }
	
	            this.is_update = true;
	
	            this.options.from = this.result.from;
	            this.options.to = this.result.to;
	
	            this.options = $.extend(this.options, options);
	            this.validate();
	            this.updateResult(options);
	
	            this.toggleInput();
	            this.remove();
	            this.init(true);
	        },
	
	        reset: function () {
	            if (!this.input) {
	                return;
	            }
	
	            this.updateResult();
	            this.update();
	        },
	
	        destroy: function () {
	            if (!this.input) {
	                return;
	            }
	
	            this.toggleInput();
	            this.$cache.input.prop("readonly", false);
	            $.data(this.input, "ionRangeSlider", null);
	
	            this.remove();
	            this.input = null;
	            this.options = null;
	        }
	    };
	
	    $.fn.ionRangeSlider = function (options) {
	        return this.each(function() {
	            if (!$.data(this, "ionRangeSlider")) {
	                $.data(this, "ionRangeSlider", new IonRangeSlider(this, options, plugin_count++));
	            }
	        });
	    };
	
	
	
	    // =================================================================================================================
	    // http://paulirish.com/2011/requestanimationframe-for-smart-animating/
	    // http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating
	
	    // requestAnimationFrame polyfill by Erik Möller. fixes from Paul Irish and Tino Zijdel
	
	    // MIT license
	
	    (function() {
	        var lastTime = 0;
	        var vendors = ['ms', 'moz', 'webkit', 'o'];
	        for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
	            window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
	            window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame']
	                || window[vendors[x]+'CancelRequestAnimationFrame'];
	        }
	
	        if (!window.requestAnimationFrame)
	            window.requestAnimationFrame = function(callback, element) {
	                var currTime = new Date().getTime();
	                var timeToCall = Math.max(0, 16 - (currTime - lastTime));
	                var id = window.setTimeout(function() { callback(currTime + timeToCall); },
	                    timeToCall);
	                lastTime = currTime + timeToCall;
	                return id;
	            };
	
	        if (!window.cancelAnimationFrame)
	            window.cancelAnimationFrame = function(id) {
	                clearTimeout(id);
	            };
	    }());
	
	}));


/***/ },
/* 34 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	module.exports = __webpack_require__(35);


/***/ },
/* 35 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;'use strict';
	
	var ps = __webpack_require__(36);
	var psInstances = __webpack_require__(41);
	
	function mountJQuery(jQuery) {
	  jQuery.fn.perfectScrollbar = function (settingOrCommand) {
	    return this.each(function () {
	      if (typeof settingOrCommand === 'object' ||
	          typeof settingOrCommand === 'undefined') {
	        // If it's an object or none, initialize.
	        var settings = settingOrCommand;
	
	        if (!psInstances.get(this)) {
	          ps.initialize(this, settings);
	        }
	      } else {
	        // Unless, it may be a command.
	        var command = settingOrCommand;
	
	        if (command === 'update') {
	          ps.update(this);
	        } else if (command === 'destroy') {
	          ps.destroy(this);
	        }
	      }
	    });
	  };
	}
	
	if (true) {
	  // AMD. Register as an anonymous module.
	  !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(11)], __WEBPACK_AMD_DEFINE_FACTORY__ = (mountJQuery), __WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ? (__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	} else {
	  var jq = window.jQuery ? window.jQuery : window.$;
	  if (typeof jq !== 'undefined') {
	    mountJQuery(jq);
	  }
	}
	
	module.exports = mountJQuery;


/***/ },
/* 36 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var destroy = __webpack_require__(37);
	var initialize = __webpack_require__(45);
	var update = __webpack_require__(55);
	
	module.exports = {
	  initialize: initialize,
	  update: update,
	  destroy: destroy
	};


/***/ },
/* 37 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _ = __webpack_require__(38);
	var dom = __webpack_require__(40);
	var instances = __webpack_require__(41);
	
	module.exports = function (element) {
	  var i = instances.get(element);
	
	  if (!i) {
	    return;
	  }
	
	  i.event.unbindAll();
	  dom.remove(i.scrollbarX);
	  dom.remove(i.scrollbarY);
	  dom.remove(i.scrollbarXRail);
	  dom.remove(i.scrollbarYRail);
	  _.removePsClasses(element);
	
	  instances.remove(element);
	};


/***/ },
/* 38 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var cls = __webpack_require__(39);
	var dom = __webpack_require__(40);
	
	var toInt = exports.toInt = function (x) {
	  return parseInt(x, 10) || 0;
	};
	
	var clone = exports.clone = function (obj) {
	  if (!obj) {
	    return null;
	  } else if (obj.constructor === Array) {
	    return obj.map(clone);
	  } else if (typeof obj === 'object') {
	    var result = {};
	    for (var key in obj) {
	      result[key] = clone(obj[key]);
	    }
	    return result;
	  } else {
	    return obj;
	  }
	};
	
	exports.extend = function (original, source) {
	  var result = clone(original);
	  for (var key in source) {
	    result[key] = clone(source[key]);
	  }
	  return result;
	};
	
	exports.isEditable = function (el) {
	  return dom.matches(el, "input,[contenteditable]") ||
	         dom.matches(el, "select,[contenteditable]") ||
	         dom.matches(el, "textarea,[contenteditable]") ||
	         dom.matches(el, "button,[contenteditable]");
	};
	
	exports.removePsClasses = function (element) {
	  var clsList = cls.list(element);
	  for (var i = 0; i < clsList.length; i++) {
	    var className = clsList[i];
	    if (className.indexOf('ps-') === 0) {
	      cls.remove(element, className);
	    }
	  }
	};
	
	exports.outerWidth = function (element) {
	  return toInt(dom.css(element, 'width')) +
	         toInt(dom.css(element, 'paddingLeft')) +
	         toInt(dom.css(element, 'paddingRight')) +
	         toInt(dom.css(element, 'borderLeftWidth')) +
	         toInt(dom.css(element, 'borderRightWidth'));
	};
	
	exports.startScrolling = function (element, axis) {
	  cls.add(element, 'ps-in-scrolling');
	  if (typeof axis !== 'undefined') {
	    cls.add(element, 'ps-' + axis);
	  } else {
	    cls.add(element, 'ps-x');
	    cls.add(element, 'ps-y');
	  }
	};
	
	exports.stopScrolling = function (element, axis) {
	  cls.remove(element, 'ps-in-scrolling');
	  if (typeof axis !== 'undefined') {
	    cls.remove(element, 'ps-' + axis);
	  } else {
	    cls.remove(element, 'ps-x');
	    cls.remove(element, 'ps-y');
	  }
	};
	
	exports.env = {
	  isWebKit: 'WebkitAppearance' in document.documentElement.style,
	  supportsTouch: (('ontouchstart' in window) || window.DocumentTouch && document instanceof window.DocumentTouch),
	  supportsIePointer: window.navigator.msMaxTouchPoints !== null
	};


/***/ },
/* 39 */
/***/ function(module, exports) {

	'use strict';
	
	function oldAdd(element, className) {
	  var classes = element.className.split(' ');
	  if (classes.indexOf(className) < 0) {
	    classes.push(className);
	  }
	  element.className = classes.join(' ');
	}
	
	function oldRemove(element, className) {
	  var classes = element.className.split(' ');
	  var idx = classes.indexOf(className);
	  if (idx >= 0) {
	    classes.splice(idx, 1);
	  }
	  element.className = classes.join(' ');
	}
	
	exports.add = function (element, className) {
	  if (element.classList) {
	    element.classList.add(className);
	  } else {
	    oldAdd(element, className);
	  }
	};
	
	exports.remove = function (element, className) {
	  if (element.classList) {
	    element.classList.remove(className);
	  } else {
	    oldRemove(element, className);
	  }
	};
	
	exports.list = function (element) {
	  if (element.classList) {
	    return Array.prototype.slice.apply(element.classList);
	  } else {
	    return element.className.split(' ');
	  }
	};


/***/ },
/* 40 */
/***/ function(module, exports) {

	'use strict';
	
	var DOM = {};
	
	DOM.e = function (tagName, className) {
	  var element = document.createElement(tagName);
	  element.className = className;
	  return element;
	};
	
	DOM.appendTo = function (child, parent) {
	  parent.appendChild(child);
	  return child;
	};
	
	function cssGet(element, styleName) {
	  return window.getComputedStyle(element)[styleName];
	}
	
	function cssSet(element, styleName, styleValue) {
	  if (typeof styleValue === 'number') {
	    styleValue = styleValue.toString() + 'px';
	  }
	  element.style[styleName] = styleValue;
	  return element;
	}
	
	function cssMultiSet(element, obj) {
	  for (var key in obj) {
	    var val = obj[key];
	    if (typeof val === 'number') {
	      val = val.toString() + 'px';
	    }
	    element.style[key] = val;
	  }
	  return element;
	}
	
	DOM.css = function (element, styleNameOrObject, styleValue) {
	  if (typeof styleNameOrObject === 'object') {
	    // multiple set with object
	    return cssMultiSet(element, styleNameOrObject);
	  } else {
	    if (typeof styleValue === 'undefined') {
	      return cssGet(element, styleNameOrObject);
	    } else {
	      return cssSet(element, styleNameOrObject, styleValue);
	    }
	  }
	};
	
	DOM.matches = function (element, query) {
	  if (typeof element.matches !== 'undefined') {
	    return element.matches(query);
	  } else {
	    if (typeof element.matchesSelector !== 'undefined') {
	      return element.matchesSelector(query);
	    } else if (typeof element.webkitMatchesSelector !== 'undefined') {
	      return element.webkitMatchesSelector(query);
	    } else if (typeof element.mozMatchesSelector !== 'undefined') {
	      return element.mozMatchesSelector(query);
	    } else if (typeof element.msMatchesSelector !== 'undefined') {
	      return element.msMatchesSelector(query);
	    }
	  }
	};
	
	DOM.remove = function (element) {
	  if (typeof element.remove !== 'undefined') {
	    element.remove();
	  } else {
	    if (element.parentNode) {
	      element.parentNode.removeChild(element);
	    }
	  }
	};
	
	DOM.queryChildren = function (element, selector) {
	  return Array.prototype.filter.call(element.childNodes, function (child) {
	    return DOM.matches(child, selector);
	  });
	};
	
	module.exports = DOM;


/***/ },
/* 41 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _ = __webpack_require__(38);
	var cls = __webpack_require__(39);
	var defaultSettings = __webpack_require__(42);
	var dom = __webpack_require__(40);
	var EventManager = __webpack_require__(43);
	var guid = __webpack_require__(44);
	
	var instances = {};
	
	function Instance(element) {
	  var i = this;
	
	  i.settings = _.clone(defaultSettings);
	  i.containerWidth = null;
	  i.containerHeight = null;
	  i.contentWidth = null;
	  i.contentHeight = null;
	
	  i.isRtl = dom.css(element, 'direction') === "rtl";
	  i.isNegativeScroll = (function () {
	    var originalScrollLeft = element.scrollLeft;
	    var result = null;
	    element.scrollLeft = -1;
	    result = element.scrollLeft < 0;
	    element.scrollLeft = originalScrollLeft;
	    return result;
	  })();
	  i.negativeScrollAdjustment = i.isNegativeScroll ? element.scrollWidth - element.clientWidth : 0;
	  i.event = new EventManager();
	  i.ownerDocument = element.ownerDocument || document;
	
	  function focus() {
	    cls.add(element, 'ps-focus');
	  }
	
	  function blur() {
	    cls.remove(element, 'ps-focus');
	  }
	
	  i.scrollbarXRail = dom.appendTo(dom.e('div', 'ps-scrollbar-x-rail'), element);
	  i.scrollbarX = dom.appendTo(dom.e('div', 'ps-scrollbar-x'), i.scrollbarXRail);
	  i.scrollbarX.setAttribute('tabindex', 0);
	  i.event.bind(i.scrollbarX, 'focus', focus);
	  i.event.bind(i.scrollbarX, 'blur', blur);
	  i.scrollbarXActive = null;
	  i.scrollbarXWidth = null;
	  i.scrollbarXLeft = null;
	  i.scrollbarXBottom = _.toInt(dom.css(i.scrollbarXRail, 'bottom'));
	  i.isScrollbarXUsingBottom = i.scrollbarXBottom === i.scrollbarXBottom; // !isNaN
	  i.scrollbarXTop = i.isScrollbarXUsingBottom ? null : _.toInt(dom.css(i.scrollbarXRail, 'top'));
	  i.railBorderXWidth = _.toInt(dom.css(i.scrollbarXRail, 'borderLeftWidth')) + _.toInt(dom.css(i.scrollbarXRail, 'borderRightWidth'));
	  // Set rail to display:block to calculate margins
	  dom.css(i.scrollbarXRail, 'display', 'block');
	  i.railXMarginWidth = _.toInt(dom.css(i.scrollbarXRail, 'marginLeft')) + _.toInt(dom.css(i.scrollbarXRail, 'marginRight'));
	  dom.css(i.scrollbarXRail, 'display', '');
	  i.railXWidth = null;
	  i.railXRatio = null;
	
	  i.scrollbarYRail = dom.appendTo(dom.e('div', 'ps-scrollbar-y-rail'), element);
	  i.scrollbarY = dom.appendTo(dom.e('div', 'ps-scrollbar-y'), i.scrollbarYRail);
	  i.scrollbarY.setAttribute('tabindex', 0);
	  i.event.bind(i.scrollbarY, 'focus', focus);
	  i.event.bind(i.scrollbarY, 'blur', blur);
	  i.scrollbarYActive = null;
	  i.scrollbarYHeight = null;
	  i.scrollbarYTop = null;
	  i.scrollbarYRight = _.toInt(dom.css(i.scrollbarYRail, 'right'));
	  i.isScrollbarYUsingRight = i.scrollbarYRight === i.scrollbarYRight; // !isNaN
	  i.scrollbarYLeft = i.isScrollbarYUsingRight ? null : _.toInt(dom.css(i.scrollbarYRail, 'left'));
	  i.scrollbarYOuterWidth = i.isRtl ? _.outerWidth(i.scrollbarY) : null;
	  i.railBorderYWidth = _.toInt(dom.css(i.scrollbarYRail, 'borderTopWidth')) + _.toInt(dom.css(i.scrollbarYRail, 'borderBottomWidth'));
	  dom.css(i.scrollbarYRail, 'display', 'block');
	  i.railYMarginHeight = _.toInt(dom.css(i.scrollbarYRail, 'marginTop')) + _.toInt(dom.css(i.scrollbarYRail, 'marginBottom'));
	  dom.css(i.scrollbarYRail, 'display', '');
	  i.railYHeight = null;
	  i.railYRatio = null;
	}
	
	function getId(element) {
	  return element.getAttribute('data-ps-id');
	}
	
	function setId(element, id) {
	  element.setAttribute('data-ps-id', id);
	}
	
	function removeId(element) {
	  element.removeAttribute('data-ps-id');
	}
	
	exports.add = function (element) {
	  var newId = guid();
	  setId(element, newId);
	  instances[newId] = new Instance(element);
	  return instances[newId];
	};
	
	exports.remove = function (element) {
	  delete instances[getId(element)];
	  removeId(element);
	};
	
	exports.get = function (element) {
	  return instances[getId(element)];
	};


/***/ },
/* 42 */
/***/ function(module, exports) {

	'use strict';
	
	module.exports = {
	  handlers: ['click-rail', 'drag-scrollbar', 'keyboard', 'wheel', 'touch'],
	  maxScrollbarLength: null,
	  minScrollbarLength: null,
	  scrollXMarginOffset: 0,
	  scrollYMarginOffset: 0,
	  suppressScrollX: false,
	  suppressScrollY: false,
	  swipePropagation: true,
	  useBothWheelAxes: false,
	  wheelPropagation: false,
	  wheelSpeed: 1,
	  theme: 'default'
	};


/***/ },
/* 43 */
/***/ function(module, exports) {

	'use strict';
	
	var EventElement = function (element) {
	  this.element = element;
	  this.events = {};
	};
	
	EventElement.prototype.bind = function (eventName, handler) {
	  if (typeof this.events[eventName] === 'undefined') {
	    this.events[eventName] = [];
	  }
	  this.events[eventName].push(handler);
	  this.element.addEventListener(eventName, handler, false);
	};
	
	EventElement.prototype.unbind = function (eventName, handler) {
	  var isHandlerProvided = (typeof handler !== 'undefined');
	  this.events[eventName] = this.events[eventName].filter(function (hdlr) {
	    if (isHandlerProvided && hdlr !== handler) {
	      return true;
	    }
	    this.element.removeEventListener(eventName, hdlr, false);
	    return false;
	  }, this);
	};
	
	EventElement.prototype.unbindAll = function () {
	  for (var name in this.events) {
	    this.unbind(name);
	  }
	};
	
	var EventManager = function () {
	  this.eventElements = [];
	};
	
	EventManager.prototype.eventElement = function (element) {
	  var ee = this.eventElements.filter(function (eventElement) {
	    return eventElement.element === element;
	  })[0];
	  if (typeof ee === 'undefined') {
	    ee = new EventElement(element);
	    this.eventElements.push(ee);
	  }
	  return ee;
	};
	
	EventManager.prototype.bind = function (element, eventName, handler) {
	  this.eventElement(element).bind(eventName, handler);
	};
	
	EventManager.prototype.unbind = function (element, eventName, handler) {
	  this.eventElement(element).unbind(eventName, handler);
	};
	
	EventManager.prototype.unbindAll = function () {
	  for (var i = 0; i < this.eventElements.length; i++) {
	    this.eventElements[i].unbindAll();
	  }
	};
	
	EventManager.prototype.once = function (element, eventName, handler) {
	  var ee = this.eventElement(element);
	  var onceHandler = function (e) {
	    ee.unbind(eventName, onceHandler);
	    handler(e);
	  };
	  ee.bind(eventName, onceHandler);
	};
	
	module.exports = EventManager;


/***/ },
/* 44 */
/***/ function(module, exports) {

	'use strict';
	
	module.exports = (function () {
	  function s4() {
	    return Math.floor((1 + Math.random()) * 0x10000)
	               .toString(16)
	               .substring(1);
	  }
	  return function () {
	    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
	           s4() + '-' + s4() + s4() + s4();
	  };
	})();


/***/ },
/* 45 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _ = __webpack_require__(38);
	var cls = __webpack_require__(39);
	var instances = __webpack_require__(41);
	var updateGeometry = __webpack_require__(46);
	
	// Handlers
	var handlers = {
	  'click-rail': __webpack_require__(48),
	  'drag-scrollbar': __webpack_require__(49),
	  'keyboard': __webpack_require__(50),
	  'wheel': __webpack_require__(51),
	  'touch': __webpack_require__(52),
	  'selection': __webpack_require__(53)
	};
	var nativeScrollHandler = __webpack_require__(54);
	
	module.exports = function (element, userSettings) {
	  userSettings = typeof userSettings === 'object' ? userSettings : {};
	
	  cls.add(element, 'ps-container');
	
	  // Create a plugin instance.
	  var i = instances.add(element);
	
	  i.settings = _.extend(i.settings, userSettings);
	  cls.add(element, 'ps-theme-' + i.settings.theme);
	
	  i.settings.handlers.forEach(function (handlerName) {
	    handlers[handlerName](element);
	  });
	
	  nativeScrollHandler(element);
	
	  updateGeometry(element);
	};


/***/ },
/* 46 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _ = __webpack_require__(38);
	var cls = __webpack_require__(39);
	var dom = __webpack_require__(40);
	var instances = __webpack_require__(41);
	var updateScroll = __webpack_require__(47);
	
	function getThumbSize(i, thumbSize) {
	  if (i.settings.minScrollbarLength) {
	    thumbSize = Math.max(thumbSize, i.settings.minScrollbarLength);
	  }
	  if (i.settings.maxScrollbarLength) {
	    thumbSize = Math.min(thumbSize, i.settings.maxScrollbarLength);
	  }
	  return thumbSize;
	}
	
	function updateCss(element, i) {
	  var xRailOffset = {width: i.railXWidth};
	  if (i.isRtl) {
	    xRailOffset.left = i.negativeScrollAdjustment + element.scrollLeft + i.containerWidth - i.contentWidth;
	  } else {
	    xRailOffset.left = element.scrollLeft;
	  }
	  if (i.isScrollbarXUsingBottom) {
	    xRailOffset.bottom = i.scrollbarXBottom - element.scrollTop;
	  } else {
	    xRailOffset.top = i.scrollbarXTop + element.scrollTop;
	  }
	  dom.css(i.scrollbarXRail, xRailOffset);
	
	  var yRailOffset = {top: element.scrollTop, height: i.railYHeight};
	  if (i.isScrollbarYUsingRight) {
	    if (i.isRtl) {
	      yRailOffset.right = i.contentWidth - (i.negativeScrollAdjustment + element.scrollLeft) - i.scrollbarYRight - i.scrollbarYOuterWidth;
	    } else {
	      yRailOffset.right = i.scrollbarYRight - element.scrollLeft;
	    }
	  } else {
	    if (i.isRtl) {
	      yRailOffset.left = i.negativeScrollAdjustment + element.scrollLeft + i.containerWidth * 2 - i.contentWidth - i.scrollbarYLeft - i.scrollbarYOuterWidth;
	    } else {
	      yRailOffset.left = i.scrollbarYLeft + element.scrollLeft;
	    }
	  }
	  dom.css(i.scrollbarYRail, yRailOffset);
	
	  dom.css(i.scrollbarX, {left: i.scrollbarXLeft, width: i.scrollbarXWidth - i.railBorderXWidth});
	  dom.css(i.scrollbarY, {top: i.scrollbarYTop, height: i.scrollbarYHeight - i.railBorderYWidth});
	}
	
	module.exports = function (element) {
	  var i = instances.get(element);
	
	  i.containerWidth = element.clientWidth;
	  i.containerHeight = element.clientHeight;
	  i.contentWidth = element.scrollWidth;
	  i.contentHeight = element.scrollHeight;
	
	  var existingRails;
	  if (!element.contains(i.scrollbarXRail)) {
	    existingRails = dom.queryChildren(element, '.ps-scrollbar-x-rail');
	    if (existingRails.length > 0) {
	      existingRails.forEach(function (rail) {
	        dom.remove(rail);
	      });
	    }
	    dom.appendTo(i.scrollbarXRail, element);
	  }
	  if (!element.contains(i.scrollbarYRail)) {
	    existingRails = dom.queryChildren(element, '.ps-scrollbar-y-rail');
	    if (existingRails.length > 0) {
	      existingRails.forEach(function (rail) {
	        dom.remove(rail);
	      });
	    }
	    dom.appendTo(i.scrollbarYRail, element);
	  }
	
	  if (!i.settings.suppressScrollX && i.containerWidth + i.settings.scrollXMarginOffset < i.contentWidth) {
	    i.scrollbarXActive = true;
	    i.railXWidth = i.containerWidth - i.railXMarginWidth;
	    i.railXRatio = i.containerWidth / i.railXWidth;
	    i.scrollbarXWidth = getThumbSize(i, _.toInt(i.railXWidth * i.containerWidth / i.contentWidth));
	    i.scrollbarXLeft = _.toInt((i.negativeScrollAdjustment + element.scrollLeft) * (i.railXWidth - i.scrollbarXWidth) / (i.contentWidth - i.containerWidth));
	  } else {
	    i.scrollbarXActive = false;
	  }
	
	  if (!i.settings.suppressScrollY && i.containerHeight + i.settings.scrollYMarginOffset < i.contentHeight) {
	    i.scrollbarYActive = true;
	    i.railYHeight = i.containerHeight - i.railYMarginHeight;
	    i.railYRatio = i.containerHeight / i.railYHeight;
	    i.scrollbarYHeight = getThumbSize(i, _.toInt(i.railYHeight * i.containerHeight / i.contentHeight));
	    i.scrollbarYTop = _.toInt(element.scrollTop * (i.railYHeight - i.scrollbarYHeight) / (i.contentHeight - i.containerHeight));
	  } else {
	    i.scrollbarYActive = false;
	  }
	
	  if (i.scrollbarXLeft >= i.railXWidth - i.scrollbarXWidth) {
	    i.scrollbarXLeft = i.railXWidth - i.scrollbarXWidth;
	  }
	  if (i.scrollbarYTop >= i.railYHeight - i.scrollbarYHeight) {
	    i.scrollbarYTop = i.railYHeight - i.scrollbarYHeight;
	  }
	
	  updateCss(element, i);
	
	  if (i.scrollbarXActive) {
	    cls.add(element, 'ps-active-x');
	  } else {
	    cls.remove(element, 'ps-active-x');
	    i.scrollbarXWidth = 0;
	    i.scrollbarXLeft = 0;
	    updateScroll(element, 'left', 0);
	  }
	  if (i.scrollbarYActive) {
	    cls.add(element, 'ps-active-y');
	  } else {
	    cls.remove(element, 'ps-active-y');
	    i.scrollbarYHeight = 0;
	    i.scrollbarYTop = 0;
	    updateScroll(element, 'top', 0);
	  }
	};


/***/ },
/* 47 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var instances = __webpack_require__(41);
	
	var lastTop;
	var lastLeft;
	
	var createDOMEvent = function (name) {
	  var event = document.createEvent("Event");
	  event.initEvent(name, true, true);
	  return event;
	};
	
	module.exports = function (element, axis, value) {
	  if (typeof element === 'undefined') {
	    throw 'You must provide an element to the update-scroll function';
	  }
	
	  if (typeof axis === 'undefined') {
	    throw 'You must provide an axis to the update-scroll function';
	  }
	
	  if (typeof value === 'undefined') {
	    throw 'You must provide a value to the update-scroll function';
	  }
	
	  if (axis === 'top' && value <= 0) {
	    element.scrollTop = value = 0; // don't allow negative scroll
	    element.dispatchEvent(createDOMEvent('ps-y-reach-start'));
	  }
	
	  if (axis === 'left' && value <= 0) {
	    element.scrollLeft = value = 0; // don't allow negative scroll
	    element.dispatchEvent(createDOMEvent('ps-x-reach-start'));
	  }
	
	  var i = instances.get(element);
	
	  if (axis === 'top' && value >= i.contentHeight - i.containerHeight) {
	    // don't allow scroll past container
	    value = i.contentHeight - i.containerHeight;
	    if (value - element.scrollTop <= 1) {
	      // mitigates rounding errors on non-subpixel scroll values
	      value = element.scrollTop;
	    } else {
	      element.scrollTop = value;
	    }
	    element.dispatchEvent(createDOMEvent('ps-y-reach-end'));
	  }
	
	  if (axis === 'left' && value >= i.contentWidth - i.containerWidth) {
	    // don't allow scroll past container
	    value = i.contentWidth - i.containerWidth;
	    if (value - element.scrollLeft <= 1) {
	      // mitigates rounding errors on non-subpixel scroll values
	      value = element.scrollLeft;
	    } else {
	      element.scrollLeft = value;
	    }
	    element.dispatchEvent(createDOMEvent('ps-x-reach-end'));
	  }
	
	  if (!lastTop) {
	    lastTop = element.scrollTop;
	  }
	
	  if (!lastLeft) {
	    lastLeft = element.scrollLeft;
	  }
	
	  if (axis === 'top' && value < lastTop) {
	    element.dispatchEvent(createDOMEvent('ps-scroll-up'));
	  }
	
	  if (axis === 'top' && value > lastTop) {
	    element.dispatchEvent(createDOMEvent('ps-scroll-down'));
	  }
	
	  if (axis === 'left' && value < lastLeft) {
	    element.dispatchEvent(createDOMEvent('ps-scroll-left'));
	  }
	
	  if (axis === 'left' && value > lastLeft) {
	    element.dispatchEvent(createDOMEvent('ps-scroll-right'));
	  }
	
	  if (axis === 'top') {
	    element.scrollTop = lastTop = value;
	    element.dispatchEvent(createDOMEvent('ps-scroll-y'));
	  }
	
	  if (axis === 'left') {
	    element.scrollLeft = lastLeft = value;
	    element.dispatchEvent(createDOMEvent('ps-scroll-x'));
	  }
	
	};


/***/ },
/* 48 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var instances = __webpack_require__(41);
	var updateGeometry = __webpack_require__(46);
	var updateScroll = __webpack_require__(47);
	
	function bindClickRailHandler(element, i) {
	  function pageOffset(el) {
	    return el.getBoundingClientRect();
	  }
	  var stopPropagation = function (e) { e.stopPropagation(); };
	
	  i.event.bind(i.scrollbarY, 'click', stopPropagation);
	  i.event.bind(i.scrollbarYRail, 'click', function (e) {
	    var positionTop = e.pageY - window.pageYOffset - pageOffset(i.scrollbarYRail).top;
	    var direction = positionTop > i.scrollbarYTop ? 1 : -1;
	
	    updateScroll(element, 'top', element.scrollTop + direction * i.containerHeight);
	    updateGeometry(element);
	
	    e.stopPropagation();
	  });
	
	  i.event.bind(i.scrollbarX, 'click', stopPropagation);
	  i.event.bind(i.scrollbarXRail, 'click', function (e) {
	    var positionLeft = e.pageX - window.pageXOffset - pageOffset(i.scrollbarXRail).left;
	    var direction = positionLeft > i.scrollbarXLeft ? 1 : -1;
	
	    updateScroll(element, 'left', element.scrollLeft + direction * i.containerWidth);
	    updateGeometry(element);
	
	    e.stopPropagation();
	  });
	}
	
	module.exports = function (element) {
	  var i = instances.get(element);
	  bindClickRailHandler(element, i);
	};


/***/ },
/* 49 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _ = __webpack_require__(38);
	var dom = __webpack_require__(40);
	var instances = __webpack_require__(41);
	var updateGeometry = __webpack_require__(46);
	var updateScroll = __webpack_require__(47);
	
	function bindMouseScrollXHandler(element, i) {
	  var currentLeft = null;
	  var currentPageX = null;
	
	  function updateScrollLeft(deltaX) {
	    var newLeft = currentLeft + (deltaX * i.railXRatio);
	    var maxLeft = Math.max(0, i.scrollbarXRail.getBoundingClientRect().left) + (i.railXRatio * (i.railXWidth - i.scrollbarXWidth));
	
	    if (newLeft < 0) {
	      i.scrollbarXLeft = 0;
	    } else if (newLeft > maxLeft) {
	      i.scrollbarXLeft = maxLeft;
	    } else {
	      i.scrollbarXLeft = newLeft;
	    }
	
	    var scrollLeft = _.toInt(i.scrollbarXLeft * (i.contentWidth - i.containerWidth) / (i.containerWidth - (i.railXRatio * i.scrollbarXWidth))) - i.negativeScrollAdjustment;
	    updateScroll(element, 'left', scrollLeft);
	  }
	
	  var mouseMoveHandler = function (e) {
	    updateScrollLeft(e.pageX - currentPageX);
	    updateGeometry(element);
	    e.stopPropagation();
	    e.preventDefault();
	  };
	
	  var mouseUpHandler = function () {
	    _.stopScrolling(element, 'x');
	    i.event.unbind(i.ownerDocument, 'mousemove', mouseMoveHandler);
	  };
	
	  i.event.bind(i.scrollbarX, 'mousedown', function (e) {
	    currentPageX = e.pageX;
	    currentLeft = _.toInt(dom.css(i.scrollbarX, 'left')) * i.railXRatio;
	    _.startScrolling(element, 'x');
	
	    i.event.bind(i.ownerDocument, 'mousemove', mouseMoveHandler);
	    i.event.once(i.ownerDocument, 'mouseup', mouseUpHandler);
	
	    e.stopPropagation();
	    e.preventDefault();
	  });
	}
	
	function bindMouseScrollYHandler(element, i) {
	  var currentTop = null;
	  var currentPageY = null;
	
	  function updateScrollTop(deltaY) {
	    var newTop = currentTop + (deltaY * i.railYRatio);
	    var maxTop = Math.max(0, i.scrollbarYRail.getBoundingClientRect().top) + (i.railYRatio * (i.railYHeight - i.scrollbarYHeight));
	
	    if (newTop < 0) {
	      i.scrollbarYTop = 0;
	    } else if (newTop > maxTop) {
	      i.scrollbarYTop = maxTop;
	    } else {
	      i.scrollbarYTop = newTop;
	    }
	
	    var scrollTop = _.toInt(i.scrollbarYTop * (i.contentHeight - i.containerHeight) / (i.containerHeight - (i.railYRatio * i.scrollbarYHeight)));
	    updateScroll(element, 'top', scrollTop);
	  }
	
	  var mouseMoveHandler = function (e) {
	    updateScrollTop(e.pageY - currentPageY);
	    updateGeometry(element);
	    e.stopPropagation();
	    e.preventDefault();
	  };
	
	  var mouseUpHandler = function () {
	    _.stopScrolling(element, 'y');
	    i.event.unbind(i.ownerDocument, 'mousemove', mouseMoveHandler);
	  };
	
	  i.event.bind(i.scrollbarY, 'mousedown', function (e) {
	    currentPageY = e.pageY;
	    currentTop = _.toInt(dom.css(i.scrollbarY, 'top')) * i.railYRatio;
	    _.startScrolling(element, 'y');
	
	    i.event.bind(i.ownerDocument, 'mousemove', mouseMoveHandler);
	    i.event.once(i.ownerDocument, 'mouseup', mouseUpHandler);
	
	    e.stopPropagation();
	    e.preventDefault();
	  });
	}
	
	module.exports = function (element) {
	  var i = instances.get(element);
	  bindMouseScrollXHandler(element, i);
	  bindMouseScrollYHandler(element, i);
	};


/***/ },
/* 50 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _ = __webpack_require__(38);
	var dom = __webpack_require__(40);
	var instances = __webpack_require__(41);
	var updateGeometry = __webpack_require__(46);
	var updateScroll = __webpack_require__(47);
	
	function bindKeyboardHandler(element, i) {
	  var hovered = false;
	  i.event.bind(element, 'mouseenter', function () {
	    hovered = true;
	  });
	  i.event.bind(element, 'mouseleave', function () {
	    hovered = false;
	  });
	
	  var shouldPrevent = false;
	  function shouldPreventDefault(deltaX, deltaY) {
	    var scrollTop = element.scrollTop;
	    if (deltaX === 0) {
	      if (!i.scrollbarYActive) {
	        return false;
	      }
	      if ((scrollTop === 0 && deltaY > 0) || (scrollTop >= i.contentHeight - i.containerHeight && deltaY < 0)) {
	        return !i.settings.wheelPropagation;
	      }
	    }
	
	    var scrollLeft = element.scrollLeft;
	    if (deltaY === 0) {
	      if (!i.scrollbarXActive) {
	        return false;
	      }
	      if ((scrollLeft === 0 && deltaX < 0) || (scrollLeft >= i.contentWidth - i.containerWidth && deltaX > 0)) {
	        return !i.settings.wheelPropagation;
	      }
	    }
	    return true;
	  }
	
	  i.event.bind(i.ownerDocument, 'keydown', function (e) {
	    if ((e.isDefaultPrevented && e.isDefaultPrevented()) || e.defaultPrevented) {
	      return;
	    }
	
	    var focused = dom.matches(i.scrollbarX, ':focus') ||
	                  dom.matches(i.scrollbarY, ':focus');
	
	    if (!hovered && !focused) {
	      return;
	    }
	
	    var activeElement = document.activeElement ? document.activeElement : i.ownerDocument.activeElement;
	    if (activeElement) {
	      if (activeElement.tagName === 'IFRAME') {
	        activeElement = activeElement.contentDocument.activeElement;
	      } else {
	        // go deeper if element is a webcomponent
	        while (activeElement.shadowRoot) {
	          activeElement = activeElement.shadowRoot.activeElement;
	        }
	      }
	      if (_.isEditable(activeElement)) {
	        return;
	      }
	    }
	
	    var deltaX = 0;
	    var deltaY = 0;
	
	    switch (e.which) {
	    case 37: // left
	      if (e.metaKey) {
	        deltaX = -i.contentWidth;
	      } else if (e.altKey) {
	        deltaX = -i.containerWidth;
	      } else {
	        deltaX = -30;
	      }
	      break;
	    case 38: // up
	      if (e.metaKey) {
	        deltaY = i.contentHeight;
	      } else if (e.altKey) {
	        deltaY = i.containerHeight;
	      } else {
	        deltaY = 30;
	      }
	      break;
	    case 39: // right
	      if (e.metaKey) {
	        deltaX = i.contentWidth;
	      } else if (e.altKey) {
	        deltaX = i.containerWidth;
	      } else {
	        deltaX = 30;
	      }
	      break;
	    case 40: // down
	      if (e.metaKey) {
	        deltaY = -i.contentHeight;
	      } else if (e.altKey) {
	        deltaY = -i.containerHeight;
	      } else {
	        deltaY = -30;
	      }
	      break;
	    case 33: // page up
	      deltaY = 90;
	      break;
	    case 32: // space bar
	      if (e.shiftKey) {
	        deltaY = 90;
	      } else {
	        deltaY = -90;
	      }
	      break;
	    case 34: // page down
	      deltaY = -90;
	      break;
	    case 35: // end
	      if (e.ctrlKey) {
	        deltaY = -i.contentHeight;
	      } else {
	        deltaY = -i.containerHeight;
	      }
	      break;
	    case 36: // home
	      if (e.ctrlKey) {
	        deltaY = element.scrollTop;
	      } else {
	        deltaY = i.containerHeight;
	      }
	      break;
	    default:
	      return;
	    }
	
	    updateScroll(element, 'top', element.scrollTop - deltaY);
	    updateScroll(element, 'left', element.scrollLeft + deltaX);
	    updateGeometry(element);
	
	    shouldPrevent = shouldPreventDefault(deltaX, deltaY);
	    if (shouldPrevent) {
	      e.preventDefault();
	    }
	  });
	}
	
	module.exports = function (element) {
	  var i = instances.get(element);
	  bindKeyboardHandler(element, i);
	};


/***/ },
/* 51 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var instances = __webpack_require__(41);
	var updateGeometry = __webpack_require__(46);
	var updateScroll = __webpack_require__(47);
	
	function bindMouseWheelHandler(element, i) {
	  var shouldPrevent = false;
	
	  function shouldPreventDefault(deltaX, deltaY) {
	    var scrollTop = element.scrollTop;
	    if (deltaX === 0) {
	      if (!i.scrollbarYActive) {
	        return false;
	      }
	      if ((scrollTop === 0 && deltaY > 0) || (scrollTop >= i.contentHeight - i.containerHeight && deltaY < 0)) {
	        return !i.settings.wheelPropagation;
	      }
	    }
	
	    var scrollLeft = element.scrollLeft;
	    if (deltaY === 0) {
	      if (!i.scrollbarXActive) {
	        return false;
	      }
	      if ((scrollLeft === 0 && deltaX < 0) || (scrollLeft >= i.contentWidth - i.containerWidth && deltaX > 0)) {
	        return !i.settings.wheelPropagation;
	      }
	    }
	    return true;
	  }
	
	  function getDeltaFromEvent(e) {
	    var deltaX = e.deltaX;
	    var deltaY = -1 * e.deltaY;
	
	    if (typeof deltaX === "undefined" || typeof deltaY === "undefined") {
	      // OS X Safari
	      deltaX = -1 * e.wheelDeltaX / 6;
	      deltaY = e.wheelDeltaY / 6;
	    }
	
	    if (e.deltaMode && e.deltaMode === 1) {
	      // Firefox in deltaMode 1: Line scrolling
	      deltaX *= 10;
	      deltaY *= 10;
	    }
	
	    if (deltaX !== deltaX && deltaY !== deltaY/* NaN checks */) {
	      // IE in some mouse drivers
	      deltaX = 0;
	      deltaY = e.wheelDelta;
	    }
	
	    if (e.shiftKey) {
	      // reverse axis with shift key
	      return [-deltaY, -deltaX];
	    }
	    return [deltaX, deltaY];
	  }
	
	  function shouldBeConsumedByChild(deltaX, deltaY) {
	    var child = element.querySelector('textarea:hover, select[multiple]:hover, .ps-child:hover');
	    if (child) {
	      if (!window.getComputedStyle(child).overflow.match(/(scroll|auto)/)) {
	        // if not scrollable
	        return false;
	      }
	
	      var maxScrollTop = child.scrollHeight - child.clientHeight;
	      if (maxScrollTop > 0) {
	        if (!(child.scrollTop === 0 && deltaY > 0) && !(child.scrollTop === maxScrollTop && deltaY < 0)) {
	          return true;
	        }
	      }
	      var maxScrollLeft = child.scrollLeft - child.clientWidth;
	      if (maxScrollLeft > 0) {
	        if (!(child.scrollLeft === 0 && deltaX < 0) && !(child.scrollLeft === maxScrollLeft && deltaX > 0)) {
	          return true;
	        }
	      }
	    }
	    return false;
	  }
	
	  function mousewheelHandler(e) {
	    var delta = getDeltaFromEvent(e);
	
	    var deltaX = delta[0];
	    var deltaY = delta[1];
	
	    if (shouldBeConsumedByChild(deltaX, deltaY)) {
	      return;
	    }
	
	    shouldPrevent = false;
	    if (!i.settings.useBothWheelAxes) {
	      // deltaX will only be used for horizontal scrolling and deltaY will
	      // only be used for vertical scrolling - this is the default
	      updateScroll(element, 'top', element.scrollTop - (deltaY * i.settings.wheelSpeed));
	      updateScroll(element, 'left', element.scrollLeft + (deltaX * i.settings.wheelSpeed));
	    } else if (i.scrollbarYActive && !i.scrollbarXActive) {
	      // only vertical scrollbar is active and useBothWheelAxes option is
	      // active, so let's scroll vertical bar using both mouse wheel axes
	      if (deltaY) {
	        updateScroll(element, 'top', element.scrollTop - (deltaY * i.settings.wheelSpeed));
	      } else {
	        updateScroll(element, 'top', element.scrollTop + (deltaX * i.settings.wheelSpeed));
	      }
	      shouldPrevent = true;
	    } else if (i.scrollbarXActive && !i.scrollbarYActive) {
	      // useBothWheelAxes and only horizontal bar is active, so use both
	      // wheel axes for horizontal bar
	      if (deltaX) {
	        updateScroll(element, 'left', element.scrollLeft + (deltaX * i.settings.wheelSpeed));
	      } else {
	        updateScroll(element, 'left', element.scrollLeft - (deltaY * i.settings.wheelSpeed));
	      }
	      shouldPrevent = true;
	    }
	
	    updateGeometry(element);
	
	    shouldPrevent = (shouldPrevent || shouldPreventDefault(deltaX, deltaY));
	    if (shouldPrevent) {
	      e.stopPropagation();
	      e.preventDefault();
	    }
	  }
	
	  if (typeof window.onwheel !== "undefined") {
	    i.event.bind(element, 'wheel', mousewheelHandler);
	  } else if (typeof window.onmousewheel !== "undefined") {
	    i.event.bind(element, 'mousewheel', mousewheelHandler);
	  }
	}
	
	module.exports = function (element) {
	  var i = instances.get(element);
	  bindMouseWheelHandler(element, i);
	};


/***/ },
/* 52 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _ = __webpack_require__(38);
	var instances = __webpack_require__(41);
	var updateGeometry = __webpack_require__(46);
	var updateScroll = __webpack_require__(47);
	
	function bindTouchHandler(element, i, supportsTouch, supportsIePointer) {
	  function shouldPreventDefault(deltaX, deltaY) {
	    var scrollTop = element.scrollTop;
	    var scrollLeft = element.scrollLeft;
	    var magnitudeX = Math.abs(deltaX);
	    var magnitudeY = Math.abs(deltaY);
	
	    if (magnitudeY > magnitudeX) {
	      // user is perhaps trying to swipe up/down the page
	
	      if (((deltaY < 0) && (scrollTop === i.contentHeight - i.containerHeight)) ||
	          ((deltaY > 0) && (scrollTop === 0))) {
	        return !i.settings.swipePropagation;
	      }
	    } else if (magnitudeX > magnitudeY) {
	      // user is perhaps trying to swipe left/right across the page
	
	      if (((deltaX < 0) && (scrollLeft === i.contentWidth - i.containerWidth)) ||
	          ((deltaX > 0) && (scrollLeft === 0))) {
	        return !i.settings.swipePropagation;
	      }
	    }
	
	    return true;
	  }
	
	  function applyTouchMove(differenceX, differenceY) {
	    updateScroll(element, 'top', element.scrollTop - differenceY);
	    updateScroll(element, 'left', element.scrollLeft - differenceX);
	
	    updateGeometry(element);
	  }
	
	  var startOffset = {};
	  var startTime = 0;
	  var speed = {};
	  var easingLoop = null;
	  var inGlobalTouch = false;
	  var inLocalTouch = false;
	
	  function globalTouchStart() {
	    inGlobalTouch = true;
	  }
	  function globalTouchEnd() {
	    inGlobalTouch = false;
	  }
	
	  function getTouch(e) {
	    if (e.targetTouches) {
	      return e.targetTouches[0];
	    } else {
	      // Maybe IE pointer
	      return e;
	    }
	  }
	  function shouldHandle(e) {
	    if (e.targetTouches && e.targetTouches.length === 1) {
	      return true;
	    }
	    if (e.pointerType && e.pointerType !== 'mouse' && e.pointerType !== e.MSPOINTER_TYPE_MOUSE) {
	      return true;
	    }
	    return false;
	  }
	  function touchStart(e) {
	    if (shouldHandle(e)) {
	      inLocalTouch = true;
	
	      var touch = getTouch(e);
	
	      startOffset.pageX = touch.pageX;
	      startOffset.pageY = touch.pageY;
	
	      startTime = (new Date()).getTime();
	
	      if (easingLoop !== null) {
	        clearInterval(easingLoop);
	      }
	
	      e.stopPropagation();
	    }
	  }
	  function touchMove(e) {
	    if (!inLocalTouch && i.settings.swipePropagation) {
	      touchStart(e);
	    }
	    if (!inGlobalTouch && inLocalTouch && shouldHandle(e)) {
	      var touch = getTouch(e);
	
	      var currentOffset = {pageX: touch.pageX, pageY: touch.pageY};
	
	      var differenceX = currentOffset.pageX - startOffset.pageX;
	      var differenceY = currentOffset.pageY - startOffset.pageY;
	
	      applyTouchMove(differenceX, differenceY);
	      startOffset = currentOffset;
	
	      var currentTime = (new Date()).getTime();
	
	      var timeGap = currentTime - startTime;
	      if (timeGap > 0) {
	        speed.x = differenceX / timeGap;
	        speed.y = differenceY / timeGap;
	        startTime = currentTime;
	      }
	
	      if (shouldPreventDefault(differenceX, differenceY)) {
	        e.stopPropagation();
	        e.preventDefault();
	      }
	    }
	  }
	  function touchEnd() {
	    if (!inGlobalTouch && inLocalTouch) {
	      inLocalTouch = false;
	
	      clearInterval(easingLoop);
	      easingLoop = setInterval(function () {
	        if (!instances.get(element)) {
	          clearInterval(easingLoop);
	          return;
	        }
	
	        if (!speed.x && !speed.y) {
	          clearInterval(easingLoop);
	          return;
	        }
	
	        if (Math.abs(speed.x) < 0.01 && Math.abs(speed.y) < 0.01) {
	          clearInterval(easingLoop);
	          return;
	        }
	
	        applyTouchMove(speed.x * 30, speed.y * 30);
	
	        speed.x *= 0.8;
	        speed.y *= 0.8;
	      }, 10);
	    }
	  }
	
	  if (supportsTouch) {
	    i.event.bind(window, 'touchstart', globalTouchStart);
	    i.event.bind(window, 'touchend', globalTouchEnd);
	    i.event.bind(element, 'touchstart', touchStart);
	    i.event.bind(element, 'touchmove', touchMove);
	    i.event.bind(element, 'touchend', touchEnd);
	  }
	
	  if (supportsIePointer) {
	    if (window.PointerEvent) {
	      i.event.bind(window, 'pointerdown', globalTouchStart);
	      i.event.bind(window, 'pointerup', globalTouchEnd);
	      i.event.bind(element, 'pointerdown', touchStart);
	      i.event.bind(element, 'pointermove', touchMove);
	      i.event.bind(element, 'pointerup', touchEnd);
	    } else if (window.MSPointerEvent) {
	      i.event.bind(window, 'MSPointerDown', globalTouchStart);
	      i.event.bind(window, 'MSPointerUp', globalTouchEnd);
	      i.event.bind(element, 'MSPointerDown', touchStart);
	      i.event.bind(element, 'MSPointerMove', touchMove);
	      i.event.bind(element, 'MSPointerUp', touchEnd);
	    }
	  }
	}
	
	module.exports = function (element) {
	  if (!_.env.supportsTouch && !_.env.supportsIePointer) {
	    return;
	  }
	
	  var i = instances.get(element);
	  bindTouchHandler(element, i, _.env.supportsTouch, _.env.supportsIePointer);
	};


/***/ },
/* 53 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _ = __webpack_require__(38);
	var instances = __webpack_require__(41);
	var updateGeometry = __webpack_require__(46);
	var updateScroll = __webpack_require__(47);
	
	function bindSelectionHandler(element, i) {
	  function getRangeNode() {
	    var selection = window.getSelection ? window.getSelection() :
	                    document.getSelection ? document.getSelection() : '';
	    if (selection.toString().length === 0) {
	      return null;
	    } else {
	      return selection.getRangeAt(0).commonAncestorContainer;
	    }
	  }
	
	  var scrollingLoop = null;
	  var scrollDiff = {top: 0, left: 0};
	  function startScrolling() {
	    if (!scrollingLoop) {
	      scrollingLoop = setInterval(function () {
	        if (!instances.get(element)) {
	          clearInterval(scrollingLoop);
	          return;
	        }
	
	        updateScroll(element, 'top', element.scrollTop + scrollDiff.top);
	        updateScroll(element, 'left', element.scrollLeft + scrollDiff.left);
	        updateGeometry(element);
	      }, 50); // every .1 sec
	    }
	  }
	  function stopScrolling() {
	    if (scrollingLoop) {
	      clearInterval(scrollingLoop);
	      scrollingLoop = null;
	    }
	    _.stopScrolling(element);
	  }
	
	  var isSelected = false;
	  i.event.bind(i.ownerDocument, 'selectionchange', function () {
	    if (element.contains(getRangeNode())) {
	      isSelected = true;
	    } else {
	      isSelected = false;
	      stopScrolling();
	    }
	  });
	  i.event.bind(window, 'mouseup', function () {
	    if (isSelected) {
	      isSelected = false;
	      stopScrolling();
	    }
	  });
	  i.event.bind(window, 'keyup', function () {
	    if (isSelected) {
	      isSelected = false;
	      stopScrolling();
	    }
	  });
	
	  i.event.bind(window, 'mousemove', function (e) {
	    if (isSelected) {
	      var mousePosition = {x: e.pageX, y: e.pageY};
	      var containerGeometry = {
	        left: element.offsetLeft,
	        right: element.offsetLeft + element.offsetWidth,
	        top: element.offsetTop,
	        bottom: element.offsetTop + element.offsetHeight
	      };
	
	      if (mousePosition.x < containerGeometry.left + 3) {
	        scrollDiff.left = -5;
	        _.startScrolling(element, 'x');
	      } else if (mousePosition.x > containerGeometry.right - 3) {
	        scrollDiff.left = 5;
	        _.startScrolling(element, 'x');
	      } else {
	        scrollDiff.left = 0;
	      }
	
	      if (mousePosition.y < containerGeometry.top + 3) {
	        if (containerGeometry.top + 3 - mousePosition.y < 5) {
	          scrollDiff.top = -5;
	        } else {
	          scrollDiff.top = -20;
	        }
	        _.startScrolling(element, 'y');
	      } else if (mousePosition.y > containerGeometry.bottom - 3) {
	        if (mousePosition.y - containerGeometry.bottom + 3 < 5) {
	          scrollDiff.top = 5;
	        } else {
	          scrollDiff.top = 20;
	        }
	        _.startScrolling(element, 'y');
	      } else {
	        scrollDiff.top = 0;
	      }
	
	      if (scrollDiff.top === 0 && scrollDiff.left === 0) {
	        stopScrolling();
	      } else {
	        startScrolling();
	      }
	    }
	  });
	}
	
	module.exports = function (element) {
	  var i = instances.get(element);
	  bindSelectionHandler(element, i);
	};


/***/ },
/* 54 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var instances = __webpack_require__(41);
	var updateGeometry = __webpack_require__(46);
	
	function bindNativeScrollHandler(element, i) {
	  i.event.bind(element, 'scroll', function () {
	    updateGeometry(element);
	  });
	}
	
	module.exports = function (element) {
	  var i = instances.get(element);
	  bindNativeScrollHandler(element, i);
	};


/***/ },
/* 55 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _ = __webpack_require__(38);
	var dom = __webpack_require__(40);
	var instances = __webpack_require__(41);
	var updateGeometry = __webpack_require__(46);
	var updateScroll = __webpack_require__(47);
	
	module.exports = function (element) {
	  var i = instances.get(element);
	
	  if (!i) {
	    return;
	  }
	
	  // Recalcuate negative scrollLeft adjustment
	  i.negativeScrollAdjustment = i.isNegativeScroll ? element.scrollWidth - element.clientWidth : 0;
	
	  // Recalculate rail margins
	  dom.css(i.scrollbarXRail, 'display', 'block');
	  dom.css(i.scrollbarYRail, 'display', 'block');
	  i.railXMarginWidth = _.toInt(dom.css(i.scrollbarXRail, 'marginLeft')) + _.toInt(dom.css(i.scrollbarXRail, 'marginRight'));
	  i.railYMarginHeight = _.toInt(dom.css(i.scrollbarYRail, 'marginTop')) + _.toInt(dom.css(i.scrollbarYRail, 'marginBottom'));
	
	  // Hide scrollbars not to affect scrollWidth and scrollHeight
	  dom.css(i.scrollbarXRail, 'display', 'none');
	  dom.css(i.scrollbarYRail, 'display', 'none');
	
	  updateGeometry(element);
	
	  // Update top/left scroll to trigger events
	  updateScroll(element, 'top', element.scrollTop);
	  updateScroll(element, 'left', element.scrollLeft);
	
	  dom.css(i.scrollbarXRail, 'display', '');
	  dom.css(i.scrollbarYRail, 'display', '');
	};


/***/ }
]));
//# sourceMappingURL=chunk-6-64a2bed2d9afc008db3a.js.map