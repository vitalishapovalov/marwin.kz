'use strict';

/**
 * App entry point.
 *
 * @module App
 */

/** Import commonly used libs (initialized by default) */
import './modules/dep/owl.carousel-custom';
import './modules/dep/bootstrap-custom';

/** Import common components (initialized by default) */
import Header from 'js/components/Header';
Header.init();
import Footer from 'js/components/Footer';
Footer.init();
import Menu from 'js/components/Menu';
Menu.init();
import Common from 'js/components/Common';
Common.init();
import ProductItem from 'js/components/ProductItem';
ProductItem.init();

/** Import page controllers */
import Main from 'js/pages/Main';
import Catalog from 'js/pages/Catalog';
import Filter from 'js/pages/Filter';
import Product from 'js/pages/Product';
import Cart from 'js/pages/Cart';
import Registration from 'js/pages/Registration';
import Search from 'js/pages/Search';
import News from 'js/pages/News';
import Calendar from 'js/pages/Calendar';
import Shops from 'js/pages/Shops';

/** Import utility functions */
import {
  currentPage
} from 'js/modules/dev/Helpers';

/**
 * Detect current page and run appropriate scripts.
 *
 * @param {String} currentPage - current page detector.
 **/
switch (currentPage) {
  /** Main page */
  case 'main': {
    Main.init();
    break
  }
  /** Catalog page */
  case 'catalog': {
    Catalog.init();
    break
  }
  /** Filter page */
  case 'filter': {
    Filter.init();
    break
  }
  /** Product page */
  case 'product': {
    Product.init();
    break
  }
  /** Cart page */
  case 'cart': {
    Cart.init();
    break
  }
  /** Registration page */
  case 'registration': {
    Registration.init();
    break
  }
  /** 'Search results' page */
  case 'search': {
    Search.init();
    break
  }
  /** News & Sales pages */
  case 'news': {
    News.init();
    break
  }
  /** Calendar page */
  case 'calendar': {
    Calendar.init();
    break
  }
  /** Shops pages */
  case 'shops': {
    Shops.init();
    break
  }
}
