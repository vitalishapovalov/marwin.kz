'use strict';

/**
 * Animated svg calendar using Snap.svg
 *
 * @module Calendar.svg
 */

/** Import utility functions */
import {
  body,
} from 'js/modules/dev/Helpers';

export default class Calendar {
  /**
   * @param {jQuery|HTMLElement|String} container - container to append svg
   */
  constructor(container = '.calendar-svg-container') {
    this.$svgContainer = $(container);

    this.currentMonth = new Date().getMonth() + 1;
    this.currentYear = new Date().getFullYear();

    this.realMonth = new Date().getMonth() + 1;

    this.$svgLayout = $('#calendar svg');
    this.$calendarLayout = this.$svgLayout.find('.calendar__layout');

    // buttons
    this.$prevButton = this.$svgLayout.find('.calendar__prev-month');
    this.$nextButton = this.$svgLayout.find('.calendar__next-month');

    // days
    this.$daysContainer = this.$svgLayout.find('.calendar__days-container');
    this.$days = () => this.$svgLayout.find('.calendar__day');
    this.daysInitialTranslateX = '0.318';
    this.daysInitialTranslateY = '0.218';
    this.daysOfWeek = ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'];

    /**
     * Load GSAP and run Calendar.svg
     */
    require.ensure([], () => {
      const gsap = require('gsap');

      this.runCalendarScripts();
    });
  }

  static getDaysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
  }

  static getFirstDayInMonth(month, year) {
    const day =  new Date(`${year}-${month}-01`).getDay();
    return day === 0 ? 7 : day;
  }

  changeCalendarsMonth() {
    TweenMax.to(this.$daysContainer, 1, {
      css: { x: '-10px', opacity: '0' },
      ease: Power2.easeInOut,
    }, () => this.$days().remove());
  }

  bindMonthsChangeButtons() {
    /*// previous month
    body.on('click tap', this.$prevButton, () => {
      if (this.currentMonth === 1) {
        this.currentYear--;
        this.currentMonth = 12;
      } else {
        this.currentMonth--;
      }

      this.changeCalendarsMonth();
    });*/

    // next month
    body.on('click tap', this.$nextButton, () => {
      if (this.currentMonth === 12) {
        this.currentYear++;
        this.currentMonth = 1;
      } else {
        this.currentMonth++;
      }

      this.changeCalendarsMonth();
    });
  }

  /**
   * Initialize Calendar.svg event handlers
   */
  runCalendarScripts() {
    // bind next/prev month buttons
    this.bindMonthsChangeButtons();
  }
};
