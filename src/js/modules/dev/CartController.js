'use strict';

/**
 * Cart functions (add to cart etc.)
 *
 * @module CartController
 */

/** Import utility functions */
import {
  body,
  has,
} from 'js/modules/dev/Helpers';

/**
 * @class CartController
 * @classdesc Class representing a website's cart actions.
 */
export default class CartController {
  constructor() {
    /** Cart */
    this.cartCount = $('.header-cart .cart');
    this.cartPrice = $('.header-cart .subtitle');
    this.cartIsEmptyText = 'Ваша корзина пуста';

    /** AJAX URLs */
    this.cartUrl = './ajax/success-cart.json';
    this.getItemsInCartUrl = './ajax/cart.json';

    /** Class-indicators names */
    this.purchasedClassName = 'added';
    this.cartIsActiveClass = 'cart-active';

    /** AJAX event names */
    this.getItemsInCartEventName = 'getCart';

    /** Other */
    this.buttonStates = {
      pending: 'pending',
      completed: 'completed'
    };

    return this
  }

  /**
   * Get current count.
   *
   * @static
   */
  static get getCurrentCount() {
    return localStorage.getItem('userProductsInCartCount')
  }

  /**
   * Get total price.
   *
   * @static
   */
  static get getTotalPrice() {
    return localStorage.getItem('userProductsInCartTotalPrice')
  }

  /**
   * Set current count.
   *
   * @static
   */
  static set currentCount(newCurrentCount) {
    localStorage.setItem('userProductsInCartCount', newCurrentCount);
  }

  /**
   * Set total price.
   *
   * @static
   */
  static set totalPrice(newPrice) {
    localStorage.setItem('userProductsInCartTotalPrice', newPrice);
  }

  /**
   * Set button state (request pending / completed)
   *
   * @param {jQuery} product
   * @param {String} state - state to set.
   * @return {CartController}
   */
  setButtonState(product, state) {
    for (const state in this.buttonStates) {
      if (has.call(this.buttonStates, state)) product.removeClass(state)
    }

    product.addClass(state)
  }

  /**
   * Set cart data.
   *
   * @param {Number} newAmount - elements in cart.
   * @param {Number} newPrice - total price.
   * @return {CartController}
   */
  setCartData(newAmount, newPrice) {
    CartController.currentCount = newAmount;
    CartController.totalPrice = newPrice;

    return this
  }

  /**
   * Change cart displayed data.
   *
   * @return {CartController}
   */
  refreshDisplayedCartData() {
    this.cartCount.text(CartController.getCurrentCount);
    this.cartPrice.text(`${CartController.getTotalPrice} т`);

    const className = this.cartIsActiveClass;
    if (+CartController.getCurrentCount > 0) {
      !body.hasClass(className) ? body.addClass(className) : null;
    } else {
      body.removeClass(className);

      this.cartPrice.text(this.cartIsEmptyText);
    }

    return this
  }

  /**
   * Set and change displayed cart data.
   *
   * @param {Number} newAmount - elements in cart.
   * @param {Number} newPrice - total price.
   * @return {CartController}
   */
  updateCartData(newPrice, newAmount = 1) {
    const currentTotalPrice = +CartController.getTotalPrice;
    const currentTotalCount = +CartController.getCurrentCount;

    newAmount = currentTotalCount + newAmount;
    newPrice = currentTotalPrice + newPrice;

    this.setCartData(newAmount, newPrice).refreshDisplayedCartData();

    return this
  }

  /**
   * Get current products in cart.
   */
  getCurrentProducts() {
    const data = {
      event: this.getItemsInCartEventName
    };

    $.ajax({
      url: this.getItemsInCartUrl,
      dataType: 'json',
      method: 'GET',
      data: data,
      success: (res) => {
        const totalAmount = +res.totalAmount;
        const totalPrice = +res.totalPrice;

        if (totalAmount && totalPrice) {
          this.setCartData(totalAmount, totalPrice)
            .refreshDisplayedCartData()
        }
      }
    });
  }
}

/**
 * Cache cart's DOM elements and bind 'Buy' button.
 *
 * @class CartItem
 * @classdesc Class used for adding products to cart.
 * @extends CartController
 *
 * @param {String} addButton - this product's 'buy' button selector
 * @param {String} removeButton - this product's 'remove' button selector
 * @param {String} dataObject - object with product data (id, price etc.) selector
 */
export class CartItem extends CartController {
  constructor(addButton = '', removeButton = '', dataObject = '') {
    super();
    this.buyButton = addButton;
    this.dataObject = dataObject;
    this.removeButton = removeButton;

    this.buyEventName = 'buyCart';
    this.removeEventName = 'removeCart';
  }

  /**
   * Change product quantity (add/remove).
   *
   * @param {String} product - product object
   * @param {String} event - Event name ('add' or 'remove').
   * @return {CartItem}
   */
  changeCartQuantity(product, event) {
    product = $(product);
    let id = product.data('id');
    let price = product.data('price');
    const purchasedClassName = this.purchasedClassName;
    const data = {
      event: event,
      id: id,
      price: price
    };

    const checkEvent = function(buyCB, removeCB) {
      switch (event) {
        case this.buyEventName: {
          buyCB.call(this);
          product.addClass(purchasedClassName);
          break
        }
        case this.removeEventName: {
          removeCB.call(this);
          product.removeClass(purchasedClassName);
          break
        }
      }
    }.bind(this);

    $.ajax({
      url: this.cartUrl,
      dataType: 'json',
      method: 'POST',
      cache: false,
      data: data,
      beforeSend: () => {
        this.setButtonState(product, this.buttonStates.pending)
      },
      success: (res) => {
        console.log(`DATA:\n${JSON.stringify(data)}`);
        const response = res.response;

        if (response === 'success') {
          checkEvent(() => {
            this.updateCartData(+price);
          }, () => {
            this.updateCartData(-(+price), -1);
          });
        } else if (response === 'change') {
          this.setCartData(res.basket.amount, res.basket.price).refreshDisplayedCartData();
        }
      },
      complete: () => {
        this.setButtonState(product, this.buttonStates.completed);
      },
    });

    return this
  }

  /**
   * Bind action buttons.
   */
  bind() {
    const _this = this;

    function bindButton(type, button) {
      body.on('click tap', button, function() {
        let dataObject;

        if (!_this.dataObject) {
          dataObject = $(this).parents('.default-item');
        } else {
          dataObject = _this.dataObject
        }

        _this.changeCartQuantity(dataObject, type)
      });
    }

    bindButton(this.buyEventName, this.buyButton);
    bindButton(this.removeEventName, this.removeButton)
  }
}