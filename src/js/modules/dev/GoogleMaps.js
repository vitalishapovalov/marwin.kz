'use strict';

/**
 * @class GoogleMaps
 * @classdesc Class used to initialize google maps, load markes, bind onClick event.
 */
export default class GoogleMaps {
  /**
   * Generate and set-up default settings.
   *
   * @constructor
   *
   * @param {Object} [options]
   * @param {Object} [markers]
   * @param {jQuery} [clickableElements]
   *
   * @param {Node} [options.map] - map container node (default: document.getElementById('map'))
   * @param {String} [options.callbackName] - function inited on ggl maps script load name (default: "initMapId1")
   * @param {String} [options.scriptUrl] - ggl maps api script url (default: "https://maps.googleapis.com/maps/api/js")
   * @param {String} [options.scriptKey] - ggl maps key id (default: "key=AIzaSyCrFjHOCFQbsMvKEGL92MTAffwFyHgogZI")
   * @param {String} [options.scriptOptions] - ggl maps options added to script's url (default: `&language=ru&callback=${this.callbackName}`)
   * @param {Object} [options.mapInitOptions] - ggl maps on create options (default: {center: {lat: 48,lng: 67.5}, zoom: 5, styles: [Array]})
   *
   * @param {String} [markers.image] - url to image. image will be used as marker on ggl map (default: './img/map-marker.png')
   * @param {String} [markers.url] - url for AJAX (get markers array) (default: './ajax/googleMapMarkers.json')
   * @param {String} [markers.eventName] - event name. used in AJAX request (default: 'markersEventName')
   */
  constructor(options = {}, markers = {}, clickableElements) {
    this.container = options.map || document.getElementById('map');
    this.callbackName = options.callbackName || "initMapId1";
    this.scriptUrl = options.scriptUrl || "https://maps.googleapis.com/maps/api/js";
    this.scriptKey = options.scriptKey || "key=AIzaSyCrFjHOCFQbsMvKEGL92MTAffwFyHgogZI";
    this.scriptOtions = options.scriptOptions || `&language=ru&callback=${this.callbackName}`;
    this.optionsZoom = 5;
    this.optionsCenterCords = {
      lat: 48,
      lng: 67.5,
    };
    this.optionsStyle = [{
      "featureType": "road.highway",
      "stylers": [{"hue": "#FFC200"}, {"saturation": -61.8}, {"lightness": 45.599999999999994}, {"gamma": 1}]
    }, {
      "featureType": "road.arterial",
      "stylers": [{"hue": "#FF0300"}, {"saturation": -100}, {"lightness": 51.19999999999999}, {"gamma": 1}]
    }, {
      "featureType": "road.local",
      "stylers": [{"hue": "#FF0300"}, {"saturation": -100}, {"lightness": 52}, {"gamma": 1}]
    }, {
      "featureType": "water",
      "stylers": [{"hue": "#0078FF"}, {"saturation": -13.200000000000003}, {"lightness": 2.4000000000000057}, {"gamma": 1}]
    }, {
      "featureType": "poi",
      "stylers": [{"hue": "#00FF6A"}, {"saturation": -1.0989010989011234}, {"lightness": 11.200000000000017}, {"gamma": 1}]
    }];
    this.mapInitOptions = options.mapInitOptions || {
      center: this.optionsCenterCords,
      zoom: this.optionsZoom,
      styles: this.optionsStyle,
    };

    this.markerImage = markers.image || './img/map-marker.png';
    this.markersUrl = markers.url || './ajax/googleMapMarkers.json';
    this.markersEventName = markers.eventName || 'getGoogleMapsShopMarkers';

    this.clickableElements = clickableElements || null;

    this.markers = [];
  }

  /**
   * Describe callback which will be called on google maps scripts load.
   *
   * @return {GoogleMaps}
   */
  setGoogleMapCallback() {
    const _this = this;

    // callback on ggl map load
    window[this.callbackName] = () => {

      //create map
      const MapInstance = new google.maps.Map(this.container, this.mapInitOptions);

      // get and set markers
      const getAndSetMarkers = new Promise((resolve, reject) => {
        $.ajax({
          url: this.markersUrl,
          method: 'GET',
          dataType: 'json',
          data: {
            eventName: this.markersEventName
          },
          success: (res) => {
            resolve(res)
          },
          error: (xhr) => {
            reject(xhr)
          }
        })
      }).then((res) => {
        res.forEach((element) => {
          // marker options
          const markerOptions = {
            map: MapInstance,
            position: element.position,
            icon: this.markerImage,
            title: `Адрес: ${element.address}`
          };

          // create marker
          const marker = new google.maps.Marker(markerOptions);

          // Marker info window content template
          const templateString = `<div id="mapLinkContainer">
                                    <img id="mapLinkImage" src="${element.image}">
                                    <div id="mapLinkTextContainer">
                                      <div id="mapLinkAddress">Адрес: ${element.address}</div>
                                      <div id="mapLinkPhone">Телефон: ${element.phone}</div>
                                      <div id="mapLinkWorkTime">Режим работы: ${element.workTime}</div>
                                    </div>
                                  </div>`;

          // create info window
          marker.infoWindow = new InfoBubble({
            content: templateString,
            shadowStyle: 0,
            borderColor: "transparent",
            backgroundClassName: 'transparent',
            padding: "10px",
            backgroundColor: "transparent",
            arrowSize: 0,
            borderRadius: 0,
            minWidth: "225px",
            minHeight: "230px",
            closeSrc: "./img/map-close-button.png",
            disableAnimation: true
          });

          // Stole the ref to infowindows in array
          this.markers.push(marker);

          // function-handler for click/tap events on markers
          function markerClickHandler() {
            if (marker.infoWindow.isOpen_) {
              marker.infoWindow.close();
              return
            }

            _this.markers.forEach((element) => {
              element.infoWindow.close()
            });

            // set map's zoom
            MapInstance.setZoom(12);

            marker.infoWindow.open(MapInstance, marker);
          }

          // add click listener to the MARKER (open infowindow, close on double-click)
          marker.addListener('click', function() {
            markerClickHandler()
          });

          // add click listener to the MAPLINK (open infowindow, close on double-click)
          const thisMaplinkElements = this.clickableElements.filter(`[data-maplink-id="${element.mapLinkId}"]`);
          thisMaplinkElements.on('click tap', function() {
            markerClickHandler()
          });
        });
      }).catch((xhr) => {
        console.log(`Error is thrown. Error text:\n${xhr.text}`);
      });
    };
  }

  /**
   * Load google maps api v3 scripts and call callback.
   */
  init() {
    require.ensure([], () => {
      const InfoBubbleInstance = require('../dep/infoBubble');

      // set callback
      this.setGoogleMapCallback();
      //append google maps api v3 script
      $('body').append(`<script async src="${this.scriptUrl}?${this.scriptKey}${this.scriptOtions}" charset="UTF-8"></script>`);
    });
  }
}