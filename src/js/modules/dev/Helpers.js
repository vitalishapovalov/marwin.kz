'use strict';

/**
 * Commonly used constants and functions.
 *
 * @module Helpers
 */

/**
 * CSS class name to indicate most elements state.
 *
 * @constant
 * @type {String}
 */
export const active = 'active';

/**
 * CSS class name to indicate form's incorrect state.
 *
 * @constant
 * @type {String}
 */
export const formIsIncorrect = 'incorrect';

/**
 * CSS class name to indicate form's correct state.
 *
 * @constant
 * @type {String}
 */
export const formIsValidated = 'validated';

/**
 * Path to SVG-sprite file.
 *
 * @constant
 * @type {String}
 */
export const svgPath = './img/svg-default.svg';

/**
 * Symbol for params separation.
 *
 * @constant
 * @type {String}
 */
export const separator = '?';

/**
 * Cache body DOM element.
 *
 * @constant
 * @type {jQuery}
 */
export const body = $('body');

/**
 * Detect current page.
 *
 * @constant
 * @type {String}
 */
export const currentPage = body.find('main').data('page');

/**
 * Detect personal cabinet's current page.
 *
 * @constant
 * @type {String}
 */
export const currentPcPage = body.find('main').data('pc-page');

/**
 * Menu should be hidden on all resolutions?
 *
 * @constant
 * @type {Boolean}
 */
export const menuIsAlwaysHidden = currentPage !== 'main';

/**
 * Detect window width once.
 *
 * @constant
 * @type {Number}
 */
export const screenWidth = $(window).width();

/**
 * Detect whether user logged in or not and return true/false flag.
 *
 * @constant
 * @type {Boolean}
 */
export const userLoggedIn = body.hasClass('logged');

/**
 * Breakpoint for landscape tablets.
 *
 * @constant
 * @type {Number}
 */
export const tabletLandscapeBreakpoint = 1280;

/**
 * Breakpoint for portrait tablets.
 *
 * @constant
 * @type {Number}
 */
export const tabletPortraitBreakpoint = 600;

/**
 * Cache has own property method.
 *
 * @constant
 * @type {Function}
 */
export const has = Object.prototype.hasOwnProperty;

/**
 * Custom HTML template for owlCarousel 'Prev' button.
 *
 * @constant
 * @type {String}
 */
export const buttonLeftTemplate = `<svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="${svgPath}#icon-item-buttonL"></use></svg>`;

/**
 * Custom HTML template for owlCarousel 'Next' button.
 *
 * @constant
 * @type {String}
 */
export const buttonRightTemplate = `<svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="${svgPath}#icon-item-buttonR"></use></svg>`;

/**
 * Common options for owlCarousel sliders.
 *
 * @type {Object}
 */
export let owlCommonOptions = {
  loop: true,
  dots: false,
  navText: [buttonLeftTemplate, buttonRightTemplate],
  margin: 10,
  responsive: {
    0: {
      nav: true,
      items: 1
    },
    600: {
      nav: true,
      items: 2
    },
    1024: {
      nav: true,
      items: 4
    },
    1670: {
      nav: true,
      items: 6
    },
  },
};

/**
 * Initialize owl carousel on each element from array.
 *
 * @param {jQuery} elements - array of elements to init owlCarousel on.
 * @param {Object} [options] - options for owlCarousel.
 */
export const initOwlCarousel = (elements, options = owlCommonOptions) => {
  elements.each((index, el) => {
    $(el).owlCarousel(options)
  });
};

/**
 * Check for the DOM element is visible in the current viewport.
 *
 * @param {jQuery} el - element to check.
 * @param {Number} range - additional range to check, 200 by default.
 */
export const isElementInViewport = (el, range = 200) => {
  el = el[0];
  const elCords = el.getBoundingClientRect();

  return (
    elCords.top >= -range &&
    elCords.left >= 0 &&
    elCords.bottom <= ($(window).height() + range) &&
    elCords.right <= $(window).width()
  )
};

/**
 * Crop text in selected element, if text's length more then specified number.
 *
 * @param {jQuery} elements - Elements to crop.
 * @param {Number} symbolsToShow - Number of symbols, that should be shown.
 */
export const cropText = (elements, symbolsToShow) => {
  elements.each(function () {
    const element = $(this);
    let text = element.text();

    if (text.length > symbolsToShow) {
      let croppedText = text.substr(0, symbolsToShow) + '...';

      element.text(croppedText);
    }
  });
};

/**
 * Detect user logged in or not.
 *
 * @returns {Number} - Current window width.
 */
export const screenWidthDynamically = () => $(window).width();


/**
 * Check specified item to be target of event.
 *
 * @param {Object} e - Event object.
 * @param {jQuery} item - Item to compare with.
 * @returns {Boolean} - Indicate whether clicked target is the specified item or no.
 */
export const checkClosest = (e, item) => $(e.target).closest(item).length > 0;

/**
 * Toggles specified class name on body.
 *
 * @param {jQuery} elem - Trigger element.
 * @param {String} className - Class name to apply.
 */
export const activeBodyClassItemInit = (elem, className) => {
  elem.on('click tap', () => {
    body.toggleClass(className);
  });
};

/**
 * Toggle dropdown on click.
 *
 * @param {jQuery} activeItem - Click event handler.
 * @param {jQuery} toggleItem - Toggling object.
 */
export const toggleDropdown = (activeItem, toggleItem) => {
  activeItem.on('click tap', () => {
    toggleItem.toggleClass(active)
  });
};

/**
 * Remove active class if click outside the element.
 *
 * @param {jQuery} activeItem - Default click event handler to active item.
 * @param {jQuery} toggleItem - Toggling object.
 * @param {String} className
 */
export const hideOnBodyClick = (activeItem, toggleItem, className) => {
  let classNameToRemove = '';

  typeof className === 'string' ? classNameToRemove = className : classNameToRemove = active;

  body.on('click tap', (e) => {
    if (!checkClosest(e, toggleItem) && toggleItem.hasClass(classNameToRemove) && !checkClosest(e, activeItem)) {
      toggleItem.removeClass(classNameToRemove)
    }
  })
};

/**
 * Remove specified class name if clicked anywhere outside the specified container.
 *
 * @param {jQuery} element - Container.
 * @param {jQuery} elementToRemoveClass - Remove class from this element.
 * @param {String} className - Class name to remove.
 */
export const removeClassOnBodyClick = (element, elementToRemoveClass = element, className = 'incorrect') => {
  body.on('click tap', (e) => {
    const clickIsOutsideTheElement = !$(e.target).closest(element).length > 0;

    if (clickIsOutsideTheElement) elementToRemoveClass.removeClass(className)
  })
};

/**
 * Makes default dropdown object with event handlers.
 *
 * @param {jQuery} activeItem - Default click event handler to active item.
 * @param {jQuery} toggleItem - Toggling object.
 */
export const dropdownItemInit = (activeItem, toggleItem) => {
  toggleDropdown(activeItem, toggleItem);
  hideOnBodyClick(activeItem, toggleItem);
};

/**
 * Select item from list: make it active ( while disabling others ), change selected item text.
 *
 * @param {jQuery} item - Clicked item.
 * @param {jQuery} selected - Selected item's text keeper.
 * @param {jQuery} list - List of items.
 * @param {String} customEventName - listen for this event
 */
export const selectOneFromListInit = (item, selected, list, customEventName = '') => {
  item.on(`click tap ${customEventName}`, function () {
    let $this = $(this);

    selected.text($this.text());

    item.removeClass(active);
    $this.addClass(active);

    typeof list === 'object' ? list.removeClass(active) : null;
  });
};

/**
 * Reduce the set of checkbox elements to checked ones.
 *
 * @param {jQuery} checkboxes - list of items.
 */
export const getChecked = (checkboxes) => {
  return $.map(checkboxes, (checkbox) => {
    if ($(checkbox).prop('checked')) return checkbox
  })
};

/**
 * Get length of checked elements from list.
 *
 * @param {jQuery} checkboxes - list of items.
 */
export const getCheckedLength = (checkboxes) => {
  return getChecked(checkboxes).length
};

/**
 * Send AJAX with specified callback.
 *
 * @param {String} url
 * @param {Object|Array|String} data
 * @param {Function} callback
 * @param {Object} [context=null] - callback is called within this context
 * @param {String} [method=GET]
 */
export const sendJsonAjaxWithCallback = (url, data, callback, context = null, method = 'GET') => {
  $.ajax({
    url: url,
    method: method,
    dataType: 'json',
    cache: false,
    data: data,
    success: (res) => {
      console.log(`DATA: ${JSON.stringify(data)} \n Response: ${JSON.stringify(res)}`);
      callback.call(context, res)
    }
  })
};