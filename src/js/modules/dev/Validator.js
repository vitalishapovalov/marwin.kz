'use strict';

/**
 * @name Validator.js
 * @version 0.1
 * @author Vitali Shapovalov
 *
 * @fileoverview
 * This modules is used for forms validation.
 * Text fields, emails, phones, checkobxes etc.
 */

/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Validator.js module
 *
 * @constructor
 *
 * @param {jQuery} form - form to validate
 * @param {Function} ajaxOptions - function that should return AJAX request options
 * @param {Object} [options] - user specified options
 * @param {Boolean} [options.nestedInModal=false] - when true, remove fields incorrect state on modal hide
 * @param {String} [options.fieldsSelector='.form-input'] - form's field selector string
 */
export default class Validator {
  constructor(form = $(), ajaxOptions = {}, options = {}) {
    /** Form to validate */
    this.form = form;

    /** User-specified AJAX options */
    this.ajaxOptions = ajaxOptions;

    /** User-specified options */
    this.options = {
      modal: options.nestedInModal || false,
      fieldsSelector: options.fieldsSelector || '.form-input',
    };

    /** Default options */
    this.default = {
      // SELECTORS
      incorrectFields: '.incorrect',
      error: '.error',

      // CLASS NAMES
      incorrectClass: 'incorrect',
      formIsValidClass: 'validated',

      // ERROR MESSAGES TEXT
      fieldEmptyText: 'Заполните поле',
      incorrectPhoneText: 'Введите корректный номер',
      incorrectEmailText: 'Введите корректный Email',

      // PARAM NAME FOR SETTING CUSTOM ERROR MESSAGES
      textDataName: 'validation-text',

      // 'data-validation-*' TYPES
      dataCondition: 'validation-condition',
      dataType: 'validation-type',

      // FIELD VALIDATION TYPES
      textType: 'text',
      phoneType: 'phone',
      emailType: 'email',
      checkboxType: 'checkbox',
      radioType: 'radio',

      // 'data-validation-condition' VALUE TYPES
      minLengthDataName: 'min-length',
      lengthDataName: 'length',

      // 'data-validation' VALUE TYPES
      requiredToValidate: 'required',
    };

    /** DOM elements */
    this.elements = {
      // DYNAMIC ELEMENTS SELECTORS
      inputs: () => this.form.find('input, textarea, select'),
      fields: () => this.form.find(this.options.fieldsSelector),

      // STATIC ELEMENTS SELECTORS
      modal: this.form.parents('.modal'),
      button: this.form.find('.validate-form-button'),
    };

    /** Buffer object */
    this.buffer = {};

    /** Initialize */
    this.init();
  }

  /**
   * Check form for validness.
   *
   * @return {Boolean}
   */
  formIsValid() {
    const incorrectFields = this.form.find(this.default.incorrectFields);

    return incorrectFields.length === 0 && this.form.hasClass(this.default.formIsValidClass);
  }

  /**
   * Validates an email.
   *
   * @static
   *
   * @param {String} email
   * @return {Boolean}
   */
  static validateEmail(email) {
    let pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);

    return pattern.test(email);
  }

  /**
   * Remove incorrect state from all fields.
   *
   * @return {Validator}
   */
  removeIncorrectState() {
    this.form
      .find(this.options.fieldsSelector).removeClass(this.default.incorrectClass);

    return this;
  }

  /**
   * Reset form to default state.
   *
   * @return {Validator}
   */
  resetForm() {
    this.form[0].reset();

    return this;
  }

  /**
   * Remove incorrect state from all fields when modal is closed.
   *
   * @return {Validator}
   */
  removeIncorrectStateOnModalClose() {
    this.elements.modal.on('hidden.bs.modal', () => this.removeIncorrectState());

    return this;
  }

  /**
   * Set incorrect state on field.
   *
   * @param {jQuery} field
   * @param {String} errorText - displayed error text
   */
  throwError(field, errorText) {
    field.addClass(this.default.incorrectClass)
      .find(this.default.error).text(errorText);
  }

  /**
   * Check field for validness and set valid/incorrect state.
   *
   * @param {jQuery} field
   * @param {Number} valueLength
   * @param {String} errorText
   * @param {Boolean} condition - condition to set valid state
   */
  checkFieldValidness(field, condition, errorText, valueLength) {
    const dataText = field.data(this.default.textDataName);

    if (dataText && dataText.length) errorText = dataText;

    if (!valueLength) {
      this.throwError(field, this.default.fieldEmptyText);
    } else if (!condition) {
      this.throwError(field, errorText);
    } else {
      this.form.addClass(this.default.formIsValidClass);
    }
  }

  /**
   * Validates field.
   *
   * @param {jQuery} field
   */
  validateField(field) {
    const type = field.data(this.default.dataType);
    let fieldParams = {
      condition: true,
      errorText: this.default.fieldEmptyText,
      length: 1,
    };

    switch (type) {
      case this.default.textType: {
        fieldParams = this.validateTextField(field);
        break;
      }
      case this.default.phoneType: {
        fieldParams = this.validateTextField(field);
        fieldParams.errorText = this.default.incorrectPhoneText;
        break;
      }
      case this.default.emailType: {
        fieldParams = this.validateEmailField(field);
        fieldParams.errorText = this.default.incorrectEmailText;
        break;
      }
      case this.default.radioType: {
        fieldParams.condition = this.validateRadioField(field);
        break;
      }
    }

    this.checkFieldValidness(field, fieldParams.condition, fieldParams.errorText, fieldParams.length);
  }

  /**
   * Validate 'Text' field.
   *
   * @param {jQuery} field
   * @return {Object}
   */
  validateTextField(field) {
    const input = field.find('input').length ? field.find('input') : field.find('textarea');
    const value = input.val();
    const valueLength = value.length;

    const conditionType = input.data(this.default.dataCondition);
    let condition, errorText;

    switch (conditionType) {
      case this.default.minLengthDataName: {
        const minLength = input.data(this.default.minLengthDataName);

        condition = valueLength >= parseInt(minLength, 10);
        errorText = `Минимальная длинна поля - ${minLength} символа`;

        break;
      }
      case this.default.lengthDataName: {
        const neededLength = input.data(this.default.lengthDataName);

        condition = valueLength === parseInt(neededLength, 10);
        errorText = `Необходимая длинна поля - ${neededLength} символов`;

        break;
      }
    }

    return {
      condition,
      errorText,
      length: valueLength,
    };
  }

  /**
   * Validate 'Email' field.
   *
   * @param {jQuery} field
   */
  validateEmailField(field) {
    const value = field.find('input').val();

    return {
      condition: Validator.validateEmail(value),
      length: value.length,
    };
  }

  /**
   * Validate 'Radio' field.
   *
   * @param {jQuery} field
   */
  validateRadioField(field) {
    const checkedVisibleRadio = field.find('input[type="radio"]:checked:visible');

    return checkedVisibleRadio.length >= 1;
  }

  /**
   * Serialize form.
   *
   * @return {Array} serialized form data
   */
  serializedFormData() {
    return this.form.serialize();
  }

  /**
   * Send data if form is valid.
   *
   * @param {Object|Function} options - ajax options
   * @return {Validator}
   */
  sendIfValidated(options = this.ajaxOptions) {
    if (this.formIsValid()) {
      $.ajax(options.call(null, this));
    }

    return this;
  }

  /**
   * Validate form fields.
   *
   * @return {Validator}
   */
  validateAllFields() {
    this.elements.fields().each((index, field) => {
      const $field = $(field);
      const requiredToValidate = $field.data('validation') === this.default.requiredToValidate;

      if (requiredToValidate) this.validateField($field);
    });

    return this;
  }

  /**
   * Validate form.
   *
   * @return {Validator}
   */
  runFormValidation() {
    this.removeIncorrectState()
      .validateAllFields()
      .sendIfValidated();

    return this;
  }

  /**
   * Initialize on-click validation.
   *
   * @return {Validator}
   */
  bindOnClickValidation() {
    this.elements.button.on('click.validation tap.validation', (e) => {
      e.preventDefault();

      this.runFormValidation();
    });

    return this;
  }

  /**
   * Unbind on-click event.
   *
   * @return {Validator}
   */
  unbindOnClick() {
    this.elements.button.unbind('click.validation tap.validation');

    return this;
  }

  /**
   * Initialize all validation scripts.
   */
  init() {
    this.bindOnClickValidation();

    if (this.options.modal) this.removeIncorrectStateOnModalClose();
  }
}
