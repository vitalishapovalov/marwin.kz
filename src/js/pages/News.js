'use strict';

/**
 * News & Sales pages scripts.
 *
 * @module News
 */

/** Import utility functions */
import {
  screenWidthDynamically,
  toggleDropdown,
} from 'js/modules/dev/Helpers';

/** Class representing a website's News & Sales pages functions. Initialized only on web site's News & Sales pages. */
export default class News {
  /**
   * Cache News & Sales pages page DOM elements for instant access.
   */
  constructor() {
    this.dropdownBox = $('.menu.category-menu');
    this.dropdownTrigger = this.dropdownBox.find('.category-separator');
  }

  /**
   * Init dropDowns on mobile.
   *
   * @return {News}
   */
  initDropdownsMobile() {
    let state = 1;

    const initDropdowns = function () {
      if (screenWidthDynamically() < 768 && state === 1) {
        toggleDropdown(this.dropdownTrigger, this.dropdownBox);
        state = 2;
      }
    }.bind(this);

    initDropdowns();

    $(window).on('resize orientationchange', initDropdowns);

    return this;
  }

  /**
   * Initialize News & Sales pages scripts.
   *
   * @static
   */
  static init() {
    let obj = new this;

    obj.initDropdownsMobile();
  }
}