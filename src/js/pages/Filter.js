'use strict';

/**
 * Filter page scripts.
 *
 * @module Filter
 */

/** Import utility functions */
import {
  dropdownItemInit,
  toggleDropdown,
  checkClosest,
  body,
  active,
  screenWidthDynamically,
  selectOneFromListInit,
} from '../modules/dev/Helpers';

/** Class representing a website's Filter page functions. Initialized only on web site's filter pages. */
export default class Filter {
  /**
   * Cache filter page DOM elements for instant access.
   */
  constructor() {
    this.sortDropdowns = $('.dropdown-box.sort');
    this.filterDropdowns = $('form.filters.mobile .dropdown-box');

    this.priceSliders = $('.filters .price-slider');

    this.filterScrollContainer = $('.filter-block-items-container');

    this.psOptions = {
      suppressScrollX: true,
      swipePropagation: false,
    };

    this.mobileFilters = $('form.filters.mobile');

    this.filtersTrigger = $('.filters-trigger');

    this.applyFiltersButton = $('.filter-button');
    this.clearFiltersButton = $('.filters-clear-button');
  }

  /**
   * Sort dropdowns (sort by type, show the number of products).
   * Dropdowns, select one from list, redirect page.
   *
   * @return {Filter}
   */
  initSortFilter() {
    this.sortDropdowns.each((index, el) => {
      const element = $(el);
      const trigger = element.find('.dropdown-box-title');
      dropdownItemInit(trigger, element);

      const sortItemSelected = element.find('.dropdown-box-title > span');
      const sortItem = element.find('.dropdown-box-option');
      selectOneFromListInit(sortItem, sortItemSelected, element);

      sortItem.on('click tap', function(){})
    });

    return this
  }

  /**
   * Initialize filter dropdowns.
   *
   * @return {Filter}
   */
  initFilterDropdowns() {
    this.filterDropdowns.each((index, el) => {
      const element = $(el);
      const trigger = element.find('.dropdown-box-title');
      toggleDropdown(trigger, element);

      body.on('click tap', (e) => {
        if (screenWidthDynamically() > 600) {
          if (!checkClosest(e, element) && element.hasClass(active) && !checkClosest(e, trigger)) {
            element.removeClass(active)
          }
        }
      })
    });

    // close all selects on orientation change
    $(window).on('orientationchange', () => this.filterDropdowns.removeClass(active));

    // Price section dropdown
    const priceFilter = this.mobileFilters.find('.filter-block.input-container');
    const trigger = priceFilter.find('.filter-block-title');
    toggleDropdown(trigger, priceFilter);

    return this
  }

  /**
   * Initialize filters container toggle button.
   *
   * @return {Filter}
   */
  initFiltersToggle() {
    const trigger = this.filtersTrigger;
    const form = trigger.parents('form');

    toggleDropdown(trigger, form);

    return this
  }

  /**
   * Initialize perfect scrollbar.
   *
   * @return {Filter}
   */
  initPerfectScrollbar() {

    this.filterScrollContainer.each((index, el) => {
      const elHeight = el.scrollHeight;

      if (elHeight > 150) {
        $(el).perfectScrollbar(this.psOptions)
      }

      $(window).on('resize orientationchange', () => {
        const elHeight = el.scrollHeight;

        if (elHeight > 150) {
          $(el).perfectScrollbar(this.psOptions)
        }
      })
    });

    return this
  }

  /**
   * Generate options for ionRangeSlider.
   *
   * @param {jQuery} priceFrom - input with min price.
   * @param {jQuery} priceTo - input with max price.
   * @return {Object} object with options for IRS plugin.
   */
  irsOptions(priceFrom, priceTo) {
    return {
      type: "double",
      min: priceFrom.attr('data-from'),
      max: priceTo.attr('data-to'),
      from: priceFrom.val(),
      to: priceTo.val(),
      hide_min_max: true,
      grid: false,
      onChange: (data) => {
        const newFrom = data.from;
        const newTo = data.to;
        priceFrom.val(newFrom);
        priceTo.val(newTo);
      }
    }
  };

  /**
   * Initialize IonRangeSlider.
   *
   * @return {Filter}
   */
  initIRS() {
    const sliders = this.priceSliders;

    sliders.each((index, slider) => {
      const $slider = $(slider);
      const form = $slider.parents('form');
      const priceFrom = form.find('.price-from');
      const priceTo = form.find('.price-to');

      $slider.ionRangeSlider(this.irsOptions(priceFrom, priceTo));

      const sliderInstance = $slider.data('ionRangeSlider');
      const elementsToBind = [priceFrom, priceTo];

      elementsToBind.forEach((element) => {
        element.on('input', () => {
          sliderInstance.update({
            from: priceFrom.val(),
            to: priceTo.val(),
          })
        })
      });
    });

    return this
  }

  /**
   * Bind 'Apply filters' button.
   *
   * @return {Filter}
   */
  bindApplyButton() {
    this.applyFiltersButton.on('click tap', function() {
      const form = $(this).parents('form.filters');
      const serialized = form.serialize();

      console.log(serialized.replace(/&/gi, '/'));
      console.log('Redirect or function here')
    });

    return this
  }

  /**
   * Bind 'Clear filters' button.
   *
   * @return {Filter}
   */
  bindClearButton() {
    this.clearFiltersButton.on('click tap', function() {
      console.log('Redirect or function here')
    });

    return this
  }

  /**
   * Initialize ITS and PS plugins.
   *
   * @return {Filter}
   */
  initPlugins() {
    require.ensure([], () => {
      const ionRangeSlider = require('../modules/dep/ion.rangeslider-custom');
      const perfectScrollbar = require('perfect-scrollbar/jquery');

      // init Perfect Scrollbar and IRS
      this.initPerfectScrollbar()
        .initIRS();
    });

    return this
  }

  /**
   * Initialize Filter page scripts.
   *
   * @static
   */
  static init() {
    let obj = new this;

    obj.initPlugins()
      .initFilterDropdowns()
      .initFiltersToggle()
      .initSortFilter()
      .bindApplyButton()
      .bindClearButton();
  }
}