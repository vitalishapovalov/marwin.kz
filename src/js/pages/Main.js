'use strict';

/**
 * Main page scripts.
 *
 * @module Main
 */

/** Import utility functions */
import {
  screenWidth,
  tabletLandscapeBreakpoint,
  cropText,
  has,
  body,
  active,
  buttonLeftTemplate,
  buttonRightTemplate,
} from '../modules/dev/Helpers';

/** Import Calendar */
import Calendar from '../modules/dev/Calendar.svg';

/** Class representing a website's Main page functions. Initialized only on web site's main page. */
export default class Main {
  /**
   * Cache main page's DOM elements for instant access and generate options for owlCarousel.
   */
  constructor() {
    this.infoContainer = $('#info .info-container');
    this.videoSection = $('section#video');

    /** Common options for owlCarousel sliders */
    this.commonItemSliderOptions = {
      loop: true,
      dots: false,
      navText: [buttonLeftTemplate, buttonRightTemplate],
      margin: 10,
      responsive: {
        0: {
          items: 1
        },
        600: {
          items: 2
        },
        1024: {
          items: 4
        }
      },
    };

    /** Slider objects and their options */
    this.sliders = {
      /** Big banners carousel */
      bigBanners: {
        container: $('.banners-container'),
        options: {
          ...this.commonItemSliderOptions,
          lazyLoad: true,
          dots: true,
          responsive: {
            0: {
              items: 1
            },
            600: {
              items: 2
            },
            1024: {
              items: 3,
              autoplay: true,
              autoplayTimeout: 15000
            }
          },
        },
        condition: true,
      },
      /** 'Super price' products */
      superPrice: {
        container: $('#super-price .items-container'),
        options: {
          ...this.commonItemSliderOptions,
          responsive: {
            0: {
              nav: true,
              items: 1
            },
            600: {
              nav: true,
              items: 2
            },
            1024: {
              nav: true
            }
          },
        },
        condition: screenWidth <= tabletLandscapeBreakpoint,
      },
      /** 'Sales' section */
      sales: {
        container: $('.sales-item-container'),
        options: {
          ...this.commonItemSliderOptions,
          loop: true,
          responsive: {
            0: {
              dots: true,
              nav: false,
              items: 1
            },
            600: {
              dots: false,
              nav: true,
              items: 1
            },
            1024: {
              nav: true,
              items: 2
            }
          },
        },
        condition: screenWidth <= 1669,
      },
      /** 'New products' section */
      newItems: {
        container: $('.new-items-container'),
        options: {
          ...this.commonItemSliderOptions,
          responsive: {
            0: {
              dots: true,
              nav: false,
              items: 1
            },
            600: {
              dots: false,
              nav: true,
              items: 1
            },
            1024: {
              items: 2,
              nav: true
            }
          },
        },
        condition: screenWidth <= 1669,
      },
      /** 'Products of the day' section */
      prodOfTheDay: {
        container: $('#best-of-the-day .items-container'),
        options: {
          ...this.commonItemSliderOptions,
          responsive: {
            0: {
              nav: true,
              items: 1
            },
            600: {
              nav: true,
              items: 2
            },
            1024: {
              nav: true,
              items: 4
            },
            1670: {
              nav: true,
              items: 6
            },
          },
        },
        condition: screenWidth < 1903,
      },
      /** 'Video' section */
      video: {
        container: $('.video-items-container'),
        options: {
          ...this.commonItemSliderOptions,
          responsive: {
            0: {
              dots: true,
              items: 1
            },
            600: {
              dots: false,
              nav: true,
              video: true,
              lazyLoad: true,
              items: 1
            },
            1024: {
              video: false,
              lazyLoad: false,
              items: 2,
              nav: true,
            },
          },
        },
        condition: screenWidth <= 1669,
      }
    };
  }

  /**
   * Initialize owl carousel on selected element.
   *
   * @param {jQuery} element - Element to activate slider on.
   * @param {Object} options - Carousel options.
   * @param {Boolean} condition - Condition to init slider.
   */
  static initSlider(element, options, condition) {
    if (condition) element.owlCarousel(options)
  }

  /**
   * Init sliders for all objects specified in settings.
   *
   * @param {Object} settings - objects with their options.
   * @return {Main}
   */
  initSliders(settings) {
    for (const section in settings) {
      if (has.call(settings, section)) {
        let element = settings[section];
        Main.initSlider(element.container, element.options, element.condition)
      }
    }

    return this
  }

  /**
   * Toggle additional info block's ( on the bottom of the page ) size on click.
   *
   * @return {Main}
   */
  initTogglingInfo() {
    if (screenWidth < 1024) {
      this.infoContainer.click(() => {
        this.infoContainer.toggleClass(active)
      })
    }

    return this
  }

  /**
   * Video lazy-load.
   *
   * @return {Main}
   */
  videoLazyloadInit() {
    const checkAndLoadVideo = () => {
      const videoItems = this.videoSection.find('.video-container iframe');
      const bodyScrollTop = body.scrollTop() + $(window).height();
      const videoOffsetTop = this.videoSection.offset().top;

      if (bodyScrollTop >= videoOffsetTop) {
        videoItems.each((index, el) => {
          const $el = $(el);
          const hasSrc = $el.attr('src');

          if (!hasSrc) {
            const src = $el.data('src');

            $el.attr('src', src)
          }
        })
      }
    };
    checkAndLoadVideo();

    $(window).on('scroll resize orientationchange', checkAndLoadVideo);

    return this
  }

  /**
   * Crop specified element's length.
   *
   * @return {Main}
   */
  initCrop() {
    /** 'Sales' items title */
    if (screenWidth < 400) {
      let $elementsToCrop = $('.cropped');

      $elementsToCrop.each(function () {
        let $this = $(this);
        let symbolsToShow = $this.data('crop-number');

        cropText($this, symbolsToShow);
      });
    }

    return this;
  }

  /**
   * Load and initialize Calendar.svg module
   *
   * @return {Main}
   */
  initCalendar() {
    // create calendar instance
    const CalendarInstance = new Calendar();

    return this;
  }

  /**
   * Initialize Main page scripts.
   *
   * @static
   */
  static init() {
    let obj = new this;

    obj.videoLazyloadInit()
      .initSliders(obj.sliders)
      .initTogglingInfo()
      .initCrop()
      .initCalendar();
  }
};
