'use strict';

/**
 * Registration page scripts.
 *
 * @module Registration
 */

/** Import utility functions */
import {
  toggleDropdown,
} from '../modules/dev/Helpers';

/** Import Validator. */
import Validator from '../modules/dev/Validator';

/** Import Footer class for subscribe form validation method */
import Footer from '../components/Footer';

/** Class representing a website's Registration page functions. Initialized only on web site's registration page. */
export default class Registration {
  /**
   * Cache registration page DOM elements for instant access.
   */
  constructor() {
    this.cartContainer = $('.cart-main-container');
    this.cartTrigger = $('.cart-toggle');

    this.orderForm = $('.form.form-order');

    this.printButton = $('.print-order-button');

    this.order = $('.cart-main');

    /** Subscribe form elements */
    this.subscribeButton = $('#registration_subscribe');
    this.subscribeInput = $('#registration-subscribe-input');
    this.subscribeFormContainer = $('form.registration-subscribe-form');

    /** AJAX URLs */
    this.subscribeUrl = './ajax/subscribe-other.json';
    this.orderFormUrl = './ajax/success.json';

    /** AJAX event names */
    this.orderFormEventName = 'validateOrder';
    this.subscribeEventName = 'subscribeForm';

    /** 'Delivery method state change' components */
    this.changeStateSelect = this.orderForm.find('label.userDelivery select');
  }

  /**
   * Init cart dropdown.
   *
   * @return {Registration}
   */
  initCartDropdown() {
    toggleDropdown(this.cartTrigger, this.cartContainer);

    return this
  }

  /**
   * Print on click.
   *
   * @return {Registration}
   */
  initPrint() {
    this.printButton.on('click tap', () => window.print());

    return this
  }

  /**
   * Init order form validation & data send.
   *
   * @return {Registration}
   */
  initOrderForm() {
    const formUrl = this.orderFormUrl;
    const eventName = this.orderFormEventName;

    const validateOrderForm = new Validator(this.orderForm, (context) => {
      return {
        url: formUrl,
        method: 'POST',
        cache: false,
        dataType: 'json',
        data: {
          data: context.serializedFormData(),
          event: eventName
        },
        success: (res) => {
          console.log(`DATA: ${context.serializedFormData()}`);
          if (res.response === 'success') {
            console.log('order successful');
          } else {
            console.log('order failed')
          }
        },
      }
    });

    return this
  }

  /**
   * Init subscribe form validation & data send.
   *
   * @return {Registration}
   */
  initSubscribeForm() {
    (new Footer()).validateSubscribeEmail.call(this);

    return this
  }

  /**
   * Init form's state change on delivery method change.
   *
   * @return {Registration}
   */
  initChangeFormStateOnDeliveryChange() {
    this.changeStateSelect.on('change', () => {
      this.orderForm.attr('data-method', this.changeStateSelect.val())
    });

    return this
  }


  /**
   * Initialize Registration page scripts.
   *
   * @static
   */
  static init() {
    let obj = new this;

    obj.initCartDropdown()
      .initOrderForm()
      .initSubscribeForm()
      .initChangeFormStateOnDeliveryChange()
      .initPrint()
  }
}