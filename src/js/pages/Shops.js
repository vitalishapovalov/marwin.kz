'use strict';

/**
 * Shops page scripts.
 *
 * @module Shops
 */

/** Import utility functions */
import {
  toggleDropdown,
  dropdownItemInit,
  selectOneFromListInit,
  body,
} from 'js/modules/dev/Helpers';

/** Import google maps class */
import GoogleMaps from '../modules/dev/GoogleMaps';

/** Class representing a website's Shops page functions. Initialized only on web site's Shops pages. */
export default class Shops {
  /**
   * Cache Shops page DOM elements for instant access.
   */
  constructor() {
    /** Desktop dropdown lists */
    this.desktopDropDownContainers = $('.map-links-container .map-link');
    this.desktopDropDownMaplinks = this.desktopDropDownContainers.find('.map-link-address');

    /** Mobile select dropdowns */
    this.selectDropdowns = $('.dropdown-box-container .dropdown-box');
    this.cityDropdown = $('.dropdown-box-container .dropdown-box.city');
    this.shopDropdown = $('.dropdown-box-container .dropdown-box.shop');
    this.dropdownScrollContainer = $('.dropdown-box-options > .relative');

    /** Map links */
    this.mapLinks = $('.map-link-address');

    /** Class names */
    this.disabledSelectClass = 'disabled';

    /** Other */
    this.psOptions = {
      suppressScrollX: true,
      swipePropagation: false,
    }
  }

  /**
   * Initialize desktop dropdown lists.
   *
   * @return {Shops}
   */
  initDesktopDropdowns() {
    this.desktopDropDownContainers.each((index, container) => {
      const $container = $(container);
      const trigger = $container.find('.title');

      toggleDropdown(trigger, $container);
    });

    return this
  }

  /**
   * Sort dropdowns (city, shop).
   * Dropdowns, select one from list.
   *
   * @return {Shops}
   */
  initDropdownSelects() {
    this.selectDropdowns.each((index, el) => {
      const element = $(el);
      const trigger = element.find('.dropdown-box-title');
      dropdownItemInit(trigger, element);

      const sortItemSelected = element.find('.dropdown-box-title > span');
      const sortItem = element.find('.dropdown-box-option');
      selectOneFromListInit(sortItem, sortItemSelected, element, 'shop.select');

      sortItem.on('click tap', function(){})
    });

    return this
  }

  /**
   * Fire 'city change' event.
   *
   * @param {Number|String} cityId
   * @param {Object} anotherParams
   * @return {Shops}
   */
  fireCityChangeEvent(cityId, anotherParams = {}) {
    const shopDropdown = this.shopDropdown;
    const disabledSelectClass = this.disabledSelectClass;

    if (shopDropdown.hasClass(disabledSelectClass)) {
      shopDropdown.removeClass(disabledSelectClass)
    }

    // Fire change event
    shopDropdown.attr('data-city-id', cityId).trigger('change.city.id');
  }

  /**
   * Select and set city ID.
   *
   * @return {Shops}
   */
  initCitySelect() {
    const cityDropdownOptions = this.cityDropdown.find('.dropdown-box-option');

    cityDropdownOptions.each((index, option) => {
      const $option = $(option);
      const value = $option.data('value');

      // Fire change event
      $option.on('click tap', () => this.fireCityChangeEvent(value));
    });

    return this
  }

  /**
   * Init city select from desktop dropdown list.
   *
   * @retirn {Shops}
   */
  initCitySelectFromDesktop() {
    this.desktopDropDownMaplinks.each((index, link) => {
      const $link = $(link);
      const value = $link.parents('.map-link').data('city-id');
      const maplinkId = $link.data('maplink-id');

      const imitateSelect = function() {
        // Select city
        this.cityDropdown.find(`.dropdown-box-option[data-value="${value}"]`).trigger('shop.select');
        // Select maplink
        this.shopDropdown.find(`.dropdown-box-option[data-maplink-id="${maplinkId}"]`).trigger('shop.select');
      }.bind(this);


      // Fire change event
      $link.on('click tap', () => {
        imitateSelect();

        this.fireCityChangeEvent(value);
      });
    });

    return this
  }

  /**
   * Filter 'shops' dropdown on city select.
   *
   * @return {Shops}
   */
  initShopSortSelect() {
    this.shopDropdown.on('change.city.id', function() {
      const $this = $(this);
      const value = this.dataset.cityId;
      const allOptions = $this.find('.dropdown-box-option');
      const showThisOptions = $this.find(`.dropdown-box-option[data-city-id="${value}"]`);

      // Switch cities
      allOptions.hide();
      showThisOptions.show();

      // update PerfectScrollbar
      const psContainer = $this.find('.ps-container');
      psContainer.perfectScrollbar('update');
    });

    return this
  }

  /**
   * Init google map.
   *
   * @return {Shops}
   */
  initGoogleMap() {
    const GoogleMapsInstance = new GoogleMaps({}, {}, this.mapLinks);
    GoogleMapsInstance.init();

    return this
  }

  /**
   * Initialize perfect scrollbar.
   *
   * @return {Shops}
   */
  initPerfectScrollbar() {
    require.ensure([], () => {
      const perfectScrollbar = require('perfect-scrollbar/jquery');

      // init Perfect Scrollbar
      this.dropdownScrollContainer.each((index, el) => {
        $(el).perfectScrollbar(this.psOptions);
        $(window).on('resize orientationchange', () => {
          $(el).perfectScrollbar('update');
        });
      });
    });

    return this
  }

  /**
   * Initialize Shops page scripts.
   *
   * @static
   */
  static init() {
    let obj = new this;

    obj.initDesktopDropdowns()
      .initDropdownSelects()
      .initCitySelect()
      .initCitySelectFromDesktop()
      .initShopSortSelect()
      .initPerfectScrollbar()
      .initGoogleMap();
  }
}