'use strict';

/**
 * Catalog page scripts.
 *
 * @module Catalog
 */

/** Import utility functions */
import {
  screenWidthDynamically,
  screenWidth,
  toggleDropdown,
  hideOnBodyClick,
  initOwlCarousel,
} from '../modules/dev/Helpers';

/** Class representing a website's Catalog page functions. Initialized only on web site's catalog pages. */
export default class Catalog {
  /**
   * Cache catalog page DOM elements for instant access and generate options for owlCarousel.
   */
  constructor() {
    /** Collect all item containers to initialize owlCarousel on. */
    this.sliders = $('section .items-container');

    /** Collect all items to initialize dropdowns on. */
    this.dropdowns = $('.dropdown-box');

    this.resolutionToInitSliders = screenWidth < 1903;
  }

  /**
   * Initialize owl carousel on all item containers.
   *
   * @return {Catalog}
   */
  initSliders() {
    initOwlCarousel(this.sliders);

    return this
  }

  /**
   * Initialize dropdown items.
   *
   * @return {Catalog}
   */
  initDropdowns() {
    this.dropdowns.each((index, el) => {
      const element = $(el);
      const trigger = element.find('.dropdown-box-title');

      toggleDropdown(trigger, element);

      if (screenWidthDynamically() > 600) {
        hideOnBodyClick(trigger, element);
      }
    });

    return this
  }

  /**
   * Initialize Catalog page scripts.
   *
   * @static
   */
  static init() {
    let obj = new this;

    obj.initDropdowns();

    if (obj.resolutionToInitSliders) {
      obj.initSliders()
    }
  }
}