// jscs:disable
'use strict';

/**
 * Cart page scripts.
 *
 * @module Cart
 */

/** Import utility functions */
import {
  initOwlCarousel,
  removeClassOnBodyClick,
  screenWidth,
  getChecked,
  getCheckedLength,
  toggleDropdown,
  formIsIncorrect,
  formIsValidated,
  sendJsonAjaxWithCallback,
} from '../modules/dev/Helpers';

/** Import Validator class */
import Validator from '../modules/dev/Validator';

/** Class representing a website's Cart page functions. Initialized only on web site's cart page. */
export default class Cart {
  /**
   * Cache cart page DOM elements for instant access.
   */
  constructor() {
    /** Bonus fields elements */
    this.sendBonusButton = $('.validate-form-button.bonus');
    this.sendPromoButton = $('.validate-form-button.promo');
    this.bonusField = $('.form-input.cart-bonus');
    this.promoField = $('.form-input.cart-promo');
    this.changeValueButtons = $('.inner-validated .change-value-button');

    /** Checkboxes and their amount output */
    this.mainCheckbox = $('label.checkbox.selectAll input');
    this.itemCheckBoxes = () => $('.cart-item .checkbox input');
    this.displayContainer = $('.checkbox.selectAll span');
    this.currentPageItems = $('.cart-links a.active span');
    this.deferredItemsContainer = $('.cart-links a.deferred span');
    this.readyToBuyItemsContainer = $('.cart-links a.readyToBuy span');

    /** Items count */
    this.countIncrement = $('.cart-item .count.plus');
    this.countDecrement = $('.cart-item .count.minus');
    this.countInput = $('.cart-item .count.current');

    /** 'Presents' section */
    this.presentsDropdownTrigger = $('.cart-presents .trigger');
    this.presentsContainer = $('.cart-presents');

    /** Other */
    this.sliders = $('section .items-container.owl-carousel');
    this.cartActionButtons = $('.cart-main-actions .action');
    this.cartMainContainer = $('.cart-main-container');
    this.promoModal = $('.modal-promo.modal');

    /** Class-indicators names */
    this.cartIsEmptyClassName = 'empty';
    this.validatedConfirm = 'validated-confirm';
    this.validatedError = 'validated-error';
    this.buttonIsDisabled = 'disabled';

    /** AJAX URLs */
    this.changeItemQuantityUrl = './ajax/cart-changeCount.json';
    this.cartItemsActionsUrl = './ajax/cart-action.json';
    this.bonusUrl = './ajax/bonus.json';
    this.promoUrl = './ajax/promo.json';
    this.changeBonusOrPromoUrl = './ajax/success.json';

    /** AJAX event names */
    this.bonusEventName = 'setBonus';
    this.promoEventName = 'setPromo';
  }

  /**
   * Initialize present block dropdown.
   *
   * @return {Cart}
   */
  initPresentsDropdown() {
    toggleDropdown(this.presentsDropdownTrigger, this.presentsContainer);

    return this
  }

  /**
   * Validate and send Bonus/Promo information.
   *
   * @return {Cart}
   */
  initBonusAndPromo() {

    const ValidatorInstance = new Validator();

    const validateField = function(field, url, eventType, callback) {
      const input = field.find('input');
      const value = input.val();
      const data = {
        eventType: eventType,
        value: value
      };

      if (!value.length) {
        ValidatorInstance.throwError(field, 'Заполните поле')
      } else {
        field.removeClass(formIsIncorrect);

        sendJsonAjaxWithCallback(url, data, callback, this)
      }
    }.bind(this);

    /** Bonus field */
    this.sendBonusButton.on('click tap', (e) => {
      e.preventDefault();

      function onAjaxSuccess(res) {
        const response = res.response;
        const value = res.value;

        if (response == 'success') {
          const valueField = this.bonusField.find('.message .value');

          valueField.text(value);
          this.bonusField.addClass(formIsValidated)
        } else {
          ValidatorInstance.throwError(this.bonusField, res.text)
        }
      }

      validateField(this.bonusField, this.bonusUrl, this.bonusEventName, onAjaxSuccess)
    });

    /** Promo field */
    this.sendPromoButton.on('click tap', (e) => {
      e.preventDefault();

      function onAjaxSuccess(res) {
        const resMessage = res.message;
        const modalIsActive = res.modal;
        const resClass = res.class;

        const valueField = this.promoField.find('.message span');

        valueField.text(resMessage);
        this.promoField.addClass(`${formIsValidated} ${resClass}`);

        if (modalIsActive) {
          const message = res.modalMessage;
          const messageBox = this.promoModal.find('.message');

          messageBox.text(message);
          this.promoModal.modal('show')
        }
      }

      validateField(this.promoField, this.promoUrl, this.promoEventName, onAjaxSuccess)
    });

    // remove incorrect style if clicked anywhere outside the container
    removeClassOnBodyClick(this.bonusField);
    removeClassOnBodyClick(this.promoField);

    return this
  }

  /**
   * Change Bonus/Promo information.
   *
   * @return {Cart}
   */
  initBonusAndPromoChange() {

    const changeValueAjax = function(eventType, field) {
      const data = {
        event: eventType
      };

      function callback(res) {
        if (res.response == 'success') {
          field.removeClass(`${formIsValidated} ${this.validatedConfirm} ${this.validatedError}`)
        }
      }

      sendJsonAjaxWithCallback(this.changeBonusOrPromoUrl, data, callback, this)
    }.bind(this);

    this.changeValueButtons.on('click tap', function(){
      const $this = $(this);
      const eventType = $this.data('event-name');
      const field = $this.parents('.form-input');

      changeValueAjax(eventType, field)
    });

    return this
  }

  /**
   * Display number of currently selected products.
   *
   * @param {Number} number - number of selected products.
   * @return {Cart}
   */
  displaySelectedProducts(number) {
    const display = this.displayContainer;
    const dividedNumber = number % 10;

    function set(string) {
      display.text(string)
    }

    if (!number) {
      set('Товар')
    } else if ( number === 1) {
      set(`Выбран ${number} товар`)
    } else if (dividedNumber === 2 || dividedNumber === 3 || dividedNumber === 4) {
      set(`Выбрано ${number} товара`)
    } else {
      set(`Выбрано ${number} товаров`)
    }

    return this
  }

  /**
   * Change displayed cart total sum.
   *
   * @param {Number} price
   * @param {Number} discount
   * @param {Number} toPay
   * @return {Cart}
   */
  static changeDisplayedCartInfo(price, discount, toPay) {
    const totalPrice = $('.cart-price-total .price-field.total .value span:first-of-type');
    const totalDiscount = $('.cart-price-total .discount .value span:first-of-type');
    const totalPriceToPay = $('.cart-price-total .price-field.to-pay .value span:first-of-type');

    totalPrice.text(price);
    totalDiscount.text(discount);
    totalPriceToPay.text(toPay);
  }

  /**
   * Change products count.
   *
   * @return {Cart}
   */
  initProductCountChangeListener() {
    const plus = this.countIncrement;
    const minus = this.countDecrement;
    const input = this.countInput;
    const url = this.changeItemQuantityUrl;
    const disabled = this.buttonIsDisabled;

    function generateDOM(context) {
      const item = $(context).parents('.cart-item');
      const input = item.find('.count.current');
      const totalPrice = item.find('.total-price');
      const totalDiscount = item.find('.total-discount');
      const minusButton = item.find('.count.minus');

      return {
        item: item,
        input: input,
        minusButton: minusButton,
        totalPrice: totalPrice,
        totalDiscount: totalDiscount
      }
    }

    function changeItemsCount(newValue, el) {
      $.ajax({
        url: url,
        method: 'GET',
        cache: false,
        dataType: 'json',
        data: {
          itemId: el.item.data('id'),
          quantity: newValue
        },
        success: (res) => {
          console.log(`DATA\nID: ${el.item.data('id')}\nNEW VALUE: ${newValue}`);

          // change cart's 'total price' data
          const basket = res.basket;
          Cart.changeDisplayedCartInfo(basket.totalPrice_txt, basket.totalDiscount_txt, basket.totalPriceToPay_txt);

          // change item's 'total price' data
          const item = res.item;
          el.item.attr('data-quantity', item.quantity);
          el.input.val(item.quantity);
          el.totalPrice.text(item.totalPrice_txt);
          el.totalDiscount.text(item.totalDiscount_txt);

          // block/unblock 'minus' button
          if (item.quantity == 1) {
            el.minusButton.addClass(disabled)
          } else {
            el.minusButton.removeClass(disabled)
          }
        }
      })
    }

    function changeOnClick(context, inc) {
      if ($(context).hasClass(disabled)) {
        return false
      }

      const el = generateDOM(context);

      let currentQuantity = el.item.attr('data-quantity');
      let newQuantity;

      if (inc == 'plus') {
        newQuantity = ++currentQuantity
      } else if (inc == 'minus') {
        newQuantity = --currentQuantity;
      }

      el.input.val(newQuantity);

      changeItemsCount(newQuantity, el)
    }

    plus.on('click tap', function(){
      changeOnClick(this, 'plus')
    });

    minus.on('click tap', function(){
      changeOnClick(this, 'minus')
    });

    input.on('input', function(){
      const el = generateDOM(this);

      let newValue = $(this).val();

      changeItemsCount(newValue, el)
    });

    return this
  }

  /**
   * Initialize actions with cart items (delay, delete, restore).
   *
   * @return {Cart}
   */
  initCartActions() {
    const _this = this;

    this.cartActionButtons.on('click tap', function(){
      const eventName = $(this).data('eventName');
      const selectedItems = getChecked(_this.itemCheckBoxes());
      const selectedItemsLength = selectedItems.length;

      // no items were selected or the cart is empty
      if (!selectedItems.length || _this.cartMainContainer.hasClass('empty')) {
        return
      }

      // data for ajax
      const data = {
        eventName: eventName,
        items: []
      };

      // push selected items to the data container
      selectedItems.forEach((input) => {
        const item = $(input).parents('.cart-item');
        const id = item.data('id');
        const quantity = item.data('quantity');

        data.items.push({id: id, quantity: quantity})
      });

      $.ajax({
        url: _this.cartItemsActionsUrl,
        method: 'POST',
        dataType: 'json',
        cache: false,
        data: data,
        success: (res) => {
          console.log(`DATA: ${JSON.stringify(data)}\nRESPONSE: ${JSON.stringify(res)}`);

          if (res.response === 'success') {
            console.log(`${eventName} success`);

            // remove items
            selectedItems.forEach((el) => $(el).parents('.cart-item').remove());

            // count remaining items
            const itemCheckboxes = _this.itemCheckBoxes();
            _this.displaySelectedProducts(getCheckedLength(itemCheckboxes));

            // change total price cart data
            const basket = res.basket;
            const newTotalPrice = basket.totalPrice_txt;
            const newTotalDiscount = basket.totalDiscount_txt;
            const newTotalPriceToPay = basket.totalPriceToPay_txt;
            Cart.changeDisplayedCartInfo(newTotalPrice, newTotalDiscount, newTotalPriceToPay);

            // if no items left
            if (!itemCheckboxes.length) {
              _this.cartMainContainer.addClass(_this.cartIsEmptyClassName)
            }

            // display items count
            const readyToBuy = _this.readyToBuyItemsContainer;
            const deferred = _this.deferredItemsContainer;
            const current = _this.currentPageItems;

            current.text(itemCheckboxes.length);

            switch (eventName) {
              case 'cart-add-deferred': {
                deferred.text(parseInt(deferred.text(), 10) + selectedItemsLength);
                break
              }
              case 'cart-restore': {
                readyToBuy.text(parseInt(readyToBuy.text(), 10) + selectedItemsLength);
                break
              }
            }
          } else {
            console.log(`${eventName} failed`)
          }
        }
      })
    });

    return this
  }

  /**
   * Init checkboxes. (select all, unselect all etc.)
   *
   * @return {Cart}
   */
  initCheckboxes() {
    const mainCheckbox = this.mainCheckbox;

    function set(state, ...items) {
      items.forEach((item) => {
        $(item).prop('checked', state)
      });
    }

    mainCheckbox.on('change', () => {
      const itemCheckboxes = this.itemCheckBoxes();

      if (getCheckedLength(itemCheckboxes) < itemCheckboxes.length) {
        set(true, itemCheckboxes)
      } else {
        set(false, itemCheckboxes)
      }

      this.displaySelectedProducts(getCheckedLength(itemCheckboxes))
    });

    this.itemCheckBoxes().on('change', () => {
      const itemCheckboxes = this.itemCheckBoxes();

      if (getCheckedLength(itemCheckboxes) < itemCheckboxes.length) {
        set(false, mainCheckbox)
      } else {
        set(true, mainCheckbox)
      }

      this.displaySelectedProducts(getCheckedLength(itemCheckboxes))
    });

    return this
  }

  /**
   * Initialize owl carousel on all item containers.
   *
   * @return {Cart}
   */
  initSliders() {
    initOwlCarousel(this.sliders);

    return this
  }

  /**
   * Initialize Cart page scripts.
   *
   * @static
   */
  static init() {
    let obj = new this;

    obj.initPresentsDropdown()
      .initBonusAndPromo()
      .initBonusAndPromoChange()
      .initCartActions()
      .initProductCountChangeListener()
      .initCheckboxes();

    if (screenWidth < 1903) {
      obj.initSliders()
    }
  }
}