'use strict';

/**
 * Calendar page scripts.
 *
 * @module Calendar
 */

/** Import utility functions */
import {
  screenWidthDynamically,
  initOwlCarousel,
  buttonLeftTemplate,
  buttonRightTemplate,
} from 'js/modules/dev/Helpers';

/** Class representing a website's Calendar page functions. Initialized only on web site's Calendar page. */
export default class Calendar {
  /**
   * Cache Calendar page page DOM elements for instant access.
   */
  constructor() {
    this.slider = $('.calendar-events.mobile');
    this.sliderOptions = {
      loop: true,
      nav: false,
      dots: true,
      navText: [buttonLeftTemplate, buttonRightTemplate],
      margin: 10,
      responsive: {
        0: {
          items: 1
        },
        600: {
          items: 2
        },
      }
    };
  }

  /**
   * Init slider on mobile.
   *
   * @return {Calendar}
   */
  initSliderMobile() {
    let state = 1;
    const initSliders = function() {
      if (screenWidthDynamically() < 768 && state === 1) {
        initOwlCarousel(this.slider, this.sliderOptions);
        state = 2
      }
    }.bind(this);
    initSliders();
    $(window).on('resize orientationchange', initSliders);

    return this
  }

  /**
   * Initialize Calendar page scripts.
   *
   * @static
   */
  static init() {
    let obj = new this;

    obj.initSliderMobile();
  }
}