'use strict';

/**
 * Product page scripts.
 *
 * @module Product
 */

/** Import utility functions */
import {
  formIsIncorrect,
  activeBodyClassItemInit,
  initOwlCarousel,
  removeClassOnBodyClick,
  userLoggedIn,
  screenWidth,
  body,
  active,
  owlCommonOptions,
  screenWidthDynamically,
} from '../modules/dev/Helpers';

/** Import validator for review form validation. */
import Validator from '../modules/dev/Validator';

/** Import cart class. */
import {CartItem} from '../modules/dev/CartController';

/** Class representing a website's Product page functions. Initialized only on web site's product pages. */
export default class Product {
  /**
   * Cache product page DOM elements for instant access.
   */
  constructor() {
    /** 'Product preview' elements */
    this.previewPhotosContainer = $('.product-preview-items');
    this.previewPhotos = $('.product-preview-item');
    this.previewMainPhoto = $('.product-preview-mainPicture');

    /** 'Gallery' elements */
    this.galleryMainPhoto = $('.gallery-main-preview');
    this.galleryPreviewPhotos = $('.gallery-photo');
    this.galleryModal = $('.modal-gallery');

    /** Slider elements */
    this.mobileSlider = $('.product-preview-carousel');
    this.sliders = $('section .items-container.owl-carousel');
    this.resolutionToInitSliders = screenWidth < 1903;

    /** Buy / Buy immediately / Remove buttons */
    this.buyButton = $('.button.buy-now');
    this.addToCartButton = '.button.add-to-cart';
    this.removeFromCartButton = '.button.remove-from-cart';

    /** 'Comments' elements */
    this.moreCommentsButton = $('.reviews-toggle');
    this.commentsContainer = $('.reviews-container');
    this.totalReviewsCount = $('.totalReviewsCount');
    this.reviewsContainer = $('.product-preview-reviews');

    /** 'Create new review' elements */
    this.newReviewFormToggle = $('.toggle-button.logged');
    this.newReviewForm = $('.review-form');
    this.newReviewFormClose = $('.review-form-close');

    /** Forms */
    this.priceNotifyForm = $('.price-notify-form');
    this.availableNotifyForm = $('.available-notify-form');
    this.fastOrderForm = $('.form-fastOrder');

    /** AJAX URLs */
    this.likesUrl = './ajax/vote.json';
    this.getMoreCommentsUrl = './ajax/comments.json';
    this.priceNotifyUrl = './ajax/success.json';
    this.notifyAvailableUrl = './ajax/success.json';
    this.favouriteUrl = './ajax/success.json';
    this.fastOrderFormUrl = './ajax/success.json';
    this.newReviewUrl = './ajax/addComment.json';

    /** AJAX event names */
    this.loadMoteCommentsEventName = 'getMoreComments';
    this.priceNotifyEventName = 'notifyPrice';
    this.notifyAvailableEventName = 'notifyAvailable';
    this.addToFavouriteEventName = 'addToFavourite';
    this.removeFromFavouriteEventName = 'removeFromFavourite';
    this.fastOrderEventName = 'fastOrder';
    this.newReviewEventName = 'addReview';

    /** Class-indicators names */
    this.allCharacteristicsActive = 'all-characteristics-opened';
    this.isInFavourite = 'in-favourite';
    this.loading = 'loading';

    /** Other */
    this.votesSelectorString = '.review-item .votes .vote';
    this.favourite = $('#favourite');
    this.productDataContainer = '.product-preview-tabloid';
    this.productId = $(this.productDataContainer).data('id');
    this.moreInfotrigger = $('.product-preview-moreInfo-trigger');
    this.perfectScrollBarOptions = {
      swipePropagation: false,
    };
  }

  /**
   * Init owl carousel (mobile devices only).
   *
   * @return {Product}
   */
  initMobileSlider() {
    const slider = this.mobileSlider;
    const options = {};
    let owlInitialized = false;

    $.extend(options, owlCommonOptions);
    delete options.responsive;

    options.nav = true;
    options.loop = true;
    options.items = 1;
    options.lazyLoad = true;
    options.autoHeight = true;

    const checkAndInitOwl = () => {
      if (screenWidthDynamically() < 600) {
        if (!owlInitialized) {
          slider.owlCarousel(options);

          owlInitialized = true
        }
      }
    };
    checkAndInitOwl();

    $(window).on('resize orientationchange', checkAndInitOwl);

    return this
  }

  /**
   * Generate comment template with data.
   *
   * @param {Object} review - object with data to insert.
   * @return {String} - comment template.
   */
  generateCommentTemplate(review) {
    return `<div class="review-item" data-review-id="${review.id}">
                            <div class="review-user-data">
                                <span class="nickname">${review.userName}</span>
                                <span class="stars inline-bottom">
                                    <span class="stars-cover" style="width: ${review.rating * 10}%"></span>
                                </span>
                                <span class="date">${review.date}</span>
                            </div>
                            <div class="review-user-message">
                                <span class="text">${review.text}</span>
                                <div class="votes">
                                    <span class="likes vote" data-event-type="like">
                                        <svg>g
                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="./img/svg-default.svg#icon-like"></use>
                                        </svg>
                                        <span class="counter">${review.likes}</span>
                                    </span>
                                    <span class="dislikes vote" data-event-type="dislike">
                                        <svg>
                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="./img/svg-default.svg#icon-dislike"></use>
                                        </svg>
                                        <span class="counter">${review.dislikes}</span></span></div>
                            </div>
                        </div>`
  }

  /**
   * 'Load more comments' button.
   *
   * @return {Product}
   */
  initLoadMoreCommentsButton() {
    const loadMoreComments = () => {
      const data = {
        event: this.loadMoteCommentsEventName,
        id: this.productId
      };

      $.ajax({
        url: this.getMoreCommentsUrl,
        method: 'GET',
        dataType: 'json',
        data: data,
        success: (res) => {
          let template = '';

          res.forEach((review) => {
            template += this.generateCommentTemplate(review)
          });

          this.commentsContainer.append(template);

          this.moreCommentsButton.remove()
        },
        beforeSend: () => this.reviewsContainer.addClass(this.loading),
        complete: () => this.reviewsContainer.removeClass(this.loading)
      })
    };

    this.moreCommentsButton.on('click tap', loadMoreComments);

    return this
  }

  /**
   * Like/dislike buttons.
   *
   * @return {Product}
   */
  initCommentLikes() {
    const votesSelector = this.votesSelectorString;
    const vote = (id, eventType, callback) => {
      const data = {
        id: id,
        eventType: eventType
      };

      $.ajax({
        url: this.likesUrl,
        method: 'POST',
        dataType: 'json',
        data: data,
        success: (res) => {
          if (res.response == 'success') {
            callback.call(null, res.likes, res.dislikes)
          }
        }
      })
    };

    body.on('click tap', votesSelector, function(){
      const $this = $(this);
      const comment = $this.parents('.review-item');
      const id = comment.attr('id');
      const eventType = $this.data('event-type');
      const likesCounter = comment.find('.likes .counter');
      const dislikesCounter = comment.find('.dislikes .counter');

      const onSuccess = (likes, dislikes) => {
        likesCounter.text(likes);
        dislikesCounter.text(dislikes);
      };

      vote(id, eventType, onSuccess)
    });

    return this
  }

  /**
   * Toggle review form button and close review init.
   *
   * @return {Product}
   */
  initReviewFormToggleAndClose() {

    const form = this.newReviewForm;
    const toggleElements = [this.newReviewFormToggle, this.newReviewFormClose];

    const toggleForm = () => {
      form.toggle();

      this.newReviewFormToggle.toggle()
    };

    toggleElements.forEach((element) => {
      element.on('click tap', toggleForm)
    });

    return this
  }

  /**
   * Bind 'Send review' button (form validation, add new review).
   *
   * @return {Product}
   */
  initSendReviewButton() {
    const generateCommentTemplate = this.generateCommentTemplate;
    const commentsContainer = this.commentsContainer;
    const totalReviewsCount = this.totalReviewsCount;
    const elementsToRemove = [this.newReviewFormToggle, this.newReviewForm];

    const url = this.newReviewUrl;
    const eventName = this.newReviewEventName;
    const onSuccessCallback = (review) => {
      const template = generateCommentTemplate(review);

      elementsToRemove.forEach((element) => {
        element.remove()
      });

      commentsContainer.prepend(template);

      totalReviewsCount.each((index, element) => {
        const $element = $(element);

        $element.text(+$element.text() + 1);
      });
    };

    const validateReview = new Validator(this.newReviewForm, (context) => {
      const data = {
        event: eventName,
        rating: context.form.find('.new-review-rating input:checked').attr('id'),
        message: context.form.find('.message textarea').val()
      };

      return {
        url,
        method: 'POST',
        cache: false,
        dataType: 'json',
        data,
        success: (res) => {
          console.log(`DATA: ${JSON.stringify(data)}`);
          if (res.response === 'success') {
            console.log('review successful');
            onSuccessCallback(res)
          } else {
            console.log('review failed')
          }
        },
      }
    });

    return this
  }

  /**
   * Bind 'Fast order' send button.
   *
   * @return {Product}
   */
  initFastOrderSendButton() {
    const url = this.fastOrderFormUrl;
    const eventName = this.fastOrderEventName;
    const options = {
      nestedInModal: true
    };

    const validateFastOrder = new Validator(this.fastOrderForm, (context) => {
      const content = context.form.parents('.modal-content');

      return {
        url: url,
        method: 'POST',
        cache: false,
        dataType: 'json',
        data: {
          data: context.serializedFormData(),
          event: eventName
        },
        success: (res) => {
          console.log(`DATA: ${context.serializedFormData()}`);
          if (res.response === 'success') {
            console.log('fastOrder successful');

            content.addClass(context.default.formIsValidClass);

            context.elements.modal.one('hidden.bs.modal', () => {
              content.removeClass(context.default.formIsValidClass);
              context.resetForm();
            })
          } else {
            console.log('fastOrder failed')
          }
        },
      }
    }, options);

    return this
  }

  /**
   * Init 'Show all characteristics' button.
   *
   * @return {Product}
   */
  initAllCharacteristicsToggle() {
    activeBodyClassItemInit(this.moreInfotrigger, this.allCharacteristicsActive);

    return this
  }

  /**
   * Change main preview picture on hover.
   *
   * @return {Product}
   */
  initChangeMainPreviewPhotoOnHover() {
    const mainPreviewPhoto = this.previewMainPhoto;

    this.previewPhotos.on('mouseover', function(){
      const $this = $(this);
      const href = $this.attr('data-href');

      mainPreviewPhoto.attr('data-href', href)
        .find('img').attr('src', href)
    });

    return this
  }

  /**
   * Bind 'Add to cart' button.
   *
   * @return {Product}
   */
  bindBuyButton() {
    const cartController = new CartItem(this.addToCartButton, this.removeFromCartButton, this.productDataContainer);

    cartController.bind();

    return this
  }

  /**
   * Product pictures gallery scripts.
   *
   * @return {Product}
   */
  initGallery() {
    const galleryMainPhoto = this.galleryMainPhoto;
    const galleryPreviewPhotos = this.galleryPreviewPhotos;

    function setActivePreviewItem(item) {
      item = $(item);

      galleryPreviewPhotos.removeClass(active);

      item.addClass(active)
    }

    function setGalleryMainPreviewPhoto(src) {
      galleryMainPhoto.find('img').attr('src', src)
    }

    function getDataAndSet(item) {
      const $item = $(item);
      const src = $item.attr('data-href');

      setGalleryMainPreviewPhoto(src)
    }

    // open gallery with selected photo
    this.galleryModal.on('show.bs.modal', function(e) {
      const target = $(e.relatedTarget);
      const gallery = $(this);
      const items = gallery.find(galleryPreviewPhotos);
      const filter = '[data-href="'+ target.attr('data-href') +'"]';
      const activeItem = items.filter(filter);

      setActivePreviewItem(activeItem);

      getDataAndSet(target[0])
    });

    // click on the gallery preview photo
    this.galleryPreviewPhotos.on('click tap', function() {
      setActivePreviewItem(this);

      getDataAndSet(this)
    });

    return this
  }

  /**
   * Init perfect scrollBar.
   *
   * @return {Product}
   */
  initPerfectScrollBar() {
    require.ensure([], () => {
      const perfectScrollbar = require('perfect-scrollbar/jquery');

      let scrollbarEnabled = false;
      const container = this.previewPhotosContainer;

      const disableScrollbar = () => {
        container.perfectScrollbar('destroy');
        scrollbarEnabled = false
      };
      const enableScrollbar = () => {
        container.perfectScrollbar(this.perfectScrollBarOptions);
        scrollbarEnabled = true
      };
      const updateScrollbar = () => container.perfectScrollbar('update');

      const checkAndUpdatePS = () => {
        if (screenWidthDynamically() < 600) {
          if (scrollbarEnabled) {
            disableScrollbar()
          }
        } else {
          if (scrollbarEnabled) {
            updateScrollbar()
          } else {
            enableScrollbar()
          }
        }
      };
      checkAndUpdatePS();

      $(window).on('resize orientationchange', checkAndUpdatePS);
    });

    return this
  }

  /**
   * Initialize owl carousel on all item containers.
   *
   * @return {Product}
   */
  initSliders() {
    if (this.resolutionToInitSliders) initOwlCarousel(this.sliders);

    return this
  }

  /**
   * Validate price-notify form.
   *
   * @return {Product}
   */
  initValidatePriceNotify() {
    const url = this.priceNotifyUrl;
    const eventName = this.priceNotifyEventName;
    const productId = this.productId;

    const validatePriceNotify = new Validator(this.priceNotifyForm, (context) => {
      const data = {
        id: productId,
        event: eventName,
        email: context.elements.inputs().val()
      };

      return {
        url,
        method: 'GET',
        cache: false,
        dataType: 'json',
        data: data,
        success: (res) => {
          console.log(`DATA: ${JSON.stringify(data)}`);
          if (res.response === 'success') {
            context.elements.fields().remove();

            context.form.append(
              `<span class="success">
                    <b>Спасибо!</b> <br />
                    Мы отправим письмо на Ваш E-mail, 
                    если цена на этот товар станет ниже.
                </span>`
            );
          } else {
            console.log('price notify failed')
          }
        },
      }
    });

    // remove incorrect style if clicked anywhere outside the container
    removeClassOnBodyClick(validatePriceNotify.form, validatePriceNotify.elements.fields());

    return this
  }

  /**
   * Validate price-notify form.
   *
   * @return {Product}
   */
  initValidateAvailableNotify() {
    const url = this.notifyAvailableUrl;
    const eventName = this.notifyAvailableEventName;
    const productId = this.productId;

    const validateAvailableNotify = new Validator(this.availableNotifyForm, (context) => {
      const data = {
        id: productId,
        event: eventName,
        email: context.elements.inputs().val()
      };

      return {
        url,
        method: 'GET',
        cache: false,
        dataType: 'json',
        data,
        success: (res) => {
          console.log(`DATA: ${JSON.stringify(data)}`);
          if (res.response === 'success') {
            context.elements.fields().remove();
            context.elements.button.remove();

            context.form.append(
              `<span class="success">
                    <b>Спасибо!</b> <br />
                    Мы отправим письмо на Ваш E-mail, 
                    когда данный товар появится в наличии.
                </span>`
            );
          } else {
            console.log('available notify failed')
          }
        },
      }
    });

    // remove incorrect style if clicked anywhere outside the container
    removeClassOnBodyClick(validateAvailableNotify.form, validateAvailableNotify.elements.fields());

    return this
  }

  /**
   * Add/remove item from favourites.
   *
   * @return {Product}
   */
  initFavourite() {
    const generateDataAndSend = function(eventName, callback) {
      const data = {
        event: eventName,
        id: this.productId
      };

      $.ajax({
        url: this.favouriteUrl,
        method: 'POST',
        cache: false,
        dataType: 'json',
        data: data,
        success: (res) => {
          console.log(`DATA:\n${JSON.stringify(data)}\nRESPONSE:\n${JSON.stringify(res)}`);
          if (res.response == 'success') {
            callback.call(null)
          }
        }
      })
    }.bind(this);

    const container = this.favourite;
    const button = container.find('a');

    button.on('click tap', (e) => {
      e.preventDefault();

      let cb, eventName;

      if (button.hasClass(this.isInFavourite)) {
        cb = () => button.text('добавить в избранное').removeClass(this.isInFavourite);
        eventName = this.removeFromFavouriteEventName

      } else {
        cb = () => button.text('убрать из избранного').addClass(this.isInFavourite);
        eventName = this.addToFavouriteEventName
      }

      generateDataAndSend(eventName, cb)
    });

    return this
  }

  /**
   * Initialize Product page scripts.
   *
   * @static
   */
  static init() {
    let obj = new this;

    obj.initPerfectScrollBar()
      .initMobileSlider()
      .initFavourite()
      .initFastOrderSendButton()
      .initAllCharacteristicsToggle()
      .initChangeMainPreviewPhotoOnHover()
      .initGallery()
      .initValidatePriceNotify()
      .initValidateAvailableNotify()
      .initSliders()
      .bindBuyButton()
      .initLoadMoreCommentsButton();

    if (userLoggedIn) {
      obj.initReviewFormToggleAndClose()
        .initSendReviewButton()
        .initCommentLikes()
    }
  }
}