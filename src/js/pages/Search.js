'use strict';

/**
 * 'Search results' page scripts.
 *
 * @module Search
 */

/** Import utility functions */
import {
  screenWidthDynamically,
  toggleDropdown,
} from 'js/modules/dev/Helpers';

/** Import Validator. */
import Validator from '../modules/dev/Validator';

/** Class representing a website's 'Search results' page functions. Initialized only on web site's 'Search results' pages. */
export default class Search {
  /**
   * Cache 'Search results' page DOM elements for instant access.
   */
  constructor() {

    this.filterCheckboxes = $('label.filter-block-item input');
    this.filterCheckboxesForm = $('form.filters');
    this.filterCheckboxesFormTrigger = this.filterCheckboxesForm.find('.filters-mobile-title');

    this.requestForm = $('form.product-request-form');

    /** AJAX URLs */
    this.requestFormUrl = './ajax/search-form.json';

    /** AJAX event names */
    this.requestFormEventName = 'productRequest';
  }

  /**
   * Register filter checkboxes on 'change(check)' event.
   *
   * @return {Search}
   */
  initFiltersAction() {
    const _this = this;

    this.filterCheckboxes.on('change', function () {
      const thisCheckboxValue = $(this).serialize();
      const allCheckboxesValue = _this.filterCheckboxesForm.serialize().replace(/&/g, '/');

      console.log('your function here');
      console.log('Checked checkbox data:\n' + thisCheckboxValue);
      console.log('All checkboxes data:\n' + allCheckboxesValue);
    });

    return this
  }

  /**
   * Validate and send request form's data.
   *
   * @return {Search}
   */
  initRequestFormValidation() {
    const form = this.requestForm;
    const formUrl = this.requestFormUrl;
    const eventName = this.requestFormEventName;

    const validateRequestForm = new Validator(this.requestForm, (context) => {
      return {
        url: formUrl,
        method: 'POST',
        cache: false,
        dataType: 'json',
        data: {
          data: context.serializedFormData(),
          event: eventName
        },
        success: (res) => {
          console.log(`DATA: ${context.serializedFormData()}`);
          if (res.response === 'success') {
            form.html(
              `<div class="form-title">${res.message_title}</div>
               <div class="form-text">${res.message_text}</div>`
            );
            console.log('product request successful');
          } else {
            console.log('product request failed')
          }
        },
      }
    });

    return this
  }

  /**
   * Init dropDowns on mobile.
   *
   * @return {Search}
   */
  initFilterDropdownsMobile() {
    let state = 1;

    const initDropdowns = function () {
      if (screenWidthDynamically() < 768 && state === 1) {
        toggleDropdown(this.filterCheckboxesFormTrigger, this.filterCheckboxesForm);
        state = 2;
      }
    }.bind(this);

    initDropdowns();

    $(window).on('resize orientationchange', initDropdowns);

    return this;
  }

  /**
   * Initialize 'Search results' page scripts.
   *
   * @static
   */
  static init() {
    let obj = new this;

    obj.initFiltersAction()
      .initFilterDropdownsMobile()
      .initRequestFormValidation();
  }
}