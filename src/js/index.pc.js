'use strict';

/**
 * User's Personal cabinet scripts entry point.
 *
 * @module PersonalCabinet
 */

/** Import libs */
import 'js/modules/dep/infoBubble';
import 'perfect-scrollbar/jquery';
import 'js/modules/dep/datepicker';

/** Import components */
import GoogleMaps from 'js/modules/dev/GoogleMaps';
import Validator from 'js/modules/dev/Validator';
import ProductItem from 'js/components/ProductItem.js';
import Shops from 'js/pages/Shops';

/** Import utility functions  */
import {
  currentPcPage,
  has,
} from 'js/modules/dev/Helpers';

/** Common libs (initialized bu default) */
// Put mask on phone number inputs
import '../../node_modules/jquery.maskedinput/src/jquery.maskedinput.js';
$('input[type="tel"]').mask("+7 (999) 999-99-99");

/**
 * Detect current page and run appropriate scripts.
 *
 * @param {String} currentPage - current page detector.
 */
switch (currentPcPage) {
  /**
   * 'Shops' page scripts.
   *
   * @data-pc-page 'custom-map'
   */
  case 'custom-map': {
    // create special class
    class PcShops extends Shops {
      /**
       * Init google map.
       *
       * @override
       * @return {PcShops}
       */
      initGoogleMap() {
        const GoogleMapsInstance = new GoogleMaps({}, {url: './ajax/googleMapMarkers.json'}, this.mapLinks);
        GoogleMapsInstance.init();

        return this
      }
    }
    // create it's new instance
    const PcShopsInstance = new PcShops();
    // run needed methods
    PcShopsInstance
      .initDropdownSelects().initCitySelect().initShopSortSelect()
      .initPerfectScrollbar()
      .initGoogleMap();

    break
  }

  /**
   * 'Favourite' page scripts.
   *
   * @data-pc-page 'custom-fav'
   */
  case 'custom-fav': {
    const ProductItemInstance = new ProductItem();
    ProductItemInstance.cropTitle().initBuyButton();

    break
  }

  /**
   * Pages with forms.
   *
   * @data-pc-page 'active'
   */
  case 'action': {

    /** DATEPICKER PLUGIN */

    // FIELD TO ACTIVATE DATEPICKER ON
    const birthdayInputField = $('.change-personal-data-form .form-input.userBirthday input');

    // ACTIVATE DATEPICKER (IF FIELD EXISTS)
    if (birthdayInputField.length) {
      birthdayInputField.prop('readonly', true).datepicker({
        maxDate: new Date(),
        autoClose: true
      });
    }

    /** FORM VALIDATION */

    // COLLECT ALL EXISTING FORMS IN ONE OBJ
    const forms = {
      emailForm: {
        $form: $('.change-email-form'),
        url: './ajax/success.json',
        event: 'changeUserEmail',
      },
      phoneForm: {
        $form: $('.change-phone-form'),
        url: './ajax/error-secondField.json',
        event: 'changeUserPhone',
      },
      passwordForm: {
        $form: $('.change-password-form'),
        url: './ajax/error-firstField.json',
        event: 'changeUserPassword',
      },
      bindCardForm: {
        $form: $('.bind-card-form'),
        url: './ajax/success.json',
        event: 'bindUserCard',
      },
      activatePromoForm: {
        $form: $('.activate-promo-form'),
        url: './ajax/success.json',
        event: 'activatePromo',
      },
      mailingListForm: {
        $form: $('.change-mailingList-form'),
        url: './ajax/success.json',
        event: 'changeMailingList',
      },
      changeDeliveryForm: {
        $form: $('.change-delivery-form'),
        url: './ajax/success.json',
        event: 'changeDelivery',
      },
      changePersonalDataForm: {
        $form: $('.change-personal-data-form'),
        url: './ajax/success.json',
        event: 'changePersonalData',
      },
    };

    // VALIDATION COMMON FUNC
    const validateField = (form, url, eventName) => {
      const validateFieldInit = new Validator(form, (context) => {
        const data = {
          event: eventName,
          data: context.serializedFormData()
        };
        return {
          url: url,
          method: 'GET',
          dataType: 'json',
          cache: false,
          data,
          success: (res) => {
            alert(`DATA:\n${JSON.stringify(data)}\nRESPONSE:\n${JSON.stringify(res)}`);
            if (res.response === 'success') {
              location.reload()
            } else if (res.response === 'error') {
              const fieldWithErrorNumber = res.field;
              const fieldWithError = context.elements.fields().eq(fieldWithErrorNumber);

              context.throwError(fieldWithError, res.message)
            }
          }
        }
      });
    };

    // RUN VALIDATION (IF FORM EXISTS)
    for (const form in forms) {
      if (has.call(forms, form) && form.length) {
        const $form = forms[form].$form;
        const url = forms[form].url;
        const eventName = forms[form].event;

        validateField($form, url, eventName)
      }
    }
  }
}