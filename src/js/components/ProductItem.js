'use strict';

/**
 * Add/remove from cart, title crop, lazy-load.
 *
 * @module Product item
 */

/** Import utility functions */
import {
  body,
  cropText,
  isElementInViewport,
} from 'js/modules/dev/Helpers';

/** Import CartItem class. */
import {CartItem} from '../modules/dev/CartController';

/** Class representing a website's product actions. Initialized by default. */
export default class ProductItem {
  /**
   * @constructor
   */
  constructor() {
    // for dynamically added elements
    this.items = () => $('.default-item');

    this.title = $('.default-item-title');

    this.buyButton = '.default-item-buy';
    this.removeButton = '.default-item-purchased';

    this.maxTitleLength = 62;

    /** Class-indicators names */
    this.itemIsLoadedClass = 'loaded';
  }

  /**
   * Crop title up to maximal allowed title length.
   *
   * @return {ProductItem}
   **/
  cropTitle() {
    cropText(this.title, this.maxTitleLength);

    return this
  }

  /**
   * Product item image lazy-load.
   *
   * @return {ProductItem}
   */
  imageLazyloadInit() {
    const checkAndLoad = () => {
      this.items().not(`.${this.itemIsLoadedClass}`).each((index, el) => {
        const $el = $(el);

        if (isElementInViewport($el, 350)) {
          const img = $el.find('.default-item-preview img');
          const src = img.attr('data-src');

          $el.addClass(this.itemIsLoadedClass);

          img.attr('src', src);
        }
      })
    };
    checkAndLoad();

    $(window).on('load scroll resize orientationchange', checkAndLoad);

    $('.owl-carousel').on('change.owl.carousel', (e) => {
      setTimeout(() => {
        const element = $(e.currentTarget).find('.owl-item.active').find('.default-item');
        element.each((index, el) => {
          const $el = $(el);
          const img = $el.find('.default-item-preview img');
          const src = img.attr('data-src');

          $el.addClass(this.itemIsLoadedClass);

          img.attr('src', src);
        });
      }, 300);
    });

    return this
  }

  /**
   * Initialize 'Add to cart' and 'Remove from cart' buttons.
   *
   * @return {ProductItem}
   */
  initBuyButton() {
    const CartItemInstance = new CartItem(this.buyButton, this.removeButton);

    CartItemInstance.bind();

    return this
  }

  /**
   * Initialize Product item scripts.
   *
   * @static
   */
  static init() {
    let obj = new this;

    obj.initBuyButton()
      .cropTitle()
      .imageLazyloadInit();
  }
}