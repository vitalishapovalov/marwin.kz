'use strict';

/**
 * Header's elements scripts.
 *
 * @module Header
 */

/** Import easy-autocomplete plugin */
import 'easy-autocomplete';

/** Import utility functions */
import {
  screenWidth,
  screenWidthDynamically,
  body,
  activeBodyClassItemInit,
  dropdownItemInit,
  selectOneFromListInit,
  userLoggedIn,
  tabletLandscapeBreakpoint,
} from '../modules/dev/Helpers';

/** Import Validator class */
import Validator from '../modules/dev/Validator';

/** Class representing a website's header functions. Initialized by default. */
export default class Header {

  /**
   * Cache header's DOM elements for instant access.
   */
  constructor() {
    /** Search */
    this.search_input = $('.header-search__search-field input');
    this.search_trigger = $('.header-search-trigger');
    this.search_button = $('.header-search__search-button');

    /** Header links */
    this.headerLinks = $('.header-links__links');
    this.headerLinks_toggle = $('.header-links-toggle');

    /** Categories */
    this.allCategories_toggle = $('.header-search__categories');
    this.allCategories = $('.all-categories-container');
    this.category = $('.all-categories-container .category');
    this.currentCategory = $('.header-search__categories > span');

    /** City */
    this.city_choice = $('#city_choice');
    this.city_list = $('.header-links__city-list');
    this.city_item = $('.header-links__city-item');
    this.city_selected = $('.header-links__city-selected');

    /** Modal's components */
    this.modalRegisterTrigger = $('#call_register_modal');
    this.modalRegister = $('.modal.modal-register');
    this.modalLogin = $('.modal.modal-login');

    /** Forms */
    this.loginForm = $('.modal .form-login');
    this.registrationForm = $('.modal .form-register');

    /** AJAX URLs */
    this.searchUrl = './ajax/search.json';
    this.cityUrl = './ajax/city.json';
    this.loginUrl = './ajax/success.json';
    this.registrationUrl = './ajax/success.json';

    /** AJAX event names */
    this.searchWithKeypressEventName = 'searchWithKeypress';
    this.searchWithEnterOrButtonEventName = 'searchWithEnterOrClick';
    this.loginEventName = 'userLogin';
    this.registrationEventName = 'userRegister';

    /** Personal cabinet dropdown */
    this.loggedUserDropdown = $('.header-user-dropdown');
    this.loggedUserDropdown_toggle = $('.header-user-toggle');
  }

  /**
   * Returns strings representing element's state.
   *
   * @return {String} class name representing element's state.
   */
  get states() {
    return {
      activeSearchInput: 'input-active',
      searchBarVisible: 'search-active',
      smallHeader: 'header-small',
    }
  }

  /**
   * Toggle header size on devices.
   *
   * @return {Header}
   */
  headerSizeToggleInit() {
    const smallHeader = this.states.smallHeader;

    $(window).on('scroll', () => {
      if (screenWidthDynamically() < 600) {
        let scrollTop = $(window).scrollTop();

        if (scrollTop > 10) {
          body.addClass(smallHeader)
        } else {
          body.removeClass(smallHeader)
        }
      }
    });

    return this
  }

  /**
   * Initialize dropDown components.
   *
   * @return {Header}
   */
  dropdownListsInit() {
    // cities
    dropdownItemInit(this.city_choice, this.city_list);
    // categories
    dropdownItemInit(this.allCategories_toggle, this.allCategories);
    // header links ( only on mobile devices )
    if (screenWidth <= tabletLandscapeBreakpoint) {
      dropdownItemInit(this.headerLinks_toggle, this.headerLinks)
    }

    return this
  }

  /**
   * Initialize one item from list selection elements.
   *
   * @return {Header}
   */
  selectItemFromListInit() {
    // city from cities
    selectOneFromListInit(this.city_item, this.city_selected, this.city_list);
    // category from categories
    selectOneFromListInit(this.category, this.currentCategory);

    return this
  }

  /**
   * Get current city and perform AJAX request for every city change.
   *
   * @return {Header}
   */
  dynamicCityChangeInit() {
    const url = this.cityUrl;
    const cityItem = this.city_item;

    cityItem.on('click tap', function() {
      const cityId = $(this).attr('data-city-id');
      const data = JSON.stringify({
        event: 'cityChange',
        id: cityId
      });

      $.ajax({
        url: url,
        method: 'POST',
        dataType: 'json',
        data: data,
        success: (res) => {
          console.log(`Data: ${data}\nresponse: ${res}`)
        }
      })
    });

    return this
  }

  /**
   * Perform AJAX request for searched phrase.
   *
   * @param {String} url - string with url.
   */
  ajaxSearch(url) {
    const categories = this.allCategories;
    const searchInput = this.search_input;
    const activeEventName = this.searchWithEnterOrButtonEventName;
    const category = categories.find('.active').attr('data-category-id');
    const phrase = searchInput.val();

    // prevent empty string search
    if (!phrase) return;

    const data = JSON.stringify({
      phrase: {
        event: activeEventName,
        text: phrase,
        category: category
      }
    });

    $.ajax({
      url: url,
      method: 'POST',
      cache: false,
      dataType: 'json',
      data: data,
      success: function(res) {
        console.log(`Data: ${data}\nresponse: ${res}`)
      }
    })
  }

  /**
   * Bind 'Enter' key and 'Search' button to perform search ajax request.
   *
   * @return {Header}
   */
  bindSearchKeys() {
    const url = this.searchUrl;

    this.search_button.on('click tap', () => this.ajaxSearch(url));

    this.search_input.on('keypress', (e) => {
      if (e.which == 13) {
        e.preventDefault();
        
        this.ajaxSearch(url)
      }
    });

    return this
  }

  /**
   * Generate options for easy-autocomplete plug-in.
   *
   * @static
   * @return {Object} options for easy-autocomplete.
   */
  static get autofillOptions() {
    const obj = new this;
    const categories = obj.allCategories;
    const url = obj.searchUrl;
    const keypressEventName = this.searchWithKeypressEventName;

    return {
      getValue: "text",

      url: url,

      ajaxSettings: {
        dataType: "json",
        method: "POST"
      },

      preparePostData: function(data, phrase) {
        const category = categories.find('.active').attr('data-category-id');

        data = JSON.stringify({
          phrase: {
            event: keypressEventName,
            text: phrase,
            category: category
          }
        });

        return data
      },

      template: {
        type: "links",
        fields: {
          link: "website-link"
        }
      },

      list: {
        showAnimation: {
          type: "fade",
          time: 150
        },
        hideAnimation: {
          type: "fade",
          time: 150
        },
        maxNumberOfElements: 6,
        match: {
          // @TODO: set 'enabled' to 'false' on production
          enabled: true
        }
      }
    }
  }

  /**
   * Initialize easy-autocomplete plug-in on search bar's input.
   *
   * @return {Header}
   */
  autofillInit() {
    this.search_input.easyAutocomplete(Header.autofillOptions);

    return this
  }

  /**
   * Initialize search bar's active state on focus.
   *
   * @return {Header}
   */
  searchActiveStateOnFocusInit() {
    const searchActiveState = this.states.activeSearchInput;

    this.search_input.focus(() => {
      body.addClass(searchActiveState)
    }).blur(() => {
      body.removeClass(searchActiveState)
    });

    return this
  }

  /**
   * Initialize search bar's show/hide toggle on click.
   *
   * @return {Header}
   */
  toggleSearchBarOnClickInit() {
    activeBodyClassItemInit(this.search_trigger, this.states.searchBarVisible);

    return this
  }

  /**
   * Initialize all of search bar scripts.
   *
   * @return {Header}
   */
  searchBarInit() {
    this.autofillInit()
      .searchActiveStateOnFocusInit()
      .toggleSearchBarOnClickInit()
      .bindSearchKeys();

    return this
  }

  /**
   * Issue with opening register modal from login modal fix.
   *
   * @return {Header}
   */
  registerModalFromLoginModal() {
    const _this = this;

    _this.modalRegisterTrigger.on('click tap', (e) => {
      e.preventDefault();

      _this.modalLogin
        .modal('hide')
        .on('hidden.bs.modal', function () {
          _this.modalRegister.modal('show');

          $(this).off('hidden.bs.modal');
        });

    });

    return this
  }

  /**
   * Initialize personal cabinet menu dropdown.
   *
   * @return {Header}
   */
  userCabinetMenuInit() {
    const container = this.loggedUserDropdown;
    const trigger = this.loggedUserDropdown_toggle;

    dropdownItemInit(trigger, container);

    return this
  }

  /**
   * Unbind login and register modal triggers.
   *
   * @return {Header}
   */
  disableLoginModal() {
    $('[data-modal-action="login"]').removeAttr('data-target');
    $('[data-modal-action="register"]').removeAttr('data-target');

    return this
  }

  /**
   * Initialize scripts for logged-in users.
   *
   * @return {Header}
   */
  loggedUserActionsInit() {
    this.disableLoginModal()
      .userCabinetMenuInit();

    return this
  }

  /**
   * Initialize Login form validation and data send.
   *
   * @return {Header}
   */
  initLoginValidation() {
    const url = this.loginUrl;
    const eventName = this.loginEventName;
    const options = {
      nestedInModal: true
    };

    const validateLogin = new Validator(this.loginForm, (context) => {
      const data = {
        event: eventName,
        data: context.serializedFormData()
      };

      return {
        url,
        method: 'GET',
        cache: false,
        dataType: 'json',
        data,
        success: (res) => {
          console.log('Your callback here');
          console.log(`DATA: ${JSON.stringify(data)}`);
          if (res.response === 'success') {
            console.log('login successful');
            context.unbindOnClick()
          } else {
            console.log('login failed')
          }
        },
      }
    }, options);

    return this
  }

  /**
   * Initialize Login form validation and data send.
   *
   * @return {Header}
   */
  initRegistrationValidation() {
    const url = this.registrationUrl;
    const eventName = this.registrationEventName;
    const options = {
      nestedInModal: true
    };

    const validateRegistration = new Validator(this.registrationForm, (context) => {
      const data = {
        event: eventName,
        data: context.serializedFormData()
      };

      return {
        url,
        method: 'GET',
        cache: false,
        dataType: 'json',
        data,
        success: (res) => {
          console.log('Your callback here');
          console.log(`DATA: ${JSON.stringify(data)}`);
          if (res.response === 'success') {
            console.log('registration successful');
            context.unbindOnClick()
          } else {
            console.log('registration failed')
          }
        },
      }
    }, options);

    return this
  }

  /**
   * Initialize scripts for not logged-in users.
   *
   * @return {Header}
   */
  notLoggedUserActionsInit() {
    this.initLoginValidation()
      .initRegistrationValidation();

    return this
  }


  /**
   * Initialize Header scripts.
   *
   * @static
   */
  static init() {
    const obj = new this;

    obj.dynamicCityChangeInit()
      .headerSizeToggleInit()
      .dropdownListsInit()
      .selectItemFromListInit()
      .registerModalFromLoginModal()
      .searchBarInit();

    if (userLoggedIn) {
      obj.loggedUserActionsInit()
    } else {
      obj.notLoggedUserActionsInit()
    }
  }
}