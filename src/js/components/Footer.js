'use strict';

/**
 * Footer's elements scripts.
 *
 * @module Footer
 */

/** Import utility functions */
import {
  formIsIncorrect,
  body,
  screenWidth,
  tabletLandscapeBreakpoint,
  active,
  buttonLeftTemplate,
  buttonRightTemplate,
  removeClassOnBodyClick,
} from '../modules/dev/Helpers';

/** Import Validator class */
import Validator from '../modules/dev/Validator';

/** Class representing a website's footer functions. Initialized by default. */
export default class Footer {

  /**
   * Cache footer's DOM elements for instant access and generate options for owlCarousel.
   */
  constructor() {
    /** AJAX URLs */
    this.subscribeUrl = './ajax/subscribe-footer.json';
    this.issueRequestUrl = './ajax/success.json';

    /** AJAX event names */
    this.subscribeEventName = 'subscribeFooter';
    this.issueRequestEventName = 'issueRequest';

    /** Other */
    this.footerInfo = $('.footer-info');
    this.footer_toggling_item = $('.footer-links > h5:not(.app-links-header)');
    this.footer_up_button = $('.footer-up-button');
    this.issueRequestForm = $('.modal .form-issue');

    /** Subscribe */
    this.subscribeButton = $('#footer_subscribe');
    this.subscribeInput = $('#footer_subscribe_input');
    this.subscribeFormContainer = $('.footer-subscribe__form');

    /** Slider */
    this.sliderOptions = {
      margin: 0,
      navText: [buttonLeftTemplate, buttonRightTemplate],
      responsive: {
        0: {
          loop: true,
          nav: true,
          items: 1
        },
        600: {
          loop: false,
          nav: true,
          items: 2
        },
        1024: {
          items: 3,
          nav: true
        }
      },
    };
  }

  /**
   * Initialize footer's links dropDown trigger (on mobile devices only).
   *
   * @return {Footer}
   */
  dropdownLinksInit() {
    this.footer_toggling_item.on('click tap', function () {
      let $this = $(this);
      let container = $this.parent();

      container.toggleClass(active);
    });

    return this
  }

  /**
   * Initialize 'Up' button.
   *
   * @return {Footer}
   */
  upButtonInit() {
    this.footer_up_button.on('click tap', () => {
      $('body, html').animate({scrollTop: 0}, 800)
    });

    return this
  }

  /**
   * Initialize owlCarousel.
   *
   * @return {Footer}
   */
  sliderInit() {
    this.footerInfo.owlCarousel(this.sliderOptions);
    return this
  }

  /**
   * Initialize subscribers email validation and data send.
   *
   * @return {Footer}
   */
  validateSubscribeEmail() {
    const url = this.subscribeUrl;
    const eventName = this.subscribeEventName;

    const validateSubscribe = new Validator(this.subscribeFormContainer, (context) => {
      const data = {
        event: eventName,
        email: context.elements.inputs().val()
      };

      return {
        url,
        method: 'GET',
        cache: false,
        dataType: 'json',
        data,
        success: (res) => {
          console.log(`DATA: ${JSON.stringify(data)}`);
          if (res.response === 'success') {
            const textToRemove = $('.hide-on-subscribe-success');

            textToRemove.animate( { opacity: 0 }, 500, function(){
              $(this).remove()
            });

            context.form.animate( { opacity: 0 }, 500, function(){
              $(this)
                .html(res.message)
                .animate({opacity: 1}, 500)
            });

          } else {
            console.log('subscribe failed')
          }
        },
      }
    });

    // remove incorrect style if clicked anywhere outside the container
    removeClassOnBodyClick(validateSubscribe.form, validateSubscribe.elements.fields());

    return this
  }

  /**
   * Validate and send data from Issue request from.
   *
   * @return {Footer}
   */
  validateIssueForm() {
    const url = this.issueRequestUrl;
    const eventName = this.issueRequestEventName;
    const options = {
      nestedInModal: true
    };

    // Custom class for issue form validation
    class ValidateIssue extends Validator {
      unbindOnClick() {
        super.unbindOnClick();

        const button = this.elements.button;
        const modal = this.elements.modal;

        // set action button to close modal
        button.on('click tap', (e) => {
          e.preventDefault();

          modal.modal('hide')
        });

        // return modal to default state when closed
        modal.on('hidden.bs.modal', () => {
          const title = modal.find('.modal-title');
          const subtitle = modal.find('.modal-subtitle');

          this.resetForm();

          title.text(this.buffer.titleText);
          subtitle.text(this.buffer.subtitleText);
          button.text(this.buffer.buttonText);

          modal.removeClass(this.default.formIsValidClass);

          button.unbind('click tap');

          this.bindOnClickValidation()
        });

        return this
      }
    }

    const validateIssue = new ValidateIssue(this.issueRequestForm, (context) => {
      const data = {
        data: context.serializedFormData(),
        event: eventName
      };

      return {
        url,
        method: 'POST',
        cache: false,
        dataType: 'json',
        data,
        success: (res) => {
          console.log(`DATA: ${JSON.stringify(data)}`);
          if (res.response === 'success') {
            const modal = context.elements.modal;
            const button = context.elements.button;

            const title = modal.find('.modal-title');
            const subtitle = modal.find('.modal-subtitle');

            // save data in buffer obj
            context.buffer = {
              titleText: title.text(),
              subtitleText: subtitle.text(),
              buttonText: button.text()
            };

            // change text
            title.text('Ваше сообщено отправлено');
            subtitle.text('Вскоре с Вами свяжется наш сотрудник. Спасибо, что помогаете нам стать лучше.');
            button.text('Закрыть');

            // change state
            modal.addClass(context.default.formIsValidClass);

            // unbind action button
            context.unbindOnClick()
          } else {
            console.log('issue request failed')
          }
        },
      }
    }, options);

    return this
  }

  /**
   * Initialize Footer scripts.
   *
   * @static
   */
  static init() {
    const obj = new this;

    obj.upButtonInit()
      .validateSubscribeEmail()
      .validateIssueForm();

    // only on mobile devices
    if (screenWidth <= tabletLandscapeBreakpoint) {
      obj.dropdownLinksInit()
        .sliderInit()
    }
  }
}