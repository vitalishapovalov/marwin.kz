'use strict';

/**
 * Breadcrumbs, issue fixing, input mask, get current products in cart.
 *
 * @module Common
 */

/** Import utility functions */
import {
  body,
  dropdownItemInit,
} from '../modules/dev/Helpers';

/** Import Cart controller. */
import Cart from '../modules/dev/CartController';

/** Import masked input plugin. */
import '../../../node_modules/jquery.maskedinput/src/jquery.maskedinput.js';

/** Class representing common functions. Initialized by default. */
export default class Common {

  /**
   * Cache DOM elements.
   */
  constructor() {
    this.breadcrumbItems = $('.breadcrumb-item');

    this.phoneInputs = $('input[type="tel"]');

    /** Class-indicators names */
    this.iphoneFixingClassName = 'iphone4fix';
  }


  /**
   * Breadcrumbs dropdown.
   *
   * @return {Common}
   */
  breadcrumbsDropdownsInit() {
    this.breadcrumbItems.each((index, element) => {
      const breadcrumb = $(element);
      const breadcrumbToggle = breadcrumb.find('.breadcrumb-title');

      dropdownItemInit(breadcrumbToggle, breadcrumb)
    });

    return this
  }

  /**
   * Mask phone inputs.
   *
   * @return {Common}
   */
  initPhoneMasks() {
    this.phoneInputs.mask("+7 (999) 999-99-99");

    return this
  }

  /**
   * iPhone 4 scroll issue (main menu) fix.
   *
   * @return {Common}
   */
  initIphone4IssueFix() {
    const checkHeightAndFix = function() {
      const isIphone4 = window.screen.height == 480;

      if (isIphone4) body.addClass(this.iphoneFixingClassName)
    }.bind(this);

    $(window).on('resize orientationchange load', checkHeightAndFix);

    return this
  }

  /**
   * Cart update on each page reload.
   *
   * @return {Common}
   */
  initCart() {
    const CartInstance = new Cart();
    CartInstance.getCurrentProducts();

    return this
  }

  /**
   * Initialize common scripts.
   *
   * @static
   */
  static init() {
    const obj = new this;

    obj.initCart()
      .initPhoneMasks()
      .breadcrumbsDropdownsInit()
      .initIphone4IssueFix()
  }
}