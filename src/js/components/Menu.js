'use strict';

/**
 * Menu scripts.
 *
 * @module Menu
 */

/** Import utility functions */
import {
  menuIsAlwaysHidden,
  screenWidthDynamically,
  body,
  screenWidth,
  tabletLandscapeBreakpoint,
  active,
  checkClosest,
} from '../modules/dev/Helpers';

/** Class representing a website's main menu functions. Initialized by default. */
export default class Menu {

  /**
   * Cache main menu's DOM elements for instant access.
   */
  constructor() {
    this.menu = $('#main-menu');
    this.menu_toggle = $('#header__menu-toggle');
    this.menu_close = $('.category-thirdLevel-title > svg');
    this.menu_bg = $('.menu-backface');
    this.menu_back_cat = $('.category-secondLevel-back');
    this.menu_firstLevel = $('#main-menu .category');
    this.menu_secondLevel = $('.category-secondLevel');
    this.menu_thirdLevelCont = $('.category-thirdLevel-title');
    this.thirdLevelTrigger = $('.category-secondLevel > span');
  }

  /**
   * Get the state.
   *
   * @static
   * @return {Object} Contains class names representing element's state.
   */
  static get states() {
    return {
      menuOpened: 'menu-opened',
      subCatOpened: 'menu-2',
      subSubCatOpened: 'menu-3',
    }
  }

  /**
   * Get main menu's sub menu object.
   *
   * @static
   * @return {Object} Contains jQuery objects - sub menu containers.
   */
  static get subMenu() {
    const obj = new this;

    return {
      first: obj.menu_firstLevel,
      second: obj.menu_secondLevel,
    }
  }

  /**
   * Relocate to second category on click init. (desktop)
   *
   * @return {Menu}
   */
  secondLevelHrefInit() {
    // polyfill
    if (!location.origin)
      location.origin = location.protocol + "//" + location.host;

    this.menu_secondLevel.on('click tap', function(){
      if (screenWidthDynamically() > 1280) {
        const link = $(this).attr('data-href');
        location.href = location.origin + '/' + link
      }
    });

    return this
  }

  /**
   * Close menu and all of sub menus.
   *
   * @return {Menu}
   */
  closeMenu() {
    body.removeClass(Menu.states.menuOpened);
    Menu.closeSubMenu.all();

    return this
  }

  /**
   * Hide menu when clicked outside main menu.
   *
   * @return {Menu}
   */
  closeMenuOnBodyClick() {
    body.on('click tap', (e) => {
      if (!checkClosest(e, this.menu) && !checkClosest(e, this.menu_toggle)) {
        this.closeMenu()
      }
    });

    return this
  }

  /**
   * Close menu when clicked on black overlay.
   *
   * @return {Menu}
   */
  blackOverlayClickHandler() {
    this.menu_bg.on('click tap', () => {
      this.closeMenu()
    });

    return this
  }

  /**
   * Initialize open/close menu trigger.
   *
   * @return {Menu}
   */
  toggleMenuOnClick() {
    const menuOpened = Menu.states.menuOpened;
    const menuTrigger = this.menu_toggle;

    // current state
    let bound = false;

    function bindMenu() {
      if ( !bound ) {
        menuTrigger.on('click.trig tap.trig', () => {
          if (body.hasClass(menuOpened)) {
            Menu.closeSubMenu.all()
          }
          body.toggleClass(menuOpened)
        });

        bound = true
      }
    }

    function unbindMenu() {
      if ( bound ) {
        menuTrigger.off('click.trig tap.trig');

        bound = false
      }
    }

    if (menuIsAlwaysHidden) {
      bindMenu()
    } else {
      let checkAndChangeHandler = function() {
        let widthToActivate = screenWidthDynamically() < tabletLandscapeBreakpoint;

        if (widthToActivate) {
          bindMenu()
        } else {
          unbindMenu()
        }
      };
      checkAndChangeHandler();

      $(window).on('resize orientationchange', checkAndChangeHandler);
    }

    return this
  }

  /**
   * Initialize 'Return' and 'Close' buttons in sub menus.
   *
   * @return {Menu}
   */
  bindBackButtons() {
    this.menu_back_cat.on('click tap', () => {
      Menu.closeSubMenu.first()
    });

    this.menu_close.on('click tap', () => {
      Menu.closeSubMenu.second()
    });

    return this
  }

  /**
   * Open sub menu.
   *
   * @static
   * @return {Object} open specified sub menu;
   */
  static get openSubMenu() {
    return {
      first: (subCategory) => {
        subCategory.addClass(active);
        body.addClass(Menu.states.subCatOpened)
      },
      second: (subSubCategory) => {
        subSubCategory.addClass(active);
        body.addClass(Menu.states.subSubCatOpened)
      },
    }
  }

  /**
   * Close sub menu.
   *
   * @static
   * @return {Object} close specified sub menu;
   */
  static get closeSubMenu() {
    const subCatOpened = this.states.subCatOpened;
    const subSubCatOpened = this.states.subSubCatOpened;
    const subMenu = this.subMenu.first;
    const subSubMenu = this.subMenu.second;

    return {
      first: () => {
        body.removeClass(subCatOpened);
        subMenu.removeClass(active)
      },
      second: () => {
        body.removeClass(subSubCatOpened);
        subSubMenu.removeClass(active)
      },
      all: () => {
        body.removeClass(subCatOpened + ' ' + subSubCatOpened);
        subMenu.removeClass(active);
        subSubMenu.removeClass(active)
      },
    }
  }

  /**
   * Open sub menu when clicked on sub menu trigger.
   *
   * @return {Menu}
   */
  openSubCatMenuInit() {
    const _this = this;
    const subCatMenuTrigger = _this.menu_firstLevel;

    subCatMenuTrigger.on('click tap', function (e) {
      const $this = $(this);
      const notBackToCat = !checkClosest(e, _this.menu_back_cat);
      const notSubSubCat = !checkClosest(e, _this.menu_secondLevel);

      if (notSubSubCat && notBackToCat) {
        if ($this.hasClass(active)) {
          Menu.closeSubMenu.all()
        } else {
          Menu.closeSubMenu.all();
          Menu.openSubMenu.first($this)
        }
      }
    });

    return this
  }

  /**
   * Open sub sub menu when clicked on sub sub menu trigger.
   *
   * @return {Menu}
   */
  openSubSubCatMenuInit() {
    const _this = this;
    const subSubCatMenuTrigger = _this.menu_secondLevel;

    subSubCatMenuTrigger.on('click tap', function (e) {
      const $this = $(this);
      const notSubSubCat = !checkClosest(e, _this.menu_thirdLevelCont);

      if (notSubSubCat) {
        if ($this.hasClass(active)) {
          Menu.closeSubMenu.second()
        } else {
          Menu.closeSubMenu.second();
          Menu.openSubMenu.second($this)
        }
      }
    });

    return this
  }

  /**
   * Show sub sub menu when hovered on sub sub menu trigger ( > 1280px screens ).
   *
   * @return {Menu}
   */
  subSubMenuOnHoverInit() {
    this.thirdLevelTrigger.on('mouseover', function () {
      const $this = $(this);
      const thisSecondLevelMenu = $this.parent();

      Menu.closeSubMenu.second();
      Menu.openSubMenu.second(thisSecondLevelMenu)
    });

    return this
  }


  /**
   * Initialize Menu scripts.
   *
   * @static
   */
  static init() {
    /** Make overflow: visible when menu opened. */
    if (menuIsAlwaysHidden) body.addClass('visible');

    const obj = new this;

    obj.openSubCatMenuInit()
      .toggleMenuOnClick()
      .secondLevelHrefInit();

    if (screenWidth > tabletLandscapeBreakpoint) {
      obj.subSubMenuOnHoverInit()
    }

    if (screenWidth <= tabletLandscapeBreakpoint || menuIsAlwaysHidden) {
      obj.openSubSubCatMenuInit()
        .bindBackButtons()
        .blackOverlayClickHandler()
    }

    if (screenWidth > 600) {
      obj.closeMenuOnBodyClick()
    }
  }
}
