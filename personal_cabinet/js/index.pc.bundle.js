/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "./";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($) {'use strict';
	
	/**
	 * User's Personal cabinet scripts entry point.
	 *
	 * @module PersonalCabinet
	 */
	
	/** Import libs */
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	__webpack_require__(2);
	
	__webpack_require__(3);
	
	__webpack_require__(25);
	
	var _GoogleMaps = __webpack_require__(26);
	
	var _GoogleMaps2 = _interopRequireDefault(_GoogleMaps);
	
	var _Validator = __webpack_require__(27);
	
	var _Validator2 = _interopRequireDefault(_Validator);
	
	var _ProductItem = __webpack_require__(28);
	
	var _ProductItem2 = _interopRequireDefault(_ProductItem);
	
	var _Shops2 = __webpack_require__(31);
	
	var _Shops3 = _interopRequireDefault(_Shops2);
	
	var _Helpers = __webpack_require__(29);
	
	__webpack_require__(32);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	/** Import components */
	
	
	/** Import utility functions  */
	
	
	/** Common libs (initialized bu default) */
	// Put mask on phone number inputs
	
	
	$('input[type="tel"]').mask("+7 (999) 999-99-99");
	
	/**
	 * Detect current page and run appropriate scripts.
	 *
	 * @param {String} currentPage - current page detector.
	 */
	switch (_Helpers.currentPcPage) {
	  /**
	   * 'Shops' page scripts.
	   *
	   * @data-pc-page 'custom-map'
	   */
	  case 'custom-map':
	    {
	      // create special class
	      var PcShops = function (_Shops) {
	        _inherits(PcShops, _Shops);
	
	        function PcShops() {
	          _classCallCheck(this, PcShops);
	
	          return _possibleConstructorReturn(this, (PcShops.__proto__ || Object.getPrototypeOf(PcShops)).apply(this, arguments));
	        }
	
	        _createClass(PcShops, [{
	          key: 'initGoogleMap',
	
	          /**
	           * Init google map.
	           *
	           * @override
	           * @return {PcShops}
	           */
	          value: function initGoogleMap() {
	            var GoogleMapsInstance = new _GoogleMaps2.default({}, { url: './ajax/googleMapMarkers.json' }, this.mapLinks);
	            GoogleMapsInstance.init();
	
	            return this;
	          }
	        }]);
	
	        return PcShops;
	      }(_Shops3.default);
	      // create it's new instance
	
	
	      var PcShopsInstance = new PcShops();
	      // run needed methods
	      PcShopsInstance.initDropdownSelects().initCitySelect().initShopSortSelect().initPerfectScrollbar().initGoogleMap();
	
	      break;
	    }
	
	  /**
	   * 'Favourite' page scripts.
	   *
	   * @data-pc-page 'custom-fav'
	   */
	  case 'custom-fav':
	    {
	      var ProductItemInstance = new _ProductItem2.default();
	      ProductItemInstance.cropTitle().initBuyButton();
	
	      break;
	    }
	
	  /**
	   * Pages with forms.
	   *
	   * @data-pc-page 'active'
	   */
	  case 'action':
	    {
	
	      /** DATEPICKER PLUGIN */
	
	      // FIELD TO ACTIVATE DATEPICKER ON
	      var birthdayInputField = $('.change-personal-data-form .form-input.userBirthday input');
	
	      // ACTIVATE DATEPICKER (IF FIELD EXISTS)
	      if (birthdayInputField.length) {
	        birthdayInputField.prop('readonly', true).datepicker({
	          maxDate: new Date(),
	          autoClose: true
	        });
	      }
	
	      /** FORM VALIDATION */
	
	      // COLLECT ALL EXISTING FORMS IN ONE OBJ
	      var forms = {
	        emailForm: {
	          $form: $('.change-email-form'),
	          url: './ajax/success.json',
	          event: 'changeUserEmail'
	        },
	        phoneForm: {
	          $form: $('.change-phone-form'),
	          url: './ajax/error-secondField.json',
	          event: 'changeUserPhone'
	        },
	        passwordForm: {
	          $form: $('.change-password-form'),
	          url: './ajax/error-firstField.json',
	          event: 'changeUserPassword'
	        },
	        bindCardForm: {
	          $form: $('.bind-card-form'),
	          url: './ajax/success.json',
	          event: 'bindUserCard'
	        },
	        activatePromoForm: {
	          $form: $('.activate-promo-form'),
	          url: './ajax/success.json',
	          event: 'activatePromo'
	        },
	        mailingListForm: {
	          $form: $('.change-mailingList-form'),
	          url: './ajax/success.json',
	          event: 'changeMailingList'
	        },
	        changeDeliveryForm: {
	          $form: $('.change-delivery-form'),
	          url: './ajax/success.json',
	          event: 'changeDelivery'
	        },
	        changePersonalDataForm: {
	          $form: $('.change-personal-data-form'),
	          url: './ajax/success.json',
	          event: 'changePersonalData'
	        }
	      };
	
	      // VALIDATION COMMON FUNC
	      var validateField = function validateField(form, url, eventName) {
	        var validateFieldInit = new _Validator2.default(form, function (context) {
	          var data = {
	            event: eventName,
	            data: context.serializedFormData()
	          };
	          return {
	            url: url,
	            method: 'GET',
	            dataType: 'json',
	            cache: false,
	            data: data,
	            success: function success(res) {
	              alert('DATA:\n' + JSON.stringify(data) + '\nRESPONSE:\n' + JSON.stringify(res));
	              if (res.response === 'success') {
	                location.reload();
	              } else if (res.response === 'error') {
	                var fieldWithErrorNumber = res.field;
	                var fieldWithError = context.elements.fields().eq(fieldWithErrorNumber);
	
	                context.throwError(fieldWithError, res.message);
	              }
	            }
	          };
	        });
	      };
	
	      // RUN VALIDATION (IF FORM EXISTS)
	      for (var form in forms) {
	        if (_Helpers.has.call(forms, form) && form.length) {
	          var $form = forms[form].$form;
	          var url = forms[form].url;
	          var eventName = forms[form].event;
	
	          validateField($form, url, eventName);
	        }
	      }
	    }
	}
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*!
	 * jQuery JavaScript Library v3.1.1
	 * https://jquery.com/
	 *
	 * Includes Sizzle.js
	 * https://sizzlejs.com/
	 *
	 * Copyright jQuery Foundation and other contributors
	 * Released under the MIT license
	 * https://jquery.org/license
	 *
	 * Date: 2016-09-22T22:30Z
	 */
	( function( global, factory ) {
	
		"use strict";
	
		if ( typeof module === "object" && typeof module.exports === "object" ) {
	
			// For CommonJS and CommonJS-like environments where a proper `window`
			// is present, execute the factory and get jQuery.
			// For environments that do not have a `window` with a `document`
			// (such as Node.js), expose a factory as module.exports.
			// This accentuates the need for the creation of a real `window`.
			// e.g. var jQuery = require("jquery")(window);
			// See ticket #14549 for more info.
			module.exports = global.document ?
				factory( global, true ) :
				function( w ) {
					if ( !w.document ) {
						throw new Error( "jQuery requires a window with a document" );
					}
					return factory( w );
				};
		} else {
			factory( global );
		}
	
	// Pass this if window is not defined yet
	} )( typeof window !== "undefined" ? window : this, function( window, noGlobal ) {
	
	// Edge <= 12 - 13+, Firefox <=18 - 45+, IE 10 - 11, Safari 5.1 - 9+, iOS 6 - 9.1
	// throw exceptions when non-strict code (e.g., ASP.NET 4.5) accesses strict mode
	// arguments.callee.caller (trac-13335). But as of jQuery 3.0 (2016), strict mode should be common
	// enough that all such attempts are guarded in a try block.
	"use strict";
	
	var arr = [];
	
	var document = window.document;
	
	var getProto = Object.getPrototypeOf;
	
	var slice = arr.slice;
	
	var concat = arr.concat;
	
	var push = arr.push;
	
	var indexOf = arr.indexOf;
	
	var class2type = {};
	
	var toString = class2type.toString;
	
	var hasOwn = class2type.hasOwnProperty;
	
	var fnToString = hasOwn.toString;
	
	var ObjectFunctionString = fnToString.call( Object );
	
	var support = {};
	
	
	
		function DOMEval( code, doc ) {
			doc = doc || document;
	
			var script = doc.createElement( "script" );
	
			script.text = code;
			doc.head.appendChild( script ).parentNode.removeChild( script );
		}
	/* global Symbol */
	// Defining this global in .eslintrc.json would create a danger of using the global
	// unguarded in another place, it seems safer to define global only for this module
	
	
	
	var
		version = "3.1.1",
	
		// Define a local copy of jQuery
		jQuery = function( selector, context ) {
	
			// The jQuery object is actually just the init constructor 'enhanced'
			// Need init if jQuery is called (just allow error to be thrown if not included)
			return new jQuery.fn.init( selector, context );
		},
	
		// Support: Android <=4.0 only
		// Make sure we trim BOM and NBSP
		rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
	
		// Matches dashed string for camelizing
		rmsPrefix = /^-ms-/,
		rdashAlpha = /-([a-z])/g,
	
		// Used by jQuery.camelCase as callback to replace()
		fcamelCase = function( all, letter ) {
			return letter.toUpperCase();
		};
	
	jQuery.fn = jQuery.prototype = {
	
		// The current version of jQuery being used
		jquery: version,
	
		constructor: jQuery,
	
		// The default length of a jQuery object is 0
		length: 0,
	
		toArray: function() {
			return slice.call( this );
		},
	
		// Get the Nth element in the matched element set OR
		// Get the whole matched element set as a clean array
		get: function( num ) {
	
			// Return all the elements in a clean array
			if ( num == null ) {
				return slice.call( this );
			}
	
			// Return just the one element from the set
			return num < 0 ? this[ num + this.length ] : this[ num ];
		},
	
		// Take an array of elements and push it onto the stack
		// (returning the new matched element set)
		pushStack: function( elems ) {
	
			// Build a new jQuery matched element set
			var ret = jQuery.merge( this.constructor(), elems );
	
			// Add the old object onto the stack (as a reference)
			ret.prevObject = this;
	
			// Return the newly-formed element set
			return ret;
		},
	
		// Execute a callback for every element in the matched set.
		each: function( callback ) {
			return jQuery.each( this, callback );
		},
	
		map: function( callback ) {
			return this.pushStack( jQuery.map( this, function( elem, i ) {
				return callback.call( elem, i, elem );
			} ) );
		},
	
		slice: function() {
			return this.pushStack( slice.apply( this, arguments ) );
		},
	
		first: function() {
			return this.eq( 0 );
		},
	
		last: function() {
			return this.eq( -1 );
		},
	
		eq: function( i ) {
			var len = this.length,
				j = +i + ( i < 0 ? len : 0 );
			return this.pushStack( j >= 0 && j < len ? [ this[ j ] ] : [] );
		},
	
		end: function() {
			return this.prevObject || this.constructor();
		},
	
		// For internal use only.
		// Behaves like an Array's method, not like a jQuery method.
		push: push,
		sort: arr.sort,
		splice: arr.splice
	};
	
	jQuery.extend = jQuery.fn.extend = function() {
		var options, name, src, copy, copyIsArray, clone,
			target = arguments[ 0 ] || {},
			i = 1,
			length = arguments.length,
			deep = false;
	
		// Handle a deep copy situation
		if ( typeof target === "boolean" ) {
			deep = target;
	
			// Skip the boolean and the target
			target = arguments[ i ] || {};
			i++;
		}
	
		// Handle case when target is a string or something (possible in deep copy)
		if ( typeof target !== "object" && !jQuery.isFunction( target ) ) {
			target = {};
		}
	
		// Extend jQuery itself if only one argument is passed
		if ( i === length ) {
			target = this;
			i--;
		}
	
		for ( ; i < length; i++ ) {
	
			// Only deal with non-null/undefined values
			if ( ( options = arguments[ i ] ) != null ) {
	
				// Extend the base object
				for ( name in options ) {
					src = target[ name ];
					copy = options[ name ];
	
					// Prevent never-ending loop
					if ( target === copy ) {
						continue;
					}
	
					// Recurse if we're merging plain objects or arrays
					if ( deep && copy && ( jQuery.isPlainObject( copy ) ||
						( copyIsArray = jQuery.isArray( copy ) ) ) ) {
	
						if ( copyIsArray ) {
							copyIsArray = false;
							clone = src && jQuery.isArray( src ) ? src : [];
	
						} else {
							clone = src && jQuery.isPlainObject( src ) ? src : {};
						}
	
						// Never move original objects, clone them
						target[ name ] = jQuery.extend( deep, clone, copy );
	
					// Don't bring in undefined values
					} else if ( copy !== undefined ) {
						target[ name ] = copy;
					}
				}
			}
		}
	
		// Return the modified object
		return target;
	};
	
	jQuery.extend( {
	
		// Unique for each copy of jQuery on the page
		expando: "jQuery" + ( version + Math.random() ).replace( /\D/g, "" ),
	
		// Assume jQuery is ready without the ready module
		isReady: true,
	
		error: function( msg ) {
			throw new Error( msg );
		},
	
		noop: function() {},
	
		isFunction: function( obj ) {
			return jQuery.type( obj ) === "function";
		},
	
		isArray: Array.isArray,
	
		isWindow: function( obj ) {
			return obj != null && obj === obj.window;
		},
	
		isNumeric: function( obj ) {
	
			// As of jQuery 3.0, isNumeric is limited to
			// strings and numbers (primitives or objects)
			// that can be coerced to finite numbers (gh-2662)
			var type = jQuery.type( obj );
			return ( type === "number" || type === "string" ) &&
	
				// parseFloat NaNs numeric-cast false positives ("")
				// ...but misinterprets leading-number strings, particularly hex literals ("0x...")
				// subtraction forces infinities to NaN
				!isNaN( obj - parseFloat( obj ) );
		},
	
		isPlainObject: function( obj ) {
			var proto, Ctor;
	
			// Detect obvious negatives
			// Use toString instead of jQuery.type to catch host objects
			if ( !obj || toString.call( obj ) !== "[object Object]" ) {
				return false;
			}
	
			proto = getProto( obj );
	
			// Objects with no prototype (e.g., `Object.create( null )`) are plain
			if ( !proto ) {
				return true;
			}
	
			// Objects with prototype are plain iff they were constructed by a global Object function
			Ctor = hasOwn.call( proto, "constructor" ) && proto.constructor;
			return typeof Ctor === "function" && fnToString.call( Ctor ) === ObjectFunctionString;
		},
	
		isEmptyObject: function( obj ) {
	
			/* eslint-disable no-unused-vars */
			// See https://github.com/eslint/eslint/issues/6125
			var name;
	
			for ( name in obj ) {
				return false;
			}
			return true;
		},
	
		type: function( obj ) {
			if ( obj == null ) {
				return obj + "";
			}
	
			// Support: Android <=2.3 only (functionish RegExp)
			return typeof obj === "object" || typeof obj === "function" ?
				class2type[ toString.call( obj ) ] || "object" :
				typeof obj;
		},
	
		// Evaluates a script in a global context
		globalEval: function( code ) {
			DOMEval( code );
		},
	
		// Convert dashed to camelCase; used by the css and data modules
		// Support: IE <=9 - 11, Edge 12 - 13
		// Microsoft forgot to hump their vendor prefix (#9572)
		camelCase: function( string ) {
			return string.replace( rmsPrefix, "ms-" ).replace( rdashAlpha, fcamelCase );
		},
	
		nodeName: function( elem, name ) {
			return elem.nodeName && elem.nodeName.toLowerCase() === name.toLowerCase();
		},
	
		each: function( obj, callback ) {
			var length, i = 0;
	
			if ( isArrayLike( obj ) ) {
				length = obj.length;
				for ( ; i < length; i++ ) {
					if ( callback.call( obj[ i ], i, obj[ i ] ) === false ) {
						break;
					}
				}
			} else {
				for ( i in obj ) {
					if ( callback.call( obj[ i ], i, obj[ i ] ) === false ) {
						break;
					}
				}
			}
	
			return obj;
		},
	
		// Support: Android <=4.0 only
		trim: function( text ) {
			return text == null ?
				"" :
				( text + "" ).replace( rtrim, "" );
		},
	
		// results is for internal usage only
		makeArray: function( arr, results ) {
			var ret = results || [];
	
			if ( arr != null ) {
				if ( isArrayLike( Object( arr ) ) ) {
					jQuery.merge( ret,
						typeof arr === "string" ?
						[ arr ] : arr
					);
				} else {
					push.call( ret, arr );
				}
			}
	
			return ret;
		},
	
		inArray: function( elem, arr, i ) {
			return arr == null ? -1 : indexOf.call( arr, elem, i );
		},
	
		// Support: Android <=4.0 only, PhantomJS 1 only
		// push.apply(_, arraylike) throws on ancient WebKit
		merge: function( first, second ) {
			var len = +second.length,
				j = 0,
				i = first.length;
	
			for ( ; j < len; j++ ) {
				first[ i++ ] = second[ j ];
			}
	
			first.length = i;
	
			return first;
		},
	
		grep: function( elems, callback, invert ) {
			var callbackInverse,
				matches = [],
				i = 0,
				length = elems.length,
				callbackExpect = !invert;
	
			// Go through the array, only saving the items
			// that pass the validator function
			for ( ; i < length; i++ ) {
				callbackInverse = !callback( elems[ i ], i );
				if ( callbackInverse !== callbackExpect ) {
					matches.push( elems[ i ] );
				}
			}
	
			return matches;
		},
	
		// arg is for internal usage only
		map: function( elems, callback, arg ) {
			var length, value,
				i = 0,
				ret = [];
	
			// Go through the array, translating each of the items to their new values
			if ( isArrayLike( elems ) ) {
				length = elems.length;
				for ( ; i < length; i++ ) {
					value = callback( elems[ i ], i, arg );
	
					if ( value != null ) {
						ret.push( value );
					}
				}
	
			// Go through every key on the object,
			} else {
				for ( i in elems ) {
					value = callback( elems[ i ], i, arg );
	
					if ( value != null ) {
						ret.push( value );
					}
				}
			}
	
			// Flatten any nested arrays
			return concat.apply( [], ret );
		},
	
		// A global GUID counter for objects
		guid: 1,
	
		// Bind a function to a context, optionally partially applying any
		// arguments.
		proxy: function( fn, context ) {
			var tmp, args, proxy;
	
			if ( typeof context === "string" ) {
				tmp = fn[ context ];
				context = fn;
				fn = tmp;
			}
	
			// Quick check to determine if target is callable, in the spec
			// this throws a TypeError, but we will just return undefined.
			if ( !jQuery.isFunction( fn ) ) {
				return undefined;
			}
	
			// Simulated bind
			args = slice.call( arguments, 2 );
			proxy = function() {
				return fn.apply( context || this, args.concat( slice.call( arguments ) ) );
			};
	
			// Set the guid of unique handler to the same of original handler, so it can be removed
			proxy.guid = fn.guid = fn.guid || jQuery.guid++;
	
			return proxy;
		},
	
		now: Date.now,
	
		// jQuery.support is not used in Core but other projects attach their
		// properties to it so it needs to exist.
		support: support
	} );
	
	if ( typeof Symbol === "function" ) {
		jQuery.fn[ Symbol.iterator ] = arr[ Symbol.iterator ];
	}
	
	// Populate the class2type map
	jQuery.each( "Boolean Number String Function Array Date RegExp Object Error Symbol".split( " " ),
	function( i, name ) {
		class2type[ "[object " + name + "]" ] = name.toLowerCase();
	} );
	
	function isArrayLike( obj ) {
	
		// Support: real iOS 8.2 only (not reproducible in simulator)
		// `in` check used to prevent JIT error (gh-2145)
		// hasOwn isn't used here due to false negatives
		// regarding Nodelist length in IE
		var length = !!obj && "length" in obj && obj.length,
			type = jQuery.type( obj );
	
		if ( type === "function" || jQuery.isWindow( obj ) ) {
			return false;
		}
	
		return type === "array" || length === 0 ||
			typeof length === "number" && length > 0 && ( length - 1 ) in obj;
	}
	var Sizzle =
	/*!
	 * Sizzle CSS Selector Engine v2.3.3
	 * https://sizzlejs.com/
	 *
	 * Copyright jQuery Foundation and other contributors
	 * Released under the MIT license
	 * http://jquery.org/license
	 *
	 * Date: 2016-08-08
	 */
	(function( window ) {
	
	var i,
		support,
		Expr,
		getText,
		isXML,
		tokenize,
		compile,
		select,
		outermostContext,
		sortInput,
		hasDuplicate,
	
		// Local document vars
		setDocument,
		document,
		docElem,
		documentIsHTML,
		rbuggyQSA,
		rbuggyMatches,
		matches,
		contains,
	
		// Instance-specific data
		expando = "sizzle" + 1 * new Date(),
		preferredDoc = window.document,
		dirruns = 0,
		done = 0,
		classCache = createCache(),
		tokenCache = createCache(),
		compilerCache = createCache(),
		sortOrder = function( a, b ) {
			if ( a === b ) {
				hasDuplicate = true;
			}
			return 0;
		},
	
		// Instance methods
		hasOwn = ({}).hasOwnProperty,
		arr = [],
		pop = arr.pop,
		push_native = arr.push,
		push = arr.push,
		slice = arr.slice,
		// Use a stripped-down indexOf as it's faster than native
		// https://jsperf.com/thor-indexof-vs-for/5
		indexOf = function( list, elem ) {
			var i = 0,
				len = list.length;
			for ( ; i < len; i++ ) {
				if ( list[i] === elem ) {
					return i;
				}
			}
			return -1;
		},
	
		booleans = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
	
		// Regular expressions
	
		// http://www.w3.org/TR/css3-selectors/#whitespace
		whitespace = "[\\x20\\t\\r\\n\\f]",
	
		// http://www.w3.org/TR/CSS21/syndata.html#value-def-identifier
		identifier = "(?:\\\\.|[\\w-]|[^\0-\\xa0])+",
	
		// Attribute selectors: http://www.w3.org/TR/selectors/#attribute-selectors
		attributes = "\\[" + whitespace + "*(" + identifier + ")(?:" + whitespace +
			// Operator (capture 2)
			"*([*^$|!~]?=)" + whitespace +
			// "Attribute values must be CSS identifiers [capture 5] or strings [capture 3 or capture 4]"
			"*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + identifier + "))|)" + whitespace +
			"*\\]",
	
		pseudos = ":(" + identifier + ")(?:\\((" +
			// To reduce the number of selectors needing tokenize in the preFilter, prefer arguments:
			// 1. quoted (capture 3; capture 4 or capture 5)
			"('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|" +
			// 2. simple (capture 6)
			"((?:\\\\.|[^\\\\()[\\]]|" + attributes + ")*)|" +
			// 3. anything else (capture 2)
			".*" +
			")\\)|)",
	
		// Leading and non-escaped trailing whitespace, capturing some non-whitespace characters preceding the latter
		rwhitespace = new RegExp( whitespace + "+", "g" ),
		rtrim = new RegExp( "^" + whitespace + "+|((?:^|[^\\\\])(?:\\\\.)*)" + whitespace + "+$", "g" ),
	
		rcomma = new RegExp( "^" + whitespace + "*," + whitespace + "*" ),
		rcombinators = new RegExp( "^" + whitespace + "*([>+~]|" + whitespace + ")" + whitespace + "*" ),
	
		rattributeQuotes = new RegExp( "=" + whitespace + "*([^\\]'\"]*?)" + whitespace + "*\\]", "g" ),
	
		rpseudo = new RegExp( pseudos ),
		ridentifier = new RegExp( "^" + identifier + "$" ),
	
		matchExpr = {
			"ID": new RegExp( "^#(" + identifier + ")" ),
			"CLASS": new RegExp( "^\\.(" + identifier + ")" ),
			"TAG": new RegExp( "^(" + identifier + "|[*])" ),
			"ATTR": new RegExp( "^" + attributes ),
			"PSEUDO": new RegExp( "^" + pseudos ),
			"CHILD": new RegExp( "^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + whitespace +
				"*(even|odd|(([+-]|)(\\d*)n|)" + whitespace + "*(?:([+-]|)" + whitespace +
				"*(\\d+)|))" + whitespace + "*\\)|)", "i" ),
			"bool": new RegExp( "^(?:" + booleans + ")$", "i" ),
			// For use in libraries implementing .is()
			// We use this for POS matching in `select`
			"needsContext": new RegExp( "^" + whitespace + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" +
				whitespace + "*((?:-\\d)?\\d*)" + whitespace + "*\\)|)(?=[^-]|$)", "i" )
		},
	
		rinputs = /^(?:input|select|textarea|button)$/i,
		rheader = /^h\d$/i,
	
		rnative = /^[^{]+\{\s*\[native \w/,
	
		// Easily-parseable/retrievable ID or TAG or CLASS selectors
		rquickExpr = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
	
		rsibling = /[+~]/,
	
		// CSS escapes
		// http://www.w3.org/TR/CSS21/syndata.html#escaped-characters
		runescape = new RegExp( "\\\\([\\da-f]{1,6}" + whitespace + "?|(" + whitespace + ")|.)", "ig" ),
		funescape = function( _, escaped, escapedWhitespace ) {
			var high = "0x" + escaped - 0x10000;
			// NaN means non-codepoint
			// Support: Firefox<24
			// Workaround erroneous numeric interpretation of +"0x"
			return high !== high || escapedWhitespace ?
				escaped :
				high < 0 ?
					// BMP codepoint
					String.fromCharCode( high + 0x10000 ) :
					// Supplemental Plane codepoint (surrogate pair)
					String.fromCharCode( high >> 10 | 0xD800, high & 0x3FF | 0xDC00 );
		},
	
		// CSS string/identifier serialization
		// https://drafts.csswg.org/cssom/#common-serializing-idioms
		rcssescape = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
		fcssescape = function( ch, asCodePoint ) {
			if ( asCodePoint ) {
	
				// U+0000 NULL becomes U+FFFD REPLACEMENT CHARACTER
				if ( ch === "\0" ) {
					return "\uFFFD";
				}
	
				// Control characters and (dependent upon position) numbers get escaped as code points
				return ch.slice( 0, -1 ) + "\\" + ch.charCodeAt( ch.length - 1 ).toString( 16 ) + " ";
			}
	
			// Other potentially-special ASCII characters get backslash-escaped
			return "\\" + ch;
		},
	
		// Used for iframes
		// See setDocument()
		// Removing the function wrapper causes a "Permission Denied"
		// error in IE
		unloadHandler = function() {
			setDocument();
		},
	
		disabledAncestor = addCombinator(
			function( elem ) {
				return elem.disabled === true && ("form" in elem || "label" in elem);
			},
			{ dir: "parentNode", next: "legend" }
		);
	
	// Optimize for push.apply( _, NodeList )
	try {
		push.apply(
			(arr = slice.call( preferredDoc.childNodes )),
			preferredDoc.childNodes
		);
		// Support: Android<4.0
		// Detect silently failing push.apply
		arr[ preferredDoc.childNodes.length ].nodeType;
	} catch ( e ) {
		push = { apply: arr.length ?
	
			// Leverage slice if possible
			function( target, els ) {
				push_native.apply( target, slice.call(els) );
			} :
	
			// Support: IE<9
			// Otherwise append directly
			function( target, els ) {
				var j = target.length,
					i = 0;
				// Can't trust NodeList.length
				while ( (target[j++] = els[i++]) ) {}
				target.length = j - 1;
			}
		};
	}
	
	function Sizzle( selector, context, results, seed ) {
		var m, i, elem, nid, match, groups, newSelector,
			newContext = context && context.ownerDocument,
	
			// nodeType defaults to 9, since context defaults to document
			nodeType = context ? context.nodeType : 9;
	
		results = results || [];
	
		// Return early from calls with invalid selector or context
		if ( typeof selector !== "string" || !selector ||
			nodeType !== 1 && nodeType !== 9 && nodeType !== 11 ) {
	
			return results;
		}
	
		// Try to shortcut find operations (as opposed to filters) in HTML documents
		if ( !seed ) {
	
			if ( ( context ? context.ownerDocument || context : preferredDoc ) !== document ) {
				setDocument( context );
			}
			context = context || document;
	
			if ( documentIsHTML ) {
	
				// If the selector is sufficiently simple, try using a "get*By*" DOM method
				// (excepting DocumentFragment context, where the methods don't exist)
				if ( nodeType !== 11 && (match = rquickExpr.exec( selector )) ) {
	
					// ID selector
					if ( (m = match[1]) ) {
	
						// Document context
						if ( nodeType === 9 ) {
							if ( (elem = context.getElementById( m )) ) {
	
								// Support: IE, Opera, Webkit
								// TODO: identify versions
								// getElementById can match elements by name instead of ID
								if ( elem.id === m ) {
									results.push( elem );
									return results;
								}
							} else {
								return results;
							}
	
						// Element context
						} else {
	
							// Support: IE, Opera, Webkit
							// TODO: identify versions
							// getElementById can match elements by name instead of ID
							if ( newContext && (elem = newContext.getElementById( m )) &&
								contains( context, elem ) &&
								elem.id === m ) {
	
								results.push( elem );
								return results;
							}
						}
	
					// Type selector
					} else if ( match[2] ) {
						push.apply( results, context.getElementsByTagName( selector ) );
						return results;
	
					// Class selector
					} else if ( (m = match[3]) && support.getElementsByClassName &&
						context.getElementsByClassName ) {
	
						push.apply( results, context.getElementsByClassName( m ) );
						return results;
					}
				}
	
				// Take advantage of querySelectorAll
				if ( support.qsa &&
					!compilerCache[ selector + " " ] &&
					(!rbuggyQSA || !rbuggyQSA.test( selector )) ) {
	
					if ( nodeType !== 1 ) {
						newContext = context;
						newSelector = selector;
	
					// qSA looks outside Element context, which is not what we want
					// Thanks to Andrew Dupont for this workaround technique
					// Support: IE <=8
					// Exclude object elements
					} else if ( context.nodeName.toLowerCase() !== "object" ) {
	
						// Capture the context ID, setting it first if necessary
						if ( (nid = context.getAttribute( "id" )) ) {
							nid = nid.replace( rcssescape, fcssescape );
						} else {
							context.setAttribute( "id", (nid = expando) );
						}
	
						// Prefix every selector in the list
						groups = tokenize( selector );
						i = groups.length;
						while ( i-- ) {
							groups[i] = "#" + nid + " " + toSelector( groups[i] );
						}
						newSelector = groups.join( "," );
	
						// Expand context for sibling selectors
						newContext = rsibling.test( selector ) && testContext( context.parentNode ) ||
							context;
					}
	
					if ( newSelector ) {
						try {
							push.apply( results,
								newContext.querySelectorAll( newSelector )
							);
							return results;
						} catch ( qsaError ) {
						} finally {
							if ( nid === expando ) {
								context.removeAttribute( "id" );
							}
						}
					}
				}
			}
		}
	
		// All others
		return select( selector.replace( rtrim, "$1" ), context, results, seed );
	}
	
	/**
	 * Create key-value caches of limited size
	 * @returns {function(string, object)} Returns the Object data after storing it on itself with
	 *	property name the (space-suffixed) string and (if the cache is larger than Expr.cacheLength)
	 *	deleting the oldest entry
	 */
	function createCache() {
		var keys = [];
	
		function cache( key, value ) {
			// Use (key + " ") to avoid collision with native prototype properties (see Issue #157)
			if ( keys.push( key + " " ) > Expr.cacheLength ) {
				// Only keep the most recent entries
				delete cache[ keys.shift() ];
			}
			return (cache[ key + " " ] = value);
		}
		return cache;
	}
	
	/**
	 * Mark a function for special use by Sizzle
	 * @param {Function} fn The function to mark
	 */
	function markFunction( fn ) {
		fn[ expando ] = true;
		return fn;
	}
	
	/**
	 * Support testing using an element
	 * @param {Function} fn Passed the created element and returns a boolean result
	 */
	function assert( fn ) {
		var el = document.createElement("fieldset");
	
		try {
			return !!fn( el );
		} catch (e) {
			return false;
		} finally {
			// Remove from its parent by default
			if ( el.parentNode ) {
				el.parentNode.removeChild( el );
			}
			// release memory in IE
			el = null;
		}
	}
	
	/**
	 * Adds the same handler for all of the specified attrs
	 * @param {String} attrs Pipe-separated list of attributes
	 * @param {Function} handler The method that will be applied
	 */
	function addHandle( attrs, handler ) {
		var arr = attrs.split("|"),
			i = arr.length;
	
		while ( i-- ) {
			Expr.attrHandle[ arr[i] ] = handler;
		}
	}
	
	/**
	 * Checks document order of two siblings
	 * @param {Element} a
	 * @param {Element} b
	 * @returns {Number} Returns less than 0 if a precedes b, greater than 0 if a follows b
	 */
	function siblingCheck( a, b ) {
		var cur = b && a,
			diff = cur && a.nodeType === 1 && b.nodeType === 1 &&
				a.sourceIndex - b.sourceIndex;
	
		// Use IE sourceIndex if available on both nodes
		if ( diff ) {
			return diff;
		}
	
		// Check if b follows a
		if ( cur ) {
			while ( (cur = cur.nextSibling) ) {
				if ( cur === b ) {
					return -1;
				}
			}
		}
	
		return a ? 1 : -1;
	}
	
	/**
	 * Returns a function to use in pseudos for input types
	 * @param {String} type
	 */
	function createInputPseudo( type ) {
		return function( elem ) {
			var name = elem.nodeName.toLowerCase();
			return name === "input" && elem.type === type;
		};
	}
	
	/**
	 * Returns a function to use in pseudos for buttons
	 * @param {String} type
	 */
	function createButtonPseudo( type ) {
		return function( elem ) {
			var name = elem.nodeName.toLowerCase();
			return (name === "input" || name === "button") && elem.type === type;
		};
	}
	
	/**
	 * Returns a function to use in pseudos for :enabled/:disabled
	 * @param {Boolean} disabled true for :disabled; false for :enabled
	 */
	function createDisabledPseudo( disabled ) {
	
		// Known :disabled false positives: fieldset[disabled] > legend:nth-of-type(n+2) :can-disable
		return function( elem ) {
	
			// Only certain elements can match :enabled or :disabled
			// https://html.spec.whatwg.org/multipage/scripting.html#selector-enabled
			// https://html.spec.whatwg.org/multipage/scripting.html#selector-disabled
			if ( "form" in elem ) {
	
				// Check for inherited disabledness on relevant non-disabled elements:
				// * listed form-associated elements in a disabled fieldset
				//   https://html.spec.whatwg.org/multipage/forms.html#category-listed
				//   https://html.spec.whatwg.org/multipage/forms.html#concept-fe-disabled
				// * option elements in a disabled optgroup
				//   https://html.spec.whatwg.org/multipage/forms.html#concept-option-disabled
				// All such elements have a "form" property.
				if ( elem.parentNode && elem.disabled === false ) {
	
					// Option elements defer to a parent optgroup if present
					if ( "label" in elem ) {
						if ( "label" in elem.parentNode ) {
							return elem.parentNode.disabled === disabled;
						} else {
							return elem.disabled === disabled;
						}
					}
	
					// Support: IE 6 - 11
					// Use the isDisabled shortcut property to check for disabled fieldset ancestors
					return elem.isDisabled === disabled ||
	
						// Where there is no isDisabled, check manually
						/* jshint -W018 */
						elem.isDisabled !== !disabled &&
							disabledAncestor( elem ) === disabled;
				}
	
				return elem.disabled === disabled;
	
			// Try to winnow out elements that can't be disabled before trusting the disabled property.
			// Some victims get caught in our net (label, legend, menu, track), but it shouldn't
			// even exist on them, let alone have a boolean value.
			} else if ( "label" in elem ) {
				return elem.disabled === disabled;
			}
	
			// Remaining elements are neither :enabled nor :disabled
			return false;
		};
	}
	
	/**
	 * Returns a function to use in pseudos for positionals
	 * @param {Function} fn
	 */
	function createPositionalPseudo( fn ) {
		return markFunction(function( argument ) {
			argument = +argument;
			return markFunction(function( seed, matches ) {
				var j,
					matchIndexes = fn( [], seed.length, argument ),
					i = matchIndexes.length;
	
				// Match elements found at the specified indexes
				while ( i-- ) {
					if ( seed[ (j = matchIndexes[i]) ] ) {
						seed[j] = !(matches[j] = seed[j]);
					}
				}
			});
		});
	}
	
	/**
	 * Checks a node for validity as a Sizzle context
	 * @param {Element|Object=} context
	 * @returns {Element|Object|Boolean} The input node if acceptable, otherwise a falsy value
	 */
	function testContext( context ) {
		return context && typeof context.getElementsByTagName !== "undefined" && context;
	}
	
	// Expose support vars for convenience
	support = Sizzle.support = {};
	
	/**
	 * Detects XML nodes
	 * @param {Element|Object} elem An element or a document
	 * @returns {Boolean} True iff elem is a non-HTML XML node
	 */
	isXML = Sizzle.isXML = function( elem ) {
		// documentElement is verified for cases where it doesn't yet exist
		// (such as loading iframes in IE - #4833)
		var documentElement = elem && (elem.ownerDocument || elem).documentElement;
		return documentElement ? documentElement.nodeName !== "HTML" : false;
	};
	
	/**
	 * Sets document-related variables once based on the current document
	 * @param {Element|Object} [doc] An element or document object to use to set the document
	 * @returns {Object} Returns the current document
	 */
	setDocument = Sizzle.setDocument = function( node ) {
		var hasCompare, subWindow,
			doc = node ? node.ownerDocument || node : preferredDoc;
	
		// Return early if doc is invalid or already selected
		if ( doc === document || doc.nodeType !== 9 || !doc.documentElement ) {
			return document;
		}
	
		// Update global variables
		document = doc;
		docElem = document.documentElement;
		documentIsHTML = !isXML( document );
	
		// Support: IE 9-11, Edge
		// Accessing iframe documents after unload throws "permission denied" errors (jQuery #13936)
		if ( preferredDoc !== document &&
			(subWindow = document.defaultView) && subWindow.top !== subWindow ) {
	
			// Support: IE 11, Edge
			if ( subWindow.addEventListener ) {
				subWindow.addEventListener( "unload", unloadHandler, false );
	
			// Support: IE 9 - 10 only
			} else if ( subWindow.attachEvent ) {
				subWindow.attachEvent( "onunload", unloadHandler );
			}
		}
	
		/* Attributes
		---------------------------------------------------------------------- */
	
		// Support: IE<8
		// Verify that getAttribute really returns attributes and not properties
		// (excepting IE8 booleans)
		support.attributes = assert(function( el ) {
			el.className = "i";
			return !el.getAttribute("className");
		});
	
		/* getElement(s)By*
		---------------------------------------------------------------------- */
	
		// Check if getElementsByTagName("*") returns only elements
		support.getElementsByTagName = assert(function( el ) {
			el.appendChild( document.createComment("") );
			return !el.getElementsByTagName("*").length;
		});
	
		// Support: IE<9
		support.getElementsByClassName = rnative.test( document.getElementsByClassName );
	
		// Support: IE<10
		// Check if getElementById returns elements by name
		// The broken getElementById methods don't pick up programmatically-set names,
		// so use a roundabout getElementsByName test
		support.getById = assert(function( el ) {
			docElem.appendChild( el ).id = expando;
			return !document.getElementsByName || !document.getElementsByName( expando ).length;
		});
	
		// ID filter and find
		if ( support.getById ) {
			Expr.filter["ID"] = function( id ) {
				var attrId = id.replace( runescape, funescape );
				return function( elem ) {
					return elem.getAttribute("id") === attrId;
				};
			};
			Expr.find["ID"] = function( id, context ) {
				if ( typeof context.getElementById !== "undefined" && documentIsHTML ) {
					var elem = context.getElementById( id );
					return elem ? [ elem ] : [];
				}
			};
		} else {
			Expr.filter["ID"] =  function( id ) {
				var attrId = id.replace( runescape, funescape );
				return function( elem ) {
					var node = typeof elem.getAttributeNode !== "undefined" &&
						elem.getAttributeNode("id");
					return node && node.value === attrId;
				};
			};
	
			// Support: IE 6 - 7 only
			// getElementById is not reliable as a find shortcut
			Expr.find["ID"] = function( id, context ) {
				if ( typeof context.getElementById !== "undefined" && documentIsHTML ) {
					var node, i, elems,
						elem = context.getElementById( id );
	
					if ( elem ) {
	
						// Verify the id attribute
						node = elem.getAttributeNode("id");
						if ( node && node.value === id ) {
							return [ elem ];
						}
	
						// Fall back on getElementsByName
						elems = context.getElementsByName( id );
						i = 0;
						while ( (elem = elems[i++]) ) {
							node = elem.getAttributeNode("id");
							if ( node && node.value === id ) {
								return [ elem ];
							}
						}
					}
	
					return [];
				}
			};
		}
	
		// Tag
		Expr.find["TAG"] = support.getElementsByTagName ?
			function( tag, context ) {
				if ( typeof context.getElementsByTagName !== "undefined" ) {
					return context.getElementsByTagName( tag );
	
				// DocumentFragment nodes don't have gEBTN
				} else if ( support.qsa ) {
					return context.querySelectorAll( tag );
				}
			} :
	
			function( tag, context ) {
				var elem,
					tmp = [],
					i = 0,
					// By happy coincidence, a (broken) gEBTN appears on DocumentFragment nodes too
					results = context.getElementsByTagName( tag );
	
				// Filter out possible comments
				if ( tag === "*" ) {
					while ( (elem = results[i++]) ) {
						if ( elem.nodeType === 1 ) {
							tmp.push( elem );
						}
					}
	
					return tmp;
				}
				return results;
			};
	
		// Class
		Expr.find["CLASS"] = support.getElementsByClassName && function( className, context ) {
			if ( typeof context.getElementsByClassName !== "undefined" && documentIsHTML ) {
				return context.getElementsByClassName( className );
			}
		};
	
		/* QSA/matchesSelector
		---------------------------------------------------------------------- */
	
		// QSA and matchesSelector support
	
		// matchesSelector(:active) reports false when true (IE9/Opera 11.5)
		rbuggyMatches = [];
	
		// qSa(:focus) reports false when true (Chrome 21)
		// We allow this because of a bug in IE8/9 that throws an error
		// whenever `document.activeElement` is accessed on an iframe
		// So, we allow :focus to pass through QSA all the time to avoid the IE error
		// See https://bugs.jquery.com/ticket/13378
		rbuggyQSA = [];
	
		if ( (support.qsa = rnative.test( document.querySelectorAll )) ) {
			// Build QSA regex
			// Regex strategy adopted from Diego Perini
			assert(function( el ) {
				// Select is set to empty string on purpose
				// This is to test IE's treatment of not explicitly
				// setting a boolean content attribute,
				// since its presence should be enough
				// https://bugs.jquery.com/ticket/12359
				docElem.appendChild( el ).innerHTML = "<a id='" + expando + "'></a>" +
					"<select id='" + expando + "-\r\\' msallowcapture=''>" +
					"<option selected=''></option></select>";
	
				// Support: IE8, Opera 11-12.16
				// Nothing should be selected when empty strings follow ^= or $= or *=
				// The test attribute must be unknown in Opera but "safe" for WinRT
				// https://msdn.microsoft.com/en-us/library/ie/hh465388.aspx#attribute_section
				if ( el.querySelectorAll("[msallowcapture^='']").length ) {
					rbuggyQSA.push( "[*^$]=" + whitespace + "*(?:''|\"\")" );
				}
	
				// Support: IE8
				// Boolean attributes and "value" are not treated correctly
				if ( !el.querySelectorAll("[selected]").length ) {
					rbuggyQSA.push( "\\[" + whitespace + "*(?:value|" + booleans + ")" );
				}
	
				// Support: Chrome<29, Android<4.4, Safari<7.0+, iOS<7.0+, PhantomJS<1.9.8+
				if ( !el.querySelectorAll( "[id~=" + expando + "-]" ).length ) {
					rbuggyQSA.push("~=");
				}
	
				// Webkit/Opera - :checked should return selected option elements
				// http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
				// IE8 throws error here and will not see later tests
				if ( !el.querySelectorAll(":checked").length ) {
					rbuggyQSA.push(":checked");
				}
	
				// Support: Safari 8+, iOS 8+
				// https://bugs.webkit.org/show_bug.cgi?id=136851
				// In-page `selector#id sibling-combinator selector` fails
				if ( !el.querySelectorAll( "a#" + expando + "+*" ).length ) {
					rbuggyQSA.push(".#.+[+~]");
				}
			});
	
			assert(function( el ) {
				el.innerHTML = "<a href='' disabled='disabled'></a>" +
					"<select disabled='disabled'><option/></select>";
	
				// Support: Windows 8 Native Apps
				// The type and name attributes are restricted during .innerHTML assignment
				var input = document.createElement("input");
				input.setAttribute( "type", "hidden" );
				el.appendChild( input ).setAttribute( "name", "D" );
	
				// Support: IE8
				// Enforce case-sensitivity of name attribute
				if ( el.querySelectorAll("[name=d]").length ) {
					rbuggyQSA.push( "name" + whitespace + "*[*^$|!~]?=" );
				}
	
				// FF 3.5 - :enabled/:disabled and hidden elements (hidden elements are still enabled)
				// IE8 throws error here and will not see later tests
				if ( el.querySelectorAll(":enabled").length !== 2 ) {
					rbuggyQSA.push( ":enabled", ":disabled" );
				}
	
				// Support: IE9-11+
				// IE's :disabled selector does not pick up the children of disabled fieldsets
				docElem.appendChild( el ).disabled = true;
				if ( el.querySelectorAll(":disabled").length !== 2 ) {
					rbuggyQSA.push( ":enabled", ":disabled" );
				}
	
				// Opera 10-11 does not throw on post-comma invalid pseudos
				el.querySelectorAll("*,:x");
				rbuggyQSA.push(",.*:");
			});
		}
	
		if ( (support.matchesSelector = rnative.test( (matches = docElem.matches ||
			docElem.webkitMatchesSelector ||
			docElem.mozMatchesSelector ||
			docElem.oMatchesSelector ||
			docElem.msMatchesSelector) )) ) {
	
			assert(function( el ) {
				// Check to see if it's possible to do matchesSelector
				// on a disconnected node (IE 9)
				support.disconnectedMatch = matches.call( el, "*" );
	
				// This should fail with an exception
				// Gecko does not error, returns false instead
				matches.call( el, "[s!='']:x" );
				rbuggyMatches.push( "!=", pseudos );
			});
		}
	
		rbuggyQSA = rbuggyQSA.length && new RegExp( rbuggyQSA.join("|") );
		rbuggyMatches = rbuggyMatches.length && new RegExp( rbuggyMatches.join("|") );
	
		/* Contains
		---------------------------------------------------------------------- */
		hasCompare = rnative.test( docElem.compareDocumentPosition );
	
		// Element contains another
		// Purposefully self-exclusive
		// As in, an element does not contain itself
		contains = hasCompare || rnative.test( docElem.contains ) ?
			function( a, b ) {
				var adown = a.nodeType === 9 ? a.documentElement : a,
					bup = b && b.parentNode;
				return a === bup || !!( bup && bup.nodeType === 1 && (
					adown.contains ?
						adown.contains( bup ) :
						a.compareDocumentPosition && a.compareDocumentPosition( bup ) & 16
				));
			} :
			function( a, b ) {
				if ( b ) {
					while ( (b = b.parentNode) ) {
						if ( b === a ) {
							return true;
						}
					}
				}
				return false;
			};
	
		/* Sorting
		---------------------------------------------------------------------- */
	
		// Document order sorting
		sortOrder = hasCompare ?
		function( a, b ) {
	
			// Flag for duplicate removal
			if ( a === b ) {
				hasDuplicate = true;
				return 0;
			}
	
			// Sort on method existence if only one input has compareDocumentPosition
			var compare = !a.compareDocumentPosition - !b.compareDocumentPosition;
			if ( compare ) {
				return compare;
			}
	
			// Calculate position if both inputs belong to the same document
			compare = ( a.ownerDocument || a ) === ( b.ownerDocument || b ) ?
				a.compareDocumentPosition( b ) :
	
				// Otherwise we know they are disconnected
				1;
	
			// Disconnected nodes
			if ( compare & 1 ||
				(!support.sortDetached && b.compareDocumentPosition( a ) === compare) ) {
	
				// Choose the first element that is related to our preferred document
				if ( a === document || a.ownerDocument === preferredDoc && contains(preferredDoc, a) ) {
					return -1;
				}
				if ( b === document || b.ownerDocument === preferredDoc && contains(preferredDoc, b) ) {
					return 1;
				}
	
				// Maintain original order
				return sortInput ?
					( indexOf( sortInput, a ) - indexOf( sortInput, b ) ) :
					0;
			}
	
			return compare & 4 ? -1 : 1;
		} :
		function( a, b ) {
			// Exit early if the nodes are identical
			if ( a === b ) {
				hasDuplicate = true;
				return 0;
			}
	
			var cur,
				i = 0,
				aup = a.parentNode,
				bup = b.parentNode,
				ap = [ a ],
				bp = [ b ];
	
			// Parentless nodes are either documents or disconnected
			if ( !aup || !bup ) {
				return a === document ? -1 :
					b === document ? 1 :
					aup ? -1 :
					bup ? 1 :
					sortInput ?
					( indexOf( sortInput, a ) - indexOf( sortInput, b ) ) :
					0;
	
			// If the nodes are siblings, we can do a quick check
			} else if ( aup === bup ) {
				return siblingCheck( a, b );
			}
	
			// Otherwise we need full lists of their ancestors for comparison
			cur = a;
			while ( (cur = cur.parentNode) ) {
				ap.unshift( cur );
			}
			cur = b;
			while ( (cur = cur.parentNode) ) {
				bp.unshift( cur );
			}
	
			// Walk down the tree looking for a discrepancy
			while ( ap[i] === bp[i] ) {
				i++;
			}
	
			return i ?
				// Do a sibling check if the nodes have a common ancestor
				siblingCheck( ap[i], bp[i] ) :
	
				// Otherwise nodes in our document sort first
				ap[i] === preferredDoc ? -1 :
				bp[i] === preferredDoc ? 1 :
				0;
		};
	
		return document;
	};
	
	Sizzle.matches = function( expr, elements ) {
		return Sizzle( expr, null, null, elements );
	};
	
	Sizzle.matchesSelector = function( elem, expr ) {
		// Set document vars if needed
		if ( ( elem.ownerDocument || elem ) !== document ) {
			setDocument( elem );
		}
	
		// Make sure that attribute selectors are quoted
		expr = expr.replace( rattributeQuotes, "='$1']" );
	
		if ( support.matchesSelector && documentIsHTML &&
			!compilerCache[ expr + " " ] &&
			( !rbuggyMatches || !rbuggyMatches.test( expr ) ) &&
			( !rbuggyQSA     || !rbuggyQSA.test( expr ) ) ) {
	
			try {
				var ret = matches.call( elem, expr );
	
				// IE 9's matchesSelector returns false on disconnected nodes
				if ( ret || support.disconnectedMatch ||
						// As well, disconnected nodes are said to be in a document
						// fragment in IE 9
						elem.document && elem.document.nodeType !== 11 ) {
					return ret;
				}
			} catch (e) {}
		}
	
		return Sizzle( expr, document, null, [ elem ] ).length > 0;
	};
	
	Sizzle.contains = function( context, elem ) {
		// Set document vars if needed
		if ( ( context.ownerDocument || context ) !== document ) {
			setDocument( context );
		}
		return contains( context, elem );
	};
	
	Sizzle.attr = function( elem, name ) {
		// Set document vars if needed
		if ( ( elem.ownerDocument || elem ) !== document ) {
			setDocument( elem );
		}
	
		var fn = Expr.attrHandle[ name.toLowerCase() ],
			// Don't get fooled by Object.prototype properties (jQuery #13807)
			val = fn && hasOwn.call( Expr.attrHandle, name.toLowerCase() ) ?
				fn( elem, name, !documentIsHTML ) :
				undefined;
	
		return val !== undefined ?
			val :
			support.attributes || !documentIsHTML ?
				elem.getAttribute( name ) :
				(val = elem.getAttributeNode(name)) && val.specified ?
					val.value :
					null;
	};
	
	Sizzle.escape = function( sel ) {
		return (sel + "").replace( rcssescape, fcssescape );
	};
	
	Sizzle.error = function( msg ) {
		throw new Error( "Syntax error, unrecognized expression: " + msg );
	};
	
	/**
	 * Document sorting and removing duplicates
	 * @param {ArrayLike} results
	 */
	Sizzle.uniqueSort = function( results ) {
		var elem,
			duplicates = [],
			j = 0,
			i = 0;
	
		// Unless we *know* we can detect duplicates, assume their presence
		hasDuplicate = !support.detectDuplicates;
		sortInput = !support.sortStable && results.slice( 0 );
		results.sort( sortOrder );
	
		if ( hasDuplicate ) {
			while ( (elem = results[i++]) ) {
				if ( elem === results[ i ] ) {
					j = duplicates.push( i );
				}
			}
			while ( j-- ) {
				results.splice( duplicates[ j ], 1 );
			}
		}
	
		// Clear input after sorting to release objects
		// See https://github.com/jquery/sizzle/pull/225
		sortInput = null;
	
		return results;
	};
	
	/**
	 * Utility function for retrieving the text value of an array of DOM nodes
	 * @param {Array|Element} elem
	 */
	getText = Sizzle.getText = function( elem ) {
		var node,
			ret = "",
			i = 0,
			nodeType = elem.nodeType;
	
		if ( !nodeType ) {
			// If no nodeType, this is expected to be an array
			while ( (node = elem[i++]) ) {
				// Do not traverse comment nodes
				ret += getText( node );
			}
		} else if ( nodeType === 1 || nodeType === 9 || nodeType === 11 ) {
			// Use textContent for elements
			// innerText usage removed for consistency of new lines (jQuery #11153)
			if ( typeof elem.textContent === "string" ) {
				return elem.textContent;
			} else {
				// Traverse its children
				for ( elem = elem.firstChild; elem; elem = elem.nextSibling ) {
					ret += getText( elem );
				}
			}
		} else if ( nodeType === 3 || nodeType === 4 ) {
			return elem.nodeValue;
		}
		// Do not include comment or processing instruction nodes
	
		return ret;
	};
	
	Expr = Sizzle.selectors = {
	
		// Can be adjusted by the user
		cacheLength: 50,
	
		createPseudo: markFunction,
	
		match: matchExpr,
	
		attrHandle: {},
	
		find: {},
	
		relative: {
			">": { dir: "parentNode", first: true },
			" ": { dir: "parentNode" },
			"+": { dir: "previousSibling", first: true },
			"~": { dir: "previousSibling" }
		},
	
		preFilter: {
			"ATTR": function( match ) {
				match[1] = match[1].replace( runescape, funescape );
	
				// Move the given value to match[3] whether quoted or unquoted
				match[3] = ( match[3] || match[4] || match[5] || "" ).replace( runescape, funescape );
	
				if ( match[2] === "~=" ) {
					match[3] = " " + match[3] + " ";
				}
	
				return match.slice( 0, 4 );
			},
	
			"CHILD": function( match ) {
				/* matches from matchExpr["CHILD"]
					1 type (only|nth|...)
					2 what (child|of-type)
					3 argument (even|odd|\d*|\d*n([+-]\d+)?|...)
					4 xn-component of xn+y argument ([+-]?\d*n|)
					5 sign of xn-component
					6 x of xn-component
					7 sign of y-component
					8 y of y-component
				*/
				match[1] = match[1].toLowerCase();
	
				if ( match[1].slice( 0, 3 ) === "nth" ) {
					// nth-* requires argument
					if ( !match[3] ) {
						Sizzle.error( match[0] );
					}
	
					// numeric x and y parameters for Expr.filter.CHILD
					// remember that false/true cast respectively to 0/1
					match[4] = +( match[4] ? match[5] + (match[6] || 1) : 2 * ( match[3] === "even" || match[3] === "odd" ) );
					match[5] = +( ( match[7] + match[8] ) || match[3] === "odd" );
	
				// other types prohibit arguments
				} else if ( match[3] ) {
					Sizzle.error( match[0] );
				}
	
				return match;
			},
	
			"PSEUDO": function( match ) {
				var excess,
					unquoted = !match[6] && match[2];
	
				if ( matchExpr["CHILD"].test( match[0] ) ) {
					return null;
				}
	
				// Accept quoted arguments as-is
				if ( match[3] ) {
					match[2] = match[4] || match[5] || "";
	
				// Strip excess characters from unquoted arguments
				} else if ( unquoted && rpseudo.test( unquoted ) &&
					// Get excess from tokenize (recursively)
					(excess = tokenize( unquoted, true )) &&
					// advance to the next closing parenthesis
					(excess = unquoted.indexOf( ")", unquoted.length - excess ) - unquoted.length) ) {
	
					// excess is a negative index
					match[0] = match[0].slice( 0, excess );
					match[2] = unquoted.slice( 0, excess );
				}
	
				// Return only captures needed by the pseudo filter method (type and argument)
				return match.slice( 0, 3 );
			}
		},
	
		filter: {
	
			"TAG": function( nodeNameSelector ) {
				var nodeName = nodeNameSelector.replace( runescape, funescape ).toLowerCase();
				return nodeNameSelector === "*" ?
					function() { return true; } :
					function( elem ) {
						return elem.nodeName && elem.nodeName.toLowerCase() === nodeName;
					};
			},
	
			"CLASS": function( className ) {
				var pattern = classCache[ className + " " ];
	
				return pattern ||
					(pattern = new RegExp( "(^|" + whitespace + ")" + className + "(" + whitespace + "|$)" )) &&
					classCache( className, function( elem ) {
						return pattern.test( typeof elem.className === "string" && elem.className || typeof elem.getAttribute !== "undefined" && elem.getAttribute("class") || "" );
					});
			},
	
			"ATTR": function( name, operator, check ) {
				return function( elem ) {
					var result = Sizzle.attr( elem, name );
	
					if ( result == null ) {
						return operator === "!=";
					}
					if ( !operator ) {
						return true;
					}
	
					result += "";
	
					return operator === "=" ? result === check :
						operator === "!=" ? result !== check :
						operator === "^=" ? check && result.indexOf( check ) === 0 :
						operator === "*=" ? check && result.indexOf( check ) > -1 :
						operator === "$=" ? check && result.slice( -check.length ) === check :
						operator === "~=" ? ( " " + result.replace( rwhitespace, " " ) + " " ).indexOf( check ) > -1 :
						operator === "|=" ? result === check || result.slice( 0, check.length + 1 ) === check + "-" :
						false;
				};
			},
	
			"CHILD": function( type, what, argument, first, last ) {
				var simple = type.slice( 0, 3 ) !== "nth",
					forward = type.slice( -4 ) !== "last",
					ofType = what === "of-type";
	
				return first === 1 && last === 0 ?
	
					// Shortcut for :nth-*(n)
					function( elem ) {
						return !!elem.parentNode;
					} :
	
					function( elem, context, xml ) {
						var cache, uniqueCache, outerCache, node, nodeIndex, start,
							dir = simple !== forward ? "nextSibling" : "previousSibling",
							parent = elem.parentNode,
							name = ofType && elem.nodeName.toLowerCase(),
							useCache = !xml && !ofType,
							diff = false;
	
						if ( parent ) {
	
							// :(first|last|only)-(child|of-type)
							if ( simple ) {
								while ( dir ) {
									node = elem;
									while ( (node = node[ dir ]) ) {
										if ( ofType ?
											node.nodeName.toLowerCase() === name :
											node.nodeType === 1 ) {
	
											return false;
										}
									}
									// Reverse direction for :only-* (if we haven't yet done so)
									start = dir = type === "only" && !start && "nextSibling";
								}
								return true;
							}
	
							start = [ forward ? parent.firstChild : parent.lastChild ];
	
							// non-xml :nth-child(...) stores cache data on `parent`
							if ( forward && useCache ) {
	
								// Seek `elem` from a previously-cached index
	
								// ...in a gzip-friendly way
								node = parent;
								outerCache = node[ expando ] || (node[ expando ] = {});
	
								// Support: IE <9 only
								// Defend against cloned attroperties (jQuery gh-1709)
								uniqueCache = outerCache[ node.uniqueID ] ||
									(outerCache[ node.uniqueID ] = {});
	
								cache = uniqueCache[ type ] || [];
								nodeIndex = cache[ 0 ] === dirruns && cache[ 1 ];
								diff = nodeIndex && cache[ 2 ];
								node = nodeIndex && parent.childNodes[ nodeIndex ];
	
								while ( (node = ++nodeIndex && node && node[ dir ] ||
	
									// Fallback to seeking `elem` from the start
									(diff = nodeIndex = 0) || start.pop()) ) {
	
									// When found, cache indexes on `parent` and break
									if ( node.nodeType === 1 && ++diff && node === elem ) {
										uniqueCache[ type ] = [ dirruns, nodeIndex, diff ];
										break;
									}
								}
	
							} else {
								// Use previously-cached element index if available
								if ( useCache ) {
									// ...in a gzip-friendly way
									node = elem;
									outerCache = node[ expando ] || (node[ expando ] = {});
	
									// Support: IE <9 only
									// Defend against cloned attroperties (jQuery gh-1709)
									uniqueCache = outerCache[ node.uniqueID ] ||
										(outerCache[ node.uniqueID ] = {});
	
									cache = uniqueCache[ type ] || [];
									nodeIndex = cache[ 0 ] === dirruns && cache[ 1 ];
									diff = nodeIndex;
								}
	
								// xml :nth-child(...)
								// or :nth-last-child(...) or :nth(-last)?-of-type(...)
								if ( diff === false ) {
									// Use the same loop as above to seek `elem` from the start
									while ( (node = ++nodeIndex && node && node[ dir ] ||
										(diff = nodeIndex = 0) || start.pop()) ) {
	
										if ( ( ofType ?
											node.nodeName.toLowerCase() === name :
											node.nodeType === 1 ) &&
											++diff ) {
	
											// Cache the index of each encountered element
											if ( useCache ) {
												outerCache = node[ expando ] || (node[ expando ] = {});
	
												// Support: IE <9 only
												// Defend against cloned attroperties (jQuery gh-1709)
												uniqueCache = outerCache[ node.uniqueID ] ||
													(outerCache[ node.uniqueID ] = {});
	
												uniqueCache[ type ] = [ dirruns, diff ];
											}
	
											if ( node === elem ) {
												break;
											}
										}
									}
								}
							}
	
							// Incorporate the offset, then check against cycle size
							diff -= last;
							return diff === first || ( diff % first === 0 && diff / first >= 0 );
						}
					};
			},
	
			"PSEUDO": function( pseudo, argument ) {
				// pseudo-class names are case-insensitive
				// http://www.w3.org/TR/selectors/#pseudo-classes
				// Prioritize by case sensitivity in case custom pseudos are added with uppercase letters
				// Remember that setFilters inherits from pseudos
				var args,
					fn = Expr.pseudos[ pseudo ] || Expr.setFilters[ pseudo.toLowerCase() ] ||
						Sizzle.error( "unsupported pseudo: " + pseudo );
	
				// The user may use createPseudo to indicate that
				// arguments are needed to create the filter function
				// just as Sizzle does
				if ( fn[ expando ] ) {
					return fn( argument );
				}
	
				// But maintain support for old signatures
				if ( fn.length > 1 ) {
					args = [ pseudo, pseudo, "", argument ];
					return Expr.setFilters.hasOwnProperty( pseudo.toLowerCase() ) ?
						markFunction(function( seed, matches ) {
							var idx,
								matched = fn( seed, argument ),
								i = matched.length;
							while ( i-- ) {
								idx = indexOf( seed, matched[i] );
								seed[ idx ] = !( matches[ idx ] = matched[i] );
							}
						}) :
						function( elem ) {
							return fn( elem, 0, args );
						};
				}
	
				return fn;
			}
		},
	
		pseudos: {
			// Potentially complex pseudos
			"not": markFunction(function( selector ) {
				// Trim the selector passed to compile
				// to avoid treating leading and trailing
				// spaces as combinators
				var input = [],
					results = [],
					matcher = compile( selector.replace( rtrim, "$1" ) );
	
				return matcher[ expando ] ?
					markFunction(function( seed, matches, context, xml ) {
						var elem,
							unmatched = matcher( seed, null, xml, [] ),
							i = seed.length;
	
						// Match elements unmatched by `matcher`
						while ( i-- ) {
							if ( (elem = unmatched[i]) ) {
								seed[i] = !(matches[i] = elem);
							}
						}
					}) :
					function( elem, context, xml ) {
						input[0] = elem;
						matcher( input, null, xml, results );
						// Don't keep the element (issue #299)
						input[0] = null;
						return !results.pop();
					};
			}),
	
			"has": markFunction(function( selector ) {
				return function( elem ) {
					return Sizzle( selector, elem ).length > 0;
				};
			}),
	
			"contains": markFunction(function( text ) {
				text = text.replace( runescape, funescape );
				return function( elem ) {
					return ( elem.textContent || elem.innerText || getText( elem ) ).indexOf( text ) > -1;
				};
			}),
	
			// "Whether an element is represented by a :lang() selector
			// is based solely on the element's language value
			// being equal to the identifier C,
			// or beginning with the identifier C immediately followed by "-".
			// The matching of C against the element's language value is performed case-insensitively.
			// The identifier C does not have to be a valid language name."
			// http://www.w3.org/TR/selectors/#lang-pseudo
			"lang": markFunction( function( lang ) {
				// lang value must be a valid identifier
				if ( !ridentifier.test(lang || "") ) {
					Sizzle.error( "unsupported lang: " + lang );
				}
				lang = lang.replace( runescape, funescape ).toLowerCase();
				return function( elem ) {
					var elemLang;
					do {
						if ( (elemLang = documentIsHTML ?
							elem.lang :
							elem.getAttribute("xml:lang") || elem.getAttribute("lang")) ) {
	
							elemLang = elemLang.toLowerCase();
							return elemLang === lang || elemLang.indexOf( lang + "-" ) === 0;
						}
					} while ( (elem = elem.parentNode) && elem.nodeType === 1 );
					return false;
				};
			}),
	
			// Miscellaneous
			"target": function( elem ) {
				var hash = window.location && window.location.hash;
				return hash && hash.slice( 1 ) === elem.id;
			},
	
			"root": function( elem ) {
				return elem === docElem;
			},
	
			"focus": function( elem ) {
				return elem === document.activeElement && (!document.hasFocus || document.hasFocus()) && !!(elem.type || elem.href || ~elem.tabIndex);
			},
	
			// Boolean properties
			"enabled": createDisabledPseudo( false ),
			"disabled": createDisabledPseudo( true ),
	
			"checked": function( elem ) {
				// In CSS3, :checked should return both checked and selected elements
				// http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
				var nodeName = elem.nodeName.toLowerCase();
				return (nodeName === "input" && !!elem.checked) || (nodeName === "option" && !!elem.selected);
			},
	
			"selected": function( elem ) {
				// Accessing this property makes selected-by-default
				// options in Safari work properly
				if ( elem.parentNode ) {
					elem.parentNode.selectedIndex;
				}
	
				return elem.selected === true;
			},
	
			// Contents
			"empty": function( elem ) {
				// http://www.w3.org/TR/selectors/#empty-pseudo
				// :empty is negated by element (1) or content nodes (text: 3; cdata: 4; entity ref: 5),
				//   but not by others (comment: 8; processing instruction: 7; etc.)
				// nodeType < 6 works because attributes (2) do not appear as children
				for ( elem = elem.firstChild; elem; elem = elem.nextSibling ) {
					if ( elem.nodeType < 6 ) {
						return false;
					}
				}
				return true;
			},
	
			"parent": function( elem ) {
				return !Expr.pseudos["empty"]( elem );
			},
	
			// Element/input types
			"header": function( elem ) {
				return rheader.test( elem.nodeName );
			},
	
			"input": function( elem ) {
				return rinputs.test( elem.nodeName );
			},
	
			"button": function( elem ) {
				var name = elem.nodeName.toLowerCase();
				return name === "input" && elem.type === "button" || name === "button";
			},
	
			"text": function( elem ) {
				var attr;
				return elem.nodeName.toLowerCase() === "input" &&
					elem.type === "text" &&
	
					// Support: IE<8
					// New HTML5 attribute values (e.g., "search") appear with elem.type === "text"
					( (attr = elem.getAttribute("type")) == null || attr.toLowerCase() === "text" );
			},
	
			// Position-in-collection
			"first": createPositionalPseudo(function() {
				return [ 0 ];
			}),
	
			"last": createPositionalPseudo(function( matchIndexes, length ) {
				return [ length - 1 ];
			}),
	
			"eq": createPositionalPseudo(function( matchIndexes, length, argument ) {
				return [ argument < 0 ? argument + length : argument ];
			}),
	
			"even": createPositionalPseudo(function( matchIndexes, length ) {
				var i = 0;
				for ( ; i < length; i += 2 ) {
					matchIndexes.push( i );
				}
				return matchIndexes;
			}),
	
			"odd": createPositionalPseudo(function( matchIndexes, length ) {
				var i = 1;
				for ( ; i < length; i += 2 ) {
					matchIndexes.push( i );
				}
				return matchIndexes;
			}),
	
			"lt": createPositionalPseudo(function( matchIndexes, length, argument ) {
				var i = argument < 0 ? argument + length : argument;
				for ( ; --i >= 0; ) {
					matchIndexes.push( i );
				}
				return matchIndexes;
			}),
	
			"gt": createPositionalPseudo(function( matchIndexes, length, argument ) {
				var i = argument < 0 ? argument + length : argument;
				for ( ; ++i < length; ) {
					matchIndexes.push( i );
				}
				return matchIndexes;
			})
		}
	};
	
	Expr.pseudos["nth"] = Expr.pseudos["eq"];
	
	// Add button/input type pseudos
	for ( i in { radio: true, checkbox: true, file: true, password: true, image: true } ) {
		Expr.pseudos[ i ] = createInputPseudo( i );
	}
	for ( i in { submit: true, reset: true } ) {
		Expr.pseudos[ i ] = createButtonPseudo( i );
	}
	
	// Easy API for creating new setFilters
	function setFilters() {}
	setFilters.prototype = Expr.filters = Expr.pseudos;
	Expr.setFilters = new setFilters();
	
	tokenize = Sizzle.tokenize = function( selector, parseOnly ) {
		var matched, match, tokens, type,
			soFar, groups, preFilters,
			cached = tokenCache[ selector + " " ];
	
		if ( cached ) {
			return parseOnly ? 0 : cached.slice( 0 );
		}
	
		soFar = selector;
		groups = [];
		preFilters = Expr.preFilter;
	
		while ( soFar ) {
	
			// Comma and first run
			if ( !matched || (match = rcomma.exec( soFar )) ) {
				if ( match ) {
					// Don't consume trailing commas as valid
					soFar = soFar.slice( match[0].length ) || soFar;
				}
				groups.push( (tokens = []) );
			}
	
			matched = false;
	
			// Combinators
			if ( (match = rcombinators.exec( soFar )) ) {
				matched = match.shift();
				tokens.push({
					value: matched,
					// Cast descendant combinators to space
					type: match[0].replace( rtrim, " " )
				});
				soFar = soFar.slice( matched.length );
			}
	
			// Filters
			for ( type in Expr.filter ) {
				if ( (match = matchExpr[ type ].exec( soFar )) && (!preFilters[ type ] ||
					(match = preFilters[ type ]( match ))) ) {
					matched = match.shift();
					tokens.push({
						value: matched,
						type: type,
						matches: match
					});
					soFar = soFar.slice( matched.length );
				}
			}
	
			if ( !matched ) {
				break;
			}
		}
	
		// Return the length of the invalid excess
		// if we're just parsing
		// Otherwise, throw an error or return tokens
		return parseOnly ?
			soFar.length :
			soFar ?
				Sizzle.error( selector ) :
				// Cache the tokens
				tokenCache( selector, groups ).slice( 0 );
	};
	
	function toSelector( tokens ) {
		var i = 0,
			len = tokens.length,
			selector = "";
		for ( ; i < len; i++ ) {
			selector += tokens[i].value;
		}
		return selector;
	}
	
	function addCombinator( matcher, combinator, base ) {
		var dir = combinator.dir,
			skip = combinator.next,
			key = skip || dir,
			checkNonElements = base && key === "parentNode",
			doneName = done++;
	
		return combinator.first ?
			// Check against closest ancestor/preceding element
			function( elem, context, xml ) {
				while ( (elem = elem[ dir ]) ) {
					if ( elem.nodeType === 1 || checkNonElements ) {
						return matcher( elem, context, xml );
					}
				}
				return false;
			} :
	
			// Check against all ancestor/preceding elements
			function( elem, context, xml ) {
				var oldCache, uniqueCache, outerCache,
					newCache = [ dirruns, doneName ];
	
				// We can't set arbitrary data on XML nodes, so they don't benefit from combinator caching
				if ( xml ) {
					while ( (elem = elem[ dir ]) ) {
						if ( elem.nodeType === 1 || checkNonElements ) {
							if ( matcher( elem, context, xml ) ) {
								return true;
							}
						}
					}
				} else {
					while ( (elem = elem[ dir ]) ) {
						if ( elem.nodeType === 1 || checkNonElements ) {
							outerCache = elem[ expando ] || (elem[ expando ] = {});
	
							// Support: IE <9 only
							// Defend against cloned attroperties (jQuery gh-1709)
							uniqueCache = outerCache[ elem.uniqueID ] || (outerCache[ elem.uniqueID ] = {});
	
							if ( skip && skip === elem.nodeName.toLowerCase() ) {
								elem = elem[ dir ] || elem;
							} else if ( (oldCache = uniqueCache[ key ]) &&
								oldCache[ 0 ] === dirruns && oldCache[ 1 ] === doneName ) {
	
								// Assign to newCache so results back-propagate to previous elements
								return (newCache[ 2 ] = oldCache[ 2 ]);
							} else {
								// Reuse newcache so results back-propagate to previous elements
								uniqueCache[ key ] = newCache;
	
								// A match means we're done; a fail means we have to keep checking
								if ( (newCache[ 2 ] = matcher( elem, context, xml )) ) {
									return true;
								}
							}
						}
					}
				}
				return false;
			};
	}
	
	function elementMatcher( matchers ) {
		return matchers.length > 1 ?
			function( elem, context, xml ) {
				var i = matchers.length;
				while ( i-- ) {
					if ( !matchers[i]( elem, context, xml ) ) {
						return false;
					}
				}
				return true;
			} :
			matchers[0];
	}
	
	function multipleContexts( selector, contexts, results ) {
		var i = 0,
			len = contexts.length;
		for ( ; i < len; i++ ) {
			Sizzle( selector, contexts[i], results );
		}
		return results;
	}
	
	function condense( unmatched, map, filter, context, xml ) {
		var elem,
			newUnmatched = [],
			i = 0,
			len = unmatched.length,
			mapped = map != null;
	
		for ( ; i < len; i++ ) {
			if ( (elem = unmatched[i]) ) {
				if ( !filter || filter( elem, context, xml ) ) {
					newUnmatched.push( elem );
					if ( mapped ) {
						map.push( i );
					}
				}
			}
		}
	
		return newUnmatched;
	}
	
	function setMatcher( preFilter, selector, matcher, postFilter, postFinder, postSelector ) {
		if ( postFilter && !postFilter[ expando ] ) {
			postFilter = setMatcher( postFilter );
		}
		if ( postFinder && !postFinder[ expando ] ) {
			postFinder = setMatcher( postFinder, postSelector );
		}
		return markFunction(function( seed, results, context, xml ) {
			var temp, i, elem,
				preMap = [],
				postMap = [],
				preexisting = results.length,
	
				// Get initial elements from seed or context
				elems = seed || multipleContexts( selector || "*", context.nodeType ? [ context ] : context, [] ),
	
				// Prefilter to get matcher input, preserving a map for seed-results synchronization
				matcherIn = preFilter && ( seed || !selector ) ?
					condense( elems, preMap, preFilter, context, xml ) :
					elems,
	
				matcherOut = matcher ?
					// If we have a postFinder, or filtered seed, or non-seed postFilter or preexisting results,
					postFinder || ( seed ? preFilter : preexisting || postFilter ) ?
	
						// ...intermediate processing is necessary
						[] :
	
						// ...otherwise use results directly
						results :
					matcherIn;
	
			// Find primary matches
			if ( matcher ) {
				matcher( matcherIn, matcherOut, context, xml );
			}
	
			// Apply postFilter
			if ( postFilter ) {
				temp = condense( matcherOut, postMap );
				postFilter( temp, [], context, xml );
	
				// Un-match failing elements by moving them back to matcherIn
				i = temp.length;
				while ( i-- ) {
					if ( (elem = temp[i]) ) {
						matcherOut[ postMap[i] ] = !(matcherIn[ postMap[i] ] = elem);
					}
				}
			}
	
			if ( seed ) {
				if ( postFinder || preFilter ) {
					if ( postFinder ) {
						// Get the final matcherOut by condensing this intermediate into postFinder contexts
						temp = [];
						i = matcherOut.length;
						while ( i-- ) {
							if ( (elem = matcherOut[i]) ) {
								// Restore matcherIn since elem is not yet a final match
								temp.push( (matcherIn[i] = elem) );
							}
						}
						postFinder( null, (matcherOut = []), temp, xml );
					}
	
					// Move matched elements from seed to results to keep them synchronized
					i = matcherOut.length;
					while ( i-- ) {
						if ( (elem = matcherOut[i]) &&
							(temp = postFinder ? indexOf( seed, elem ) : preMap[i]) > -1 ) {
	
							seed[temp] = !(results[temp] = elem);
						}
					}
				}
	
			// Add elements to results, through postFinder if defined
			} else {
				matcherOut = condense(
					matcherOut === results ?
						matcherOut.splice( preexisting, matcherOut.length ) :
						matcherOut
				);
				if ( postFinder ) {
					postFinder( null, results, matcherOut, xml );
				} else {
					push.apply( results, matcherOut );
				}
			}
		});
	}
	
	function matcherFromTokens( tokens ) {
		var checkContext, matcher, j,
			len = tokens.length,
			leadingRelative = Expr.relative[ tokens[0].type ],
			implicitRelative = leadingRelative || Expr.relative[" "],
			i = leadingRelative ? 1 : 0,
	
			// The foundational matcher ensures that elements are reachable from top-level context(s)
			matchContext = addCombinator( function( elem ) {
				return elem === checkContext;
			}, implicitRelative, true ),
			matchAnyContext = addCombinator( function( elem ) {
				return indexOf( checkContext, elem ) > -1;
			}, implicitRelative, true ),
			matchers = [ function( elem, context, xml ) {
				var ret = ( !leadingRelative && ( xml || context !== outermostContext ) ) || (
					(checkContext = context).nodeType ?
						matchContext( elem, context, xml ) :
						matchAnyContext( elem, context, xml ) );
				// Avoid hanging onto element (issue #299)
				checkContext = null;
				return ret;
			} ];
	
		for ( ; i < len; i++ ) {
			if ( (matcher = Expr.relative[ tokens[i].type ]) ) {
				matchers = [ addCombinator(elementMatcher( matchers ), matcher) ];
			} else {
				matcher = Expr.filter[ tokens[i].type ].apply( null, tokens[i].matches );
	
				// Return special upon seeing a positional matcher
				if ( matcher[ expando ] ) {
					// Find the next relative operator (if any) for proper handling
					j = ++i;
					for ( ; j < len; j++ ) {
						if ( Expr.relative[ tokens[j].type ] ) {
							break;
						}
					}
					return setMatcher(
						i > 1 && elementMatcher( matchers ),
						i > 1 && toSelector(
							// If the preceding token was a descendant combinator, insert an implicit any-element `*`
							tokens.slice( 0, i - 1 ).concat({ value: tokens[ i - 2 ].type === " " ? "*" : "" })
						).replace( rtrim, "$1" ),
						matcher,
						i < j && matcherFromTokens( tokens.slice( i, j ) ),
						j < len && matcherFromTokens( (tokens = tokens.slice( j )) ),
						j < len && toSelector( tokens )
					);
				}
				matchers.push( matcher );
			}
		}
	
		return elementMatcher( matchers );
	}
	
	function matcherFromGroupMatchers( elementMatchers, setMatchers ) {
		var bySet = setMatchers.length > 0,
			byElement = elementMatchers.length > 0,
			superMatcher = function( seed, context, xml, results, outermost ) {
				var elem, j, matcher,
					matchedCount = 0,
					i = "0",
					unmatched = seed && [],
					setMatched = [],
					contextBackup = outermostContext,
					// We must always have either seed elements or outermost context
					elems = seed || byElement && Expr.find["TAG"]( "*", outermost ),
					// Use integer dirruns iff this is the outermost matcher
					dirrunsUnique = (dirruns += contextBackup == null ? 1 : Math.random() || 0.1),
					len = elems.length;
	
				if ( outermost ) {
					outermostContext = context === document || context || outermost;
				}
	
				// Add elements passing elementMatchers directly to results
				// Support: IE<9, Safari
				// Tolerate NodeList properties (IE: "length"; Safari: <number>) matching elements by id
				for ( ; i !== len && (elem = elems[i]) != null; i++ ) {
					if ( byElement && elem ) {
						j = 0;
						if ( !context && elem.ownerDocument !== document ) {
							setDocument( elem );
							xml = !documentIsHTML;
						}
						while ( (matcher = elementMatchers[j++]) ) {
							if ( matcher( elem, context || document, xml) ) {
								results.push( elem );
								break;
							}
						}
						if ( outermost ) {
							dirruns = dirrunsUnique;
						}
					}
	
					// Track unmatched elements for set filters
					if ( bySet ) {
						// They will have gone through all possible matchers
						if ( (elem = !matcher && elem) ) {
							matchedCount--;
						}
	
						// Lengthen the array for every element, matched or not
						if ( seed ) {
							unmatched.push( elem );
						}
					}
				}
	
				// `i` is now the count of elements visited above, and adding it to `matchedCount`
				// makes the latter nonnegative.
				matchedCount += i;
	
				// Apply set filters to unmatched elements
				// NOTE: This can be skipped if there are no unmatched elements (i.e., `matchedCount`
				// equals `i`), unless we didn't visit _any_ elements in the above loop because we have
				// no element matchers and no seed.
				// Incrementing an initially-string "0" `i` allows `i` to remain a string only in that
				// case, which will result in a "00" `matchedCount` that differs from `i` but is also
				// numerically zero.
				if ( bySet && i !== matchedCount ) {
					j = 0;
					while ( (matcher = setMatchers[j++]) ) {
						matcher( unmatched, setMatched, context, xml );
					}
	
					if ( seed ) {
						// Reintegrate element matches to eliminate the need for sorting
						if ( matchedCount > 0 ) {
							while ( i-- ) {
								if ( !(unmatched[i] || setMatched[i]) ) {
									setMatched[i] = pop.call( results );
								}
							}
						}
	
						// Discard index placeholder values to get only actual matches
						setMatched = condense( setMatched );
					}
	
					// Add matches to results
					push.apply( results, setMatched );
	
					// Seedless set matches succeeding multiple successful matchers stipulate sorting
					if ( outermost && !seed && setMatched.length > 0 &&
						( matchedCount + setMatchers.length ) > 1 ) {
	
						Sizzle.uniqueSort( results );
					}
				}
	
				// Override manipulation of globals by nested matchers
				if ( outermost ) {
					dirruns = dirrunsUnique;
					outermostContext = contextBackup;
				}
	
				return unmatched;
			};
	
		return bySet ?
			markFunction( superMatcher ) :
			superMatcher;
	}
	
	compile = Sizzle.compile = function( selector, match /* Internal Use Only */ ) {
		var i,
			setMatchers = [],
			elementMatchers = [],
			cached = compilerCache[ selector + " " ];
	
		if ( !cached ) {
			// Generate a function of recursive functions that can be used to check each element
			if ( !match ) {
				match = tokenize( selector );
			}
			i = match.length;
			while ( i-- ) {
				cached = matcherFromTokens( match[i] );
				if ( cached[ expando ] ) {
					setMatchers.push( cached );
				} else {
					elementMatchers.push( cached );
				}
			}
	
			// Cache the compiled function
			cached = compilerCache( selector, matcherFromGroupMatchers( elementMatchers, setMatchers ) );
	
			// Save selector and tokenization
			cached.selector = selector;
		}
		return cached;
	};
	
	/**
	 * A low-level selection function that works with Sizzle's compiled
	 *  selector functions
	 * @param {String|Function} selector A selector or a pre-compiled
	 *  selector function built with Sizzle.compile
	 * @param {Element} context
	 * @param {Array} [results]
	 * @param {Array} [seed] A set of elements to match against
	 */
	select = Sizzle.select = function( selector, context, results, seed ) {
		var i, tokens, token, type, find,
			compiled = typeof selector === "function" && selector,
			match = !seed && tokenize( (selector = compiled.selector || selector) );
	
		results = results || [];
	
		// Try to minimize operations if there is only one selector in the list and no seed
		// (the latter of which guarantees us context)
		if ( match.length === 1 ) {
	
			// Reduce context if the leading compound selector is an ID
			tokens = match[0] = match[0].slice( 0 );
			if ( tokens.length > 2 && (token = tokens[0]).type === "ID" &&
					context.nodeType === 9 && documentIsHTML && Expr.relative[ tokens[1].type ] ) {
	
				context = ( Expr.find["ID"]( token.matches[0].replace(runescape, funescape), context ) || [] )[0];
				if ( !context ) {
					return results;
	
				// Precompiled matchers will still verify ancestry, so step up a level
				} else if ( compiled ) {
					context = context.parentNode;
				}
	
				selector = selector.slice( tokens.shift().value.length );
			}
	
			// Fetch a seed set for right-to-left matching
			i = matchExpr["needsContext"].test( selector ) ? 0 : tokens.length;
			while ( i-- ) {
				token = tokens[i];
	
				// Abort if we hit a combinator
				if ( Expr.relative[ (type = token.type) ] ) {
					break;
				}
				if ( (find = Expr.find[ type ]) ) {
					// Search, expanding context for leading sibling combinators
					if ( (seed = find(
						token.matches[0].replace( runescape, funescape ),
						rsibling.test( tokens[0].type ) && testContext( context.parentNode ) || context
					)) ) {
	
						// If seed is empty or no tokens remain, we can return early
						tokens.splice( i, 1 );
						selector = seed.length && toSelector( tokens );
						if ( !selector ) {
							push.apply( results, seed );
							return results;
						}
	
						break;
					}
				}
			}
		}
	
		// Compile and execute a filtering function if one is not provided
		// Provide `match` to avoid retokenization if we modified the selector above
		( compiled || compile( selector, match ) )(
			seed,
			context,
			!documentIsHTML,
			results,
			!context || rsibling.test( selector ) && testContext( context.parentNode ) || context
		);
		return results;
	};
	
	// One-time assignments
	
	// Sort stability
	support.sortStable = expando.split("").sort( sortOrder ).join("") === expando;
	
	// Support: Chrome 14-35+
	// Always assume duplicates if they aren't passed to the comparison function
	support.detectDuplicates = !!hasDuplicate;
	
	// Initialize against the default document
	setDocument();
	
	// Support: Webkit<537.32 - Safari 6.0.3/Chrome 25 (fixed in Chrome 27)
	// Detached nodes confoundingly follow *each other*
	support.sortDetached = assert(function( el ) {
		// Should return 1, but returns 4 (following)
		return el.compareDocumentPosition( document.createElement("fieldset") ) & 1;
	});
	
	// Support: IE<8
	// Prevent attribute/property "interpolation"
	// https://msdn.microsoft.com/en-us/library/ms536429%28VS.85%29.aspx
	if ( !assert(function( el ) {
		el.innerHTML = "<a href='#'></a>";
		return el.firstChild.getAttribute("href") === "#" ;
	}) ) {
		addHandle( "type|href|height|width", function( elem, name, isXML ) {
			if ( !isXML ) {
				return elem.getAttribute( name, name.toLowerCase() === "type" ? 1 : 2 );
			}
		});
	}
	
	// Support: IE<9
	// Use defaultValue in place of getAttribute("value")
	if ( !support.attributes || !assert(function( el ) {
		el.innerHTML = "<input/>";
		el.firstChild.setAttribute( "value", "" );
		return el.firstChild.getAttribute( "value" ) === "";
	}) ) {
		addHandle( "value", function( elem, name, isXML ) {
			if ( !isXML && elem.nodeName.toLowerCase() === "input" ) {
				return elem.defaultValue;
			}
		});
	}
	
	// Support: IE<9
	// Use getAttributeNode to fetch booleans when getAttribute lies
	if ( !assert(function( el ) {
		return el.getAttribute("disabled") == null;
	}) ) {
		addHandle( booleans, function( elem, name, isXML ) {
			var val;
			if ( !isXML ) {
				return elem[ name ] === true ? name.toLowerCase() :
						(val = elem.getAttributeNode( name )) && val.specified ?
						val.value :
					null;
			}
		});
	}
	
	return Sizzle;
	
	})( window );
	
	
	
	jQuery.find = Sizzle;
	jQuery.expr = Sizzle.selectors;
	
	// Deprecated
	jQuery.expr[ ":" ] = jQuery.expr.pseudos;
	jQuery.uniqueSort = jQuery.unique = Sizzle.uniqueSort;
	jQuery.text = Sizzle.getText;
	jQuery.isXMLDoc = Sizzle.isXML;
	jQuery.contains = Sizzle.contains;
	jQuery.escapeSelector = Sizzle.escape;
	
	
	
	
	var dir = function( elem, dir, until ) {
		var matched = [],
			truncate = until !== undefined;
	
		while ( ( elem = elem[ dir ] ) && elem.nodeType !== 9 ) {
			if ( elem.nodeType === 1 ) {
				if ( truncate && jQuery( elem ).is( until ) ) {
					break;
				}
				matched.push( elem );
			}
		}
		return matched;
	};
	
	
	var siblings = function( n, elem ) {
		var matched = [];
	
		for ( ; n; n = n.nextSibling ) {
			if ( n.nodeType === 1 && n !== elem ) {
				matched.push( n );
			}
		}
	
		return matched;
	};
	
	
	var rneedsContext = jQuery.expr.match.needsContext;
	
	var rsingleTag = ( /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i );
	
	
	
	var risSimple = /^.[^:#\[\.,]*$/;
	
	// Implement the identical functionality for filter and not
	function winnow( elements, qualifier, not ) {
		if ( jQuery.isFunction( qualifier ) ) {
			return jQuery.grep( elements, function( elem, i ) {
				return !!qualifier.call( elem, i, elem ) !== not;
			} );
		}
	
		// Single element
		if ( qualifier.nodeType ) {
			return jQuery.grep( elements, function( elem ) {
				return ( elem === qualifier ) !== not;
			} );
		}
	
		// Arraylike of elements (jQuery, arguments, Array)
		if ( typeof qualifier !== "string" ) {
			return jQuery.grep( elements, function( elem ) {
				return ( indexOf.call( qualifier, elem ) > -1 ) !== not;
			} );
		}
	
		// Simple selector that can be filtered directly, removing non-Elements
		if ( risSimple.test( qualifier ) ) {
			return jQuery.filter( qualifier, elements, not );
		}
	
		// Complex selector, compare the two sets, removing non-Elements
		qualifier = jQuery.filter( qualifier, elements );
		return jQuery.grep( elements, function( elem ) {
			return ( indexOf.call( qualifier, elem ) > -1 ) !== not && elem.nodeType === 1;
		} );
	}
	
	jQuery.filter = function( expr, elems, not ) {
		var elem = elems[ 0 ];
	
		if ( not ) {
			expr = ":not(" + expr + ")";
		}
	
		if ( elems.length === 1 && elem.nodeType === 1 ) {
			return jQuery.find.matchesSelector( elem, expr ) ? [ elem ] : [];
		}
	
		return jQuery.find.matches( expr, jQuery.grep( elems, function( elem ) {
			return elem.nodeType === 1;
		} ) );
	};
	
	jQuery.fn.extend( {
		find: function( selector ) {
			var i, ret,
				len = this.length,
				self = this;
	
			if ( typeof selector !== "string" ) {
				return this.pushStack( jQuery( selector ).filter( function() {
					for ( i = 0; i < len; i++ ) {
						if ( jQuery.contains( self[ i ], this ) ) {
							return true;
						}
					}
				} ) );
			}
	
			ret = this.pushStack( [] );
	
			for ( i = 0; i < len; i++ ) {
				jQuery.find( selector, self[ i ], ret );
			}
	
			return len > 1 ? jQuery.uniqueSort( ret ) : ret;
		},
		filter: function( selector ) {
			return this.pushStack( winnow( this, selector || [], false ) );
		},
		not: function( selector ) {
			return this.pushStack( winnow( this, selector || [], true ) );
		},
		is: function( selector ) {
			return !!winnow(
				this,
	
				// If this is a positional/relative selector, check membership in the returned set
				// so $("p:first").is("p:last") won't return true for a doc with two "p".
				typeof selector === "string" && rneedsContext.test( selector ) ?
					jQuery( selector ) :
					selector || [],
				false
			).length;
		}
	} );
	
	
	// Initialize a jQuery object
	
	
	// A central reference to the root jQuery(document)
	var rootjQuery,
	
		// A simple way to check for HTML strings
		// Prioritize #id over <tag> to avoid XSS via location.hash (#9521)
		// Strict HTML recognition (#11290: must start with <)
		// Shortcut simple #id case for speed
		rquickExpr = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/,
	
		init = jQuery.fn.init = function( selector, context, root ) {
			var match, elem;
	
			// HANDLE: $(""), $(null), $(undefined), $(false)
			if ( !selector ) {
				return this;
			}
	
			// Method init() accepts an alternate rootjQuery
			// so migrate can support jQuery.sub (gh-2101)
			root = root || rootjQuery;
	
			// Handle HTML strings
			if ( typeof selector === "string" ) {
				if ( selector[ 0 ] === "<" &&
					selector[ selector.length - 1 ] === ">" &&
					selector.length >= 3 ) {
	
					// Assume that strings that start and end with <> are HTML and skip the regex check
					match = [ null, selector, null ];
	
				} else {
					match = rquickExpr.exec( selector );
				}
	
				// Match html or make sure no context is specified for #id
				if ( match && ( match[ 1 ] || !context ) ) {
	
					// HANDLE: $(html) -> $(array)
					if ( match[ 1 ] ) {
						context = context instanceof jQuery ? context[ 0 ] : context;
	
						// Option to run scripts is true for back-compat
						// Intentionally let the error be thrown if parseHTML is not present
						jQuery.merge( this, jQuery.parseHTML(
							match[ 1 ],
							context && context.nodeType ? context.ownerDocument || context : document,
							true
						) );
	
						// HANDLE: $(html, props)
						if ( rsingleTag.test( match[ 1 ] ) && jQuery.isPlainObject( context ) ) {
							for ( match in context ) {
	
								// Properties of context are called as methods if possible
								if ( jQuery.isFunction( this[ match ] ) ) {
									this[ match ]( context[ match ] );
	
								// ...and otherwise set as attributes
								} else {
									this.attr( match, context[ match ] );
								}
							}
						}
	
						return this;
	
					// HANDLE: $(#id)
					} else {
						elem = document.getElementById( match[ 2 ] );
	
						if ( elem ) {
	
							// Inject the element directly into the jQuery object
							this[ 0 ] = elem;
							this.length = 1;
						}
						return this;
					}
	
				// HANDLE: $(expr, $(...))
				} else if ( !context || context.jquery ) {
					return ( context || root ).find( selector );
	
				// HANDLE: $(expr, context)
				// (which is just equivalent to: $(context).find(expr)
				} else {
					return this.constructor( context ).find( selector );
				}
	
			// HANDLE: $(DOMElement)
			} else if ( selector.nodeType ) {
				this[ 0 ] = selector;
				this.length = 1;
				return this;
	
			// HANDLE: $(function)
			// Shortcut for document ready
			} else if ( jQuery.isFunction( selector ) ) {
				return root.ready !== undefined ?
					root.ready( selector ) :
	
					// Execute immediately if ready is not present
					selector( jQuery );
			}
	
			return jQuery.makeArray( selector, this );
		};
	
	// Give the init function the jQuery prototype for later instantiation
	init.prototype = jQuery.fn;
	
	// Initialize central reference
	rootjQuery = jQuery( document );
	
	
	var rparentsprev = /^(?:parents|prev(?:Until|All))/,
	
		// Methods guaranteed to produce a unique set when starting from a unique set
		guaranteedUnique = {
			children: true,
			contents: true,
			next: true,
			prev: true
		};
	
	jQuery.fn.extend( {
		has: function( target ) {
			var targets = jQuery( target, this ),
				l = targets.length;
	
			return this.filter( function() {
				var i = 0;
				for ( ; i < l; i++ ) {
					if ( jQuery.contains( this, targets[ i ] ) ) {
						return true;
					}
				}
			} );
		},
	
		closest: function( selectors, context ) {
			var cur,
				i = 0,
				l = this.length,
				matched = [],
				targets = typeof selectors !== "string" && jQuery( selectors );
	
			// Positional selectors never match, since there's no _selection_ context
			if ( !rneedsContext.test( selectors ) ) {
				for ( ; i < l; i++ ) {
					for ( cur = this[ i ]; cur && cur !== context; cur = cur.parentNode ) {
	
						// Always skip document fragments
						if ( cur.nodeType < 11 && ( targets ?
							targets.index( cur ) > -1 :
	
							// Don't pass non-elements to Sizzle
							cur.nodeType === 1 &&
								jQuery.find.matchesSelector( cur, selectors ) ) ) {
	
							matched.push( cur );
							break;
						}
					}
				}
			}
	
			return this.pushStack( matched.length > 1 ? jQuery.uniqueSort( matched ) : matched );
		},
	
		// Determine the position of an element within the set
		index: function( elem ) {
	
			// No argument, return index in parent
			if ( !elem ) {
				return ( this[ 0 ] && this[ 0 ].parentNode ) ? this.first().prevAll().length : -1;
			}
	
			// Index in selector
			if ( typeof elem === "string" ) {
				return indexOf.call( jQuery( elem ), this[ 0 ] );
			}
	
			// Locate the position of the desired element
			return indexOf.call( this,
	
				// If it receives a jQuery object, the first element is used
				elem.jquery ? elem[ 0 ] : elem
			);
		},
	
		add: function( selector, context ) {
			return this.pushStack(
				jQuery.uniqueSort(
					jQuery.merge( this.get(), jQuery( selector, context ) )
				)
			);
		},
	
		addBack: function( selector ) {
			return this.add( selector == null ?
				this.prevObject : this.prevObject.filter( selector )
			);
		}
	} );
	
	function sibling( cur, dir ) {
		while ( ( cur = cur[ dir ] ) && cur.nodeType !== 1 ) {}
		return cur;
	}
	
	jQuery.each( {
		parent: function( elem ) {
			var parent = elem.parentNode;
			return parent && parent.nodeType !== 11 ? parent : null;
		},
		parents: function( elem ) {
			return dir( elem, "parentNode" );
		},
		parentsUntil: function( elem, i, until ) {
			return dir( elem, "parentNode", until );
		},
		next: function( elem ) {
			return sibling( elem, "nextSibling" );
		},
		prev: function( elem ) {
			return sibling( elem, "previousSibling" );
		},
		nextAll: function( elem ) {
			return dir( elem, "nextSibling" );
		},
		prevAll: function( elem ) {
			return dir( elem, "previousSibling" );
		},
		nextUntil: function( elem, i, until ) {
			return dir( elem, "nextSibling", until );
		},
		prevUntil: function( elem, i, until ) {
			return dir( elem, "previousSibling", until );
		},
		siblings: function( elem ) {
			return siblings( ( elem.parentNode || {} ).firstChild, elem );
		},
		children: function( elem ) {
			return siblings( elem.firstChild );
		},
		contents: function( elem ) {
			return elem.contentDocument || jQuery.merge( [], elem.childNodes );
		}
	}, function( name, fn ) {
		jQuery.fn[ name ] = function( until, selector ) {
			var matched = jQuery.map( this, fn, until );
	
			if ( name.slice( -5 ) !== "Until" ) {
				selector = until;
			}
	
			if ( selector && typeof selector === "string" ) {
				matched = jQuery.filter( selector, matched );
			}
	
			if ( this.length > 1 ) {
	
				// Remove duplicates
				if ( !guaranteedUnique[ name ] ) {
					jQuery.uniqueSort( matched );
				}
	
				// Reverse order for parents* and prev-derivatives
				if ( rparentsprev.test( name ) ) {
					matched.reverse();
				}
			}
	
			return this.pushStack( matched );
		};
	} );
	var rnothtmlwhite = ( /[^\x20\t\r\n\f]+/g );
	
	
	
	// Convert String-formatted options into Object-formatted ones
	function createOptions( options ) {
		var object = {};
		jQuery.each( options.match( rnothtmlwhite ) || [], function( _, flag ) {
			object[ flag ] = true;
		} );
		return object;
	}
	
	/*
	 * Create a callback list using the following parameters:
	 *
	 *	options: an optional list of space-separated options that will change how
	 *			the callback list behaves or a more traditional option object
	 *
	 * By default a callback list will act like an event callback list and can be
	 * "fired" multiple times.
	 *
	 * Possible options:
	 *
	 *	once:			will ensure the callback list can only be fired once (like a Deferred)
	 *
	 *	memory:			will keep track of previous values and will call any callback added
	 *					after the list has been fired right away with the latest "memorized"
	 *					values (like a Deferred)
	 *
	 *	unique:			will ensure a callback can only be added once (no duplicate in the list)
	 *
	 *	stopOnFalse:	interrupt callings when a callback returns false
	 *
	 */
	jQuery.Callbacks = function( options ) {
	
		// Convert options from String-formatted to Object-formatted if needed
		// (we check in cache first)
		options = typeof options === "string" ?
			createOptions( options ) :
			jQuery.extend( {}, options );
	
		var // Flag to know if list is currently firing
			firing,
	
			// Last fire value for non-forgettable lists
			memory,
	
			// Flag to know if list was already fired
			fired,
	
			// Flag to prevent firing
			locked,
	
			// Actual callback list
			list = [],
	
			// Queue of execution data for repeatable lists
			queue = [],
	
			// Index of currently firing callback (modified by add/remove as needed)
			firingIndex = -1,
	
			// Fire callbacks
			fire = function() {
	
				// Enforce single-firing
				locked = options.once;
	
				// Execute callbacks for all pending executions,
				// respecting firingIndex overrides and runtime changes
				fired = firing = true;
				for ( ; queue.length; firingIndex = -1 ) {
					memory = queue.shift();
					while ( ++firingIndex < list.length ) {
	
						// Run callback and check for early termination
						if ( list[ firingIndex ].apply( memory[ 0 ], memory[ 1 ] ) === false &&
							options.stopOnFalse ) {
	
							// Jump to end and forget the data so .add doesn't re-fire
							firingIndex = list.length;
							memory = false;
						}
					}
				}
	
				// Forget the data if we're done with it
				if ( !options.memory ) {
					memory = false;
				}
	
				firing = false;
	
				// Clean up if we're done firing for good
				if ( locked ) {
	
					// Keep an empty list if we have data for future add calls
					if ( memory ) {
						list = [];
	
					// Otherwise, this object is spent
					} else {
						list = "";
					}
				}
			},
	
			// Actual Callbacks object
			self = {
	
				// Add a callback or a collection of callbacks to the list
				add: function() {
					if ( list ) {
	
						// If we have memory from a past run, we should fire after adding
						if ( memory && !firing ) {
							firingIndex = list.length - 1;
							queue.push( memory );
						}
	
						( function add( args ) {
							jQuery.each( args, function( _, arg ) {
								if ( jQuery.isFunction( arg ) ) {
									if ( !options.unique || !self.has( arg ) ) {
										list.push( arg );
									}
								} else if ( arg && arg.length && jQuery.type( arg ) !== "string" ) {
	
									// Inspect recursively
									add( arg );
								}
							} );
						} )( arguments );
	
						if ( memory && !firing ) {
							fire();
						}
					}
					return this;
				},
	
				// Remove a callback from the list
				remove: function() {
					jQuery.each( arguments, function( _, arg ) {
						var index;
						while ( ( index = jQuery.inArray( arg, list, index ) ) > -1 ) {
							list.splice( index, 1 );
	
							// Handle firing indexes
							if ( index <= firingIndex ) {
								firingIndex--;
							}
						}
					} );
					return this;
				},
	
				// Check if a given callback is in the list.
				// If no argument is given, return whether or not list has callbacks attached.
				has: function( fn ) {
					return fn ?
						jQuery.inArray( fn, list ) > -1 :
						list.length > 0;
				},
	
				// Remove all callbacks from the list
				empty: function() {
					if ( list ) {
						list = [];
					}
					return this;
				},
	
				// Disable .fire and .add
				// Abort any current/pending executions
				// Clear all callbacks and values
				disable: function() {
					locked = queue = [];
					list = memory = "";
					return this;
				},
				disabled: function() {
					return !list;
				},
	
				// Disable .fire
				// Also disable .add unless we have memory (since it would have no effect)
				// Abort any pending executions
				lock: function() {
					locked = queue = [];
					if ( !memory && !firing ) {
						list = memory = "";
					}
					return this;
				},
				locked: function() {
					return !!locked;
				},
	
				// Call all callbacks with the given context and arguments
				fireWith: function( context, args ) {
					if ( !locked ) {
						args = args || [];
						args = [ context, args.slice ? args.slice() : args ];
						queue.push( args );
						if ( !firing ) {
							fire();
						}
					}
					return this;
				},
	
				// Call all the callbacks with the given arguments
				fire: function() {
					self.fireWith( this, arguments );
					return this;
				},
	
				// To know if the callbacks have already been called at least once
				fired: function() {
					return !!fired;
				}
			};
	
		return self;
	};
	
	
	function Identity( v ) {
		return v;
	}
	function Thrower( ex ) {
		throw ex;
	}
	
	function adoptValue( value, resolve, reject ) {
		var method;
	
		try {
	
			// Check for promise aspect first to privilege synchronous behavior
			if ( value && jQuery.isFunction( ( method = value.promise ) ) ) {
				method.call( value ).done( resolve ).fail( reject );
	
			// Other thenables
			} else if ( value && jQuery.isFunction( ( method = value.then ) ) ) {
				method.call( value, resolve, reject );
	
			// Other non-thenables
			} else {
	
				// Support: Android 4.0 only
				// Strict mode functions invoked without .call/.apply get global-object context
				resolve.call( undefined, value );
			}
	
		// For Promises/A+, convert exceptions into rejections
		// Since jQuery.when doesn't unwrap thenables, we can skip the extra checks appearing in
		// Deferred#then to conditionally suppress rejection.
		} catch ( value ) {
	
			// Support: Android 4.0 only
			// Strict mode functions invoked without .call/.apply get global-object context
			reject.call( undefined, value );
		}
	}
	
	jQuery.extend( {
	
		Deferred: function( func ) {
			var tuples = [
	
					// action, add listener, callbacks,
					// ... .then handlers, argument index, [final state]
					[ "notify", "progress", jQuery.Callbacks( "memory" ),
						jQuery.Callbacks( "memory" ), 2 ],
					[ "resolve", "done", jQuery.Callbacks( "once memory" ),
						jQuery.Callbacks( "once memory" ), 0, "resolved" ],
					[ "reject", "fail", jQuery.Callbacks( "once memory" ),
						jQuery.Callbacks( "once memory" ), 1, "rejected" ]
				],
				state = "pending",
				promise = {
					state: function() {
						return state;
					},
					always: function() {
						deferred.done( arguments ).fail( arguments );
						return this;
					},
					"catch": function( fn ) {
						return promise.then( null, fn );
					},
	
					// Keep pipe for back-compat
					pipe: function( /* fnDone, fnFail, fnProgress */ ) {
						var fns = arguments;
	
						return jQuery.Deferred( function( newDefer ) {
							jQuery.each( tuples, function( i, tuple ) {
	
								// Map tuples (progress, done, fail) to arguments (done, fail, progress)
								var fn = jQuery.isFunction( fns[ tuple[ 4 ] ] ) && fns[ tuple[ 4 ] ];
	
								// deferred.progress(function() { bind to newDefer or newDefer.notify })
								// deferred.done(function() { bind to newDefer or newDefer.resolve })
								// deferred.fail(function() { bind to newDefer or newDefer.reject })
								deferred[ tuple[ 1 ] ]( function() {
									var returned = fn && fn.apply( this, arguments );
									if ( returned && jQuery.isFunction( returned.promise ) ) {
										returned.promise()
											.progress( newDefer.notify )
											.done( newDefer.resolve )
											.fail( newDefer.reject );
									} else {
										newDefer[ tuple[ 0 ] + "With" ](
											this,
											fn ? [ returned ] : arguments
										);
									}
								} );
							} );
							fns = null;
						} ).promise();
					},
					then: function( onFulfilled, onRejected, onProgress ) {
						var maxDepth = 0;
						function resolve( depth, deferred, handler, special ) {
							return function() {
								var that = this,
									args = arguments,
									mightThrow = function() {
										var returned, then;
	
										// Support: Promises/A+ section 2.3.3.3.3
										// https://promisesaplus.com/#point-59
										// Ignore double-resolution attempts
										if ( depth < maxDepth ) {
											return;
										}
	
										returned = handler.apply( that, args );
	
										// Support: Promises/A+ section 2.3.1
										// https://promisesaplus.com/#point-48
										if ( returned === deferred.promise() ) {
											throw new TypeError( "Thenable self-resolution" );
										}
	
										// Support: Promises/A+ sections 2.3.3.1, 3.5
										// https://promisesaplus.com/#point-54
										// https://promisesaplus.com/#point-75
										// Retrieve `then` only once
										then = returned &&
	
											// Support: Promises/A+ section 2.3.4
											// https://promisesaplus.com/#point-64
											// Only check objects and functions for thenability
											( typeof returned === "object" ||
												typeof returned === "function" ) &&
											returned.then;
	
										// Handle a returned thenable
										if ( jQuery.isFunction( then ) ) {
	
											// Special processors (notify) just wait for resolution
											if ( special ) {
												then.call(
													returned,
													resolve( maxDepth, deferred, Identity, special ),
													resolve( maxDepth, deferred, Thrower, special )
												);
	
											// Normal processors (resolve) also hook into progress
											} else {
	
												// ...and disregard older resolution values
												maxDepth++;
	
												then.call(
													returned,
													resolve( maxDepth, deferred, Identity, special ),
													resolve( maxDepth, deferred, Thrower, special ),
													resolve( maxDepth, deferred, Identity,
														deferred.notifyWith )
												);
											}
	
										// Handle all other returned values
										} else {
	
											// Only substitute handlers pass on context
											// and multiple values (non-spec behavior)
											if ( handler !== Identity ) {
												that = undefined;
												args = [ returned ];
											}
	
											// Process the value(s)
											// Default process is resolve
											( special || deferred.resolveWith )( that, args );
										}
									},
	
									// Only normal processors (resolve) catch and reject exceptions
									process = special ?
										mightThrow :
										function() {
											try {
												mightThrow();
											} catch ( e ) {
	
												if ( jQuery.Deferred.exceptionHook ) {
													jQuery.Deferred.exceptionHook( e,
														process.stackTrace );
												}
	
												// Support: Promises/A+ section 2.3.3.3.4.1
												// https://promisesaplus.com/#point-61
												// Ignore post-resolution exceptions
												if ( depth + 1 >= maxDepth ) {
	
													// Only substitute handlers pass on context
													// and multiple values (non-spec behavior)
													if ( handler !== Thrower ) {
														that = undefined;
														args = [ e ];
													}
	
													deferred.rejectWith( that, args );
												}
											}
										};
	
								// Support: Promises/A+ section 2.3.3.3.1
								// https://promisesaplus.com/#point-57
								// Re-resolve promises immediately to dodge false rejection from
								// subsequent errors
								if ( depth ) {
									process();
								} else {
	
									// Call an optional hook to record the stack, in case of exception
									// since it's otherwise lost when execution goes async
									if ( jQuery.Deferred.getStackHook ) {
										process.stackTrace = jQuery.Deferred.getStackHook();
									}
									window.setTimeout( process );
								}
							};
						}
	
						return jQuery.Deferred( function( newDefer ) {
	
							// progress_handlers.add( ... )
							tuples[ 0 ][ 3 ].add(
								resolve(
									0,
									newDefer,
									jQuery.isFunction( onProgress ) ?
										onProgress :
										Identity,
									newDefer.notifyWith
								)
							);
	
							// fulfilled_handlers.add( ... )
							tuples[ 1 ][ 3 ].add(
								resolve(
									0,
									newDefer,
									jQuery.isFunction( onFulfilled ) ?
										onFulfilled :
										Identity
								)
							);
	
							// rejected_handlers.add( ... )
							tuples[ 2 ][ 3 ].add(
								resolve(
									0,
									newDefer,
									jQuery.isFunction( onRejected ) ?
										onRejected :
										Thrower
								)
							);
						} ).promise();
					},
	
					// Get a promise for this deferred
					// If obj is provided, the promise aspect is added to the object
					promise: function( obj ) {
						return obj != null ? jQuery.extend( obj, promise ) : promise;
					}
				},
				deferred = {};
	
			// Add list-specific methods
			jQuery.each( tuples, function( i, tuple ) {
				var list = tuple[ 2 ],
					stateString = tuple[ 5 ];
	
				// promise.progress = list.add
				// promise.done = list.add
				// promise.fail = list.add
				promise[ tuple[ 1 ] ] = list.add;
	
				// Handle state
				if ( stateString ) {
					list.add(
						function() {
	
							// state = "resolved" (i.e., fulfilled)
							// state = "rejected"
							state = stateString;
						},
	
						// rejected_callbacks.disable
						// fulfilled_callbacks.disable
						tuples[ 3 - i ][ 2 ].disable,
	
						// progress_callbacks.lock
						tuples[ 0 ][ 2 ].lock
					);
				}
	
				// progress_handlers.fire
				// fulfilled_handlers.fire
				// rejected_handlers.fire
				list.add( tuple[ 3 ].fire );
	
				// deferred.notify = function() { deferred.notifyWith(...) }
				// deferred.resolve = function() { deferred.resolveWith(...) }
				// deferred.reject = function() { deferred.rejectWith(...) }
				deferred[ tuple[ 0 ] ] = function() {
					deferred[ tuple[ 0 ] + "With" ]( this === deferred ? undefined : this, arguments );
					return this;
				};
	
				// deferred.notifyWith = list.fireWith
				// deferred.resolveWith = list.fireWith
				// deferred.rejectWith = list.fireWith
				deferred[ tuple[ 0 ] + "With" ] = list.fireWith;
			} );
	
			// Make the deferred a promise
			promise.promise( deferred );
	
			// Call given func if any
			if ( func ) {
				func.call( deferred, deferred );
			}
	
			// All done!
			return deferred;
		},
	
		// Deferred helper
		when: function( singleValue ) {
			var
	
				// count of uncompleted subordinates
				remaining = arguments.length,
	
				// count of unprocessed arguments
				i = remaining,
	
				// subordinate fulfillment data
				resolveContexts = Array( i ),
				resolveValues = slice.call( arguments ),
	
				// the master Deferred
				master = jQuery.Deferred(),
	
				// subordinate callback factory
				updateFunc = function( i ) {
					return function( value ) {
						resolveContexts[ i ] = this;
						resolveValues[ i ] = arguments.length > 1 ? slice.call( arguments ) : value;
						if ( !( --remaining ) ) {
							master.resolveWith( resolveContexts, resolveValues );
						}
					};
				};
	
			// Single- and empty arguments are adopted like Promise.resolve
			if ( remaining <= 1 ) {
				adoptValue( singleValue, master.done( updateFunc( i ) ).resolve, master.reject );
	
				// Use .then() to unwrap secondary thenables (cf. gh-3000)
				if ( master.state() === "pending" ||
					jQuery.isFunction( resolveValues[ i ] && resolveValues[ i ].then ) ) {
	
					return master.then();
				}
			}
	
			// Multiple arguments are aggregated like Promise.all array elements
			while ( i-- ) {
				adoptValue( resolveValues[ i ], updateFunc( i ), master.reject );
			}
	
			return master.promise();
		}
	} );
	
	
	// These usually indicate a programmer mistake during development,
	// warn about them ASAP rather than swallowing them by default.
	var rerrorNames = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
	
	jQuery.Deferred.exceptionHook = function( error, stack ) {
	
		// Support: IE 8 - 9 only
		// Console exists when dev tools are open, which can happen at any time
		if ( window.console && window.console.warn && error && rerrorNames.test( error.name ) ) {
			window.console.warn( "jQuery.Deferred exception: " + error.message, error.stack, stack );
		}
	};
	
	
	
	
	jQuery.readyException = function( error ) {
		window.setTimeout( function() {
			throw error;
		} );
	};
	
	
	
	
	// The deferred used on DOM ready
	var readyList = jQuery.Deferred();
	
	jQuery.fn.ready = function( fn ) {
	
		readyList
			.then( fn )
	
			// Wrap jQuery.readyException in a function so that the lookup
			// happens at the time of error handling instead of callback
			// registration.
			.catch( function( error ) {
				jQuery.readyException( error );
			} );
	
		return this;
	};
	
	jQuery.extend( {
	
		// Is the DOM ready to be used? Set to true once it occurs.
		isReady: false,
	
		// A counter to track how many items to wait for before
		// the ready event fires. See #6781
		readyWait: 1,
	
		// Hold (or release) the ready event
		holdReady: function( hold ) {
			if ( hold ) {
				jQuery.readyWait++;
			} else {
				jQuery.ready( true );
			}
		},
	
		// Handle when the DOM is ready
		ready: function( wait ) {
	
			// Abort if there are pending holds or we're already ready
			if ( wait === true ? --jQuery.readyWait : jQuery.isReady ) {
				return;
			}
	
			// Remember that the DOM is ready
			jQuery.isReady = true;
	
			// If a normal DOM Ready event fired, decrement, and wait if need be
			if ( wait !== true && --jQuery.readyWait > 0 ) {
				return;
			}
	
			// If there are functions bound, to execute
			readyList.resolveWith( document, [ jQuery ] );
		}
	} );
	
	jQuery.ready.then = readyList.then;
	
	// The ready event handler and self cleanup method
	function completed() {
		document.removeEventListener( "DOMContentLoaded", completed );
		window.removeEventListener( "load", completed );
		jQuery.ready();
	}
	
	// Catch cases where $(document).ready() is called
	// after the browser event has already occurred.
	// Support: IE <=9 - 10 only
	// Older IE sometimes signals "interactive" too soon
	if ( document.readyState === "complete" ||
		( document.readyState !== "loading" && !document.documentElement.doScroll ) ) {
	
		// Handle it asynchronously to allow scripts the opportunity to delay ready
		window.setTimeout( jQuery.ready );
	
	} else {
	
		// Use the handy event callback
		document.addEventListener( "DOMContentLoaded", completed );
	
		// A fallback to window.onload, that will always work
		window.addEventListener( "load", completed );
	}
	
	
	
	
	// Multifunctional method to get and set values of a collection
	// The value/s can optionally be executed if it's a function
	var access = function( elems, fn, key, value, chainable, emptyGet, raw ) {
		var i = 0,
			len = elems.length,
			bulk = key == null;
	
		// Sets many values
		if ( jQuery.type( key ) === "object" ) {
			chainable = true;
			for ( i in key ) {
				access( elems, fn, i, key[ i ], true, emptyGet, raw );
			}
	
		// Sets one value
		} else if ( value !== undefined ) {
			chainable = true;
	
			if ( !jQuery.isFunction( value ) ) {
				raw = true;
			}
	
			if ( bulk ) {
	
				// Bulk operations run against the entire set
				if ( raw ) {
					fn.call( elems, value );
					fn = null;
	
				// ...except when executing function values
				} else {
					bulk = fn;
					fn = function( elem, key, value ) {
						return bulk.call( jQuery( elem ), value );
					};
				}
			}
	
			if ( fn ) {
				for ( ; i < len; i++ ) {
					fn(
						elems[ i ], key, raw ?
						value :
						value.call( elems[ i ], i, fn( elems[ i ], key ) )
					);
				}
			}
		}
	
		if ( chainable ) {
			return elems;
		}
	
		// Gets
		if ( bulk ) {
			return fn.call( elems );
		}
	
		return len ? fn( elems[ 0 ], key ) : emptyGet;
	};
	var acceptData = function( owner ) {
	
		// Accepts only:
		//  - Node
		//    - Node.ELEMENT_NODE
		//    - Node.DOCUMENT_NODE
		//  - Object
		//    - Any
		return owner.nodeType === 1 || owner.nodeType === 9 || !( +owner.nodeType );
	};
	
	
	
	
	function Data() {
		this.expando = jQuery.expando + Data.uid++;
	}
	
	Data.uid = 1;
	
	Data.prototype = {
	
		cache: function( owner ) {
	
			// Check if the owner object already has a cache
			var value = owner[ this.expando ];
	
			// If not, create one
			if ( !value ) {
				value = {};
	
				// We can accept data for non-element nodes in modern browsers,
				// but we should not, see #8335.
				// Always return an empty object.
				if ( acceptData( owner ) ) {
	
					// If it is a node unlikely to be stringify-ed or looped over
					// use plain assignment
					if ( owner.nodeType ) {
						owner[ this.expando ] = value;
	
					// Otherwise secure it in a non-enumerable property
					// configurable must be true to allow the property to be
					// deleted when data is removed
					} else {
						Object.defineProperty( owner, this.expando, {
							value: value,
							configurable: true
						} );
					}
				}
			}
	
			return value;
		},
		set: function( owner, data, value ) {
			var prop,
				cache = this.cache( owner );
	
			// Handle: [ owner, key, value ] args
			// Always use camelCase key (gh-2257)
			if ( typeof data === "string" ) {
				cache[ jQuery.camelCase( data ) ] = value;
	
			// Handle: [ owner, { properties } ] args
			} else {
	
				// Copy the properties one-by-one to the cache object
				for ( prop in data ) {
					cache[ jQuery.camelCase( prop ) ] = data[ prop ];
				}
			}
			return cache;
		},
		get: function( owner, key ) {
			return key === undefined ?
				this.cache( owner ) :
	
				// Always use camelCase key (gh-2257)
				owner[ this.expando ] && owner[ this.expando ][ jQuery.camelCase( key ) ];
		},
		access: function( owner, key, value ) {
	
			// In cases where either:
			//
			//   1. No key was specified
			//   2. A string key was specified, but no value provided
			//
			// Take the "read" path and allow the get method to determine
			// which value to return, respectively either:
			//
			//   1. The entire cache object
			//   2. The data stored at the key
			//
			if ( key === undefined ||
					( ( key && typeof key === "string" ) && value === undefined ) ) {
	
				return this.get( owner, key );
			}
	
			// When the key is not a string, or both a key and value
			// are specified, set or extend (existing objects) with either:
			//
			//   1. An object of properties
			//   2. A key and value
			//
			this.set( owner, key, value );
	
			// Since the "set" path can have two possible entry points
			// return the expected data based on which path was taken[*]
			return value !== undefined ? value : key;
		},
		remove: function( owner, key ) {
			var i,
				cache = owner[ this.expando ];
	
			if ( cache === undefined ) {
				return;
			}
	
			if ( key !== undefined ) {
	
				// Support array or space separated string of keys
				if ( jQuery.isArray( key ) ) {
	
					// If key is an array of keys...
					// We always set camelCase keys, so remove that.
					key = key.map( jQuery.camelCase );
				} else {
					key = jQuery.camelCase( key );
	
					// If a key with the spaces exists, use it.
					// Otherwise, create an array by matching non-whitespace
					key = key in cache ?
						[ key ] :
						( key.match( rnothtmlwhite ) || [] );
				}
	
				i = key.length;
	
				while ( i-- ) {
					delete cache[ key[ i ] ];
				}
			}
	
			// Remove the expando if there's no more data
			if ( key === undefined || jQuery.isEmptyObject( cache ) ) {
	
				// Support: Chrome <=35 - 45
				// Webkit & Blink performance suffers when deleting properties
				// from DOM nodes, so set to undefined instead
				// https://bugs.chromium.org/p/chromium/issues/detail?id=378607 (bug restricted)
				if ( owner.nodeType ) {
					owner[ this.expando ] = undefined;
				} else {
					delete owner[ this.expando ];
				}
			}
		},
		hasData: function( owner ) {
			var cache = owner[ this.expando ];
			return cache !== undefined && !jQuery.isEmptyObject( cache );
		}
	};
	var dataPriv = new Data();
	
	var dataUser = new Data();
	
	
	
	//	Implementation Summary
	//
	//	1. Enforce API surface and semantic compatibility with 1.9.x branch
	//	2. Improve the module's maintainability by reducing the storage
	//		paths to a single mechanism.
	//	3. Use the same single mechanism to support "private" and "user" data.
	//	4. _Never_ expose "private" data to user code (TODO: Drop _data, _removeData)
	//	5. Avoid exposing implementation details on user objects (eg. expando properties)
	//	6. Provide a clear path for implementation upgrade to WeakMap in 2014
	
	var rbrace = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
		rmultiDash = /[A-Z]/g;
	
	function getData( data ) {
		if ( data === "true" ) {
			return true;
		}
	
		if ( data === "false" ) {
			return false;
		}
	
		if ( data === "null" ) {
			return null;
		}
	
		// Only convert to a number if it doesn't change the string
		if ( data === +data + "" ) {
			return +data;
		}
	
		if ( rbrace.test( data ) ) {
			return JSON.parse( data );
		}
	
		return data;
	}
	
	function dataAttr( elem, key, data ) {
		var name;
	
		// If nothing was found internally, try to fetch any
		// data from the HTML5 data-* attribute
		if ( data === undefined && elem.nodeType === 1 ) {
			name = "data-" + key.replace( rmultiDash, "-$&" ).toLowerCase();
			data = elem.getAttribute( name );
	
			if ( typeof data === "string" ) {
				try {
					data = getData( data );
				} catch ( e ) {}
	
				// Make sure we set the data so it isn't changed later
				dataUser.set( elem, key, data );
			} else {
				data = undefined;
			}
		}
		return data;
	}
	
	jQuery.extend( {
		hasData: function( elem ) {
			return dataUser.hasData( elem ) || dataPriv.hasData( elem );
		},
	
		data: function( elem, name, data ) {
			return dataUser.access( elem, name, data );
		},
	
		removeData: function( elem, name ) {
			dataUser.remove( elem, name );
		},
	
		// TODO: Now that all calls to _data and _removeData have been replaced
		// with direct calls to dataPriv methods, these can be deprecated.
		_data: function( elem, name, data ) {
			return dataPriv.access( elem, name, data );
		},
	
		_removeData: function( elem, name ) {
			dataPriv.remove( elem, name );
		}
	} );
	
	jQuery.fn.extend( {
		data: function( key, value ) {
			var i, name, data,
				elem = this[ 0 ],
				attrs = elem && elem.attributes;
	
			// Gets all values
			if ( key === undefined ) {
				if ( this.length ) {
					data = dataUser.get( elem );
	
					if ( elem.nodeType === 1 && !dataPriv.get( elem, "hasDataAttrs" ) ) {
						i = attrs.length;
						while ( i-- ) {
	
							// Support: IE 11 only
							// The attrs elements can be null (#14894)
							if ( attrs[ i ] ) {
								name = attrs[ i ].name;
								if ( name.indexOf( "data-" ) === 0 ) {
									name = jQuery.camelCase( name.slice( 5 ) );
									dataAttr( elem, name, data[ name ] );
								}
							}
						}
						dataPriv.set( elem, "hasDataAttrs", true );
					}
				}
	
				return data;
			}
	
			// Sets multiple values
			if ( typeof key === "object" ) {
				return this.each( function() {
					dataUser.set( this, key );
				} );
			}
	
			return access( this, function( value ) {
				var data;
	
				// The calling jQuery object (element matches) is not empty
				// (and therefore has an element appears at this[ 0 ]) and the
				// `value` parameter was not undefined. An empty jQuery object
				// will result in `undefined` for elem = this[ 0 ] which will
				// throw an exception if an attempt to read a data cache is made.
				if ( elem && value === undefined ) {
	
					// Attempt to get data from the cache
					// The key will always be camelCased in Data
					data = dataUser.get( elem, key );
					if ( data !== undefined ) {
						return data;
					}
	
					// Attempt to "discover" the data in
					// HTML5 custom data-* attrs
					data = dataAttr( elem, key );
					if ( data !== undefined ) {
						return data;
					}
	
					// We tried really hard, but the data doesn't exist.
					return;
				}
	
				// Set the data...
				this.each( function() {
	
					// We always store the camelCased key
					dataUser.set( this, key, value );
				} );
			}, null, value, arguments.length > 1, null, true );
		},
	
		removeData: function( key ) {
			return this.each( function() {
				dataUser.remove( this, key );
			} );
		}
	} );
	
	
	jQuery.extend( {
		queue: function( elem, type, data ) {
			var queue;
	
			if ( elem ) {
				type = ( type || "fx" ) + "queue";
				queue = dataPriv.get( elem, type );
	
				// Speed up dequeue by getting out quickly if this is just a lookup
				if ( data ) {
					if ( !queue || jQuery.isArray( data ) ) {
						queue = dataPriv.access( elem, type, jQuery.makeArray( data ) );
					} else {
						queue.push( data );
					}
				}
				return queue || [];
			}
		},
	
		dequeue: function( elem, type ) {
			type = type || "fx";
	
			var queue = jQuery.queue( elem, type ),
				startLength = queue.length,
				fn = queue.shift(),
				hooks = jQuery._queueHooks( elem, type ),
				next = function() {
					jQuery.dequeue( elem, type );
				};
	
			// If the fx queue is dequeued, always remove the progress sentinel
			if ( fn === "inprogress" ) {
				fn = queue.shift();
				startLength--;
			}
	
			if ( fn ) {
	
				// Add a progress sentinel to prevent the fx queue from being
				// automatically dequeued
				if ( type === "fx" ) {
					queue.unshift( "inprogress" );
				}
	
				// Clear up the last queue stop function
				delete hooks.stop;
				fn.call( elem, next, hooks );
			}
	
			if ( !startLength && hooks ) {
				hooks.empty.fire();
			}
		},
	
		// Not public - generate a queueHooks object, or return the current one
		_queueHooks: function( elem, type ) {
			var key = type + "queueHooks";
			return dataPriv.get( elem, key ) || dataPriv.access( elem, key, {
				empty: jQuery.Callbacks( "once memory" ).add( function() {
					dataPriv.remove( elem, [ type + "queue", key ] );
				} )
			} );
		}
	} );
	
	jQuery.fn.extend( {
		queue: function( type, data ) {
			var setter = 2;
	
			if ( typeof type !== "string" ) {
				data = type;
				type = "fx";
				setter--;
			}
	
			if ( arguments.length < setter ) {
				return jQuery.queue( this[ 0 ], type );
			}
	
			return data === undefined ?
				this :
				this.each( function() {
					var queue = jQuery.queue( this, type, data );
	
					// Ensure a hooks for this queue
					jQuery._queueHooks( this, type );
	
					if ( type === "fx" && queue[ 0 ] !== "inprogress" ) {
						jQuery.dequeue( this, type );
					}
				} );
		},
		dequeue: function( type ) {
			return this.each( function() {
				jQuery.dequeue( this, type );
			} );
		},
		clearQueue: function( type ) {
			return this.queue( type || "fx", [] );
		},
	
		// Get a promise resolved when queues of a certain type
		// are emptied (fx is the type by default)
		promise: function( type, obj ) {
			var tmp,
				count = 1,
				defer = jQuery.Deferred(),
				elements = this,
				i = this.length,
				resolve = function() {
					if ( !( --count ) ) {
						defer.resolveWith( elements, [ elements ] );
					}
				};
	
			if ( typeof type !== "string" ) {
				obj = type;
				type = undefined;
			}
			type = type || "fx";
	
			while ( i-- ) {
				tmp = dataPriv.get( elements[ i ], type + "queueHooks" );
				if ( tmp && tmp.empty ) {
					count++;
					tmp.empty.add( resolve );
				}
			}
			resolve();
			return defer.promise( obj );
		}
	} );
	var pnum = ( /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/ ).source;
	
	var rcssNum = new RegExp( "^(?:([+-])=|)(" + pnum + ")([a-z%]*)$", "i" );
	
	
	var cssExpand = [ "Top", "Right", "Bottom", "Left" ];
	
	var isHiddenWithinTree = function( elem, el ) {
	
			// isHiddenWithinTree might be called from jQuery#filter function;
			// in that case, element will be second argument
			elem = el || elem;
	
			// Inline style trumps all
			return elem.style.display === "none" ||
				elem.style.display === "" &&
	
				// Otherwise, check computed style
				// Support: Firefox <=43 - 45
				// Disconnected elements can have computed display: none, so first confirm that elem is
				// in the document.
				jQuery.contains( elem.ownerDocument, elem ) &&
	
				jQuery.css( elem, "display" ) === "none";
		};
	
	var swap = function( elem, options, callback, args ) {
		var ret, name,
			old = {};
	
		// Remember the old values, and insert the new ones
		for ( name in options ) {
			old[ name ] = elem.style[ name ];
			elem.style[ name ] = options[ name ];
		}
	
		ret = callback.apply( elem, args || [] );
	
		// Revert the old values
		for ( name in options ) {
			elem.style[ name ] = old[ name ];
		}
	
		return ret;
	};
	
	
	
	
	function adjustCSS( elem, prop, valueParts, tween ) {
		var adjusted,
			scale = 1,
			maxIterations = 20,
			currentValue = tween ?
				function() {
					return tween.cur();
				} :
				function() {
					return jQuery.css( elem, prop, "" );
				},
			initial = currentValue(),
			unit = valueParts && valueParts[ 3 ] || ( jQuery.cssNumber[ prop ] ? "" : "px" ),
	
			// Starting value computation is required for potential unit mismatches
			initialInUnit = ( jQuery.cssNumber[ prop ] || unit !== "px" && +initial ) &&
				rcssNum.exec( jQuery.css( elem, prop ) );
	
		if ( initialInUnit && initialInUnit[ 3 ] !== unit ) {
	
			// Trust units reported by jQuery.css
			unit = unit || initialInUnit[ 3 ];
	
			// Make sure we update the tween properties later on
			valueParts = valueParts || [];
	
			// Iteratively approximate from a nonzero starting point
			initialInUnit = +initial || 1;
	
			do {
	
				// If previous iteration zeroed out, double until we get *something*.
				// Use string for doubling so we don't accidentally see scale as unchanged below
				scale = scale || ".5";
	
				// Adjust and apply
				initialInUnit = initialInUnit / scale;
				jQuery.style( elem, prop, initialInUnit + unit );
	
			// Update scale, tolerating zero or NaN from tween.cur()
			// Break the loop if scale is unchanged or perfect, or if we've just had enough.
			} while (
				scale !== ( scale = currentValue() / initial ) && scale !== 1 && --maxIterations
			);
		}
	
		if ( valueParts ) {
			initialInUnit = +initialInUnit || +initial || 0;
	
			// Apply relative offset (+=/-=) if specified
			adjusted = valueParts[ 1 ] ?
				initialInUnit + ( valueParts[ 1 ] + 1 ) * valueParts[ 2 ] :
				+valueParts[ 2 ];
			if ( tween ) {
				tween.unit = unit;
				tween.start = initialInUnit;
				tween.end = adjusted;
			}
		}
		return adjusted;
	}
	
	
	var defaultDisplayMap = {};
	
	function getDefaultDisplay( elem ) {
		var temp,
			doc = elem.ownerDocument,
			nodeName = elem.nodeName,
			display = defaultDisplayMap[ nodeName ];
	
		if ( display ) {
			return display;
		}
	
		temp = doc.body.appendChild( doc.createElement( nodeName ) );
		display = jQuery.css( temp, "display" );
	
		temp.parentNode.removeChild( temp );
	
		if ( display === "none" ) {
			display = "block";
		}
		defaultDisplayMap[ nodeName ] = display;
	
		return display;
	}
	
	function showHide( elements, show ) {
		var display, elem,
			values = [],
			index = 0,
			length = elements.length;
	
		// Determine new display value for elements that need to change
		for ( ; index < length; index++ ) {
			elem = elements[ index ];
			if ( !elem.style ) {
				continue;
			}
	
			display = elem.style.display;
			if ( show ) {
	
				// Since we force visibility upon cascade-hidden elements, an immediate (and slow)
				// check is required in this first loop unless we have a nonempty display value (either
				// inline or about-to-be-restored)
				if ( display === "none" ) {
					values[ index ] = dataPriv.get( elem, "display" ) || null;
					if ( !values[ index ] ) {
						elem.style.display = "";
					}
				}
				if ( elem.style.display === "" && isHiddenWithinTree( elem ) ) {
					values[ index ] = getDefaultDisplay( elem );
				}
			} else {
				if ( display !== "none" ) {
					values[ index ] = "none";
	
					// Remember what we're overwriting
					dataPriv.set( elem, "display", display );
				}
			}
		}
	
		// Set the display of the elements in a second loop to avoid constant reflow
		for ( index = 0; index < length; index++ ) {
			if ( values[ index ] != null ) {
				elements[ index ].style.display = values[ index ];
			}
		}
	
		return elements;
	}
	
	jQuery.fn.extend( {
		show: function() {
			return showHide( this, true );
		},
		hide: function() {
			return showHide( this );
		},
		toggle: function( state ) {
			if ( typeof state === "boolean" ) {
				return state ? this.show() : this.hide();
			}
	
			return this.each( function() {
				if ( isHiddenWithinTree( this ) ) {
					jQuery( this ).show();
				} else {
					jQuery( this ).hide();
				}
			} );
		}
	} );
	var rcheckableType = ( /^(?:checkbox|radio)$/i );
	
	var rtagName = ( /<([a-z][^\/\0>\x20\t\r\n\f]+)/i );
	
	var rscriptType = ( /^$|\/(?:java|ecma)script/i );
	
	
	
	// We have to close these tags to support XHTML (#13200)
	var wrapMap = {
	
		// Support: IE <=9 only
		option: [ 1, "<select multiple='multiple'>", "</select>" ],
	
		// XHTML parsers do not magically insert elements in the
		// same way that tag soup parsers do. So we cannot shorten
		// this by omitting <tbody> or other required elements.
		thead: [ 1, "<table>", "</table>" ],
		col: [ 2, "<table><colgroup>", "</colgroup></table>" ],
		tr: [ 2, "<table><tbody>", "</tbody></table>" ],
		td: [ 3, "<table><tbody><tr>", "</tr></tbody></table>" ],
	
		_default: [ 0, "", "" ]
	};
	
	// Support: IE <=9 only
	wrapMap.optgroup = wrapMap.option;
	
	wrapMap.tbody = wrapMap.tfoot = wrapMap.colgroup = wrapMap.caption = wrapMap.thead;
	wrapMap.th = wrapMap.td;
	
	
	function getAll( context, tag ) {
	
		// Support: IE <=9 - 11 only
		// Use typeof to avoid zero-argument method invocation on host objects (#15151)
		var ret;
	
		if ( typeof context.getElementsByTagName !== "undefined" ) {
			ret = context.getElementsByTagName( tag || "*" );
	
		} else if ( typeof context.querySelectorAll !== "undefined" ) {
			ret = context.querySelectorAll( tag || "*" );
	
		} else {
			ret = [];
		}
	
		if ( tag === undefined || tag && jQuery.nodeName( context, tag ) ) {
			return jQuery.merge( [ context ], ret );
		}
	
		return ret;
	}
	
	
	// Mark scripts as having already been evaluated
	function setGlobalEval( elems, refElements ) {
		var i = 0,
			l = elems.length;
	
		for ( ; i < l; i++ ) {
			dataPriv.set(
				elems[ i ],
				"globalEval",
				!refElements || dataPriv.get( refElements[ i ], "globalEval" )
			);
		}
	}
	
	
	var rhtml = /<|&#?\w+;/;
	
	function buildFragment( elems, context, scripts, selection, ignored ) {
		var elem, tmp, tag, wrap, contains, j,
			fragment = context.createDocumentFragment(),
			nodes = [],
			i = 0,
			l = elems.length;
	
		for ( ; i < l; i++ ) {
			elem = elems[ i ];
	
			if ( elem || elem === 0 ) {
	
				// Add nodes directly
				if ( jQuery.type( elem ) === "object" ) {
	
					// Support: Android <=4.0 only, PhantomJS 1 only
					// push.apply(_, arraylike) throws on ancient WebKit
					jQuery.merge( nodes, elem.nodeType ? [ elem ] : elem );
	
				// Convert non-html into a text node
				} else if ( !rhtml.test( elem ) ) {
					nodes.push( context.createTextNode( elem ) );
	
				// Convert html into DOM nodes
				} else {
					tmp = tmp || fragment.appendChild( context.createElement( "div" ) );
	
					// Deserialize a standard representation
					tag = ( rtagName.exec( elem ) || [ "", "" ] )[ 1 ].toLowerCase();
					wrap = wrapMap[ tag ] || wrapMap._default;
					tmp.innerHTML = wrap[ 1 ] + jQuery.htmlPrefilter( elem ) + wrap[ 2 ];
	
					// Descend through wrappers to the right content
					j = wrap[ 0 ];
					while ( j-- ) {
						tmp = tmp.lastChild;
					}
	
					// Support: Android <=4.0 only, PhantomJS 1 only
					// push.apply(_, arraylike) throws on ancient WebKit
					jQuery.merge( nodes, tmp.childNodes );
	
					// Remember the top-level container
					tmp = fragment.firstChild;
	
					// Ensure the created nodes are orphaned (#12392)
					tmp.textContent = "";
				}
			}
		}
	
		// Remove wrapper from fragment
		fragment.textContent = "";
	
		i = 0;
		while ( ( elem = nodes[ i++ ] ) ) {
	
			// Skip elements already in the context collection (trac-4087)
			if ( selection && jQuery.inArray( elem, selection ) > -1 ) {
				if ( ignored ) {
					ignored.push( elem );
				}
				continue;
			}
	
			contains = jQuery.contains( elem.ownerDocument, elem );
	
			// Append to fragment
			tmp = getAll( fragment.appendChild( elem ), "script" );
	
			// Preserve script evaluation history
			if ( contains ) {
				setGlobalEval( tmp );
			}
	
			// Capture executables
			if ( scripts ) {
				j = 0;
				while ( ( elem = tmp[ j++ ] ) ) {
					if ( rscriptType.test( elem.type || "" ) ) {
						scripts.push( elem );
					}
				}
			}
		}
	
		return fragment;
	}
	
	
	( function() {
		var fragment = document.createDocumentFragment(),
			div = fragment.appendChild( document.createElement( "div" ) ),
			input = document.createElement( "input" );
	
		// Support: Android 4.0 - 4.3 only
		// Check state lost if the name is set (#11217)
		// Support: Windows Web Apps (WWA)
		// `name` and `type` must use .setAttribute for WWA (#14901)
		input.setAttribute( "type", "radio" );
		input.setAttribute( "checked", "checked" );
		input.setAttribute( "name", "t" );
	
		div.appendChild( input );
	
		// Support: Android <=4.1 only
		// Older WebKit doesn't clone checked state correctly in fragments
		support.checkClone = div.cloneNode( true ).cloneNode( true ).lastChild.checked;
	
		// Support: IE <=11 only
		// Make sure textarea (and checkbox) defaultValue is properly cloned
		div.innerHTML = "<textarea>x</textarea>";
		support.noCloneChecked = !!div.cloneNode( true ).lastChild.defaultValue;
	} )();
	var documentElement = document.documentElement;
	
	
	
	var
		rkeyEvent = /^key/,
		rmouseEvent = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
		rtypenamespace = /^([^.]*)(?:\.(.+)|)/;
	
	function returnTrue() {
		return true;
	}
	
	function returnFalse() {
		return false;
	}
	
	// Support: IE <=9 only
	// See #13393 for more info
	function safeActiveElement() {
		try {
			return document.activeElement;
		} catch ( err ) { }
	}
	
	function on( elem, types, selector, data, fn, one ) {
		var origFn, type;
	
		// Types can be a map of types/handlers
		if ( typeof types === "object" ) {
	
			// ( types-Object, selector, data )
			if ( typeof selector !== "string" ) {
	
				// ( types-Object, data )
				data = data || selector;
				selector = undefined;
			}
			for ( type in types ) {
				on( elem, type, selector, data, types[ type ], one );
			}
			return elem;
		}
	
		if ( data == null && fn == null ) {
	
			// ( types, fn )
			fn = selector;
			data = selector = undefined;
		} else if ( fn == null ) {
			if ( typeof selector === "string" ) {
	
				// ( types, selector, fn )
				fn = data;
				data = undefined;
			} else {
	
				// ( types, data, fn )
				fn = data;
				data = selector;
				selector = undefined;
			}
		}
		if ( fn === false ) {
			fn = returnFalse;
		} else if ( !fn ) {
			return elem;
		}
	
		if ( one === 1 ) {
			origFn = fn;
			fn = function( event ) {
	
				// Can use an empty set, since event contains the info
				jQuery().off( event );
				return origFn.apply( this, arguments );
			};
	
			// Use same guid so caller can remove using origFn
			fn.guid = origFn.guid || ( origFn.guid = jQuery.guid++ );
		}
		return elem.each( function() {
			jQuery.event.add( this, types, fn, data, selector );
		} );
	}
	
	/*
	 * Helper functions for managing events -- not part of the public interface.
	 * Props to Dean Edwards' addEvent library for many of the ideas.
	 */
	jQuery.event = {
	
		global: {},
	
		add: function( elem, types, handler, data, selector ) {
	
			var handleObjIn, eventHandle, tmp,
				events, t, handleObj,
				special, handlers, type, namespaces, origType,
				elemData = dataPriv.get( elem );
	
			// Don't attach events to noData or text/comment nodes (but allow plain objects)
			if ( !elemData ) {
				return;
			}
	
			// Caller can pass in an object of custom data in lieu of the handler
			if ( handler.handler ) {
				handleObjIn = handler;
				handler = handleObjIn.handler;
				selector = handleObjIn.selector;
			}
	
			// Ensure that invalid selectors throw exceptions at attach time
			// Evaluate against documentElement in case elem is a non-element node (e.g., document)
			if ( selector ) {
				jQuery.find.matchesSelector( documentElement, selector );
			}
	
			// Make sure that the handler has a unique ID, used to find/remove it later
			if ( !handler.guid ) {
				handler.guid = jQuery.guid++;
			}
	
			// Init the element's event structure and main handler, if this is the first
			if ( !( events = elemData.events ) ) {
				events = elemData.events = {};
			}
			if ( !( eventHandle = elemData.handle ) ) {
				eventHandle = elemData.handle = function( e ) {
	
					// Discard the second event of a jQuery.event.trigger() and
					// when an event is called after a page has unloaded
					return typeof jQuery !== "undefined" && jQuery.event.triggered !== e.type ?
						jQuery.event.dispatch.apply( elem, arguments ) : undefined;
				};
			}
	
			// Handle multiple events separated by a space
			types = ( types || "" ).match( rnothtmlwhite ) || [ "" ];
			t = types.length;
			while ( t-- ) {
				tmp = rtypenamespace.exec( types[ t ] ) || [];
				type = origType = tmp[ 1 ];
				namespaces = ( tmp[ 2 ] || "" ).split( "." ).sort();
	
				// There *must* be a type, no attaching namespace-only handlers
				if ( !type ) {
					continue;
				}
	
				// If event changes its type, use the special event handlers for the changed type
				special = jQuery.event.special[ type ] || {};
	
				// If selector defined, determine special event api type, otherwise given type
				type = ( selector ? special.delegateType : special.bindType ) || type;
	
				// Update special based on newly reset type
				special = jQuery.event.special[ type ] || {};
	
				// handleObj is passed to all event handlers
				handleObj = jQuery.extend( {
					type: type,
					origType: origType,
					data: data,
					handler: handler,
					guid: handler.guid,
					selector: selector,
					needsContext: selector && jQuery.expr.match.needsContext.test( selector ),
					namespace: namespaces.join( "." )
				}, handleObjIn );
	
				// Init the event handler queue if we're the first
				if ( !( handlers = events[ type ] ) ) {
					handlers = events[ type ] = [];
					handlers.delegateCount = 0;
	
					// Only use addEventListener if the special events handler returns false
					if ( !special.setup ||
						special.setup.call( elem, data, namespaces, eventHandle ) === false ) {
	
						if ( elem.addEventListener ) {
							elem.addEventListener( type, eventHandle );
						}
					}
				}
	
				if ( special.add ) {
					special.add.call( elem, handleObj );
	
					if ( !handleObj.handler.guid ) {
						handleObj.handler.guid = handler.guid;
					}
				}
	
				// Add to the element's handler list, delegates in front
				if ( selector ) {
					handlers.splice( handlers.delegateCount++, 0, handleObj );
				} else {
					handlers.push( handleObj );
				}
	
				// Keep track of which events have ever been used, for event optimization
				jQuery.event.global[ type ] = true;
			}
	
		},
	
		// Detach an event or set of events from an element
		remove: function( elem, types, handler, selector, mappedTypes ) {
	
			var j, origCount, tmp,
				events, t, handleObj,
				special, handlers, type, namespaces, origType,
				elemData = dataPriv.hasData( elem ) && dataPriv.get( elem );
	
			if ( !elemData || !( events = elemData.events ) ) {
				return;
			}
	
			// Once for each type.namespace in types; type may be omitted
			types = ( types || "" ).match( rnothtmlwhite ) || [ "" ];
			t = types.length;
			while ( t-- ) {
				tmp = rtypenamespace.exec( types[ t ] ) || [];
				type = origType = tmp[ 1 ];
				namespaces = ( tmp[ 2 ] || "" ).split( "." ).sort();
	
				// Unbind all events (on this namespace, if provided) for the element
				if ( !type ) {
					for ( type in events ) {
						jQuery.event.remove( elem, type + types[ t ], handler, selector, true );
					}
					continue;
				}
	
				special = jQuery.event.special[ type ] || {};
				type = ( selector ? special.delegateType : special.bindType ) || type;
				handlers = events[ type ] || [];
				tmp = tmp[ 2 ] &&
					new RegExp( "(^|\\.)" + namespaces.join( "\\.(?:.*\\.|)" ) + "(\\.|$)" );
	
				// Remove matching events
				origCount = j = handlers.length;
				while ( j-- ) {
					handleObj = handlers[ j ];
	
					if ( ( mappedTypes || origType === handleObj.origType ) &&
						( !handler || handler.guid === handleObj.guid ) &&
						( !tmp || tmp.test( handleObj.namespace ) ) &&
						( !selector || selector === handleObj.selector ||
							selector === "**" && handleObj.selector ) ) {
						handlers.splice( j, 1 );
	
						if ( handleObj.selector ) {
							handlers.delegateCount--;
						}
						if ( special.remove ) {
							special.remove.call( elem, handleObj );
						}
					}
				}
	
				// Remove generic event handler if we removed something and no more handlers exist
				// (avoids potential for endless recursion during removal of special event handlers)
				if ( origCount && !handlers.length ) {
					if ( !special.teardown ||
						special.teardown.call( elem, namespaces, elemData.handle ) === false ) {
	
						jQuery.removeEvent( elem, type, elemData.handle );
					}
	
					delete events[ type ];
				}
			}
	
			// Remove data and the expando if it's no longer used
			if ( jQuery.isEmptyObject( events ) ) {
				dataPriv.remove( elem, "handle events" );
			}
		},
	
		dispatch: function( nativeEvent ) {
	
			// Make a writable jQuery.Event from the native event object
			var event = jQuery.event.fix( nativeEvent );
	
			var i, j, ret, matched, handleObj, handlerQueue,
				args = new Array( arguments.length ),
				handlers = ( dataPriv.get( this, "events" ) || {} )[ event.type ] || [],
				special = jQuery.event.special[ event.type ] || {};
	
			// Use the fix-ed jQuery.Event rather than the (read-only) native event
			args[ 0 ] = event;
	
			for ( i = 1; i < arguments.length; i++ ) {
				args[ i ] = arguments[ i ];
			}
	
			event.delegateTarget = this;
	
			// Call the preDispatch hook for the mapped type, and let it bail if desired
			if ( special.preDispatch && special.preDispatch.call( this, event ) === false ) {
				return;
			}
	
			// Determine handlers
			handlerQueue = jQuery.event.handlers.call( this, event, handlers );
	
			// Run delegates first; they may want to stop propagation beneath us
			i = 0;
			while ( ( matched = handlerQueue[ i++ ] ) && !event.isPropagationStopped() ) {
				event.currentTarget = matched.elem;
	
				j = 0;
				while ( ( handleObj = matched.handlers[ j++ ] ) &&
					!event.isImmediatePropagationStopped() ) {
	
					// Triggered event must either 1) have no namespace, or 2) have namespace(s)
					// a subset or equal to those in the bound event (both can have no namespace).
					if ( !event.rnamespace || event.rnamespace.test( handleObj.namespace ) ) {
	
						event.handleObj = handleObj;
						event.data = handleObj.data;
	
						ret = ( ( jQuery.event.special[ handleObj.origType ] || {} ).handle ||
							handleObj.handler ).apply( matched.elem, args );
	
						if ( ret !== undefined ) {
							if ( ( event.result = ret ) === false ) {
								event.preventDefault();
								event.stopPropagation();
							}
						}
					}
				}
			}
	
			// Call the postDispatch hook for the mapped type
			if ( special.postDispatch ) {
				special.postDispatch.call( this, event );
			}
	
			return event.result;
		},
	
		handlers: function( event, handlers ) {
			var i, handleObj, sel, matchedHandlers, matchedSelectors,
				handlerQueue = [],
				delegateCount = handlers.delegateCount,
				cur = event.target;
	
			// Find delegate handlers
			if ( delegateCount &&
	
				// Support: IE <=9
				// Black-hole SVG <use> instance trees (trac-13180)
				cur.nodeType &&
	
				// Support: Firefox <=42
				// Suppress spec-violating clicks indicating a non-primary pointer button (trac-3861)
				// https://www.w3.org/TR/DOM-Level-3-Events/#event-type-click
				// Support: IE 11 only
				// ...but not arrow key "clicks" of radio inputs, which can have `button` -1 (gh-2343)
				!( event.type === "click" && event.button >= 1 ) ) {
	
				for ( ; cur !== this; cur = cur.parentNode || this ) {
	
					// Don't check non-elements (#13208)
					// Don't process clicks on disabled elements (#6911, #8165, #11382, #11764)
					if ( cur.nodeType === 1 && !( event.type === "click" && cur.disabled === true ) ) {
						matchedHandlers = [];
						matchedSelectors = {};
						for ( i = 0; i < delegateCount; i++ ) {
							handleObj = handlers[ i ];
	
							// Don't conflict with Object.prototype properties (#13203)
							sel = handleObj.selector + " ";
	
							if ( matchedSelectors[ sel ] === undefined ) {
								matchedSelectors[ sel ] = handleObj.needsContext ?
									jQuery( sel, this ).index( cur ) > -1 :
									jQuery.find( sel, this, null, [ cur ] ).length;
							}
							if ( matchedSelectors[ sel ] ) {
								matchedHandlers.push( handleObj );
							}
						}
						if ( matchedHandlers.length ) {
							handlerQueue.push( { elem: cur, handlers: matchedHandlers } );
						}
					}
				}
			}
	
			// Add the remaining (directly-bound) handlers
			cur = this;
			if ( delegateCount < handlers.length ) {
				handlerQueue.push( { elem: cur, handlers: handlers.slice( delegateCount ) } );
			}
	
			return handlerQueue;
		},
	
		addProp: function( name, hook ) {
			Object.defineProperty( jQuery.Event.prototype, name, {
				enumerable: true,
				configurable: true,
	
				get: jQuery.isFunction( hook ) ?
					function() {
						if ( this.originalEvent ) {
								return hook( this.originalEvent );
						}
					} :
					function() {
						if ( this.originalEvent ) {
								return this.originalEvent[ name ];
						}
					},
	
				set: function( value ) {
					Object.defineProperty( this, name, {
						enumerable: true,
						configurable: true,
						writable: true,
						value: value
					} );
				}
			} );
		},
	
		fix: function( originalEvent ) {
			return originalEvent[ jQuery.expando ] ?
				originalEvent :
				new jQuery.Event( originalEvent );
		},
	
		special: {
			load: {
	
				// Prevent triggered image.load events from bubbling to window.load
				noBubble: true
			},
			focus: {
	
				// Fire native event if possible so blur/focus sequence is correct
				trigger: function() {
					if ( this !== safeActiveElement() && this.focus ) {
						this.focus();
						return false;
					}
				},
				delegateType: "focusin"
			},
			blur: {
				trigger: function() {
					if ( this === safeActiveElement() && this.blur ) {
						this.blur();
						return false;
					}
				},
				delegateType: "focusout"
			},
			click: {
	
				// For checkbox, fire native event so checked state will be right
				trigger: function() {
					if ( this.type === "checkbox" && this.click && jQuery.nodeName( this, "input" ) ) {
						this.click();
						return false;
					}
				},
	
				// For cross-browser consistency, don't fire native .click() on links
				_default: function( event ) {
					return jQuery.nodeName( event.target, "a" );
				}
			},
	
			beforeunload: {
				postDispatch: function( event ) {
	
					// Support: Firefox 20+
					// Firefox doesn't alert if the returnValue field is not set.
					if ( event.result !== undefined && event.originalEvent ) {
						event.originalEvent.returnValue = event.result;
					}
				}
			}
		}
	};
	
	jQuery.removeEvent = function( elem, type, handle ) {
	
		// This "if" is needed for plain objects
		if ( elem.removeEventListener ) {
			elem.removeEventListener( type, handle );
		}
	};
	
	jQuery.Event = function( src, props ) {
	
		// Allow instantiation without the 'new' keyword
		if ( !( this instanceof jQuery.Event ) ) {
			return new jQuery.Event( src, props );
		}
	
		// Event object
		if ( src && src.type ) {
			this.originalEvent = src;
			this.type = src.type;
	
			// Events bubbling up the document may have been marked as prevented
			// by a handler lower down the tree; reflect the correct value.
			this.isDefaultPrevented = src.defaultPrevented ||
					src.defaultPrevented === undefined &&
	
					// Support: Android <=2.3 only
					src.returnValue === false ?
				returnTrue :
				returnFalse;
	
			// Create target properties
			// Support: Safari <=6 - 7 only
			// Target should not be a text node (#504, #13143)
			this.target = ( src.target && src.target.nodeType === 3 ) ?
				src.target.parentNode :
				src.target;
	
			this.currentTarget = src.currentTarget;
			this.relatedTarget = src.relatedTarget;
	
		// Event type
		} else {
			this.type = src;
		}
	
		// Put explicitly provided properties onto the event object
		if ( props ) {
			jQuery.extend( this, props );
		}
	
		// Create a timestamp if incoming event doesn't have one
		this.timeStamp = src && src.timeStamp || jQuery.now();
	
		// Mark it as fixed
		this[ jQuery.expando ] = true;
	};
	
	// jQuery.Event is based on DOM3 Events as specified by the ECMAScript Language Binding
	// https://www.w3.org/TR/2003/WD-DOM-Level-3-Events-20030331/ecma-script-binding.html
	jQuery.Event.prototype = {
		constructor: jQuery.Event,
		isDefaultPrevented: returnFalse,
		isPropagationStopped: returnFalse,
		isImmediatePropagationStopped: returnFalse,
		isSimulated: false,
	
		preventDefault: function() {
			var e = this.originalEvent;
	
			this.isDefaultPrevented = returnTrue;
	
			if ( e && !this.isSimulated ) {
				e.preventDefault();
			}
		},
		stopPropagation: function() {
			var e = this.originalEvent;
	
			this.isPropagationStopped = returnTrue;
	
			if ( e && !this.isSimulated ) {
				e.stopPropagation();
			}
		},
		stopImmediatePropagation: function() {
			var e = this.originalEvent;
	
			this.isImmediatePropagationStopped = returnTrue;
	
			if ( e && !this.isSimulated ) {
				e.stopImmediatePropagation();
			}
	
			this.stopPropagation();
		}
	};
	
	// Includes all common event props including KeyEvent and MouseEvent specific props
	jQuery.each( {
		altKey: true,
		bubbles: true,
		cancelable: true,
		changedTouches: true,
		ctrlKey: true,
		detail: true,
		eventPhase: true,
		metaKey: true,
		pageX: true,
		pageY: true,
		shiftKey: true,
		view: true,
		"char": true,
		charCode: true,
		key: true,
		keyCode: true,
		button: true,
		buttons: true,
		clientX: true,
		clientY: true,
		offsetX: true,
		offsetY: true,
		pointerId: true,
		pointerType: true,
		screenX: true,
		screenY: true,
		targetTouches: true,
		toElement: true,
		touches: true,
	
		which: function( event ) {
			var button = event.button;
	
			// Add which for key events
			if ( event.which == null && rkeyEvent.test( event.type ) ) {
				return event.charCode != null ? event.charCode : event.keyCode;
			}
	
			// Add which for click: 1 === left; 2 === middle; 3 === right
			if ( !event.which && button !== undefined && rmouseEvent.test( event.type ) ) {
				if ( button & 1 ) {
					return 1;
				}
	
				if ( button & 2 ) {
					return 3;
				}
	
				if ( button & 4 ) {
					return 2;
				}
	
				return 0;
			}
	
			return event.which;
		}
	}, jQuery.event.addProp );
	
	// Create mouseenter/leave events using mouseover/out and event-time checks
	// so that event delegation works in jQuery.
	// Do the same for pointerenter/pointerleave and pointerover/pointerout
	//
	// Support: Safari 7 only
	// Safari sends mouseenter too often; see:
	// https://bugs.chromium.org/p/chromium/issues/detail?id=470258
	// for the description of the bug (it existed in older Chrome versions as well).
	jQuery.each( {
		mouseenter: "mouseover",
		mouseleave: "mouseout",
		pointerenter: "pointerover",
		pointerleave: "pointerout"
	}, function( orig, fix ) {
		jQuery.event.special[ orig ] = {
			delegateType: fix,
			bindType: fix,
	
			handle: function( event ) {
				var ret,
					target = this,
					related = event.relatedTarget,
					handleObj = event.handleObj;
	
				// For mouseenter/leave call the handler if related is outside the target.
				// NB: No relatedTarget if the mouse left/entered the browser window
				if ( !related || ( related !== target && !jQuery.contains( target, related ) ) ) {
					event.type = handleObj.origType;
					ret = handleObj.handler.apply( this, arguments );
					event.type = fix;
				}
				return ret;
			}
		};
	} );
	
	jQuery.fn.extend( {
	
		on: function( types, selector, data, fn ) {
			return on( this, types, selector, data, fn );
		},
		one: function( types, selector, data, fn ) {
			return on( this, types, selector, data, fn, 1 );
		},
		off: function( types, selector, fn ) {
			var handleObj, type;
			if ( types && types.preventDefault && types.handleObj ) {
	
				// ( event )  dispatched jQuery.Event
				handleObj = types.handleObj;
				jQuery( types.delegateTarget ).off(
					handleObj.namespace ?
						handleObj.origType + "." + handleObj.namespace :
						handleObj.origType,
					handleObj.selector,
					handleObj.handler
				);
				return this;
			}
			if ( typeof types === "object" ) {
	
				// ( types-object [, selector] )
				for ( type in types ) {
					this.off( type, selector, types[ type ] );
				}
				return this;
			}
			if ( selector === false || typeof selector === "function" ) {
	
				// ( types [, fn] )
				fn = selector;
				selector = undefined;
			}
			if ( fn === false ) {
				fn = returnFalse;
			}
			return this.each( function() {
				jQuery.event.remove( this, types, fn, selector );
			} );
		}
	} );
	
	
	var
	
		/* eslint-disable max-len */
	
		// See https://github.com/eslint/eslint/issues/3229
		rxhtmlTag = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,
	
		/* eslint-enable */
	
		// Support: IE <=10 - 11, Edge 12 - 13
		// In IE/Edge using regex groups here causes severe slowdowns.
		// See https://connect.microsoft.com/IE/feedback/details/1736512/
		rnoInnerhtml = /<script|<style|<link/i,
	
		// checked="checked" or checked
		rchecked = /checked\s*(?:[^=]|=\s*.checked.)/i,
		rscriptTypeMasked = /^true\/(.*)/,
		rcleanScript = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;
	
	function manipulationTarget( elem, content ) {
		if ( jQuery.nodeName( elem, "table" ) &&
			jQuery.nodeName( content.nodeType !== 11 ? content : content.firstChild, "tr" ) ) {
	
			return elem.getElementsByTagName( "tbody" )[ 0 ] || elem;
		}
	
		return elem;
	}
	
	// Replace/restore the type attribute of script elements for safe DOM manipulation
	function disableScript( elem ) {
		elem.type = ( elem.getAttribute( "type" ) !== null ) + "/" + elem.type;
		return elem;
	}
	function restoreScript( elem ) {
		var match = rscriptTypeMasked.exec( elem.type );
	
		if ( match ) {
			elem.type = match[ 1 ];
		} else {
			elem.removeAttribute( "type" );
		}
	
		return elem;
	}
	
	function cloneCopyEvent( src, dest ) {
		var i, l, type, pdataOld, pdataCur, udataOld, udataCur, events;
	
		if ( dest.nodeType !== 1 ) {
			return;
		}
	
		// 1. Copy private data: events, handlers, etc.
		if ( dataPriv.hasData( src ) ) {
			pdataOld = dataPriv.access( src );
			pdataCur = dataPriv.set( dest, pdataOld );
			events = pdataOld.events;
	
			if ( events ) {
				delete pdataCur.handle;
				pdataCur.events = {};
	
				for ( type in events ) {
					for ( i = 0, l = events[ type ].length; i < l; i++ ) {
						jQuery.event.add( dest, type, events[ type ][ i ] );
					}
				}
			}
		}
	
		// 2. Copy user data
		if ( dataUser.hasData( src ) ) {
			udataOld = dataUser.access( src );
			udataCur = jQuery.extend( {}, udataOld );
	
			dataUser.set( dest, udataCur );
		}
	}
	
	// Fix IE bugs, see support tests
	function fixInput( src, dest ) {
		var nodeName = dest.nodeName.toLowerCase();
	
		// Fails to persist the checked state of a cloned checkbox or radio button.
		if ( nodeName === "input" && rcheckableType.test( src.type ) ) {
			dest.checked = src.checked;
	
		// Fails to return the selected option to the default selected state when cloning options
		} else if ( nodeName === "input" || nodeName === "textarea" ) {
			dest.defaultValue = src.defaultValue;
		}
	}
	
	function domManip( collection, args, callback, ignored ) {
	
		// Flatten any nested arrays
		args = concat.apply( [], args );
	
		var fragment, first, scripts, hasScripts, node, doc,
			i = 0,
			l = collection.length,
			iNoClone = l - 1,
			value = args[ 0 ],
			isFunction = jQuery.isFunction( value );
	
		// We can't cloneNode fragments that contain checked, in WebKit
		if ( isFunction ||
				( l > 1 && typeof value === "string" &&
					!support.checkClone && rchecked.test( value ) ) ) {
			return collection.each( function( index ) {
				var self = collection.eq( index );
				if ( isFunction ) {
					args[ 0 ] = value.call( this, index, self.html() );
				}
				domManip( self, args, callback, ignored );
			} );
		}
	
		if ( l ) {
			fragment = buildFragment( args, collection[ 0 ].ownerDocument, false, collection, ignored );
			first = fragment.firstChild;
	
			if ( fragment.childNodes.length === 1 ) {
				fragment = first;
			}
	
			// Require either new content or an interest in ignored elements to invoke the callback
			if ( first || ignored ) {
				scripts = jQuery.map( getAll( fragment, "script" ), disableScript );
				hasScripts = scripts.length;
	
				// Use the original fragment for the last item
				// instead of the first because it can end up
				// being emptied incorrectly in certain situations (#8070).
				for ( ; i < l; i++ ) {
					node = fragment;
	
					if ( i !== iNoClone ) {
						node = jQuery.clone( node, true, true );
	
						// Keep references to cloned scripts for later restoration
						if ( hasScripts ) {
	
							// Support: Android <=4.0 only, PhantomJS 1 only
							// push.apply(_, arraylike) throws on ancient WebKit
							jQuery.merge( scripts, getAll( node, "script" ) );
						}
					}
	
					callback.call( collection[ i ], node, i );
				}
	
				if ( hasScripts ) {
					doc = scripts[ scripts.length - 1 ].ownerDocument;
	
					// Reenable scripts
					jQuery.map( scripts, restoreScript );
	
					// Evaluate executable scripts on first document insertion
					for ( i = 0; i < hasScripts; i++ ) {
						node = scripts[ i ];
						if ( rscriptType.test( node.type || "" ) &&
							!dataPriv.access( node, "globalEval" ) &&
							jQuery.contains( doc, node ) ) {
	
							if ( node.src ) {
	
								// Optional AJAX dependency, but won't run scripts if not present
								if ( jQuery._evalUrl ) {
									jQuery._evalUrl( node.src );
								}
							} else {
								DOMEval( node.textContent.replace( rcleanScript, "" ), doc );
							}
						}
					}
				}
			}
		}
	
		return collection;
	}
	
	function remove( elem, selector, keepData ) {
		var node,
			nodes = selector ? jQuery.filter( selector, elem ) : elem,
			i = 0;
	
		for ( ; ( node = nodes[ i ] ) != null; i++ ) {
			if ( !keepData && node.nodeType === 1 ) {
				jQuery.cleanData( getAll( node ) );
			}
	
			if ( node.parentNode ) {
				if ( keepData && jQuery.contains( node.ownerDocument, node ) ) {
					setGlobalEval( getAll( node, "script" ) );
				}
				node.parentNode.removeChild( node );
			}
		}
	
		return elem;
	}
	
	jQuery.extend( {
		htmlPrefilter: function( html ) {
			return html.replace( rxhtmlTag, "<$1></$2>" );
		},
	
		clone: function( elem, dataAndEvents, deepDataAndEvents ) {
			var i, l, srcElements, destElements,
				clone = elem.cloneNode( true ),
				inPage = jQuery.contains( elem.ownerDocument, elem );
	
			// Fix IE cloning issues
			if ( !support.noCloneChecked && ( elem.nodeType === 1 || elem.nodeType === 11 ) &&
					!jQuery.isXMLDoc( elem ) ) {
	
				// We eschew Sizzle here for performance reasons: https://jsperf.com/getall-vs-sizzle/2
				destElements = getAll( clone );
				srcElements = getAll( elem );
	
				for ( i = 0, l = srcElements.length; i < l; i++ ) {
					fixInput( srcElements[ i ], destElements[ i ] );
				}
			}
	
			// Copy the events from the original to the clone
			if ( dataAndEvents ) {
				if ( deepDataAndEvents ) {
					srcElements = srcElements || getAll( elem );
					destElements = destElements || getAll( clone );
	
					for ( i = 0, l = srcElements.length; i < l; i++ ) {
						cloneCopyEvent( srcElements[ i ], destElements[ i ] );
					}
				} else {
					cloneCopyEvent( elem, clone );
				}
			}
	
			// Preserve script evaluation history
			destElements = getAll( clone, "script" );
			if ( destElements.length > 0 ) {
				setGlobalEval( destElements, !inPage && getAll( elem, "script" ) );
			}
	
			// Return the cloned set
			return clone;
		},
	
		cleanData: function( elems ) {
			var data, elem, type,
				special = jQuery.event.special,
				i = 0;
	
			for ( ; ( elem = elems[ i ] ) !== undefined; i++ ) {
				if ( acceptData( elem ) ) {
					if ( ( data = elem[ dataPriv.expando ] ) ) {
						if ( data.events ) {
							for ( type in data.events ) {
								if ( special[ type ] ) {
									jQuery.event.remove( elem, type );
	
								// This is a shortcut to avoid jQuery.event.remove's overhead
								} else {
									jQuery.removeEvent( elem, type, data.handle );
								}
							}
						}
	
						// Support: Chrome <=35 - 45+
						// Assign undefined instead of using delete, see Data#remove
						elem[ dataPriv.expando ] = undefined;
					}
					if ( elem[ dataUser.expando ] ) {
	
						// Support: Chrome <=35 - 45+
						// Assign undefined instead of using delete, see Data#remove
						elem[ dataUser.expando ] = undefined;
					}
				}
			}
		}
	} );
	
	jQuery.fn.extend( {
		detach: function( selector ) {
			return remove( this, selector, true );
		},
	
		remove: function( selector ) {
			return remove( this, selector );
		},
	
		text: function( value ) {
			return access( this, function( value ) {
				return value === undefined ?
					jQuery.text( this ) :
					this.empty().each( function() {
						if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
							this.textContent = value;
						}
					} );
			}, null, value, arguments.length );
		},
	
		append: function() {
			return domManip( this, arguments, function( elem ) {
				if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
					var target = manipulationTarget( this, elem );
					target.appendChild( elem );
				}
			} );
		},
	
		prepend: function() {
			return domManip( this, arguments, function( elem ) {
				if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
					var target = manipulationTarget( this, elem );
					target.insertBefore( elem, target.firstChild );
				}
			} );
		},
	
		before: function() {
			return domManip( this, arguments, function( elem ) {
				if ( this.parentNode ) {
					this.parentNode.insertBefore( elem, this );
				}
			} );
		},
	
		after: function() {
			return domManip( this, arguments, function( elem ) {
				if ( this.parentNode ) {
					this.parentNode.insertBefore( elem, this.nextSibling );
				}
			} );
		},
	
		empty: function() {
			var elem,
				i = 0;
	
			for ( ; ( elem = this[ i ] ) != null; i++ ) {
				if ( elem.nodeType === 1 ) {
	
					// Prevent memory leaks
					jQuery.cleanData( getAll( elem, false ) );
	
					// Remove any remaining nodes
					elem.textContent = "";
				}
			}
	
			return this;
		},
	
		clone: function( dataAndEvents, deepDataAndEvents ) {
			dataAndEvents = dataAndEvents == null ? false : dataAndEvents;
			deepDataAndEvents = deepDataAndEvents == null ? dataAndEvents : deepDataAndEvents;
	
			return this.map( function() {
				return jQuery.clone( this, dataAndEvents, deepDataAndEvents );
			} );
		},
	
		html: function( value ) {
			return access( this, function( value ) {
				var elem = this[ 0 ] || {},
					i = 0,
					l = this.length;
	
				if ( value === undefined && elem.nodeType === 1 ) {
					return elem.innerHTML;
				}
	
				// See if we can take a shortcut and just use innerHTML
				if ( typeof value === "string" && !rnoInnerhtml.test( value ) &&
					!wrapMap[ ( rtagName.exec( value ) || [ "", "" ] )[ 1 ].toLowerCase() ] ) {
	
					value = jQuery.htmlPrefilter( value );
	
					try {
						for ( ; i < l; i++ ) {
							elem = this[ i ] || {};
	
							// Remove element nodes and prevent memory leaks
							if ( elem.nodeType === 1 ) {
								jQuery.cleanData( getAll( elem, false ) );
								elem.innerHTML = value;
							}
						}
	
						elem = 0;
	
					// If using innerHTML throws an exception, use the fallback method
					} catch ( e ) {}
				}
	
				if ( elem ) {
					this.empty().append( value );
				}
			}, null, value, arguments.length );
		},
	
		replaceWith: function() {
			var ignored = [];
	
			// Make the changes, replacing each non-ignored context element with the new content
			return domManip( this, arguments, function( elem ) {
				var parent = this.parentNode;
	
				if ( jQuery.inArray( this, ignored ) < 0 ) {
					jQuery.cleanData( getAll( this ) );
					if ( parent ) {
						parent.replaceChild( elem, this );
					}
				}
	
			// Force callback invocation
			}, ignored );
		}
	} );
	
	jQuery.each( {
		appendTo: "append",
		prependTo: "prepend",
		insertBefore: "before",
		insertAfter: "after",
		replaceAll: "replaceWith"
	}, function( name, original ) {
		jQuery.fn[ name ] = function( selector ) {
			var elems,
				ret = [],
				insert = jQuery( selector ),
				last = insert.length - 1,
				i = 0;
	
			for ( ; i <= last; i++ ) {
				elems = i === last ? this : this.clone( true );
				jQuery( insert[ i ] )[ original ]( elems );
	
				// Support: Android <=4.0 only, PhantomJS 1 only
				// .get() because push.apply(_, arraylike) throws on ancient WebKit
				push.apply( ret, elems.get() );
			}
	
			return this.pushStack( ret );
		};
	} );
	var rmargin = ( /^margin/ );
	
	var rnumnonpx = new RegExp( "^(" + pnum + ")(?!px)[a-z%]+$", "i" );
	
	var getStyles = function( elem ) {
	
			// Support: IE <=11 only, Firefox <=30 (#15098, #14150)
			// IE throws on elements created in popups
			// FF meanwhile throws on frame elements through "defaultView.getComputedStyle"
			var view = elem.ownerDocument.defaultView;
	
			if ( !view || !view.opener ) {
				view = window;
			}
	
			return view.getComputedStyle( elem );
		};
	
	
	
	( function() {
	
		// Executing both pixelPosition & boxSizingReliable tests require only one layout
		// so they're executed at the same time to save the second computation.
		function computeStyleTests() {
	
			// This is a singleton, we need to execute it only once
			if ( !div ) {
				return;
			}
	
			div.style.cssText =
				"box-sizing:border-box;" +
				"position:relative;display:block;" +
				"margin:auto;border:1px;padding:1px;" +
				"top:1%;width:50%";
			div.innerHTML = "";
			documentElement.appendChild( container );
	
			var divStyle = window.getComputedStyle( div );
			pixelPositionVal = divStyle.top !== "1%";
	
			// Support: Android 4.0 - 4.3 only, Firefox <=3 - 44
			reliableMarginLeftVal = divStyle.marginLeft === "2px";
			boxSizingReliableVal = divStyle.width === "4px";
	
			// Support: Android 4.0 - 4.3 only
			// Some styles come back with percentage values, even though they shouldn't
			div.style.marginRight = "50%";
			pixelMarginRightVal = divStyle.marginRight === "4px";
	
			documentElement.removeChild( container );
	
			// Nullify the div so it wouldn't be stored in the memory and
			// it will also be a sign that checks already performed
			div = null;
		}
	
		var pixelPositionVal, boxSizingReliableVal, pixelMarginRightVal, reliableMarginLeftVal,
			container = document.createElement( "div" ),
			div = document.createElement( "div" );
	
		// Finish early in limited (non-browser) environments
		if ( !div.style ) {
			return;
		}
	
		// Support: IE <=9 - 11 only
		// Style of cloned element affects source element cloned (#8908)
		div.style.backgroundClip = "content-box";
		div.cloneNode( true ).style.backgroundClip = "";
		support.clearCloneStyle = div.style.backgroundClip === "content-box";
	
		container.style.cssText = "border:0;width:8px;height:0;top:0;left:-9999px;" +
			"padding:0;margin-top:1px;position:absolute";
		container.appendChild( div );
	
		jQuery.extend( support, {
			pixelPosition: function() {
				computeStyleTests();
				return pixelPositionVal;
			},
			boxSizingReliable: function() {
				computeStyleTests();
				return boxSizingReliableVal;
			},
			pixelMarginRight: function() {
				computeStyleTests();
				return pixelMarginRightVal;
			},
			reliableMarginLeft: function() {
				computeStyleTests();
				return reliableMarginLeftVal;
			}
		} );
	} )();
	
	
	function curCSS( elem, name, computed ) {
		var width, minWidth, maxWidth, ret,
			style = elem.style;
	
		computed = computed || getStyles( elem );
	
		// Support: IE <=9 only
		// getPropertyValue is only needed for .css('filter') (#12537)
		if ( computed ) {
			ret = computed.getPropertyValue( name ) || computed[ name ];
	
			if ( ret === "" && !jQuery.contains( elem.ownerDocument, elem ) ) {
				ret = jQuery.style( elem, name );
			}
	
			// A tribute to the "awesome hack by Dean Edwards"
			// Android Browser returns percentage for some values,
			// but width seems to be reliably pixels.
			// This is against the CSSOM draft spec:
			// https://drafts.csswg.org/cssom/#resolved-values
			if ( !support.pixelMarginRight() && rnumnonpx.test( ret ) && rmargin.test( name ) ) {
	
				// Remember the original values
				width = style.width;
				minWidth = style.minWidth;
				maxWidth = style.maxWidth;
	
				// Put in the new values to get a computed value out
				style.minWidth = style.maxWidth = style.width = ret;
				ret = computed.width;
	
				// Revert the changed values
				style.width = width;
				style.minWidth = minWidth;
				style.maxWidth = maxWidth;
			}
		}
	
		return ret !== undefined ?
	
			// Support: IE <=9 - 11 only
			// IE returns zIndex value as an integer.
			ret + "" :
			ret;
	}
	
	
	function addGetHookIf( conditionFn, hookFn ) {
	
		// Define the hook, we'll check on the first run if it's really needed.
		return {
			get: function() {
				if ( conditionFn() ) {
	
					// Hook not needed (or it's not possible to use it due
					// to missing dependency), remove it.
					delete this.get;
					return;
				}
	
				// Hook needed; redefine it so that the support test is not executed again.
				return ( this.get = hookFn ).apply( this, arguments );
			}
		};
	}
	
	
	var
	
		// Swappable if display is none or starts with table
		// except "table", "table-cell", or "table-caption"
		// See here for display values: https://developer.mozilla.org/en-US/docs/CSS/display
		rdisplayswap = /^(none|table(?!-c[ea]).+)/,
		cssShow = { position: "absolute", visibility: "hidden", display: "block" },
		cssNormalTransform = {
			letterSpacing: "0",
			fontWeight: "400"
		},
	
		cssPrefixes = [ "Webkit", "Moz", "ms" ],
		emptyStyle = document.createElement( "div" ).style;
	
	// Return a css property mapped to a potentially vendor prefixed property
	function vendorPropName( name ) {
	
		// Shortcut for names that are not vendor prefixed
		if ( name in emptyStyle ) {
			return name;
		}
	
		// Check for vendor prefixed names
		var capName = name[ 0 ].toUpperCase() + name.slice( 1 ),
			i = cssPrefixes.length;
	
		while ( i-- ) {
			name = cssPrefixes[ i ] + capName;
			if ( name in emptyStyle ) {
				return name;
			}
		}
	}
	
	function setPositiveNumber( elem, value, subtract ) {
	
		// Any relative (+/-) values have already been
		// normalized at this point
		var matches = rcssNum.exec( value );
		return matches ?
	
			// Guard against undefined "subtract", e.g., when used as in cssHooks
			Math.max( 0, matches[ 2 ] - ( subtract || 0 ) ) + ( matches[ 3 ] || "px" ) :
			value;
	}
	
	function augmentWidthOrHeight( elem, name, extra, isBorderBox, styles ) {
		var i,
			val = 0;
	
		// If we already have the right measurement, avoid augmentation
		if ( extra === ( isBorderBox ? "border" : "content" ) ) {
			i = 4;
	
		// Otherwise initialize for horizontal or vertical properties
		} else {
			i = name === "width" ? 1 : 0;
		}
	
		for ( ; i < 4; i += 2 ) {
	
			// Both box models exclude margin, so add it if we want it
			if ( extra === "margin" ) {
				val += jQuery.css( elem, extra + cssExpand[ i ], true, styles );
			}
	
			if ( isBorderBox ) {
	
				// border-box includes padding, so remove it if we want content
				if ( extra === "content" ) {
					val -= jQuery.css( elem, "padding" + cssExpand[ i ], true, styles );
				}
	
				// At this point, extra isn't border nor margin, so remove border
				if ( extra !== "margin" ) {
					val -= jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );
				}
			} else {
	
				// At this point, extra isn't content, so add padding
				val += jQuery.css( elem, "padding" + cssExpand[ i ], true, styles );
	
				// At this point, extra isn't content nor padding, so add border
				if ( extra !== "padding" ) {
					val += jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );
				}
			}
		}
	
		return val;
	}
	
	function getWidthOrHeight( elem, name, extra ) {
	
		// Start with offset property, which is equivalent to the border-box value
		var val,
			valueIsBorderBox = true,
			styles = getStyles( elem ),
			isBorderBox = jQuery.css( elem, "boxSizing", false, styles ) === "border-box";
	
		// Support: IE <=11 only
		// Running getBoundingClientRect on a disconnected node
		// in IE throws an error.
		if ( elem.getClientRects().length ) {
			val = elem.getBoundingClientRect()[ name ];
		}
	
		// Some non-html elements return undefined for offsetWidth, so check for null/undefined
		// svg - https://bugzilla.mozilla.org/show_bug.cgi?id=649285
		// MathML - https://bugzilla.mozilla.org/show_bug.cgi?id=491668
		if ( val <= 0 || val == null ) {
	
			// Fall back to computed then uncomputed css if necessary
			val = curCSS( elem, name, styles );
			if ( val < 0 || val == null ) {
				val = elem.style[ name ];
			}
	
			// Computed unit is not pixels. Stop here and return.
			if ( rnumnonpx.test( val ) ) {
				return val;
			}
	
			// Check for style in case a browser which returns unreliable values
			// for getComputedStyle silently falls back to the reliable elem.style
			valueIsBorderBox = isBorderBox &&
				( support.boxSizingReliable() || val === elem.style[ name ] );
	
			// Normalize "", auto, and prepare for extra
			val = parseFloat( val ) || 0;
		}
	
		// Use the active box-sizing model to add/subtract irrelevant styles
		return ( val +
			augmentWidthOrHeight(
				elem,
				name,
				extra || ( isBorderBox ? "border" : "content" ),
				valueIsBorderBox,
				styles
			)
		) + "px";
	}
	
	jQuery.extend( {
	
		// Add in style property hooks for overriding the default
		// behavior of getting and setting a style property
		cssHooks: {
			opacity: {
				get: function( elem, computed ) {
					if ( computed ) {
	
						// We should always get a number back from opacity
						var ret = curCSS( elem, "opacity" );
						return ret === "" ? "1" : ret;
					}
				}
			}
		},
	
		// Don't automatically add "px" to these possibly-unitless properties
		cssNumber: {
			"animationIterationCount": true,
			"columnCount": true,
			"fillOpacity": true,
			"flexGrow": true,
			"flexShrink": true,
			"fontWeight": true,
			"lineHeight": true,
			"opacity": true,
			"order": true,
			"orphans": true,
			"widows": true,
			"zIndex": true,
			"zoom": true
		},
	
		// Add in properties whose names you wish to fix before
		// setting or getting the value
		cssProps: {
			"float": "cssFloat"
		},
	
		// Get and set the style property on a DOM Node
		style: function( elem, name, value, extra ) {
	
			// Don't set styles on text and comment nodes
			if ( !elem || elem.nodeType === 3 || elem.nodeType === 8 || !elem.style ) {
				return;
			}
	
			// Make sure that we're working with the right name
			var ret, type, hooks,
				origName = jQuery.camelCase( name ),
				style = elem.style;
	
			name = jQuery.cssProps[ origName ] ||
				( jQuery.cssProps[ origName ] = vendorPropName( origName ) || origName );
	
			// Gets hook for the prefixed version, then unprefixed version
			hooks = jQuery.cssHooks[ name ] || jQuery.cssHooks[ origName ];
	
			// Check if we're setting a value
			if ( value !== undefined ) {
				type = typeof value;
	
				// Convert "+=" or "-=" to relative numbers (#7345)
				if ( type === "string" && ( ret = rcssNum.exec( value ) ) && ret[ 1 ] ) {
					value = adjustCSS( elem, name, ret );
	
					// Fixes bug #9237
					type = "number";
				}
	
				// Make sure that null and NaN values aren't set (#7116)
				if ( value == null || value !== value ) {
					return;
				}
	
				// If a number was passed in, add the unit (except for certain CSS properties)
				if ( type === "number" ) {
					value += ret && ret[ 3 ] || ( jQuery.cssNumber[ origName ] ? "" : "px" );
				}
	
				// background-* props affect original clone's values
				if ( !support.clearCloneStyle && value === "" && name.indexOf( "background" ) === 0 ) {
					style[ name ] = "inherit";
				}
	
				// If a hook was provided, use that value, otherwise just set the specified value
				if ( !hooks || !( "set" in hooks ) ||
					( value = hooks.set( elem, value, extra ) ) !== undefined ) {
	
					style[ name ] = value;
				}
	
			} else {
	
				// If a hook was provided get the non-computed value from there
				if ( hooks && "get" in hooks &&
					( ret = hooks.get( elem, false, extra ) ) !== undefined ) {
	
					return ret;
				}
	
				// Otherwise just get the value from the style object
				return style[ name ];
			}
		},
	
		css: function( elem, name, extra, styles ) {
			var val, num, hooks,
				origName = jQuery.camelCase( name );
	
			// Make sure that we're working with the right name
			name = jQuery.cssProps[ origName ] ||
				( jQuery.cssProps[ origName ] = vendorPropName( origName ) || origName );
	
			// Try prefixed name followed by the unprefixed name
			hooks = jQuery.cssHooks[ name ] || jQuery.cssHooks[ origName ];
	
			// If a hook was provided get the computed value from there
			if ( hooks && "get" in hooks ) {
				val = hooks.get( elem, true, extra );
			}
	
			// Otherwise, if a way to get the computed value exists, use that
			if ( val === undefined ) {
				val = curCSS( elem, name, styles );
			}
	
			// Convert "normal" to computed value
			if ( val === "normal" && name in cssNormalTransform ) {
				val = cssNormalTransform[ name ];
			}
	
			// Make numeric if forced or a qualifier was provided and val looks numeric
			if ( extra === "" || extra ) {
				num = parseFloat( val );
				return extra === true || isFinite( num ) ? num || 0 : val;
			}
			return val;
		}
	} );
	
	jQuery.each( [ "height", "width" ], function( i, name ) {
		jQuery.cssHooks[ name ] = {
			get: function( elem, computed, extra ) {
				if ( computed ) {
	
					// Certain elements can have dimension info if we invisibly show them
					// but it must have a current display style that would benefit
					return rdisplayswap.test( jQuery.css( elem, "display" ) ) &&
	
						// Support: Safari 8+
						// Table columns in Safari have non-zero offsetWidth & zero
						// getBoundingClientRect().width unless display is changed.
						// Support: IE <=11 only
						// Running getBoundingClientRect on a disconnected node
						// in IE throws an error.
						( !elem.getClientRects().length || !elem.getBoundingClientRect().width ) ?
							swap( elem, cssShow, function() {
								return getWidthOrHeight( elem, name, extra );
							} ) :
							getWidthOrHeight( elem, name, extra );
				}
			},
	
			set: function( elem, value, extra ) {
				var matches,
					styles = extra && getStyles( elem ),
					subtract = extra && augmentWidthOrHeight(
						elem,
						name,
						extra,
						jQuery.css( elem, "boxSizing", false, styles ) === "border-box",
						styles
					);
	
				// Convert to pixels if value adjustment is needed
				if ( subtract && ( matches = rcssNum.exec( value ) ) &&
					( matches[ 3 ] || "px" ) !== "px" ) {
	
					elem.style[ name ] = value;
					value = jQuery.css( elem, name );
				}
	
				return setPositiveNumber( elem, value, subtract );
			}
		};
	} );
	
	jQuery.cssHooks.marginLeft = addGetHookIf( support.reliableMarginLeft,
		function( elem, computed ) {
			if ( computed ) {
				return ( parseFloat( curCSS( elem, "marginLeft" ) ) ||
					elem.getBoundingClientRect().left -
						swap( elem, { marginLeft: 0 }, function() {
							return elem.getBoundingClientRect().left;
						} )
					) + "px";
			}
		}
	);
	
	// These hooks are used by animate to expand properties
	jQuery.each( {
		margin: "",
		padding: "",
		border: "Width"
	}, function( prefix, suffix ) {
		jQuery.cssHooks[ prefix + suffix ] = {
			expand: function( value ) {
				var i = 0,
					expanded = {},
	
					// Assumes a single number if not a string
					parts = typeof value === "string" ? value.split( " " ) : [ value ];
	
				for ( ; i < 4; i++ ) {
					expanded[ prefix + cssExpand[ i ] + suffix ] =
						parts[ i ] || parts[ i - 2 ] || parts[ 0 ];
				}
	
				return expanded;
			}
		};
	
		if ( !rmargin.test( prefix ) ) {
			jQuery.cssHooks[ prefix + suffix ].set = setPositiveNumber;
		}
	} );
	
	jQuery.fn.extend( {
		css: function( name, value ) {
			return access( this, function( elem, name, value ) {
				var styles, len,
					map = {},
					i = 0;
	
				if ( jQuery.isArray( name ) ) {
					styles = getStyles( elem );
					len = name.length;
	
					for ( ; i < len; i++ ) {
						map[ name[ i ] ] = jQuery.css( elem, name[ i ], false, styles );
					}
	
					return map;
				}
	
				return value !== undefined ?
					jQuery.style( elem, name, value ) :
					jQuery.css( elem, name );
			}, name, value, arguments.length > 1 );
		}
	} );
	
	
	function Tween( elem, options, prop, end, easing ) {
		return new Tween.prototype.init( elem, options, prop, end, easing );
	}
	jQuery.Tween = Tween;
	
	Tween.prototype = {
		constructor: Tween,
		init: function( elem, options, prop, end, easing, unit ) {
			this.elem = elem;
			this.prop = prop;
			this.easing = easing || jQuery.easing._default;
			this.options = options;
			this.start = this.now = this.cur();
			this.end = end;
			this.unit = unit || ( jQuery.cssNumber[ prop ] ? "" : "px" );
		},
		cur: function() {
			var hooks = Tween.propHooks[ this.prop ];
	
			return hooks && hooks.get ?
				hooks.get( this ) :
				Tween.propHooks._default.get( this );
		},
		run: function( percent ) {
			var eased,
				hooks = Tween.propHooks[ this.prop ];
	
			if ( this.options.duration ) {
				this.pos = eased = jQuery.easing[ this.easing ](
					percent, this.options.duration * percent, 0, 1, this.options.duration
				);
			} else {
				this.pos = eased = percent;
			}
			this.now = ( this.end - this.start ) * eased + this.start;
	
			if ( this.options.step ) {
				this.options.step.call( this.elem, this.now, this );
			}
	
			if ( hooks && hooks.set ) {
				hooks.set( this );
			} else {
				Tween.propHooks._default.set( this );
			}
			return this;
		}
	};
	
	Tween.prototype.init.prototype = Tween.prototype;
	
	Tween.propHooks = {
		_default: {
			get: function( tween ) {
				var result;
	
				// Use a property on the element directly when it is not a DOM element,
				// or when there is no matching style property that exists.
				if ( tween.elem.nodeType !== 1 ||
					tween.elem[ tween.prop ] != null && tween.elem.style[ tween.prop ] == null ) {
					return tween.elem[ tween.prop ];
				}
	
				// Passing an empty string as a 3rd parameter to .css will automatically
				// attempt a parseFloat and fallback to a string if the parse fails.
				// Simple values such as "10px" are parsed to Float;
				// complex values such as "rotate(1rad)" are returned as-is.
				result = jQuery.css( tween.elem, tween.prop, "" );
	
				// Empty strings, null, undefined and "auto" are converted to 0.
				return !result || result === "auto" ? 0 : result;
			},
			set: function( tween ) {
	
				// Use step hook for back compat.
				// Use cssHook if its there.
				// Use .style if available and use plain properties where available.
				if ( jQuery.fx.step[ tween.prop ] ) {
					jQuery.fx.step[ tween.prop ]( tween );
				} else if ( tween.elem.nodeType === 1 &&
					( tween.elem.style[ jQuery.cssProps[ tween.prop ] ] != null ||
						jQuery.cssHooks[ tween.prop ] ) ) {
					jQuery.style( tween.elem, tween.prop, tween.now + tween.unit );
				} else {
					tween.elem[ tween.prop ] = tween.now;
				}
			}
		}
	};
	
	// Support: IE <=9 only
	// Panic based approach to setting things on disconnected nodes
	Tween.propHooks.scrollTop = Tween.propHooks.scrollLeft = {
		set: function( tween ) {
			if ( tween.elem.nodeType && tween.elem.parentNode ) {
				tween.elem[ tween.prop ] = tween.now;
			}
		}
	};
	
	jQuery.easing = {
		linear: function( p ) {
			return p;
		},
		swing: function( p ) {
			return 0.5 - Math.cos( p * Math.PI ) / 2;
		},
		_default: "swing"
	};
	
	jQuery.fx = Tween.prototype.init;
	
	// Back compat <1.8 extension point
	jQuery.fx.step = {};
	
	
	
	
	var
		fxNow, timerId,
		rfxtypes = /^(?:toggle|show|hide)$/,
		rrun = /queueHooks$/;
	
	function raf() {
		if ( timerId ) {
			window.requestAnimationFrame( raf );
			jQuery.fx.tick();
		}
	}
	
	// Animations created synchronously will run synchronously
	function createFxNow() {
		window.setTimeout( function() {
			fxNow = undefined;
		} );
		return ( fxNow = jQuery.now() );
	}
	
	// Generate parameters to create a standard animation
	function genFx( type, includeWidth ) {
		var which,
			i = 0,
			attrs = { height: type };
	
		// If we include width, step value is 1 to do all cssExpand values,
		// otherwise step value is 2 to skip over Left and Right
		includeWidth = includeWidth ? 1 : 0;
		for ( ; i < 4; i += 2 - includeWidth ) {
			which = cssExpand[ i ];
			attrs[ "margin" + which ] = attrs[ "padding" + which ] = type;
		}
	
		if ( includeWidth ) {
			attrs.opacity = attrs.width = type;
		}
	
		return attrs;
	}
	
	function createTween( value, prop, animation ) {
		var tween,
			collection = ( Animation.tweeners[ prop ] || [] ).concat( Animation.tweeners[ "*" ] ),
			index = 0,
			length = collection.length;
		for ( ; index < length; index++ ) {
			if ( ( tween = collection[ index ].call( animation, prop, value ) ) ) {
	
				// We're done with this property
				return tween;
			}
		}
	}
	
	function defaultPrefilter( elem, props, opts ) {
		var prop, value, toggle, hooks, oldfire, propTween, restoreDisplay, display,
			isBox = "width" in props || "height" in props,
			anim = this,
			orig = {},
			style = elem.style,
			hidden = elem.nodeType && isHiddenWithinTree( elem ),
			dataShow = dataPriv.get( elem, "fxshow" );
	
		// Queue-skipping animations hijack the fx hooks
		if ( !opts.queue ) {
			hooks = jQuery._queueHooks( elem, "fx" );
			if ( hooks.unqueued == null ) {
				hooks.unqueued = 0;
				oldfire = hooks.empty.fire;
				hooks.empty.fire = function() {
					if ( !hooks.unqueued ) {
						oldfire();
					}
				};
			}
			hooks.unqueued++;
	
			anim.always( function() {
	
				// Ensure the complete handler is called before this completes
				anim.always( function() {
					hooks.unqueued--;
					if ( !jQuery.queue( elem, "fx" ).length ) {
						hooks.empty.fire();
					}
				} );
			} );
		}
	
		// Detect show/hide animations
		for ( prop in props ) {
			value = props[ prop ];
			if ( rfxtypes.test( value ) ) {
				delete props[ prop ];
				toggle = toggle || value === "toggle";
				if ( value === ( hidden ? "hide" : "show" ) ) {
	
					// Pretend to be hidden if this is a "show" and
					// there is still data from a stopped show/hide
					if ( value === "show" && dataShow && dataShow[ prop ] !== undefined ) {
						hidden = true;
	
					// Ignore all other no-op show/hide data
					} else {
						continue;
					}
				}
				orig[ prop ] = dataShow && dataShow[ prop ] || jQuery.style( elem, prop );
			}
		}
	
		// Bail out if this is a no-op like .hide().hide()
		propTween = !jQuery.isEmptyObject( props );
		if ( !propTween && jQuery.isEmptyObject( orig ) ) {
			return;
		}
	
		// Restrict "overflow" and "display" styles during box animations
		if ( isBox && elem.nodeType === 1 ) {
	
			// Support: IE <=9 - 11, Edge 12 - 13
			// Record all 3 overflow attributes because IE does not infer the shorthand
			// from identically-valued overflowX and overflowY
			opts.overflow = [ style.overflow, style.overflowX, style.overflowY ];
	
			// Identify a display type, preferring old show/hide data over the CSS cascade
			restoreDisplay = dataShow && dataShow.display;
			if ( restoreDisplay == null ) {
				restoreDisplay = dataPriv.get( elem, "display" );
			}
			display = jQuery.css( elem, "display" );
			if ( display === "none" ) {
				if ( restoreDisplay ) {
					display = restoreDisplay;
				} else {
	
					// Get nonempty value(s) by temporarily forcing visibility
					showHide( [ elem ], true );
					restoreDisplay = elem.style.display || restoreDisplay;
					display = jQuery.css( elem, "display" );
					showHide( [ elem ] );
				}
			}
	
			// Animate inline elements as inline-block
			if ( display === "inline" || display === "inline-block" && restoreDisplay != null ) {
				if ( jQuery.css( elem, "float" ) === "none" ) {
	
					// Restore the original display value at the end of pure show/hide animations
					if ( !propTween ) {
						anim.done( function() {
							style.display = restoreDisplay;
						} );
						if ( restoreDisplay == null ) {
							display = style.display;
							restoreDisplay = display === "none" ? "" : display;
						}
					}
					style.display = "inline-block";
				}
			}
		}
	
		if ( opts.overflow ) {
			style.overflow = "hidden";
			anim.always( function() {
				style.overflow = opts.overflow[ 0 ];
				style.overflowX = opts.overflow[ 1 ];
				style.overflowY = opts.overflow[ 2 ];
			} );
		}
	
		// Implement show/hide animations
		propTween = false;
		for ( prop in orig ) {
	
			// General show/hide setup for this element animation
			if ( !propTween ) {
				if ( dataShow ) {
					if ( "hidden" in dataShow ) {
						hidden = dataShow.hidden;
					}
				} else {
					dataShow = dataPriv.access( elem, "fxshow", { display: restoreDisplay } );
				}
	
				// Store hidden/visible for toggle so `.stop().toggle()` "reverses"
				if ( toggle ) {
					dataShow.hidden = !hidden;
				}
	
				// Show elements before animating them
				if ( hidden ) {
					showHide( [ elem ], true );
				}
	
				/* eslint-disable no-loop-func */
	
				anim.done( function() {
	
				/* eslint-enable no-loop-func */
	
					// The final step of a "hide" animation is actually hiding the element
					if ( !hidden ) {
						showHide( [ elem ] );
					}
					dataPriv.remove( elem, "fxshow" );
					for ( prop in orig ) {
						jQuery.style( elem, prop, orig[ prop ] );
					}
				} );
			}
	
			// Per-property setup
			propTween = createTween( hidden ? dataShow[ prop ] : 0, prop, anim );
			if ( !( prop in dataShow ) ) {
				dataShow[ prop ] = propTween.start;
				if ( hidden ) {
					propTween.end = propTween.start;
					propTween.start = 0;
				}
			}
		}
	}
	
	function propFilter( props, specialEasing ) {
		var index, name, easing, value, hooks;
	
		// camelCase, specialEasing and expand cssHook pass
		for ( index in props ) {
			name = jQuery.camelCase( index );
			easing = specialEasing[ name ];
			value = props[ index ];
			if ( jQuery.isArray( value ) ) {
				easing = value[ 1 ];
				value = props[ index ] = value[ 0 ];
			}
	
			if ( index !== name ) {
				props[ name ] = value;
				delete props[ index ];
			}
	
			hooks = jQuery.cssHooks[ name ];
			if ( hooks && "expand" in hooks ) {
				value = hooks.expand( value );
				delete props[ name ];
	
				// Not quite $.extend, this won't overwrite existing keys.
				// Reusing 'index' because we have the correct "name"
				for ( index in value ) {
					if ( !( index in props ) ) {
						props[ index ] = value[ index ];
						specialEasing[ index ] = easing;
					}
				}
			} else {
				specialEasing[ name ] = easing;
			}
		}
	}
	
	function Animation( elem, properties, options ) {
		var result,
			stopped,
			index = 0,
			length = Animation.prefilters.length,
			deferred = jQuery.Deferred().always( function() {
	
				// Don't match elem in the :animated selector
				delete tick.elem;
			} ),
			tick = function() {
				if ( stopped ) {
					return false;
				}
				var currentTime = fxNow || createFxNow(),
					remaining = Math.max( 0, animation.startTime + animation.duration - currentTime ),
	
					// Support: Android 2.3 only
					// Archaic crash bug won't allow us to use `1 - ( 0.5 || 0 )` (#12497)
					temp = remaining / animation.duration || 0,
					percent = 1 - temp,
					index = 0,
					length = animation.tweens.length;
	
				for ( ; index < length; index++ ) {
					animation.tweens[ index ].run( percent );
				}
	
				deferred.notifyWith( elem, [ animation, percent, remaining ] );
	
				if ( percent < 1 && length ) {
					return remaining;
				} else {
					deferred.resolveWith( elem, [ animation ] );
					return false;
				}
			},
			animation = deferred.promise( {
				elem: elem,
				props: jQuery.extend( {}, properties ),
				opts: jQuery.extend( true, {
					specialEasing: {},
					easing: jQuery.easing._default
				}, options ),
				originalProperties: properties,
				originalOptions: options,
				startTime: fxNow || createFxNow(),
				duration: options.duration,
				tweens: [],
				createTween: function( prop, end ) {
					var tween = jQuery.Tween( elem, animation.opts, prop, end,
							animation.opts.specialEasing[ prop ] || animation.opts.easing );
					animation.tweens.push( tween );
					return tween;
				},
				stop: function( gotoEnd ) {
					var index = 0,
	
						// If we are going to the end, we want to run all the tweens
						// otherwise we skip this part
						length = gotoEnd ? animation.tweens.length : 0;
					if ( stopped ) {
						return this;
					}
					stopped = true;
					for ( ; index < length; index++ ) {
						animation.tweens[ index ].run( 1 );
					}
	
					// Resolve when we played the last frame; otherwise, reject
					if ( gotoEnd ) {
						deferred.notifyWith( elem, [ animation, 1, 0 ] );
						deferred.resolveWith( elem, [ animation, gotoEnd ] );
					} else {
						deferred.rejectWith( elem, [ animation, gotoEnd ] );
					}
					return this;
				}
			} ),
			props = animation.props;
	
		propFilter( props, animation.opts.specialEasing );
	
		for ( ; index < length; index++ ) {
			result = Animation.prefilters[ index ].call( animation, elem, props, animation.opts );
			if ( result ) {
				if ( jQuery.isFunction( result.stop ) ) {
					jQuery._queueHooks( animation.elem, animation.opts.queue ).stop =
						jQuery.proxy( result.stop, result );
				}
				return result;
			}
		}
	
		jQuery.map( props, createTween, animation );
	
		if ( jQuery.isFunction( animation.opts.start ) ) {
			animation.opts.start.call( elem, animation );
		}
	
		jQuery.fx.timer(
			jQuery.extend( tick, {
				elem: elem,
				anim: animation,
				queue: animation.opts.queue
			} )
		);
	
		// attach callbacks from options
		return animation.progress( animation.opts.progress )
			.done( animation.opts.done, animation.opts.complete )
			.fail( animation.opts.fail )
			.always( animation.opts.always );
	}
	
	jQuery.Animation = jQuery.extend( Animation, {
	
		tweeners: {
			"*": [ function( prop, value ) {
				var tween = this.createTween( prop, value );
				adjustCSS( tween.elem, prop, rcssNum.exec( value ), tween );
				return tween;
			} ]
		},
	
		tweener: function( props, callback ) {
			if ( jQuery.isFunction( props ) ) {
				callback = props;
				props = [ "*" ];
			} else {
				props = props.match( rnothtmlwhite );
			}
	
			var prop,
				index = 0,
				length = props.length;
	
			for ( ; index < length; index++ ) {
				prop = props[ index ];
				Animation.tweeners[ prop ] = Animation.tweeners[ prop ] || [];
				Animation.tweeners[ prop ].unshift( callback );
			}
		},
	
		prefilters: [ defaultPrefilter ],
	
		prefilter: function( callback, prepend ) {
			if ( prepend ) {
				Animation.prefilters.unshift( callback );
			} else {
				Animation.prefilters.push( callback );
			}
		}
	} );
	
	jQuery.speed = function( speed, easing, fn ) {
		var opt = speed && typeof speed === "object" ? jQuery.extend( {}, speed ) : {
			complete: fn || !fn && easing ||
				jQuery.isFunction( speed ) && speed,
			duration: speed,
			easing: fn && easing || easing && !jQuery.isFunction( easing ) && easing
		};
	
		// Go to the end state if fx are off or if document is hidden
		if ( jQuery.fx.off || document.hidden ) {
			opt.duration = 0;
	
		} else {
			if ( typeof opt.duration !== "number" ) {
				if ( opt.duration in jQuery.fx.speeds ) {
					opt.duration = jQuery.fx.speeds[ opt.duration ];
	
				} else {
					opt.duration = jQuery.fx.speeds._default;
				}
			}
		}
	
		// Normalize opt.queue - true/undefined/null -> "fx"
		if ( opt.queue == null || opt.queue === true ) {
			opt.queue = "fx";
		}
	
		// Queueing
		opt.old = opt.complete;
	
		opt.complete = function() {
			if ( jQuery.isFunction( opt.old ) ) {
				opt.old.call( this );
			}
	
			if ( opt.queue ) {
				jQuery.dequeue( this, opt.queue );
			}
		};
	
		return opt;
	};
	
	jQuery.fn.extend( {
		fadeTo: function( speed, to, easing, callback ) {
	
			// Show any hidden elements after setting opacity to 0
			return this.filter( isHiddenWithinTree ).css( "opacity", 0 ).show()
	
				// Animate to the value specified
				.end().animate( { opacity: to }, speed, easing, callback );
		},
		animate: function( prop, speed, easing, callback ) {
			var empty = jQuery.isEmptyObject( prop ),
				optall = jQuery.speed( speed, easing, callback ),
				doAnimation = function() {
	
					// Operate on a copy of prop so per-property easing won't be lost
					var anim = Animation( this, jQuery.extend( {}, prop ), optall );
	
					// Empty animations, or finishing resolves immediately
					if ( empty || dataPriv.get( this, "finish" ) ) {
						anim.stop( true );
					}
				};
				doAnimation.finish = doAnimation;
	
			return empty || optall.queue === false ?
				this.each( doAnimation ) :
				this.queue( optall.queue, doAnimation );
		},
		stop: function( type, clearQueue, gotoEnd ) {
			var stopQueue = function( hooks ) {
				var stop = hooks.stop;
				delete hooks.stop;
				stop( gotoEnd );
			};
	
			if ( typeof type !== "string" ) {
				gotoEnd = clearQueue;
				clearQueue = type;
				type = undefined;
			}
			if ( clearQueue && type !== false ) {
				this.queue( type || "fx", [] );
			}
	
			return this.each( function() {
				var dequeue = true,
					index = type != null && type + "queueHooks",
					timers = jQuery.timers,
					data = dataPriv.get( this );
	
				if ( index ) {
					if ( data[ index ] && data[ index ].stop ) {
						stopQueue( data[ index ] );
					}
				} else {
					for ( index in data ) {
						if ( data[ index ] && data[ index ].stop && rrun.test( index ) ) {
							stopQueue( data[ index ] );
						}
					}
				}
	
				for ( index = timers.length; index--; ) {
					if ( timers[ index ].elem === this &&
						( type == null || timers[ index ].queue === type ) ) {
	
						timers[ index ].anim.stop( gotoEnd );
						dequeue = false;
						timers.splice( index, 1 );
					}
				}
	
				// Start the next in the queue if the last step wasn't forced.
				// Timers currently will call their complete callbacks, which
				// will dequeue but only if they were gotoEnd.
				if ( dequeue || !gotoEnd ) {
					jQuery.dequeue( this, type );
				}
			} );
		},
		finish: function( type ) {
			if ( type !== false ) {
				type = type || "fx";
			}
			return this.each( function() {
				var index,
					data = dataPriv.get( this ),
					queue = data[ type + "queue" ],
					hooks = data[ type + "queueHooks" ],
					timers = jQuery.timers,
					length = queue ? queue.length : 0;
	
				// Enable finishing flag on private data
				data.finish = true;
	
				// Empty the queue first
				jQuery.queue( this, type, [] );
	
				if ( hooks && hooks.stop ) {
					hooks.stop.call( this, true );
				}
	
				// Look for any active animations, and finish them
				for ( index = timers.length; index--; ) {
					if ( timers[ index ].elem === this && timers[ index ].queue === type ) {
						timers[ index ].anim.stop( true );
						timers.splice( index, 1 );
					}
				}
	
				// Look for any animations in the old queue and finish them
				for ( index = 0; index < length; index++ ) {
					if ( queue[ index ] && queue[ index ].finish ) {
						queue[ index ].finish.call( this );
					}
				}
	
				// Turn off finishing flag
				delete data.finish;
			} );
		}
	} );
	
	jQuery.each( [ "toggle", "show", "hide" ], function( i, name ) {
		var cssFn = jQuery.fn[ name ];
		jQuery.fn[ name ] = function( speed, easing, callback ) {
			return speed == null || typeof speed === "boolean" ?
				cssFn.apply( this, arguments ) :
				this.animate( genFx( name, true ), speed, easing, callback );
		};
	} );
	
	// Generate shortcuts for custom animations
	jQuery.each( {
		slideDown: genFx( "show" ),
		slideUp: genFx( "hide" ),
		slideToggle: genFx( "toggle" ),
		fadeIn: { opacity: "show" },
		fadeOut: { opacity: "hide" },
		fadeToggle: { opacity: "toggle" }
	}, function( name, props ) {
		jQuery.fn[ name ] = function( speed, easing, callback ) {
			return this.animate( props, speed, easing, callback );
		};
	} );
	
	jQuery.timers = [];
	jQuery.fx.tick = function() {
		var timer,
			i = 0,
			timers = jQuery.timers;
	
		fxNow = jQuery.now();
	
		for ( ; i < timers.length; i++ ) {
			timer = timers[ i ];
	
			// Checks the timer has not already been removed
			if ( !timer() && timers[ i ] === timer ) {
				timers.splice( i--, 1 );
			}
		}
	
		if ( !timers.length ) {
			jQuery.fx.stop();
		}
		fxNow = undefined;
	};
	
	jQuery.fx.timer = function( timer ) {
		jQuery.timers.push( timer );
		if ( timer() ) {
			jQuery.fx.start();
		} else {
			jQuery.timers.pop();
		}
	};
	
	jQuery.fx.interval = 13;
	jQuery.fx.start = function() {
		if ( !timerId ) {
			timerId = window.requestAnimationFrame ?
				window.requestAnimationFrame( raf ) :
				window.setInterval( jQuery.fx.tick, jQuery.fx.interval );
		}
	};
	
	jQuery.fx.stop = function() {
		if ( window.cancelAnimationFrame ) {
			window.cancelAnimationFrame( timerId );
		} else {
			window.clearInterval( timerId );
		}
	
		timerId = null;
	};
	
	jQuery.fx.speeds = {
		slow: 600,
		fast: 200,
	
		// Default speed
		_default: 400
	};
	
	
	// Based off of the plugin by Clint Helfers, with permission.
	// https://web.archive.org/web/20100324014747/http://blindsignals.com/index.php/2009/07/jquery-delay/
	jQuery.fn.delay = function( time, type ) {
		time = jQuery.fx ? jQuery.fx.speeds[ time ] || time : time;
		type = type || "fx";
	
		return this.queue( type, function( next, hooks ) {
			var timeout = window.setTimeout( next, time );
			hooks.stop = function() {
				window.clearTimeout( timeout );
			};
		} );
	};
	
	
	( function() {
		var input = document.createElement( "input" ),
			select = document.createElement( "select" ),
			opt = select.appendChild( document.createElement( "option" ) );
	
		input.type = "checkbox";
	
		// Support: Android <=4.3 only
		// Default value for a checkbox should be "on"
		support.checkOn = input.value !== "";
	
		// Support: IE <=11 only
		// Must access selectedIndex to make default options select
		support.optSelected = opt.selected;
	
		// Support: IE <=11 only
		// An input loses its value after becoming a radio
		input = document.createElement( "input" );
		input.value = "t";
		input.type = "radio";
		support.radioValue = input.value === "t";
	} )();
	
	
	var boolHook,
		attrHandle = jQuery.expr.attrHandle;
	
	jQuery.fn.extend( {
		attr: function( name, value ) {
			return access( this, jQuery.attr, name, value, arguments.length > 1 );
		},
	
		removeAttr: function( name ) {
			return this.each( function() {
				jQuery.removeAttr( this, name );
			} );
		}
	} );
	
	jQuery.extend( {
		attr: function( elem, name, value ) {
			var ret, hooks,
				nType = elem.nodeType;
	
			// Don't get/set attributes on text, comment and attribute nodes
			if ( nType === 3 || nType === 8 || nType === 2 ) {
				return;
			}
	
			// Fallback to prop when attributes are not supported
			if ( typeof elem.getAttribute === "undefined" ) {
				return jQuery.prop( elem, name, value );
			}
	
			// Attribute hooks are determined by the lowercase version
			// Grab necessary hook if one is defined
			if ( nType !== 1 || !jQuery.isXMLDoc( elem ) ) {
				hooks = jQuery.attrHooks[ name.toLowerCase() ] ||
					( jQuery.expr.match.bool.test( name ) ? boolHook : undefined );
			}
	
			if ( value !== undefined ) {
				if ( value === null ) {
					jQuery.removeAttr( elem, name );
					return;
				}
	
				if ( hooks && "set" in hooks &&
					( ret = hooks.set( elem, value, name ) ) !== undefined ) {
					return ret;
				}
	
				elem.setAttribute( name, value + "" );
				return value;
			}
	
			if ( hooks && "get" in hooks && ( ret = hooks.get( elem, name ) ) !== null ) {
				return ret;
			}
	
			ret = jQuery.find.attr( elem, name );
	
			// Non-existent attributes return null, we normalize to undefined
			return ret == null ? undefined : ret;
		},
	
		attrHooks: {
			type: {
				set: function( elem, value ) {
					if ( !support.radioValue && value === "radio" &&
						jQuery.nodeName( elem, "input" ) ) {
						var val = elem.value;
						elem.setAttribute( "type", value );
						if ( val ) {
							elem.value = val;
						}
						return value;
					}
				}
			}
		},
	
		removeAttr: function( elem, value ) {
			var name,
				i = 0,
	
				// Attribute names can contain non-HTML whitespace characters
				// https://html.spec.whatwg.org/multipage/syntax.html#attributes-2
				attrNames = value && value.match( rnothtmlwhite );
	
			if ( attrNames && elem.nodeType === 1 ) {
				while ( ( name = attrNames[ i++ ] ) ) {
					elem.removeAttribute( name );
				}
			}
		}
	} );
	
	// Hooks for boolean attributes
	boolHook = {
		set: function( elem, value, name ) {
			if ( value === false ) {
	
				// Remove boolean attributes when set to false
				jQuery.removeAttr( elem, name );
			} else {
				elem.setAttribute( name, name );
			}
			return name;
		}
	};
	
	jQuery.each( jQuery.expr.match.bool.source.match( /\w+/g ), function( i, name ) {
		var getter = attrHandle[ name ] || jQuery.find.attr;
	
		attrHandle[ name ] = function( elem, name, isXML ) {
			var ret, handle,
				lowercaseName = name.toLowerCase();
	
			if ( !isXML ) {
	
				// Avoid an infinite loop by temporarily removing this function from the getter
				handle = attrHandle[ lowercaseName ];
				attrHandle[ lowercaseName ] = ret;
				ret = getter( elem, name, isXML ) != null ?
					lowercaseName :
					null;
				attrHandle[ lowercaseName ] = handle;
			}
			return ret;
		};
	} );
	
	
	
	
	var rfocusable = /^(?:input|select|textarea|button)$/i,
		rclickable = /^(?:a|area)$/i;
	
	jQuery.fn.extend( {
		prop: function( name, value ) {
			return access( this, jQuery.prop, name, value, arguments.length > 1 );
		},
	
		removeProp: function( name ) {
			return this.each( function() {
				delete this[ jQuery.propFix[ name ] || name ];
			} );
		}
	} );
	
	jQuery.extend( {
		prop: function( elem, name, value ) {
			var ret, hooks,
				nType = elem.nodeType;
	
			// Don't get/set properties on text, comment and attribute nodes
			if ( nType === 3 || nType === 8 || nType === 2 ) {
				return;
			}
	
			if ( nType !== 1 || !jQuery.isXMLDoc( elem ) ) {
	
				// Fix name and attach hooks
				name = jQuery.propFix[ name ] || name;
				hooks = jQuery.propHooks[ name ];
			}
	
			if ( value !== undefined ) {
				if ( hooks && "set" in hooks &&
					( ret = hooks.set( elem, value, name ) ) !== undefined ) {
					return ret;
				}
	
				return ( elem[ name ] = value );
			}
	
			if ( hooks && "get" in hooks && ( ret = hooks.get( elem, name ) ) !== null ) {
				return ret;
			}
	
			return elem[ name ];
		},
	
		propHooks: {
			tabIndex: {
				get: function( elem ) {
	
					// Support: IE <=9 - 11 only
					// elem.tabIndex doesn't always return the
					// correct value when it hasn't been explicitly set
					// https://web.archive.org/web/20141116233347/http://fluidproject.org/blog/2008/01/09/getting-setting-and-removing-tabindex-values-with-javascript/
					// Use proper attribute retrieval(#12072)
					var tabindex = jQuery.find.attr( elem, "tabindex" );
	
					if ( tabindex ) {
						return parseInt( tabindex, 10 );
					}
	
					if (
						rfocusable.test( elem.nodeName ) ||
						rclickable.test( elem.nodeName ) &&
						elem.href
					) {
						return 0;
					}
	
					return -1;
				}
			}
		},
	
		propFix: {
			"for": "htmlFor",
			"class": "className"
		}
	} );
	
	// Support: IE <=11 only
	// Accessing the selectedIndex property
	// forces the browser to respect setting selected
	// on the option
	// The getter ensures a default option is selected
	// when in an optgroup
	// eslint rule "no-unused-expressions" is disabled for this code
	// since it considers such accessions noop
	if ( !support.optSelected ) {
		jQuery.propHooks.selected = {
			get: function( elem ) {
	
				/* eslint no-unused-expressions: "off" */
	
				var parent = elem.parentNode;
				if ( parent && parent.parentNode ) {
					parent.parentNode.selectedIndex;
				}
				return null;
			},
			set: function( elem ) {
	
				/* eslint no-unused-expressions: "off" */
	
				var parent = elem.parentNode;
				if ( parent ) {
					parent.selectedIndex;
	
					if ( parent.parentNode ) {
						parent.parentNode.selectedIndex;
					}
				}
			}
		};
	}
	
	jQuery.each( [
		"tabIndex",
		"readOnly",
		"maxLength",
		"cellSpacing",
		"cellPadding",
		"rowSpan",
		"colSpan",
		"useMap",
		"frameBorder",
		"contentEditable"
	], function() {
		jQuery.propFix[ this.toLowerCase() ] = this;
	} );
	
	
	
	
		// Strip and collapse whitespace according to HTML spec
		// https://html.spec.whatwg.org/multipage/infrastructure.html#strip-and-collapse-whitespace
		function stripAndCollapse( value ) {
			var tokens = value.match( rnothtmlwhite ) || [];
			return tokens.join( " " );
		}
	
	
	function getClass( elem ) {
		return elem.getAttribute && elem.getAttribute( "class" ) || "";
	}
	
	jQuery.fn.extend( {
		addClass: function( value ) {
			var classes, elem, cur, curValue, clazz, j, finalValue,
				i = 0;
	
			if ( jQuery.isFunction( value ) ) {
				return this.each( function( j ) {
					jQuery( this ).addClass( value.call( this, j, getClass( this ) ) );
				} );
			}
	
			if ( typeof value === "string" && value ) {
				classes = value.match( rnothtmlwhite ) || [];
	
				while ( ( elem = this[ i++ ] ) ) {
					curValue = getClass( elem );
					cur = elem.nodeType === 1 && ( " " + stripAndCollapse( curValue ) + " " );
	
					if ( cur ) {
						j = 0;
						while ( ( clazz = classes[ j++ ] ) ) {
							if ( cur.indexOf( " " + clazz + " " ) < 0 ) {
								cur += clazz + " ";
							}
						}
	
						// Only assign if different to avoid unneeded rendering.
						finalValue = stripAndCollapse( cur );
						if ( curValue !== finalValue ) {
							elem.setAttribute( "class", finalValue );
						}
					}
				}
			}
	
			return this;
		},
	
		removeClass: function( value ) {
			var classes, elem, cur, curValue, clazz, j, finalValue,
				i = 0;
	
			if ( jQuery.isFunction( value ) ) {
				return this.each( function( j ) {
					jQuery( this ).removeClass( value.call( this, j, getClass( this ) ) );
				} );
			}
	
			if ( !arguments.length ) {
				return this.attr( "class", "" );
			}
	
			if ( typeof value === "string" && value ) {
				classes = value.match( rnothtmlwhite ) || [];
	
				while ( ( elem = this[ i++ ] ) ) {
					curValue = getClass( elem );
	
					// This expression is here for better compressibility (see addClass)
					cur = elem.nodeType === 1 && ( " " + stripAndCollapse( curValue ) + " " );
	
					if ( cur ) {
						j = 0;
						while ( ( clazz = classes[ j++ ] ) ) {
	
							// Remove *all* instances
							while ( cur.indexOf( " " + clazz + " " ) > -1 ) {
								cur = cur.replace( " " + clazz + " ", " " );
							}
						}
	
						// Only assign if different to avoid unneeded rendering.
						finalValue = stripAndCollapse( cur );
						if ( curValue !== finalValue ) {
							elem.setAttribute( "class", finalValue );
						}
					}
				}
			}
	
			return this;
		},
	
		toggleClass: function( value, stateVal ) {
			var type = typeof value;
	
			if ( typeof stateVal === "boolean" && type === "string" ) {
				return stateVal ? this.addClass( value ) : this.removeClass( value );
			}
	
			if ( jQuery.isFunction( value ) ) {
				return this.each( function( i ) {
					jQuery( this ).toggleClass(
						value.call( this, i, getClass( this ), stateVal ),
						stateVal
					);
				} );
			}
	
			return this.each( function() {
				var className, i, self, classNames;
	
				if ( type === "string" ) {
	
					// Toggle individual class names
					i = 0;
					self = jQuery( this );
					classNames = value.match( rnothtmlwhite ) || [];
	
					while ( ( className = classNames[ i++ ] ) ) {
	
						// Check each className given, space separated list
						if ( self.hasClass( className ) ) {
							self.removeClass( className );
						} else {
							self.addClass( className );
						}
					}
	
				// Toggle whole class name
				} else if ( value === undefined || type === "boolean" ) {
					className = getClass( this );
					if ( className ) {
	
						// Store className if set
						dataPriv.set( this, "__className__", className );
					}
	
					// If the element has a class name or if we're passed `false`,
					// then remove the whole classname (if there was one, the above saved it).
					// Otherwise bring back whatever was previously saved (if anything),
					// falling back to the empty string if nothing was stored.
					if ( this.setAttribute ) {
						this.setAttribute( "class",
							className || value === false ?
							"" :
							dataPriv.get( this, "__className__" ) || ""
						);
					}
				}
			} );
		},
	
		hasClass: function( selector ) {
			var className, elem,
				i = 0;
	
			className = " " + selector + " ";
			while ( ( elem = this[ i++ ] ) ) {
				if ( elem.nodeType === 1 &&
					( " " + stripAndCollapse( getClass( elem ) ) + " " ).indexOf( className ) > -1 ) {
						return true;
				}
			}
	
			return false;
		}
	} );
	
	
	
	
	var rreturn = /\r/g;
	
	jQuery.fn.extend( {
		val: function( value ) {
			var hooks, ret, isFunction,
				elem = this[ 0 ];
	
			if ( !arguments.length ) {
				if ( elem ) {
					hooks = jQuery.valHooks[ elem.type ] ||
						jQuery.valHooks[ elem.nodeName.toLowerCase() ];
	
					if ( hooks &&
						"get" in hooks &&
						( ret = hooks.get( elem, "value" ) ) !== undefined
					) {
						return ret;
					}
	
					ret = elem.value;
	
					// Handle most common string cases
					if ( typeof ret === "string" ) {
						return ret.replace( rreturn, "" );
					}
	
					// Handle cases where value is null/undef or number
					return ret == null ? "" : ret;
				}
	
				return;
			}
	
			isFunction = jQuery.isFunction( value );
	
			return this.each( function( i ) {
				var val;
	
				if ( this.nodeType !== 1 ) {
					return;
				}
	
				if ( isFunction ) {
					val = value.call( this, i, jQuery( this ).val() );
				} else {
					val = value;
				}
	
				// Treat null/undefined as ""; convert numbers to string
				if ( val == null ) {
					val = "";
	
				} else if ( typeof val === "number" ) {
					val += "";
	
				} else if ( jQuery.isArray( val ) ) {
					val = jQuery.map( val, function( value ) {
						return value == null ? "" : value + "";
					} );
				}
	
				hooks = jQuery.valHooks[ this.type ] || jQuery.valHooks[ this.nodeName.toLowerCase() ];
	
				// If set returns undefined, fall back to normal setting
				if ( !hooks || !( "set" in hooks ) || hooks.set( this, val, "value" ) === undefined ) {
					this.value = val;
				}
			} );
		}
	} );
	
	jQuery.extend( {
		valHooks: {
			option: {
				get: function( elem ) {
	
					var val = jQuery.find.attr( elem, "value" );
					return val != null ?
						val :
	
						// Support: IE <=10 - 11 only
						// option.text throws exceptions (#14686, #14858)
						// Strip and collapse whitespace
						// https://html.spec.whatwg.org/#strip-and-collapse-whitespace
						stripAndCollapse( jQuery.text( elem ) );
				}
			},
			select: {
				get: function( elem ) {
					var value, option, i,
						options = elem.options,
						index = elem.selectedIndex,
						one = elem.type === "select-one",
						values = one ? null : [],
						max = one ? index + 1 : options.length;
	
					if ( index < 0 ) {
						i = max;
	
					} else {
						i = one ? index : 0;
					}
	
					// Loop through all the selected options
					for ( ; i < max; i++ ) {
						option = options[ i ];
	
						// Support: IE <=9 only
						// IE8-9 doesn't update selected after form reset (#2551)
						if ( ( option.selected || i === index ) &&
	
								// Don't return options that are disabled or in a disabled optgroup
								!option.disabled &&
								( !option.parentNode.disabled ||
									!jQuery.nodeName( option.parentNode, "optgroup" ) ) ) {
	
							// Get the specific value for the option
							value = jQuery( option ).val();
	
							// We don't need an array for one selects
							if ( one ) {
								return value;
							}
	
							// Multi-Selects return an array
							values.push( value );
						}
					}
	
					return values;
				},
	
				set: function( elem, value ) {
					var optionSet, option,
						options = elem.options,
						values = jQuery.makeArray( value ),
						i = options.length;
	
					while ( i-- ) {
						option = options[ i ];
	
						/* eslint-disable no-cond-assign */
	
						if ( option.selected =
							jQuery.inArray( jQuery.valHooks.option.get( option ), values ) > -1
						) {
							optionSet = true;
						}
	
						/* eslint-enable no-cond-assign */
					}
	
					// Force browsers to behave consistently when non-matching value is set
					if ( !optionSet ) {
						elem.selectedIndex = -1;
					}
					return values;
				}
			}
		}
	} );
	
	// Radios and checkboxes getter/setter
	jQuery.each( [ "radio", "checkbox" ], function() {
		jQuery.valHooks[ this ] = {
			set: function( elem, value ) {
				if ( jQuery.isArray( value ) ) {
					return ( elem.checked = jQuery.inArray( jQuery( elem ).val(), value ) > -1 );
				}
			}
		};
		if ( !support.checkOn ) {
			jQuery.valHooks[ this ].get = function( elem ) {
				return elem.getAttribute( "value" ) === null ? "on" : elem.value;
			};
		}
	} );
	
	
	
	
	// Return jQuery for attributes-only inclusion
	
	
	var rfocusMorph = /^(?:focusinfocus|focusoutblur)$/;
	
	jQuery.extend( jQuery.event, {
	
		trigger: function( event, data, elem, onlyHandlers ) {
	
			var i, cur, tmp, bubbleType, ontype, handle, special,
				eventPath = [ elem || document ],
				type = hasOwn.call( event, "type" ) ? event.type : event,
				namespaces = hasOwn.call( event, "namespace" ) ? event.namespace.split( "." ) : [];
	
			cur = tmp = elem = elem || document;
	
			// Don't do events on text and comment nodes
			if ( elem.nodeType === 3 || elem.nodeType === 8 ) {
				return;
			}
	
			// focus/blur morphs to focusin/out; ensure we're not firing them right now
			if ( rfocusMorph.test( type + jQuery.event.triggered ) ) {
				return;
			}
	
			if ( type.indexOf( "." ) > -1 ) {
	
				// Namespaced trigger; create a regexp to match event type in handle()
				namespaces = type.split( "." );
				type = namespaces.shift();
				namespaces.sort();
			}
			ontype = type.indexOf( ":" ) < 0 && "on" + type;
	
			// Caller can pass in a jQuery.Event object, Object, or just an event type string
			event = event[ jQuery.expando ] ?
				event :
				new jQuery.Event( type, typeof event === "object" && event );
	
			// Trigger bitmask: & 1 for native handlers; & 2 for jQuery (always true)
			event.isTrigger = onlyHandlers ? 2 : 3;
			event.namespace = namespaces.join( "." );
			event.rnamespace = event.namespace ?
				new RegExp( "(^|\\.)" + namespaces.join( "\\.(?:.*\\.|)" ) + "(\\.|$)" ) :
				null;
	
			// Clean up the event in case it is being reused
			event.result = undefined;
			if ( !event.target ) {
				event.target = elem;
			}
	
			// Clone any incoming data and prepend the event, creating the handler arg list
			data = data == null ?
				[ event ] :
				jQuery.makeArray( data, [ event ] );
	
			// Allow special events to draw outside the lines
			special = jQuery.event.special[ type ] || {};
			if ( !onlyHandlers && special.trigger && special.trigger.apply( elem, data ) === false ) {
				return;
			}
	
			// Determine event propagation path in advance, per W3C events spec (#9951)
			// Bubble up to document, then to window; watch for a global ownerDocument var (#9724)
			if ( !onlyHandlers && !special.noBubble && !jQuery.isWindow( elem ) ) {
	
				bubbleType = special.delegateType || type;
				if ( !rfocusMorph.test( bubbleType + type ) ) {
					cur = cur.parentNode;
				}
				for ( ; cur; cur = cur.parentNode ) {
					eventPath.push( cur );
					tmp = cur;
				}
	
				// Only add window if we got to document (e.g., not plain obj or detached DOM)
				if ( tmp === ( elem.ownerDocument || document ) ) {
					eventPath.push( tmp.defaultView || tmp.parentWindow || window );
				}
			}
	
			// Fire handlers on the event path
			i = 0;
			while ( ( cur = eventPath[ i++ ] ) && !event.isPropagationStopped() ) {
	
				event.type = i > 1 ?
					bubbleType :
					special.bindType || type;
	
				// jQuery handler
				handle = ( dataPriv.get( cur, "events" ) || {} )[ event.type ] &&
					dataPriv.get( cur, "handle" );
				if ( handle ) {
					handle.apply( cur, data );
				}
	
				// Native handler
				handle = ontype && cur[ ontype ];
				if ( handle && handle.apply && acceptData( cur ) ) {
					event.result = handle.apply( cur, data );
					if ( event.result === false ) {
						event.preventDefault();
					}
				}
			}
			event.type = type;
	
			// If nobody prevented the default action, do it now
			if ( !onlyHandlers && !event.isDefaultPrevented() ) {
	
				if ( ( !special._default ||
					special._default.apply( eventPath.pop(), data ) === false ) &&
					acceptData( elem ) ) {
	
					// Call a native DOM method on the target with the same name as the event.
					// Don't do default actions on window, that's where global variables be (#6170)
					if ( ontype && jQuery.isFunction( elem[ type ] ) && !jQuery.isWindow( elem ) ) {
	
						// Don't re-trigger an onFOO event when we call its FOO() method
						tmp = elem[ ontype ];
	
						if ( tmp ) {
							elem[ ontype ] = null;
						}
	
						// Prevent re-triggering of the same event, since we already bubbled it above
						jQuery.event.triggered = type;
						elem[ type ]();
						jQuery.event.triggered = undefined;
	
						if ( tmp ) {
							elem[ ontype ] = tmp;
						}
					}
				}
			}
	
			return event.result;
		},
	
		// Piggyback on a donor event to simulate a different one
		// Used only for `focus(in | out)` events
		simulate: function( type, elem, event ) {
			var e = jQuery.extend(
				new jQuery.Event(),
				event,
				{
					type: type,
					isSimulated: true
				}
			);
	
			jQuery.event.trigger( e, null, elem );
		}
	
	} );
	
	jQuery.fn.extend( {
	
		trigger: function( type, data ) {
			return this.each( function() {
				jQuery.event.trigger( type, data, this );
			} );
		},
		triggerHandler: function( type, data ) {
			var elem = this[ 0 ];
			if ( elem ) {
				return jQuery.event.trigger( type, data, elem, true );
			}
		}
	} );
	
	
	jQuery.each( ( "blur focus focusin focusout resize scroll click dblclick " +
		"mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave " +
		"change select submit keydown keypress keyup contextmenu" ).split( " " ),
		function( i, name ) {
	
		// Handle event binding
		jQuery.fn[ name ] = function( data, fn ) {
			return arguments.length > 0 ?
				this.on( name, null, data, fn ) :
				this.trigger( name );
		};
	} );
	
	jQuery.fn.extend( {
		hover: function( fnOver, fnOut ) {
			return this.mouseenter( fnOver ).mouseleave( fnOut || fnOver );
		}
	} );
	
	
	
	
	support.focusin = "onfocusin" in window;
	
	
	// Support: Firefox <=44
	// Firefox doesn't have focus(in | out) events
	// Related ticket - https://bugzilla.mozilla.org/show_bug.cgi?id=687787
	//
	// Support: Chrome <=48 - 49, Safari <=9.0 - 9.1
	// focus(in | out) events fire after focus & blur events,
	// which is spec violation - http://www.w3.org/TR/DOM-Level-3-Events/#events-focusevent-event-order
	// Related ticket - https://bugs.chromium.org/p/chromium/issues/detail?id=449857
	if ( !support.focusin ) {
		jQuery.each( { focus: "focusin", blur: "focusout" }, function( orig, fix ) {
	
			// Attach a single capturing handler on the document while someone wants focusin/focusout
			var handler = function( event ) {
				jQuery.event.simulate( fix, event.target, jQuery.event.fix( event ) );
			};
	
			jQuery.event.special[ fix ] = {
				setup: function() {
					var doc = this.ownerDocument || this,
						attaches = dataPriv.access( doc, fix );
	
					if ( !attaches ) {
						doc.addEventListener( orig, handler, true );
					}
					dataPriv.access( doc, fix, ( attaches || 0 ) + 1 );
				},
				teardown: function() {
					var doc = this.ownerDocument || this,
						attaches = dataPriv.access( doc, fix ) - 1;
	
					if ( !attaches ) {
						doc.removeEventListener( orig, handler, true );
						dataPriv.remove( doc, fix );
	
					} else {
						dataPriv.access( doc, fix, attaches );
					}
				}
			};
		} );
	}
	var location = window.location;
	
	var nonce = jQuery.now();
	
	var rquery = ( /\?/ );
	
	
	
	// Cross-browser xml parsing
	jQuery.parseXML = function( data ) {
		var xml;
		if ( !data || typeof data !== "string" ) {
			return null;
		}
	
		// Support: IE 9 - 11 only
		// IE throws on parseFromString with invalid input.
		try {
			xml = ( new window.DOMParser() ).parseFromString( data, "text/xml" );
		} catch ( e ) {
			xml = undefined;
		}
	
		if ( !xml || xml.getElementsByTagName( "parsererror" ).length ) {
			jQuery.error( "Invalid XML: " + data );
		}
		return xml;
	};
	
	
	var
		rbracket = /\[\]$/,
		rCRLF = /\r?\n/g,
		rsubmitterTypes = /^(?:submit|button|image|reset|file)$/i,
		rsubmittable = /^(?:input|select|textarea|keygen)/i;
	
	function buildParams( prefix, obj, traditional, add ) {
		var name;
	
		if ( jQuery.isArray( obj ) ) {
	
			// Serialize array item.
			jQuery.each( obj, function( i, v ) {
				if ( traditional || rbracket.test( prefix ) ) {
	
					// Treat each array item as a scalar.
					add( prefix, v );
	
				} else {
	
					// Item is non-scalar (array or object), encode its numeric index.
					buildParams(
						prefix + "[" + ( typeof v === "object" && v != null ? i : "" ) + "]",
						v,
						traditional,
						add
					);
				}
			} );
	
		} else if ( !traditional && jQuery.type( obj ) === "object" ) {
	
			// Serialize object item.
			for ( name in obj ) {
				buildParams( prefix + "[" + name + "]", obj[ name ], traditional, add );
			}
	
		} else {
	
			// Serialize scalar item.
			add( prefix, obj );
		}
	}
	
	// Serialize an array of form elements or a set of
	// key/values into a query string
	jQuery.param = function( a, traditional ) {
		var prefix,
			s = [],
			add = function( key, valueOrFunction ) {
	
				// If value is a function, invoke it and use its return value
				var value = jQuery.isFunction( valueOrFunction ) ?
					valueOrFunction() :
					valueOrFunction;
	
				s[ s.length ] = encodeURIComponent( key ) + "=" +
					encodeURIComponent( value == null ? "" : value );
			};
	
		// If an array was passed in, assume that it is an array of form elements.
		if ( jQuery.isArray( a ) || ( a.jquery && !jQuery.isPlainObject( a ) ) ) {
	
			// Serialize the form elements
			jQuery.each( a, function() {
				add( this.name, this.value );
			} );
	
		} else {
	
			// If traditional, encode the "old" way (the way 1.3.2 or older
			// did it), otherwise encode params recursively.
			for ( prefix in a ) {
				buildParams( prefix, a[ prefix ], traditional, add );
			}
		}
	
		// Return the resulting serialization
		return s.join( "&" );
	};
	
	jQuery.fn.extend( {
		serialize: function() {
			return jQuery.param( this.serializeArray() );
		},
		serializeArray: function() {
			return this.map( function() {
	
				// Can add propHook for "elements" to filter or add form elements
				var elements = jQuery.prop( this, "elements" );
				return elements ? jQuery.makeArray( elements ) : this;
			} )
			.filter( function() {
				var type = this.type;
	
				// Use .is( ":disabled" ) so that fieldset[disabled] works
				return this.name && !jQuery( this ).is( ":disabled" ) &&
					rsubmittable.test( this.nodeName ) && !rsubmitterTypes.test( type ) &&
					( this.checked || !rcheckableType.test( type ) );
			} )
			.map( function( i, elem ) {
				var val = jQuery( this ).val();
	
				if ( val == null ) {
					return null;
				}
	
				if ( jQuery.isArray( val ) ) {
					return jQuery.map( val, function( val ) {
						return { name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
					} );
				}
	
				return { name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
			} ).get();
		}
	} );
	
	
	var
		r20 = /%20/g,
		rhash = /#.*$/,
		rantiCache = /([?&])_=[^&]*/,
		rheaders = /^(.*?):[ \t]*([^\r\n]*)$/mg,
	
		// #7653, #8125, #8152: local protocol detection
		rlocalProtocol = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
		rnoContent = /^(?:GET|HEAD)$/,
		rprotocol = /^\/\//,
	
		/* Prefilters
		 * 1) They are useful to introduce custom dataTypes (see ajax/jsonp.js for an example)
		 * 2) These are called:
		 *    - BEFORE asking for a transport
		 *    - AFTER param serialization (s.data is a string if s.processData is true)
		 * 3) key is the dataType
		 * 4) the catchall symbol "*" can be used
		 * 5) execution will start with transport dataType and THEN continue down to "*" if needed
		 */
		prefilters = {},
	
		/* Transports bindings
		 * 1) key is the dataType
		 * 2) the catchall symbol "*" can be used
		 * 3) selection will start with transport dataType and THEN go to "*" if needed
		 */
		transports = {},
	
		// Avoid comment-prolog char sequence (#10098); must appease lint and evade compression
		allTypes = "*/".concat( "*" ),
	
		// Anchor tag for parsing the document origin
		originAnchor = document.createElement( "a" );
		originAnchor.href = location.href;
	
	// Base "constructor" for jQuery.ajaxPrefilter and jQuery.ajaxTransport
	function addToPrefiltersOrTransports( structure ) {
	
		// dataTypeExpression is optional and defaults to "*"
		return function( dataTypeExpression, func ) {
	
			if ( typeof dataTypeExpression !== "string" ) {
				func = dataTypeExpression;
				dataTypeExpression = "*";
			}
	
			var dataType,
				i = 0,
				dataTypes = dataTypeExpression.toLowerCase().match( rnothtmlwhite ) || [];
	
			if ( jQuery.isFunction( func ) ) {
	
				// For each dataType in the dataTypeExpression
				while ( ( dataType = dataTypes[ i++ ] ) ) {
	
					// Prepend if requested
					if ( dataType[ 0 ] === "+" ) {
						dataType = dataType.slice( 1 ) || "*";
						( structure[ dataType ] = structure[ dataType ] || [] ).unshift( func );
	
					// Otherwise append
					} else {
						( structure[ dataType ] = structure[ dataType ] || [] ).push( func );
					}
				}
			}
		};
	}
	
	// Base inspection function for prefilters and transports
	function inspectPrefiltersOrTransports( structure, options, originalOptions, jqXHR ) {
	
		var inspected = {},
			seekingTransport = ( structure === transports );
	
		function inspect( dataType ) {
			var selected;
			inspected[ dataType ] = true;
			jQuery.each( structure[ dataType ] || [], function( _, prefilterOrFactory ) {
				var dataTypeOrTransport = prefilterOrFactory( options, originalOptions, jqXHR );
				if ( typeof dataTypeOrTransport === "string" &&
					!seekingTransport && !inspected[ dataTypeOrTransport ] ) {
	
					options.dataTypes.unshift( dataTypeOrTransport );
					inspect( dataTypeOrTransport );
					return false;
				} else if ( seekingTransport ) {
					return !( selected = dataTypeOrTransport );
				}
			} );
			return selected;
		}
	
		return inspect( options.dataTypes[ 0 ] ) || !inspected[ "*" ] && inspect( "*" );
	}
	
	// A special extend for ajax options
	// that takes "flat" options (not to be deep extended)
	// Fixes #9887
	function ajaxExtend( target, src ) {
		var key, deep,
			flatOptions = jQuery.ajaxSettings.flatOptions || {};
	
		for ( key in src ) {
			if ( src[ key ] !== undefined ) {
				( flatOptions[ key ] ? target : ( deep || ( deep = {} ) ) )[ key ] = src[ key ];
			}
		}
		if ( deep ) {
			jQuery.extend( true, target, deep );
		}
	
		return target;
	}
	
	/* Handles responses to an ajax request:
	 * - finds the right dataType (mediates between content-type and expected dataType)
	 * - returns the corresponding response
	 */
	function ajaxHandleResponses( s, jqXHR, responses ) {
	
		var ct, type, finalDataType, firstDataType,
			contents = s.contents,
			dataTypes = s.dataTypes;
	
		// Remove auto dataType and get content-type in the process
		while ( dataTypes[ 0 ] === "*" ) {
			dataTypes.shift();
			if ( ct === undefined ) {
				ct = s.mimeType || jqXHR.getResponseHeader( "Content-Type" );
			}
		}
	
		// Check if we're dealing with a known content-type
		if ( ct ) {
			for ( type in contents ) {
				if ( contents[ type ] && contents[ type ].test( ct ) ) {
					dataTypes.unshift( type );
					break;
				}
			}
		}
	
		// Check to see if we have a response for the expected dataType
		if ( dataTypes[ 0 ] in responses ) {
			finalDataType = dataTypes[ 0 ];
		} else {
	
			// Try convertible dataTypes
			for ( type in responses ) {
				if ( !dataTypes[ 0 ] || s.converters[ type + " " + dataTypes[ 0 ] ] ) {
					finalDataType = type;
					break;
				}
				if ( !firstDataType ) {
					firstDataType = type;
				}
			}
	
			// Or just use first one
			finalDataType = finalDataType || firstDataType;
		}
	
		// If we found a dataType
		// We add the dataType to the list if needed
		// and return the corresponding response
		if ( finalDataType ) {
			if ( finalDataType !== dataTypes[ 0 ] ) {
				dataTypes.unshift( finalDataType );
			}
			return responses[ finalDataType ];
		}
	}
	
	/* Chain conversions given the request and the original response
	 * Also sets the responseXXX fields on the jqXHR instance
	 */
	function ajaxConvert( s, response, jqXHR, isSuccess ) {
		var conv2, current, conv, tmp, prev,
			converters = {},
	
			// Work with a copy of dataTypes in case we need to modify it for conversion
			dataTypes = s.dataTypes.slice();
	
		// Create converters map with lowercased keys
		if ( dataTypes[ 1 ] ) {
			for ( conv in s.converters ) {
				converters[ conv.toLowerCase() ] = s.converters[ conv ];
			}
		}
	
		current = dataTypes.shift();
	
		// Convert to each sequential dataType
		while ( current ) {
	
			if ( s.responseFields[ current ] ) {
				jqXHR[ s.responseFields[ current ] ] = response;
			}
	
			// Apply the dataFilter if provided
			if ( !prev && isSuccess && s.dataFilter ) {
				response = s.dataFilter( response, s.dataType );
			}
	
			prev = current;
			current = dataTypes.shift();
	
			if ( current ) {
	
				// There's only work to do if current dataType is non-auto
				if ( current === "*" ) {
	
					current = prev;
	
				// Convert response if prev dataType is non-auto and differs from current
				} else if ( prev !== "*" && prev !== current ) {
	
					// Seek a direct converter
					conv = converters[ prev + " " + current ] || converters[ "* " + current ];
	
					// If none found, seek a pair
					if ( !conv ) {
						for ( conv2 in converters ) {
	
							// If conv2 outputs current
							tmp = conv2.split( " " );
							if ( tmp[ 1 ] === current ) {
	
								// If prev can be converted to accepted input
								conv = converters[ prev + " " + tmp[ 0 ] ] ||
									converters[ "* " + tmp[ 0 ] ];
								if ( conv ) {
	
									// Condense equivalence converters
									if ( conv === true ) {
										conv = converters[ conv2 ];
	
									// Otherwise, insert the intermediate dataType
									} else if ( converters[ conv2 ] !== true ) {
										current = tmp[ 0 ];
										dataTypes.unshift( tmp[ 1 ] );
									}
									break;
								}
							}
						}
					}
	
					// Apply converter (if not an equivalence)
					if ( conv !== true ) {
	
						// Unless errors are allowed to bubble, catch and return them
						if ( conv && s.throws ) {
							response = conv( response );
						} else {
							try {
								response = conv( response );
							} catch ( e ) {
								return {
									state: "parsererror",
									error: conv ? e : "No conversion from " + prev + " to " + current
								};
							}
						}
					}
				}
			}
		}
	
		return { state: "success", data: response };
	}
	
	jQuery.extend( {
	
		// Counter for holding the number of active queries
		active: 0,
	
		// Last-Modified header cache for next request
		lastModified: {},
		etag: {},
	
		ajaxSettings: {
			url: location.href,
			type: "GET",
			isLocal: rlocalProtocol.test( location.protocol ),
			global: true,
			processData: true,
			async: true,
			contentType: "application/x-www-form-urlencoded; charset=UTF-8",
	
			/*
			timeout: 0,
			data: null,
			dataType: null,
			username: null,
			password: null,
			cache: null,
			throws: false,
			traditional: false,
			headers: {},
			*/
	
			accepts: {
				"*": allTypes,
				text: "text/plain",
				html: "text/html",
				xml: "application/xml, text/xml",
				json: "application/json, text/javascript"
			},
	
			contents: {
				xml: /\bxml\b/,
				html: /\bhtml/,
				json: /\bjson\b/
			},
	
			responseFields: {
				xml: "responseXML",
				text: "responseText",
				json: "responseJSON"
			},
	
			// Data converters
			// Keys separate source (or catchall "*") and destination types with a single space
			converters: {
	
				// Convert anything to text
				"* text": String,
	
				// Text to html (true = no transformation)
				"text html": true,
	
				// Evaluate text as a json expression
				"text json": JSON.parse,
	
				// Parse text as xml
				"text xml": jQuery.parseXML
			},
	
			// For options that shouldn't be deep extended:
			// you can add your own custom options here if
			// and when you create one that shouldn't be
			// deep extended (see ajaxExtend)
			flatOptions: {
				url: true,
				context: true
			}
		},
	
		// Creates a full fledged settings object into target
		// with both ajaxSettings and settings fields.
		// If target is omitted, writes into ajaxSettings.
		ajaxSetup: function( target, settings ) {
			return settings ?
	
				// Building a settings object
				ajaxExtend( ajaxExtend( target, jQuery.ajaxSettings ), settings ) :
	
				// Extending ajaxSettings
				ajaxExtend( jQuery.ajaxSettings, target );
		},
	
		ajaxPrefilter: addToPrefiltersOrTransports( prefilters ),
		ajaxTransport: addToPrefiltersOrTransports( transports ),
	
		// Main method
		ajax: function( url, options ) {
	
			// If url is an object, simulate pre-1.5 signature
			if ( typeof url === "object" ) {
				options = url;
				url = undefined;
			}
	
			// Force options to be an object
			options = options || {};
	
			var transport,
	
				// URL without anti-cache param
				cacheURL,
	
				// Response headers
				responseHeadersString,
				responseHeaders,
	
				// timeout handle
				timeoutTimer,
	
				// Url cleanup var
				urlAnchor,
	
				// Request state (becomes false upon send and true upon completion)
				completed,
	
				// To know if global events are to be dispatched
				fireGlobals,
	
				// Loop variable
				i,
	
				// uncached part of the url
				uncached,
	
				// Create the final options object
				s = jQuery.ajaxSetup( {}, options ),
	
				// Callbacks context
				callbackContext = s.context || s,
	
				// Context for global events is callbackContext if it is a DOM node or jQuery collection
				globalEventContext = s.context &&
					( callbackContext.nodeType || callbackContext.jquery ) ?
						jQuery( callbackContext ) :
						jQuery.event,
	
				// Deferreds
				deferred = jQuery.Deferred(),
				completeDeferred = jQuery.Callbacks( "once memory" ),
	
				// Status-dependent callbacks
				statusCode = s.statusCode || {},
	
				// Headers (they are sent all at once)
				requestHeaders = {},
				requestHeadersNames = {},
	
				// Default abort message
				strAbort = "canceled",
	
				// Fake xhr
				jqXHR = {
					readyState: 0,
	
					// Builds headers hashtable if needed
					getResponseHeader: function( key ) {
						var match;
						if ( completed ) {
							if ( !responseHeaders ) {
								responseHeaders = {};
								while ( ( match = rheaders.exec( responseHeadersString ) ) ) {
									responseHeaders[ match[ 1 ].toLowerCase() ] = match[ 2 ];
								}
							}
							match = responseHeaders[ key.toLowerCase() ];
						}
						return match == null ? null : match;
					},
	
					// Raw string
					getAllResponseHeaders: function() {
						return completed ? responseHeadersString : null;
					},
	
					// Caches the header
					setRequestHeader: function( name, value ) {
						if ( completed == null ) {
							name = requestHeadersNames[ name.toLowerCase() ] =
								requestHeadersNames[ name.toLowerCase() ] || name;
							requestHeaders[ name ] = value;
						}
						return this;
					},
	
					// Overrides response content-type header
					overrideMimeType: function( type ) {
						if ( completed == null ) {
							s.mimeType = type;
						}
						return this;
					},
	
					// Status-dependent callbacks
					statusCode: function( map ) {
						var code;
						if ( map ) {
							if ( completed ) {
	
								// Execute the appropriate callbacks
								jqXHR.always( map[ jqXHR.status ] );
							} else {
	
								// Lazy-add the new callbacks in a way that preserves old ones
								for ( code in map ) {
									statusCode[ code ] = [ statusCode[ code ], map[ code ] ];
								}
							}
						}
						return this;
					},
	
					// Cancel the request
					abort: function( statusText ) {
						var finalText = statusText || strAbort;
						if ( transport ) {
							transport.abort( finalText );
						}
						done( 0, finalText );
						return this;
					}
				};
	
			// Attach deferreds
			deferred.promise( jqXHR );
	
			// Add protocol if not provided (prefilters might expect it)
			// Handle falsy url in the settings object (#10093: consistency with old signature)
			// We also use the url parameter if available
			s.url = ( ( url || s.url || location.href ) + "" )
				.replace( rprotocol, location.protocol + "//" );
	
			// Alias method option to type as per ticket #12004
			s.type = options.method || options.type || s.method || s.type;
	
			// Extract dataTypes list
			s.dataTypes = ( s.dataType || "*" ).toLowerCase().match( rnothtmlwhite ) || [ "" ];
	
			// A cross-domain request is in order when the origin doesn't match the current origin.
			if ( s.crossDomain == null ) {
				urlAnchor = document.createElement( "a" );
	
				// Support: IE <=8 - 11, Edge 12 - 13
				// IE throws exception on accessing the href property if url is malformed,
				// e.g. http://example.com:80x/
				try {
					urlAnchor.href = s.url;
	
					// Support: IE <=8 - 11 only
					// Anchor's host property isn't correctly set when s.url is relative
					urlAnchor.href = urlAnchor.href;
					s.crossDomain = originAnchor.protocol + "//" + originAnchor.host !==
						urlAnchor.protocol + "//" + urlAnchor.host;
				} catch ( e ) {
	
					// If there is an error parsing the URL, assume it is crossDomain,
					// it can be rejected by the transport if it is invalid
					s.crossDomain = true;
				}
			}
	
			// Convert data if not already a string
			if ( s.data && s.processData && typeof s.data !== "string" ) {
				s.data = jQuery.param( s.data, s.traditional );
			}
	
			// Apply prefilters
			inspectPrefiltersOrTransports( prefilters, s, options, jqXHR );
	
			// If request was aborted inside a prefilter, stop there
			if ( completed ) {
				return jqXHR;
			}
	
			// We can fire global events as of now if asked to
			// Don't fire events if jQuery.event is undefined in an AMD-usage scenario (#15118)
			fireGlobals = jQuery.event && s.global;
	
			// Watch for a new set of requests
			if ( fireGlobals && jQuery.active++ === 0 ) {
				jQuery.event.trigger( "ajaxStart" );
			}
	
			// Uppercase the type
			s.type = s.type.toUpperCase();
	
			// Determine if request has content
			s.hasContent = !rnoContent.test( s.type );
	
			// Save the URL in case we're toying with the If-Modified-Since
			// and/or If-None-Match header later on
			// Remove hash to simplify url manipulation
			cacheURL = s.url.replace( rhash, "" );
	
			// More options handling for requests with no content
			if ( !s.hasContent ) {
	
				// Remember the hash so we can put it back
				uncached = s.url.slice( cacheURL.length );
	
				// If data is available, append data to url
				if ( s.data ) {
					cacheURL += ( rquery.test( cacheURL ) ? "&" : "?" ) + s.data;
	
					// #9682: remove data so that it's not used in an eventual retry
					delete s.data;
				}
	
				// Add or update anti-cache param if needed
				if ( s.cache === false ) {
					cacheURL = cacheURL.replace( rantiCache, "$1" );
					uncached = ( rquery.test( cacheURL ) ? "&" : "?" ) + "_=" + ( nonce++ ) + uncached;
				}
	
				// Put hash and anti-cache on the URL that will be requested (gh-1732)
				s.url = cacheURL + uncached;
	
			// Change '%20' to '+' if this is encoded form body content (gh-2658)
			} else if ( s.data && s.processData &&
				( s.contentType || "" ).indexOf( "application/x-www-form-urlencoded" ) === 0 ) {
				s.data = s.data.replace( r20, "+" );
			}
	
			// Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
			if ( s.ifModified ) {
				if ( jQuery.lastModified[ cacheURL ] ) {
					jqXHR.setRequestHeader( "If-Modified-Since", jQuery.lastModified[ cacheURL ] );
				}
				if ( jQuery.etag[ cacheURL ] ) {
					jqXHR.setRequestHeader( "If-None-Match", jQuery.etag[ cacheURL ] );
				}
			}
	
			// Set the correct header, if data is being sent
			if ( s.data && s.hasContent && s.contentType !== false || options.contentType ) {
				jqXHR.setRequestHeader( "Content-Type", s.contentType );
			}
	
			// Set the Accepts header for the server, depending on the dataType
			jqXHR.setRequestHeader(
				"Accept",
				s.dataTypes[ 0 ] && s.accepts[ s.dataTypes[ 0 ] ] ?
					s.accepts[ s.dataTypes[ 0 ] ] +
						( s.dataTypes[ 0 ] !== "*" ? ", " + allTypes + "; q=0.01" : "" ) :
					s.accepts[ "*" ]
			);
	
			// Check for headers option
			for ( i in s.headers ) {
				jqXHR.setRequestHeader( i, s.headers[ i ] );
			}
	
			// Allow custom headers/mimetypes and early abort
			if ( s.beforeSend &&
				( s.beforeSend.call( callbackContext, jqXHR, s ) === false || completed ) ) {
	
				// Abort if not done already and return
				return jqXHR.abort();
			}
	
			// Aborting is no longer a cancellation
			strAbort = "abort";
	
			// Install callbacks on deferreds
			completeDeferred.add( s.complete );
			jqXHR.done( s.success );
			jqXHR.fail( s.error );
	
			// Get transport
			transport = inspectPrefiltersOrTransports( transports, s, options, jqXHR );
	
			// If no transport, we auto-abort
			if ( !transport ) {
				done( -1, "No Transport" );
			} else {
				jqXHR.readyState = 1;
	
				// Send global event
				if ( fireGlobals ) {
					globalEventContext.trigger( "ajaxSend", [ jqXHR, s ] );
				}
	
				// If request was aborted inside ajaxSend, stop there
				if ( completed ) {
					return jqXHR;
				}
	
				// Timeout
				if ( s.async && s.timeout > 0 ) {
					timeoutTimer = window.setTimeout( function() {
						jqXHR.abort( "timeout" );
					}, s.timeout );
				}
	
				try {
					completed = false;
					transport.send( requestHeaders, done );
				} catch ( e ) {
	
					// Rethrow post-completion exceptions
					if ( completed ) {
						throw e;
					}
	
					// Propagate others as results
					done( -1, e );
				}
			}
	
			// Callback for when everything is done
			function done( status, nativeStatusText, responses, headers ) {
				var isSuccess, success, error, response, modified,
					statusText = nativeStatusText;
	
				// Ignore repeat invocations
				if ( completed ) {
					return;
				}
	
				completed = true;
	
				// Clear timeout if it exists
				if ( timeoutTimer ) {
					window.clearTimeout( timeoutTimer );
				}
	
				// Dereference transport for early garbage collection
				// (no matter how long the jqXHR object will be used)
				transport = undefined;
	
				// Cache response headers
				responseHeadersString = headers || "";
	
				// Set readyState
				jqXHR.readyState = status > 0 ? 4 : 0;
	
				// Determine if successful
				isSuccess = status >= 200 && status < 300 || status === 304;
	
				// Get response data
				if ( responses ) {
					response = ajaxHandleResponses( s, jqXHR, responses );
				}
	
				// Convert no matter what (that way responseXXX fields are always set)
				response = ajaxConvert( s, response, jqXHR, isSuccess );
	
				// If successful, handle type chaining
				if ( isSuccess ) {
	
					// Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
					if ( s.ifModified ) {
						modified = jqXHR.getResponseHeader( "Last-Modified" );
						if ( modified ) {
							jQuery.lastModified[ cacheURL ] = modified;
						}
						modified = jqXHR.getResponseHeader( "etag" );
						if ( modified ) {
							jQuery.etag[ cacheURL ] = modified;
						}
					}
	
					// if no content
					if ( status === 204 || s.type === "HEAD" ) {
						statusText = "nocontent";
	
					// if not modified
					} else if ( status === 304 ) {
						statusText = "notmodified";
	
					// If we have data, let's convert it
					} else {
						statusText = response.state;
						success = response.data;
						error = response.error;
						isSuccess = !error;
					}
				} else {
	
					// Extract error from statusText and normalize for non-aborts
					error = statusText;
					if ( status || !statusText ) {
						statusText = "error";
						if ( status < 0 ) {
							status = 0;
						}
					}
				}
	
				// Set data for the fake xhr object
				jqXHR.status = status;
				jqXHR.statusText = ( nativeStatusText || statusText ) + "";
	
				// Success/Error
				if ( isSuccess ) {
					deferred.resolveWith( callbackContext, [ success, statusText, jqXHR ] );
				} else {
					deferred.rejectWith( callbackContext, [ jqXHR, statusText, error ] );
				}
	
				// Status-dependent callbacks
				jqXHR.statusCode( statusCode );
				statusCode = undefined;
	
				if ( fireGlobals ) {
					globalEventContext.trigger( isSuccess ? "ajaxSuccess" : "ajaxError",
						[ jqXHR, s, isSuccess ? success : error ] );
				}
	
				// Complete
				completeDeferred.fireWith( callbackContext, [ jqXHR, statusText ] );
	
				if ( fireGlobals ) {
					globalEventContext.trigger( "ajaxComplete", [ jqXHR, s ] );
	
					// Handle the global AJAX counter
					if ( !( --jQuery.active ) ) {
						jQuery.event.trigger( "ajaxStop" );
					}
				}
			}
	
			return jqXHR;
		},
	
		getJSON: function( url, data, callback ) {
			return jQuery.get( url, data, callback, "json" );
		},
	
		getScript: function( url, callback ) {
			return jQuery.get( url, undefined, callback, "script" );
		}
	} );
	
	jQuery.each( [ "get", "post" ], function( i, method ) {
		jQuery[ method ] = function( url, data, callback, type ) {
	
			// Shift arguments if data argument was omitted
			if ( jQuery.isFunction( data ) ) {
				type = type || callback;
				callback = data;
				data = undefined;
			}
	
			// The url can be an options object (which then must have .url)
			return jQuery.ajax( jQuery.extend( {
				url: url,
				type: method,
				dataType: type,
				data: data,
				success: callback
			}, jQuery.isPlainObject( url ) && url ) );
		};
	} );
	
	
	jQuery._evalUrl = function( url ) {
		return jQuery.ajax( {
			url: url,
	
			// Make this explicit, since user can override this through ajaxSetup (#11264)
			type: "GET",
			dataType: "script",
			cache: true,
			async: false,
			global: false,
			"throws": true
		} );
	};
	
	
	jQuery.fn.extend( {
		wrapAll: function( html ) {
			var wrap;
	
			if ( this[ 0 ] ) {
				if ( jQuery.isFunction( html ) ) {
					html = html.call( this[ 0 ] );
				}
	
				// The elements to wrap the target around
				wrap = jQuery( html, this[ 0 ].ownerDocument ).eq( 0 ).clone( true );
	
				if ( this[ 0 ].parentNode ) {
					wrap.insertBefore( this[ 0 ] );
				}
	
				wrap.map( function() {
					var elem = this;
	
					while ( elem.firstElementChild ) {
						elem = elem.firstElementChild;
					}
	
					return elem;
				} ).append( this );
			}
	
			return this;
		},
	
		wrapInner: function( html ) {
			if ( jQuery.isFunction( html ) ) {
				return this.each( function( i ) {
					jQuery( this ).wrapInner( html.call( this, i ) );
				} );
			}
	
			return this.each( function() {
				var self = jQuery( this ),
					contents = self.contents();
	
				if ( contents.length ) {
					contents.wrapAll( html );
	
				} else {
					self.append( html );
				}
			} );
		},
	
		wrap: function( html ) {
			var isFunction = jQuery.isFunction( html );
	
			return this.each( function( i ) {
				jQuery( this ).wrapAll( isFunction ? html.call( this, i ) : html );
			} );
		},
	
		unwrap: function( selector ) {
			this.parent( selector ).not( "body" ).each( function() {
				jQuery( this ).replaceWith( this.childNodes );
			} );
			return this;
		}
	} );
	
	
	jQuery.expr.pseudos.hidden = function( elem ) {
		return !jQuery.expr.pseudos.visible( elem );
	};
	jQuery.expr.pseudos.visible = function( elem ) {
		return !!( elem.offsetWidth || elem.offsetHeight || elem.getClientRects().length );
	};
	
	
	
	
	jQuery.ajaxSettings.xhr = function() {
		try {
			return new window.XMLHttpRequest();
		} catch ( e ) {}
	};
	
	var xhrSuccessStatus = {
	
			// File protocol always yields status code 0, assume 200
			0: 200,
	
			// Support: IE <=9 only
			// #1450: sometimes IE returns 1223 when it should be 204
			1223: 204
		},
		xhrSupported = jQuery.ajaxSettings.xhr();
	
	support.cors = !!xhrSupported && ( "withCredentials" in xhrSupported );
	support.ajax = xhrSupported = !!xhrSupported;
	
	jQuery.ajaxTransport( function( options ) {
		var callback, errorCallback;
	
		// Cross domain only allowed if supported through XMLHttpRequest
		if ( support.cors || xhrSupported && !options.crossDomain ) {
			return {
				send: function( headers, complete ) {
					var i,
						xhr = options.xhr();
	
					xhr.open(
						options.type,
						options.url,
						options.async,
						options.username,
						options.password
					);
	
					// Apply custom fields if provided
					if ( options.xhrFields ) {
						for ( i in options.xhrFields ) {
							xhr[ i ] = options.xhrFields[ i ];
						}
					}
	
					// Override mime type if needed
					if ( options.mimeType && xhr.overrideMimeType ) {
						xhr.overrideMimeType( options.mimeType );
					}
	
					// X-Requested-With header
					// For cross-domain requests, seeing as conditions for a preflight are
					// akin to a jigsaw puzzle, we simply never set it to be sure.
					// (it can always be set on a per-request basis or even using ajaxSetup)
					// For same-domain requests, won't change header if already provided.
					if ( !options.crossDomain && !headers[ "X-Requested-With" ] ) {
						headers[ "X-Requested-With" ] = "XMLHttpRequest";
					}
	
					// Set headers
					for ( i in headers ) {
						xhr.setRequestHeader( i, headers[ i ] );
					}
	
					// Callback
					callback = function( type ) {
						return function() {
							if ( callback ) {
								callback = errorCallback = xhr.onload =
									xhr.onerror = xhr.onabort = xhr.onreadystatechange = null;
	
								if ( type === "abort" ) {
									xhr.abort();
								} else if ( type === "error" ) {
	
									// Support: IE <=9 only
									// On a manual native abort, IE9 throws
									// errors on any property access that is not readyState
									if ( typeof xhr.status !== "number" ) {
										complete( 0, "error" );
									} else {
										complete(
	
											// File: protocol always yields status 0; see #8605, #14207
											xhr.status,
											xhr.statusText
										);
									}
								} else {
									complete(
										xhrSuccessStatus[ xhr.status ] || xhr.status,
										xhr.statusText,
	
										// Support: IE <=9 only
										// IE9 has no XHR2 but throws on binary (trac-11426)
										// For XHR2 non-text, let the caller handle it (gh-2498)
										( xhr.responseType || "text" ) !== "text"  ||
										typeof xhr.responseText !== "string" ?
											{ binary: xhr.response } :
											{ text: xhr.responseText },
										xhr.getAllResponseHeaders()
									);
								}
							}
						};
					};
	
					// Listen to events
					xhr.onload = callback();
					errorCallback = xhr.onerror = callback( "error" );
	
					// Support: IE 9 only
					// Use onreadystatechange to replace onabort
					// to handle uncaught aborts
					if ( xhr.onabort !== undefined ) {
						xhr.onabort = errorCallback;
					} else {
						xhr.onreadystatechange = function() {
	
							// Check readyState before timeout as it changes
							if ( xhr.readyState === 4 ) {
	
								// Allow onerror to be called first,
								// but that will not handle a native abort
								// Also, save errorCallback to a variable
								// as xhr.onerror cannot be accessed
								window.setTimeout( function() {
									if ( callback ) {
										errorCallback();
									}
								} );
							}
						};
					}
	
					// Create the abort callback
					callback = callback( "abort" );
	
					try {
	
						// Do send the request (this may raise an exception)
						xhr.send( options.hasContent && options.data || null );
					} catch ( e ) {
	
						// #14683: Only rethrow if this hasn't been notified as an error yet
						if ( callback ) {
							throw e;
						}
					}
				},
	
				abort: function() {
					if ( callback ) {
						callback();
					}
				}
			};
		}
	} );
	
	
	
	
	// Prevent auto-execution of scripts when no explicit dataType was provided (See gh-2432)
	jQuery.ajaxPrefilter( function( s ) {
		if ( s.crossDomain ) {
			s.contents.script = false;
		}
	} );
	
	// Install script dataType
	jQuery.ajaxSetup( {
		accepts: {
			script: "text/javascript, application/javascript, " +
				"application/ecmascript, application/x-ecmascript"
		},
		contents: {
			script: /\b(?:java|ecma)script\b/
		},
		converters: {
			"text script": function( text ) {
				jQuery.globalEval( text );
				return text;
			}
		}
	} );
	
	// Handle cache's special case and crossDomain
	jQuery.ajaxPrefilter( "script", function( s ) {
		if ( s.cache === undefined ) {
			s.cache = false;
		}
		if ( s.crossDomain ) {
			s.type = "GET";
		}
	} );
	
	// Bind script tag hack transport
	jQuery.ajaxTransport( "script", function( s ) {
	
		// This transport only deals with cross domain requests
		if ( s.crossDomain ) {
			var script, callback;
			return {
				send: function( _, complete ) {
					script = jQuery( "<script>" ).prop( {
						charset: s.scriptCharset,
						src: s.url
					} ).on(
						"load error",
						callback = function( evt ) {
							script.remove();
							callback = null;
							if ( evt ) {
								complete( evt.type === "error" ? 404 : 200, evt.type );
							}
						}
					);
	
					// Use native DOM manipulation to avoid our domManip AJAX trickery
					document.head.appendChild( script[ 0 ] );
				},
				abort: function() {
					if ( callback ) {
						callback();
					}
				}
			};
		}
	} );
	
	
	
	
	var oldCallbacks = [],
		rjsonp = /(=)\?(?=&|$)|\?\?/;
	
	// Default jsonp settings
	jQuery.ajaxSetup( {
		jsonp: "callback",
		jsonpCallback: function() {
			var callback = oldCallbacks.pop() || ( jQuery.expando + "_" + ( nonce++ ) );
			this[ callback ] = true;
			return callback;
		}
	} );
	
	// Detect, normalize options and install callbacks for jsonp requests
	jQuery.ajaxPrefilter( "json jsonp", function( s, originalSettings, jqXHR ) {
	
		var callbackName, overwritten, responseContainer,
			jsonProp = s.jsonp !== false && ( rjsonp.test( s.url ) ?
				"url" :
				typeof s.data === "string" &&
					( s.contentType || "" )
						.indexOf( "application/x-www-form-urlencoded" ) === 0 &&
					rjsonp.test( s.data ) && "data"
			);
	
		// Handle iff the expected data type is "jsonp" or we have a parameter to set
		if ( jsonProp || s.dataTypes[ 0 ] === "jsonp" ) {
	
			// Get callback name, remembering preexisting value associated with it
			callbackName = s.jsonpCallback = jQuery.isFunction( s.jsonpCallback ) ?
				s.jsonpCallback() :
				s.jsonpCallback;
	
			// Insert callback into url or form data
			if ( jsonProp ) {
				s[ jsonProp ] = s[ jsonProp ].replace( rjsonp, "$1" + callbackName );
			} else if ( s.jsonp !== false ) {
				s.url += ( rquery.test( s.url ) ? "&" : "?" ) + s.jsonp + "=" + callbackName;
			}
	
			// Use data converter to retrieve json after script execution
			s.converters[ "script json" ] = function() {
				if ( !responseContainer ) {
					jQuery.error( callbackName + " was not called" );
				}
				return responseContainer[ 0 ];
			};
	
			// Force json dataType
			s.dataTypes[ 0 ] = "json";
	
			// Install callback
			overwritten = window[ callbackName ];
			window[ callbackName ] = function() {
				responseContainer = arguments;
			};
	
			// Clean-up function (fires after converters)
			jqXHR.always( function() {
	
				// If previous value didn't exist - remove it
				if ( overwritten === undefined ) {
					jQuery( window ).removeProp( callbackName );
	
				// Otherwise restore preexisting value
				} else {
					window[ callbackName ] = overwritten;
				}
	
				// Save back as free
				if ( s[ callbackName ] ) {
	
					// Make sure that re-using the options doesn't screw things around
					s.jsonpCallback = originalSettings.jsonpCallback;
	
					// Save the callback name for future use
					oldCallbacks.push( callbackName );
				}
	
				// Call if it was a function and we have a response
				if ( responseContainer && jQuery.isFunction( overwritten ) ) {
					overwritten( responseContainer[ 0 ] );
				}
	
				responseContainer = overwritten = undefined;
			} );
	
			// Delegate to script
			return "script";
		}
	} );
	
	
	
	
	// Support: Safari 8 only
	// In Safari 8 documents created via document.implementation.createHTMLDocument
	// collapse sibling forms: the second one becomes a child of the first one.
	// Because of that, this security measure has to be disabled in Safari 8.
	// https://bugs.webkit.org/show_bug.cgi?id=137337
	support.createHTMLDocument = ( function() {
		var body = document.implementation.createHTMLDocument( "" ).body;
		body.innerHTML = "<form></form><form></form>";
		return body.childNodes.length === 2;
	} )();
	
	
	// Argument "data" should be string of html
	// context (optional): If specified, the fragment will be created in this context,
	// defaults to document
	// keepScripts (optional): If true, will include scripts passed in the html string
	jQuery.parseHTML = function( data, context, keepScripts ) {
		if ( typeof data !== "string" ) {
			return [];
		}
		if ( typeof context === "boolean" ) {
			keepScripts = context;
			context = false;
		}
	
		var base, parsed, scripts;
	
		if ( !context ) {
	
			// Stop scripts or inline event handlers from being executed immediately
			// by using document.implementation
			if ( support.createHTMLDocument ) {
				context = document.implementation.createHTMLDocument( "" );
	
				// Set the base href for the created document
				// so any parsed elements with URLs
				// are based on the document's URL (gh-2965)
				base = context.createElement( "base" );
				base.href = document.location.href;
				context.head.appendChild( base );
			} else {
				context = document;
			}
		}
	
		parsed = rsingleTag.exec( data );
		scripts = !keepScripts && [];
	
		// Single tag
		if ( parsed ) {
			return [ context.createElement( parsed[ 1 ] ) ];
		}
	
		parsed = buildFragment( [ data ], context, scripts );
	
		if ( scripts && scripts.length ) {
			jQuery( scripts ).remove();
		}
	
		return jQuery.merge( [], parsed.childNodes );
	};
	
	
	/**
	 * Load a url into a page
	 */
	jQuery.fn.load = function( url, params, callback ) {
		var selector, type, response,
			self = this,
			off = url.indexOf( " " );
	
		if ( off > -1 ) {
			selector = stripAndCollapse( url.slice( off ) );
			url = url.slice( 0, off );
		}
	
		// If it's a function
		if ( jQuery.isFunction( params ) ) {
	
			// We assume that it's the callback
			callback = params;
			params = undefined;
	
		// Otherwise, build a param string
		} else if ( params && typeof params === "object" ) {
			type = "POST";
		}
	
		// If we have elements to modify, make the request
		if ( self.length > 0 ) {
			jQuery.ajax( {
				url: url,
	
				// If "type" variable is undefined, then "GET" method will be used.
				// Make value of this field explicit since
				// user can override it through ajaxSetup method
				type: type || "GET",
				dataType: "html",
				data: params
			} ).done( function( responseText ) {
	
				// Save response for use in complete callback
				response = arguments;
	
				self.html( selector ?
	
					// If a selector was specified, locate the right elements in a dummy div
					// Exclude scripts to avoid IE 'Permission Denied' errors
					jQuery( "<div>" ).append( jQuery.parseHTML( responseText ) ).find( selector ) :
	
					// Otherwise use the full result
					responseText );
	
			// If the request succeeds, this function gets "data", "status", "jqXHR"
			// but they are ignored because response was set above.
			// If it fails, this function gets "jqXHR", "status", "error"
			} ).always( callback && function( jqXHR, status ) {
				self.each( function() {
					callback.apply( this, response || [ jqXHR.responseText, status, jqXHR ] );
				} );
			} );
		}
	
		return this;
	};
	
	
	
	
	// Attach a bunch of functions for handling common AJAX events
	jQuery.each( [
		"ajaxStart",
		"ajaxStop",
		"ajaxComplete",
		"ajaxError",
		"ajaxSuccess",
		"ajaxSend"
	], function( i, type ) {
		jQuery.fn[ type ] = function( fn ) {
			return this.on( type, fn );
		};
	} );
	
	
	
	
	jQuery.expr.pseudos.animated = function( elem ) {
		return jQuery.grep( jQuery.timers, function( fn ) {
			return elem === fn.elem;
		} ).length;
	};
	
	
	
	
	/**
	 * Gets a window from an element
	 */
	function getWindow( elem ) {
		return jQuery.isWindow( elem ) ? elem : elem.nodeType === 9 && elem.defaultView;
	}
	
	jQuery.offset = {
		setOffset: function( elem, options, i ) {
			var curPosition, curLeft, curCSSTop, curTop, curOffset, curCSSLeft, calculatePosition,
				position = jQuery.css( elem, "position" ),
				curElem = jQuery( elem ),
				props = {};
	
			// Set position first, in-case top/left are set even on static elem
			if ( position === "static" ) {
				elem.style.position = "relative";
			}
	
			curOffset = curElem.offset();
			curCSSTop = jQuery.css( elem, "top" );
			curCSSLeft = jQuery.css( elem, "left" );
			calculatePosition = ( position === "absolute" || position === "fixed" ) &&
				( curCSSTop + curCSSLeft ).indexOf( "auto" ) > -1;
	
			// Need to be able to calculate position if either
			// top or left is auto and position is either absolute or fixed
			if ( calculatePosition ) {
				curPosition = curElem.position();
				curTop = curPosition.top;
				curLeft = curPosition.left;
	
			} else {
				curTop = parseFloat( curCSSTop ) || 0;
				curLeft = parseFloat( curCSSLeft ) || 0;
			}
	
			if ( jQuery.isFunction( options ) ) {
	
				// Use jQuery.extend here to allow modification of coordinates argument (gh-1848)
				options = options.call( elem, i, jQuery.extend( {}, curOffset ) );
			}
	
			if ( options.top != null ) {
				props.top = ( options.top - curOffset.top ) + curTop;
			}
			if ( options.left != null ) {
				props.left = ( options.left - curOffset.left ) + curLeft;
			}
	
			if ( "using" in options ) {
				options.using.call( elem, props );
	
			} else {
				curElem.css( props );
			}
		}
	};
	
	jQuery.fn.extend( {
		offset: function( options ) {
	
			// Preserve chaining for setter
			if ( arguments.length ) {
				return options === undefined ?
					this :
					this.each( function( i ) {
						jQuery.offset.setOffset( this, options, i );
					} );
			}
	
			var docElem, win, rect, doc,
				elem = this[ 0 ];
	
			if ( !elem ) {
				return;
			}
	
			// Support: IE <=11 only
			// Running getBoundingClientRect on a
			// disconnected node in IE throws an error
			if ( !elem.getClientRects().length ) {
				return { top: 0, left: 0 };
			}
	
			rect = elem.getBoundingClientRect();
	
			// Make sure element is not hidden (display: none)
			if ( rect.width || rect.height ) {
				doc = elem.ownerDocument;
				win = getWindow( doc );
				docElem = doc.documentElement;
	
				return {
					top: rect.top + win.pageYOffset - docElem.clientTop,
					left: rect.left + win.pageXOffset - docElem.clientLeft
				};
			}
	
			// Return zeros for disconnected and hidden elements (gh-2310)
			return rect;
		},
	
		position: function() {
			if ( !this[ 0 ] ) {
				return;
			}
	
			var offsetParent, offset,
				elem = this[ 0 ],
				parentOffset = { top: 0, left: 0 };
	
			// Fixed elements are offset from window (parentOffset = {top:0, left: 0},
			// because it is its only offset parent
			if ( jQuery.css( elem, "position" ) === "fixed" ) {
	
				// Assume getBoundingClientRect is there when computed position is fixed
				offset = elem.getBoundingClientRect();
	
			} else {
	
				// Get *real* offsetParent
				offsetParent = this.offsetParent();
	
				// Get correct offsets
				offset = this.offset();
				if ( !jQuery.nodeName( offsetParent[ 0 ], "html" ) ) {
					parentOffset = offsetParent.offset();
				}
	
				// Add offsetParent borders
				parentOffset = {
					top: parentOffset.top + jQuery.css( offsetParent[ 0 ], "borderTopWidth", true ),
					left: parentOffset.left + jQuery.css( offsetParent[ 0 ], "borderLeftWidth", true )
				};
			}
	
			// Subtract parent offsets and element margins
			return {
				top: offset.top - parentOffset.top - jQuery.css( elem, "marginTop", true ),
				left: offset.left - parentOffset.left - jQuery.css( elem, "marginLeft", true )
			};
		},
	
		// This method will return documentElement in the following cases:
		// 1) For the element inside the iframe without offsetParent, this method will return
		//    documentElement of the parent window
		// 2) For the hidden or detached element
		// 3) For body or html element, i.e. in case of the html node - it will return itself
		//
		// but those exceptions were never presented as a real life use-cases
		// and might be considered as more preferable results.
		//
		// This logic, however, is not guaranteed and can change at any point in the future
		offsetParent: function() {
			return this.map( function() {
				var offsetParent = this.offsetParent;
	
				while ( offsetParent && jQuery.css( offsetParent, "position" ) === "static" ) {
					offsetParent = offsetParent.offsetParent;
				}
	
				return offsetParent || documentElement;
			} );
		}
	} );
	
	// Create scrollLeft and scrollTop methods
	jQuery.each( { scrollLeft: "pageXOffset", scrollTop: "pageYOffset" }, function( method, prop ) {
		var top = "pageYOffset" === prop;
	
		jQuery.fn[ method ] = function( val ) {
			return access( this, function( elem, method, val ) {
				var win = getWindow( elem );
	
				if ( val === undefined ) {
					return win ? win[ prop ] : elem[ method ];
				}
	
				if ( win ) {
					win.scrollTo(
						!top ? val : win.pageXOffset,
						top ? val : win.pageYOffset
					);
	
				} else {
					elem[ method ] = val;
				}
			}, method, val, arguments.length );
		};
	} );
	
	// Support: Safari <=7 - 9.1, Chrome <=37 - 49
	// Add the top/left cssHooks using jQuery.fn.position
	// Webkit bug: https://bugs.webkit.org/show_bug.cgi?id=29084
	// Blink bug: https://bugs.chromium.org/p/chromium/issues/detail?id=589347
	// getComputedStyle returns percent when specified for top/left/bottom/right;
	// rather than make the css module depend on the offset module, just check for it here
	jQuery.each( [ "top", "left" ], function( i, prop ) {
		jQuery.cssHooks[ prop ] = addGetHookIf( support.pixelPosition,
			function( elem, computed ) {
				if ( computed ) {
					computed = curCSS( elem, prop );
	
					// If curCSS returns percentage, fallback to offset
					return rnumnonpx.test( computed ) ?
						jQuery( elem ).position()[ prop ] + "px" :
						computed;
				}
			}
		);
	} );
	
	
	// Create innerHeight, innerWidth, height, width, outerHeight and outerWidth methods
	jQuery.each( { Height: "height", Width: "width" }, function( name, type ) {
		jQuery.each( { padding: "inner" + name, content: type, "": "outer" + name },
			function( defaultExtra, funcName ) {
	
			// Margin is only for outerHeight, outerWidth
			jQuery.fn[ funcName ] = function( margin, value ) {
				var chainable = arguments.length && ( defaultExtra || typeof margin !== "boolean" ),
					extra = defaultExtra || ( margin === true || value === true ? "margin" : "border" );
	
				return access( this, function( elem, type, value ) {
					var doc;
	
					if ( jQuery.isWindow( elem ) ) {
	
						// $( window ).outerWidth/Height return w/h including scrollbars (gh-1729)
						return funcName.indexOf( "outer" ) === 0 ?
							elem[ "inner" + name ] :
							elem.document.documentElement[ "client" + name ];
					}
	
					// Get document width or height
					if ( elem.nodeType === 9 ) {
						doc = elem.documentElement;
	
						// Either scroll[Width/Height] or offset[Width/Height] or client[Width/Height],
						// whichever is greatest
						return Math.max(
							elem.body[ "scroll" + name ], doc[ "scroll" + name ],
							elem.body[ "offset" + name ], doc[ "offset" + name ],
							doc[ "client" + name ]
						);
					}
	
					return value === undefined ?
	
						// Get width or height on the element, requesting but not forcing parseFloat
						jQuery.css( elem, type, extra ) :
	
						// Set width or height on the element
						jQuery.style( elem, type, value, extra );
				}, type, chainable ? margin : undefined, chainable );
			};
		} );
	} );
	
	
	jQuery.fn.extend( {
	
		bind: function( types, data, fn ) {
			return this.on( types, null, data, fn );
		},
		unbind: function( types, fn ) {
			return this.off( types, null, fn );
		},
	
		delegate: function( selector, types, data, fn ) {
			return this.on( types, selector, data, fn );
		},
		undelegate: function( selector, types, fn ) {
	
			// ( namespace ) or ( selector, types [, fn] )
			return arguments.length === 1 ?
				this.off( selector, "**" ) :
				this.off( types, selector || "**", fn );
		}
	} );
	
	jQuery.parseJSON = JSON.parse;
	
	
	
	
	// Register as a named AMD module, since jQuery can be concatenated with other
	// files that may use define, but not via a proper concatenation script that
	// understands anonymous AMD modules. A named AMD is safest and most robust
	// way to register. Lowercase jquery is used because AMD module names are
	// derived from file names, and jQuery is normally delivered in a lowercase
	// file name. Do this after creating the global so that if an AMD module wants
	// to call noConflict to hide this version of jQuery, it will work.
	
	// Note that for maximum portability, libraries that are not jQuery should
	// declare themselves as anonymous modules, and avoid setting a global if an
	// AMD loader is present. jQuery is a special case. For more information, see
	// https://github.com/jrburke/requirejs/wiki/Updating-existing-libraries#wiki-anon
	
	if ( true ) {
		!(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_RESULT__ = function() {
			return jQuery;
		}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	}
	
	
	
	
	var
	
		// Map over jQuery in case of overwrite
		_jQuery = window.jQuery,
	
		// Map over the $ in case of overwrite
		_$ = window.$;
	
	jQuery.noConflict = function( deep ) {
		if ( window.$ === jQuery ) {
			window.$ = _$;
		}
	
		if ( deep && window.jQuery === jQuery ) {
			window.jQuery = _jQuery;
		}
	
		return jQuery;
	};
	
	// Expose jQuery and $ identifiers, even in AMD
	// (#7102#comment:10, https://github.com/jquery/jquery/pull/557)
	// and CommonJS for browser emulators (#13566)
	if ( !noGlobal ) {
		window.jQuery = window.$ = jQuery;
	}
	
	
	
	
	
	return jQuery;
	} );


/***/ },
/* 2 */
/***/ function(module, exports) {

	// ==ClosureCompiler==
	// @compilation_level ADVANCED_OPTIMIZATIONS
	// @externs_url https://raw.githubusercontent.com/google/closure-compiler/master/contrib/externs/maps/google_maps_api_v3_16.js
	// ==/ClosureCompiler==
	
	/**
	 * @name CSS3 InfoBubble with tabs for Google Maps API V3
	 * @version 0.8
	 * @author Luke Mahe
	 * @fileoverview
	 * This library is a CSS Infobubble with tabs. It uses css3 rounded corners and
	 * drop shadows and animations. It also allows tabs
	 */
	
	/*
	 * Licensed under the Apache License, Version 2.0 (the "License");
	 * you may not use this file except in compliance with the License.
	 * You may obtain a copy of the License at
	 *
	 *     http://www.apache.org/licenses/LICENSE-2.0
	 *
	 * Unless required by applicable law or agreed to in writing, software
	 * distributed under the License is distributed on an "AS IS" BASIS,
	 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	 * See the License for the specific language governing permissions and
	 * limitations under the License.
	 */
	
	
	/**
	 * A CSS3 InfoBubble v0.8
	 * @param {Object.<string, *>=} opt_options Optional properties to set.
	 * @extends {google.maps.OverlayView}
	 * @constructor
	 */
	function InfoBubble(opt_options) {
	  this.extend(InfoBubble, google.maps.OverlayView);
	  this.tabs_ = [];
	  this.activeTab_ = null;
	  this.baseZIndex_ = 100;
	  this.isOpen_ = false;
	
	  var options = opt_options || {};
	
	  if (options['backgroundColor'] == undefined) {
	    options['backgroundColor'] = this.BACKGROUND_COLOR_;
	  }
	
	  if (options['borderColor'] == undefined) {
	    options['borderColor'] = this.BORDER_COLOR_;
	  }
	
	  if (options['borderRadius'] == undefined) {
	    options['borderRadius'] = this.BORDER_RADIUS_;
	  }
	
	  if (options['borderWidth'] == undefined) {
	    options['borderWidth'] = this.BORDER_WIDTH_;
	  }
	
	  if (options['padding'] == undefined) {
	    options['padding'] = this.PADDING_;
	  }
	
	  if (options['arrowPosition'] == undefined) {
	    options['arrowPosition'] = this.ARROW_POSITION_;
	  }
	
	  if (options['disableAutoPan'] == undefined) {
	    options['disableAutoPan'] = false;
	  }
	
	  if (options['disableAnimation'] == undefined) {
	    options['disableAnimation'] = false;
	  }
	
	  if (options['minWidth'] == undefined) {
	    options['minWidth'] = this.MIN_WIDTH_;
	  }
	
	  if (options['shadowStyle'] == undefined) {
	    options['shadowStyle'] = this.SHADOW_STYLE_;
	  }
	
	  if (options['arrowSize'] == undefined) {
	    options['arrowSize'] = this.ARROW_SIZE_;
	  }
	
	  if (options['arrowStyle'] == undefined) {
	    options['arrowStyle'] = this.ARROW_STYLE_;
	  }
	
	  if (options['closeSrc'] == undefined) {
	    options['closeSrc'] = this.CLOSE_SRC_;
	  }
	
	  this.buildDom_();
	  this.setValues(options);
	}
	window['InfoBubble'] = InfoBubble;
	
	
	/**
	 * Default arrow size
	 * @const
	 * @private
	 */
	InfoBubble.prototype.ARROW_SIZE_ = 15;
	
	
	/**
	 * Default arrow style
	 * @const
	 * @private
	 */
	InfoBubble.prototype.ARROW_STYLE_ = 0;
	
	
	/**
	 * Default shadow style
	 * @const
	 * @private
	 */
	InfoBubble.prototype.SHADOW_STYLE_ = 1;
	
	
	/**
	 * Default min width
	 * @const
	 * @private
	 */
	InfoBubble.prototype.MIN_WIDTH_ = 50;
	
	
	/**
	 * Default arrow position
	 * @const
	 * @private
	 */
	InfoBubble.prototype.ARROW_POSITION_ = 50;
	
	
	/**
	 * Default padding
	 * @const
	 * @private
	 */
	InfoBubble.prototype.PADDING_ = 10;
	
	
	/**
	 * Default border width
	 * @const
	 * @private
	 */
	InfoBubble.prototype.BORDER_WIDTH_ = 1;
	
	
	/**
	 * Default border color
	 * @const
	 * @private
	 */
	InfoBubble.prototype.BORDER_COLOR_ = '#ccc';
	
	
	/**
	 * Default border radius
	 * @const
	 * @private
	 */
	InfoBubble.prototype.BORDER_RADIUS_ = 10;
	
	
	/**
	 * Default background color
	 * @const
	 * @private
	 */
	InfoBubble.prototype.BACKGROUND_COLOR_ = '#fff';
	
	/**
	 * Default close image source
	 * @const
	 * @private
	 */
	InfoBubble.prototype.CLOSE_SRC_ = 'https://maps.gstatic.com/intl/en_us/mapfiles/iw_close.gif';
	
	/**
	 * Extends a objects prototype by anothers.
	 *
	 * @param {Object} obj1 The object to be extended.
	 * @param {Object} obj2 The object to extend with.
	 * @return {Object} The new extended object.
	 * @ignore
	 */
	InfoBubble.prototype.extend = function(obj1, obj2) {
	  return (function(object) {
	    for (var property in object.prototype) {
	      this.prototype[property] = object.prototype[property];
	    }
	    return this;
	  }).apply(obj1, [obj2]);
	};
	
	
	/**
	 * Builds the InfoBubble dom
	 * @private
	 */
	InfoBubble.prototype.buildDom_ = function() {
	  var bubble = this.bubble_ = document.createElement('DIV');
	  bubble.style['position'] = 'absolute';
	  bubble.style['zIndex'] = this.baseZIndex_;
	
	  var tabsContainer = this.tabsContainer_ = document.createElement('DIV');
	  tabsContainer.style['position'] = 'relative';
	
	  // Close button
	  var close = this.close_ = document.createElement('IMG');
	  close.style['position'] = 'absolute';
	  close.style['border'] = 0;
	  close.style['zIndex'] = this.baseZIndex_ + 1;
	  close.style['cursor'] = 'pointer';
	  close.className = 'js-info-bubble-close';
	  close.src = this.get('closeSrc');
	
	  var that = this;
	  google.maps.event.addDomListener(close, 'click', function() {
	    that.close();
	    google.maps.event.trigger(that, 'closeclick');
	  });
	
	  // Content area
	  var contentContainer = this.contentContainer_ = document.createElement('DIV');
	  contentContainer.style['overflowX'] = 'auto';
	  contentContainer.style['overflowY'] = 'auto';
	  contentContainer.style['cursor'] = 'default';
	  contentContainer.style['clear'] = 'both';
	  contentContainer.style['position'] = 'relative';
	
	  var content = this.content_ = document.createElement('DIV');
	  contentContainer.appendChild(content);
	
	  // Arrow
	  var arrow = this.arrow_ = document.createElement('DIV');
	  arrow.style['position'] = 'relative';
	
	  var arrowOuter = this.arrowOuter_ = document.createElement('DIV');
	  var arrowInner = this.arrowInner_ = document.createElement('DIV');
	
	  var arrowSize = this.getArrowSize_();
	
	  arrowOuter.style['position'] = arrowInner.style['position'] = 'absolute';
	  arrowOuter.style['left'] = arrowInner.style['left'] = '50%';
	  arrowOuter.style['height'] = arrowInner.style['height'] = '0';
	  arrowOuter.style['width'] = arrowInner.style['width'] = '0';
	  arrowOuter.style['marginLeft'] = this.px(-arrowSize);
	  arrowOuter.style['borderWidth'] = this.px(arrowSize);
	  arrowOuter.style['borderBottomWidth'] = 0;
	
	  // Shadow
	  var bubbleShadow = this.bubbleShadow_ = document.createElement('DIV');
	  bubbleShadow.style['position'] = 'absolute';
	
	  // Hide the InfoBubble by default
	  bubble.style['display'] = bubbleShadow.style['display'] = 'none';
	
	  bubble.appendChild(this.tabsContainer_);
	  bubble.appendChild(close);
	  bubble.appendChild(contentContainer);
	  arrow.appendChild(arrowOuter);
	  arrow.appendChild(arrowInner);
	  bubble.appendChild(arrow);
	
	  var stylesheet = document.createElement('style');
	  stylesheet.setAttribute('type', 'text/css');
	
	  /**
	   * The animation for the infobubble
	   * @type {string}
	   */
	  this.animationName_ = '_ibani_' + Math.round(Math.random() * 10000);
	
	  var css = '.' + this.animationName_ + '{-webkit-animation-name:' +
	    this.animationName_ + ';-webkit-animation-duration:0.5s;' +
	    '-webkit-animation-iteration-count:1;}' +
	    '@-webkit-keyframes ' + this.animationName_ + ' {from {' +
	    '-webkit-transform: scale(0)}50% {-webkit-transform: scale(1.2)}90% ' +
	    '{-webkit-transform: scale(0.95)}to {-webkit-transform: scale(1)}}';
	
	  stylesheet.textContent = css;
	  document.getElementsByTagName('head')[0].appendChild(stylesheet);
	};
	
	
	/**
	 * Sets the background class name
	 *
	 * @param {string} className The class name to set.
	 */
	InfoBubble.prototype.setBackgroundClassName = function(className) {
	  this.set('backgroundClassName', className);
	};
	InfoBubble.prototype['setBackgroundClassName'] = InfoBubble.prototype.setBackgroundClassName;
	
	
	/**
	 * changed MVC callback
	 */
	InfoBubble.prototype.backgroundClassName_changed = function() {
	  this.content_.className = this.get('backgroundClassName');
	};
	InfoBubble.prototype['backgroundClassName_changed'] = InfoBubble.prototype.backgroundClassName_changed;
	
	
	/**
	 * Sets the class of the tab
	 *
	 * @param {string} className the class name to set.
	 */
	InfoBubble.prototype.setTabClassName = function(className) {
	  this.set('tabClassName', className);
	};
	InfoBubble.prototype['setTabClassName'] = InfoBubble.prototype.setTabClassName;
	
	
	/**
	 * tabClassName changed MVC callback
	 */
	InfoBubble.prototype.tabClassName_changed = function() {
	  this.updateTabStyles_();
	};
	InfoBubble.prototype['tabClassName_changed'] = InfoBubble.prototype.tabClassName_changed;
	
	
	/**
	 * Gets the style of the arrow
	 *
	 * @private
	 * @return {number} The style of the arrow.
	 */
	InfoBubble.prototype.getArrowStyle_ = function() {
	  return parseInt(this.get('arrowStyle'), 10) || 0;
	};
	
	
	/**
	 * Sets the style of the arrow
	 *
	 * @param {number} style The style of the arrow.
	 */
	InfoBubble.prototype.setArrowStyle = function(style) {
	  this.set('arrowStyle', style);
	};
	InfoBubble.prototype['setArrowStyle'] = InfoBubble.prototype.setArrowStyle;
	
	
	/**
	 * Arrow style changed MVC callback
	 */
	InfoBubble.prototype.arrowStyle_changed = function() {
	  this.arrowSize_changed();
	};
	InfoBubble.prototype['arrowStyle_changed'] = InfoBubble.prototype.arrowStyle_changed;
	
	
	/**
	 * Gets the size of the arrow
	 *
	 * @private
	 * @return {number} The size of the arrow.
	 */
	InfoBubble.prototype.getArrowSize_ = function() {
	  return parseInt(this.get('arrowSize'), 10) || 0;
	};
	
	
	/**
	 * Sets the size of the arrow
	 *
	 * @param {number} size The size of the arrow.
	 */
	InfoBubble.prototype.setArrowSize = function(size) {
	  this.set('arrowSize', size);
	};
	InfoBubble.prototype['setArrowSize'] = InfoBubble.prototype.setArrowSize;
	
	
	/**
	 * Arrow size changed MVC callback
	 */
	InfoBubble.prototype.arrowSize_changed = function() {
	  this.borderWidth_changed();
	};
	InfoBubble.prototype['arrowSize_changed'] = InfoBubble.prototype.arrowSize_changed;
	
	
	/**
	 * Set the position of the InfoBubble arrow
	 *
	 * @param {number} pos The position to set.
	 */
	InfoBubble.prototype.setArrowPosition = function(pos) {
	  this.set('arrowPosition', pos);
	};
	InfoBubble.prototype['setArrowPosition'] = InfoBubble.prototype.setArrowPosition;
	
	
	/**
	 * Get the position of the InfoBubble arrow
	 *
	 * @private
	 * @return {number} The position..
	 */
	InfoBubble.prototype.getArrowPosition_ = function() {
	  return parseInt(this.get('arrowPosition'), 10) || 0;
	};
	
	
	/**
	 * arrowPosition changed MVC callback
	 */
	InfoBubble.prototype.arrowPosition_changed = function() {
	  var pos = this.getArrowPosition_();
	  this.arrowOuter_.style['left'] = this.arrowInner_.style['left'] = pos + '%';
	
	  this.redraw_();
	};
	InfoBubble.prototype['arrowPosition_changed'] = InfoBubble.prototype.arrowPosition_changed;
	
	
	/**
	 * Set the zIndex of the InfoBubble
	 *
	 * @param {number} zIndex The zIndex to set.
	 */
	InfoBubble.prototype.setZIndex = function(zIndex) {
	  this.set('zIndex', zIndex);
	};
	InfoBubble.prototype['setZIndex'] = InfoBubble.prototype.setZIndex;
	
	
	/**
	 * Get the zIndex of the InfoBubble
	 *
	 * @return {number} The zIndex to set.
	 */
	InfoBubble.prototype.getZIndex = function() {
	  return parseInt(this.get('zIndex'), 10) || this.baseZIndex_;
	};
	
	
	/**
	 * zIndex changed MVC callback
	 */
	InfoBubble.prototype.zIndex_changed = function() {
	  var zIndex = this.getZIndex();
	
	  this.bubble_.style['zIndex'] = this.baseZIndex_ = zIndex;
	  this.close_.style['zIndex'] = zIndex + 1;
	};
	InfoBubble.prototype['zIndex_changed'] = InfoBubble.prototype.zIndex_changed;
	
	
	/**
	 * Set the style of the shadow
	 *
	 * @param {number} shadowStyle The style of the shadow.
	 */
	InfoBubble.prototype.setShadowStyle = function(shadowStyle) {
	  this.set('shadowStyle', shadowStyle);
	};
	InfoBubble.prototype['setShadowStyle'] = InfoBubble.prototype.setShadowStyle;
	
	
	/**
	 * Get the style of the shadow
	 *
	 * @private
	 * @return {number} The style of the shadow.
	 */
	InfoBubble.prototype.getShadowStyle_ = function() {
	  return parseInt(this.get('shadowStyle'), 10) || 0;
	};
	
	
	/**
	 * shadowStyle changed MVC callback
	 */
	InfoBubble.prototype.shadowStyle_changed = function() {
	  var shadowStyle = this.getShadowStyle_();
	
	  var display = '';
	  var shadow = '';
	  var backgroundColor = '';
	  switch (shadowStyle) {
	    case 0:
	      display = 'none';
	      break;
	    case 1:
	      shadow = '40px 15px 10px rgba(33,33,33,0.3)';
	      backgroundColor = 'transparent';
	      break;
	    case 2:
	      shadow = '0 0 2px rgba(33,33,33,0.3)';
	      backgroundColor = 'rgba(33,33,33,0.35)';
	      break;
	  }
	  this.bubbleShadow_.style['boxShadow'] =
	    this.bubbleShadow_.style['webkitBoxShadow'] =
	      this.bubbleShadow_.style['MozBoxShadow'] = shadow;
	  this.bubbleShadow_.style['backgroundColor'] = backgroundColor;
	  if (this.isOpen_) {
	    this.bubbleShadow_.style['display'] = display;
	    this.draw();
	  }
	};
	InfoBubble.prototype['shadowStyle_changed'] = InfoBubble.prototype.shadowStyle_changed;
	
	
	/**
	 * Show the close button
	 */
	InfoBubble.prototype.showCloseButton = function() {
	  this.set('hideCloseButton', false);
	};
	InfoBubble.prototype['showCloseButton'] = InfoBubble.prototype.showCloseButton;
	
	
	/**
	 * Hide the close button
	 */
	InfoBubble.prototype.hideCloseButton = function() {
	  this.set('hideCloseButton', true);
	};
	InfoBubble.prototype['hideCloseButton'] = InfoBubble.prototype.hideCloseButton;
	
	
	/**
	 * hideCloseButton changed MVC callback
	 */
	InfoBubble.prototype.hideCloseButton_changed = function() {
	  this.close_.style['display'] = this.get('hideCloseButton') ? 'none' : '';
	};
	InfoBubble.prototype['hideCloseButton_changed'] = InfoBubble.prototype.hideCloseButton_changed;
	
	
	/**
	 * Set the background color
	 *
	 * @param {string} color The color to set.
	 */
	InfoBubble.prototype.setBackgroundColor = function(color) {
	  if (color) {
	    this.set('backgroundColor', color);
	  }
	};
	InfoBubble.prototype['setBackgroundColor'] = InfoBubble.prototype.setBackgroundColor;
	
	
	/**
	 * backgroundColor changed MVC callback
	 */
	InfoBubble.prototype.backgroundColor_changed = function() {
	  var backgroundColor = this.get('backgroundColor');
	  this.contentContainer_.style['backgroundColor'] = backgroundColor;
	
	  this.arrowInner_.style['borderColor'] = backgroundColor +
	    ' transparent transparent';
	  this.updateTabStyles_();
	};
	InfoBubble.prototype['backgroundColor_changed'] = InfoBubble.prototype.backgroundColor_changed;
	
	
	/**
	 * Set the border color
	 *
	 * @param {string} color The border color.
	 */
	InfoBubble.prototype.setBorderColor = function(color) {
	  if (color) {
	    this.set('borderColor', color);
	  }
	};
	InfoBubble.prototype['setBorderColor'] = InfoBubble.prototype.setBorderColor;
	
	
	/**
	 * borderColor changed MVC callback
	 */
	InfoBubble.prototype.borderColor_changed = function() {
	  var borderColor = this.get('borderColor');
	
	  var contentContainer = this.contentContainer_;
	  var arrowOuter = this.arrowOuter_;
	  contentContainer.style['borderColor'] = borderColor;
	
	  arrowOuter.style['borderColor'] = borderColor +
	    ' transparent transparent';
	
	  contentContainer.style['borderStyle'] =
	    arrowOuter.style['borderStyle'] =
	      this.arrowInner_.style['borderStyle'] = 'solid';
	
	  this.updateTabStyles_();
	};
	InfoBubble.prototype['borderColor_changed'] = InfoBubble.prototype.borderColor_changed;
	
	
	/**
	 * Set the radius of the border
	 *
	 * @param {number} radius The radius of the border.
	 */
	InfoBubble.prototype.setBorderRadius = function(radius) {
	  this.set('borderRadius', radius);
	};
	InfoBubble.prototype['setBorderRadius'] = InfoBubble.prototype.setBorderRadius;
	
	
	/**
	 * Get the radius of the border
	 *
	 * @private
	 * @return {number} The radius of the border.
	 */
	InfoBubble.prototype.getBorderRadius_ = function() {
	  return parseInt(this.get('borderRadius'), 10) || 0;
	};
	
	
	/**
	 * borderRadius changed MVC callback
	 */
	InfoBubble.prototype.borderRadius_changed = function() {
	  var borderRadius = this.getBorderRadius_();
	  var borderWidth = this.getBorderWidth_();
	
	  this.contentContainer_.style['borderRadius'] =
	    this.contentContainer_.style['MozBorderRadius'] =
	      this.contentContainer_.style['webkitBorderRadius'] =
	        this.bubbleShadow_.style['borderRadius'] =
	          this.bubbleShadow_.style['MozBorderRadius'] =
	            this.bubbleShadow_.style['webkitBorderRadius'] = this.px(borderRadius);
	
	  this.tabsContainer_.style['paddingLeft'] =
	    this.tabsContainer_.style['paddingRight'] =
	      this.px(borderRadius + borderWidth);
	
	  this.redraw_();
	};
	InfoBubble.prototype['borderRadius_changed'] = InfoBubble.prototype.borderRadius_changed;
	
	
	/**
	 * Get the width of the border
	 *
	 * @private
	 * @return {number} width The width of the border.
	 */
	InfoBubble.prototype.getBorderWidth_ = function() {
	  return parseInt(this.get('borderWidth'), 10) || 0;
	};
	
	
	/**
	 * Set the width of the border
	 *
	 * @param {number} width The width of the border.
	 */
	InfoBubble.prototype.setBorderWidth = function(width) {
	  this.set('borderWidth', width);
	};
	InfoBubble.prototype['setBorderWidth'] = InfoBubble.prototype.setBorderWidth;
	
	
	/**
	 * borderWidth change MVC callback
	 */
	InfoBubble.prototype.borderWidth_changed = function() {
	  var borderWidth = this.getBorderWidth_();
	
	  this.contentContainer_.style['borderWidth'] = this.px(borderWidth);
	  this.tabsContainer_.style['top'] = this.px(borderWidth);
	
	  this.updateArrowStyle_();
	  this.updateTabStyles_();
	  this.borderRadius_changed();
	  this.redraw_();
	};
	InfoBubble.prototype['borderWidth_changed'] = InfoBubble.prototype.borderWidth_changed;
	
	
	/**
	 * Update the arrow style
	 * @private
	 */
	InfoBubble.prototype.updateArrowStyle_ = function() {
	  var borderWidth = this.getBorderWidth_();
	  var arrowSize = this.getArrowSize_();
	  var arrowStyle = this.getArrowStyle_();
	  var arrowOuterSizePx = this.px(arrowSize);
	  var arrowInnerSizePx = this.px(Math.max(0, arrowSize - borderWidth));
	
	  var outer = this.arrowOuter_;
	  var inner = this.arrowInner_;
	
	  this.arrow_.style['marginTop'] = this.px(-borderWidth);
	  outer.style['borderTopWidth'] = arrowOuterSizePx;
	  inner.style['borderTopWidth'] = arrowInnerSizePx;
	
	  // Full arrow or arrow pointing to the left
	  if (arrowStyle == 0 || arrowStyle == 1) {
	    outer.style['borderLeftWidth'] = arrowOuterSizePx;
	    inner.style['borderLeftWidth'] = arrowInnerSizePx;
	  } else {
	    outer.style['borderLeftWidth'] = inner.style['borderLeftWidth'] = 0;
	  }
	
	  // Full arrow or arrow pointing to the right
	  if (arrowStyle == 0 || arrowStyle == 2) {
	    outer.style['borderRightWidth'] = arrowOuterSizePx;
	    inner.style['borderRightWidth'] = arrowInnerSizePx;
	  } else {
	    outer.style['borderRightWidth'] = inner.style['borderRightWidth'] = 0;
	  }
	
	  if (arrowStyle < 2) {
	    outer.style['marginLeft'] = this.px(-(arrowSize));
	    inner.style['marginLeft'] = this.px(-(arrowSize - borderWidth));
	  } else {
	    outer.style['marginLeft'] = inner.style['marginLeft'] = 0;
	  }
	
	  // If there is no border then don't show thw outer arrow
	  if (borderWidth == 0) {
	    outer.style['display'] = 'none';
	  } else {
	    outer.style['display'] = '';
	  }
	};
	
	
	/**
	 * Set the padding of the InfoBubble
	 *
	 * @param {number} padding The padding to apply.
	 */
	InfoBubble.prototype.setPadding = function(padding) {
	  this.set('padding', padding);
	};
	InfoBubble.prototype['setPadding'] = InfoBubble.prototype.setPadding;
	
	
	/**
	 * Set the close image url
	 *
	 * @param {string} src The url of the image used as a close icon
	 */
	InfoBubble.prototype.setCloseSrc = function(src) {
	  if (src && this.close_) {
	    this.close_.src = src;
	  }
	};
	InfoBubble.prototype['setCloseSrc'] = InfoBubble.prototype.setCloseSrc;
	
	
	/**
	 * Set the padding of the InfoBubble
	 *
	 * @private
	 * @return {number} padding The padding to apply.
	 */
	InfoBubble.prototype.getPadding_ = function() {
	  return parseInt(this.get('padding'), 10) || 0;
	};
	
	
	/**
	 * padding changed MVC callback
	 */
	InfoBubble.prototype.padding_changed = function() {
	  var padding = this.getPadding_();
	  this.contentContainer_.style['padding'] = this.px(padding);
	  this.updateTabStyles_();
	
	  this.redraw_();
	};
	InfoBubble.prototype['padding_changed'] = InfoBubble.prototype.padding_changed;
	
	
	/**
	 * Add px extention to the number
	 *
	 * @param {number} num The number to wrap.
	 * @return {string|number} A wrapped number.
	 */
	InfoBubble.prototype.px = function(num) {
	  if (num) {
	    // 0 doesn't need to be wrapped
	    return num + 'px';
	  }
	  return num;
	};
	
	
	/**
	 * Add events to stop propagation
	 * @private
	 */
	InfoBubble.prototype.addEvents_ = function() {
	  // We want to cancel all the events so they do not go to the map
	  var events = ['mousedown', 'mousemove', 'mouseover', 'mouseout', 'mouseup',
	    'mousewheel', 'DOMMouseScroll', 'touchstart', 'touchend', 'touchmove',
	    'dblclick', 'contextmenu', 'click'];
	
	  var bubble = this.bubble_;
	  this.listeners_ = [];
	  for (var i = 0, event; event = events[i]; i++) {
	    this.listeners_.push(
	      google.maps.event.addDomListener(bubble, event, function(e) {
	        e.cancelBubble = true;
	        if (e.stopPropagation) {
	          e.stopPropagation();
	        }
	      })
	    );
	  }
	};
	
	
	/**
	 * On Adding the InfoBubble to a map
	 * Implementing the OverlayView interface
	 */
	InfoBubble.prototype.onAdd = function() {
	  if (!this.bubble_) {
	    this.buildDom_();
	  }
	
	  this.addEvents_();
	
	  var panes = this.getPanes();
	  if (panes) {
	    panes.floatPane.appendChild(this.bubble_);
	    panes.floatShadow.appendChild(this.bubbleShadow_);
	  }
	
	  /* once the infoBubble has been added to the DOM, fire 'domready' event */
	  google.maps.event.trigger(this, 'domready');
	};
	InfoBubble.prototype['onAdd'] = InfoBubble.prototype.onAdd;
	
	
	/**
	 * Draw the InfoBubble
	 * Implementing the OverlayView interface
	 */
	InfoBubble.prototype.draw = function() {
	  var projection = this.getProjection();
	
	  if (!projection) {
	    // The map projection is not ready yet so do nothing
	    return;
	  }
	
	  var latLng = /** @type {google.maps.LatLng} */ (this.get('position'));
	
	  if (!latLng) {
	    this.close();
	    return;
	  }
	
	  var tabHeight = 0;
	
	  if (this.activeTab_) {
	    tabHeight = this.activeTab_.offsetHeight;
	  }
	
	  var anchorHeight = this.getAnchorHeight_();
	  var arrowSize = this.getArrowSize_();
	  var arrowPosition = this.getArrowPosition_();
	
	  arrowPosition = arrowPosition / 100;
	
	  var pos = projection.fromLatLngToDivPixel(latLng);
	  var width = this.contentContainer_.offsetWidth;
	  var height = this.bubble_.offsetHeight;
	
	  if (!width) {
	    return;
	  }
	
	  // Adjust for the height of the info bubble
	  var top = pos.y - (height + arrowSize);
	
	  if (anchorHeight) {
	    // If there is an anchor then include the height
	    top -= anchorHeight;
	  }
	
	  var left = pos.x - (width * arrowPosition);
	
	  this.bubble_.style['top'] = this.px(top);
	  this.bubble_.style['left'] = this.px(left);
	
	  var shadowStyle = parseInt(this.get('shadowStyle'), 10);
	
	  switch (shadowStyle) {
	    case 1:
	      // Shadow is behind
	      this.bubbleShadow_.style['top'] = this.px(top + tabHeight - 1);
	      this.bubbleShadow_.style['left'] = this.px(left);
	      this.bubbleShadow_.style['width'] = this.px(width);
	      this.bubbleShadow_.style['height'] =
	        this.px(this.contentContainer_.offsetHeight - arrowSize);
	      break;
	    case 2:
	      // Shadow is below
	      width = width * 0.8;
	      if (anchorHeight) {
	        this.bubbleShadow_.style['top'] = this.px(pos.y);
	      } else {
	        this.bubbleShadow_.style['top'] = this.px(pos.y + arrowSize);
	      }
	      this.bubbleShadow_.style['left'] = this.px(pos.x - width * arrowPosition);
	
	      this.bubbleShadow_.style['width'] = this.px(width);
	      this.bubbleShadow_.style['height'] = this.px(2);
	      break;
	  }
	};
	InfoBubble.prototype['draw'] = InfoBubble.prototype.draw;
	
	
	/**
	 * Removing the InfoBubble from a map
	 */
	InfoBubble.prototype.onRemove = function() {
	  if (this.bubble_ && this.bubble_.parentNode) {
	    this.bubble_.parentNode.removeChild(this.bubble_);
	  }
	  if (this.bubbleShadow_ && this.bubbleShadow_.parentNode) {
	    this.bubbleShadow_.parentNode.removeChild(this.bubbleShadow_);
	  }
	
	  for (var i = 0, listener; listener = this.listeners_[i]; i++) {
	    google.maps.event.removeListener(listener);
	  }
	};
	InfoBubble.prototype['onRemove'] = InfoBubble.prototype.onRemove;
	
	
	/**
	 * Is the InfoBubble open
	 *
	 * @return {boolean} If the InfoBubble is open.
	 */
	InfoBubble.prototype.isOpen = function() {
	  return this.isOpen_;
	};
	InfoBubble.prototype['isOpen'] = InfoBubble.prototype.isOpen;
	
	
	/**
	 * Close the InfoBubble
	 */
	InfoBubble.prototype.close = function() {
	  if (this.bubble_) {
	    this.bubble_.style['display'] = 'none';
	    // Remove the animation so we next time it opens it will animate again
	    this.bubble_.className =
	      this.bubble_.className.replace(this.animationName_, '');
	  }
	
	  if (this.bubbleShadow_) {
	    this.bubbleShadow_.style['display'] = 'none';
	    this.bubbleShadow_.className =
	      this.bubbleShadow_.className.replace(this.animationName_, '');
	  }
	  this.isOpen_ = false;
	};
	InfoBubble.prototype['close'] = InfoBubble.prototype.close;
	
	
	/**
	 * Open the InfoBubble (asynchronous).
	 *
	 * @param {google.maps.Map=} opt_map Optional map to open on.
	 * @param {google.maps.MVCObject=} opt_anchor Optional anchor to position at.
	 */
	InfoBubble.prototype.open = function(opt_map, opt_anchor) {
	  var that = this;
	  window.setTimeout(function() {
	    that.open_(opt_map, opt_anchor);
	  }, 0);
	};
	
	
	/**
	 * Open the InfoBubble
	 * @private
	 * @param {google.maps.Map=} opt_map Optional map to open on.
	 * @param {google.maps.MVCObject=} opt_anchor Optional anchor to position at.
	 */
	InfoBubble.prototype.open_ = function(opt_map, opt_anchor) {
	  this.updateContent_();
	
	  if (opt_map) {
	    this.setMap(opt_map);
	  }
	
	  if (opt_anchor) {
	    this.set('anchor', opt_anchor);
	    this.bindTo('anchorPoint', opt_anchor);
	    this.bindTo('position', opt_anchor);
	  }
	
	  // Show the bubble and the show
	  this.bubble_.style['display'] = this.bubbleShadow_.style['display'] = '';
	  var animation = !this.get('disableAnimation');
	
	  if (animation) {
	    // Add the animation
	    this.bubble_.className += ' ' + this.animationName_;
	    this.bubbleShadow_.className += ' ' + this.animationName_;
	  }
	
	  this.redraw_();
	  this.isOpen_ = true;
	
	  var pan = !this.get('disableAutoPan');
	  if (pan) {
	    var that = this;
	    window.setTimeout(function() {
	      // Pan into view, done in a time out to make it feel nicer :)
	      that.panToView();
	    }, 200);
	  }
	};
	InfoBubble.prototype['open'] = InfoBubble.prototype.open;
	
	
	/**
	 * Set the position of the InfoBubble
	 *
	 * @param {google.maps.LatLng} position The position to set.
	 */
	InfoBubble.prototype.setPosition = function(position) {
	  if (position) {
	    this.set('position', position);
	  }
	};
	InfoBubble.prototype['setPosition'] = InfoBubble.prototype.setPosition;
	
	
	/**
	 * Returns the position of the InfoBubble
	 *
	 * @return {google.maps.LatLng} the position.
	 */
	InfoBubble.prototype.getPosition = function() {
	  return /** @type {google.maps.LatLng} */ (this.get('position'));
	};
	InfoBubble.prototype['getPosition'] = InfoBubble.prototype.getPosition;
	
	
	/**
	 * position changed MVC callback
	 */
	InfoBubble.prototype.position_changed = function() {
	  this.draw();
	};
	InfoBubble.prototype['position_changed'] = InfoBubble.prototype.position_changed;
	
	
	/**
	 * Pan the InfoBubble into view
	 */
	InfoBubble.prototype.panToView = function() {
	  var projection = this.getProjection();
	
	  if (!projection) {
	    // The map projection is not ready yet so do nothing
	    return;
	  }
	
	  if (!this.bubble_) {
	    // No Bubble yet so do nothing
	    return;
	  }
	
	  var anchorHeight = this.getAnchorHeight_();
	  var height = this.bubble_.offsetHeight + anchorHeight;
	  var map = this.get('map');
	  var mapDiv = map.getDiv();
	  var mapHeight = mapDiv.offsetHeight;
	
	  var latLng = this.getPosition();
	  var centerPos = projection.fromLatLngToContainerPixel(map.getCenter());
	  var pos = projection.fromLatLngToContainerPixel(latLng);
	
	  // Find out how much space at the top is free
	  var spaceTop = centerPos.y - height;
	
	  // Fine out how much space at the bottom is free
	  var spaceBottom = mapHeight - centerPos.y;
	
	  var needsTop = spaceTop < 0;
	  var deltaY = 0;
	
	  if (needsTop) {
	    spaceTop *= -1;
	    deltaY = (spaceTop + spaceBottom) / 2;
	  }
	
	  pos.y -= deltaY;
	  latLng = projection.fromContainerPixelToLatLng(pos);
	
	  if (map.getCenter() != latLng) {
	    map.panTo(latLng);
	  }
	};
	InfoBubble.prototype['panToView'] = InfoBubble.prototype.panToView;
	
	
	/**
	 * Converts a HTML string to a document fragment.
	 *
	 * @param {string} htmlString The HTML string to convert.
	 * @return {Node} A HTML document fragment.
	 * @private
	 */
	InfoBubble.prototype.htmlToDocumentFragment_ = function(htmlString) {
	  htmlString = htmlString.replace(/^\s*([\S\s]*)\b\s*$/, '$1');
	  var tempDiv = document.createElement('DIV');
	  tempDiv.innerHTML = htmlString;
	  if (tempDiv.childNodes.length == 1) {
	    return /** @type {!Node} */ (tempDiv.removeChild(tempDiv.firstChild));
	  } else {
	    var fragment = document.createDocumentFragment();
	    while (tempDiv.firstChild) {
	      fragment.appendChild(tempDiv.firstChild);
	    }
	    return fragment;
	  }
	};
	
	
	/**
	 * Removes all children from the node.
	 *
	 * @param {Node} node The node to remove all children from.
	 * @private
	 */
	InfoBubble.prototype.removeChildren_ = function(node) {
	  if (!node) {
	    return;
	  }
	
	  var child;
	  while (child = node.firstChild) {
	    node.removeChild(child);
	  }
	};
	
	
	/**
	 * Sets the content of the infobubble.
	 *
	 * @param {string|Node} content The content to set.
	 */
	InfoBubble.prototype.setContent = function(content) {
	  this.set('content', content);
	};
	InfoBubble.prototype['setContent'] = InfoBubble.prototype.setContent;
	
	
	/**
	 * Get the content of the infobubble.
	 *
	 * @return {string|Node} The marker content.
	 */
	InfoBubble.prototype.getContent = function() {
	  return /** @type {Node|string} */ (this.get('content'));
	};
	InfoBubble.prototype['getContent'] = InfoBubble.prototype.getContent;
	
	
	/**
	 * Sets the marker content and adds loading events to images
	 */
	InfoBubble.prototype.updateContent_ = function() {
	  if (!this.content_) {
	    // The Content area doesnt exist.
	    return;
	  }
	
	  this.removeChildren_(this.content_);
	  var content = this.getContent();
	  if (content) {
	    if (typeof content == 'string') {
	      content = this.htmlToDocumentFragment_(content);
	    }
	    this.content_.appendChild(content);
	
	    var that = this;
	    var images = this.content_.getElementsByTagName('IMG');
	    for (var i = 0, image; image = images[i]; i++) {
	      // Because we don't know the size of an image till it loads, add a
	      // listener to the image load so the marker can resize and reposition
	      // itself to be the correct height.
	      google.maps.event.addDomListener(image, 'load', function() {
	        that.imageLoaded_();
	      });
	    }
	  }
	  this.redraw_();
	};
	
	
	/**
	 * Image loaded
	 * @private
	 */
	InfoBubble.prototype.imageLoaded_ = function() {
	  var pan = !this.get('disableAutoPan');
	  this.redraw_();
	  if (pan && (this.tabs_.length == 0 || this.activeTab_.index == 0)) {
	    this.panToView();
	  }
	};
	
	
	/**
	 * Updates the styles of the tabs
	 * @private
	 */
	InfoBubble.prototype.updateTabStyles_ = function() {
	  if (this.tabs_ && this.tabs_.length) {
	    for (var i = 0, tab; tab = this.tabs_[i]; i++) {
	      this.setTabStyle_(tab.tab);
	    }
	    this.activeTab_.style['zIndex'] = this.baseZIndex_;
	    var borderWidth = this.getBorderWidth_();
	    var padding = this.getPadding_() / 2;
	    this.activeTab_.style['borderBottomWidth'] = 0;
	    this.activeTab_.style['paddingBottom'] = this.px(padding + borderWidth);
	  }
	};
	
	
	/**
	 * Sets the style of a tab
	 * @private
	 * @param {Element} tab The tab to style.
	 */
	InfoBubble.prototype.setTabStyle_ = function(tab) {
	  var backgroundColor = this.get('backgroundColor');
	  var borderColor = this.get('borderColor');
	  var borderRadius = this.getBorderRadius_();
	  var borderWidth = this.getBorderWidth_();
	  var padding = this.getPadding_();
	
	  var marginRight = this.px(-(Math.max(padding, borderRadius)));
	  var borderRadiusPx = this.px(borderRadius);
	
	  var index = this.baseZIndex_;
	  if (tab.index) {
	    index -= tab.index;
	  }
	
	  // The styles for the tab
	  var styles = {
	    'cssFloat': 'left',
	    'position': 'relative',
	    'cursor': 'pointer',
	    'backgroundColor': backgroundColor,
	    'border': this.px(borderWidth) + ' solid ' + borderColor,
	    'padding': this.px(padding / 2) + ' ' + this.px(padding),
	    'marginRight': marginRight,
	    'whiteSpace': 'nowrap',
	    'borderRadiusTopLeft': borderRadiusPx,
	    'MozBorderRadiusTopleft': borderRadiusPx,
	    'webkitBorderTopLeftRadius': borderRadiusPx,
	    'borderRadiusTopRight': borderRadiusPx,
	    'MozBorderRadiusTopright': borderRadiusPx,
	    'webkitBorderTopRightRadius': borderRadiusPx,
	    'zIndex': index,
	    'display': 'inline'
	  };
	
	  for (var style in styles) {
	    tab.style[style] = styles[style];
	  }
	
	  var className = this.get('tabClassName');
	  if (className != undefined) {
	    tab.className += ' ' + className;
	  }
	};
	
	
	/**
	 * Add user actions to a tab
	 * @private
	 * @param {Object} tab The tab to add the actions to.
	 */
	InfoBubble.prototype.addTabActions_ = function(tab) {
	  var that = this;
	  tab.listener_ = google.maps.event.addDomListener(tab, 'click', function() {
	    that.setTabActive_(this);
	  });
	};
	
	
	/**
	 * Set a tab at a index to be active
	 *
	 * @param {number} index The index of the tab.
	 */
	InfoBubble.prototype.setTabActive = function(index) {
	  var tab = this.tabs_[index - 1];
	
	  if (tab) {
	    this.setTabActive_(tab.tab);
	  }
	};
	InfoBubble.prototype['setTabActive'] = InfoBubble.prototype.setTabActive;
	
	
	/**
	 * Set a tab to be active
	 * @private
	 * @param {Object} tab The tab to set active.
	 */
	InfoBubble.prototype.setTabActive_ = function(tab) {
	  if (!tab) {
	    this.setContent('');
	    this.updateContent_();
	    return;
	  }
	
	  var padding = this.getPadding_() / 2;
	  var borderWidth = this.getBorderWidth_();
	
	  if (this.activeTab_) {
	    var activeTab = this.activeTab_;
	    activeTab.style['zIndex'] = this.baseZIndex_ - activeTab.index;
	    activeTab.style['paddingBottom'] = this.px(padding);
	    activeTab.style['borderBottomWidth'] = this.px(borderWidth);
	  }
	
	  tab.style['zIndex'] = this.baseZIndex_;
	  tab.style['borderBottomWidth'] = 0;
	  tab.style['marginBottomWidth'] = '-10px';
	  tab.style['paddingBottom'] = this.px(padding + borderWidth);
	
	  this.setContent(this.tabs_[tab.index].content);
	  this.updateContent_();
	
	  this.activeTab_ = tab;
	
	  this.redraw_();
	};
	
	
	/**
	 * Set the max width of the InfoBubble
	 *
	 * @param {number} width The max width.
	 */
	InfoBubble.prototype.setMaxWidth = function(width) {
	  this.set('maxWidth', width);
	};
	InfoBubble.prototype['setMaxWidth'] = InfoBubble.prototype.setMaxWidth;
	
	
	/**
	 * maxWidth changed MVC callback
	 */
	InfoBubble.prototype.maxWidth_changed = function() {
	  this.redraw_();
	};
	InfoBubble.prototype['maxWidth_changed'] = InfoBubble.prototype.maxWidth_changed;
	
	
	/**
	 * Set the max height of the InfoBubble
	 *
	 * @param {number} height The max height.
	 */
	InfoBubble.prototype.setMaxHeight = function(height) {
	  this.set('maxHeight', height);
	};
	InfoBubble.prototype['setMaxHeight'] = InfoBubble.prototype.setMaxHeight;
	
	
	/**
	 * maxHeight changed MVC callback
	 */
	InfoBubble.prototype.maxHeight_changed = function() {
	  this.redraw_();
	};
	InfoBubble.prototype['maxHeight_changed'] = InfoBubble.prototype.maxHeight_changed;
	
	
	/**
	 * Set the min width of the InfoBubble
	 *
	 * @param {number} width The min width.
	 */
	InfoBubble.prototype.setMinWidth = function(width) {
	  this.set('minWidth', width);
	};
	InfoBubble.prototype['setMinWidth'] = InfoBubble.prototype.setMinWidth;
	
	
	/**
	 * minWidth changed MVC callback
	 */
	InfoBubble.prototype.minWidth_changed = function() {
	  this.redraw_();
	};
	InfoBubble.prototype['minWidth_changed'] = InfoBubble.prototype.minWidth_changed;
	
	
	/**
	 * Set the min height of the InfoBubble
	 *
	 * @param {number} height The min height.
	 */
	InfoBubble.prototype.setMinHeight = function(height) {
	  this.set('minHeight', height);
	};
	InfoBubble.prototype['setMinHeight'] = InfoBubble.prototype.setMinHeight;
	
	
	/**
	 * minHeight changed MVC callback
	 */
	InfoBubble.prototype.minHeight_changed = function() {
	  this.redraw_();
	};
	InfoBubble.prototype['minHeight_changed'] = InfoBubble.prototype.minHeight_changed;
	
	
	/**
	 * Add a tab
	 *
	 * @param {string} label The label of the tab.
	 * @param {string|Element} content The content of the tab.
	 */
	InfoBubble.prototype.addTab = function(label, content) {
	  var tab = document.createElement('DIV');
	  tab.innerHTML = label;
	
	  this.setTabStyle_(tab);
	  this.addTabActions_(tab);
	
	  this.tabsContainer_.appendChild(tab);
	
	  this.tabs_.push({
	    label: label,
	    content: content,
	    tab: tab
	  });
	
	  tab.index = this.tabs_.length - 1;
	  tab.style['zIndex'] = this.baseZIndex_ - tab.index;
	
	  if (!this.activeTab_) {
	    this.setTabActive_(tab);
	  }
	
	  tab.className = tab.className + ' ' + this.animationName_;
	
	  this.redraw_();
	};
	InfoBubble.prototype['addTab'] = InfoBubble.prototype.addTab;
	
	
	/**
	 * Update a tab at a speicifc index
	 *
	 * @param {number} index The index of the tab.
	 * @param {?string} opt_label The label to change to.
	 * @param {?string} opt_content The content to update to.
	 */
	InfoBubble.prototype.updateTab = function(index, opt_label, opt_content) {
	  if (!this.tabs_.length || index < 0 || index >= this.tabs_.length) {
	    return;
	  }
	
	  var tab = this.tabs_[index];
	  if (opt_label != undefined) {
	    tab.tab.innerHTML = tab.label = opt_label;
	  }
	
	  if (opt_content != undefined) {
	    tab.content = opt_content;
	  }
	
	  if (this.activeTab_ == tab.tab) {
	    this.setContent(tab.content);
	    this.updateContent_();
	  }
	  this.redraw_();
	};
	InfoBubble.prototype['updateTab'] = InfoBubble.prototype.updateTab;
	
	
	/**
	 * Remove a tab at a specific index
	 *
	 * @param {number} index The index of the tab to remove.
	 */
	InfoBubble.prototype.removeTab = function(index) {
	  if (!this.tabs_.length || index < 0 || index >= this.tabs_.length) {
	    return;
	  }
	
	  var tab = this.tabs_[index];
	  tab.tab.parentNode.removeChild(tab.tab);
	
	  google.maps.event.removeListener(tab.tab.listener_);
	
	  this.tabs_.splice(index, 1);
	
	  delete tab;
	
	  for (var i = 0, t; t = this.tabs_[i]; i++) {
	    t.tab.index = i;
	  }
	
	  if (tab.tab == this.activeTab_) {
	    // Removing the current active tab
	    if (this.tabs_[index]) {
	      // Show the tab to the right
	      this.activeTab_ = this.tabs_[index].tab;
	    } else if (this.tabs_[index - 1]) {
	      // Show a tab to the left
	      this.activeTab_ = this.tabs_[index - 1].tab;
	    } else {
	      // No tabs left to sho
	      this.activeTab_ = undefined;
	    }
	
	    this.setTabActive_(this.activeTab_);
	  }
	
	  this.redraw_();
	};
	InfoBubble.prototype['removeTab'] = InfoBubble.prototype.removeTab;
	
	
	/**
	 * Get the size of an element
	 * @private
	 * @param {Node|string} element The element to size.
	 * @param {number=} opt_maxWidth Optional max width of the element.
	 * @param {number=} opt_maxHeight Optional max height of the element.
	 * @return {google.maps.Size} The size of the element.
	 */
	InfoBubble.prototype.getElementSize_ = function(element, opt_maxWidth,
	                                                opt_maxHeight) {
	  var sizer = document.createElement('DIV');
	  sizer.style['display'] = 'inline';
	  sizer.style['position'] = 'absolute';
	  sizer.style['visibility'] = 'hidden';
	
	  if (typeof element == 'string') {
	    sizer.innerHTML = element;
	  } else {
	    sizer.appendChild(element.cloneNode(true));
	  }
	
	  document.body.appendChild(sizer);
	  var size = new google.maps.Size(sizer.offsetWidth, sizer.offsetHeight);
	
	  // If the width is bigger than the max width then set the width and size again
	  if (opt_maxWidth && size.width > opt_maxWidth) {
	    sizer.style['width'] = this.px(opt_maxWidth);
	    size = new google.maps.Size(sizer.offsetWidth, sizer.offsetHeight);
	  }
	
	  // If the height is bigger than the max height then set the height and size
	  // again
	  if (opt_maxHeight && size.height > opt_maxHeight) {
	    sizer.style['height'] = this.px(opt_maxHeight);
	    size = new google.maps.Size(sizer.offsetWidth, sizer.offsetHeight);
	  }
	
	  document.body.removeChild(sizer);
	  delete sizer;
	  return size;
	};
	
	
	/**
	 * Redraw the InfoBubble
	 * @private
	 */
	InfoBubble.prototype.redraw_ = function() {
	  this.figureOutSize_();
	  this.positionCloseButton_();
	  this.draw();
	};
	
	
	/**
	 * Figure out the optimum size of the InfoBubble
	 * @private
	 */
	InfoBubble.prototype.figureOutSize_ = function() {
	  var map = this.get('map');
	
	  if (!map) {
	    return;
	  }
	
	  var padding = this.getPadding_();
	  var borderWidth = this.getBorderWidth_();
	  var borderRadius = this.getBorderRadius_();
	  var arrowSize = this.getArrowSize_();
	
	  var mapDiv = map.getDiv();
	  var gutter = arrowSize * 2;
	  var mapWidth = mapDiv.offsetWidth - gutter;
	  var mapHeight = mapDiv.offsetHeight - gutter - this.getAnchorHeight_();
	  var tabHeight = 0;
	  var width = /** @type {number} */ (this.get('minWidth') || 0);
	  var height = /** @type {number} */ (this.get('minHeight') || 0);
	  var maxWidth = /** @type {number} */ (this.get('maxWidth') || 0);
	  var maxHeight = /** @type {number} */ (this.get('maxHeight') || 0);
	
	  maxWidth = Math.min(mapWidth, maxWidth);
	  maxHeight = Math.min(mapHeight, maxHeight);
	
	  var tabWidth = 0;
	  if (this.tabs_.length) {
	    // If there are tabs then you need to check the size of each tab's content
	    for (var i = 0, tab; tab = this.tabs_[i]; i++) {
	      var tabSize = this.getElementSize_(tab.tab, maxWidth, maxHeight);
	      var contentSize = this.getElementSize_(tab.content, maxWidth, maxHeight);
	
	      if (width < tabSize.width) {
	        width = tabSize.width;
	      }
	
	      // Add up all the tab widths because they might end up being wider than
	      // the content
	      tabWidth += tabSize.width;
	
	      if (height < tabSize.height) {
	        height = tabSize.height;
	      }
	
	      if (tabSize.height > tabHeight) {
	        tabHeight = tabSize.height;
	      }
	
	      if (width < contentSize.width) {
	        width = contentSize.width;
	      }
	
	      if (height < contentSize.height) {
	        height = contentSize.height;
	      }
	    }
	  } else {
	    var content = /** @type {string|Node} */ (this.get('content'));
	    if (typeof content == 'string') {
	      content = this.htmlToDocumentFragment_(content);
	    }
	    if (content) {
	      var contentSize = this.getElementSize_(content, maxWidth, maxHeight);
	
	      if (width < contentSize.width) {
	        width = contentSize.width;
	      }
	
	      if (height < contentSize.height) {
	        height = contentSize.height;
	      }
	    }
	  }
	
	  if (maxWidth) {
	    width = Math.min(width, maxWidth);
	  }
	
	  if (maxHeight) {
	    height = Math.min(height, maxHeight);
	  }
	
	  width = Math.max(width, tabWidth);
	
	  if (width == tabWidth) {
	    width = width + 2 * padding;
	  }
	
	  arrowSize = arrowSize * 2;
	  width = Math.max(width, arrowSize);
	
	  // Maybe add this as a option so they can go bigger than the map if the user
	  // wants
	  if (width > mapWidth) {
	    width = mapWidth;
	  }
	
	  if (height > mapHeight) {
	    height = mapHeight - tabHeight;
	  }
	
	  if (this.tabsContainer_) {
	    this.tabHeight_ = tabHeight;
	    this.tabsContainer_.style['width'] = this.px(tabWidth);
	  }
	
	  this.contentContainer_.style['width'] = this.px(width);
	  this.contentContainer_.style['height'] = this.px(height);
	};
	
	
	/**
	 *  Get the height of the anchor
	 *
	 *  This function is a hack for now and doesn't really work that good, need to
	 *  wait for pixelBounds to be correctly exposed.
	 *  @private
	 *  @return {number} The height of the anchor.
	 */
	InfoBubble.prototype.getAnchorHeight_ = function() {
	  var anchor = this.get('anchor');
	  if (anchor) {
	    var anchorPoint = /** @type google.maps.Point */(this.get('anchorPoint'));
	
	    if (anchorPoint) {
	      return -1 * anchorPoint.y;
	    }
	  }
	  return 0;
	};
	
	InfoBubble.prototype.anchorPoint_changed = function() {
	  this.draw();
	};
	InfoBubble.prototype['anchorPoint_changed'] = InfoBubble.prototype.anchorPoint_changed;
	
	
	/**
	 * Position the close button in the right spot.
	 * @private
	 */
	InfoBubble.prototype.positionCloseButton_ = function() {
	  var br = this.getBorderRadius_();
	  var bw = this.getBorderWidth_();
	
	  var right = 2;
	  var top = 2;
	
	  if (this.tabs_.length && this.tabHeight_) {
	    top += this.tabHeight_;
	  }
	
	  top += bw;
	  right += bw;
	
	  var c = this.contentContainer_;
	  if (c && c.clientHeight < c.scrollHeight) {
	    // If there are scrollbars then move the cross in so it is not over
	    // scrollbar
	    right += 15;
	  }
	
	  this.close_.style['right'] = this.px(right);
	  this.close_.style['top'] = this.px(top);
	};

/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	module.exports = __webpack_require__(4);


/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;'use strict';
	
	var ps = __webpack_require__(5);
	var psInstances = __webpack_require__(10);
	
	function mountJQuery(jQuery) {
	  jQuery.fn.perfectScrollbar = function (settingOrCommand) {
	    return this.each(function () {
	      if (typeof settingOrCommand === 'object' ||
	          typeof settingOrCommand === 'undefined') {
	        // If it's an object or none, initialize.
	        var settings = settingOrCommand;
	
	        if (!psInstances.get(this)) {
	          ps.initialize(this, settings);
	        }
	      } else {
	        // Unless, it may be a command.
	        var command = settingOrCommand;
	
	        if (command === 'update') {
	          ps.update(this);
	        } else if (command === 'destroy') {
	          ps.destroy(this);
	        }
	      }
	    });
	  };
	}
	
	if (true) {
	  // AMD. Register as an anonymous module.
	  !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(1)], __WEBPACK_AMD_DEFINE_FACTORY__ = (mountJQuery), __WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ? (__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	} else {
	  var jq = window.jQuery ? window.jQuery : window.$;
	  if (typeof jq !== 'undefined') {
	    mountJQuery(jq);
	  }
	}
	
	module.exports = mountJQuery;


/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var destroy = __webpack_require__(6);
	var initialize = __webpack_require__(14);
	var update = __webpack_require__(24);
	
	module.exports = {
	  initialize: initialize,
	  update: update,
	  destroy: destroy
	};


/***/ },
/* 6 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _ = __webpack_require__(7);
	var dom = __webpack_require__(9);
	var instances = __webpack_require__(10);
	
	module.exports = function (element) {
	  var i = instances.get(element);
	
	  if (!i) {
	    return;
	  }
	
	  i.event.unbindAll();
	  dom.remove(i.scrollbarX);
	  dom.remove(i.scrollbarY);
	  dom.remove(i.scrollbarXRail);
	  dom.remove(i.scrollbarYRail);
	  _.removePsClasses(element);
	
	  instances.remove(element);
	};


/***/ },
/* 7 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var cls = __webpack_require__(8);
	var dom = __webpack_require__(9);
	
	var toInt = exports.toInt = function (x) {
	  return parseInt(x, 10) || 0;
	};
	
	var clone = exports.clone = function (obj) {
	  if (!obj) {
	    return null;
	  } else if (obj.constructor === Array) {
	    return obj.map(clone);
	  } else if (typeof obj === 'object') {
	    var result = {};
	    for (var key in obj) {
	      result[key] = clone(obj[key]);
	    }
	    return result;
	  } else {
	    return obj;
	  }
	};
	
	exports.extend = function (original, source) {
	  var result = clone(original);
	  for (var key in source) {
	    result[key] = clone(source[key]);
	  }
	  return result;
	};
	
	exports.isEditable = function (el) {
	  return dom.matches(el, "input,[contenteditable]") ||
	         dom.matches(el, "select,[contenteditable]") ||
	         dom.matches(el, "textarea,[contenteditable]") ||
	         dom.matches(el, "button,[contenteditable]");
	};
	
	exports.removePsClasses = function (element) {
	  var clsList = cls.list(element);
	  for (var i = 0; i < clsList.length; i++) {
	    var className = clsList[i];
	    if (className.indexOf('ps-') === 0) {
	      cls.remove(element, className);
	    }
	  }
	};
	
	exports.outerWidth = function (element) {
	  return toInt(dom.css(element, 'width')) +
	         toInt(dom.css(element, 'paddingLeft')) +
	         toInt(dom.css(element, 'paddingRight')) +
	         toInt(dom.css(element, 'borderLeftWidth')) +
	         toInt(dom.css(element, 'borderRightWidth'));
	};
	
	exports.startScrolling = function (element, axis) {
	  cls.add(element, 'ps-in-scrolling');
	  if (typeof axis !== 'undefined') {
	    cls.add(element, 'ps-' + axis);
	  } else {
	    cls.add(element, 'ps-x');
	    cls.add(element, 'ps-y');
	  }
	};
	
	exports.stopScrolling = function (element, axis) {
	  cls.remove(element, 'ps-in-scrolling');
	  if (typeof axis !== 'undefined') {
	    cls.remove(element, 'ps-' + axis);
	  } else {
	    cls.remove(element, 'ps-x');
	    cls.remove(element, 'ps-y');
	  }
	};
	
	exports.env = {
	  isWebKit: 'WebkitAppearance' in document.documentElement.style,
	  supportsTouch: (('ontouchstart' in window) || window.DocumentTouch && document instanceof window.DocumentTouch),
	  supportsIePointer: window.navigator.msMaxTouchPoints !== null
	};


/***/ },
/* 8 */
/***/ function(module, exports) {

	'use strict';
	
	function oldAdd(element, className) {
	  var classes = element.className.split(' ');
	  if (classes.indexOf(className) < 0) {
	    classes.push(className);
	  }
	  element.className = classes.join(' ');
	}
	
	function oldRemove(element, className) {
	  var classes = element.className.split(' ');
	  var idx = classes.indexOf(className);
	  if (idx >= 0) {
	    classes.splice(idx, 1);
	  }
	  element.className = classes.join(' ');
	}
	
	exports.add = function (element, className) {
	  if (element.classList) {
	    element.classList.add(className);
	  } else {
	    oldAdd(element, className);
	  }
	};
	
	exports.remove = function (element, className) {
	  if (element.classList) {
	    element.classList.remove(className);
	  } else {
	    oldRemove(element, className);
	  }
	};
	
	exports.list = function (element) {
	  if (element.classList) {
	    return Array.prototype.slice.apply(element.classList);
	  } else {
	    return element.className.split(' ');
	  }
	};


/***/ },
/* 9 */
/***/ function(module, exports) {

	'use strict';
	
	var DOM = {};
	
	DOM.e = function (tagName, className) {
	  var element = document.createElement(tagName);
	  element.className = className;
	  return element;
	};
	
	DOM.appendTo = function (child, parent) {
	  parent.appendChild(child);
	  return child;
	};
	
	function cssGet(element, styleName) {
	  return window.getComputedStyle(element)[styleName];
	}
	
	function cssSet(element, styleName, styleValue) {
	  if (typeof styleValue === 'number') {
	    styleValue = styleValue.toString() + 'px';
	  }
	  element.style[styleName] = styleValue;
	  return element;
	}
	
	function cssMultiSet(element, obj) {
	  for (var key in obj) {
	    var val = obj[key];
	    if (typeof val === 'number') {
	      val = val.toString() + 'px';
	    }
	    element.style[key] = val;
	  }
	  return element;
	}
	
	DOM.css = function (element, styleNameOrObject, styleValue) {
	  if (typeof styleNameOrObject === 'object') {
	    // multiple set with object
	    return cssMultiSet(element, styleNameOrObject);
	  } else {
	    if (typeof styleValue === 'undefined') {
	      return cssGet(element, styleNameOrObject);
	    } else {
	      return cssSet(element, styleNameOrObject, styleValue);
	    }
	  }
	};
	
	DOM.matches = function (element, query) {
	  if (typeof element.matches !== 'undefined') {
	    return element.matches(query);
	  } else {
	    if (typeof element.matchesSelector !== 'undefined') {
	      return element.matchesSelector(query);
	    } else if (typeof element.webkitMatchesSelector !== 'undefined') {
	      return element.webkitMatchesSelector(query);
	    } else if (typeof element.mozMatchesSelector !== 'undefined') {
	      return element.mozMatchesSelector(query);
	    } else if (typeof element.msMatchesSelector !== 'undefined') {
	      return element.msMatchesSelector(query);
	    }
	  }
	};
	
	DOM.remove = function (element) {
	  if (typeof element.remove !== 'undefined') {
	    element.remove();
	  } else {
	    if (element.parentNode) {
	      element.parentNode.removeChild(element);
	    }
	  }
	};
	
	DOM.queryChildren = function (element, selector) {
	  return Array.prototype.filter.call(element.childNodes, function (child) {
	    return DOM.matches(child, selector);
	  });
	};
	
	module.exports = DOM;


/***/ },
/* 10 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _ = __webpack_require__(7);
	var cls = __webpack_require__(8);
	var defaultSettings = __webpack_require__(11);
	var dom = __webpack_require__(9);
	var EventManager = __webpack_require__(12);
	var guid = __webpack_require__(13);
	
	var instances = {};
	
	function Instance(element) {
	  var i = this;
	
	  i.settings = _.clone(defaultSettings);
	  i.containerWidth = null;
	  i.containerHeight = null;
	  i.contentWidth = null;
	  i.contentHeight = null;
	
	  i.isRtl = dom.css(element, 'direction') === "rtl";
	  i.isNegativeScroll = (function () {
	    var originalScrollLeft = element.scrollLeft;
	    var result = null;
	    element.scrollLeft = -1;
	    result = element.scrollLeft < 0;
	    element.scrollLeft = originalScrollLeft;
	    return result;
	  })();
	  i.negativeScrollAdjustment = i.isNegativeScroll ? element.scrollWidth - element.clientWidth : 0;
	  i.event = new EventManager();
	  i.ownerDocument = element.ownerDocument || document;
	
	  function focus() {
	    cls.add(element, 'ps-focus');
	  }
	
	  function blur() {
	    cls.remove(element, 'ps-focus');
	  }
	
	  i.scrollbarXRail = dom.appendTo(dom.e('div', 'ps-scrollbar-x-rail'), element);
	  i.scrollbarX = dom.appendTo(dom.e('div', 'ps-scrollbar-x'), i.scrollbarXRail);
	  i.scrollbarX.setAttribute('tabindex', 0);
	  i.event.bind(i.scrollbarX, 'focus', focus);
	  i.event.bind(i.scrollbarX, 'blur', blur);
	  i.scrollbarXActive = null;
	  i.scrollbarXWidth = null;
	  i.scrollbarXLeft = null;
	  i.scrollbarXBottom = _.toInt(dom.css(i.scrollbarXRail, 'bottom'));
	  i.isScrollbarXUsingBottom = i.scrollbarXBottom === i.scrollbarXBottom; // !isNaN
	  i.scrollbarXTop = i.isScrollbarXUsingBottom ? null : _.toInt(dom.css(i.scrollbarXRail, 'top'));
	  i.railBorderXWidth = _.toInt(dom.css(i.scrollbarXRail, 'borderLeftWidth')) + _.toInt(dom.css(i.scrollbarXRail, 'borderRightWidth'));
	  // Set rail to display:block to calculate margins
	  dom.css(i.scrollbarXRail, 'display', 'block');
	  i.railXMarginWidth = _.toInt(dom.css(i.scrollbarXRail, 'marginLeft')) + _.toInt(dom.css(i.scrollbarXRail, 'marginRight'));
	  dom.css(i.scrollbarXRail, 'display', '');
	  i.railXWidth = null;
	  i.railXRatio = null;
	
	  i.scrollbarYRail = dom.appendTo(dom.e('div', 'ps-scrollbar-y-rail'), element);
	  i.scrollbarY = dom.appendTo(dom.e('div', 'ps-scrollbar-y'), i.scrollbarYRail);
	  i.scrollbarY.setAttribute('tabindex', 0);
	  i.event.bind(i.scrollbarY, 'focus', focus);
	  i.event.bind(i.scrollbarY, 'blur', blur);
	  i.scrollbarYActive = null;
	  i.scrollbarYHeight = null;
	  i.scrollbarYTop = null;
	  i.scrollbarYRight = _.toInt(dom.css(i.scrollbarYRail, 'right'));
	  i.isScrollbarYUsingRight = i.scrollbarYRight === i.scrollbarYRight; // !isNaN
	  i.scrollbarYLeft = i.isScrollbarYUsingRight ? null : _.toInt(dom.css(i.scrollbarYRail, 'left'));
	  i.scrollbarYOuterWidth = i.isRtl ? _.outerWidth(i.scrollbarY) : null;
	  i.railBorderYWidth = _.toInt(dom.css(i.scrollbarYRail, 'borderTopWidth')) + _.toInt(dom.css(i.scrollbarYRail, 'borderBottomWidth'));
	  dom.css(i.scrollbarYRail, 'display', 'block');
	  i.railYMarginHeight = _.toInt(dom.css(i.scrollbarYRail, 'marginTop')) + _.toInt(dom.css(i.scrollbarYRail, 'marginBottom'));
	  dom.css(i.scrollbarYRail, 'display', '');
	  i.railYHeight = null;
	  i.railYRatio = null;
	}
	
	function getId(element) {
	  return element.getAttribute('data-ps-id');
	}
	
	function setId(element, id) {
	  element.setAttribute('data-ps-id', id);
	}
	
	function removeId(element) {
	  element.removeAttribute('data-ps-id');
	}
	
	exports.add = function (element) {
	  var newId = guid();
	  setId(element, newId);
	  instances[newId] = new Instance(element);
	  return instances[newId];
	};
	
	exports.remove = function (element) {
	  delete instances[getId(element)];
	  removeId(element);
	};
	
	exports.get = function (element) {
	  return instances[getId(element)];
	};


/***/ },
/* 11 */
/***/ function(module, exports) {

	'use strict';
	
	module.exports = {
	  handlers: ['click-rail', 'drag-scrollbar', 'keyboard', 'wheel', 'touch'],
	  maxScrollbarLength: null,
	  minScrollbarLength: null,
	  scrollXMarginOffset: 0,
	  scrollYMarginOffset: 0,
	  suppressScrollX: false,
	  suppressScrollY: false,
	  swipePropagation: true,
	  useBothWheelAxes: false,
	  wheelPropagation: false,
	  wheelSpeed: 1,
	  theme: 'default'
	};


/***/ },
/* 12 */
/***/ function(module, exports) {

	'use strict';
	
	var EventElement = function (element) {
	  this.element = element;
	  this.events = {};
	};
	
	EventElement.prototype.bind = function (eventName, handler) {
	  if (typeof this.events[eventName] === 'undefined') {
	    this.events[eventName] = [];
	  }
	  this.events[eventName].push(handler);
	  this.element.addEventListener(eventName, handler, false);
	};
	
	EventElement.prototype.unbind = function (eventName, handler) {
	  var isHandlerProvided = (typeof handler !== 'undefined');
	  this.events[eventName] = this.events[eventName].filter(function (hdlr) {
	    if (isHandlerProvided && hdlr !== handler) {
	      return true;
	    }
	    this.element.removeEventListener(eventName, hdlr, false);
	    return false;
	  }, this);
	};
	
	EventElement.prototype.unbindAll = function () {
	  for (var name in this.events) {
	    this.unbind(name);
	  }
	};
	
	var EventManager = function () {
	  this.eventElements = [];
	};
	
	EventManager.prototype.eventElement = function (element) {
	  var ee = this.eventElements.filter(function (eventElement) {
	    return eventElement.element === element;
	  })[0];
	  if (typeof ee === 'undefined') {
	    ee = new EventElement(element);
	    this.eventElements.push(ee);
	  }
	  return ee;
	};
	
	EventManager.prototype.bind = function (element, eventName, handler) {
	  this.eventElement(element).bind(eventName, handler);
	};
	
	EventManager.prototype.unbind = function (element, eventName, handler) {
	  this.eventElement(element).unbind(eventName, handler);
	};
	
	EventManager.prototype.unbindAll = function () {
	  for (var i = 0; i < this.eventElements.length; i++) {
	    this.eventElements[i].unbindAll();
	  }
	};
	
	EventManager.prototype.once = function (element, eventName, handler) {
	  var ee = this.eventElement(element);
	  var onceHandler = function (e) {
	    ee.unbind(eventName, onceHandler);
	    handler(e);
	  };
	  ee.bind(eventName, onceHandler);
	};
	
	module.exports = EventManager;


/***/ },
/* 13 */
/***/ function(module, exports) {

	'use strict';
	
	module.exports = (function () {
	  function s4() {
	    return Math.floor((1 + Math.random()) * 0x10000)
	               .toString(16)
	               .substring(1);
	  }
	  return function () {
	    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
	           s4() + '-' + s4() + s4() + s4();
	  };
	})();


/***/ },
/* 14 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _ = __webpack_require__(7);
	var cls = __webpack_require__(8);
	var instances = __webpack_require__(10);
	var updateGeometry = __webpack_require__(15);
	
	// Handlers
	var handlers = {
	  'click-rail': __webpack_require__(17),
	  'drag-scrollbar': __webpack_require__(18),
	  'keyboard': __webpack_require__(19),
	  'wheel': __webpack_require__(20),
	  'touch': __webpack_require__(21),
	  'selection': __webpack_require__(22)
	};
	var nativeScrollHandler = __webpack_require__(23);
	
	module.exports = function (element, userSettings) {
	  userSettings = typeof userSettings === 'object' ? userSettings : {};
	
	  cls.add(element, 'ps-container');
	
	  // Create a plugin instance.
	  var i = instances.add(element);
	
	  i.settings = _.extend(i.settings, userSettings);
	  cls.add(element, 'ps-theme-' + i.settings.theme);
	
	  i.settings.handlers.forEach(function (handlerName) {
	    handlers[handlerName](element);
	  });
	
	  nativeScrollHandler(element);
	
	  updateGeometry(element);
	};


/***/ },
/* 15 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _ = __webpack_require__(7);
	var cls = __webpack_require__(8);
	var dom = __webpack_require__(9);
	var instances = __webpack_require__(10);
	var updateScroll = __webpack_require__(16);
	
	function getThumbSize(i, thumbSize) {
	  if (i.settings.minScrollbarLength) {
	    thumbSize = Math.max(thumbSize, i.settings.minScrollbarLength);
	  }
	  if (i.settings.maxScrollbarLength) {
	    thumbSize = Math.min(thumbSize, i.settings.maxScrollbarLength);
	  }
	  return thumbSize;
	}
	
	function updateCss(element, i) {
	  var xRailOffset = {width: i.railXWidth};
	  if (i.isRtl) {
	    xRailOffset.left = i.negativeScrollAdjustment + element.scrollLeft + i.containerWidth - i.contentWidth;
	  } else {
	    xRailOffset.left = element.scrollLeft;
	  }
	  if (i.isScrollbarXUsingBottom) {
	    xRailOffset.bottom = i.scrollbarXBottom - element.scrollTop;
	  } else {
	    xRailOffset.top = i.scrollbarXTop + element.scrollTop;
	  }
	  dom.css(i.scrollbarXRail, xRailOffset);
	
	  var yRailOffset = {top: element.scrollTop, height: i.railYHeight};
	  if (i.isScrollbarYUsingRight) {
	    if (i.isRtl) {
	      yRailOffset.right = i.contentWidth - (i.negativeScrollAdjustment + element.scrollLeft) - i.scrollbarYRight - i.scrollbarYOuterWidth;
	    } else {
	      yRailOffset.right = i.scrollbarYRight - element.scrollLeft;
	    }
	  } else {
	    if (i.isRtl) {
	      yRailOffset.left = i.negativeScrollAdjustment + element.scrollLeft + i.containerWidth * 2 - i.contentWidth - i.scrollbarYLeft - i.scrollbarYOuterWidth;
	    } else {
	      yRailOffset.left = i.scrollbarYLeft + element.scrollLeft;
	    }
	  }
	  dom.css(i.scrollbarYRail, yRailOffset);
	
	  dom.css(i.scrollbarX, {left: i.scrollbarXLeft, width: i.scrollbarXWidth - i.railBorderXWidth});
	  dom.css(i.scrollbarY, {top: i.scrollbarYTop, height: i.scrollbarYHeight - i.railBorderYWidth});
	}
	
	module.exports = function (element) {
	  var i = instances.get(element);
	
	  i.containerWidth = element.clientWidth;
	  i.containerHeight = element.clientHeight;
	  i.contentWidth = element.scrollWidth;
	  i.contentHeight = element.scrollHeight;
	
	  var existingRails;
	  if (!element.contains(i.scrollbarXRail)) {
	    existingRails = dom.queryChildren(element, '.ps-scrollbar-x-rail');
	    if (existingRails.length > 0) {
	      existingRails.forEach(function (rail) {
	        dom.remove(rail);
	      });
	    }
	    dom.appendTo(i.scrollbarXRail, element);
	  }
	  if (!element.contains(i.scrollbarYRail)) {
	    existingRails = dom.queryChildren(element, '.ps-scrollbar-y-rail');
	    if (existingRails.length > 0) {
	      existingRails.forEach(function (rail) {
	        dom.remove(rail);
	      });
	    }
	    dom.appendTo(i.scrollbarYRail, element);
	  }
	
	  if (!i.settings.suppressScrollX && i.containerWidth + i.settings.scrollXMarginOffset < i.contentWidth) {
	    i.scrollbarXActive = true;
	    i.railXWidth = i.containerWidth - i.railXMarginWidth;
	    i.railXRatio = i.containerWidth / i.railXWidth;
	    i.scrollbarXWidth = getThumbSize(i, _.toInt(i.railXWidth * i.containerWidth / i.contentWidth));
	    i.scrollbarXLeft = _.toInt((i.negativeScrollAdjustment + element.scrollLeft) * (i.railXWidth - i.scrollbarXWidth) / (i.contentWidth - i.containerWidth));
	  } else {
	    i.scrollbarXActive = false;
	  }
	
	  if (!i.settings.suppressScrollY && i.containerHeight + i.settings.scrollYMarginOffset < i.contentHeight) {
	    i.scrollbarYActive = true;
	    i.railYHeight = i.containerHeight - i.railYMarginHeight;
	    i.railYRatio = i.containerHeight / i.railYHeight;
	    i.scrollbarYHeight = getThumbSize(i, _.toInt(i.railYHeight * i.containerHeight / i.contentHeight));
	    i.scrollbarYTop = _.toInt(element.scrollTop * (i.railYHeight - i.scrollbarYHeight) / (i.contentHeight - i.containerHeight));
	  } else {
	    i.scrollbarYActive = false;
	  }
	
	  if (i.scrollbarXLeft >= i.railXWidth - i.scrollbarXWidth) {
	    i.scrollbarXLeft = i.railXWidth - i.scrollbarXWidth;
	  }
	  if (i.scrollbarYTop >= i.railYHeight - i.scrollbarYHeight) {
	    i.scrollbarYTop = i.railYHeight - i.scrollbarYHeight;
	  }
	
	  updateCss(element, i);
	
	  if (i.scrollbarXActive) {
	    cls.add(element, 'ps-active-x');
	  } else {
	    cls.remove(element, 'ps-active-x');
	    i.scrollbarXWidth = 0;
	    i.scrollbarXLeft = 0;
	    updateScroll(element, 'left', 0);
	  }
	  if (i.scrollbarYActive) {
	    cls.add(element, 'ps-active-y');
	  } else {
	    cls.remove(element, 'ps-active-y');
	    i.scrollbarYHeight = 0;
	    i.scrollbarYTop = 0;
	    updateScroll(element, 'top', 0);
	  }
	};


/***/ },
/* 16 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var instances = __webpack_require__(10);
	
	var lastTop;
	var lastLeft;
	
	var createDOMEvent = function (name) {
	  var event = document.createEvent("Event");
	  event.initEvent(name, true, true);
	  return event;
	};
	
	module.exports = function (element, axis, value) {
	  if (typeof element === 'undefined') {
	    throw 'You must provide an element to the update-scroll function';
	  }
	
	  if (typeof axis === 'undefined') {
	    throw 'You must provide an axis to the update-scroll function';
	  }
	
	  if (typeof value === 'undefined') {
	    throw 'You must provide a value to the update-scroll function';
	  }
	
	  if (axis === 'top' && value <= 0) {
	    element.scrollTop = value = 0; // don't allow negative scroll
	    element.dispatchEvent(createDOMEvent('ps-y-reach-start'));
	  }
	
	  if (axis === 'left' && value <= 0) {
	    element.scrollLeft = value = 0; // don't allow negative scroll
	    element.dispatchEvent(createDOMEvent('ps-x-reach-start'));
	  }
	
	  var i = instances.get(element);
	
	  if (axis === 'top' && value >= i.contentHeight - i.containerHeight) {
	    // don't allow scroll past container
	    value = i.contentHeight - i.containerHeight;
	    if (value - element.scrollTop <= 1) {
	      // mitigates rounding errors on non-subpixel scroll values
	      value = element.scrollTop;
	    } else {
	      element.scrollTop = value;
	    }
	    element.dispatchEvent(createDOMEvent('ps-y-reach-end'));
	  }
	
	  if (axis === 'left' && value >= i.contentWidth - i.containerWidth) {
	    // don't allow scroll past container
	    value = i.contentWidth - i.containerWidth;
	    if (value - element.scrollLeft <= 1) {
	      // mitigates rounding errors on non-subpixel scroll values
	      value = element.scrollLeft;
	    } else {
	      element.scrollLeft = value;
	    }
	    element.dispatchEvent(createDOMEvent('ps-x-reach-end'));
	  }
	
	  if (!lastTop) {
	    lastTop = element.scrollTop;
	  }
	
	  if (!lastLeft) {
	    lastLeft = element.scrollLeft;
	  }
	
	  if (axis === 'top' && value < lastTop) {
	    element.dispatchEvent(createDOMEvent('ps-scroll-up'));
	  }
	
	  if (axis === 'top' && value > lastTop) {
	    element.dispatchEvent(createDOMEvent('ps-scroll-down'));
	  }
	
	  if (axis === 'left' && value < lastLeft) {
	    element.dispatchEvent(createDOMEvent('ps-scroll-left'));
	  }
	
	  if (axis === 'left' && value > lastLeft) {
	    element.dispatchEvent(createDOMEvent('ps-scroll-right'));
	  }
	
	  if (axis === 'top') {
	    element.scrollTop = lastTop = value;
	    element.dispatchEvent(createDOMEvent('ps-scroll-y'));
	  }
	
	  if (axis === 'left') {
	    element.scrollLeft = lastLeft = value;
	    element.dispatchEvent(createDOMEvent('ps-scroll-x'));
	  }
	
	};


/***/ },
/* 17 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var instances = __webpack_require__(10);
	var updateGeometry = __webpack_require__(15);
	var updateScroll = __webpack_require__(16);
	
	function bindClickRailHandler(element, i) {
	  function pageOffset(el) {
	    return el.getBoundingClientRect();
	  }
	  var stopPropagation = function (e) { e.stopPropagation(); };
	
	  i.event.bind(i.scrollbarY, 'click', stopPropagation);
	  i.event.bind(i.scrollbarYRail, 'click', function (e) {
	    var positionTop = e.pageY - window.pageYOffset - pageOffset(i.scrollbarYRail).top;
	    var direction = positionTop > i.scrollbarYTop ? 1 : -1;
	
	    updateScroll(element, 'top', element.scrollTop + direction * i.containerHeight);
	    updateGeometry(element);
	
	    e.stopPropagation();
	  });
	
	  i.event.bind(i.scrollbarX, 'click', stopPropagation);
	  i.event.bind(i.scrollbarXRail, 'click', function (e) {
	    var positionLeft = e.pageX - window.pageXOffset - pageOffset(i.scrollbarXRail).left;
	    var direction = positionLeft > i.scrollbarXLeft ? 1 : -1;
	
	    updateScroll(element, 'left', element.scrollLeft + direction * i.containerWidth);
	    updateGeometry(element);
	
	    e.stopPropagation();
	  });
	}
	
	module.exports = function (element) {
	  var i = instances.get(element);
	  bindClickRailHandler(element, i);
	};


/***/ },
/* 18 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _ = __webpack_require__(7);
	var dom = __webpack_require__(9);
	var instances = __webpack_require__(10);
	var updateGeometry = __webpack_require__(15);
	var updateScroll = __webpack_require__(16);
	
	function bindMouseScrollXHandler(element, i) {
	  var currentLeft = null;
	  var currentPageX = null;
	
	  function updateScrollLeft(deltaX) {
	    var newLeft = currentLeft + (deltaX * i.railXRatio);
	    var maxLeft = Math.max(0, i.scrollbarXRail.getBoundingClientRect().left) + (i.railXRatio * (i.railXWidth - i.scrollbarXWidth));
	
	    if (newLeft < 0) {
	      i.scrollbarXLeft = 0;
	    } else if (newLeft > maxLeft) {
	      i.scrollbarXLeft = maxLeft;
	    } else {
	      i.scrollbarXLeft = newLeft;
	    }
	
	    var scrollLeft = _.toInt(i.scrollbarXLeft * (i.contentWidth - i.containerWidth) / (i.containerWidth - (i.railXRatio * i.scrollbarXWidth))) - i.negativeScrollAdjustment;
	    updateScroll(element, 'left', scrollLeft);
	  }
	
	  var mouseMoveHandler = function (e) {
	    updateScrollLeft(e.pageX - currentPageX);
	    updateGeometry(element);
	    e.stopPropagation();
	    e.preventDefault();
	  };
	
	  var mouseUpHandler = function () {
	    _.stopScrolling(element, 'x');
	    i.event.unbind(i.ownerDocument, 'mousemove', mouseMoveHandler);
	  };
	
	  i.event.bind(i.scrollbarX, 'mousedown', function (e) {
	    currentPageX = e.pageX;
	    currentLeft = _.toInt(dom.css(i.scrollbarX, 'left')) * i.railXRatio;
	    _.startScrolling(element, 'x');
	
	    i.event.bind(i.ownerDocument, 'mousemove', mouseMoveHandler);
	    i.event.once(i.ownerDocument, 'mouseup', mouseUpHandler);
	
	    e.stopPropagation();
	    e.preventDefault();
	  });
	}
	
	function bindMouseScrollYHandler(element, i) {
	  var currentTop = null;
	  var currentPageY = null;
	
	  function updateScrollTop(deltaY) {
	    var newTop = currentTop + (deltaY * i.railYRatio);
	    var maxTop = Math.max(0, i.scrollbarYRail.getBoundingClientRect().top) + (i.railYRatio * (i.railYHeight - i.scrollbarYHeight));
	
	    if (newTop < 0) {
	      i.scrollbarYTop = 0;
	    } else if (newTop > maxTop) {
	      i.scrollbarYTop = maxTop;
	    } else {
	      i.scrollbarYTop = newTop;
	    }
	
	    var scrollTop = _.toInt(i.scrollbarYTop * (i.contentHeight - i.containerHeight) / (i.containerHeight - (i.railYRatio * i.scrollbarYHeight)));
	    updateScroll(element, 'top', scrollTop);
	  }
	
	  var mouseMoveHandler = function (e) {
	    updateScrollTop(e.pageY - currentPageY);
	    updateGeometry(element);
	    e.stopPropagation();
	    e.preventDefault();
	  };
	
	  var mouseUpHandler = function () {
	    _.stopScrolling(element, 'y');
	    i.event.unbind(i.ownerDocument, 'mousemove', mouseMoveHandler);
	  };
	
	  i.event.bind(i.scrollbarY, 'mousedown', function (e) {
	    currentPageY = e.pageY;
	    currentTop = _.toInt(dom.css(i.scrollbarY, 'top')) * i.railYRatio;
	    _.startScrolling(element, 'y');
	
	    i.event.bind(i.ownerDocument, 'mousemove', mouseMoveHandler);
	    i.event.once(i.ownerDocument, 'mouseup', mouseUpHandler);
	
	    e.stopPropagation();
	    e.preventDefault();
	  });
	}
	
	module.exports = function (element) {
	  var i = instances.get(element);
	  bindMouseScrollXHandler(element, i);
	  bindMouseScrollYHandler(element, i);
	};


/***/ },
/* 19 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _ = __webpack_require__(7);
	var dom = __webpack_require__(9);
	var instances = __webpack_require__(10);
	var updateGeometry = __webpack_require__(15);
	var updateScroll = __webpack_require__(16);
	
	function bindKeyboardHandler(element, i) {
	  var hovered = false;
	  i.event.bind(element, 'mouseenter', function () {
	    hovered = true;
	  });
	  i.event.bind(element, 'mouseleave', function () {
	    hovered = false;
	  });
	
	  var shouldPrevent = false;
	  function shouldPreventDefault(deltaX, deltaY) {
	    var scrollTop = element.scrollTop;
	    if (deltaX === 0) {
	      if (!i.scrollbarYActive) {
	        return false;
	      }
	      if ((scrollTop === 0 && deltaY > 0) || (scrollTop >= i.contentHeight - i.containerHeight && deltaY < 0)) {
	        return !i.settings.wheelPropagation;
	      }
	    }
	
	    var scrollLeft = element.scrollLeft;
	    if (deltaY === 0) {
	      if (!i.scrollbarXActive) {
	        return false;
	      }
	      if ((scrollLeft === 0 && deltaX < 0) || (scrollLeft >= i.contentWidth - i.containerWidth && deltaX > 0)) {
	        return !i.settings.wheelPropagation;
	      }
	    }
	    return true;
	  }
	
	  i.event.bind(i.ownerDocument, 'keydown', function (e) {
	    if ((e.isDefaultPrevented && e.isDefaultPrevented()) || e.defaultPrevented) {
	      return;
	    }
	
	    var focused = dom.matches(i.scrollbarX, ':focus') ||
	                  dom.matches(i.scrollbarY, ':focus');
	
	    if (!hovered && !focused) {
	      return;
	    }
	
	    var activeElement = document.activeElement ? document.activeElement : i.ownerDocument.activeElement;
	    if (activeElement) {
	      if (activeElement.tagName === 'IFRAME') {
	        activeElement = activeElement.contentDocument.activeElement;
	      } else {
	        // go deeper if element is a webcomponent
	        while (activeElement.shadowRoot) {
	          activeElement = activeElement.shadowRoot.activeElement;
	        }
	      }
	      if (_.isEditable(activeElement)) {
	        return;
	      }
	    }
	
	    var deltaX = 0;
	    var deltaY = 0;
	
	    switch (e.which) {
	    case 37: // left
	      if (e.metaKey) {
	        deltaX = -i.contentWidth;
	      } else if (e.altKey) {
	        deltaX = -i.containerWidth;
	      } else {
	        deltaX = -30;
	      }
	      break;
	    case 38: // up
	      if (e.metaKey) {
	        deltaY = i.contentHeight;
	      } else if (e.altKey) {
	        deltaY = i.containerHeight;
	      } else {
	        deltaY = 30;
	      }
	      break;
	    case 39: // right
	      if (e.metaKey) {
	        deltaX = i.contentWidth;
	      } else if (e.altKey) {
	        deltaX = i.containerWidth;
	      } else {
	        deltaX = 30;
	      }
	      break;
	    case 40: // down
	      if (e.metaKey) {
	        deltaY = -i.contentHeight;
	      } else if (e.altKey) {
	        deltaY = -i.containerHeight;
	      } else {
	        deltaY = -30;
	      }
	      break;
	    case 33: // page up
	      deltaY = 90;
	      break;
	    case 32: // space bar
	      if (e.shiftKey) {
	        deltaY = 90;
	      } else {
	        deltaY = -90;
	      }
	      break;
	    case 34: // page down
	      deltaY = -90;
	      break;
	    case 35: // end
	      if (e.ctrlKey) {
	        deltaY = -i.contentHeight;
	      } else {
	        deltaY = -i.containerHeight;
	      }
	      break;
	    case 36: // home
	      if (e.ctrlKey) {
	        deltaY = element.scrollTop;
	      } else {
	        deltaY = i.containerHeight;
	      }
	      break;
	    default:
	      return;
	    }
	
	    updateScroll(element, 'top', element.scrollTop - deltaY);
	    updateScroll(element, 'left', element.scrollLeft + deltaX);
	    updateGeometry(element);
	
	    shouldPrevent = shouldPreventDefault(deltaX, deltaY);
	    if (shouldPrevent) {
	      e.preventDefault();
	    }
	  });
	}
	
	module.exports = function (element) {
	  var i = instances.get(element);
	  bindKeyboardHandler(element, i);
	};


/***/ },
/* 20 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var instances = __webpack_require__(10);
	var updateGeometry = __webpack_require__(15);
	var updateScroll = __webpack_require__(16);
	
	function bindMouseWheelHandler(element, i) {
	  var shouldPrevent = false;
	
	  function shouldPreventDefault(deltaX, deltaY) {
	    var scrollTop = element.scrollTop;
	    if (deltaX === 0) {
	      if (!i.scrollbarYActive) {
	        return false;
	      }
	      if ((scrollTop === 0 && deltaY > 0) || (scrollTop >= i.contentHeight - i.containerHeight && deltaY < 0)) {
	        return !i.settings.wheelPropagation;
	      }
	    }
	
	    var scrollLeft = element.scrollLeft;
	    if (deltaY === 0) {
	      if (!i.scrollbarXActive) {
	        return false;
	      }
	      if ((scrollLeft === 0 && deltaX < 0) || (scrollLeft >= i.contentWidth - i.containerWidth && deltaX > 0)) {
	        return !i.settings.wheelPropagation;
	      }
	    }
	    return true;
	  }
	
	  function getDeltaFromEvent(e) {
	    var deltaX = e.deltaX;
	    var deltaY = -1 * e.deltaY;
	
	    if (typeof deltaX === "undefined" || typeof deltaY === "undefined") {
	      // OS X Safari
	      deltaX = -1 * e.wheelDeltaX / 6;
	      deltaY = e.wheelDeltaY / 6;
	    }
	
	    if (e.deltaMode && e.deltaMode === 1) {
	      // Firefox in deltaMode 1: Line scrolling
	      deltaX *= 10;
	      deltaY *= 10;
	    }
	
	    if (deltaX !== deltaX && deltaY !== deltaY/* NaN checks */) {
	      // IE in some mouse drivers
	      deltaX = 0;
	      deltaY = e.wheelDelta;
	    }
	
	    if (e.shiftKey) {
	      // reverse axis with shift key
	      return [-deltaY, -deltaX];
	    }
	    return [deltaX, deltaY];
	  }
	
	  function shouldBeConsumedByChild(deltaX, deltaY) {
	    var child = element.querySelector('textarea:hover, select[multiple]:hover, .ps-child:hover');
	    if (child) {
	      if (!window.getComputedStyle(child).overflow.match(/(scroll|auto)/)) {
	        // if not scrollable
	        return false;
	      }
	
	      var maxScrollTop = child.scrollHeight - child.clientHeight;
	      if (maxScrollTop > 0) {
	        if (!(child.scrollTop === 0 && deltaY > 0) && !(child.scrollTop === maxScrollTop && deltaY < 0)) {
	          return true;
	        }
	      }
	      var maxScrollLeft = child.scrollLeft - child.clientWidth;
	      if (maxScrollLeft > 0) {
	        if (!(child.scrollLeft === 0 && deltaX < 0) && !(child.scrollLeft === maxScrollLeft && deltaX > 0)) {
	          return true;
	        }
	      }
	    }
	    return false;
	  }
	
	  function mousewheelHandler(e) {
	    var delta = getDeltaFromEvent(e);
	
	    var deltaX = delta[0];
	    var deltaY = delta[1];
	
	    if (shouldBeConsumedByChild(deltaX, deltaY)) {
	      return;
	    }
	
	    shouldPrevent = false;
	    if (!i.settings.useBothWheelAxes) {
	      // deltaX will only be used for horizontal scrolling and deltaY will
	      // only be used for vertical scrolling - this is the default
	      updateScroll(element, 'top', element.scrollTop - (deltaY * i.settings.wheelSpeed));
	      updateScroll(element, 'left', element.scrollLeft + (deltaX * i.settings.wheelSpeed));
	    } else if (i.scrollbarYActive && !i.scrollbarXActive) {
	      // only vertical scrollbar is active and useBothWheelAxes option is
	      // active, so let's scroll vertical bar using both mouse wheel axes
	      if (deltaY) {
	        updateScroll(element, 'top', element.scrollTop - (deltaY * i.settings.wheelSpeed));
	      } else {
	        updateScroll(element, 'top', element.scrollTop + (deltaX * i.settings.wheelSpeed));
	      }
	      shouldPrevent = true;
	    } else if (i.scrollbarXActive && !i.scrollbarYActive) {
	      // useBothWheelAxes and only horizontal bar is active, so use both
	      // wheel axes for horizontal bar
	      if (deltaX) {
	        updateScroll(element, 'left', element.scrollLeft + (deltaX * i.settings.wheelSpeed));
	      } else {
	        updateScroll(element, 'left', element.scrollLeft - (deltaY * i.settings.wheelSpeed));
	      }
	      shouldPrevent = true;
	    }
	
	    updateGeometry(element);
	
	    shouldPrevent = (shouldPrevent || shouldPreventDefault(deltaX, deltaY));
	    if (shouldPrevent) {
	      e.stopPropagation();
	      e.preventDefault();
	    }
	  }
	
	  if (typeof window.onwheel !== "undefined") {
	    i.event.bind(element, 'wheel', mousewheelHandler);
	  } else if (typeof window.onmousewheel !== "undefined") {
	    i.event.bind(element, 'mousewheel', mousewheelHandler);
	  }
	}
	
	module.exports = function (element) {
	  var i = instances.get(element);
	  bindMouseWheelHandler(element, i);
	};


/***/ },
/* 21 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _ = __webpack_require__(7);
	var instances = __webpack_require__(10);
	var updateGeometry = __webpack_require__(15);
	var updateScroll = __webpack_require__(16);
	
	function bindTouchHandler(element, i, supportsTouch, supportsIePointer) {
	  function shouldPreventDefault(deltaX, deltaY) {
	    var scrollTop = element.scrollTop;
	    var scrollLeft = element.scrollLeft;
	    var magnitudeX = Math.abs(deltaX);
	    var magnitudeY = Math.abs(deltaY);
	
	    if (magnitudeY > magnitudeX) {
	      // user is perhaps trying to swipe up/down the page
	
	      if (((deltaY < 0) && (scrollTop === i.contentHeight - i.containerHeight)) ||
	          ((deltaY > 0) && (scrollTop === 0))) {
	        return !i.settings.swipePropagation;
	      }
	    } else if (magnitudeX > magnitudeY) {
	      // user is perhaps trying to swipe left/right across the page
	
	      if (((deltaX < 0) && (scrollLeft === i.contentWidth - i.containerWidth)) ||
	          ((deltaX > 0) && (scrollLeft === 0))) {
	        return !i.settings.swipePropagation;
	      }
	    }
	
	    return true;
	  }
	
	  function applyTouchMove(differenceX, differenceY) {
	    updateScroll(element, 'top', element.scrollTop - differenceY);
	    updateScroll(element, 'left', element.scrollLeft - differenceX);
	
	    updateGeometry(element);
	  }
	
	  var startOffset = {};
	  var startTime = 0;
	  var speed = {};
	  var easingLoop = null;
	  var inGlobalTouch = false;
	  var inLocalTouch = false;
	
	  function globalTouchStart() {
	    inGlobalTouch = true;
	  }
	  function globalTouchEnd() {
	    inGlobalTouch = false;
	  }
	
	  function getTouch(e) {
	    if (e.targetTouches) {
	      return e.targetTouches[0];
	    } else {
	      // Maybe IE pointer
	      return e;
	    }
	  }
	  function shouldHandle(e) {
	    if (e.targetTouches && e.targetTouches.length === 1) {
	      return true;
	    }
	    if (e.pointerType && e.pointerType !== 'mouse' && e.pointerType !== e.MSPOINTER_TYPE_MOUSE) {
	      return true;
	    }
	    return false;
	  }
	  function touchStart(e) {
	    if (shouldHandle(e)) {
	      inLocalTouch = true;
	
	      var touch = getTouch(e);
	
	      startOffset.pageX = touch.pageX;
	      startOffset.pageY = touch.pageY;
	
	      startTime = (new Date()).getTime();
	
	      if (easingLoop !== null) {
	        clearInterval(easingLoop);
	      }
	
	      e.stopPropagation();
	    }
	  }
	  function touchMove(e) {
	    if (!inLocalTouch && i.settings.swipePropagation) {
	      touchStart(e);
	    }
	    if (!inGlobalTouch && inLocalTouch && shouldHandle(e)) {
	      var touch = getTouch(e);
	
	      var currentOffset = {pageX: touch.pageX, pageY: touch.pageY};
	
	      var differenceX = currentOffset.pageX - startOffset.pageX;
	      var differenceY = currentOffset.pageY - startOffset.pageY;
	
	      applyTouchMove(differenceX, differenceY);
	      startOffset = currentOffset;
	
	      var currentTime = (new Date()).getTime();
	
	      var timeGap = currentTime - startTime;
	      if (timeGap > 0) {
	        speed.x = differenceX / timeGap;
	        speed.y = differenceY / timeGap;
	        startTime = currentTime;
	      }
	
	      if (shouldPreventDefault(differenceX, differenceY)) {
	        e.stopPropagation();
	        e.preventDefault();
	      }
	    }
	  }
	  function touchEnd() {
	    if (!inGlobalTouch && inLocalTouch) {
	      inLocalTouch = false;
	
	      clearInterval(easingLoop);
	      easingLoop = setInterval(function () {
	        if (!instances.get(element)) {
	          clearInterval(easingLoop);
	          return;
	        }
	
	        if (!speed.x && !speed.y) {
	          clearInterval(easingLoop);
	          return;
	        }
	
	        if (Math.abs(speed.x) < 0.01 && Math.abs(speed.y) < 0.01) {
	          clearInterval(easingLoop);
	          return;
	        }
	
	        applyTouchMove(speed.x * 30, speed.y * 30);
	
	        speed.x *= 0.8;
	        speed.y *= 0.8;
	      }, 10);
	    }
	  }
	
	  if (supportsTouch) {
	    i.event.bind(window, 'touchstart', globalTouchStart);
	    i.event.bind(window, 'touchend', globalTouchEnd);
	    i.event.bind(element, 'touchstart', touchStart);
	    i.event.bind(element, 'touchmove', touchMove);
	    i.event.bind(element, 'touchend', touchEnd);
	  }
	
	  if (supportsIePointer) {
	    if (window.PointerEvent) {
	      i.event.bind(window, 'pointerdown', globalTouchStart);
	      i.event.bind(window, 'pointerup', globalTouchEnd);
	      i.event.bind(element, 'pointerdown', touchStart);
	      i.event.bind(element, 'pointermove', touchMove);
	      i.event.bind(element, 'pointerup', touchEnd);
	    } else if (window.MSPointerEvent) {
	      i.event.bind(window, 'MSPointerDown', globalTouchStart);
	      i.event.bind(window, 'MSPointerUp', globalTouchEnd);
	      i.event.bind(element, 'MSPointerDown', touchStart);
	      i.event.bind(element, 'MSPointerMove', touchMove);
	      i.event.bind(element, 'MSPointerUp', touchEnd);
	    }
	  }
	}
	
	module.exports = function (element) {
	  if (!_.env.supportsTouch && !_.env.supportsIePointer) {
	    return;
	  }
	
	  var i = instances.get(element);
	  bindTouchHandler(element, i, _.env.supportsTouch, _.env.supportsIePointer);
	};


/***/ },
/* 22 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _ = __webpack_require__(7);
	var instances = __webpack_require__(10);
	var updateGeometry = __webpack_require__(15);
	var updateScroll = __webpack_require__(16);
	
	function bindSelectionHandler(element, i) {
	  function getRangeNode() {
	    var selection = window.getSelection ? window.getSelection() :
	                    document.getSelection ? document.getSelection() : '';
	    if (selection.toString().length === 0) {
	      return null;
	    } else {
	      return selection.getRangeAt(0).commonAncestorContainer;
	    }
	  }
	
	  var scrollingLoop = null;
	  var scrollDiff = {top: 0, left: 0};
	  function startScrolling() {
	    if (!scrollingLoop) {
	      scrollingLoop = setInterval(function () {
	        if (!instances.get(element)) {
	          clearInterval(scrollingLoop);
	          return;
	        }
	
	        updateScroll(element, 'top', element.scrollTop + scrollDiff.top);
	        updateScroll(element, 'left', element.scrollLeft + scrollDiff.left);
	        updateGeometry(element);
	      }, 50); // every .1 sec
	    }
	  }
	  function stopScrolling() {
	    if (scrollingLoop) {
	      clearInterval(scrollingLoop);
	      scrollingLoop = null;
	    }
	    _.stopScrolling(element);
	  }
	
	  var isSelected = false;
	  i.event.bind(i.ownerDocument, 'selectionchange', function () {
	    if (element.contains(getRangeNode())) {
	      isSelected = true;
	    } else {
	      isSelected = false;
	      stopScrolling();
	    }
	  });
	  i.event.bind(window, 'mouseup', function () {
	    if (isSelected) {
	      isSelected = false;
	      stopScrolling();
	    }
	  });
	  i.event.bind(window, 'keyup', function () {
	    if (isSelected) {
	      isSelected = false;
	      stopScrolling();
	    }
	  });
	
	  i.event.bind(window, 'mousemove', function (e) {
	    if (isSelected) {
	      var mousePosition = {x: e.pageX, y: e.pageY};
	      var containerGeometry = {
	        left: element.offsetLeft,
	        right: element.offsetLeft + element.offsetWidth,
	        top: element.offsetTop,
	        bottom: element.offsetTop + element.offsetHeight
	      };
	
	      if (mousePosition.x < containerGeometry.left + 3) {
	        scrollDiff.left = -5;
	        _.startScrolling(element, 'x');
	      } else if (mousePosition.x > containerGeometry.right - 3) {
	        scrollDiff.left = 5;
	        _.startScrolling(element, 'x');
	      } else {
	        scrollDiff.left = 0;
	      }
	
	      if (mousePosition.y < containerGeometry.top + 3) {
	        if (containerGeometry.top + 3 - mousePosition.y < 5) {
	          scrollDiff.top = -5;
	        } else {
	          scrollDiff.top = -20;
	        }
	        _.startScrolling(element, 'y');
	      } else if (mousePosition.y > containerGeometry.bottom - 3) {
	        if (mousePosition.y - containerGeometry.bottom + 3 < 5) {
	          scrollDiff.top = 5;
	        } else {
	          scrollDiff.top = 20;
	        }
	        _.startScrolling(element, 'y');
	      } else {
	        scrollDiff.top = 0;
	      }
	
	      if (scrollDiff.top === 0 && scrollDiff.left === 0) {
	        stopScrolling();
	      } else {
	        startScrolling();
	      }
	    }
	  });
	}
	
	module.exports = function (element) {
	  var i = instances.get(element);
	  bindSelectionHandler(element, i);
	};


/***/ },
/* 23 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var instances = __webpack_require__(10);
	var updateGeometry = __webpack_require__(15);
	
	function bindNativeScrollHandler(element, i) {
	  i.event.bind(element, 'scroll', function () {
	    updateGeometry(element);
	  });
	}
	
	module.exports = function (element) {
	  var i = instances.get(element);
	  bindNativeScrollHandler(element, i);
	};


/***/ },
/* 24 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _ = __webpack_require__(7);
	var dom = __webpack_require__(9);
	var instances = __webpack_require__(10);
	var updateGeometry = __webpack_require__(15);
	var updateScroll = __webpack_require__(16);
	
	module.exports = function (element) {
	  var i = instances.get(element);
	
	  if (!i) {
	    return;
	  }
	
	  // Recalcuate negative scrollLeft adjustment
	  i.negativeScrollAdjustment = i.isNegativeScroll ? element.scrollWidth - element.clientWidth : 0;
	
	  // Recalculate rail margins
	  dom.css(i.scrollbarXRail, 'display', 'block');
	  dom.css(i.scrollbarYRail, 'display', 'block');
	  i.railXMarginWidth = _.toInt(dom.css(i.scrollbarXRail, 'marginLeft')) + _.toInt(dom.css(i.scrollbarXRail, 'marginRight'));
	  i.railYMarginHeight = _.toInt(dom.css(i.scrollbarYRail, 'marginTop')) + _.toInt(dom.css(i.scrollbarYRail, 'marginBottom'));
	
	  // Hide scrollbars not to affect scrollWidth and scrollHeight
	  dom.css(i.scrollbarXRail, 'display', 'none');
	  dom.css(i.scrollbarYRail, 'display', 'none');
	
	  updateGeometry(element);
	
	  // Update top/left scroll to trigger events
	  updateScroll(element, 'top', element.scrollTop);
	  updateScroll(element, 'left', element.scrollLeft);
	
	  dom.css(i.scrollbarXRail, 'display', '');
	  dom.css(i.scrollbarYRail, 'display', '');
	};


/***/ },
/* 25 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(jQuery) {!function(t,e,i){!function(){var s,a,n,h="2.2.0",r="datepicker",o=".datepicker-here",c=!1,d='<div class="datepicker"><i class="datepicker--pointer"></i><nav class="datepicker--nav"></nav><div class="datepicker--content"></div></div>',l={classes:"",inline:!1,language:"ru",startDate:new Date,firstDay:"",weekends:[6,0],dateFormat:"",altField:"",altFieldDateFormat:"@",toggleSelected:!0,keyboardNav:!0,position:"bottom left",offset:12,view:"days",minView:"days",showOtherMonths:!0,selectOtherMonths:!0,moveToOtherMonthsOnSelect:!0,showOtherYears:!0,selectOtherYears:!0,moveToOtherYearsOnSelect:!0,minDate:"",maxDate:"",disableNavWhenOutOfRange:!0,multipleDates:!1,multipleDatesSeparator:",",range:!1,todayButton:!1,clearButton:!1,showEvent:"focus",autoClose:!1,monthsField:"monthsShort",prevHtml:'<svg><path d="M 17,12 l -5,5 l 5,5"></path></svg>',nextHtml:'<svg><path d="M 14,12 l 5,5 l -5,5"></path></svg>',navTitles:{days:"MM, <i>yyyy</i>",months:"yyyy",years:"yyyy1 - yyyy2"},timepicker:!1,onlyTimepicker:!1,dateTimeSeparator:" ",timeFormat:"",minHours:0,maxHours:24,minMinutes:0,maxMinutes:59,hoursStep:1,minutesStep:1,onSelect:"",onShow:"",onHide:"",onChangeMonth:"",onChangeYear:"",onChangeDecade:"",onChangeView:"",onRenderCell:""},u={ctrlRight:[17,39],ctrlUp:[17,38],ctrlLeft:[17,37],ctrlDown:[17,40],shiftRight:[16,39],shiftUp:[16,38],shiftLeft:[16,37],shiftDown:[16,40],altUp:[18,38],altRight:[18,39],altLeft:[18,37],altDown:[18,40],ctrlShiftUp:[16,17,38]},m=function(t,a){this.el=t,this.$el=e(t),this.opts=e.extend(!0,{},l,a,this.$el.data()),s==i&&(s=e("body")),this.opts.startDate||(this.opts.startDate=new Date),"INPUT"==this.el.nodeName&&(this.elIsInput=!0),this.opts.altField&&(this.$altField="string"==typeof this.opts.altField?e(this.opts.altField):this.opts.altField),this.inited=!1,this.visible=!1,this.silent=!1,this.currentDate=this.opts.startDate,this.currentView=this.opts.view,this._createShortCuts(),this.selectedDates=[],this.views={},this.keys=[],this.minRange="",this.maxRange="",this._prevOnSelectValue="",this.init()};n=m,n.prototype={VERSION:h,viewIndexes:["days","months","years"],init:function(){c||this.opts.inline||!this.elIsInput||this._buildDatepickersContainer(),this._buildBaseHtml(),this._defineLocale(this.opts.language),this._syncWithMinMaxDates(),this.elIsInput&&(this.opts.inline||(this._setPositionClasses(this.opts.position),this._bindEvents()),this.opts.keyboardNav&&!this.opts.onlyTimepicker&&this._bindKeyboardEvents(),this.$datepicker.on("mousedown",this._onMouseDownDatepicker.bind(this)),this.$datepicker.on("mouseup",this._onMouseUpDatepicker.bind(this))),this.opts.classes&&this.$datepicker.addClass(this.opts.classes),this.opts.timepicker&&(this.timepicker=new e.fn.datepicker.Timepicker(this,this.opts),this._bindTimepickerEvents()),this.opts.onlyTimepicker&&this.$datepicker.addClass("-only-timepicker-"),this.views[this.currentView]=new e.fn.datepicker.Body(this,this.currentView,this.opts),this.views[this.currentView].show(),this.nav=new e.fn.datepicker.Navigation(this,this.opts),this.view=this.currentView,this.$el.on("clickCell.adp",this._onClickCell.bind(this)),this.$datepicker.on("mouseenter",".datepicker--cell",this._onMouseEnterCell.bind(this)),this.$datepicker.on("mouseleave",".datepicker--cell",this._onMouseLeaveCell.bind(this)),this.inited=!0},_createShortCuts:function(){this.minDate=this.opts.minDate?this.opts.minDate:new Date(-86399999136e5),this.maxDate=this.opts.maxDate?this.opts.maxDate:new Date(86399999136e5)},_bindEvents:function(){this.$el.on(this.opts.showEvent+".adp",this._onShowEvent.bind(this)),this.$el.on("mouseup.adp",this._onMouseUpEl.bind(this)),this.$el.on("blur.adp",this._onBlur.bind(this)),this.$el.on("keyup.adp",this._onKeyUpGeneral.bind(this)),e(t).on("resize.adp",this._onResize.bind(this)),e("body").on("mouseup.adp",this._onMouseUpBody.bind(this))},_bindKeyboardEvents:function(){this.$el.on("keydown.adp",this._onKeyDown.bind(this)),this.$el.on("keyup.adp",this._onKeyUp.bind(this)),this.$el.on("hotKey.adp",this._onHotKey.bind(this))},_bindTimepickerEvents:function(){this.$el.on("timeChange.adp",this._onTimeChange.bind(this))},isWeekend:function(t){return-1!==this.opts.weekends.indexOf(t)},_defineLocale:function(t){"string"==typeof t?(this.loc=e.fn.datepicker.language[t],this.loc||(console.warn("Can't find language \""+t+'" in Datepicker.language, will use "ru" instead'),this.loc=e.extend(!0,{},e.fn.datepicker.language.ru)),this.loc=e.extend(!0,{},e.fn.datepicker.language.ru,e.fn.datepicker.language[t])):this.loc=e.extend(!0,{},e.fn.datepicker.language.ru,t),this.opts.dateFormat&&(this.loc.dateFormat=this.opts.dateFormat),this.opts.timeFormat&&(this.loc.timeFormat=this.opts.timeFormat),""!==this.opts.firstDay&&(this.loc.firstDay=this.opts.firstDay),this.opts.timepicker&&(this.loc.dateFormat=[this.loc.dateFormat,this.loc.timeFormat].join(this.opts.dateTimeSeparator)),this.opts.onlyTimepicker&&(this.loc.dateFormat=this.loc.timeFormat);var i=this._getWordBoundaryRegExp;(this.loc.timeFormat.match(i("aa"))||this.loc.timeFormat.match(i("AA")))&&(this.ampm=!0)},_buildDatepickersContainer:function(){c=!0,s.append('<div class="datepickers-container" id="datepickers-container"></div>'),a=e("#datepickers-container")},_buildBaseHtml:function(){var t,i=e('<div class="datepicker-inline">');t="INPUT"==this.el.nodeName?this.opts.inline?i.insertAfter(this.$el):a:i.appendTo(this.$el),this.$datepicker=e(d).appendTo(t),this.$content=e(".datepicker--content",this.$datepicker),this.$nav=e(".datepicker--nav",this.$datepicker)},_triggerOnChange:function(){if(!this.selectedDates.length){if(""===this._prevOnSelectValue)return;return this._prevOnSelectValue="",this.opts.onSelect("","",this)}var t,e=this.selectedDates,i=n.getParsedDate(e[0]),s=this,a=new Date(i.year,i.month,i.date,i.hours,i.minutes);t=e.map(function(t){return s.formatDate(s.loc.dateFormat,t)}).join(this.opts.multipleDatesSeparator),(this.opts.multipleDates||this.opts.range)&&(a=e.map(function(t){var e=n.getParsedDate(t);return new Date(e.year,e.month,e.date,e.hours,e.minutes)})),this._prevOnSelectValue=t,this.opts.onSelect(t,a,this)},next:function(){var t=this.parsedDate,e=this.opts;switch(this.view){case"days":this.date=new Date(t.year,t.month+1,1),e.onChangeMonth&&e.onChangeMonth(this.parsedDate.month,this.parsedDate.year);break;case"months":this.date=new Date(t.year+1,t.month,1),e.onChangeYear&&e.onChangeYear(this.parsedDate.year);break;case"years":this.date=new Date(t.year+10,0,1),e.onChangeDecade&&e.onChangeDecade(this.curDecade)}},prev:function(){var t=this.parsedDate,e=this.opts;switch(this.view){case"days":this.date=new Date(t.year,t.month-1,1),e.onChangeMonth&&e.onChangeMonth(this.parsedDate.month,this.parsedDate.year);break;case"months":this.date=new Date(t.year-1,t.month,1),e.onChangeYear&&e.onChangeYear(this.parsedDate.year);break;case"years":this.date=new Date(t.year-10,0,1),e.onChangeDecade&&e.onChangeDecade(this.curDecade)}},formatDate:function(t,e){e=e||this.date;var i,s=t,a=this._getWordBoundaryRegExp,h=this.loc,r=n.getLeadingZeroNum,o=n.getDecade(e),c=n.getParsedDate(e),d=c.fullHours,l=c.hours,u=t.match(a("aa"))||t.match(a("AA")),m="am";switch(this.opts.timepicker&&this.timepicker&&u&&(i=this.timepicker._getValidHoursFromDate(e,u),d=r(i.hours),l=i.hours,m=i.dayPeriod),!0){case/@/.test(s):s=s.replace(/@/,e.getTime());case/aa/.test(s):s=s.replace(a("aa"),m);case/AA/.test(s):s=s.replace(a("AA"),m.toUpperCase());case/dd/.test(s):s=s.replace(a("dd"),c.fullDate);case/d/.test(s):s=s.replace(a("d"),c.date);case/DD/.test(s):s=s.replace(a("DD"),h.days[c.day]);case/D/.test(s):s=s.replace(a("D"),h.daysShort[c.day]);case/mm/.test(s):s=s.replace(a("mm"),c.fullMonth);case/m/.test(s):s=s.replace(a("m"),c.month+1);case/MM/.test(s):s=s.replace(a("MM"),this.loc.months[c.month]);case/M/.test(s):s=s.replace(a("M"),h.monthsShort[c.month]);case/ii/.test(s):s=s.replace(a("ii"),c.fullMinutes);case/i/.test(s):s=s.replace(a("i"),c.minutes);case/hh/.test(s):s=s.replace(a("hh"),d);case/h/.test(s):s=s.replace(a("h"),l);case/yyyy/.test(s):s=s.replace(a("yyyy"),c.year);case/yyyy1/.test(s):s=s.replace(a("yyyy1"),o[0]);case/yyyy2/.test(s):s=s.replace(a("yyyy2"),o[1]);case/yy/.test(s):s=s.replace(a("yy"),c.year.toString().slice(-2))}return s},_getWordBoundaryRegExp:function(t){return new RegExp("\\b(?=[a-zA-Z0-9áäöüúÁßÉÄÖÜÚ<])"+t+"(?![>a-zA-Z0-9áäöüÁßÉÄÖÜÚ])")},selectDate:function(t){var e=this,i=e.opts,s=e.parsedDate,a=e.selectedDates,h=a.length,r="";if(Array.isArray(t))return void t.forEach(function(t){e.selectDate(t)});if(t instanceof Date){if(this.lastSelectedDate=t,this.timepicker&&this.timepicker._setTime(t),e._trigger("selectDate",t),this.timepicker&&(t.setHours(this.timepicker.hours),t.setMinutes(this.timepicker.minutes)),"days"==e.view&&t.getMonth()!=s.month&&i.moveToOtherMonthsOnSelect&&(r=new Date(t.getFullYear(),t.getMonth(),1)),"years"==e.view&&t.getFullYear()!=s.year&&i.moveToOtherYearsOnSelect&&(r=new Date(t.getFullYear(),0,1)),r&&(e.silent=!0,e.date=r,e.silent=!1,e.nav._render()),i.multipleDates&&!i.range){if(h===i.multipleDates)return;e._isSelected(t)||e.selectedDates.push(t)}else i.range?2==h?(e.selectedDates=[t],e.minRange=t,e.maxRange=""):1==h?(e.selectedDates.push(t),e.maxRange?e.minRange=t:e.maxRange=t,n.bigger(e.maxRange,e.minRange)&&(e.maxRange=e.minRange,e.minRange=t),e.selectedDates=[e.minRange,e.maxRange]):(e.selectedDates=[t],e.minRange=t):e.selectedDates=[t];e._setInputValue(),i.onSelect&&e._triggerOnChange(),i.autoClose&&!this.timepickerIsActive&&(i.multipleDates||i.range?i.range&&2==e.selectedDates.length&&e.hide():e.hide()),e.views[this.currentView]._render()}},removeDate:function(t){var e=this.selectedDates,i=this;if(t instanceof Date)return e.some(function(s,a){return n.isSame(s,t)?(e.splice(a,1),i.selectedDates.length?i.lastSelectedDate=i.selectedDates[i.selectedDates.length-1]:(i.minRange="",i.maxRange="",i.lastSelectedDate=""),i.views[i.currentView]._render(),i._setInputValue(),i.opts.onSelect&&i._triggerOnChange(),!0):void 0})},today:function(){this.silent=!0,this.view=this.opts.minView,this.silent=!1,this.date=new Date,this.opts.todayButton instanceof Date&&this.selectDate(this.opts.todayButton)},clear:function(){this.selectedDates=[],this.minRange="",this.maxRange="",this.views[this.currentView]._render(),this._setInputValue(),this.opts.onSelect&&this._triggerOnChange()},update:function(t,i){var s=arguments.length,a=this.lastSelectedDate;return 2==s?this.opts[t]=i:1==s&&"object"==typeof t&&(this.opts=e.extend(!0,this.opts,t)),this._createShortCuts(),this._syncWithMinMaxDates(),this._defineLocale(this.opts.language),this.nav._addButtonsIfNeed(),this.opts.onlyTimepicker||this.nav._render(),this.views[this.currentView]._render(),this.elIsInput&&!this.opts.inline&&(this._setPositionClasses(this.opts.position),this.visible&&this.setPosition(this.opts.position)),this.opts.classes&&this.$datepicker.addClass(this.opts.classes),this.opts.onlyTimepicker&&this.$datepicker.addClass("-only-timepicker-"),this.opts.timepicker&&(a&&this.timepicker._handleDate(a),this.timepicker._updateRanges(),this.timepicker._updateCurrentTime(),a&&(a.setHours(this.timepicker.hours),a.setMinutes(this.timepicker.minutes))),this._setInputValue(),this},_syncWithMinMaxDates:function(){var t=this.date.getTime();this.silent=!0,this.minTime>t&&(this.date=this.minDate),this.maxTime<t&&(this.date=this.maxDate),this.silent=!1},_isSelected:function(t,e){var i=!1;return this.selectedDates.some(function(s){return n.isSame(s,t,e)?(i=s,!0):void 0}),i},_setInputValue:function(){var t,e=this,i=e.opts,s=e.loc.dateFormat,a=i.altFieldDateFormat,n=e.selectedDates.map(function(t){return e.formatDate(s,t)});i.altField&&e.$altField.length&&(t=this.selectedDates.map(function(t){return e.formatDate(a,t)}),t=t.join(this.opts.multipleDatesSeparator),this.$altField.val(t)),n=n.join(this.opts.multipleDatesSeparator),this.$el.val(n)},_isInRange:function(t,e){var i=t.getTime(),s=n.getParsedDate(t),a=n.getParsedDate(this.minDate),h=n.getParsedDate(this.maxDate),r=new Date(s.year,s.month,a.date).getTime(),o=new Date(s.year,s.month,h.date).getTime(),c={day:i>=this.minTime&&i<=this.maxTime,month:r>=this.minTime&&o<=this.maxTime,year:s.year>=a.year&&s.year<=h.year};return e?c[e]:c.day},_getDimensions:function(t){var e=t.offset();return{width:t.outerWidth(),height:t.outerHeight(),left:e.left,top:e.top}},_getDateFromCell:function(t){var e=this.parsedDate,s=t.data("year")||e.year,a=t.data("month")==i?e.month:t.data("month"),n=t.data("date")||1;return new Date(s,a,n)},_setPositionClasses:function(t){t=t.split(" ");var e=t[0],i=t[1],s="datepicker -"+e+"-"+i+"- -from-"+e+"-";this.visible&&(s+=" active"),this.$datepicker.removeAttr("class").addClass(s)},setPosition:function(t){t=t||this.opts.position;var e,i,s=this._getDimensions(this.$el),a=this._getDimensions(this.$datepicker),n=t.split(" "),h=this.opts.offset,r=n[0],o=n[1];switch(r){case"top":e=s.top-a.height-h;break;case"right":i=s.left+s.width+h;break;case"bottom":e=s.top+s.height+h;break;case"left":i=s.left-a.width-h}switch(o){case"top":e=s.top;break;case"right":i=s.left+s.width-a.width;break;case"bottom":e=s.top+s.height-a.height;break;case"left":i=s.left;break;case"center":/left|right/.test(r)?e=s.top+s.height/2-a.height/2:i=s.left+s.width/2-a.width/2}this.$datepicker.css({left:i,top:e})},show:function(){var t=this.opts.onShow;this.setPosition(this.opts.position),this.$datepicker.addClass("active"),this.visible=!0,t&&this._bindVisionEvents(t)},hide:function(){var t=this.opts.onHide;this.$datepicker.removeClass("active").css({left:"-100000px"}),this.focused="",this.keys=[],this.inFocus=!1,this.visible=!1,this.$el.blur(),t&&this._bindVisionEvents(t)},down:function(t){this._changeView(t,"down")},up:function(t){this._changeView(t,"up")},_bindVisionEvents:function(t){this.$datepicker.off("transitionend.dp"),t(this,!1),this.$datepicker.one("transitionend.dp",t.bind(this,this,!0))},_changeView:function(t,e){t=t||this.focused||this.date;var i="up"==e?this.viewIndex+1:this.viewIndex-1;i>2&&(i=2),0>i&&(i=0),this.silent=!0,this.date=new Date(t.getFullYear(),t.getMonth(),1),this.silent=!1,this.view=this.viewIndexes[i]},_handleHotKey:function(t){var e,i,s,a=n.getParsedDate(this._getFocusedDate()),h=this.opts,r=!1,o=!1,c=!1,d=a.year,l=a.month,u=a.date;switch(t){case"ctrlRight":case"ctrlUp":l+=1,r=!0;break;case"ctrlLeft":case"ctrlDown":l-=1,r=!0;break;case"shiftRight":case"shiftUp":o=!0,d+=1;break;case"shiftLeft":case"shiftDown":o=!0,d-=1;break;case"altRight":case"altUp":c=!0,d+=10;break;case"altLeft":case"altDown":c=!0,d-=10;break;case"ctrlShiftUp":this.up()}s=n.getDaysCount(new Date(d,l)),i=new Date(d,l,u),u>s&&(u=s),i.getTime()<this.minTime?i=this.minDate:i.getTime()>this.maxTime&&(i=this.maxDate),this.focused=i,e=n.getParsedDate(i),r&&h.onChangeMonth&&h.onChangeMonth(e.month,e.year),o&&h.onChangeYear&&h.onChangeYear(e.year),c&&h.onChangeDecade&&h.onChangeDecade(this.curDecade)},_registerKey:function(t){var e=this.keys.some(function(e){return e==t});e||this.keys.push(t)},_unRegisterKey:function(t){var e=this.keys.indexOf(t);this.keys.splice(e,1)},_isHotKeyPressed:function(){var t,e=!1,i=this,s=this.keys.sort();for(var a in u)t=u[a],s.length==t.length&&t.every(function(t,e){return t==s[e]})&&(i._trigger("hotKey",a),e=!0);return e},_trigger:function(t,e){this.$el.trigger(t,e)},_focusNextCell:function(t,e){e=e||this.cellType;var i=n.getParsedDate(this._getFocusedDate()),s=i.year,a=i.month,h=i.date;if(!this._isHotKeyPressed()){switch(t){case 37:"day"==e?h-=1:"","month"==e?a-=1:"","year"==e?s-=1:"";break;case 38:"day"==e?h-=7:"","month"==e?a-=3:"","year"==e?s-=4:"";break;case 39:"day"==e?h+=1:"","month"==e?a+=1:"","year"==e?s+=1:"";break;case 40:"day"==e?h+=7:"","month"==e?a+=3:"","year"==e?s+=4:""}var r=new Date(s,a,h);r.getTime()<this.minTime?r=this.minDate:r.getTime()>this.maxTime&&(r=this.maxDate),this.focused=r}},_getFocusedDate:function(){var t=this.focused||this.selectedDates[this.selectedDates.length-1],e=this.parsedDate;if(!t)switch(this.view){case"days":t=new Date(e.year,e.month,(new Date).getDate());break;case"months":t=new Date(e.year,e.month,1);break;case"years":t=new Date(e.year,0,1)}return t},_getCell:function(t,i){i=i||this.cellType;var s,a=n.getParsedDate(t),h='.datepicker--cell[data-year="'+a.year+'"]';switch(i){case"month":h='[data-month="'+a.month+'"]';break;case"day":h+='[data-month="'+a.month+'"][data-date="'+a.date+'"]'}return s=this.views[this.currentView].$el.find(h),s.length?s:e("")},destroy:function(){var t=this;t.$el.off(".adp").data("datepicker",""),t.selectedDates=[],t.focused="",t.views={},t.keys=[],t.minRange="",t.maxRange="",t.opts.inline||!t.elIsInput?t.$datepicker.closest(".datepicker-inline").remove():t.$datepicker.remove()},_handleAlreadySelectedDates:function(t,e){this.opts.range?this.opts.toggleSelected?this.removeDate(e):2!=this.selectedDates.length&&this._trigger("clickCell",e):this.opts.toggleSelected&&this.removeDate(e),this.opts.toggleSelected||(this.lastSelectedDate=t,this.opts.timepicker&&(this.timepicker._setTime(t),this.timepicker.update()))},_onShowEvent:function(t){this.visible||this.show()},_onBlur:function(){!this.inFocus&&this.visible&&this.hide()},_onMouseDownDatepicker:function(t){this.inFocus=!0},_onMouseUpDatepicker:function(t){this.inFocus=!1,t.originalEvent.inFocus=!0,t.originalEvent.timepickerFocus||this.$el.focus()},_onKeyUpGeneral:function(t){var e=this.$el.val();e||this.clear()},_onResize:function(){this.visible&&this.setPosition()},_onMouseUpBody:function(t){t.originalEvent.inFocus||this.visible&&!this.inFocus&&this.hide()},_onMouseUpEl:function(t){t.originalEvent.inFocus=!0,setTimeout(this._onKeyUpGeneral.bind(this),4)},_onKeyDown:function(t){var e=t.which;if(this._registerKey(e),e>=37&&40>=e&&(t.preventDefault(),this._focusNextCell(e)),13==e&&this.focused){if(this._getCell(this.focused).hasClass("-disabled-"))return;if(this.view!=this.opts.minView)this.down();else{var i=this._isSelected(this.focused,this.cellType);if(!i)return this.timepicker&&(this.focused.setHours(this.timepicker.hours),this.focused.setMinutes(this.timepicker.minutes)),void this.selectDate(this.focused);this._handleAlreadySelectedDates(i,this.focused)}}27==e&&this.hide()},_onKeyUp:function(t){var e=t.which;this._unRegisterKey(e)},_onHotKey:function(t,e){this._handleHotKey(e)},_onMouseEnterCell:function(t){var i=e(t.target).closest(".datepicker--cell"),s=this._getDateFromCell(i);this.silent=!0,this.focused&&(this.focused=""),i.addClass("-focus-"),this.focused=s,this.silent=!1,this.opts.range&&1==this.selectedDates.length&&(this.minRange=this.selectedDates[0],this.maxRange="",n.less(this.minRange,this.focused)&&(this.maxRange=this.minRange,this.minRange=""),this.views[this.currentView]._update())},_onMouseLeaveCell:function(t){var i=e(t.target).closest(".datepicker--cell");i.removeClass("-focus-"),this.silent=!0,this.focused="",this.silent=!1},_onTimeChange:function(t,e,i){var s=new Date,a=this.selectedDates,n=!1;a.length&&(n=!0,s=this.lastSelectedDate),s.setHours(e),s.setMinutes(i),n||this._getCell(s).hasClass("-disabled-")?(this._setInputValue(),this.opts.onSelect&&this._triggerOnChange()):this.selectDate(s)},_onClickCell:function(t,e){this.timepicker&&(e.setHours(this.timepicker.hours),e.setMinutes(this.timepicker.minutes)),this.selectDate(e)},set focused(t){if(!t&&this.focused){var e=this._getCell(this.focused);e.length&&e.removeClass("-focus-")}this._focused=t,this.opts.range&&1==this.selectedDates.length&&(this.minRange=this.selectedDates[0],this.maxRange="",n.less(this.minRange,this._focused)&&(this.maxRange=this.minRange,this.minRange="")),this.silent||(this.date=t)},get focused(){return this._focused},get parsedDate(){return n.getParsedDate(this.date)},set date(t){return t instanceof Date?(this.currentDate=t,this.inited&&!this.silent&&(this.views[this.view]._render(),this.nav._render(),this.visible&&this.elIsInput&&this.setPosition()),t):void 0},get date(){return this.currentDate},set view(t){return this.viewIndex=this.viewIndexes.indexOf(t),this.viewIndex<0?void 0:(this.prevView=this.currentView,this.currentView=t,this.inited&&(this.views[t]?this.views[t]._render():this.views[t]=new e.fn.datepicker.Body(this,t,this.opts),this.views[this.prevView].hide(),this.views[t].show(),this.nav._render(),this.opts.onChangeView&&this.opts.onChangeView(t),this.elIsInput&&this.visible&&this.setPosition()),t)},get view(){return this.currentView},get cellType(){return this.view.substring(0,this.view.length-1)},get minTime(){var t=n.getParsedDate(this.minDate);return new Date(t.year,t.month,t.date).getTime()},get maxTime(){var t=n.getParsedDate(this.maxDate);return new Date(t.year,t.month,t.date).getTime()},get curDecade(){return n.getDecade(this.date)}},n.getDaysCount=function(t){return new Date(t.getFullYear(),t.getMonth()+1,0).getDate()},n.getParsedDate=function(t){return{year:t.getFullYear(),month:t.getMonth(),fullMonth:t.getMonth()+1<10?"0"+(t.getMonth()+1):t.getMonth()+1,date:t.getDate(),fullDate:t.getDate()<10?"0"+t.getDate():t.getDate(),day:t.getDay(),hours:t.getHours(),fullHours:t.getHours()<10?"0"+t.getHours():t.getHours(),minutes:t.getMinutes(),fullMinutes:t.getMinutes()<10?"0"+t.getMinutes():t.getMinutes()}},n.getDecade=function(t){var e=10*Math.floor(t.getFullYear()/10);return[e,e+9]},n.template=function(t,e){return t.replace(/#\{([\w]+)\}/g,function(t,i){return e[i]||0===e[i]?e[i]:void 0})},n.isSame=function(t,e,i){if(!t||!e)return!1;var s=n.getParsedDate(t),a=n.getParsedDate(e),h=i?i:"day",r={day:s.date==a.date&&s.month==a.month&&s.year==a.year,month:s.month==a.month&&s.year==a.year,year:s.year==a.year};return r[h]},n.less=function(t,e,i){return t&&e?e.getTime()<t.getTime():!1},n.bigger=function(t,e,i){return t&&e?e.getTime()>t.getTime():!1},n.getLeadingZeroNum=function(t){return parseInt(t)<10?"0"+t:t},n.resetTime=function(t){return"object"==typeof t?(t=n.getParsedDate(t),new Date(t.year,t.month,t.date)):void 0},e.fn.datepicker=function(t){return this.each(function(){if(e.data(this,r)){var i=e.data(this,r);i.opts=e.extend(!0,i.opts,t),i.update()}else e.data(this,r,new m(this,t))})},e.fn.datepicker.Constructor=m,e.fn.datepicker.language={ru:{days:["Воскресенье","Понедельник","Вторник","Среда","Четверг","Пятница","Суббота"],daysShort:["Вос","Пон","Вто","Сре","Чет","Пят","Суб"],daysMin:["Вс","Пн","Вт","Ср","Чт","Пт","Сб"],months:["Январь","Февраль","Март","Апрель","Май","Июнь","Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь"],monthsShort:["Янв","Фев","Мар","Апр","Май","Июн","Июл","Авг","Сен","Окт","Ноя","Дек"],today:"Сегодня",clear:"Очистить",dateFormat:"dd.mm.yyyy",timeFormat:"hh:ii",firstDay:1}},e(function(){e(o).datepicker()})}(),function(){var t={days:'<div class="datepicker--days datepicker--body"><div class="datepicker--days-names"></div><div class="datepicker--cells datepicker--cells-days"></div></div>',months:'<div class="datepicker--months datepicker--body"><div class="datepicker--cells datepicker--cells-months"></div></div>',years:'<div class="datepicker--years datepicker--body"><div class="datepicker--cells datepicker--cells-years"></div></div>'},s=e.fn.datepicker,a=s.Constructor;s.Body=function(t,i,s){this.d=t,this.type=i,this.opts=s,this.$el=e(""),this.opts.onlyTimepicker||this.init()},s.Body.prototype={init:function(){this._buildBaseHtml(),this._render(),this._bindEvents()},_bindEvents:function(){this.$el.on("click",".datepicker--cell",e.proxy(this._onClickCell,this))},_buildBaseHtml:function(){this.$el=e(t[this.type]).appendTo(this.d.$content),this.$names=e(".datepicker--days-names",this.$el),this.$cells=e(".datepicker--cells",this.$el)},_getDayNamesHtml:function(t,e,s,a){return e=e!=i?e:t,s=s?s:"",a=a!=i?a:0,a>7?s:7==e?this._getDayNamesHtml(t,0,s,++a):(s+='<div class="datepicker--day-name'+(this.d.isWeekend(e)?" -weekend-":"")+'">'+this.d.loc.daysMin[e]+"</div>",this._getDayNamesHtml(t,++e,s,++a))},_getCellContents:function(t,e){var i="datepicker--cell datepicker--cell-"+e,s=new Date,n=this.d,h=a.resetTime(n.minRange),r=a.resetTime(n.maxRange),o=n.opts,c=a.getParsedDate(t),d={},l=c.date;switch(e){case"day":n.isWeekend(c.day)&&(i+=" -weekend-"),c.month!=this.d.parsedDate.month&&(i+=" -other-month-",o.selectOtherMonths||(i+=" -disabled-"),o.showOtherMonths||(l=""));break;case"month":l=n.loc[n.opts.monthsField][c.month];break;case"year":var u=n.curDecade;l=c.year,(c.year<u[0]||c.year>u[1])&&(i+=" -other-decade-",o.selectOtherYears||(i+=" -disabled-"),o.showOtherYears||(l=""))}return o.onRenderCell&&(d=o.onRenderCell(t,e)||{},l=d.html?d.html:l,i+=d.classes?" "+d.classes:""),o.range&&(a.isSame(h,t,e)&&(i+=" -range-from-"),a.isSame(r,t,e)&&(i+=" -range-to-"),1==n.selectedDates.length&&n.focused?((a.bigger(h,t)&&a.less(n.focused,t)||a.less(r,t)&&a.bigger(n.focused,t))&&(i+=" -in-range-"),a.less(r,t)&&a.isSame(n.focused,t)&&(i+=" -range-from-"),a.bigger(h,t)&&a.isSame(n.focused,t)&&(i+=" -range-to-")):2==n.selectedDates.length&&a.bigger(h,t)&&a.less(r,t)&&(i+=" -in-range-")),a.isSame(s,t,e)&&(i+=" -current-"),n.focused&&a.isSame(t,n.focused,e)&&(i+=" -focus-"),n._isSelected(t,e)&&(i+=" -selected-"),(!n._isInRange(t,e)||d.disabled)&&(i+=" -disabled-"),{html:l,classes:i}},_getDaysHtml:function(t){var e=a.getDaysCount(t),i=new Date(t.getFullYear(),t.getMonth(),1).getDay(),s=new Date(t.getFullYear(),t.getMonth(),e).getDay(),n=i-this.d.loc.firstDay,h=6-s+this.d.loc.firstDay;n=0>n?n+7:n,h=h>6?h-7:h;for(var r,o,c=-n+1,d="",l=c,u=e+h;u>=l;l++)o=t.getFullYear(),r=t.getMonth(),d+=this._getDayHtml(new Date(o,r,l));return d},_getDayHtml:function(t){var e=this._getCellContents(t,"day");return'<div class="'+e.classes+'" data-date="'+t.getDate()+'" data-month="'+t.getMonth()+'" data-year="'+t.getFullYear()+'">'+e.html+"</div>"},_getMonthsHtml:function(t){for(var e="",i=a.getParsedDate(t),s=0;12>s;)e+=this._getMonthHtml(new Date(i.year,s)),s++;return e},_getMonthHtml:function(t){var e=this._getCellContents(t,"month");return'<div class="'+e.classes+'" data-month="'+t.getMonth()+'">'+e.html+"</div>"},_getYearsHtml:function(t){var e=(a.getParsedDate(t),a.getDecade(t)),i=e[0]-1,s="",n=i;for(n;n<=e[1]+1;n++)s+=this._getYearHtml(new Date(n,0));return s},_getYearHtml:function(t){var e=this._getCellContents(t,"year");return'<div class="'+e.classes+'" data-year="'+t.getFullYear()+'">'+e.html+"</div>"},_renderTypes:{days:function(){var t=this._getDayNamesHtml(this.d.loc.firstDay),e=this._getDaysHtml(this.d.currentDate);this.$cells.html(e),this.$names.html(t)},months:function(){var t=this._getMonthsHtml(this.d.currentDate);this.$cells.html(t)},years:function(){var t=this._getYearsHtml(this.d.currentDate);this.$cells.html(t)}},_render:function(){this.opts.onlyTimepicker||this._renderTypes[this.type].bind(this)()},_update:function(){var t,i,s,a=e(".datepicker--cell",this.$cells),n=this;a.each(function(a,h){i=e(this),s=n.d._getDateFromCell(e(this)),t=n._getCellContents(s,n.d.cellType),i.attr("class",t.classes)})},show:function(){this.opts.onlyTimepicker||(this.$el.addClass("active"),this.acitve=!0)},hide:function(){this.$el.removeClass("active"),this.active=!1},_handleClick:function(t){var e=t.data("date")||1,i=t.data("month")||0,s=t.data("year")||this.d.parsedDate.year,a=this.d;if(a.view!=this.opts.minView)return void a.down(new Date(s,i,e));var n=new Date(s,i,e),h=this.d._isSelected(n,this.d.cellType);return h?void a._handleAlreadySelectedDates.bind(a,h,n)():void a._trigger("clickCell",n)},_onClickCell:function(t){var i=e(t.target).closest(".datepicker--cell");i.hasClass("-disabled-")||this._handleClick.bind(this)(i)}}}(),function(){var t='<div class="datepicker--nav-action" data-action="prev">#{prevHtml}</div><div class="datepicker--nav-title">#{title}</div><div class="datepicker--nav-action" data-action="next">#{nextHtml}</div>',i='<div class="datepicker--buttons"></div>',s='<span class="datepicker--button" data-action="#{action}">#{label}</span>',a=e.fn.datepicker,n=a.Constructor;a.Navigation=function(t,e){this.d=t,this.opts=e,this.$buttonsContainer="",this.init()},a.Navigation.prototype={init:function(){this._buildBaseHtml(),this._bindEvents()},_bindEvents:function(){this.d.$nav.on("click",".datepicker--nav-action",e.proxy(this._onClickNavButton,this)),this.d.$nav.on("click",".datepicker--nav-title",e.proxy(this._onClickNavTitle,this)),this.d.$datepicker.on("click",".datepicker--button",e.proxy(this._onClickNavButton,this))},_buildBaseHtml:function(){this.opts.onlyTimepicker||this._render(),this._addButtonsIfNeed()},_addButtonsIfNeed:function(){this.opts.todayButton&&this._addButton("today"),this.opts.clearButton&&this._addButton("clear")},_render:function(){var i=this._getTitle(this.d.currentDate),s=n.template(t,e.extend({title:i},this.opts));this.d.$nav.html(s),"years"==this.d.view&&e(".datepicker--nav-title",this.d.$nav).addClass("-disabled-"),this.setNavStatus()},_getTitle:function(t){return this.d.formatDate(this.opts.navTitles[this.d.view],t)},_addButton:function(t){this.$buttonsContainer.length||this._addButtonsContainer();var i={action:t,label:this.d.loc[t]},a=n.template(s,i);e("[data-action="+t+"]",this.$buttonsContainer).length||this.$buttonsContainer.append(a)},_addButtonsContainer:function(){this.d.$datepicker.append(i),this.$buttonsContainer=e(".datepicker--buttons",this.d.$datepicker)},setNavStatus:function(){if((this.opts.minDate||this.opts.maxDate)&&this.opts.disableNavWhenOutOfRange){var t=this.d.parsedDate,e=t.month,i=t.year,s=t.date;switch(this.d.view){case"days":this.d._isInRange(new Date(i,e-1,s),"month")||this._disableNav("prev"),this.d._isInRange(new Date(i,e+1,s),"month")||this._disableNav("next");break;case"months":this.d._isInRange(new Date(i-1,e,s),"year")||this._disableNav("prev"),this.d._isInRange(new Date(i+1,e,s),"year")||this._disableNav("next");break;case"years":this.d._isInRange(new Date(i-10,e,s),"year")||this._disableNav("prev"),this.d._isInRange(new Date(i+10,e,s),"year")||this._disableNav("next")}}},_disableNav:function(t){e('[data-action="'+t+'"]',this.d.$nav).addClass("-disabled-")},_activateNav:function(t){e('[data-action="'+t+'"]',this.d.$nav).removeClass("-disabled-")},_onClickNavButton:function(t){var i=e(t.target).closest("[data-action]"),s=i.data("action");this.d[s]()},_onClickNavTitle:function(t){return e(t.target).hasClass("-disabled-")?void 0:"days"==this.d.view?this.d.view="months":void(this.d.view="years")}}}(),function(){var t='<div class="datepicker--time"><div class="datepicker--time-current">   <span class="datepicker--time-current-hours">#{hourVisible}</span>   <span class="datepicker--time-current-colon">:</span>   <span class="datepicker--time-current-minutes">#{minValue}</span></div><div class="datepicker--time-sliders">   <div class="datepicker--time-row">      <input type="range" name="hours" value="#{hourValue}" min="#{hourMin}" max="#{hourMax}" step="#{hourStep}"/>   </div>   <div class="datepicker--time-row">      <input type="range" name="minutes" value="#{minValue}" min="#{minMin}" max="#{minMax}" step="#{minStep}"/>   </div></div></div>',i=e.fn.datepicker,s=i.Constructor;i.Timepicker=function(t,e){this.d=t,this.opts=e,this.init()},i.Timepicker.prototype={init:function(){var t="input";this._setTime(this.d.date),this._buildHTML(),navigator.userAgent.match(/trident/gi)&&(t="change"),this.d.$el.on("selectDate",this._onSelectDate.bind(this)),this.$ranges.on(t,this._onChangeRange.bind(this)),this.$ranges.on("mouseup",this._onMouseUpRange.bind(this)),this.$ranges.on("mousemove focus ",this._onMouseEnterRange.bind(this)),this.$ranges.on("mouseout blur",this._onMouseOutRange.bind(this))},_setTime:function(t){var e=s.getParsedDate(t);this._handleDate(t),this.hours=e.hours<this.minHours?this.minHours:e.hours,this.minutes=e.minutes<this.minMinutes?this.minMinutes:e.minutes},_setMinTimeFromDate:function(t){this.minHours=t.getHours(),this.minMinutes=t.getMinutes(),this.d.lastSelectedDate&&this.d.lastSelectedDate.getHours()>t.getHours()&&(this.minMinutes=this.opts.minMinutes)},_setMaxTimeFromDate:function(t){this.maxHours=t.getHours(),this.maxMinutes=t.getMinutes(),
	this.d.lastSelectedDate&&this.d.lastSelectedDate.getHours()<t.getHours()&&(this.maxMinutes=this.opts.maxMinutes)},_setDefaultMinMaxTime:function(){var t=23,e=59,i=this.opts;this.minHours=i.minHours<0||i.minHours>t?0:i.minHours,this.minMinutes=i.minMinutes<0||i.minMinutes>e?0:i.minMinutes,this.maxHours=i.maxHours<0||i.maxHours>t?t:i.maxHours,this.maxMinutes=i.maxMinutes<0||i.maxMinutes>e?e:i.maxMinutes},_validateHoursMinutes:function(t){this.hours<this.minHours?this.hours=this.minHours:this.hours>this.maxHours&&(this.hours=this.maxHours),this.minutes<this.minMinutes?this.minutes=this.minMinutes:this.minutes>this.maxMinutes&&(this.minutes=this.maxMinutes)},_buildHTML:function(){var i=s.getLeadingZeroNum,a={hourMin:this.minHours,hourMax:i(this.maxHours),hourStep:this.opts.hoursStep,hourValue:this.hours,hourVisible:i(this.displayHours),minMin:this.minMinutes,minMax:i(this.maxMinutes),minStep:this.opts.minutesStep,minValue:i(this.minutes)},n=s.template(t,a);this.$timepicker=e(n).appendTo(this.d.$datepicker),this.$ranges=e('[type="range"]',this.$timepicker),this.$hours=e('[name="hours"]',this.$timepicker),this.$minutes=e('[name="minutes"]',this.$timepicker),this.$hoursText=e(".datepicker--time-current-hours",this.$timepicker),this.$minutesText=e(".datepicker--time-current-minutes",this.$timepicker),this.d.ampm&&(this.$ampm=e('<span class="datepicker--time-current-ampm">').appendTo(e(".datepicker--time-current",this.$timepicker)).html(this.dayPeriod),this.$timepicker.addClass("-am-pm-"))},_updateCurrentTime:function(){var t=s.getLeadingZeroNum(this.displayHours),e=s.getLeadingZeroNum(this.minutes);this.$hoursText.html(t),this.$minutesText.html(e),this.d.ampm&&this.$ampm.html(this.dayPeriod)},_updateRanges:function(){this.$hours.attr({min:this.minHours,max:this.maxHours}).val(this.hours),this.$minutes.attr({min:this.minMinutes,max:this.maxMinutes}).val(this.minutes)},_handleDate:function(t){this._setDefaultMinMaxTime(),t&&(s.isSame(t,this.d.opts.minDate)?this._setMinTimeFromDate(this.d.opts.minDate):s.isSame(t,this.d.opts.maxDate)&&this._setMaxTimeFromDate(this.d.opts.maxDate)),this._validateHoursMinutes(t)},update:function(){this._updateRanges(),this._updateCurrentTime()},_getValidHoursFromDate:function(t,e){var i=t,a=t;t instanceof Date&&(i=s.getParsedDate(t),a=i.hours);var n=e||this.d.ampm,h="am";if(n)switch(!0){case 0==a:a=12;break;case 12==a:h="pm";break;case a>11:a-=12,h="pm"}return{hours:a,dayPeriod:h}},set hours(t){this._hours=t;var e=this._getValidHoursFromDate(t);this.displayHours=e.hours,this.dayPeriod=e.dayPeriod},get hours(){return this._hours},_onChangeRange:function(t){var i=e(t.target),s=i.attr("name");this.d.timepickerIsActive=!0,this[s]=i.val(),this._updateCurrentTime(),this.d._trigger("timeChange",[this.hours,this.minutes]),this._handleDate(this.d.lastSelectedDate),this.update()},_onSelectDate:function(t,e){this._handleDate(e),this.update()},_onMouseEnterRange:function(t){var i=e(t.target).attr("name");e(".datepicker--time-current-"+i,this.$timepicker).addClass("-focus-")},_onMouseOutRange:function(t){var i=e(t.target).attr("name");this.d.inFocus||e(".datepicker--time-current-"+i,this.$timepicker).removeClass("-focus-")},_onMouseUpRange:function(t){this.d.timepickerIsActive=!1}}}()}(window,jQuery);
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ },
/* 26 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($) {'use strict';
	
	/**
	 * @class GoogleMaps
	 * @classdesc Class used to initialize google maps, load markes, bind onClick event.
	 */
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var GoogleMaps = function () {
	  /**
	   * Generate and set-up default settings.
	   *
	   * @constructor
	   *
	   * @param {Object} [options]
	   * @param {Object} [markers]
	   * @param {jQuery} [clickableElements]
	   *
	   * @param {Node} [options.map] - map container node (default: document.getElementById('map'))
	   * @param {String} [options.callbackName] - function inited on ggl maps script load name (default: "initMapId1")
	   * @param {String} [options.scriptUrl] - ggl maps api script url (default: "https://maps.googleapis.com/maps/api/js")
	   * @param {String} [options.scriptKey] - ggl maps key id (default: "key=AIzaSyCrFjHOCFQbsMvKEGL92MTAffwFyHgogZI")
	   * @param {String} [options.scriptOptions] - ggl maps options added to script's url (default: `&language=ru&callback=${this.callbackName}`)
	   * @param {Object} [options.mapInitOptions] - ggl maps on create options (default: {center: {lat: 48,lng: 67.5}, zoom: 5, styles: [Array]})
	   *
	   * @param {String} [markers.image] - url to image. image will be used as marker on ggl map (default: './img/map-marker.png')
	   * @param {String} [markers.url] - url for AJAX (get markers array) (default: './ajax/googleMapMarkers.json')
	   * @param {String} [markers.eventName] - event name. used in AJAX request (default: 'markersEventName')
	   */
	  function GoogleMaps() {
	    var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
	    var markers = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
	    var clickableElements = arguments[2];
	
	    _classCallCheck(this, GoogleMaps);
	
	    this.container = options.map || document.getElementById('map');
	    this.callbackName = options.callbackName || "initMapId1";
	    this.scriptUrl = options.scriptUrl || "https://maps.googleapis.com/maps/api/js";
	    this.scriptKey = options.scriptKey || "key=AIzaSyCrFjHOCFQbsMvKEGL92MTAffwFyHgogZI";
	    this.scriptOtions = options.scriptOptions || '&language=ru&callback=' + this.callbackName;
	    this.optionsZoom = 5;
	    this.optionsCenterCords = {
	      lat: 48,
	      lng: 67.5
	    };
	    this.optionsStyle = [{
	      "featureType": "road.highway",
	      "stylers": [{ "hue": "#FFC200" }, { "saturation": -61.8 }, { "lightness": 45.599999999999994 }, { "gamma": 1 }]
	    }, {
	      "featureType": "road.arterial",
	      "stylers": [{ "hue": "#FF0300" }, { "saturation": -100 }, { "lightness": 51.19999999999999 }, { "gamma": 1 }]
	    }, {
	      "featureType": "road.local",
	      "stylers": [{ "hue": "#FF0300" }, { "saturation": -100 }, { "lightness": 52 }, { "gamma": 1 }]
	    }, {
	      "featureType": "water",
	      "stylers": [{ "hue": "#0078FF" }, { "saturation": -13.200000000000003 }, { "lightness": 2.4000000000000057 }, { "gamma": 1 }]
	    }, {
	      "featureType": "poi",
	      "stylers": [{ "hue": "#00FF6A" }, { "saturation": -1.0989010989011234 }, { "lightness": 11.200000000000017 }, { "gamma": 1 }]
	    }];
	    this.mapInitOptions = options.mapInitOptions || {
	      center: this.optionsCenterCords,
	      zoom: this.optionsZoom,
	      styles: this.optionsStyle
	    };
	
	    this.markerImage = markers.image || './img/map-marker.png';
	    this.markersUrl = markers.url || './ajax/googleMapMarkers.json';
	    this.markersEventName = markers.eventName || 'getGoogleMapsShopMarkers';
	
	    this.clickableElements = clickableElements || null;
	
	    this.markers = [];
	  }
	
	  /**
	   * Describe callback which will be called on google maps scripts load.
	   *
	   * @return {GoogleMaps}
	   */
	
	
	  _createClass(GoogleMaps, [{
	    key: 'setGoogleMapCallback',
	    value: function setGoogleMapCallback() {
	      var _this2 = this;
	
	      var _this = this;
	
	      // callback on ggl map load
	      window[this.callbackName] = function () {
	
	        //create map
	        var MapInstance = new google.maps.Map(_this2.container, _this2.mapInitOptions);
	
	        // get and set markers
	        var getAndSetMarkers = new Promise(function (resolve, reject) {
	          $.ajax({
	            url: _this2.markersUrl,
	            method: 'GET',
	            dataType: 'json',
	            data: {
	              eventName: _this2.markersEventName
	            },
	            success: function success(res) {
	              resolve(res);
	            },
	            error: function error(xhr) {
	              reject(xhr);
	            }
	          });
	        }).then(function (res) {
	          res.forEach(function (element) {
	            // marker options
	            var markerOptions = {
	              map: MapInstance,
	              position: element.position,
	              icon: _this2.markerImage,
	              title: '\u0410\u0434\u0440\u0435\u0441: ' + element.address
	            };
	
	            // create marker
	            var marker = new google.maps.Marker(markerOptions);
	
	            // Marker info window content template
	            var templateString = '<div id="mapLinkContainer">\n                                    <img id="mapLinkImage" src="' + element.image + '">\n                                    <div id="mapLinkTextContainer">\n                                      <div id="mapLinkAddress">\u0410\u0434\u0440\u0435\u0441: ' + element.address + '</div>\n                                      <div id="mapLinkPhone">\u0422\u0435\u043B\u0435\u0444\u043E\u043D: ' + element.phone + '</div>\n                                      <div id="mapLinkWorkTime">\u0420\u0435\u0436\u0438\u043C \u0440\u0430\u0431\u043E\u0442\u044B: ' + element.workTime + '</div>\n                                    </div>\n                                  </div>';
	
	            // create info window
	            marker.infoWindow = new InfoBubble({
	              content: templateString,
	              shadowStyle: 0,
	              borderColor: "transparent",
	              backgroundClassName: 'transparent',
	              padding: "10px",
	              backgroundColor: "transparent",
	              arrowSize: 0,
	              borderRadius: 0,
	              minWidth: "225px",
	              minHeight: "230px",
	              closeSrc: "./img/map-close-button.png",
	              disableAnimation: true
	            });
	
	            // Stole the ref to infowindows in array
	            _this2.markers.push(marker);
	
	            // function-handler for click/tap events on markers
	            function markerClickHandler() {
	              if (marker.infoWindow.isOpen_) {
	                marker.infoWindow.close();
	                return;
	              }
	
	              _this.markers.forEach(function (element) {
	                element.infoWindow.close();
	              });
	
	              // set map's zoom
	              MapInstance.setZoom(12);
	
	              marker.infoWindow.open(MapInstance, marker);
	            }
	
	            // add click listener to the MARKER (open infowindow, close on double-click)
	            marker.addListener('click', function () {
	              markerClickHandler();
	            });
	
	            // add click listener to the MAPLINK (open infowindow, close on double-click)
	            var thisMaplinkElements = _this2.clickableElements.filter('[data-maplink-id="' + element.mapLinkId + '"]');
	            thisMaplinkElements.on('click tap', function () {
	              markerClickHandler();
	            });
	          });
	        }).catch(function (xhr) {
	          console.log('Error is thrown. Error text:\n' + xhr.text);
	        });
	      };
	    }
	
	    /**
	     * Load google maps api v3 scripts and call callback.
	     */
	
	  }, {
	    key: 'init',
	    value: function init() {
	      var _this3 = this;
	
	      !/* require.ensure */(function () {/* WEBPACK VAR INJECTION */(function($) {
	        var InfoBubbleInstance = __webpack_require__(2);
	
	        // set callback
	        _this3.setGoogleMapCallback();
	        //append google maps api v3 script
	        $('body').append('<script async src="' + _this3.scriptUrl + '?' + _this3.scriptKey + _this3.scriptOtions + '" charset="UTF-8"></script>');
	      
	/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(1)))}(__webpack_require__));
	    }
	  }]);
	
	  return GoogleMaps;
	}();
	
	exports.default = GoogleMaps;
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ },
/* 27 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($) {'use strict';
	
	/**
	 * @name Validator.js
	 * @version 0.1
	 * @author Vitali Shapovalov
	 *
	 * @fileoverview
	 * This modules is used for forms validation.
	 * Text fields, emails, phones, checkobxes etc.
	 */
	
	/*
	 * Licensed under the Apache License, Version 2.0 (the "License");
	 * you may not use this file except in compliance with the License.
	 * You may obtain a copy of the License at
	 *
	 *     http://www.apache.org/licenses/LICENSE-2.0
	 *
	 * Unless required by applicable law or agreed to in writing, software
	 * distributed under the License is distributed on an "AS IS" BASIS,
	 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	 * See the License for the specific language governing permissions and
	 * limitations under the License.
	 */
	
	/**
	 * Validator.js module
	 *
	 * @constructor
	 *
	 * @param {jQuery} form - form to validate
	 * @param {Function} ajaxOptions - function that should return AJAX request options
	 * @param {Object} [options] - user specified options
	 * @param {Boolean} [options.nestedInModal=false] - when true, remove fields incorrect state on modal hide
	 * @param {String} [options.fieldsSelector='.form-input'] - form's field selector string
	 */
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var Validator = function () {
	  function Validator() {
	    var form = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : $();
	
	    var _this = this;
	
	    var ajaxOptions = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
	    var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
	
	    _classCallCheck(this, Validator);
	
	    /** Form to validate */
	    this.form = form;
	
	    /** User-specified AJAX options */
	    this.ajaxOptions = ajaxOptions;
	
	    /** User-specified options */
	    this.options = {
	      modal: options.nestedInModal || false,
	      fieldsSelector: options.fieldsSelector || '.form-input'
	    };
	
	    /** Default options */
	    this.default = {
	      // SELECTORS
	      incorrectFields: '.incorrect',
	      error: '.error',
	
	      // CLASS NAMES
	      incorrectClass: 'incorrect',
	      formIsValidClass: 'validated',
	
	      // ERROR MESSAGES TEXT
	      fieldEmptyText: 'Заполните поле',
	      incorrectPhoneText: 'Введите корректный номер',
	      incorrectEmailText: 'Введите корректный Email',
	
	      // PARAM NAME FOR SETTING CUSTOM ERROR MESSAGES
	      textDataName: 'validation-text',
	
	      // 'data-validation-*' TYPES
	      dataCondition: 'validation-condition',
	      dataType: 'validation-type',
	
	      // FIELD VALIDATION TYPES
	      textType: 'text',
	      phoneType: 'phone',
	      emailType: 'email',
	      checkboxType: 'checkbox',
	      radioType: 'radio',
	
	      // 'data-validation-condition' VALUE TYPES
	      minLengthDataName: 'min-length',
	      lengthDataName: 'length',
	
	      // 'data-validation' VALUE TYPES
	      requiredToValidate: 'required'
	    };
	
	    /** DOM elements */
	    this.elements = {
	      // DYNAMIC ELEMENTS SELECTORS
	      inputs: function inputs() {
	        return _this.form.find('input, textarea, select');
	      },
	      fields: function fields() {
	        return _this.form.find(_this.options.fieldsSelector);
	      },
	
	      // STATIC ELEMENTS SELECTORS
	      modal: this.form.parents('.modal'),
	      button: this.form.find('.validate-form-button')
	    };
	
	    /** Buffer object */
	    this.buffer = {};
	
	    /** Initialize */
	    this.init();
	  }
	
	  /**
	   * Check form for validness.
	   *
	   * @return {Boolean}
	   */
	
	
	  _createClass(Validator, [{
	    key: 'formIsValid',
	    value: function formIsValid() {
	      var incorrectFields = this.form.find(this.default.incorrectFields);
	
	      return incorrectFields.length === 0 && this.form.hasClass(this.default.formIsValidClass);
	    }
	
	    /**
	     * Validates an email.
	     *
	     * @static
	     *
	     * @param {String} email
	     * @return {Boolean}
	     */
	
	  }, {
	    key: 'removeIncorrectState',
	
	
	    /**
	     * Remove incorrect state from all fields.
	     *
	     * @return {Validator}
	     */
	    value: function removeIncorrectState() {
	      this.form.find(this.options.fieldsSelector).removeClass(this.default.incorrectClass);
	
	      return this;
	    }
	
	    /**
	     * Reset form to default state.
	     *
	     * @return {Validator}
	     */
	
	  }, {
	    key: 'resetForm',
	    value: function resetForm() {
	      this.form[0].reset();
	
	      return this;
	    }
	
	    /**
	     * Remove incorrect state from all fields when modal is closed.
	     *
	     * @return {Validator}
	     */
	
	  }, {
	    key: 'removeIncorrectStateOnModalClose',
	    value: function removeIncorrectStateOnModalClose() {
	      var _this2 = this;
	
	      this.elements.modal.on('hidden.bs.modal', function () {
	        return _this2.removeIncorrectState();
	      });
	
	      return this;
	    }
	
	    /**
	     * Set incorrect state on field.
	     *
	     * @param {jQuery} field
	     * @param {String} errorText - displayed error text
	     */
	
	  }, {
	    key: 'throwError',
	    value: function throwError(field, errorText) {
	      field.addClass(this.default.incorrectClass).find(this.default.error).text(errorText);
	    }
	
	    /**
	     * Check field for validness and set valid/incorrect state.
	     *
	     * @param {jQuery} field
	     * @param {Number} valueLength
	     * @param {String} errorText
	     * @param {Boolean} condition - condition to set valid state
	     */
	
	  }, {
	    key: 'checkFieldValidness',
	    value: function checkFieldValidness(field, condition, errorText, valueLength) {
	      var dataText = field.data(this.default.textDataName);
	
	      if (dataText && dataText.length) errorText = dataText;
	
	      if (!valueLength) {
	        this.throwError(field, this.default.fieldEmptyText);
	      } else if (!condition) {
	        this.throwError(field, errorText);
	      } else {
	        this.form.addClass(this.default.formIsValidClass);
	      }
	    }
	
	    /**
	     * Validates field.
	     *
	     * @param {jQuery} field
	     */
	
	  }, {
	    key: 'validateField',
	    value: function validateField(field) {
	      var type = field.data(this.default.dataType);
	      var fieldParams = {
	        condition: true,
	        errorText: this.default.fieldEmptyText,
	        length: 1
	      };
	
	      switch (type) {
	        case this.default.textType:
	          {
	            fieldParams = this.validateTextField(field);
	            break;
	          }
	        case this.default.phoneType:
	          {
	            fieldParams = this.validateTextField(field);
	            fieldParams.errorText = this.default.incorrectPhoneText;
	            break;
	          }
	        case this.default.emailType:
	          {
	            fieldParams = this.validateEmailField(field);
	            fieldParams.errorText = this.default.incorrectEmailText;
	            break;
	          }
	        case this.default.radioType:
	          {
	            fieldParams.condition = this.validateRadioField(field);
	            break;
	          }
	      }
	
	      this.checkFieldValidness(field, fieldParams.condition, fieldParams.errorText, fieldParams.length);
	    }
	
	    /**
	     * Validate 'Text' field.
	     *
	     * @param {jQuery} field
	     * @return {Object}
	     */
	
	  }, {
	    key: 'validateTextField',
	    value: function validateTextField(field) {
	      var input = field.find('input').length ? field.find('input') : field.find('textarea');
	      var value = input.val();
	      var valueLength = value.length;
	
	      var conditionType = input.data(this.default.dataCondition);
	      var condition = void 0,
	          errorText = void 0;
	
	      switch (conditionType) {
	        case this.default.minLengthDataName:
	          {
	            var minLength = input.data(this.default.minLengthDataName);
	
	            condition = valueLength >= parseInt(minLength, 10);
	            errorText = '\u041C\u0438\u043D\u0438\u043C\u0430\u043B\u044C\u043D\u0430\u044F \u0434\u043B\u0438\u043D\u043D\u0430 \u043F\u043E\u043B\u044F - ' + minLength + ' \u0441\u0438\u043C\u0432\u043E\u043B\u0430';
	
	            break;
	          }
	        case this.default.lengthDataName:
	          {
	            var neededLength = input.data(this.default.lengthDataName);
	
	            condition = valueLength === parseInt(neededLength, 10);
	            errorText = '\u041D\u0435\u043E\u0431\u0445\u043E\u0434\u0438\u043C\u0430\u044F \u0434\u043B\u0438\u043D\u043D\u0430 \u043F\u043E\u043B\u044F - ' + neededLength + ' \u0441\u0438\u043C\u0432\u043E\u043B\u043E\u0432';
	
	            break;
	          }
	      }
	
	      return {
	        condition: condition,
	        errorText: errorText,
	        length: valueLength
	      };
	    }
	
	    /**
	     * Validate 'Email' field.
	     *
	     * @param {jQuery} field
	     */
	
	  }, {
	    key: 'validateEmailField',
	    value: function validateEmailField(field) {
	      var value = field.find('input').val();
	
	      return {
	        condition: Validator.validateEmail(value),
	        length: value.length
	      };
	    }
	
	    /**
	     * Validate 'Radio' field.
	     *
	     * @param {jQuery} field
	     */
	
	  }, {
	    key: 'validateRadioField',
	    value: function validateRadioField(field) {
	      var checkedVisibleRadio = field.find('input[type="radio"]:checked:visible');
	
	      return checkedVisibleRadio.length >= 1;
	    }
	
	    /**
	     * Serialize form.
	     *
	     * @return {Array} serialized form data
	     */
	
	  }, {
	    key: 'serializedFormData',
	    value: function serializedFormData() {
	      return this.form.serialize();
	    }
	
	    /**
	     * Send data if form is valid.
	     *
	     * @param {Object|Function} options - ajax options
	     * @return {Validator}
	     */
	
	  }, {
	    key: 'sendIfValidated',
	    value: function sendIfValidated() {
	      var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.ajaxOptions;
	
	      if (this.formIsValid()) {
	        $.ajax(options.call(null, this));
	      }
	
	      return this;
	    }
	
	    /**
	     * Validate form fields.
	     *
	     * @return {Validator}
	     */
	
	  }, {
	    key: 'validateAllFields',
	    value: function validateAllFields() {
	      var _this3 = this;
	
	      this.elements.fields().each(function (index, field) {
	        var $field = $(field);
	        var requiredToValidate = $field.data('validation') === _this3.default.requiredToValidate;
	
	        if (requiredToValidate) _this3.validateField($field);
	      });
	
	      return this;
	    }
	
	    /**
	     * Validate form.
	     *
	     * @return {Validator}
	     */
	
	  }, {
	    key: 'runFormValidation',
	    value: function runFormValidation() {
	      this.removeIncorrectState().validateAllFields().sendIfValidated();
	
	      return this;
	    }
	
	    /**
	     * Initialize on-click validation.
	     *
	     * @return {Validator}
	     */
	
	  }, {
	    key: 'bindOnClickValidation',
	    value: function bindOnClickValidation() {
	      var _this4 = this;
	
	      this.elements.button.on('click.validation tap.validation', function (e) {
	        e.preventDefault();
	
	        _this4.runFormValidation();
	      });
	
	      return this;
	    }
	
	    /**
	     * Unbind on-click event.
	     *
	     * @return {Validator}
	     */
	
	  }, {
	    key: 'unbindOnClick',
	    value: function unbindOnClick() {
	      this.elements.button.unbind('click.validation tap.validation');
	
	      return this;
	    }
	
	    /**
	     * Initialize all validation scripts.
	     */
	
	  }, {
	    key: 'init',
	    value: function init() {
	      this.bindOnClickValidation();
	
	      if (this.options.modal) this.removeIncorrectStateOnModalClose();
	    }
	  }], [{
	    key: 'validateEmail',
	    value: function validateEmail(email) {
	      var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
	
	      return pattern.test(email);
	    }
	  }]);
	
	  return Validator;
	}();
	
	exports.default = Validator;
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ },
/* 28 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($) {'use strict';
	
	/**
	 * Add/remove from cart, title crop, lazy-load.
	 *
	 * @module Product item
	 */
	
	/** Import utility functions */
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	/** Import CartItem class. */
	
	
	var _Helpers = __webpack_require__(29);
	
	var _CartController = __webpack_require__(30);
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	/** Class representing a website's product actions. Initialized by default. */
	var ProductItem = function () {
	  /**
	   * @constructor
	   */
	  function ProductItem() {
	    _classCallCheck(this, ProductItem);
	
	    // for dynamically added elements
	    this.items = function () {
	      return $('.default-item');
	    };
	
	    this.title = $('.default-item-title');
	
	    this.buyButton = '.default-item-buy';
	    this.removeButton = '.default-item-purchased';
	
	    this.maxTitleLength = 62;
	
	    /** Class-indicators names */
	    this.itemIsLoadedClass = 'loaded';
	  }
	
	  /**
	   * Crop title up to maximal allowed title length.
	   *
	   * @return {ProductItem}
	   **/
	
	
	  _createClass(ProductItem, [{
	    key: 'cropTitle',
	    value: function cropTitle() {
	      (0, _Helpers.cropText)(this.title, this.maxTitleLength);
	
	      return this;
	    }
	
	    /**
	     * Product item image lazy-load.
	     *
	     * @return {ProductItem}
	     */
	
	  }, {
	    key: 'imageLazyloadInit',
	    value: function imageLazyloadInit() {
	      var _this = this;
	
	      var checkAndLoad = function checkAndLoad() {
	        _this.items().not('.' + _this.itemIsLoadedClass).each(function (index, el) {
	          var $el = $(el);
	
	          if ((0, _Helpers.isElementInViewport)($el, 350)) {
	            var img = $el.find('.default-item-preview img');
	            var src = img.attr('data-src');
	
	            $el.addClass(_this.itemIsLoadedClass);
	
	            img.attr('src', src);
	          }
	        });
	      };
	      checkAndLoad();
	
	      $(window).on('load scroll resize orientationchange', checkAndLoad);
	
	      $('.owl-carousel').on('change.owl.carousel', function (e) {
	        setTimeout(function () {
	          var element = $(e.currentTarget).find('.owl-item.active').find('.default-item');
	          element.each(function (index, el) {
	            var $el = $(el);
	            var img = $el.find('.default-item-preview img');
	            var src = img.attr('data-src');
	
	            $el.addClass(_this.itemIsLoadedClass);
	
	            img.attr('src', src);
	          });
	        }, 300);
	      });
	
	      return this;
	    }
	
	    /**
	     * Initialize 'Add to cart' and 'Remove from cart' buttons.
	     *
	     * @return {ProductItem}
	     */
	
	  }, {
	    key: 'initBuyButton',
	    value: function initBuyButton() {
	      var CartItemInstance = new _CartController.CartItem(this.buyButton, this.removeButton);
	
	      CartItemInstance.bind();
	
	      return this;
	    }
	
	    /**
	     * Initialize Product item scripts.
	     *
	     * @static
	     */
	
	  }], [{
	    key: 'init',
	    value: function init() {
	      var obj = new this();
	
	      obj.initBuyButton().cropTitle().imageLazyloadInit();
	    }
	  }]);
	
	  return ProductItem;
	}();
	
	exports.default = ProductItem;
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ },
/* 29 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($) {'use strict';
	
	/**
	 * Commonly used constants and functions.
	 *
	 * @module Helpers
	 */
	
	/**
	 * CSS class name to indicate most elements state.
	 *
	 * @constant
	 * @type {String}
	 */
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };
	
	var active = exports.active = 'active';
	
	/**
	 * CSS class name to indicate form's incorrect state.
	 *
	 * @constant
	 * @type {String}
	 */
	var formIsIncorrect = exports.formIsIncorrect = 'incorrect';
	
	/**
	 * CSS class name to indicate form's correct state.
	 *
	 * @constant
	 * @type {String}
	 */
	var formIsValidated = exports.formIsValidated = 'validated';
	
	/**
	 * Path to SVG-sprite file.
	 *
	 * @constant
	 * @type {String}
	 */
	var svgPath = exports.svgPath = './img/svg-default.svg';
	
	/**
	 * Symbol for params separation.
	 *
	 * @constant
	 * @type {String}
	 */
	var separator = exports.separator = '?';
	
	/**
	 * Cache body DOM element.
	 *
	 * @constant
	 * @type {jQuery}
	 */
	var body = exports.body = $('body');
	
	/**
	 * Detect current page.
	 *
	 * @constant
	 * @type {String}
	 */
	var currentPage = exports.currentPage = body.find('main').data('page');
	
	/**
	 * Detect personal cabinet's current page.
	 *
	 * @constant
	 * @type {String}
	 */
	var currentPcPage = exports.currentPcPage = body.find('main').data('pc-page');
	
	/**
	 * Menu should be hidden on all resolutions?
	 *
	 * @constant
	 * @type {Boolean}
	 */
	var menuIsAlwaysHidden = exports.menuIsAlwaysHidden = currentPage !== 'main';
	
	/**
	 * Detect window width once.
	 *
	 * @constant
	 * @type {Number}
	 */
	var screenWidth = exports.screenWidth = $(window).width();
	
	/**
	 * Detect whether user logged in or not and return true/false flag.
	 *
	 * @constant
	 * @type {Boolean}
	 */
	var userLoggedIn = exports.userLoggedIn = body.hasClass('logged');
	
	/**
	 * Breakpoint for landscape tablets.
	 *
	 * @constant
	 * @type {Number}
	 */
	var tabletLandscapeBreakpoint = exports.tabletLandscapeBreakpoint = 1280;
	
	/**
	 * Breakpoint for portrait tablets.
	 *
	 * @constant
	 * @type {Number}
	 */
	var tabletPortraitBreakpoint = exports.tabletPortraitBreakpoint = 600;
	
	/**
	 * Cache has own property method.
	 *
	 * @constant
	 * @type {Function}
	 */
	var has = exports.has = Object.prototype.hasOwnProperty;
	
	/**
	 * Custom HTML template for owlCarousel 'Prev' button.
	 *
	 * @constant
	 * @type {String}
	 */
	var buttonLeftTemplate = exports.buttonLeftTemplate = '<svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="' + svgPath + '#icon-item-buttonL"></use></svg>';
	
	/**
	 * Custom HTML template for owlCarousel 'Next' button.
	 *
	 * @constant
	 * @type {String}
	 */
	var buttonRightTemplate = exports.buttonRightTemplate = '<svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="' + svgPath + '#icon-item-buttonR"></use></svg>';
	
	/**
	 * Common options for owlCarousel sliders.
	 *
	 * @type {Object}
	 */
	var owlCommonOptions = exports.owlCommonOptions = {
	  loop: true,
	  dots: false,
	  navText: [buttonLeftTemplate, buttonRightTemplate],
	  margin: 10,
	  responsive: {
	    0: {
	      nav: true,
	      items: 1
	    },
	    600: {
	      nav: true,
	      items: 2
	    },
	    1024: {
	      nav: true,
	      items: 4
	    },
	    1670: {
	      nav: true,
	      items: 6
	    }
	  }
	};
	
	/**
	 * Initialize owl carousel on each element from array.
	 *
	 * @param {jQuery} elements - array of elements to init owlCarousel on.
	 * @param {Object} [options] - options for owlCarousel.
	 */
	var initOwlCarousel = exports.initOwlCarousel = function initOwlCarousel(elements) {
	  var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : owlCommonOptions;
	
	  elements.each(function (index, el) {
	    $(el).owlCarousel(options);
	  });
	};
	
	/**
	 * Check for the DOM element is visible in the current viewport.
	 *
	 * @param {jQuery} el - element to check.
	 * @param {Number} range - additional range to check, 200 by default.
	 */
	var isElementInViewport = exports.isElementInViewport = function isElementInViewport(el) {
	  var range = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 200;
	
	  el = el[0];
	  var elCords = el.getBoundingClientRect();
	
	  return elCords.top >= -range && elCords.left >= 0 && elCords.bottom <= $(window).height() + range && elCords.right <= $(window).width();
	};
	
	/**
	 * Crop text in selected element, if text's length more then specified number.
	 *
	 * @param {jQuery} elements - Elements to crop.
	 * @param {Number} symbolsToShow - Number of symbols, that should be shown.
	 */
	var cropText = exports.cropText = function cropText(elements, symbolsToShow) {
	  elements.each(function () {
	    var element = $(this);
	    var text = element.text();
	
	    if (text.length > symbolsToShow) {
	      var croppedText = text.substr(0, symbolsToShow) + '...';
	
	      element.text(croppedText);
	    }
	  });
	};
	
	/**
	 * Detect user logged in or not.
	 *
	 * @returns {Number} - Current window width.
	 */
	var screenWidthDynamically = exports.screenWidthDynamically = function screenWidthDynamically() {
	  return $(window).width();
	};
	
	/**
	 * Check specified item to be target of event.
	 *
	 * @param {Object} e - Event object.
	 * @param {jQuery} item - Item to compare with.
	 * @returns {Boolean} - Indicate whether clicked target is the specified item or no.
	 */
	var checkClosest = exports.checkClosest = function checkClosest(e, item) {
	  return $(e.target).closest(item).length > 0;
	};
	
	/**
	 * Toggles specified class name on body.
	 *
	 * @param {jQuery} elem - Trigger element.
	 * @param {String} className - Class name to apply.
	 */
	var activeBodyClassItemInit = exports.activeBodyClassItemInit = function activeBodyClassItemInit(elem, className) {
	  elem.on('click tap', function () {
	    body.toggleClass(className);
	  });
	};
	
	/**
	 * Toggle dropdown on click.
	 *
	 * @param {jQuery} activeItem - Click event handler.
	 * @param {jQuery} toggleItem - Toggling object.
	 */
	var toggleDropdown = exports.toggleDropdown = function toggleDropdown(activeItem, toggleItem) {
	  activeItem.on('click tap', function () {
	    toggleItem.toggleClass(active);
	  });
	};
	
	/**
	 * Remove active class if click outside the element.
	 *
	 * @param {jQuery} activeItem - Default click event handler to active item.
	 * @param {jQuery} toggleItem - Toggling object.
	 * @param {String} className
	 */
	var hideOnBodyClick = exports.hideOnBodyClick = function hideOnBodyClick(activeItem, toggleItem, className) {
	  var classNameToRemove = '';
	
	  typeof className === 'string' ? classNameToRemove = className : classNameToRemove = active;
	
	  body.on('click tap', function (e) {
	    if (!checkClosest(e, toggleItem) && toggleItem.hasClass(classNameToRemove) && !checkClosest(e, activeItem)) {
	      toggleItem.removeClass(classNameToRemove);
	    }
	  });
	};
	
	/**
	 * Remove specified class name if clicked anywhere outside the specified container.
	 *
	 * @param {jQuery} element - Container.
	 * @param {jQuery} elementToRemoveClass - Remove class from this element.
	 * @param {String} className - Class name to remove.
	 */
	var removeClassOnBodyClick = exports.removeClassOnBodyClick = function removeClassOnBodyClick(element) {
	  var elementToRemoveClass = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : element;
	  var className = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'incorrect';
	
	  body.on('click tap', function (e) {
	    var clickIsOutsideTheElement = !$(e.target).closest(element).length > 0;
	
	    if (clickIsOutsideTheElement) elementToRemoveClass.removeClass(className);
	  });
	};
	
	/**
	 * Makes default dropdown object with event handlers.
	 *
	 * @param {jQuery} activeItem - Default click event handler to active item.
	 * @param {jQuery} toggleItem - Toggling object.
	 */
	var dropdownItemInit = exports.dropdownItemInit = function dropdownItemInit(activeItem, toggleItem) {
	  toggleDropdown(activeItem, toggleItem);
	  hideOnBodyClick(activeItem, toggleItem);
	};
	
	/**
	 * Select item from list: make it active ( while disabling others ), change selected item text.
	 *
	 * @param {jQuery} item - Clicked item.
	 * @param {jQuery} selected - Selected item's text keeper.
	 * @param {jQuery} list - List of items.
	 * @param {String} customEventName - listen for this event
	 */
	var selectOneFromListInit = exports.selectOneFromListInit = function selectOneFromListInit(item, selected, list) {
	  var customEventName = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : '';
	
	  item.on('click tap ' + customEventName, function () {
	    var $this = $(this);
	
	    selected.text($this.text());
	
	    item.removeClass(active);
	    $this.addClass(active);
	
	    (typeof list === 'undefined' ? 'undefined' : _typeof(list)) === 'object' ? list.removeClass(active) : null;
	  });
	};
	
	/**
	 * Reduce the set of checkbox elements to checked ones.
	 *
	 * @param {jQuery} checkboxes - list of items.
	 */
	var getChecked = exports.getChecked = function getChecked(checkboxes) {
	  return $.map(checkboxes, function (checkbox) {
	    if ($(checkbox).prop('checked')) return checkbox;
	  });
	};
	
	/**
	 * Get length of checked elements from list.
	 *
	 * @param {jQuery} checkboxes - list of items.
	 */
	var getCheckedLength = exports.getCheckedLength = function getCheckedLength(checkboxes) {
	  return getChecked(checkboxes).length;
	};
	
	/**
	 * Send AJAX with specified callback.
	 *
	 * @param {String} url
	 * @param {Object|Array|String} data
	 * @param {Function} callback
	 * @param {Object} [context=null] - callback is called within this context
	 * @param {String} [method=GET]
	 */
	var sendJsonAjaxWithCallback = exports.sendJsonAjaxWithCallback = function sendJsonAjaxWithCallback(url, data, callback) {
	  var context = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
	  var method = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : 'GET';
	
	  $.ajax({
	    url: url,
	    method: method,
	    dataType: 'json',
	    cache: false,
	    data: data,
	    success: function success(res) {
	      console.log('DATA: ' + JSON.stringify(data) + ' \n Response: ' + JSON.stringify(res));
	      callback.call(context, res);
	    }
	  });
	};
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ },
/* 30 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($) {'use strict';
	
	/**
	 * Cart functions (add to cart etc.)
	 *
	 * @module CartController
	 */
	
	/** Import utility functions */
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.CartItem = undefined;
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _Helpers = __webpack_require__(29);
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	/**
	 * @class CartController
	 * @classdesc Class representing a website's cart actions.
	 */
	var CartController = function () {
	  function CartController() {
	    _classCallCheck(this, CartController);
	
	    /** Cart */
	    this.cartCount = $('.header-cart .cart');
	    this.cartPrice = $('.header-cart .subtitle');
	    this.cartIsEmptyText = 'Ваша корзина пуста';
	
	    /** AJAX URLs */
	    this.cartUrl = './ajax/success-cart.json';
	    this.getItemsInCartUrl = './ajax/cart.json';
	
	    /** Class-indicators names */
	    this.purchasedClassName = 'added';
	    this.cartIsActiveClass = 'cart-active';
	
	    /** AJAX event names */
	    this.getItemsInCartEventName = 'getCart';
	
	    /** Other */
	    this.buttonStates = {
	      pending: 'pending',
	      completed: 'completed'
	    };
	
	    return this;
	  }
	
	  /**
	   * Get current count.
	   *
	   * @static
	   */
	
	
	  _createClass(CartController, [{
	    key: 'setButtonState',
	
	
	    /**
	     * Set button state (request pending / completed)
	     *
	     * @param {jQuery} product
	     * @param {String} state - state to set.
	     * @return {CartController}
	     */
	    value: function setButtonState(product, state) {
	      for (var _state in this.buttonStates) {
	        if (_Helpers.has.call(this.buttonStates, _state)) product.removeClass(_state);
	      }
	
	      product.addClass(state);
	    }
	
	    /**
	     * Set cart data.
	     *
	     * @param {Number} newAmount - elements in cart.
	     * @param {Number} newPrice - total price.
	     * @return {CartController}
	     */
	
	  }, {
	    key: 'setCartData',
	    value: function setCartData(newAmount, newPrice) {
	      CartController.currentCount = newAmount;
	      CartController.totalPrice = newPrice;
	
	      return this;
	    }
	
	    /**
	     * Change cart displayed data.
	     *
	     * @return {CartController}
	     */
	
	  }, {
	    key: 'refreshDisplayedCartData',
	    value: function refreshDisplayedCartData() {
	      this.cartCount.text(CartController.getCurrentCount);
	      this.cartPrice.text(CartController.getTotalPrice + ' \u0442');
	
	      var className = this.cartIsActiveClass;
	      if (+CartController.getCurrentCount > 0) {
	        !_Helpers.body.hasClass(className) ? _Helpers.body.addClass(className) : null;
	      } else {
	        _Helpers.body.removeClass(className);
	
	        this.cartPrice.text(this.cartIsEmptyText);
	      }
	
	      return this;
	    }
	
	    /**
	     * Set and change displayed cart data.
	     *
	     * @param {Number} newAmount - elements in cart.
	     * @param {Number} newPrice - total price.
	     * @return {CartController}
	     */
	
	  }, {
	    key: 'updateCartData',
	    value: function updateCartData(newPrice) {
	      var newAmount = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
	
	      var currentTotalPrice = +CartController.getTotalPrice;
	      var currentTotalCount = +CartController.getCurrentCount;
	
	      newAmount = currentTotalCount + newAmount;
	      newPrice = currentTotalPrice + newPrice;
	
	      this.setCartData(newAmount, newPrice).refreshDisplayedCartData();
	
	      return this;
	    }
	
	    /**
	     * Get current products in cart.
	     */
	
	  }, {
	    key: 'getCurrentProducts',
	    value: function getCurrentProducts() {
	      var _this2 = this;
	
	      var data = {
	        event: this.getItemsInCartEventName
	      };
	
	      $.ajax({
	        url: this.getItemsInCartUrl,
	        dataType: 'json',
	        method: 'GET',
	        data: data,
	        success: function success(res) {
	          var totalAmount = +res.totalAmount;
	          var totalPrice = +res.totalPrice;
	
	          if (totalAmount && totalPrice) {
	            _this2.setCartData(totalAmount, totalPrice).refreshDisplayedCartData();
	          }
	        }
	      });
	    }
	  }], [{
	    key: 'getCurrentCount',
	    get: function get() {
	      return localStorage.getItem('userProductsInCartCount');
	    }
	
	    /**
	     * Get total price.
	     *
	     * @static
	     */
	
	  }, {
	    key: 'getTotalPrice',
	    get: function get() {
	      return localStorage.getItem('userProductsInCartTotalPrice');
	    }
	
	    /**
	     * Set current count.
	     *
	     * @static
	     */
	
	  }, {
	    key: 'currentCount',
	    set: function set(newCurrentCount) {
	      localStorage.setItem('userProductsInCartCount', newCurrentCount);
	    }
	
	    /**
	     * Set total price.
	     *
	     * @static
	     */
	
	  }, {
	    key: 'totalPrice',
	    set: function set(newPrice) {
	      localStorage.setItem('userProductsInCartTotalPrice', newPrice);
	    }
	  }]);
	
	  return CartController;
	}();
	
	/**
	 * Cache cart's DOM elements and bind 'Buy' button.
	 *
	 * @class CartItem
	 * @classdesc Class used for adding products to cart.
	 * @extends CartController
	 *
	 * @param {String} addButton - this product's 'buy' button selector
	 * @param {String} removeButton - this product's 'remove' button selector
	 * @param {String} dataObject - object with product data (id, price etc.) selector
	 */
	
	
	exports.default = CartController;
	
	var CartItem = exports.CartItem = function (_CartController) {
	  _inherits(CartItem, _CartController);
	
	  function CartItem() {
	    var addButton = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
	    var removeButton = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
	    var dataObject = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '';
	
	    _classCallCheck(this, CartItem);
	
	    var _this3 = _possibleConstructorReturn(this, (CartItem.__proto__ || Object.getPrototypeOf(CartItem)).call(this));
	
	    _this3.buyButton = addButton;
	    _this3.dataObject = dataObject;
	    _this3.removeButton = removeButton;
	
	    _this3.buyEventName = 'buyCart';
	    _this3.removeEventName = 'removeCart';
	    return _this3;
	  }
	
	  /**
	   * Change product quantity (add/remove).
	   *
	   * @param {String} product - product object
	   * @param {String} event - Event name ('add' or 'remove').
	   * @return {CartItem}
	   */
	
	
	  _createClass(CartItem, [{
	    key: 'changeCartQuantity',
	    value: function changeCartQuantity(product, event) {
	      var _this4 = this;
	
	      product = $(product);
	      var id = product.data('id');
	      var price = product.data('price');
	      var purchasedClassName = this.purchasedClassName;
	      var data = {
	        event: event,
	        id: id,
	        price: price
	      };
	
	      var checkEvent = function (buyCB, removeCB) {
	        switch (event) {
	          case this.buyEventName:
	            {
	              buyCB.call(this);
	              product.addClass(purchasedClassName);
	              break;
	            }
	          case this.removeEventName:
	            {
	              removeCB.call(this);
	              product.removeClass(purchasedClassName);
	              break;
	            }
	        }
	      }.bind(this);
	
	      $.ajax({
	        url: this.cartUrl,
	        dataType: 'json',
	        method: 'POST',
	        cache: false,
	        data: data,
	        beforeSend: function beforeSend() {
	          _this4.setButtonState(product, _this4.buttonStates.pending);
	        },
	        success: function success(res) {
	          console.log('DATA:\n' + JSON.stringify(data));
	          var response = res.response;
	
	          if (response === 'success') {
	            checkEvent(function () {
	              _this4.updateCartData(+price);
	            }, function () {
	              _this4.updateCartData(-+price, -1);
	            });
	          } else if (response === 'change') {
	            _this4.setCartData(res.basket.amount, res.basket.price).refreshDisplayedCartData();
	          }
	        },
	        complete: function complete() {
	          _this4.setButtonState(product, _this4.buttonStates.completed);
	        }
	      });
	
	      return this;
	    }
	
	    /**
	     * Bind action buttons.
	     */
	
	  }, {
	    key: 'bind',
	    value: function bind() {
	      var _this = this;
	
	      function bindButton(type, button) {
	        _Helpers.body.on('click tap', button, function () {
	          var dataObject = void 0;
	
	          if (!_this.dataObject) {
	            dataObject = $(this).parents('.default-item');
	          } else {
	            dataObject = _this.dataObject;
	          }
	
	          _this.changeCartQuantity(dataObject, type);
	        });
	      }
	
	      bindButton(this.buyEventName, this.buyButton);
	      bindButton(this.removeEventName, this.removeButton);
	    }
	  }]);
	
	  return CartItem;
	}(CartController);
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ },
/* 31 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function($) {'use strict';
	
	/**
	 * Shops page scripts.
	 *
	 * @module Shops
	 */
	
	/** Import utility functions */
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	/** Import google maps class */
	
	
	var _Helpers = __webpack_require__(29);
	
	var _GoogleMaps = __webpack_require__(26);
	
	var _GoogleMaps2 = _interopRequireDefault(_GoogleMaps);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	/** Class representing a website's Shops page functions. Initialized only on web site's Shops pages. */
	var Shops = function () {
	  /**
	   * Cache Shops page DOM elements for instant access.
	   */
	  function Shops() {
	    _classCallCheck(this, Shops);
	
	    /** Desktop dropdown lists */
	    this.desktopDropDownContainers = $('.map-links-container .map-link');
	    this.desktopDropDownMaplinks = this.desktopDropDownContainers.find('.map-link-address');
	
	    /** Mobile select dropdowns */
	    this.selectDropdowns = $('.dropdown-box-container .dropdown-box');
	    this.cityDropdown = $('.dropdown-box-container .dropdown-box.city');
	    this.shopDropdown = $('.dropdown-box-container .dropdown-box.shop');
	    this.dropdownScrollContainer = $('.dropdown-box-options > .relative');
	
	    /** Map links */
	    this.mapLinks = $('.map-link-address');
	
	    /** Class names */
	    this.disabledSelectClass = 'disabled';
	
	    /** Other */
	    this.psOptions = {
	      suppressScrollX: true,
	      swipePropagation: false
	    };
	  }
	
	  /**
	   * Initialize desktop dropdown lists.
	   *
	   * @return {Shops}
	   */
	
	
	  _createClass(Shops, [{
	    key: 'initDesktopDropdowns',
	    value: function initDesktopDropdowns() {
	      this.desktopDropDownContainers.each(function (index, container) {
	        var $container = $(container);
	        var trigger = $container.find('.title');
	
	        (0, _Helpers.toggleDropdown)(trigger, $container);
	      });
	
	      return this;
	    }
	
	    /**
	     * Sort dropdowns (city, shop).
	     * Dropdowns, select one from list.
	     *
	     * @return {Shops}
	     */
	
	  }, {
	    key: 'initDropdownSelects',
	    value: function initDropdownSelects() {
	      this.selectDropdowns.each(function (index, el) {
	        var element = $(el);
	        var trigger = element.find('.dropdown-box-title');
	        (0, _Helpers.dropdownItemInit)(trigger, element);
	
	        var sortItemSelected = element.find('.dropdown-box-title > span');
	        var sortItem = element.find('.dropdown-box-option');
	        (0, _Helpers.selectOneFromListInit)(sortItem, sortItemSelected, element, 'shop.select');
	
	        sortItem.on('click tap', function () {});
	      });
	
	      return this;
	    }
	
	    /**
	     * Fire 'city change' event.
	     *
	     * @param {Number|String} cityId
	     * @param {Object} anotherParams
	     * @return {Shops}
	     */
	
	  }, {
	    key: 'fireCityChangeEvent',
	    value: function fireCityChangeEvent(cityId) {
	      var anotherParams = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
	
	      var shopDropdown = this.shopDropdown;
	      var disabledSelectClass = this.disabledSelectClass;
	
	      if (shopDropdown.hasClass(disabledSelectClass)) {
	        shopDropdown.removeClass(disabledSelectClass);
	      }
	
	      // Fire change event
	      shopDropdown.attr('data-city-id', cityId).trigger('change.city.id');
	    }
	
	    /**
	     * Select and set city ID.
	     *
	     * @return {Shops}
	     */
	
	  }, {
	    key: 'initCitySelect',
	    value: function initCitySelect() {
	      var _this = this;
	
	      var cityDropdownOptions = this.cityDropdown.find('.dropdown-box-option');
	
	      cityDropdownOptions.each(function (index, option) {
	        var $option = $(option);
	        var value = $option.data('value');
	
	        // Fire change event
	        $option.on('click tap', function () {
	          return _this.fireCityChangeEvent(value);
	        });
	      });
	
	      return this;
	    }
	
	    /**
	     * Init city select from desktop dropdown list.
	     *
	     * @retirn {Shops}
	     */
	
	  }, {
	    key: 'initCitySelectFromDesktop',
	    value: function initCitySelectFromDesktop() {
	      var _this2 = this;
	
	      this.desktopDropDownMaplinks.each(function (index, link) {
	        var $link = $(link);
	        var value = $link.parents('.map-link').data('city-id');
	        var maplinkId = $link.data('maplink-id');
	
	        var imitateSelect = function () {
	          // Select city
	          this.cityDropdown.find('.dropdown-box-option[data-value="' + value + '"]').trigger('shop.select');
	          // Select maplink
	          this.shopDropdown.find('.dropdown-box-option[data-maplink-id="' + maplinkId + '"]').trigger('shop.select');
	        }.bind(_this2);
	
	        // Fire change event
	        $link.on('click tap', function () {
	          imitateSelect();
	
	          _this2.fireCityChangeEvent(value);
	        });
	      });
	
	      return this;
	    }
	
	    /**
	     * Filter 'shops' dropdown on city select.
	     *
	     * @return {Shops}
	     */
	
	  }, {
	    key: 'initShopSortSelect',
	    value: function initShopSortSelect() {
	      this.shopDropdown.on('change.city.id', function () {
	        var $this = $(this);
	        var value = this.dataset.cityId;
	        var allOptions = $this.find('.dropdown-box-option');
	        var showThisOptions = $this.find('.dropdown-box-option[data-city-id="' + value + '"]');
	
	        // Switch cities
	        allOptions.hide();
	        showThisOptions.show();
	
	        // update PerfectScrollbar
	        var psContainer = $this.find('.ps-container');
	        psContainer.perfectScrollbar('update');
	      });
	
	      return this;
	    }
	
	    /**
	     * Init google map.
	     *
	     * @return {Shops}
	     */
	
	  }, {
	    key: 'initGoogleMap',
	    value: function initGoogleMap() {
	      var GoogleMapsInstance = new _GoogleMaps2.default({}, {}, this.mapLinks);
	      GoogleMapsInstance.init();
	
	      return this;
	    }
	
	    /**
	     * Initialize perfect scrollbar.
	     *
	     * @return {Shops}
	     */
	
	  }, {
	    key: 'initPerfectScrollbar',
	    value: function initPerfectScrollbar() {
	      var _this3 = this;
	
	      !/* require.ensure */(function () {/* WEBPACK VAR INJECTION */(function($) {
	        var perfectScrollbar = __webpack_require__(3);
	
	        // init Perfect Scrollbar
	        _this3.dropdownScrollContainer.each(function (index, el) {
	          $(el).perfectScrollbar(_this3.psOptions);
	          $(window).on('resize orientationchange', function () {
	            $(el).perfectScrollbar('update');
	          });
	        });
	      
	/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(1)))}(__webpack_require__));
	
	      return this;
	    }
	
	    /**
	     * Initialize Shops page scripts.
	     *
	     * @static
	     */
	
	  }], [{
	    key: 'init',
	    value: function init() {
	      var obj = new this();
	
	      obj.initDesktopDropdowns().initDropdownSelects().initCitySelect().initCitySelectFromDesktop().initShopSortSelect().initPerfectScrollbar().initGoogleMap();
	    }
	  }]);
	
	  return Shops;
	}();
	
	exports.default = Shops;
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ },
/* 32 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;(function (factory) {
	    if (true) {
	        // AMD. Register as an anonymous module.
	        !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(1)], __WEBPACK_AMD_DEFINE_FACTORY__ = (factory), __WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ? (__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	    } else if (typeof exports === 'object') {
	        // Node/CommonJS
	        factory(require('jquery'));
	    } else {
	        // Browser globals
	        factory(jQuery);
	    }
	}(function ($) {
	
	var ua = navigator.userAgent,
		iPhone = /iphone/i.test(ua),
		chrome = /chrome/i.test(ua),
		android = /android/i.test(ua),
		caretTimeoutId;
	
	$.mask = {
		//Predefined character definitions
		definitions: {
			'9': "[0-9]",
			'a': "[A-Za-z]",
			'*': "[A-Za-z0-9]"
		},
		autoclear: true,
		dataName: "rawMaskFn",
		placeholder: '_'
	};
	
	$.fn.extend({
		//Helper Function for Caret positioning
		caret: function(begin, end) {
			var range;
	
			if (this.length === 0 || this.is(":hidden") || this.get(0) !== document.activeElement) {
				return;
			}
	
			if (typeof begin == 'number') {
				end = (typeof end === 'number') ? end : begin;
				return this.each(function() {
					if (this.setSelectionRange) {
						this.setSelectionRange(begin, end);
					} else if (this.createTextRange) {
						range = this.createTextRange();
						range.collapse(true);
						range.moveEnd('character', end);
						range.moveStart('character', begin);
						range.select();
					}
				});
			} else {
				if (this[0].setSelectionRange) {
					begin = this[0].selectionStart;
					end = this[0].selectionEnd;
				} else if (document.selection && document.selection.createRange) {
					range = document.selection.createRange();
					begin = 0 - range.duplicate().moveStart('character', -100000);
					end = begin + range.text.length;
				}
				return { begin: begin, end: end };
			}
		},
		unmask: function() {
			return this.trigger("unmask");
		},
		mask: function(mask, settings) {
			var input,
				defs,
				tests,
				partialPosition,
				firstNonMaskPos,
	            lastRequiredNonMaskPos,
	            len,
	            oldVal;
	
			if (!mask && this.length > 0) {
				input = $(this[0]);
	            var fn = input.data($.mask.dataName)
				return fn?fn():undefined;
			}
	
			settings = $.extend({
				autoclear: $.mask.autoclear,
				placeholder: $.mask.placeholder, // Load default placeholder
				completed: null
			}, settings);
	
	
			defs = $.mask.definitions;
			tests = [];
			partialPosition = len = mask.length;
			firstNonMaskPos = null;
	
			mask = String(mask);
	
			$.each(mask.split(""), function(i, c) {
				if (c == '?') {
					len--;
					partialPosition = i;
				} else if (defs[c]) {
					tests.push(new RegExp(defs[c]));
					if (firstNonMaskPos === null) {
						firstNonMaskPos = tests.length - 1;
					}
	                if(i < partialPosition){
	                    lastRequiredNonMaskPos = tests.length - 1;
	                }
				} else {
					tests.push(null);
				}
			});
	
			return this.trigger("unmask").each(function() {
				var input = $(this),
					buffer = $.map(
	    				mask.split(""),
	    				function(c, i) {
	    					if (c != '?') {
	    						return defs[c] ? getPlaceholder(i) : c;
	    					}
	    				}),
					defaultBuffer = buffer.join(''),
					focusText = input.val();
	
	            function tryFireCompleted(){
	                if (!settings.completed) {
	                    return;
	                }
	
	                for (var i = firstNonMaskPos; i <= lastRequiredNonMaskPos; i++) {
	                    if (tests[i] && buffer[i] === getPlaceholder(i)) {
	                        return;
	                    }
	                }
	                settings.completed.call(input);
	            }
	
	            function getPlaceholder(i){
	                if(i < settings.placeholder.length)
	                    return settings.placeholder.charAt(i);
	                return settings.placeholder.charAt(0);
	            }
	
				function seekNext(pos) {
					while (++pos < len && !tests[pos]);
					return pos;
				}
	
				function seekPrev(pos) {
					while (--pos >= 0 && !tests[pos]);
					return pos;
				}
	
				function shiftL(begin,end) {
					var i,
						j;
	
					if (begin<0) {
						return;
					}
	
					for (i = begin, j = seekNext(end); i < len; i++) {
						if (tests[i]) {
							if (j < len && tests[i].test(buffer[j])) {
								buffer[i] = buffer[j];
								buffer[j] = getPlaceholder(j);
							} else {
								break;
							}
	
							j = seekNext(j);
						}
					}
					writeBuffer();
					input.caret(Math.max(firstNonMaskPos, begin));
				}
	
				function shiftR(pos) {
					var i,
						c,
						j,
						t;
	
					for (i = pos, c = getPlaceholder(pos); i < len; i++) {
						if (tests[i]) {
							j = seekNext(i);
							t = buffer[i];
							buffer[i] = c;
							if (j < len && tests[j].test(t)) {
								c = t;
							} else {
								break;
							}
						}
					}
				}
	
				function androidInputEvent(e) {
					var curVal = input.val();
					var pos = input.caret();
					if (oldVal && oldVal.length && oldVal.length > curVal.length ) {
						// a deletion or backspace happened
						checkVal(true);
						while (pos.begin > 0 && !tests[pos.begin-1])
							pos.begin--;
						if (pos.begin === 0)
						{
							while (pos.begin < firstNonMaskPos && !tests[pos.begin])
								pos.begin++;
						}
						input.caret(pos.begin,pos.begin);
					} else {
						var pos2 = checkVal(true);
						var lastEnteredValue = curVal.charAt(pos.begin);
						if (pos.begin < len){
							if(!tests[pos.begin]){
								pos.begin++;
								if(tests[pos.begin].test(lastEnteredValue)){
									pos.begin++;
								}
							}else{
								if(tests[pos.begin].test(lastEnteredValue)){
									pos.begin++;
								}
							}
						}
						input.caret(pos.begin,pos.begin);
					}
					tryFireCompleted();
				}
	
	
				function blurEvent(e) {
	                checkVal();
	
	                if (input.val() != focusText)
	                    input.change();
	            }
	
				function keydownEvent(e) {
	                if (input.prop("readonly")){
	                    return;
	                }
	
					var k = e.which || e.keyCode,
						pos,
						begin,
						end;
	                    oldVal = input.val();
					//backspace, delete, and escape get special treatment
					if (k === 8 || k === 46 || (iPhone && k === 127)) {
						pos = input.caret();
						begin = pos.begin;
						end = pos.end;
	
						if (end - begin === 0) {
							begin=k!==46?seekPrev(begin):(end=seekNext(begin-1));
							end=k===46?seekNext(end):end;
						}
						clearBuffer(begin, end);
						shiftL(begin, end - 1);
	
						e.preventDefault();
					} else if( k === 13 ) { // enter
						blurEvent.call(this, e);
					} else if (k === 27) { // escape
						input.val(focusText);
						input.caret(0, checkVal());
						e.preventDefault();
					}
				}
	
				function keypressEvent(e) {
	                if (input.prop("readonly")){
	                    return;
	                }
	
					var k = e.which || e.keyCode,
						pos = input.caret(),
						p,
						c,
						next;
	
					if (e.ctrlKey || e.altKey || e.metaKey || k < 32) {//Ignore
						return;
					} else if ( k && k !== 13 ) {
						if (pos.end - pos.begin !== 0){
							clearBuffer(pos.begin, pos.end);
							shiftL(pos.begin, pos.end-1);
						}
	
						p = seekNext(pos.begin - 1);
						if (p < len) {
							c = String.fromCharCode(k);
							if (tests[p].test(c)) {
								shiftR(p);
	
								buffer[p] = c;
								writeBuffer();
								next = seekNext(p);
	
								if(android){
									//Path for CSP Violation on FireFox OS 1.1
									var proxy = function() {
										$.proxy($.fn.caret,input,next)();
									};
	
									setTimeout(proxy,0);
								}else{
									input.caret(next);
								}
	                            if(pos.begin <= lastRequiredNonMaskPos){
			                         tryFireCompleted();
	                             }
							}
						}
						e.preventDefault();
					}
				}
	
				function clearBuffer(start, end) {
					var i;
					for (i = start; i < end && i < len; i++) {
						if (tests[i]) {
							buffer[i] = getPlaceholder(i);
						}
					}
				}
	
				function writeBuffer() { input.val(buffer.join('')); }
	
				function checkVal(allow) {
					//try to place characters where they belong
					var test = input.val(),
						lastMatch = -1,
						i,
						c,
						pos;
	
					for (i = 0, pos = 0; i < len; i++) {
						if (tests[i]) {
							buffer[i] = getPlaceholder(i);
							while (pos++ < test.length) {
								c = test.charAt(pos - 1);
								if (tests[i].test(c)) {
									buffer[i] = c;
									lastMatch = i;
									break;
								}
							}
							if (pos > test.length) {
								clearBuffer(i + 1, len);
								break;
							}
						} else {
	                        if (buffer[i] === test.charAt(pos)) {
	                            pos++;
	                        }
	                        if( i < partialPosition){
	                            lastMatch = i;
	                        }
						}
					}
					if (allow) {
						writeBuffer();
					} else if (lastMatch + 1 < partialPosition) {
						if (settings.autoclear || buffer.join('') === defaultBuffer) {
							// Invalid value. Remove it and replace it with the
							// mask, which is the default behavior.
							if(input.val()) input.val("");
							clearBuffer(0, len);
						} else {
							// Invalid value, but we opt to show the value to the
							// user and allow them to correct their mistake.
							writeBuffer();
						}
					} else {
						writeBuffer();
						input.val(input.val().substring(0, lastMatch + 1));
					}
					return (partialPosition ? i : firstNonMaskPos);
				}
	
				input.data($.mask.dataName,function(){
					return $.map(buffer, function(c, i) {
						return tests[i]&&c!=getPlaceholder(i) ? c : null;
					}).join('');
				});
	
	
				input
					.one("unmask", function() {
						input
							.off(".mask")
							.removeData($.mask.dataName);
					})
					.on("focus.mask", function() {
	                    if (input.prop("readonly")){
	                        return;
	                    }
	
						clearTimeout(caretTimeoutId);
						var pos;
	
						focusText = input.val();
	
						pos = checkVal();
	
						caretTimeoutId = setTimeout(function(){
	                        if(input.get(0) !== document.activeElement){
	                            return;
	                        }
							writeBuffer();
							if (pos == mask.replace("?","").length) {
								input.caret(0, pos);
							} else {
								input.caret(pos);
							}
						}, 10);
					})
					.on("blur.mask", blurEvent)
					.on("keydown.mask", keydownEvent)
					.on("keypress.mask", keypressEvent)
					.on("input.mask paste.mask", function() {
	                    if (input.prop("readonly")){
	                        return;
	                    }
	
						setTimeout(function() {
							var pos=checkVal(true);
							input.caret(pos);
	                        tryFireCompleted();
						}, 0);
					});
	                if (chrome && android)
	                {
	                    input
	                        .off('input.mask')
	                        .on('input.mask', androidInputEvent);
	                }
					checkVal(); //Perform initial check for existing values
			});
		}
	});
	}));


/***/ }
/******/ ]);
//# sourceMappingURL=index.pc.bundle.js.map