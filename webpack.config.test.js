'use strict';

const webpack  			 = require('webpack');
const path 	   			 = require("path");
const WebpackAssetsManifest = require('webpack-assets-manifest');
const ChunkManifestPlugin = require('chunk-manifest-webpack-plugin');
const WebpackMd5Hash = require('webpack-md5-hash');
const fs = require('fs');

const INCORRECT_CHUNKS_MANIFEST_FILENAME = 'trash (delete on prod)/wrong-chunks-manifest-file.json';
const ASSETS_MANIFEST_FILENAME = 'webpack-assets-manifest.json';
const CHUNKS_MANIFEST_FILENAME = 'webpack-chunks-manifest.json';
const ASSETS_MANIFEST_VARIABLE = 'webpackManifest';

const outputPath = path.resolve(__dirname, 'test/test-js/');

module.exports = {
	entry: {
		"common.test": "./test/test-js-es6/common.test.js",
		"header.test": "./test/test-js-es6/header.test.js",
		"menu.test": "./test/test-js-es6/menu.test.js",
		"footer.test": "./test/test-js-es6/footer.test.js",
		"validator.test": "./test/test-js-es6/validator.test.js",

		"main-page.test": "./test/test-js-es6/main-page.test.js",
		"catalog-page.test": "./test/test-js-es6/catalog-page.test.js",
		"filter-page.test": "./test/test-js-es6/filter-page.test.js",
		"product-page.test": "./test/test-js-es6/product-page.test.js",
		"cart-page.test": "./test/test-js-es6/cart-page.test.js",
		"registration-page.test": "./test/test-js-es6/registration-page.test.js",
		"calendar-page.test": "./test/test-js-es6/calendar-page.test.js",
		"news-page.test": "./test/test-js-es6/news-page.test.js",
		"search-page.test": "./test/test-js-es6/search-page.test.js",
		"shops-page.test": "./test/test-js-es6/shops-page.test.js"
	},


	output: {
		path: outputPath,
		publicPath: './',
		filename: "[name].js",
    chunkFilename: "chunks/chunk-[id]-[chunkhash].js"
	},

	watch: true,

	watchOptions: {
		aggregateTimeout: 100
	},

	devtool: "source-map",

	plugins: [
		new WebpackMd5Hash(),

		new webpack.ProvidePlugin({
			$: 'jquery',
			jQuery: 'jquery'
		}),

		new webpack.NoErrorsPlugin(),

		new WebpackAssetsManifest({
			output: ASSETS_MANIFEST_FILENAME,
			replacer: null,
			space: 2,
			writeToDisk: false,
			sortManifest: true,
			merge: false,
			publicPath: '',
			done: (manifest) => {
				console.log(`The manifest has been written to ${manifest.getOutputPath()}`);
			}
		}),

		new ChunkManifestPlugin({
			filename: INCORRECT_CHUNKS_MANIFEST_FILENAME,
			manifestVariable: ASSETS_MANIFEST_VARIABLE
		}),

		function() {
			this.plugin('done', function(stats) {
				var result = {};

				stats.toJson().assets.filter(function(item) {
					return item.name.match('.js$') !== null
				}).forEach(function(item) {
					result[item.chunks[0]] = item.name
				});

				fs.writeFileSync(
					path.join(path.resolve(__dirname, 'test/test-js'), CHUNKS_MANIFEST_FILENAME),
					JSON.stringify(result));
			});
		},

		function() {
			this.plugin('done', function() {
				const pathToWrongManifest = path.join(outputPath, INCORRECT_CHUNKS_MANIFEST_FILENAME);

				try {
					fs.accessSync(path, fs.F_OK);
					fs.unlink(pathToWrongManifest);
				} catch (e) {
					console.log('Chunks to delete were not found')
				}
			});
		}
	],

	resolve: {
		modulesDirectories: ['node_modules'],
		extensions: ['', '.js'],
		root: [path.join(__dirname, './src')],
		alias: {}
	},

	module: {
	  loaders: [
	    {
	      loader: 'babel',
	      test: /\.js$/,
				exclude: [/node_modules/, path.resolve(__dirname, 'src/js/modules/dep')],
	      query: {
		      presets: ['es2015', 'stage-2']
	      }
	    },
			{
				loader: 'mocha',
				test: /\.js$/,
				exclude: [/node_modules/, /src/]
			}
	  ]
	}
};