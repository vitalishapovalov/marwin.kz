'use strict';

// JSDocs
const jsdoc          = require('gulp-jsdoc3');

// utils
const gulp     	     = require('gulp');
const runSequence    = require('run-sequence');
const plumber        = require('gulp-plumber');
const gutil          = require('gulp-util');

// live-ftp-upload
const connect        = require('gulp-connect');
const ftp            = require('vinyl-ftp');

// html
const pug            = require('gulp-pug');

// css
const sourcemaps     = require('gulp-sourcemaps');
const pleeease       = require('gulp-pleeease');
const rename         = require('gulp-rename');

// js ( see webpack.config file for bundle settings )
const Webpack        = require('webpack');
const webpack_stream = require('webpack-stream');
const webpackConfig  = require('./webpack.config.js');
const webpackConfigTest = require('./webpack.config.test.js');
const webpackConfigPc = require('./webpack.config.pc.js');

// live reload with browsersync
const browserSync    = require('browser-sync').create();

// prod/dev variables for different bundles
const NODE_ENV = process.env.NODE_ENV || 'development';
const NODE_DEP = process.env.NODE_DEP || 'livereload';
const development = NODE_ENV == 'development';
const production  = NODE_ENV == 'production';

// configure webpack options
if ( development ) {
    // set watcher and it's options
    webpackConfig.watch = true;
    webpackConfig.watchOptions = {
        aggregateTimeout: 100
    }
} else if ( production ) {
    // disable watcher
    webpackConfig.watch = false;
    // minify js/css output file
    webpackConfig.plugins.push(
        new Webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false,
                unsafe: true
            }
        })
    );
}
//----------------------------------------FTP TASKS----------------------------------------//

// vars for server connection and deploy task
const user = process.env.FTP_USER;
const password = process.env.FTP_PWD;
const host = '194.28.86.115';
const port = 21;
const localFilesGlob = ['www/**/*'];
const remoteFolder = 'public_html/meloman';

// build an FTP connection
function getFtpConnection() {
    return ftp.create({
        host: host,
        port: port,
        user: user,
        password: password,
        parallel: 5,
        log: gutil.log
    });
}

// upload files to the server on every file change in www/
gulp.task('ftp-deploy-watch', () => {
    let conn = getFtpConnection();
    gulp.watch(localFilesGlob)
        .on('change', function(event) {
            console.log('Changes detected! Uploading file "' + event.path + '", ' + event.type);
            return gulp.src( [event.path], { base: '.', buffer: false } )
                .pipe(plumber())
                .pipe( conn.newer( remoteFolder ) ) // only upload newer files
                .pipe( conn.dest( remoteFolder ) );
        });
});

//-----------------------------------------HTML TASKS---------------------------------------//

function compileHtml(src, dest) {
  return gulp.src([src])
    .pipe(plumber())
    .pipe(pug(pugOptions))
    .pipe(gulp.dest(dest));
}
let pugOptions = {
    pretty: false,
    nspaces: 4,
    tabs: true,
    donotencode: true
};
if ( production )
    pugOptions.pretty = true;

//compile .pug to .html
gulp.task('pug_compile', () => compileHtml('src/pug/pages/*.pug', './www/'));

// watch .pug files
gulp.task('watch-html', () => {
    gulp.watch('src/pug/**/*.pug', ['pug_compile']);
});

//-------------------------------------------CSS TASKS----------------------------------------//

function compileCss(src, dest) {
  gulp.src(src)
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(pleeease({
      stylus: {
        paths: ['./src/styl/**']
      },
      browsers: ["last 3 versions", "Android 2.3", "ie 8", "Firefox ESR", "Opera 12.1", "Chrome > 20", "ios 4"],
      minifier: {
        preserveHacks: true,
        removeAllComments: true
      }
    }))
    .pipe(rename({
      suffix: '.min',
      extname: '.css'
    }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(dest));
}

// pleeease
gulp.task('pls', () => compileCss('./src/styl/index.styl', './www/css/'));

// watch for changes
gulp.task('watch-css', () => {
    gulp.watch('src/styl/**/*.styl', ['pls']);
});

//-----------------------------------------WEBPACK TASKS--------------------------------------//

// run webpack
gulp.task('webpack', () => {
    return gulp.src('src/js/index.js')
        .pipe(webpack_stream( webpackConfig ))
        .pipe(gulp.dest('www/'));
});

//---------------------------------------LIVE RELOAD TASK-----------------------------------//

// initialize BS and watch changes
gulp.task('livereload', () => {
    browserSync.init({
        server: 'www'
    });
    browserSync.watch('./www/**/*.*').on('change', browserSync.reload);
});

//-----------------------------------------DEFAULT TASK------------------------------------//

// variables for deploy/livereload
const livereload = NODE_DEP == 'livereload';
const deployment = NODE_DEP == 'deployment';

// configure task sequence ( dev/prod )
let tasksArray = ['webpack', 'watch-html', 'watch-css'];
if ( livereload )
    tasksArray.push('livereload');
else if ( deployment )
    tasksArray.push('ftp-deploy-watch');

// default task
gulp.task('default', () => {
    runSequence(tasksArray);
});

//------------------------------------------PERSONAL CABINET-------------------------------//

// initialize BS and watch changes
gulp.task('livereload-pc', () => {
  browserSync.init({
    server: 'personal_cabinet'
  });
  browserSync.watch('./personal_cabinet/**/*.*').on('change', browserSync.reload);
});

//compile .pug to .html (PERSONAL CABINET)
gulp.task('pug_compile-pc', () => compileHtml('src/pug/pages/other/personal_cabinet/*.pug', './personal_cabinet/'));

// watch .pug files
gulp.task('watch-html-pc', () => {
  gulp.watch('src/pug/pages/other/personal_cabinet/*.pug', ['pug_compile-pc']);
});

// pleeease
gulp.task('pls-pc', () => compileCss('./src/styl/index.pc.styl', './personal_cabinet/css/'));

// watch for changes
gulp.task('watch-css-pc', () => {
  gulp.watch('src/styl/**/*.styl', ['pls-pc']);
});

// run webpack
gulp.task('webpack-pc', () => {
  return gulp.src('src/js/index.pc.js')
    .pipe(webpack_stream( webpackConfigPc ))
    .pipe(gulp.dest('personal_cabinet/'));
});

gulp.task('default-pc', () => {
  runSequence([
    'watch-html-pc', 'watch-css-pc', 'webpack-pc', 'livereload-pc'
  ])
});

//------------------------------------------TESTS TASK-------------------------------------//

// compile .pug to .html (test)
gulp.task('pug_compile-test', () => {
  return gulp.src(['src/pug/test/*.pug'])
    .pipe(plumber())
    .pipe(pug({
      pretty: true,
      nspaces: 4,
      tabs: true,
      donotencode: true
    }))
    .pipe(gulp.dest('./test/'));
});

// watch test files
gulp.task('watch-test-pug', () => {
  gulp.watch('src/pug/test/*.pug', ['pug_compile-test']);
});

// run webpack
gulp.task('webpack-test', () => {
  return gulp.src('test/test-js/*.js')
    .pipe(webpack_stream( webpackConfigTest ))
    .pipe(gulp.dest('test/test-js/'));
});

// default task
gulp.task('tests', () => {
  runSequence(['watch-test-pug', 'webpack-test']);
});

//------------------------------------------JSDocs TASK------------------------------------//

gulp.task('JSDocs', function (cb) {
    gulp.src(['README.md', './src/js/**/*.js'], {read: false})
      .pipe(jsdoc(cb));
});